<?php

error_reporting(E_ALL);

// Report all PHP errors
error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);


date_default_timezone_set("Asia/Bangkok");


/* task-id */
$taskId = date("d-m-Y-H-i-s") . "-" . uniqid();

/* input xls file */
$menu = "user_status_leave";
$inputXls   = "c:\\update_status_type2_resign.xls";  // full path (uploaded file)


$dest = "C:\\sap-data\\inbox\\user_status_leave";

$outputText = "c:\\inetpub\\wwwroot\\public\\inbox\\$menu\\$taskId-" . $menu .".txt";


$r = shell_exec("c:\\inetpub\\wwwroot\\public\\xls2txt.bat $inputXls $outputText $taskId $dest ");



// progress status
// "select * from TBL_IMPORT_PROGRESS where TRANS_ID = $taskId and [TOTAL_RECORDS] = [LINE_NO] 
//  and check TRANS_ID in table TBL_IMPORT_FAIL if have record then mean have fail imported ";
//  if fail  when field  allow user download out put file query record from TBL_IMPORT_FAIL 

echo("task-id: $r");

?>