@extends('backend.layouts.default')
@section('content')
<?php
$data = getmemulist();
$arrSidebar =getSideBar($data);
?>

<style type="text/css">
 #graph_label .tickLabel{

    font-size: 100%;
 }

/* CheckBox */ 
.hiddencol { display:none; };
.showcol {display:none;};
 
/* Scroll Menu */
#meacontainer {
     position:absolute;
     width:100%;
     top:0;
     bottom:0;
     /*left:154;*/
     z-index:-1;
     overflow:hidden;
     background: #FF0000;
 }

 #dpsettings {
     overflow-y: scroll;
     width:200px;
     background: #FFFFFF;
     float:left;
     height:350px;
     margin-top:-5px;
     margin-left:154px
     position: absolute;
 }

 .glyphicon-plus {
       color: #00FF00; 
   }
   .glyphicon-danger {
       color: #b41111; 
   }
   .glyphicon-lightgreen {
       color: #12940d; 
   }
   .glyphicon-green {
       color: #00FF00; 
   }
   .glyphicon-info {
       color: #5bc0de; 
   }
   .glyphicon-pink {
       color: #f20772; 
   }
   .glyphicon-luna {
       color: #c92456; 
   }

   /* table split row */
   .top_row_line {
    display: table;
    width: 100%;
   }

   .top_row_line > div {
    display: table-cell;
    width: 50%;
    border-bottom: 1px solid #ccc;
   }
   /* auto complete */
   .container { width: 800px; margin: 0 auto; }

   .autocomplete-suggestions { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 1px solid #999; background: #FFF; cursor: default; overflow: auto; -webkit-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); -moz-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); }
   .autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
   .autocomplete-no-suggestion { padding: 2px 5px;}
   .autocomplete-selected { background: #F0F0F0; }
   .autocomplete-suggestions strong { font-weight: bold; color: #000; }
   .autocomplete-group { padding: 2px 5px; }
   .autocomplete-group strong { font-weight: bold; font-size: 18px; color: #000; display: block; border-bottom: 1px solid #000; }

   /* Section Header*/
    .custom-combobox {
        position: relative;
        display: inline-block;
    }
    
    .custom-combobox-toggle {
        position: absolute;
        top: 0;
        bottom: 0;
        margin-left: -1px;
        padding: 0;
    }
      .custom-combobox-input {
        margin: 0;
        padding: 5px 10px;
      }

      optgroup {
        background-color: #FF8000; //#DCDCDC;
        color: white;
      }
      option {
        background-color: white;
        color: black;
      }

</style>

<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>
                
                {{getMenutitle($arrSidebar)}}

            </h1>
        </div>

    </div>


    <div class="row" id="widget-grid" >

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <ul class="nav nav-tabs pull-left in">

                        <li class="active">

                            <a data-toggle="tab" href="#hr1"> <i class="fa fa-lg fa-arrow-circle-o-down"></i> <span class="hidden-mobile hidden-tablet"> ระบุเงื่อนไขการค้นหา </span> </a>

                        </li>

                        <!--li>
                            <a data-toggle="tab" href="#hr2"> <i class="fa fa-lg fa-arrow-circle-o-up"></i> <span class="hidden-mobile hidden-tablet"> รายงานสรุปของตราสารทุน  </span> </a>
                        </li-->

                    </ul>
                </header>

                <!-- widget div-->
                <div>


                    <!-- widget content -->
                    <div class="widget-body">

                        <div class="tab-content">
                            <div class="tab-pane active" id="hr1">

                                <div class="widget-body no-padding" style="width: 100% ;margin: 0 auto;background-color: #000000;border: 1px solid #E1E8F3">
                                    <header style="color: #fff; height: 40px; line-height: 40px; font-size: 18px; background-color: #a90329;padding-left: 20px;"> กล่องค้นหา </header>

                                    <form  id="comment-form_darkman" class="smart-form" novalidate="novalidate">

                                        <fieldset>
                                            <div class="row">
                                                 <section class="col col-4">
                                                    <label class="label">&nbsp;ชื่อหลักทรัพย์</label>
                                                    <label class="input">
                                                          <!--autocomplete-dynamic --> 
                                                          <input type="text"  name="symbol" 
                                                                 placeholder="ระบุชื่อหลักทรัพย์"
                                                                 id="symbol" 
                                                                 style="width: 100%; border-width: 1px;"/>
                                                    </label>
                                                </section>

                                                <section class="col col-4">
                                                     <label style="font-size:18px">กลุ่มอุตสาหกรรม</label>
                                                     <label class="input">
                                                        <select name="industrial" id="industrial" class="form-control">
                                                            <option value="">&nbsp;กรุณาเลือก หนึ่งรายการ &nbsp;</option>
                                                            @if($sectionList)
                                                                @foreach($sectionList as $key => $value)
                                                                    <optgroup label="&nbsp;{{$key}}">
                                                                    @foreach($value as $child)                                                                      
                                                                        <option value="{{$child['CATE_ID']}}">&nbsp;&nbsp;&nbsp;{{$child['BU']}}</option>
                                                                    @endforeach
                                                                    </optgroup> 
                                                                @endforeach   
                                                            @endif
                                                        </select>
                                                    </label>
                                                </section>

                                                
                                                <section class="col col-4">
                                                    <label class="label">&nbsp;ชื่อบริษัท จัดการ</label>
                                                    <label class="input">
                                                        <select name="securities_name" id="securities_name" class="form-control">
                                                            <option value="">ชื่อบริษัท จัดการ</option>
                                                            @if($equitylist)
                                                                @foreach($securitieslist as $item)
                                                                    <option value="{{$item->NAME_SHT}}">&nbsp;{{$item->NAME_SHT}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                        
                                                    </label>
                                                </section>
                                            </div>

                                            <div class="row">
                                                <section class="col col-4" style="display:none;">
                                                    <label class="label">ระบุช่วงเวลา</label>
                                                    <label class="input"><i class="icon-append fa fa-calendar"></i>
                                                        <input type="text" class="mea_date_picker" id="date_start"  >
                                                    </label>
                                                </section>
                                                <section class="col col-4" style="display:none;">
                                                    <label class="label">ถึง</label>
                                                    <label class="input"><i class="icon-append fa fa-calendar"></i>
                                                        <input type="text" class="mea_date_picker" id="date_end" >
                                                    </label>
                                                </section>

                                                 <section class="col col-4">
                                                    <label class="label">ระบุเดือน(<strong style="font-size: 18px; color:red;">*</strong>)</label>
                                                    <label class="input"> 
                                                        <select name="sel_month" id="sel_month" class="form-control">
                                                            <option value="">ระบุเดือน</option>
                                                            <option value="1">&nbsp;มกราคม</option>
                                                            <option value="2">&nbsp;กุมภาพันธ์</option>
                                                            <option value="3">&nbsp;มีนาคม</option>
                                                            <option value="4">&nbsp;เมษายน</option>
                                                            <option value="5">&nbsp;พฤษภาคม</option>
                                                            <option value="6">&nbsp;มิถุนายน</option>
                                                            <option value="7">&nbsp;กรกฎาคม</option>
                                                            <option value="8">&nbsp;สิงหาคม</option>
                                                            <option value="9">&nbsp;กันยายน</option>
                                                            <option value="10">&nbsp;ตุลาคม</option>
                                                            <option value="11">&nbsp;พฤศจิกายน</option>
                                                            <option value="12">&nbsp;ธันวาคม</option>
                                                        </select>
                                                    </label>
                                                </section>
                                                <section class="col col-4">
                                                    <label class="label">ระบุปีอ้างอิง(<strong style="font-size: 18px; color:red;">*</strong>)</label>
                                                    <label class="input"> 
                                                         
                                                         <select name="sel_year" id="sel_year" class="form-control">
                                                            <option value="">ระบุปีอ้างอิง</option>
                                                            @if(count($years) > 0)
                                                                @foreach($years as $yyyy)
                                                                    <option value="{{$yyyy}}">&nbsp;{{$yyyy + 543}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </label>

                                                </section>


                                                <section class="col col-4">
                                                    <button type="submit" id="btn_search" style="padding: 5px 20px 5px 20px; margin-top:20px;font-size: 18px;" name="submit" class="btn btn-primary">
                                                        ค้นหา
                                                    </button>
                                                </section>
                                            </div>
                                        </fieldset>
                                    </form>

                                </div>
                               
                                <input type="hidden" id="table_title" name="table_title" value="{{getMenutitle($arrSidebar)}}"/> 

                                <a href="#" id="export_search" style="margin-top: 30px;" class="btn btn-labeled btn-success mea-btn-export"> <span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i></span>Export </a>
                               
                                <!-- Widget ID (each widget will need unique ID)-->
                                <div class="jarviswidget jarviswidget-color-blueDark" style="margin-top: 5px;" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
                                    <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                    data-widget-colorbutton="false"
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true"export_search
                                    data-widget-sortable="false"

                                    -->

                                    <select class="form-control mea-pagesize" id="page-size-search">
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>

                                    <header>
                                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    </header>

                                    <!-- widget div-->
                                    <div>

                                        <!-- widget edit box -->
                                        <div class="jarviswidget-editbox">
                                            <!-- This area used as dropdown edit box -->

                                        </div>
                                        <!-- end widget edit box -->

                                        <!-- widget content -->
                                        <div class="widget-body">
                                            <p class="report-title">
                                                  {{--getMenutitle($arrSidebar)--}}
                                            </p>
                                            <p class="report-period"> </p>
                                            <div class="table-responsive">
                                                <div id="serch_data">

                                                </div>
                                            </div>

                                        </div>
                                        <!-- end widget content -->

                                    </div>
                                    <!-- end widget div -->

                                </div>
                                <!-- end widget -->

                            </div>
                            

                        </div>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->



        </article>
        <!-- WIDGET END -->



    </div>



</div>
<!-- END MAIN CONTENT -->

<!-- ECHART english -->
<script src="{{asset('backend/js/plugin/echart/www/js/echarts-all-english-v2.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('backend/js/plugin/echart/www/js/style.css')}}" />

<!-- Auto Complete -->
<script src="{{asset('backend/js/plugin/jquery-autocomplete/scripts/jquery.mockjax.js')}}"></script> 
<script src="{{asset('backend/js/plugin/jquery-autocomplete/src/jquery.autocomplete.js')}}"></script> 
 

<script type="text/javascript">

     /**
     * Auto Complete
     * see AdminP2EquityBrokerageReport :: "SELECT SYMBOL, COMP_NAME FROM TBL_P2_EQUITY_INDEX ORDER BY SYMBOL ASC";
     */
var symbols = { 
        @foreach($equitybroker as $item)
            '{{$item->NAME_SHT}}' : '{{$item->NAME_SHT}} | {{$item->BROKER_NAME}}', 
        @endforeach
    }

    // START: EChart 
    /**
     * EChart theme
     */
var theme1 = {
    backgroundColor: '#FFFFFF',//'#F2F2E6',
    //  
    color: [
        '#E49e09','#d32f2f','#8AED35','#ef3430','#8AED35','#30b193','#8AED35','#F4E24E','#FE9616','#ff69b4',
        '#E42B6D','#ba55d3','#cd5c5c','#ffa500','#40e0d0',
        '#E95569','#ff6347','#7b68ee','#00fa9a','#ffd700',
        '#6699FF','#ff6666','#3cb371','#b8860b','#30e0e0'
    ],

    //  
    title: {
        //backgroundColor: '#FFFFFF', //'#F2F2E6',
        itemGap: 0 ,                
        textStyle: {
           // color: '#000', //'#8A826D',
            fontFamily : 'DB Ozone X'
        },
        subtextStyle: {
            color: '#8A826D',//'#E877A3',
            fontFamily : 'DB Ozone X'           
        }
    },

    dataRange: {
        x:'right',
        y:'center',
        itemWidth: 5,
        itemHeight:25,
        color:['#E42B6D','#F9AD96'],
        text:['高','低'],          
        textStyle: {
            color: '#8A826D'           
        }
    },

    toolbox: {
        color : ['#E95569','#E95569','#E95569','#E95569'],
        effectiveColor : '#ff4500',
        itemGap: 8
    },

    tooltip: {
        backgroundColor: 'rgba(100,100,79,0.8)', //'rgba(138,130,109,0.5)',      
        axisPointer : {             
            type : 'line',          
            lineStyle : {           
                color: '#6B6455',
                type: 'dashed'
            },
            crossStyle: {           
                color: '#A6A299'
            },
            shadowStyle : {                      
                color: 'rgba(200,200,200,0.3)'
            }
        }
    },

 
    dataZoom: {
        dataBackgroundColor: 'rgba(130,197,209,0.5)',            
        fillerColor: 'rgba(233,84,105,0.1)',    
        handleColor: 'rgba(107,99,84,0.8)'     
    },

     
    grid: {
        borderWidth:0
    },

   
    categoryAxis: {
        axisLine: {             
            lineStyle: {        
                color: '#6B6455'
            }
        },
        splitLine: {            
            show: false
        }
    },

     
    valueAxis: {
        axisLine: {             
            show: false
        },
        splitArea : {
            show: false
        },
        splitLine: {            
            lineStyle: {        
                color: ['#FFF'],
                type: 'dashed'
            }
        }
    },

    polar : {
        axisLine: {             
            lineStyle: {        
                color: '#ddd'
            }
        },
        splitArea : {
            show : true,
            areaStyle : {
                color: ['rgba(250,250,250,0.2)','rgba(200,200,200,0.2)']
            }
        },
        splitLine : {
            lineStyle : {
                color : '#ddd'
            }
        }
    },

    timeline : {
        lineStyle : {
            color : '#6B6455'
        },
        controlStyle : {
            normal : { color : '#6B6455'},
            emphasis : { color : '#6B6455'}
        },
        symbol : 'emptyCircle',
        symbolSize : 3
    },

     
    bar: {
        itemStyle: {
            normal: {
                barBorderRadius: 0
            },
            emphasis: {
                barBorderRadius: 0
            }
        }
    },

 
    line: {
        smooth : true,
        symbol: 'emptyCircle',   
        symbolSize: 3            
    },


     
    k: {
        itemStyle: {
            normal: {
                color: '#E42B6D',       
                color0: '#44B7D3',       
                lineStyle: {
                    width: 1,
                    color: '#E42B6D',    
                    color0: '#44B7D3'    
                }
            }
        }
    },

    scatter: {
        itemStyle: {
            normal: {
                borderWidth:1,
                borderColor:'rgba(200,200,200,0.5)'
            },
            emphasis: {
                borderWidth:0
            }
        },
        symbol: 'circle',   
        symbolSize: 4        
    },

     
    radar : {
        symbol: 'emptyCircle',     
        symbolSize:3 
    },

    map: {
        itemStyle: {
            normal: {
                areaStyle: {
                    color: '#ddd'
                },
                label: {
                    textStyle: {
                        color: '#000' //'#E42B6D'
                    }
                }
            },
            emphasis: {                 
                areaStyle: {
                    color: '#fe994e'
                },
                label: {
                    textStyle: {
                        color: 'rgb(100,0,0)'
                    }
                }
            }
        }
    },

    force : {
        itemStyle: {
            normal: {
                nodeStyle : {
                    borderColor : 'rgba(0,0,0,0)'
                },
                linkStyle : {
                    color : '#6B6455'
                }
            }
        }
    },

    chord : {
        itemStyle : {
            normal : {
                chordStyle : {
                    lineStyle : {
                        width : 0,
                        color : 'rgba(128, 128, 128, 0.5)'
                    }
                }
            },
            emphasis : {
                chordStyle : {
                    lineStyle : {
                        width : 1,
                        color : 'rgba(128, 128, 128, 0.5)'
                    }
                }
            }
        }
    },

    gauge : {                  
        center:['50%','80%'],
        radius:'100%',
        startAngle: 180,
        endAngle : 0,
        axisLine: {            
            show: true,        
            lineStyle: {       
                color: [[0.2, '#44B7D3'],[0.8, '#6B6455'],[1, '#E42B6D']],
                width: '40%'
            }
        },
        axisTick: {            
            splitNumber: 2,    
            length: 5,         
            lineStyle: {       
                color: '#fff'
            }
        },
        axisLabel: {           
            textStyle: {       
                color: '#fff',
                fontWeight:'bolder'
            }
        },
        splitLine: {           
            length: '5%',      
            lineStyle: {       
                color: '#fff'
            }
        },
        pointer : {
            width : '40%',
            length: '80%',
            color: '#fff'
        },
        title : {
          offsetCenter: [0, -20],        
          textStyle: {        
            color: 'auto',
            fontSize: 20
          }
        },
        detail : {
            offsetCenter: [0, 0],       
            textStyle: {        
                color: 'auto',
                fontSize: 40
            }
        }
    }
};
// END: EChart theme

    // checked menu
    var menus = []; 

    (function($)  {
    $.fn.extend({
      check : function()  {
         return this.filter(":checkbox").attr("checked", true);
      },
      uncheck : function()  {
         return this.filter(":checkbox").removeAttr("checked");
      }
    });
    }(jQuery));


    $(document).ready(function(){

        /* validator method */ 
        $.validator.addMethod("valueNotEquals", function(value, element, arg) {
            return arg != value;
        }, "Please Choose one");

        /* register validattion */
        $("#smart-form-register").validate({

            /* rules for form validation */
            rules : {

                sel_year: {
                    required : true
                },

                sel_month: {
                    required : true
                }
            },

            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());

            }
        });


        $('#export_search').on('click',function() {

            var PageSizeAll = $('#page-size-search').val();

            //if($("#smart-form-register").valid()){

                var name_sht    = $('#securities_name').val();   // "UOBAM"
                var symbol      = $('#symbol').val();           // "APCS"
                var cate_id     = $('#industrial').val();
                var array       = symbol.split("|"); 
                if(array.length > 0)
                   symbol = array[0].trim();

                var industrial  = $('#industrial').val();        // CATE_ID --> 21 
                var date_start = $('#hd_date_start').val();
                var date_end = $('#hd_date_end').val();

                var sel_year  = $('#sel_year').val();
                var sel_month = $('#sel_month').val();



                var table_title = $('#table_title').val();

                window.location.href =  "EquityClosedReport/exportsearch?industrial=" + industrial +
                                         "&sel_year=" + sel_year + "&sel_month=" + sel_month +
                                        "&table_title=" + table_title + "&date_start=" +date_start + "&date_end=" + date_end+ 
                                        '&name_sht=' + name_sht+ '&symbol=' + symbol +"&cate_id=" + cate_id;
                return false;
            //}
            //return false;
        });

        meaDatepicker("date_start","date_end");
        meaDatepicker("date_end");

        $('html').append('<input type="hidden" value="1" id="currentpage_search" />');

        $('#btn_search').on('click',function() {
            
            // if($("#smart-form-register").valid()){
                var PageSizeAll = $('#page-size-search').val(); 
                var name_sht    = $('#securities_name').val();   // "UOBAM"
                var symbol      = $('#symbol').val();            // "APCS"
                var array       = symbol.split("|"); 
                if(array.length > 0)
                   symbol = array[0].trim();

                var industrial  = $('#industrial').val();        // CATE_ID --> 21 
                var date_start = $('#hd_date_start').val();
                var date_end = $('#hd_date_end').val();

                var sel_year  = $('#sel_year').val();
                var sel_month = $('#sel_month').val();



                var table_title = $('#table_title').val();

                if(date_start != "" && date_end != "" ){
                    var str =  'ในช่วงวันที่ ' + GetDateFormat(date_start) + ' ถึง ' + GetDateFormat(date_end);
                    $('.report-period').html(str);
                }

                var jsondata = {
                    pagesize : PageSizeAll,
                    PageNumber: 1,
                    industrial : industrial,
                    name_sht :name_sht,
                    symbol : symbol,
                    date_start:date_start,
                    date_end:date_end,
                    sel_month: sel_month,
                    sel_year: sel_year,
                    table_title: table_title
                };

                console.log(jsondata);
                $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
                MeaAjax(jsondata,"EquityClosedReport/search", RenderSearch);
                return false;
            //} // validate
            //return false;
        });


        $("#page-size-search").on('change',function() {
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            var val = $(this).val();

            var name_sht    = $('#securities_name').val();   // "UOBAM"
            var symbol      = $('#symbol').val();            // "APCS"
            var array       = symbol.split("|"); 
            if(array.length > 0)
               symbol = array[0].trim();

            var industrial  = $('#industrial').val();        // CATE_ID --> 21 
            var date_start = $('#hd_date_start').val();
            var date_end = $('#hd_date_end').val();

            var sel_year  = $('#sel_year').val();
            var sel_month = $('#sel_month').val();


            var table_title = $('#table_title').val();
            var jsondata = {
                pagesize : val,
                PageNumber:1,
                industrial : industrial,
                name_sht :name_sht,
                symbol : symbol,
                date_start:date_start,
                date_end:date_end,

                sel_month: sel_month,
                sel_year: sel_year,

                table_title: table_title
            };

            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(jsondata,"EquityClosedReport/search", RenderSearch);

        });

         // Initialize autocomplete with custom appendTo:
        var symbolsArray = $.map(symbols, function (value, key) { return { value: value, data: key }; });
            $('#symbol').autocomplete({
                lookup: symbolsArray
        });
    });  // jquery ready


    function RenderSearch(data){
        $("#serch_data").html(data.html);
        $("#serch_data").fadeIn('300');
        $("#page_click_search li a").on('click', PageRenderSearch);
    }

    function PageRenderSearch(){
//alert('PageRenderSearch');
        var p = $(this).attr('data-page');
        var page_size = $('#page-size-search').val();
        var CurPage = $('#currentpage_search').val();
        $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');

        if(p == "pre"){
            p = parseInt(CurPage) - 1;
        }

        if(p == "next"){
            p = parseInt(CurPage) + 1;
        }
        $('#currentpage_search').val(p);

        //alert('page_size->'+page_size+',CurPage->'+CurPage);

        var name_sht    = $('#securities_name').val();   // "UOBAM"
        var symbol      = $('#symbol').val();            // "APCS"
        var array       = symbol.split("|"); 
        if(array.length > 0)
            symbol = array[0].trim();
        var industrial  = $('#industrial').val();        // CATE_ID --> 21 
        var date_start = $('#hd_date_start').val();
        var date_end = $('#hd_date_end').val();
        var table_title = $('#table_title').val();
        
        /******************************************************************************************************************************** */
        /*
        Author        : Chalermpol Chueayen (chalermpols@msn.com)
        Date Modified : 2019-03-19
        Purpose       : Add parameter for page navigator
        */
        var sel_year  = $('#sel_year').val();
        var sel_month = $('#sel_month').val();
        /******************************************************************************************************************************** */

        var jsondata = {
            pagesize : page_size,
            PageNumber:p,
            industrial : industrial,
            name_sht :name_sht,
            symbol : symbol,
            date_start:date_start,
            date_end:date_end,

            sel_month: sel_month,
            sel_year: sel_year,

            table_title: table_title
        };

        MeaAjax(jsondata,"EquityClosedReport/search", RenderSearch);
    };

</script>

@stop
