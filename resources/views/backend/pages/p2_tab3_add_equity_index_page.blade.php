@extends('backend.layouts.default')
@section('content')
    <?php
    $data = getmemulist();
    $arrSidebar =getSideBar($data);
    ?>

<link rel="stylesheet" href="{{asset('backend/css/jquery-ui.css')}}">
<style>
  .custom-combobox {
    position: relative;
    display: inline-block;
  }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
  }
  .custom-combobox-input {
    margin: 0;
    padding: 5px 10px;
  }

  optgroup {
    background-color: #FF8000; //#DCDCDC;
    color: white;
  }
  option {
    background-color: white;
    color: black;
  }

</style>

    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-table fa-fw "></i>

                    {{--Auth::user()->username--}} 
                    {{getMenutitle($arrSidebar)}}
                </h1>
            </div>

        </div>


        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>


                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        
                        <form id="smart-form-register" action=""   class="smart-form">
                           

                            <fieldset>
                                <div class="row" id="div_record_id" style="display:none;">
                                    <section class="col col-5" >
                                        <lable style="font-size:18px">รหัส หลักทรัพย์</lable>
                                        <label class="input">
                                            <input type="text" id="record_id" name="record_id" placeholder="รหัส บริษัทจัด การหลักทรัพย์" readonly>
                                        </label>
                                    </section>
                                </div>

                                <div class="row"> 
                                    <section class="col col-4">
                                        <lable style="font-size:18px">ตลาด(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                        <label class="input">
                                            <select id="market" name="market" class="form-control">
                                                <option value="">&nbsp;กรุณาเลือก หนึ่งรายการ &nbsp;</option>
                                    
                                                <option value="SET">&nbsp;SET</option>
                                                <option value="mai">&nbsp;mai</option>
                                     
                                            </select>

                                            <!--b class="tooltip tooltip-bottom-right">ระบุ SET Index</b> 

                                            <input type="text" id="market" name="market" placeholder="ระบุตลาด">
                                            <b class="tooltip tooltip-bottom-right">ระบุตลาด</b--> 
                                        </label>
                                    </section>

                                    <section class="col col-4">
                                        <lable style="font-size:18px">SET Index(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                        <label class="input">
                                            <!--input type="text" id="setindex" name="setindex" placeholder="ระบุ SET Index"-->
                                            <select id="setindex" name="setindex" class="form-control">
                                                <option value="">&nbsp;กรุณาเลือก หนึ่งรายการ &nbsp;</option>
                                    
                                                <option value="SET100">&nbsp;SET100</option>
                                                <option value="SET50">&nbsp;SET50</option>
                                                <option value="None">&nbsp;None</option>
                                     
                                            </select>
                                             
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <!--section class="col col-10">
                                        <lable style="font-size:18px">กลุ่มอุตสาหกรรม</lable>
                                        <label class="input">
                                            
                                            <select id="industrial" name="industrial">
                                                <option value="">&nbsp;กรุณาเลือก หนึ่งรายการ &nbsp;</option>
                                    
                                                <option value="SET">&nbsp;SET100</option>
                                                <option value="mai">&nbsp;SET50</option>
                                                <option value="mai">&nbsp;None</option>
                                 
                                            </select>
                                        </label>
                                    </section-->

                                    <section class="col col-6">
                                         <label style="font-size:18px">กลุ่มอุตสาหกรรม(<lable style="font-size:18px; color:red;">*</lable>)</label>
                                         <label class="input">
                                            <select name="industrial" id="industrial" class="form-control">
                                                <option value="">&nbsp;กรุณาเลือก หนึ่งรายการ &nbsp;</option>
                                                @if($sectionList)
                                                  @foreach($sectionList as $key => $value)
                                                    <optgroup label="&nbsp;{{$key}}">
                                                        @foreach($value as $child)
                                                          
                                                              <option value="{{$child['CATE_ID']}}">&nbsp;&nbsp;&nbsp;{{$child['BU']}}</option>

                                                        @endforeach
                                                    </optgroup> 
                                                  @endforeach   
                                                @endif
                                                
                                                
                                            </select>
                                        </label>
                                    </section>
                                </div>

                                <!--div class="row">
                                    <section class="col col-5">
                                        <lable style="font-size:18px">หมวดธุรกิจ</lable>
                                        <label class="input">
                                             <select id="bu" name="bu" class="form-control">
                                                <option value="">&nbsp;กรุณาเลือก หนึ่งรายการ &nbsp;</option>
                                    
                                                <option value="SET">&nbsp;SET100</option>
                                                <option value="mai">&nbsp;SET50</option>
                                                <option value="mai">&nbsp;None</option>
                                 
                                            </select>
                                            
                                        </label>
                                    </section>
                                </div-->
                                <div class="row">
                                    <section class="col col-5">
                                        <lable style="font-size:18px">ชื่อย่อ(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                        <label class="input">
                                            <input type="text" id="name_sht" name="name_sht"
                                             placeholder="ระบุชือย่อหลักทรัพย์">
                                             <b class="tooltip tooltip-bottom-right">ชื่อย่อ</b> 
                                        </label>
                                        
                                    </section>

                                    <div id="div_check_change_name" style="display:none;">
                                        <br/>
                                        <input type="checkbox"  name="check_change_name"  
                                            class="check_change_name" id="check_change_name" />
                                            
                                        <lable style="font-size:18px">เปลี่ยนชื่อหลักทรัพย์</lable>
                                    </div>
                                </div>

                            </fieldset>


                            <fieldset id="change_name" name="change_name" style="display:none;">
                                    <div class="row">
                                    <section class="col col-5">
                                        <lable style="font-size:18px">ชื่อหลักทรัพย์ใหม่</lable>
                                        <label class="input">
                                            <input type="text"  id="new_symbol" name="new_symbol" placeholder="ชื่อหลักทรัพย์ใหม่">
                                            <b class="tooltip tooltip-bottom-right">ชื่อหลักทรัพย์ใหม่</b>
                                        </label>
                                    </section>

                                    <section  class="col col-5">
                                     <lable style="font-size:18px">วันที่มีผล</lable>
                                        <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="effective_date"  class="mea_date_picker" id="effective_date" placeholder="ระบุวันที่มีผล">
                                            <b class="tooltip tooltip-bottom-right">ระบุวันที่มีผล</b>
                                        </label>
                                    </section>
                                    </div>
                             </fieldset>

                             <fieldset>
                                    <div class="row">
                                        <section class="col col-8" >
                                            <lable style="font-size:18px">ชื่อบริษัท(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                            <label class="input">
                                                <input type="text" id="company_name" name="company_name" placeholder="ระบุชื่อบริษัท">
                                                <b class="tooltip tooltip-bottom-right">ระบุชื่อบริษัท</b>
                                            </label>
                                        </section>
                                    </div>

                                    <div class="row">
                                        <section class="col col-8" >

                                            <lable style="font-size:18px">ที่อยู่บริษัท</lable>
                                            <label class="input">
                                               <textarea style="font-size:18px" class="form-control" rows="6" id="company_addr" name="company_addr"
                                                placeholder=" ระบุที่อยู่บริษัท   "></textarea>
                                               <b class="tooltip tooltip-bottom-right">ระบุที่อยู่บริษัท</b> 
                                            </label>
                                        </section>
                                    </div>
                                    
                                    <div class="row">    
                                        <section class="col col-6" >
                                            <lable style="font-size:18px">รหัสไปรษณีย์</lable>
                                            <label class="input">
                                                <input type="text" id="zipcode" name="zipcode" 
                                                 onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"
                                                 placeholder="ระบุ รหัสไปรษณีย์">
                                                <b class="tooltip tooltip-bottom-right">ระบุ รหัสไปรษณีย์</b>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="row">    
                                        <section class="col col-10" >
                                            <lable style="font-size:18px">URL</lable>
                                            <label class="input">
                                                <input type="text" id="equity_url" name="equity_url" placeholder="ระบุ URL">
                                                <b class="tooltip tooltip-bottom-right">ระบุ URL</b>
                                            </label>
                                        </section>
                                    </div>

                            </fieldset>

                            <fieldset>

                                <div class="row">

                                    <section class="col col-6">
                                        <lable style="font-size:18px"> เบอร์โทรศัพท์(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                        <label class="input"></i>
                                            <input type="text" name="company_phone"  id="company_phone" placeholder="ระบุเบอร์โทรศัพท์" >
                                            <b class="tooltip tooltip-bottom-right">ระบุเบอร์โทรศัพท์</b> 
                                        </label>
                                    </section>

                                    <section class="col col-6">
                                        <lable style="font-size:18px">เบอร์โทรสาร(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                        <label class="input"> </i>
                                            <input type="text" name="company_fax" id="company_fax" placeholder="ระบุเบอร์โทรสาร" >
                                            <b class="tooltip tooltip-bottom-right">ระบุเบอร์โทรสาร</b> 
                                        </label>
                                    </section>
                                </div>

                                


                            </fieldset>


                            <fieldset>

                                <div class="row">
                                    <section class="col col-6">
                                        <lable style="font-size:18px">วันที่เริ่มต้น</lable>
                                        <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="equity_start"  class="mea_date_picker" id="equity_start" placeholder="ระบุวันที่เริ่มต้น" >
                                            <b class="tooltip tooltip-bottom-right">ระบุวันที่เริ่มต้น</b> 
                                        </label>
                                    </section>

                                    <section class="col col-6">
                                        <lable style="font-size:18px">วันที่สิ้นสุด</lable>
                                        <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="equity_end"  class="mea_date_picker" id="equity_end" placeholder="ระบุวันที่สิ้นสุด" >

                                           <b class="tooltip tooltip-bottom-right">ระบุวันที่สิ้นสุด</b> 
                                        </label>
                                    </section>
                                </div>


                            </fieldset>
                            <footer>
                                <div class="row">
                                   <lable style="font-size:18px; color:red;">&nbsp;&nbsp;&nbsp;&nbsp;* Required field ​(ห้ามเป็นค่าว่าง)</lable>   
                                </div>

                                <button type="button"  id="btn_form" class="btn btn-primary">
                                    ยืนยัน
                                </button>
                                <button type="button" class="btn btn-default" onclick="window.history.back();">
                                    ยกเลิก
                                </button>
                                
                            </footer>
                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->

    </div>


    <!-- PAGE RELATED PLUGIN(S) -->
    <!--script src="{{asset('backend/js/plugin/jquery-form/jquery-form.min.js')}}"></script-->
    <script src="{{asset('backend/js/plugin/summernote/summernote.min.js')}}"></script>

    <script type="text/javascript">

        $(document).ready(function() {

            $.validator.addMethod("valueNotEquals", function(value, element, arg) {
                return arg != value;
            }, "Please Choose one");

            

            $("#btn_form").on('click',function(){

               $("#smart-form-register").validate({

                    // Rules for form validation
                    rules : {
                        
                        name_sht : {
                            required : true
                        },

                        industrial : {
                            required : true
                        },

                        //bu : {
                        //    required : true
                        //},

                        market : {
                            required : true
                        },

                        setindex : {
                            required : true
                        },

                        new_symbol : {
                            required: false
                        },

                        zipcode : {
                            required: false
                        },

                        equity_url : {
                            required: false
                        },

                        effective_date: {
                            required: false 
                        },

                        company_name : {
                            required : true
                        },

                        company_addr : {
                            required : false
                        },

                        company_phone : {
                            required : true,
                            maxlength : 40
                            
                        },
                        
                        company_fax : {
                            required : true,
                            maxlength : 40
                              
                        },
/*
                        equity_start : {
                            required : false
                        },

                        equity_end : {
                            required : false
                        }*/
                    },

                    errorPlacement : function(error, element) {
                        error.insertAfter(element.parent());

                    }
                });

                function validatePhone(t) {
                    var a = t; //document.getElementById(txtPhone).value;
                    // var filter = /((\+[0-9]{1,1}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
                    var filter = /^[\*]?[0-9, -]+$/;
                    if (filter.test(a)) {
                        return true;
                    }
                    else {
                       return false;
                    }
                } 

                // if($registerForm.valid()){
                if($("#smart-form-register").valid()){

                    var record_id      = $('#record_id').val();
                    var market         = $('#market').val();
                    var setindex       = $('#setindex').val();
                    var industrial_cate_id        = $("#industrial").val(); //industrial     = $("#industrial").val();  // cate_id
                    //var bu             = $("#bu").val();
                    var name_sht       = $("#name_sht").val();
                    var new_symbol     = $("#new_symbol").val();
                    var effective_date = $("#effective_date").val();
                    
                    var company_name  = $("#company_name").val();
                    var company_addr  = $("#company_addr").val();
                    var company_phone = $("#company_phone").val();
                    var company_fax   = $("#company_fax").val();

                     
                    var zipcode       = $("#zipcode").val();
                    var equity_url    = $("#equity_url").val();


                    // var equity_start = $('#equity_start').datepicker({ dateFormat: 'dd-mm-yy' }).val();
                    // var equity_start = $('#equity_end').datepicker({ dateFormat: 'dd-mm-yy' }).val();
                    //  var md = Date.parseDate(date:$("#equity_start").val(), pattern:"dd-mm-yyyy", loc:'th-th'));
                    // Alert("ERROR", md , null, null);
                    // return false;
                    
                    var equity_start  = $("#equity_start").val();
                    var equity_end    = $("#equity_end").val();
                    

                    if(!validatePhone(company_phone)) {
                        Alert("มีข้อผิดพลาด", "เบอร์ โทรศัพท์ไม่ถูกต้อง", null, null);
                        return false;
                    }
                    if(!validatePhone(company_fax)) {
                        Alert("มีข้อผิดพลาด", "เบอร์ โทรสารไม่ถูกต้อง", null, null);
                        return false;
                    }

                    
                    //var user_id       = get_userID();
                    //var plan_status  = $('input[name=plan_status]:checked').val();

                    var jsondata = {
                        record_id:     record_id,
                        market:        market,
                        setindex:      setindex,
                        industrial_cate_id:       industrial_cate_id,  
                        name_sht:      name_sht,
                       // bu:            bu,
                        new_symbol:    new_symbol,
                        effective_date: effective_date,

                        zipcode: zipcode,
                        equity_url: equity_url,



                        company_name:  company_name,
                        company_addr:  company_addr,

                        company_phone: company_phone,
                        company_fax:   company_fax,

                        equity_start:  equity_start,
                        equity_end:    equity_end
                    };

                    console.log(jsondata);
                    
                    MeaAjax(jsondata, "addEquityIndex", function(mresponse) {
                        if(mresponse.success){
                            AlertSuccess("บันทึกจัดการบริษ้ทเรียบร้อยแล้ว",function(){
                                window.location.href = "/admin/EquityCompany/equityindex";
                            });

                        } else {
                            Alert("ERROR", mresponse.html, null, null);
                        }
                    });
                    
                    return false;
                }
                return false;
            });

            //
            //  meaDatepicker("plan_start","plan_end");
            //  meaDatepicker("plan_end");
            //
            
            meaDatepicker("equity_start", "equity_end");
            meaDatepicker("equity_end");
            meaDatepicker("effective_date");


            // events handler
            $('#check_change_name').on( "click", function() {
                //if($('#check_change_name').is(':checked')) 
                //alert('ddsfdsdf');
                
                if($('#check_change_name').is(':checked'))
                  $("#change_name").show();
                else
                  $("#change_name").hide();  
                  //"display:inline-block;" : "display:none;";
            });
            //$( "input[type=checkbox]" ).on( "click", countChecked );
        });

    </script>

@stop





