@extends('backend.layouts.default')
@section('content')
<?php
/**
 * Menu items
 */
$data = getmemulist();
/**
 * Menu Sidebar
 */
$arrSidebar =getSideBar($data);
?>

<!-- MAIN CONTENT -->
<div id="content">

    <div class="widget-body fuelux">
         <div class="wizard" >
            <ul class="steps form-wizard">
                
                <li data-target="#step1" style="font-size:18px" >
                    <a href="{{action('BondCompanyManagementController@getindex')}}"  class="badge badge-info">1</a>
                    <a href="{{action('BondCompanyManagementController@getindex')}}">บริษัทจัดการ</a>
                    <span class="chevron"></span>
                </li>
                <li data-target="#step2" style="font-size:18px">
                    <a href="{{action('BondCompanyManagementController@getindexCategory')}}"  class="badge">2</a>
                    <a href="{{action('BondCompanyManagementController@getindexCategory')}}">หมวดหมู่ตราสารหนี้</a>
                    <span class="chevron"></span>
                </li>
                <li data-target="#step3" style="font-size:18px" class="active">
                    <a href="{{action('BondCompanyManagementController@getindexBondIndex')}}"  class="badge">3</a>
                    <a href="{{action('BondCompanyManagementController@getindexBondIndex')}}">ข้อมูลตราสารหนี้</a>
                    <span class="chevron"></span>
                </li>
                <li data-target="#step4" style="font-size:18px">
                    <a href="{{action('BondCompanyManagementController@getindexBroker')}}" class="badge">4</a>
                    <a href="{{action('BondCompanyManagementController@getindexBroker')}}">บริษัท Broker</a>
                    <span class="chevron"></span>
                </li>
            </ul>
        </div>
    </div>
    <br/>
 
    <!-- BEGINE: search from -->
    <div id="search_form" name="search_form" style="width: 100%; padding: 10px;">
                                
    </div>
    <!-- END: search form -->

    <div class="row">
        
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">  
			ข้อมูลตราสารหนี้
                {{--getMenutitle($arrSidebar)--}} 
            </h1>
            
        </div>
         
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            <ul id="sparks" class="">
                 <li class="sparks-info">
                    <a href="{{action('BondCompanyManagementController@getimportBondIndex')}}" class="btn bg-color-orange txt-color-white"><i class="fa fa-download"></i> นำเข้า</a>
                </li>

                <li class="sparks-info">
                    <a href="{{action('BondCompanyManagementController@getAddBondIndex')}}" class="btn bg-color-green txt-color-white"><i class="fa fa-plus"></i> เพิ่ม</a>
                </li>
                <li class="sparks-info">
                    <a href="javascript:void(0);" id="mea_edit"  class="btn bg-color-blueDark txt-color-white"><i class="fa fa-gear fa-lg"></i> แก้ไข</a>
                </li>
                <li class="sparks-info">
                    <a href="javascript:void(0);" id="mea_delete" class="btn bg-color-red txt-color-white"><i class="glyphicon glyphicon-trash"></i> ลบ</a>
                </li>

            </ul>
        </div>
    </div>

    

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false"
                     data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false">
                    <!-- widget options:
						usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

						data-widget-colorbutton="false"
						data-widget-editbutton="false"
						data-widget-togglebutton="false"
						data-widget-deletebutton="false"
						data-widget-fullscreenbutton="false"
						data-widget-custombutton="false"
						data-widget-collapsed="true"
						data-widget-sortable="false"

						-->
                    <header>


                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                                
                        </div>
                        <!-- end widget edit box -->
                        

                        <!-- widget content -->
                        <div class="widget-body no-padding">

                            <div class="table-responsive">
                                <div class="result" style="width: 100%; padding: 10px;">
                                
                                </div>
                            </div>

                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- WIDGET END -->

        </div>

        <!-- end row -->

        <!-- end row -->

    </section>
    <!-- end widget grid -->


</div>
<!-- END MAIN CONTENT -->


<!-- PAGE RELATED PLUGIN(S) -->


<script src="{{asset('backend/js/plugin/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/datatables/dataTables.colVis.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/datatables/dataTables.tableTools.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>


<script>
  $( function() {
    $.widget( "custom.combobox", {
      _create: function() {
        this.wrapper = $( "<span>" )
          .addClass( "custom-combobox" )
          .insertAfter( this.element );
 
        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
      },
 
      _createAutocomplete: function() {
        var selected = this.element.children( ":selected" ),
          value = selected.val() ? selected.text() : "";
 
        this.input = $( "<input>" )
          .appendTo( this.wrapper )
          .val( value )
          .attr( "title", "" )
          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
          .autocomplete({
            delay: 0,
            minLength: 0,
            source: $.proxy( this, "_source" )
          })
          .tooltip({
            classes: {
              "ui-tooltip": "ui-state-highlight"
            }
          });
 
        this._on( this.input, {
          autocompleteselect: function( event, ui ) {
            ui.item.option.selected = true;
            this._trigger( "select", event, {
              item: ui.item.option
            });
          },
 
          autocompletechange: "_removeIfInvalid"
        });
      },
 
      _createShowAllButton: function() {
        var input = this.input,
          wasOpen = false;
 
        $( "<a>" )
          .attr( "tabIndex", -1 )
          .attr( "title", "Show All Items" )
          .tooltip()
          .appendTo( this.wrapper )
          .button({
            icons: {
              primary: "ui-icon-triangle-1-s"
            },
            text: false
          })
          .removeClass( "ui-corner-all" )
          .addClass( "custom-combobox-toggle ui-corner-right" )
          .on( "mousedown", function() {
            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
          })
          .on( "click", function() {
            input.trigger( "focus" );
 
            // Close if already visible
            if ( wasOpen ) {
              return;
            }
 
            // Pass empty string as value to search for, displaying all results
            input.autocomplete( "search", "" );
          });
      },
 
      _source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        response( this.element.children( "option" ).map(function() {
          var text = $( this ).text();
          if ( this.value && ( !request.term || matcher.test(text) ) )
            return {
              label: text,
              value: text,
              option: this
            };
        }) );
      },
 
      _removeIfInvalid: function( event, ui ) {
 
        // Selected an item, nothing to do
        if ( ui.item ) {
          return;
        }
 
        // Search for a match (case-insensitive)
        var value = this.input.val(),
          valueLowerCase = value.toLowerCase(),
          valid = false;
        this.element.children( "option" ).each(function() {
          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
            this.selected = valid = true;
            return false;
          }
        });
 
        // Found a match, nothing to do
        if ( valid ) {
          return;
        }
 
        // Remove invalid value
        this.input
          .val( "" )
          .attr( "title", value + " didn't match any item" )
          .tooltip( "open" );
        this.element.val( "" );
        this._delay(function() {
          this.input.tooltip( "close" ).attr( "title", "" );
        }, 2500 );
        this.input.autocomplete( "instance" ).term = "";
      },
 
      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      }
    });
 
    $( "#combobox" ).combobox();
    $( "#toggle" ).on( "click", function() {
      $( "#combobox" ).toggle();
    });
  } );
  </script>

<script type="text/javascript">

    function RenderForm(data) {
        $("#search_form").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
        $("#search_form").html(data.html);
        $("#search_form").fadeIn('300');

        $('#btn_search').on('click',function() {
            // var parameters = {pagesize : 20, PageNumber:1};
            var method = "getallBondIndex";

             var symbol  = $("#symbol").val();  
     

            var jsondata = {pagesize : 20, PageNumber:1 , symbol: symbol};

            MeaAjax(jsondata, method, Render);          
        }); 
    }

    function Render(data){
        $(".result").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');

        $(".result").html(data.html);
        $(".result").fadeIn('300');

        $("#mainCheck").on("click",function(){$(".item_checked").not(this).prop('checked', this.checked);});

        $(".mea_delete_by").on('click',function(){

            var id = $(this).attr("data-id");
            $.SmartMessageBox({
                title : "คำเตือน",
                content : "ท่านแน่ใจที่ต้องการจะลบ รายการที่ท่านเลือก",
                buttons : '[ยกเลิก][OK]'

            }, function(ButtonPressed) {
                if (ButtonPressed === "OK") {

                    //var plan_id = $("#plan_id").val();
                    //var jsondata = {group_id : id, plan_id : plan_id};
                    
                    var jsondata = {group_id : id};
 
                    $.ajax({

                        type: 'post', 
                        dataType: 'json',
                        url: '/admin/BondCompany/deleteBondIndex',
                        data: jsondata,

                        success: function(data) {

                            if(data.ret == "1"){
                                $.smallBox({
                                    title: "Congratulations! Your form was submitted",
                                    content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                                    color: "#5F895F",
                                    iconSmall: "fa fa-check bounce animated",
                                    timeout: 4000
                                });

                                window.location.href = '/admin/BondCompany';
                            }

                        },

                        error: function(xhr, textStatus, thrownError) {
//                                alert(xhr.status);
//                                alert(thrownError);
//                                alert(textStatus);
                        }
                    });
                }
                if (ButtonPressed === "ยกเลิก") {

                }

            });

        });

        $("#mea_delete").on('click',function(){

            var checkcount = $(".item_checked:checked").length;

            if(checkcount > 0){
                var checked = "";
                $(".item_checked").each(function(){

                    if($(this).is(":checked")){
                        checked = checked + $(this).val() + ",";
                    }

                });
                var jsondata = {group_id : checked};

                $.ajax({

                    type: 'post', 
                    dataType: 'json',
                    url: '/admin/BondCompany/deleteBondIndex',
                    data: jsondata,

                    success: function(data) {

                        if(data.ret == "1"){
                            $.smallBox({
                                title: "Congratulations! Your form was submitted",
                                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                                color: "#5F895F",
                                iconSmall: "fa fa-check bounce animated",
                                timeout: 4000
                            });

                            window.location.href = '/admin/BondCompany';
                        }



                    },
                    error: function(xhr, textStatus, thrownError) {
//                                alert(xhr.status);
//                                alert(thrownError);
//                                alert(textStatus);
                    }
                });
            }else {
                $.SmartMessageBox({
                    title : "Error!",
                    content : "ท่านยังไม่ได้เลือกรายการ",
                    buttons : '[OK]'
                }, function(ButtonPressed) {
                    if (ButtonPressed === "OK") {


                    }
                    if (ButtonPressed === "No") {

                    }

                });
            }


        });

        $("#mea_edit").on('click',function(){

            var checkcount = $(".item_checked:checked").length;

            if(checkcount > 1 || checkcount  == 0){
                $.SmartMessageBox({
                    title : "Error!",
                    content : "ไม่สามารถแก้ไขได้ กรุณาเลือกรายการเพียงรายการเดียว",
                    buttons : '[OK]'
                }, function(ButtonPressed) {
                    if (ButtonPressed === "OK") {

                         return false;
                    }
                    if (ButtonPressed === "No") {

                    }

                });
            }else {
                window.location.href = "/admin/BondCompany/editBondIndex/" + $(".item_checked:checked").val();
            }
        });

       /* $('#btn_search').on('click',function() {
            var parameters = {pagesize : 20, PageNumber:1};
            var method = "getallBondIndex";

            MeaAjax(parameters, method, Render);
          
        });*/ 
         //page click
        $("#page_click_search li a").on('click', PageRender);
    }
    
 
    function PageRender(){
        var p = $(this).attr('data-page');
        var page_size = 20;
        var CurPage = $('#currentpage_search').val();
        $("#all_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');

        if(p == "pre"){
            p = parseInt(CurPage) - 1;
        }

        if(p == "next"){
            p = parseInt(CurPage) + 1;
        }

        $('#currentpage_search').val(p);
        var symbol  = $("#symbol").val();  
     

        var jsondata = {pagesize : page_size, PageNumber:p , symbol: symbol};

        MeaAjax(jsondata,"getallBondIndex",Render);

        
 
    };


    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    //$.noConflict();
    
 
    $(document).ready(function() {

        var parameters = {pagesize : 20, PageNumber:1};
        var method = "getallBondIndex";
        MeaAjax(parameters, method, Render);


        method = "getBondIndexSearchForm";
        MeaAjax(parameters, method, RenderForm);

       
    })

</script>

@stop