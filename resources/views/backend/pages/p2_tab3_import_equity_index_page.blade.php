<!--
FILE: p2_tab4_import_equity_index_page.blade.php
Purpose: Import XLS into database
-->

@extends('backend.layouts.default')
@section('content')
<?php
$data = getmemulist();
$arrSidebar =getSideBar($data);
?>

    <style type="text/css">
        #sort-by-year{
            /*position:absolute;*/
            width: 100%;
            margin-bottom: 10px;
            /*z-index: 999;*/
            /*left: 200px;*/
            /*top:5px*/
        }
        #sort-by-year select{
            width: 300px;

        }
        #datatable_fixed_column tbody tr td a i{
            font-size: 11px !important;
            line-height: 12px!important;
        }
        #datatable_fixed_column tbody tr td a{
            line-height: 12px!important;

        }
    </style>

<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>

                {{getMenutitle($arrSidebar)}}
            </h1>
        </div>
    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="well well-sm">
                <!-- Timeline Content -->
                <div class="smart-timeline">
                    <ul class="smart-timeline-list">
                        <li>
                            <div class="smart-timeline-icon bg-color-greenDark">
                                <i class="fa fa-file-text"></i>
                            </div>
                            <div class="smart-timeline-time">
                                <small>รูปแบบ</small>
                            </div>
                            <div class="smart-timeline-content">
                                <p>
                                    <a href="javascript:void(0);"><strong>เพื่อทําการนำเข้าข้อมูล หลักทรัพย์  แบบ Multiple record</strong></a>
                                </p>
                                <p>ชนิดไฟล์ที่อนุญาติให้นำเข้า:Exel (.xls)</p>
                                <a class="btn btn-xs btn-success" href="{{action('EquityCompanyManagementController@dowloadsampleEquityIndex')}}"> ดูตัวอย่างไฟล์</a>
                                <p></p>
                                <table  class="table table-bordered">
                                    <tr><th style="width:10%;">SYMBOL</th><th data-class="expand">COMP_NAME</th><th>MARKET</th><th>SET_INDEX50</th><th>SET_INDEX100</th><th>...</th><th data-class="expand">URL</th></tr>
                                    <tr><td style="width:10%;">AMATA</td><td data-class="expand">บริษัท อมตะ คอร์ปอเรชัน จำกัด (มหาชน)</td><td>SET</td><td>Y</td><td>Y</td><td>...</td><td>http://www.amata.com</td></tr>
                                </table>

								 <table  class="table table-bordered">
                                    <tr>
										<th style="width:10%;">ชื่อฟิลด์</th>
										<th style="width:20%;">คำอธิบาย</th>
										<th style="width:20%;">หมายเหตุ</th>
									</tr>
										<tr>
											<td style="width:10%;">INDEX_ID</td>
											<td style="width:20%;">ลำดับที่ของหุ้น หรือหลักทรัพย์ในระบบ (ระบบจะทำการสร้างอัตโนมัติ)</td>
											<td style="width:20%;"></td>
										</tr>
										<tr>
											<td style="width:10%;">SYMBOL</td>
											<td style="width:20%;">ชื่อหุ้น หรือชื่อหลักทรัพย์</td>
											<td style="width:20%;">***</td>
										</tr>
										<tr>
											<td style="width:10%;">SYMBOL_RENAME_FLAG</td>
											<td style="width:20%;">Flag กรณีที่มีการเปลี่ยนชื่อหุ้น หรือชื่อหลักทรัพย์</td>
											<td style="width:20%;"></td>
										</tr>
										<tr>
											<td style="width:10%;">OLD_SYMBOL</td>
											<td style="width:20%;">ชื่อเดิมของหุ้น หรือหลักทรัพย์ ก่อนจะมีการเปลี่ยนชื่อ</td>
											<td style="width:20%;"></td>
										</tr>
										<tr>
											<td style="width:10%;">MODIFY_DATE</td>
											<td style="width:20%;">วันที่ทำรายการเปลี่ยนชื่อหุ้น หรือหลักทรัพย์</td>
											<td style="width:20%;"></td>
										</tr>
										<tr>
											<td style="width:10%;">MODIFY_BY</td>
											<td style="width:20%;">ผู้ที่ทำรายการเปลี่ยนชื่อหุ้น หรือหลักทรัพย์</td>
											<td style="width:20%;"></td>
										</tr>
										<tr>
											<td style="width:10%;">EFFECTIVE_DATE</td>
											<td style="width:20%;">วันที่มีผลของการเปลี่ยนชื่อหุ้น หรือหลักทรัพย์</td>
											<td style="width:20%;"></td>
										</tr>
										<tr>
											<td style="width:10%;">COMP_NAME</td>
											<td style="width:20%;">ชื่อบริษัทหลักทรัพย์</td>
											<td style="width:20%;">***</td>
										</tr>
										<tr>
											<td style="width:10%;">MARKET</td>
											<td style="width:20%;">ชื่อตลาดของหลักทรัพย์ เช่น SET, mai</td>
											<td style="width:20%;">***</td>
										</tr>
										<tr>
											<td style="width:10%;">SET_INDEX50</td>
											<td style="width:20%;">Flag เพื่อบอกว่าหลักทรัพย์อยู่ในกลุ่ม SET50 หรือไม่ หากอยู่ในกลุ่มนี้ให้ใส่ค่า Y</td>
											<td style="width:20%;"></td>
										</tr>
										<tr>
											<td style="width:10%;">SET_INDEX100</td>
											<td style="width:20%;">Flag เพื่อบอกว่าหลักทรัพย์อยู่ในกลุ่ม SET100 หรือไม่ หากอยู่ในกลุ่มนี้ให้ใส่ค่า Y</td>
											<td style="width:20%;"></td>
										</tr>
										<tr>
											<td style="width:10%;">SET_HD</td>
											<td style="width:20%;">Flag เพื่อบอกว่าหลักทรัพย์อยู่ในกลุ่ม SET HD หรือไม่ หากอยู่ในกลุ่มนี้ให้ใส่ค่า Y</td>
											<td style="width:20%;"></td>
										</tr>
										<tr>
											<td style="width:10%;">INDUSTRIAL</td>
											<td style="width:20%;">ชื่อกลุ่มอุตสาหกรรมที่หลักทรัพย์อยู่</td>
											<td style="width:20%;">***</td>
										</tr>
										<tr>
											<td style="width:10%;">BU</td>
											<td style="width:20%;">ชื่อหมวดธุรกิจที่หลักทรัพย์อยู่</td>
											<td style="width:20%;">***</td>
										</tr>
										<tr>
											<td style="width:10%;">ADDRESS</td>
											<td style="width:20%;">ที่อยู่ของบริษัทหลักทรัพย์</td>
											<td style="width:20%;"></td>
										</tr>
										<tr>
											<td style="width:10%;">ZIP_CODE</td>
											<td style="width:20%;">รหัสไปรษณีย์ของบริษัทหลักทรัพย์</td>
											<td style="width:20%;"></td>
										</tr>
										<tr>
											<td style="width:10%;">PHONE</td>
											<td style="width:20%;">หมายเลขโทรศัพย์ของบริษัทหลักทรัพย์</td>
											<td style="width:20%;"></td>
										</tr>
										<tr>
											<td style="width:10%;">FAX</td>
											<td style="width:20%;">หมายเลขโทรสารของบริษัทหลักทรัพย์</td>
											<td style="width:20%;"></td>
										</tr>
										<tr>
											<td style="width:10%;">URL</td>
											<td style="width:20%;">เว็บไซต์ของบริษัทหลักทรัพย์</td>
											<td style="width:20%;"></td>
										</tr>
										<tr>
											<td style="width:10%;">START_DATE</td>
											<td style="width:20%;">วันที่เริ่มต้นของหลักทรัพย์</td>
											<td style="width:20%;"></td>
										</tr>
										<tr>
											<td style="width:10%;">END_DATE</td>
											<td style="width:20%;">วันที่สิ้นสุดของหลักทรัพย์</td>
											<td style="width:20%;"></td>
										</tr>
										<tr>
											<td style="width:10%;">CREATE_DATE</td>
											<td style="width:20%;">วันที่นำข้อมูลหลักทรัพย์เข้าระบบ</td>
											<td style="width:20%;"></td>
										</tr>
										<tr>
											<td style="width:10%;">CREATE_BY</td>
											<td style="width:20%;">ผู้ที่นำข้อมูลหลักทรัพย์เข้าระบบ</td>
											<td style="width:20%;"></td>
										</tr>

                                </table>

								<p><span style="color: red">***</span>หมายถึง ต้องมีข้อมูลในคอลัมภ์นั้น
                                </p>

								<p>
                                    <!--p class="help-block">
                                        เลือกไฟล์<br/>
                                        <span style="color: red">จำนวน record ที่สามารถ import ได้สูงสุดต่อครั้งคือ <strong>10,000 record เท่านั้น!!!</strong></span>
                                    </p-->


                                   
                                    <!--div style="display: block; width: 120px; height: 40px; overflow: hidden;">
                                        <button style="width: 110px; height: 30px; position: relative; top: 0px; left: 0px;"><a href="javascript: void(0)">Browse File...</a>
                                            
                                        </button>
                                        <input type="file" id="import1" name="import1" style="font-size: 30px; width: 120px; opacity: 0; filter:alpha(opacity=0);  position: relative; top: -30px;; left: -20px" />
                                        
                                    </div-->
                                    
                                          <div class="form-group">
                                                <p class="help-block">
                                                <span style="color: red">จำนวน record ที่สามารถ import ได้สูงสุดต่อครั้งคือ <strong>10,000 records เท่านั้น!!!</strong></span>
                                                    <br/>เลือกไฟล์ ที่ต้องการนำเข้า
                                                    
                                                </p>
                                                <input type="file" class="filestyle" data-buttonBefore="true" data-buttonName="btn-primary" 
                                                     data-buttonText="เลือกไฟล์" id="import1" name="import1">
                                          </div>

                                    

                                    <!--p class="help-block" id="file_attached" name="file_attached">[ Not Selected ]</p-->

                                </p>
                                <p>
                                     <span style="font-size: 16px;color: #3276b1; font-style: italic;">
                                        * เพื่อทําการนำเข้าข้อมูล หลักทรัพย์จำนวนมากกว่า 1 record ขึ้นไป 
                                     </span>   
                                     
                                </p>

                                <p id="progress_import1" style="display: none;"><img src="{{asset('backend/img/shot.gif')}}"  /> กำลังนำเข้าข้อมูล</p>
                                <p id="check_ret1" style="display: none"></p>
                                <p>
                                    <a id="btnCancel1" href="javascript:void(0);" onclick="window.history.back();" class="btn btn-xs bg-color-blueDark txt-color-white"><i class="fa fa-back"></i>   ยกเลิก  </a>
                                    <a id="btnImport1" href="javascript:void(0);"  data-input="import1" data-import="1" class="btn_import btn btn-xs btn-primary"><i class="fa fa-download"></i> ยืนยัน</a>
                                 </p>
                                <p>
                                
                                    
                                </p>

                                <div class="row">

                                </div>
                            </div>
                        </li>
                        <!--li>
                            <div class="smart-timeline-icon bg-color-greenDark">
                                <i class="fa fa-file-text"></i>
                            </div>
                            <div class="smart-timeline-time">
                                <small>รูปแบบที่ 2</small>
                            </div>
                            <div class="smart-timeline-content">
                                <p>
                                    <a href="javascript:void(0);"><strong>เพื่อทําการ update status, ค่า flag ที่เกี่ยวข้อง กรณีท่ีมีสมาชิกลาออกจากกองทุน</strong></a>
                                </p>
                                <p>ชนิดไฟล์ที่อนุญาติให้นำเข้า:Exel </p>
                                <a class="btn btn-xs btn-success" href="{{ action('UserController@dowloadsample2')}}"> ดูตัวอย่างไฟล์</a>
                                <p></p>
                                <table style="width: 100px;" class="table table-bordered">
                                    <tr><td>EMP_ID</td><td>USER_STATUS_ID</td><td>LEAVE_FUND_GROUP_DATE</td><td>LEAVE_FUND_FLAG</td></tr>
                                    <tr><td>0000001</td><td>11</td>
                                    <td>2015-01-01</td>
                                    <td>1</td></tr>
                                </table>
<p>EMP_ID = รหัสพนักงาน
</p>
                                <p>USER_STATUS_ID = สถานะของสมาชิก
                                </p>
                                <p>LEAVE_FUND_GROUP_DATE = วันที่สมาชิกลาออกจากกองทุน (รูปแบบ yyyy-mm-dd)
                                </p>
                                <p>LEAVE_FUND_FLAG = ครั้งที่สมาชิกลาออกจากกองทุน
                                </p>
                                <div>
                                    <table style="width: 600px;" class="table table-bordered">
                                        <tr><td>USER_STATUS_ID</td><td>คำอธิบาย</td></tr>
                                        <tr><td>01</td><td>พ้นสภาพเนื่องจากลาออกจาก กฟน และกองทุน</td></tr>
                                        <tr><td>04</td><td>พ้นสภาพเนื่องจากลาออกจากกองทุนครั้งที่ 1</td></tr>
                                        <tr><td>05</td><td>สมาชิกแบบคงเงิน</td></tr>
                                        <tr><td>06</td><td>สมาชิกแบบรับเงินเป็นงวด</td></tr>
                                        <tr><td>11</td><td>สมาชิกปัจจุบัน</td></tr>
                                        <tr><td>12</td><td>สมาชิกปัจจุบัน (Re-Entry)</td></tr>
                                        <tr><td>13</td><td>สมาชิกใหม่</td></tr>
                                        <tr><td>14</td><td>Unknown</td></tr>
                                        <tr><td>15</td><td>พ้นสภาพสมาชิก (ลาออกจากกองทุนครบ 2 ครั้ง)</td></tr>


                                    </table>
                                </div>

                                <p>
                                <p class="help-block">
                                    เลือกไฟล์
                                </p>
                                <input type="file" class="btn btn-default" id="import2" name="import2">


                                </p>
								<p id="progress_import2" style="display: none;"><img src="{{asset('backend/img/shot.gif')}}"  /> กำลังนำเข้าข้อมูล</p>
                                <p id="check_ret2" style="display: none"></p>
                               
                                <p>
                                    <a href="javascript:void(0);" data-input="import2" data-import="2" class="btn_import btn btn-xs btn-primary"><i class="fa fa-download"></i> นำเข้าข้อมูล</a>
                                </p>

                                <div class="row">

                                </div>

                            </div>
                        </li>
                        <li>
                            <div class="smart-timeline-icon bg-color-greenDark">
                                <i class="fa fa-file-text"></i>
                            </div>
                            <div class="smart-timeline-time">
                                <small>รูปแบบที่ 3</small>
                            </div>
                            <div class="smart-timeline-content">
                                <p>
                                    <a href="javascript:void(0);"><strong>เพื่อทําการ update status, ค่า flag ที่เกี่ยวข้อง กรณีที่มีสมาชิกกลับเข้ากองทุน</strong></a>
                                </p>
                                <p>ชนิดไฟล์ที่อนุญาติให้นำเข้า:Exel</p>
                                <a class="btn btn-xs btn-success" href="{{ action('UserController@dowloadsample3')}}"> ดูตัวอย่างไฟล์</a>
                                <p></p>
                                <table style="width: 100px;" class="table table-bordered">
                                    <tr><td>EMP_ID</td><td>USER_STATUS_ID</td><td>RETURN_FUND_GROUP_DATE</td></tr>
                                    <tr><td>0000001</td><td>12</td><td>2016-01-01</td></tr>
                                </table>
<p>EMP_ID = รหัสพนักงาน
</p>
                                <p>USER_STATUS_ID = สถานะของสมาชิก
                                </p>
                                <p>RETURN_FUND_GROUP_DATE = วันที่สมาชิกกลับเข้ากองทุน (รูปแบบ yyyy-mm-dd)
                                </p>
                                <div>
                                    <table style="width: 600px;" class="table table-bordered">
                                        <tr><td>USER_STATUS_ID</td><td>คำอธิบาย</td></tr>
                                        <tr><td>01</td><td>พ้นสภาพเนื่องจากลาออกจาก กฟน และกองทุน</td></tr>
                                        <tr><td>04</td><td>พ้นสภาพเนื่องจากลาออกจากกองทุนครั้งที่ 1</td></tr>
                                        <tr><td>05</td><td>สมาชิกแบบคงเงิน</td></tr>
                                        <tr><td>06</td><td>สมาชิกแบบรับเงินเป็นงวด</td></tr>
                                        <tr><td>11</td><td>สมาชิกปัจจุบัน</td></tr>
                                        <tr><td>12</td><td>สมาชิกปัจจุบัน (Re-Entry)</td></tr>
                                        <tr><td>13</td><td>สมาชิกใหม่</td></tr>
                                        <tr><td>14</td><td>Unknown</td></tr>
                                        <tr><td>15</td><td>พ้นสภาพสมาชิก (ลาออกจากกองทุนครบ 2 ครั้ง)</td></tr>


                                    </table>
                                </div>

                                <p>
                                <p class="help-block">
                                    เลือกไฟล์
                                </p>
                                <input type="file" class="btn btn-default" id="import3" name="import3">


                                </p>
							    <p id="progress_import3" style="display: none;"><img src="{{asset('backend/img/shot.gif')}}"  /> กำลังนำเข้าข้อมูล</p>
                                <p id="check_ret3" style="display: none"></p>
                               
                                <p>
                                    <a href="javascript:void(0);" data-input="import3" data-import="3" class="btn_import btn btn-xs btn-primary"><i class="fa fa-download"></i> นำเข้าข้อมูล</a>
                                </p>

                                <div class="row">

                                </div>
                            </div>
                        </li-->


                    </ul>
                </div>
                <!-- END Timeline Content -->

            </div>

        </div>

    </div>

</div>
<!-- END MAIN CONTENT -->


<!-- PAGE RELATED PLUGIN(S) -->
{{--<script src="{{asset('backend/js/plugin/datatables/jquery.dataTables.min.js')}}"></script>--}}
{{--<script src="{{asset('backend/js/plugin/datatables/dataTables.colVis.min.js')}}"></script>--}}
{{--<script src="{{asset('backend/js/plugin/datatables/dataTables.tableTools.min.js')}}"></script>--}}
{{--<script src="{{asset('backend/js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>--}}
{{--<script src="{{asset('backend/js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>--}}

<script src="{{asset('backend/js/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script> 
<script type="text/javascript">


    $(document).ready(function() {


        if (navigator.userAgent.indexOf("MSIE") > 0) {
            $("#import1").mousedown(function() {
                $(this).trigger('click');
            })
        } else if($.browser.mozilla) {
           
        }

       /* $('input[type=file]').change(function(e){
            $in=$(this);
            // alert($in.val());
            $('#file_attached').text($in.val());
        });*/

        $('.btn_import').on('click',function(){

            var dataimport = new FormData();
            var target = $(this).attr('data-input');

            var files = $("#" + target).get(0).files;

            var importType= $(this).attr('data-import');


            if (files.length > 0) {
                dataimport.append("exelimport", files[0]);


            }
            
            /// ::LAM-StartBlock::
            /// ###########################################
            ///
            /// importType=1, menu="equity_category_1"
            /// importType=2, menu="equity_category_2"
            /// importType=3, menu="equity_category_3" 
            /// importType=4, menu="equity_category_4" 
            ///
            /// ###########################################
            var menu_name = ["equity_index_type1", "equity_index_type1"]; 
            dataimport.append('menu', menu_name[Number(importType)]);

            
            /// ::LAM-EndBlock::
            $('#check_ret' + importType).hide();
            $('#progress_import' + importType).show();
            /// 
            dataimport.append('type', importType); 
            

            $.ajax({
                cache: false,
                type: 'POST',
                contentType: false,
                processData: false,
                url: 'importEquityIndex',    // UserController
                data: dataimport,

                success: function(data){
        
                    if(data.success){
                        $('#progress_import' + importType).hide();
                        $('#check_ret'+importType).show();
                        $('#check_ret'+importType).html("ข้อมูลได้ถูก นำเข้า เรียบร้อยแล้ว" + data.html);

                        $('#btnImport' + importType).hide();
                        $('#btnCancel' + importType).html('กลับ');
                        AlertSuccess("ข้อมูลได้ถูก นำเข้า เรียบร้อยแล้ว",function(){
                           // window.location.href = "simple";
                        });
                        Alert('OK', "ข้อมูลได้ถูก นำเข้า เรียบร้อยแล้ว : ");

                    } else {
                        $('#btnCancel' + importType).html('ยกเลิก');
                        $('#progress_import' + importType).hide();
                        $('#check_ret' + importType).hide();
                        Alert('Import','การนำเข้า ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของไฟล์ ')
                    }

                },

                error: function(xhr, textStatus, thrownError) {
                    $('#progress_import' + importType).hide();
                    $('#check_ret' + importType).hide();
                    Alert('Error', "การนำเข้า ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของไฟล์ ");
                }
            });
        });  // button import

    
        ////////////////////////////////////////////////////////
        var eventName = function() {
            $.getJSON("progress", function(data) {
                // update the view with your fresh data
                console.log('########## total:' + data.total + ' progress: ' + data.percent_progress + '%');
                if (data.percent_progress < 100)
                     eventName();
            });
            /*
            var dataimport = new FormData();
            dataimport.append("method", "checkProgress");

            $.ajax({
                cache: false,
                type: 'POST',
                contentType: false,
                processData: false,
                url: 'progress',    // UserController
                data: dataimport,

                success: function(data){
                    console.log('progress' + data.progress_count);
                    if (data.progress_count < 10)
                        eventName();
                },
                error: function(xhr, textStatus, thrownError) {
                    Alert('Error', "การ import ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ");
                }
            });
            */

        };
        ///////////////////////////////////////////////////////

    });  // ready

</script>

@stop