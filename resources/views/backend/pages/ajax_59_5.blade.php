
@if($data)

    <table class="table table-bordered table-striped">
        <thead >
        <tr>
            <th style="width: 30%">URL</th>
            <th style="width: 30%">สถานะ</th>
        <th style="width: 10%">Action</th>
        </tr>
        </thead>
        <tbody>
@foreach($data as $index =>$list  )

    <tr>

        <td><input type="text" id="REDIRECT_LINK_{{$list->CONTROL_ID}}" value="{{$list->REDIRECT_LINK}}" class="form-control"></td>
        <td><input type="text" id="REDIRECT_FLAG_{{$list->CONTROL_ID}}" value="{{$list->REDIRECT_FLAG}}" class="form-control"></td>
        
        <td>
            <a href="#" class="btn btn-primary btn-xs btn_edit_list" data-id="{{$list->CONTROL_ID}}">บันทึก</a>
        </td>
    </tr>


@endforeach
        </tbody>
    </table>
@else
    <div class="list" style="margin-bottom: 10px; padding: 10px">
        <p style="font-size: 20px;padding: 10px;text-align: center;width: 100%;background-color: #f1f1f1;border: 1px solid #E1E8F3">ไม่พบข้อมูล</p>
    </div>

@endif


