@extends('backend.layouts.default')
@section('content')
<?php
$data = getmemulist();
$arrSidebar =getSideBar($data);
/**
 * File: p2_64_1_korchor_report.blade.php
 * Korchor Report  Menu 64-1
 */
?>

<style>
   .glyphicon-plus {
       color: #00FF00; 
   }
   .glyphicon-danger {
       color: #b41111; 
   }
   .glyphicon-lightgreen {
       color: #12940d; 
   }
   .glyphicon-green {
       color: #00FF00; 
   }
   .glyphicon-info {
       color: #5bc0de; 
   }
   .glyphicon-pink {
       color: #f20772; 
   }
   .glyphicon-luna {
       color: #c92456; 
   }

   /* table split row */
   .top_row_line {
    display: table;
    width: 100%;
   }

   .top_row_line > div {
    display: table-cell;
    width: 50%;
    border-bottom: 1px solid #ccc;
   }


   .hiddencol { display:none; };
   .showcol {display:none;};
 
    /* Scroll Menu */
    #meacontainer {
     position:absolute;
     width:100%;
     top:0;
     bottom:0;
     /*left:154;*/
     z-index:-1;
     overflow:hidden;
     background: #FF0000;
    }

    #dpsettings {
     overflow-y: scroll;
     width:200px;
     background: #FFFFFF;
     float:left;
     height:350px;
     margin-top:-5px;
     margin-left:154px
     position: absolute;
    }

   /* auto complete */
   .container { width: 800px; margin: 0 auto; }

   .autocomplete-suggestions { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 1px solid #999; background: #FFF; cursor: default; overflow: auto; -webkit-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); -moz-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); }
   .autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
   .autocomplete-no-suggestion { padding: 2px 5px;}
   .autocomplete-selected { background: #F0F0F0; }
   .autocomplete-suggestions strong { font-weight: bold; color: #000; }
   .autocomplete-group { padding: 2px 5px; }
   .autocomplete-group strong { font-weight: bold; font-size: 18px; color: #000; display: block; border-bottom: 1px solid #000; }

   /* input { font-size: 23px;  }*/

    .prism {
  white-space: pre-wrap;
  line-height: 20px;
  border-left: 4px solid #05A5D1;
  padding: 5px 10px;
  background-color: rgba(5, 165, 209, 0.05);
  overflow: auto;
   }

  .prism + .prism {
     margin-top: 10px;
   }

   #outer
{
    width:100%;
    text-align: center;
}
.inner
{
    display: inline-block;
}


</style>

<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>

                {{getMenutitle($arrSidebar)}}

            </h1>
        </div>

    </div>


    <div class="row" id="widget-grid" >

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <ul class="nav nav-tabs pull-left in">

                        <li class="active">

                            <a data-toggle="tab" href="#hr1"> <i class="fa fa-lg fa-arrow-circle-o-down"></i> <span class="hidden-mobile hidden-tablet"> ระบุเงื่อนไขการค้นหา </span> </a>

                        </li>

                        <!--li>
                            <a data-toggle="tab" href="#hr2"> <i class="fa fa-lg fa-arrow-circle-o-up"></i> <span class="hidden-mobile hidden-tablet"> รายงานสรุปของตราสารทุน  </span> </a>
                        </li-->

                    </ul>
                </header>

                <!-- widget div-->
                <div>


                    <!-- widget content -->
                    <div class="widget-body">

                        <div class="tab-content">
                            <div class="tab-pane active" id="hr1">

                                <div class="widget-body no-padding" style="width: 100% ;margin: 0 auto;background-color: #000000;border: 1px solid #E1E8F3">

                                    <header style="color: #fff; height: 40px; line-height: 40px; font-size: 18px; background-color: #a90329;padding-left: 20px;"> กล่องค้นหา </header>
                                
                                    <form  id="comment-form_darkman" class="smart-form" novalidate="novalidate">
                                        <fieldset>
                                            <div class="row">
                                                
                                                <section class="col col-4">
                                                    <label class="label"><!--input type="checkbox" id="check_depart" value="depart"-->&nbsp;ประเภทรายงาน</label>
                                                    <label class="input">
                                                        
                                                        <select name="kcreport" id="kcreport" class="form-control">
                                                            <option value="">ประเภทรายงาน</option>
                                                            @if($kcindex)
                                                                @foreach($kcindex as $item)
                                                                    <option value="{{$item->REPORT_ID}}">&nbsp;{{$item->REPORT_DESC}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>

                                                    </label>
                                                </section>
                                                <section class="col col-4">
                                                    <label class="label"><!--input type="checkbox" id="check_plan" value="plan"-->&nbsp;ชื่อบริษัท จัดการ</label>
                                                    <label class="input">
                                                        <select name="plan" id="plan" class="form-control">
                                                            <option value="">ชื่อบริษัท จัดการ</option>
                                                            @if($equitylist)
                                                                @foreach($equitylist as $item)
                                                                    <option value="{{$item->NAME_SHT}}">&nbsp;{{$item->NAME_SHT}}</option>
                                                                @endforeach
                                                            @endif
                                                            <option value="CONSO">&nbsp;CONSO</option>
                                                        </select>
                                                        {{--<input type="text" name="plan" id="plan">--}}
                                                    </label>
                                                </section>
                                            </div>

                                            <div class="row">
                                                <section class="col col-4">
                                                    <label class="label">&nbsp;เลือกช่วงเวลา เดือน/ ปี</label>
                                                    <label class="input"><i class="icon-append fa fa-calendar"></i>
                                                        <input type="text" class="mea_date_picker" id="date_start">
                                                    </label>
                                                </section>

                                                <!--section class="col col-4" style="display: block;">
                                                    <label class="label"><input type="checkbox" id="check_period_2h" value="2H"> &nbsp;ครึ่งปีหลัง ( ก.ค. - ธ. ค. )</label>
                                                    <label class="input">
                                                        <input type="text" class="mea_date_picker" id="date_end">
                                                        
                                                         
                                                    </label>
                                                </section-->

                                                <section class="col col-4">
                                                    <label class="label"></label>
                                                    <label class="input">
                                                    <button type="submit" id="btn_search" style="padding: 5px 20px 5px 20px; margin-top:20px;font-size: 18px;" name="btn_search" class="btn btn-primary">
                                                        ค้นหา
                                                    </button>
                                                    <!--button type="submit" id="btn_import" style="padding: 5px 20px 5px 20px; margin-top:20px;font-size: 18px;" name="btn_import" class="btn btn-primary">
                                                        นำเข้า
                                                    </button>
                                                     <button type="submit" id="btn_delete" style="padding: 5px 20px 5px 20px; margin-top:20px;font-size: 18px;" name="btn_delete" class="btn btn-primary">
                                                        ลบ
                                                    </button-->
                                                    </label>
                                                </section>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>

                               
                                <div id="meacontainer"></div>
                                <div class="container" style="margin-left:-12px; margin-top: 0px;">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="outer">

                                                <dtv class="inner">
                                                   <a href="{{action('AdminKorchorReportController@getimport')}}" id="mea_import" 
                                                        style="margin-top: 30px; margin-right: 5px; width:110px; display: inline-block;" 
                                                        class="btn btn-labeled bg-color-orange txt-color-white"> 
                                                        <span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i></span>นำเข้า</a>
                                                </dtv>
                                                <dtv class="inner">
                                                <a href="javascript:void(0);" id="mea_delete" 
                                                      style="margin-top: 30px; margin-right: 5px; width:100px; display: inline-block;" 
                                                      class="btn btn-labeled btn-danger"><span class="btn-label"><i class="glyphicon glyphicon-trash"></i></span>ลบ</a>
                                                </dtv>
                                             </div>                                 
                                                 
                                        </div>  
                                    </div>
                                    
                                </div>
                                  

                               
                                <!-- Widget ID (each widget will need unique ID)-->
                                <div class="jarviswidget jarviswidget-color-blueDark" style="margin-top: 5px;" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
                                    <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                    data-widget-colorbutton="false"
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true"export_search
                                    data-widget-sortable="false"

                                    -->

                                    <select class="form-control mea-pagesize" id="page-size-search">
                                        <option value="5">5</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>

                                    <header>
                                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    </header>

                                    <!-- widget div-->
                                    <div>

                                        <!-- widget edit box -->
                                        <div class="jarviswidget-editbox">
                                            <!-- This area used as dropdown edit box -->

                                        </div>
                                        <!-- end widget edit box -->

                                        <!-- widget content -->
                                        <div class="widget-body">
                                            <p class="report-title">
                                                  {{--getMenutitle($arrSidebar)--}}
                                            </p>
                                            <p class="report-period"> </p>
                                            <div class="table-responsive">
                                                <div id="serch_data">

                                                </div>
                                            </div>

                                        </div>
                                        <!-- end widget content -->

                                    </div>
                                    <!-- end widget div -->

                                </div>
                                <!-- end widget -->

                            </div>
                            

                        </div>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->



        </article>
        <!-- WIDGET END -->

    </div>



</div>
<!-- END MAIN CONTENT -->
<script src="{{asset('backend/js/plugin/jquery-autocomplete/scripts/jquery.mockjax.js')}}"></script> 
<script src="{{asset('backend/js/plugin/jquery-autocomplete/src/jquery.autocomplete.js')}}"></script> 
 
<script type="text/javascript">

/*var xxsymbols = { 
    {{--@foreach($symbols as $item)--}}
    '{{-- {{$item->SYMBOL}}' : '{{$item->SYMBOL}} | {{$item->COMP_NAME}}', --}}
    {{--@endforeach--}}

}*/
  
    $(document).ready(function(){
      
      $("#mea_delete").on('click',function(){
          var checkcount = $(".item_checked:checked").length;
            if(checkcount > 0){
                var checked = "";
                $(".item_checked").each(function(){
                    if($(this).is(":checked")){
                        checked = checked + $(this).val() + ",";
                    }
                });
                
                $.SmartMessageBox({
                    title : "ลบข้อมูล",
                    content : "ท่านแน่ใจที่ต้องการจะลบ รายการที่ท่านเลือก",
                    buttons : '[ยกเลิก][ตกลง]'

                    }, 
                    function(ButtonPressed) {
                      if (ButtonPressed === "ตกลง") {
                          var jsondata = {group_id : checked};
                          $.ajax({
                            type: 'post', // or post?
                            dataType: 'json',
                            url: '/admin/AdminKorchorReport/delete',
                            data: jsondata,

                            success: function(data) {
                              if(data.ret == "1"){
                                  $.smallBox({
                                      title: "Congratulations! Your form was submitted",
                                      content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                                      color: "#5F895F",
                                      iconSmall: "fa fa-check bounce animated",
                                      timeout: 8000
                                  });
                                  //window.location.href = '/admin/AdminKorchorReport';
                                  $('#btn_search').trigger( "click" );
                              }
                            },
                            error: function(xhr, textStatus, thrownError) {

                            }
                          });  //ajax
                      }
                  }); // smartMessageBox
              

            } else {
                $.SmartMessageBox({
                    title : "Error!",
                    content : "ท่านยังไม่ได้เลือกรายการ",
                    buttons : '[OK]'
                }, function(ButtonPressed) {
                    if (ButtonPressed === "OK") {
                    }
                    if (ButtonPressed === "No") {
                    }

                });
            }
        });




        $('#export_search').on('click',function() {
           /* var PageSizeAll = $('#page-size-search').val();
            var EmpID = $('#name').val();
            var depart = $('#depart').val();
            var plan = $('#plan').val();
            var date_start = $('#hd_date_start').val();
            var date_end =$('#hd_date_end').val();


            var check_name =$('#check_name').is(':checked');
            var check_depart =$('#check_depart').is(':checked');
            var check_plan =$('#check_plan').is(':checked');
            var check_date =$('#check_date').is(':checked');

            window.location.href =  "EquityStockUniverseReport/exportsearch?EmpID=" + EmpID +"&depart=" + depart +
                                    "&plan=" + plan + "&date_start=" +date_start + "&date_end=" + date_end+ 
                                    '&check_name=' + check_name+ '&check_depart=' + check_depart+ '&check_plan=' + 
                                    check_plan+ '&check_date=' + check_date;*/
            return false;
        });

        meaThaiDatepicker("date_start","date_end");
        meaThaiDatepicker("date_end");
/*
        $('#datepicker1').datepicker( {
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'MM yy',
            onClose: function(dateText, inst) { 
               var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
               var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
               $(this).datepicker('setDate', new Date(year, month, 1));
           }
        });
*/

        $('html').append('<input type="hidden" value="1" id="currentpage_search" />');

        $('#btn_search').on('click',function() {
            var PageSizeAll = $('#page-size-search').val();
            var report_id = $('#kcreport').val();
            var plan = $('#plan').val();
            var date_start =  $('#date_start').val();
            
            /*var check_name =$('#check_name').is(':checked');
            var check_depart =$('#check_depart').is(':checked');
            var check_plan =$('#check_plan').is(':checked');
            var check_date =$('#check_date').is(':checked');
              */
               console.log('date_start==>' + date_start);
            if(date_start == "") {
                date_start = '';// '1' + ' ' + $('#date_start').val();
                var str =  'ช่วงเวลา ทั้งหมด ' ;
                //$('.report-period').html(str);
                console.log(str);
            } 
            else if(date_start != ""){
                var str =  'ในช่วงเวลา ' + GetDateFormat(date_start) ;
                //$('.report-period').html(str);
                //date_start =  '1' + ' ' + $('#date_start').val();
                // convert format: 1 ม.ค. 2017 to 2017-01-01
                // 

            }

            var jsondata = {
                pagesize : PageSizeAll,
                PageNumber:1,
                report_id : report_id,
                plan : plan,
                date_start: date_start
            };

            console.log(jsondata);
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(jsondata,"AdminKorchorReport/search", RenderSearch);

            return false;

        });


        $("#page-size-search").on('change',function() {
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            var val = $(this).val();

            var report_id = $('#kcreport').val();
            var plan = $('#plan').val();
            var date_start =  $('#date_start').val();
           
            if(date_start =="") {
                date_start = '';// '1' + ' ' + $('#date_start').val();
                var str =  'ช่วงเวลา ทั้งหมด ' ;
                //$('.report-period').html(str);
            } 
            else if(date_start != ""){
                var str =  'ในช่วงเวลา ' + GetDateFormat(date_start) ;
                //$('.report-period').html(str);
               // date_start =  '1' + ' ' + $('#date_start').val();
            }

            /*var jsondata = {
                pagesize : val,
                PageNumber:1,
                emp_id : EmpID,
                depart :depart,
                plan : plan,
                date_start:date_start,
                date_end:date_end,
                check_name :check_name,
                check_depart:check_depart,
                check_plan:check_plan,
                check_date:check_date

            };*/
            var jsondata = {
                pagesize : val,
                PageNumber:1,
                report_id : report_id,
                plan : plan,
                date_start:date_start
            };

//            var jsondata = {pagesize : val,PageNumber:1};
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(jsondata,"AdminKorchorReport/search",RenderSearch);

        });


        // Initialize autocomplete with custom appendTo:
        /*var symbolsArray = $.map(symbols, function (value, key) { return { value: value, data: key }; });
        $('#autocomplete-dynamic').autocomplete({
            lookup: symbolsArray
         });
*/
    });  // jquery ready


    /**
     * Utils
     */ 
     function meaThaiDatepicker(id, id_date_end){
     
        if($("#hd_"+id).length == 0){
            $("#"+id).after("<input type='hidden' value='' name='hd_"+id+"' id='hd_"+id+"' />");
        }

        $("#" + id).datepicker({
            //dateFormat: 'M yy',
            dateFormat: 'dd M yy',
            altField: "#hd_"+id+"",
            altFormat: "yyyy-mm-dd",
            prevText : '<i class="fa fa-chevron-left"></i>',
            nextText : '<i class="fa fa-chevron-right"></i>',
            // defaultDate : new Date(d.getFullYear() + 543,d.getMonth(),d.getDate()),
            isBuddhist: true,
            //defaultDate: toDay,
            dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
            dayNamesMin: ['อา.','จ.','อ.','พ.','พฤ.','ศ.','ส.'],
            monthNames: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],
            monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
            beforeShow : function(date){
             //console.log($(date).val());
                //var dates = $(date).val()
             //
                //var arrselectedDate = dates.split(' ');
             //
                //var year = parseInt(arrselectedDate[2]);
                //  year = year - 543;
             //
                //$(date).val(arrselectedDate[0] + " " + arrselectedDate[1] + " " + (parseInt(arrselectedDate[2]) - 543))


            },

            onSelect : function(selectedDate) {
                $("#date_end").datepicker('option', 'minDate', selectedDate);
                if(id_date_end != null){
                    $("#" + id_date_end).datepicker('option', 'minDate', selectedDate);
                }
            }
        });
    }


    function RenderSearch(data){

        $("#serch_data").html(data.html);
        $("#serch_data").fadeIn('300');


        $("#page_click_search li a").on('click', PageRenderSearch);



        $(".mea_delete_by").on('click',function(){
            
            // alert('mea_delete_by');
            var id = $(this).attr("data-id");

            $.SmartMessageBox({
                title : "ลบข้อมูล",
                content : "ท่านแน่ใจที่ต้องการจะลบ รายการที่ท่านเลือก",
                buttons : '[ยกเลิก][ตกลง]'

            }, function(ButtonPressed) {

                if (ButtonPressed === "ตกลง") {

                    var jsondata = {group_id : id};
 
                    $.ajax({
                        type: 'post', // or post?
                        dataType: 'json',
                        url: '/admin/AdminKorchorReport/delete',
                        data: jsondata,

                        success: function(data) {

                            if(data.ret == "1"){
                                $.smallBox({
                                    title: "Congratulations! Your form was submitted",
                                    content: "<i class='fa fa-clock-o'></i> <i>0.5 seconds ago...</i>",
                                    color: "#5F895F",
                                    iconSmall: "fa fa-check bounce animated",
                                    timeout: 8000
                                });

                                $('#btn_search').trigger( "click" );
                            }
                        },
                        error: function(xhr, textStatus, thrownError) {
                        }
                    });
                }
                if (ButtonPressed === "ยกเลิก") {
                }
            });
        });

    }

    function PageRenderSearch(){

        var p = $(this).attr('data-page');
        var page_size = $('#page-size-search').val();
        var CurPage = $('#currentpage_search').val();
        $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');

        if(p == "pre"){
            p = parseInt(CurPage) - 1;
        }

        if(p == "next"){
            p = parseInt(CurPage) + 1;
        }
        $('#currentpage_search').val(p);

       /* var EmpID = $('#name').val();
        var depart = $('#depart').val();
        var plan = $('#plan').val();
        var date_start = $('#hd_date_start').val();
        var date_end =$('#hd_date_end').val();

        var check_name =$('#check_name').is(':checked');
        var check_depart =$('#check_depart').is(':checked');
        var check_plan =$('#check_plan').is(':checked');
        var check_date =$('#check_date').is(':checked');
*/
        var report_id  = $('#kcreport').val();
        var plan       = $('#plan').val();
        var date_start =  $('#date_start').val();
       
        if(date_start =="") {
            date_start = '';// '1' + ' ' + $('#date_start').val();
            var str =  'ช่วงเวลา ทั้งหมด ' ;
            //$('.report-period').html(str);
        } 
        else if(date_start != ""){
            var str =  'ในช่วงเวลา ' + GetDateFormat(date_start) ;
            //$('.report-period').html(str);
           // date_start =  '1' + ' ' + $('#date_start').val();
        }


        var jsondata = {
            pagesize : page_size,
            PageNumber:p,
            report_id : report_id,
            plan : plan,
            date_start:date_start

        };

        MeaAjax(jsondata,"AdminKorchorReport/search",RenderSearch);
    };

</script>

@stop
