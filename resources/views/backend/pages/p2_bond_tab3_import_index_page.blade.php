<!--
FILE: p2_bond_tab3_import_index_page.blade.php
Purpose: Import XLS into database
-->

@extends('backend.layouts.default')
@section('content')
<?php
$data = getmemulist();
$arrSidebar =getSideBar($data);
//echo ('bon_tab3_import_index_page => '.print_r($arrSidebar));
?>

    <style type="text/css">
        #sort-by-year{
            /*position:absolute;*/
            width: 100%;
            margin-bottom: 10px;
            /*z-index: 999;*/
            /*left: 200px;*/
            /*top:5px*/
        }
        #sort-by-year select{
            width: 300px;

        }
        #datatable_fixed_column tbody tr td a i{
            font-size: 11px !important;
            line-height: 12px!important;
        }
        #datatable_fixed_column tbody tr td a{
            line-height: 12px!important;

        }
    </style>
 
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>
                {{getMenutitle($arrSidebar)}}
            </h1>
        </div>
    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="well well-sm">
                <!-- Timeline Content -->
                <div class="smart-timeline">
                    <ul class="smart-timeline-list">
                        <li>
                            <div class="smart-timeline-icon bg-color-greenDark">
                                <i class="fa fa-file-text"></i>
                            </div>
                            <div class="smart-timeline-time">
                                <small>รูปแบบ</small>
                            </div>
                            <div class="smart-timeline-content">
                                <p>
                                    <a href="javascript:void(0);"><strong>เพื่อทําการนำเข้าข้อมูล หลักทรัพย์ จํานวนมาก</strong></a>
                                </p>
                                <p>ชนิดไฟล์ที่อนุญาติให้นำเข้า:Exel</p>
                                <a class="btn btn-xs btn-success" href="{{action('BondCompanyManagementController@dowloadsampleBondIndex')}}"> ดูตัวอย่างไฟล์</a>
                                <p></p>
                                <table  class="table table-bordered">
                                    <tr><th style="width:10%;">หลักทรัพย์</th><th data-class="expand">บริษัท</th><th>ตลาด</th><th>SET INDEX50</th><th>SET INDEX100</th><th>...</th><th data-class="expand">เว๊บไซต์</th></tr>
                                    <tr><td style="width:10%;">AMATA</td><td data-class="expand">บริษัท อมตะ คอร์ปอเรชัน จำกัด (มหาชน)</td><td>SET</td><td>Y</td><td>Y</td><td>...</td><td>http://www.amata.com</td></tr>
                                </table>


                                <table  class="table table-bordered">
                                    <tr>
										<th style="width:10%;">ชื่อฟิลด์</th>
										<th style="width:20%;">คำอธิบาย</th>
										<th style="width:20%;">หมายเหตุ</th>
									</tr>
									<tr><td style='width:10%;'>ThaiBMA Symbol</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>ISIN</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Issue Name Thai</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Issuer</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Company Rating / Rating Agency</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Business Sector</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Bond Type by Issuer</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Principal Payment</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Bond Structure</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Debt Instrument Type</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Secured Type</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Secured by</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>TRIS</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Fitch (Thailand)</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Moody's</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>S&P</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Fitch Rating</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>R&I</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Current Par (THB)</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Issue Size (THB mln)</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Outstanding Value (THB mln)</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Issue Term (Yrs.)</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>TTM (Yrs.)</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Issue Date</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Maturity Date</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Registered Date</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Coupon Payment</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Payment Date</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>XI Date</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Reset Date</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Coupon Frequency</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Coupon (%)</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Embedded Option</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Claim Type</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Distribution Type</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Duration</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Registrar</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Underwriter</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Bondholder Representative</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                    <tr><td style='width:10%;'>Issuer Name</td><td style='width:20%;'></td><td style='width:20%;'></td></tr>
                                </table>


                                <div class="form-group">
                                      <p class="help-block">
                                      <span style="color: red">จำนวน record ที่สามารถ import ได้สูงสุดต่อครั้งคือ <strong>10,000 records เท่านั้น!!!</strong></span>
                                          <br/>เลือกไฟล์ ที่ต้องการนำเข้า
                                          
                                      </p>
                                      <input type="file" class="filestyle" data-buttonBefore="true" data-buttonName="btn-primary" 
                                           data-buttonText="เลือกไฟล์" id="import1" name="import1">
                                </div>

                                

                                    <!--p class="help-block" id="file_attached" name="file_attached">[ Not Selected ]</p-->

                                </p>
                                <p>
                                     <span style="font-size: 16px;color: #3276b1; font-style: italic;">
                                        * เพื่อทําการนำเข้าข้อมูล หลักทรัพย์จำนวนมากกว่า 1 record ขึ้นไป 
                                     </span>   
                                     
                                </p>

                                <p id="progress_import1" style="display: none;"><img src="{{asset('backend/img/shot.gif')}}"  /> กำลังนำเข้าข้อมูล</p>
                                <p id="check_ret1" style="display: none"></p>
                                <p>
                                    <a id="btnCancel1" href="javascript:void(0);" onclick="window.history.back();" class="btn btn-xs bg-color-blueDark txt-color-white"><i class="fa fa-back"></i>   ยกเลิก  </a>
                                    <a id="btnImport1" href="javascript:void(0);"  data-input="import1" data-import="1" class="btn_import btn btn-xs btn-primary"><i class="fa fa-download"></i> ยืนยัน</a>
                                 </p>
                                <p>
                                
                                    
                                </p>

                                <div class="row">

                                </div>
                            </div>
                        </li>                        
                    </ul>
                </div>
                <!-- END Timeline Content -->

            </div>

        </div>

    </div>

</div>
<!-- END MAIN CONTENT -->


<!-- PAGE RELATED PLUGIN(S) -->
{{--<script src="{{asset('backend/js/plugin/datatables/jquery.dataTables.min.js')}}"></script>--}}
{{--<script src="{{asset('backend/js/plugin/datatables/dataTables.colVis.min.js')}}"></script>--}}
{{--<script src="{{asset('backend/js/plugin/datatables/dataTables.tableTools.min.js')}}"></script>--}}
{{--<script src="{{asset('backend/js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>--}}
{{--<script src="{{asset('backend/js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>--}}

<script src="{{asset('backend/js/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script> 
<script type="text/javascript">


    $(document).ready(function() {


        if (navigator.userAgent.indexOf("MSIE") > 0) {
            $("#import1").mousedown(function() {
                $(this).trigger('click');
            })
        } else if($.browser.mozilla) {
           
        }

       /* $('input[type=file]').change(function(e){
            $in=$(this);
            // alert($in.val());
            $('#file_attached').text($in.val());
        });*/

        $('.btn_import').on('click',function(){

            var dataimport = new FormData();
            var target = $(this).attr('data-input');

            var files = $("#" + target).get(0).files;            
            var importType= $(this).attr('data-import');

            if (files.length > 0) {
                dataimport.append("exelimport", files[0]);
            }    

            var menu_name = ["bond_index_type1", "bond_index_type1"]; 
            dataimport.append('menu', menu_name[Number(importType)]);

            $('#check_ret' + importType).hide();
            $('#progress_import' + importType).show();

            dataimport.append('type', importType); 
            //alert('dataimport -> ' + JSON.stringify(dataimport));
            $.ajax({
                cache: false,
                type: 'POST',
                contentType: false,
                processData: false,
                url: 'importBondIndex',   
                data: dataimport,
 
                success: function(data){
                    //alert('success => data [' + data + ']');
                
                    if(data.success){
                        $('#progress_import' + importType).hide();
                        $('#check_ret'+importType).show();
                        $('#check_ret'+importType).html("ข้อมูลได้ถูก นำเข้า เรียบร้อยแล้ว" + data.html);

                        $('#btnImport' + importType).hide();
                        $('#btnCancel' + importType).html('กลับ');
                        AlertSuccess("ข้อมูลได้ถูก นำเข้า เรียบร้อยแล้ว",function(){
                           // window.location.href = "simple";
                        });
                        Alert('OK', "ข้อมูลได้ถูก นำเข้า เรียบร้อยแล้ว : ");

                    } else {
                        $('#btnCancel' + importType).html('ยกเลิก');
                        $('#progress_import' + importType).hide();
                        $('#check_ret' + importType).hide();
                        Alert('Import','การนำเข้า ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของไฟล์ ');
                    }

                },

                error: function(xhr, textStatus, thrownError) {
                    $('#progress_import' + importType).hide();
                    $('#check_ret' + importType).hide();
                    Alert('Error', "การนำเข้า ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของไฟล์ ");
                }
            });
        });  // button import

    
        ////////////////////////////////////////////////////////
        var eventName = function() {
            $.getJSON("progress", function(data) {
                // update the view with your fresh data
                console.log('########## total:' + data.total + ' progress: ' + data.percent_progress + '%');
                if (data.percent_progress < 100)
                     eventName();
            });
        };
        ///////////////////////////////////////////////////////

    });  // ready

</script>

@stop