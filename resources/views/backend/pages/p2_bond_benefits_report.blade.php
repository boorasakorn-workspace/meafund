@extends('backend.layouts.default')
@section('content')
<?php
$data = getmemulist();
$arrSidebar =getSideBar($data);
?>

<!--link rel="stylesheet" href="{{asset('backend/js/plugin/chartist/chartist.min.css')}}"/> 
<link rel="stylesheet" href="{{asset('backend/js/plugin/chartist/ct-customize.css')}}">
<link rel="stylesheet" href="{{asset('backend/js/plugin/chartist/plugin/chartist-plugin-tooltip.css')}}"/-->

<!--link href="{{asset('backend/js/plugin/echart/asset/css/carousel.css')}}" rel="stylesheet"-->
<style type="text/css">
 #graph_label .tickLabel{

    font-size: 100%;
 }



/* CheckBox */ 
.hiddencol { display:none; };
.showcol {display:none;};
 
/* Scroll Menu */
#meacontainer {
     position:absolute;
     width:100%;
     top:0;
     bottom:0;
     /*left:154;*/
     z-index:-1;
     overflow:hidden;
     background: #FF0000;
 }

 #dpsettings {
     overflow-y: scroll;
     width:200px;
     background: #FFFFFF;
     float:left;
     height:350px;
     margin-top:-5px;
     margin-left:154px
     position: absolute;
 }

 .glyphicon-plus {
       color: #00FF00; 
   }
   .glyphicon-danger {
       color: #b41111; 
   }
   .glyphicon-lightgreen {
       color: #12940d; 
   }
   .glyphicon-green {
       color: #00FF00; 
   }
   .glyphicon-info {
       color: #5bc0de; 
   }
   .glyphicon-pink {
       color: #f20772; 
   }
   .glyphicon-luna {
       color: #c92456; 
   }

   /* table split row */
   .top_row_line {
    display: table;
    width: 100%;
   }

   .top_row_line > div {
    display: table-cell;
    width: 50%;
    border-bottom: 1px solid #ccc;
   }
   /* auto complete */
   .container { width: 800px; margin: 0 auto; }

   .autocomplete-suggestions { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 1px solid #999; background: #FFF; cursor: default; overflow: auto; -webkit-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); -moz-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); }
   .autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
   .autocomplete-no-suggestion { padding: 2px 5px;}
   .autocomplete-selected { background: #F0F0F0; }
   .autocomplete-suggestions strong { font-weight: bold; color: #000; }
   .autocomplete-group { padding: 2px 5px; }
   .autocomplete-group strong { font-weight: bold; font-size: 18px; color: #000; display: block; border-bottom: 1px solid #000; }

   /* Section Header*/
    .custom-combobox {
        position: relative;
        display: inline-block;
    }
    
    .custom-combobox-toggle {
        position: absolute;
        top: 0;
        bottom: 0;
        margin-left: -1px;
        padding: 0;
    }
      .custom-combobox-input {
        margin: 0;
        padding: 5px 10px;
      }

      optgroup {
        background-color: #FF8000; //#DCDCDC;
        color: white;
      }
      option {
        background-color: white;
        color: black;
      }

</style>

<div id="content">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>

                {{getMenutitle($arrSidebar)}}

            </h1>
        </div>

    </div>


    <div class="row" id="widget-grid" >

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"
                -->
                <header>
                    <ul class="nav nav-tabs pull-left in">
                        <li class="active">
                            <a data-toggle="tab" href="#hr1"> 
                               <i class="fa fa-lg fa-arrow-circle-o-down"></i> 
                               <span class="hidden-mobile hidden-tablet"> ระบุเงื่อนไขการค้นหา </span> 
                            </a>
                        </li>
                    </ul>
                </header>

                <!-- widget div-->
                <div>
                    <!-- widget content -->
                    <div class="widget-body">

                        <div class="tab-content">
                            <div class="tab-pane active" id="hr1">

                                <div class="widget-body no-padding" style="width: 100% ;margin: 0 auto;background-color: #000000;border: 1px solid #E1E8F3">
                                    <header style="color: #fff; height: 40px; line-height: 40px; font-size: 18px; background-color: #a90329;padding-left: 20px;"> กล่องค้นหา </header>

                                    <form  id="comment-form_darkman" class="smart-form" novalidate="novalidate">
                                        {{-- {!! csrf_field() !!} --}}
                                        <fieldset>
                                            <div class="row">
                                                <section class="col col-4">
                                                     <label style="font-size:18px">&nbsp;ประเภทตราสาร</label>
                                                     <label class="input">
                                                        <select name="industrial" id="industrial" class="form-control">
                                                            <option value="">&nbsp;กรุณาเลือก หนึ่งรายการ &nbsp;</option>
                                                            @if($sectionList)
                                                                @foreach($sectionList as $key => $value)
                                                                    <optgroup label="&nbsp;{{$key}}">
                                                                    @foreach($value as $child)
                                                                        <option value="{{$child['CATE_ID']}}">&nbsp;&nbsp;&nbsp;{{$child['BOND_TYPE_THA']}}</option>
                                                                    @endforeach
                                                                    </optgroup> 
                                                                @endforeach   
                                                            @endif
                                                        </select>
                                                    </label>
                                                </section>

                                                 <section class="col col-4">
                                                    <label class="label">&nbsp;อายุตราสาร</label>
                                                    <label class="input">
                                                        <select name="age" id="age" class="form-control">
                                                            <option value="">ระบุอายุตราสาร</option>
                                                            <option value="0">&nbsp;คงเหลือน้อยกว่าหรือเท่ากับ 3 เดือน)</option>
                                                            <option value="1">คงเหลือน้อยกว่า หรือเท่ากับ 6 เดือน </option>
                                                            <option value="2">คงเหลือน้อยกว่าหรือเท่ากับ 1 ปี</option>
                                                            <option value="3">คงเหลือมากกว่า 6 เดือน แต่น้อยกว่า หรือเท่ากับ 1 ปี</option>
                                                            <option value="4">คงเหลือมากกว่า 1 ปี แต่น้อยกว่า หรือเท่ากับ 3 ปี</option>
                                                            <option value="5">คงเหลือมากกว่า 3 ปี แต่น้อยกว่า หรือเท่ากับ 5 ปี</option>
                                                            <option value="6">คงเหลือมากกว่า 5 ปีขึ้นไป</option>

                                                        </select>
                                                    </label>
                                                </section>
                                            </div>

                                            <div class="row">
                                                 <section class="col col-4">
                                                    <label class="label">&nbsp;ชื่อบริษัท จัดการ</label>
                                                    <label class="input">
                                                        <select name="securities_name" id="securities_name" class="form-control">
                                                            <option value="">ระบุชื่อบริษัท จัดการ</option>
                                                            @if($securitieslist)
                                                                @foreach($securitieslist as $item)
                                                                    <option value="{{$item->NAME_SHT}}">&nbsp;{{$item->NAME_SHT}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </label>
                                                </section>

                                                <section class="col col-4">
                                                    <label class="label">&nbsp;ระบุปีอ้างอิง</label>
                                                    <label class="input">
                                                         <select name="years" id="years" class="form-control">
                                                            <option value="">ระบุปีอ้างอิง</option>
                                                            @if($years)
                                                                @foreach($years as $yyyy)
                                                                    <option value="{{$yyyy}}">&nbsp;{{$yyyy}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
      
                                                    </label>
                                                </section>
                                                
                                            </div>

                                            <div class="row" style="display:none">
                                                <section class="col col-4">
                                                    <label class="label">&nbsp;ชื่อตราสาร</label>
                                                    <label class="input">
                                                          <input type="text"  name="symbol" placeholder="ระบุประเภทตราสาร"
                                                          id="symbol" 
                                                          style="width: 100%; border-width: 1px;"/>
                                                    </label>
                                                </section>

                                                
                                                 
                                                <section class="col col-4">
                                                    <label class="label">&nbsp;ชื่อบริษัท Broker</label>
                                                    <label class="input">
                                                        <select name="broker" id="broker" class="form-control">
                                                            <option value="">ระบุชื่อบริษัท Broker</option>
                                                            @if($equitybroker)
                                                                @foreach($equitybroker as $item)
                                                                    <option value="{{$item->NAME_SHT}}">&nbsp;{{$item->BROKER_NAME}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </label>
                                                </section>
                                            </div>

                                            <div class="row" style="display:block;">
                                                

                                                <section class="col col-4" style="display:none;">
                                                    <label class="label">&nbsp;ช่วงกำไร-ขาดทุน</label>
                                                    <label class="input">
                                                          <!--autocomplete-dynamic --> 
                                                          <input type="text"  name="bondrange" placeholder="ระบุช่วงกำไร-ขาดทุน"
                                                          id="bondrange" 
                                                          style="width: 100%; border-width: 1px;"/>
        
                                                    </label>
                                                </section>
                                            </div>    

                                            <div class="row" >
                                                <section class="col col-4" style="display:none;">
                                                    <label class="label">&nbsp;ระบุช่วงเวลา  </label>
                                                    <label class="input"><i class="icon-append fa fa-calendar"></i>
                                                        <input type="text" class="mea_date_picker" id="date_start" >
                                                    </label>
                                                </section>

                                                <section class="col col-4" style="display:none;">
                                                    <label class="label">ถึง</label>
                                                    <label class="input"><i class="icon-append fa fa-calendar"></i>
                                                        <input type="text" class="mea_date_picker" id="date_end">
                                                    </label>
                                                </section>

                                                <section class="col col-4">
                                                    <button type="submit" id="btn_search" style="padding: 5px 20px 5px 20px; margin-top:20px;font-size: 18px;" name="submit" class="btn btn-primary">
                                                        ค้นหา
                                                    </button>
                                                </section>
                                            </div>
                                        </fieldset>

                                    </form>
                                </div>

                                <div id="meacontainer"></div>
                                <div class="container" style="margin-left:-12px; margin-top: 0px;">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="button-group bullet">
                                                
                                                 <a href="#" id="export_search" style="margin-top: 30px; display:none;" 
                                                    class="btn btn-labeled btn-success mea-btn-export"> 
                                                    <span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i>
                                                 </span>Export </a>

                                                 <a href="#" id="btn_customize" style="margin-top: 30px; display:none;" class="btn btn-labeled btn-warning dropdown-toggle" data-toggle="dropdown"> 
                                                    <span class="btn-label"><i class="glyphicon glyphicon-list"></i></span>Customize </a>
                                             
                                                 <ul id='dpsettings' role="menu" class="dropdown-menu pull-center">
                                                    <li><a href="#" class="small" data-value="selectall" tabIndex="-1"><input id="selectall" type="checkbox"/>&nbsp;เลือกทุกค่า</a></li>
                                                   

                                                    <li class="divider"></li>
                                                    <p><a href="#"><button class="btn btn-success" id='btn-save-settings' style="margin-left:20px; margin-top:5px; margin-right:5px; padding: 5px 10px 5px 5px; ">&nbsp;จัดเก็บ</button></a>
                                                        <a href="#">
                                                          <button class="btn btn-danger" id='btn-cancel-settings' style="margin-left:20px; margin-top:5px; margin-right:5px; padding: 5px 10px 5px 5px; ">&nbsp;ยกเลิก</button></a></p>
                                                     <li class="divider"></li>
                                                    </a></p>
                                                     
                                                    <li><a href="#" class="small" data-value="option0" tabIndex="-1"><input id="option0" type="checkbox"/>&nbsp;วันเดือนปี</a></li>
                                                    <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input id="option1" type="checkbox"/>&nbsp;ชื่อ</a></li>
                                                    <li><a href="#" class="small" data-value="option2" tabIndex="-1"><input id="option2" type="checkbox"/>&nbsp;บลจ.</a></li>
                                                    <li><a href="#" class="small" data-value="option3" tabIndex="-1"><input id="option3" type="checkbox"/>&nbsp;กลุ่มอุตสาหกรรม</a></li>
                                                    <li><a href="#" class="small" data-value="option4" tabIndex="-1"><input id="option4" type="checkbox"/>&nbsp;หมวดธุรกิจ</a></li>
                                                     
                                                    <li class="divider"></li>
                                                    <li class="disabled" label="เงินปันผล"><a href="#"><i class="icon-user"></i> <b> เงินปันผล</b></a></li>
                                                     
                                                    <li><a href="#" class="small" data-value="option5" tabIndex="-1"><input id="option5" type="checkbox"/>&nbsp;ผลปันผล</a></li>
                                                    <!--li><a href="#" class="small" data-value="option6" tabIndex="-1"><input id="option5" type="checkbox"/>&nbsp;ราคาต่อหน่วย</a></li-->
                                                    <li><a href="#" class="small" data-value="option6" tabIndex="-1"><input id="option6" type="checkbox"/>&nbsp;จำนวนเงิน</a></li>
                                                    
                                                    <li class="divider"></li>
                                                    <li class="disabled"><a href="#"><i class="icon-user"></i> <b> ข้อมูลการซื้อ</b></a></li>  
                                                    <li><a href="#" class="small" data-value="option7" tabIndex="-1"><input id="option7" type="checkbox"/>&nbsp;หน่วยซื้อ</a></li>
                                                    <li><a href="#" class="small" data-value="option8" tabIndex="-1"><input id="option8" type="checkbox"/>&nbsp;ราคาซื้อต่อหน่วย </a></li>
                                                    <li><a href="#" class="small" data-value="option9" tabIndex="-1"><input id="option9" type="checkbox"/>&nbsp;ยอดซื้อ (บาท)</a></li>
                                                    <li><a href="#" class="small" data-value="option10" tabIndex="-1"><input id="option10" type="checkbox"/>&nbsp;คงเหลือ (หน่วย)</a></li>

                                                    <li class="divider"></li>
                                                    <li class="disabled"><a href="#"><i class="icon-user"></i> <b> ข้อมูลการขาย</b></a></li>                                                       
                                                    <li><a href="#" class="small" data-value="option11" tabIndex="-1"><input id="option11" type="checkbox"/>&nbsp;หน่วยขาย</a></li>
                                                    <li><a href="#" class="small" data-value="option12" tabIndex="-1"><input id="option12" type="checkbox"/>&nbsp;ราคาขายต่อหน่วย</a></li>
                                                    <li><a href="#" class="small" data-value="option13" tabIndex="-1"><input id="option13" type="checkbox"/>&nbsp;ยอดขาย (บาท) </a></li>
                                                    <li><a href="#" class="small" data-value="option14" tabIndex="-1"><input id="option14" type="checkbox"/>&nbsp;กำไร-ขาดทุน</a></li>
                                                    <li><a href="#" class="small" data-value="option15" tabIndex="-1"><input id="option15" type="checkbox"/>&nbsp;คงเหลือ (หน่วย)</a></li>

                                                </ul>
                                                
                                                <a href="#" id="btn_chart"     style="margin-top: 30px; display:none;" class="btn btn-labeled btn-info"> <span class="btn-label"><i class="glyphicon glyphicon-signal"></i></span>Chart </a>
 
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Widget ID (each widget will need unique ID)-->
                                
                                <div class="jarviswidget jarviswidget-color-blueDark" style="margin-top: 5px; display:block;" 
                                     id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
                                    <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                    data-widget-colorbutton="false"
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true"export_search
                                    data-widget-sortable="false"

                                    -->

                                    <select class="form-control mea-pagesize" id="page-size-search" style="display:none;">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>

                                    <header>
                                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    </header>

                                    <!-- widget div-->
                                    <div id="widget_search_table" style="display:block;">

                                        <!-- widget edit box -->
                                        <div class="jarviswidget-editbox">
                                            <!-- This area used as dropdown edit box -->

                                        </div>
                                        <!-- end widget edit box -->
                                        
                                        
                                        <!-- widget content -->
                                        <div class="widget-body"  >
                                            <div class="table-responsive" >
                                                <!--p class="report-title">
                                                    {{getMenutitle($arrSidebar)}}
                                                </p-->
                                                <div id="serch_data">

                                                </div>
                                            </div>

                                        </div>
                                        <!-- end widget content -->
                                           
                                    </div>
                                    
                                    <!-- end widget div -->

                                

                                

                                 
                                    <!-- bar chart -->
                                    <div class="table-responsive" id="widget_bar_chart" style="display:none;">

                                            @if((!empty($date_start)) && (!empty($date_end)))
                                            <p class="report-title" id="benefits-chart-title" style="display:block;">
                
                                                {{getMenutitle($arrSidebar)}}
                                            </p>
                                            <p class="report-period">
                                                
                                                @if($date_start != $date_end)
                                                     ข้อมูล ระหว่าง วันที่ <span color="#ff0000">{{$date_start}}</span> ถึง <span color="#ff0000">{{$date_end}} </span>
                                                @else
                                                     ข้อมูล วันที่ <span color="#ff0000">{{$date_start}}</span> เวลา
                                                                <span color="#ff0000">00:00</span> ถึง <span color="#ff0000">23:59</span>     
                                                @endif
                                            </p>
                                            @else
                                              <!--p class="report-period">
                                              รายงานข้อมูล ณ วันที่ {{get_date_notime(date("Y-m-d H:i:s"))}}
                                              </p-->
                                            @endif
                                            <div id="mea_chart" class="mea_chart" style="width: 100%; padding: 0px; display:block;">

                                            </div>
                                            
                                    </div>

                                </div>
                                 
                                <!-- end widget -->
                            </div>
                            

                        </div>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->



        </article>
        <!-- WIDGET END -->



    </div>



</div>
<!-- END MAIN CONTENT -->

<!-- FLOT CHARTS -->
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.time.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.selection.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.resize.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.pie.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.stack.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.crosshair.min.js')}}"></script>
<script type="text/javascript" language="javascript" src="{{asset('backend/js/plugin/flot_p2/jquery.flot.orderBars.js')}}"></script>

<!-- flot tooltips plugin-->
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.tooltip.min.js')}}"></script>

<!-- FLOT GROWRAF -->
<script src="{{asset('backend/js/plugin/flot-growraf/jquery.flot.growraf.min.js')}}"></script>



<!-- ECHART english -->
<script src="{{asset('backend/js/plugin/echart/www/js/echarts-all-english-v2.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('backend/js/plugin/echart/www/js/style.css')}}" />

<!-- Auto Complete -->
<script src="{{asset('backend/js/plugin/jquery-autocomplete/scripts/jquery.mockjax.js')}}"></script> 
<script src="{{asset('backend/js/plugin/jquery-autocomplete/src/jquery.autocomplete.js')}}"></script> 
 

<script type="text/javascript">

    /**
     * Auto Complete
     * see AdminP2EquityTradingReport :: "SELECT SYMBOL, COMP_NAME FROM TBL_P2_EQUITY_INDEX ORDER BY SYMBOL ASC";
     */
    var symbols = { 
        @foreach($symbols as $item)
            '{{$item->SYMBOL}}' : '{{$item->SYMBOL}}', 
        @endforeach
    }

    // START: EChart 
    /**
     * EChart theme
     */
    var theme1 = {
    backgroundColor: '#FFFFFF',//'#F2F2E6',
    //  
    color: [
        '#04b8ce','#d32f2f','#8AED35','#ef3430','#8AED35','#30b193','#8AED35','#F4E24E','#FE9616','#ff69b4',
        '#E42B6D','#ba55d3','#cd5c5c','#ffa500','#40e0d0',
        '#E95569','#ff6347','#7b68ee','#00fa9a','#ffd700',
        '#6699FF','#ff6666','#3cb371','#b8860b','#30e0e0'
    ],

    //  
    title: {
        //backgroundColor: '#FFFFFF', //'#F2F2E6',
        itemGap: 0 ,                
        textStyle: {
           // color: '#000', //'#8A826D',
            fontFamily : 'DB Ozone X'
        },
        subtextStyle: {
            color: '#8A826D',//'#E877A3',
            fontFamily : 'DB Ozone X'           
        }
    },

    dataRange: {
        x:'right',
        y:'center',
        itemWidth: 5,
        itemHeight:25,
        color:['#E42B6D','#F9AD96'],
        text:['高','低'],          
        textStyle: {
            color: '#8A826D'           
        }
    },

    toolbox: {
        color : ['#E95569','#E95569','#E95569','#E95569'],
        effectiveColor : '#ff4500',
        itemGap: 8
    },

    tooltip: {
        backgroundColor: 'rgba(100,100,79,0.8)', //'rgba(138,130,109,0.5)',      
        axisPointer : {             
            type : 'line',          
            lineStyle : {           
                color: '#6B6455',
                type: 'dashed'
            },
            crossStyle: {           
                color: '#A6A299'
            },
            shadowStyle : {                      
                color: 'rgba(200,200,200,0.3)'
            }
        }
    },

 
    dataZoom: {
        dataBackgroundColor: 'rgba(130,197,209,0.5)',            
        fillerColor: 'rgba(233,84,105,0.1)',    
        handleColor: 'rgba(107,99,84,0.8)'     
    },

     
    grid: {
        borderWidth:0
    },

   
    categoryAxis: {
        axisLine: {             
            lineStyle: {        
                color: '#6B6455'
            }
        },
        splitLine: {            
            show: false
        }
    },

     
    valueAxis: {
        axisLine: {             
            show: false
        },
        splitArea : {
            show: false
        },
        splitLine: {            
            lineStyle: {        
                color: ['#FFF'],
                type: 'dashed'
            }
        }
    },

    polar : {
        axisLine: {             
            lineStyle: {        
                color: '#ddd'
            }
        },
        splitArea : {
            show : true,
            areaStyle : {
                color: ['rgba(250,250,250,0.2)','rgba(200,200,200,0.2)']
            }
        },
        splitLine : {
            lineStyle : {
                color : '#ddd'
            }
        }
    },

    timeline : {
        lineStyle : {
            color : '#6B6455'
        },
        controlStyle : {
            normal : { color : '#6B6455'},
            emphasis : { color : '#6B6455'}
        },
        symbol : 'emptyCircle',
        symbolSize : 3
    },

    // 柱形图默认参数
    bar: {
        itemStyle: {
            normal: {
                barBorderRadius: 0
            },
            emphasis: {
                barBorderRadius: 0
            }
        }
    },

 
    line: {
        smooth : true,
        symbol: 'emptyCircle',   
        symbolSize: 3            
    },


     
    k: {
        itemStyle: {
            normal: {
                color: '#E42B6D',       
                color0: '#44B7D3',       
                lineStyle: {
                    width: 1,
                    color: '#E42B6D',    
                    color0: '#44B7D3'    
                }
            }
        }
    },

    scatter: {
        itemStyle: {
            normal: {
                borderWidth:1,
                borderColor:'rgba(200,200,200,0.5)'
            },
            emphasis: {
                borderWidth:0
            }
        },
        symbol: 'circle',   
        symbolSize: 4        
    },

     
    radar : {
        symbol: 'emptyCircle',     
        symbolSize:3 
    },

    map: {
        itemStyle: {
            normal: {
                areaStyle: {
                    color: '#ddd'
                },
                label: {
                    textStyle: {
                        color: '#E42B6D'
                    }
                }
            },
            emphasis: {                 
                areaStyle: {
                    color: '#fe994e'
                },
                label: {
                    textStyle: {
                        color: 'rgb(100,0,0)'
                    }
                }
            }
        }
    },

    force : {
        itemStyle: {
            normal: {
                nodeStyle : {
                    borderColor : 'rgba(0,0,0,0)'
                },
                linkStyle : {
                    color : '#6B6455'
                }
            }
        }
    },

    chord : {
        itemStyle : {
            normal : {
                chordStyle : {
                    lineStyle : {
                        width : 0,
                        color : 'rgba(128, 128, 128, 0.5)'
                    }
                }
            },
            emphasis : {
                chordStyle : {
                    lineStyle : {
                        width : 1,
                        color : 'rgba(128, 128, 128, 0.5)'
                    }
                }
            }
        }
    },

    gauge : {                  
        center:['50%','80%'],
        radius:'100%',
        startAngle: 180,
        endAngle : 0,
        axisLine: {            
            show: true,        
            lineStyle: {       
                color: [[0.2, '#44B7D3'],[0.8, '#6B6455'],[1, '#E42B6D']],
                width: '40%'
            }
        },
        axisTick: {            
            splitNumber: 2,    
            length: 5,         
            lineStyle: {       
                color: '#fff'
            }
        },
        axisLabel: {           
            textStyle: {       
                color: '#fff',
                fontWeight:'bolder'
            }
        },
        splitLine: {           
            length: '5%',      
            lineStyle: {       
                color: '#fff'
            }
        },
        pointer : {
            width : '40%',
            length: '80%',
            color: '#fff'
        },
        title : {
          offsetCenter: [0, -20],        
          textStyle: {        
            color: 'auto',
            fontSize: 20
          }
        },
        detail : {
            offsetCenter: [0, 0],       
            textStyle: {        
                color: 'auto',
                fontSize: 40
            }
        }
    }
};

    // END: EChart theme

    // checked menu
    var menus = []; 

    (function($)  {
    $.fn.extend({
      check : function()  {
         return this.filter(":checkbox").attr("checked", true);
      },
      uncheck : function()  {
         return this.filter(":checkbox").removeAttr("checked");
      }
    });
    }(jQuery));

    $(document).ready(function(){


        $('#export_search').on('click',function() {
            // ensure dropdown closed 
            // hide dropdown menu
            $("body").trigger("click");

            var name_sht    = $('#securities_name').val();   // ชื่อบริษัท จัดการ "UOBAM" or "KTAM"
            var symbol      = $('#symbol').val();            // ชื่อตราสาร symbol eg. "APCS"
            var year        = $('#years').val();
            var bondrange   = $('#bondrange').val();

            var array       = symbol.split("|");           
            if(array.length > 0)
               symbol = array[0].trim();
            var age         = $('#age').val();               // อายุตราสาร (age 0-5)
            var broker      = $('#broker').val();            // บริษัท broker
            var industrial  = $('#industrial').val();        // CATE_ID --> 21 
            var date_start  = $('#hd_date_start').val();     // start date
            var date_end    = $('#hd_date_end').val();       // end date  


            window.location.href =  "BondBenefitsReport/exportsearch?symbol=" + symbol +"&name_sht=" + name_sht +
                                    "&age=" + age +"&bondrange=" + bondrange +   
                                    "&broker=" + broker +
                                    "&date_start=" +date_start + "&date_end=" + date_end+ 
                                    "&industrial=" + industrial;
            return false;
        });

        meaDatepicker("date_start","date_end");
        meaDatepicker("date_end");

        $('html').append('<input type="hidden" value="1" id="currentpage_search" />');

        $('#btn_search').on('click',function(e) {
          
            // hide dropdown menu
            $("body").trigger("click");
           
            // e.preventDefault();
            showByFlag(document.getElementById('page-size-search').id, false);
            showByFlag(document.getElementById('widget_search_table').id, true); 
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
           
            /* showhideByFlag(document.getElementById('trading-date').id, false);
                // showhideByFlag(document.getElementById('mea_chart2').id, false);
                // showhideByFlag(document.getElementById('wid-id-0').id, true);
            */
            var PageSizeAll = $('#page-size-search').val();
            var name_sht    = $('#securities_name').val();   // ชื่อบริษัท จัดการ "UOBAM"
            var symbol      = $('#symbol').val();            // ชื่อตราสาร symbol eg. "APCS"
            var bondrange   = $('#bondrange').val();         // ช่วงกำไรขาดทุน 
            var broker      = $('#broker').val();            // ชื่อบริษัท Broker
            var age         = $('#age').val();               // อายุตราสาร
            var year        = $('#years').val();               // อายุตราสาร
            var array       = symbol.split("|"); 

            if(array.length > 0)
               symbol = array[0].trim();
            // ประเภทตราสาร (use var industrial)
            var industrial  = $('#industrial').val();        // ประเภทตราสาร CATE_ID --> 21 
            //alert(industrial  + ', name_sht=' + name_sht  + 'symbol=' + symbol );
            
            // @@ IMPORTANT: don't use #date_start @@ //
            var date_start = $('#hd_date_start').val();      //  ระบุช่วงเวลา Start and End Date
            var date_end = $('#hd_date_end').val();

           
            /*  if(check_date && date_start != "" && date_end != "" ){
                var str =  'ในช่วงวันที่ ' + GetDateFormat(date_start) + ' ถึง ' + GetDateFormat(date_end);
                $('.report-period').html(str);
                }
            */
            var jsondata = {
                pagesize : PageSizeAll,
                PageNumber:1,

                symbol : symbol,
                name_sht : name_sht,
                broker: broker,
                year: year,
                age: age,
                bondrange: bondrange,
                industrial : industrial,
        
                date_start:date_start,
                date_end:date_end
                // 
                // check_name :check_name,
                // check_depart:check_depart,
                // check_plan:check_plan,
                // check_date:check_date
            };

            console.log(jsondata);
           
            MeaAjax(jsondata,"BondBenefitsReport/search", RenderSearch);

            
            return false;

        });


        $("#page-size-search").on('change',function() {
            // hide dropdown menu
            $("body").trigger("click");

            showByFlag(document.getElementById('widget_bar_chart').id, false);
            showByFlag(document.getElementById('widget_search_table').id, true);
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            var val = $(this).val();
            var name_sht    = $('#securities_name').val();   // ชื่อบริษัท จัดการ "UOBAM"
            var symbol      = $('#symbol').val();            // ชื่อตราสาร symbol eg. "APCS"
            var year        = $('#years').val();
            var bondrange   = $('#bondrange').val();
            var broker      = $('#broker').val();            // ชื่อบริษัท Broker
            var age         = $('#age').val();               // อายุตราสาร
            var array       = symbol.split("|"); 

            if(array.length > 0)
               symbol = array[0].trim();
            // ประเภทตราสาร (use var industrial)
            var industrial  = $('#industrial').val();        // ประเภทตราสาร CATE_ID --> 21 
            
            // @@ IMPORTANT: don't use #date_start @@ //
            var date_start = $('#hd_date_start').val();      //  ระบุช่วงเวลา Start and End Date
            var date_end = $('#hd_date_end').val();

           

            var jsondata = {
                pagesize : val,
                PageNumber:1,
                symbol : symbol,
                name_sht : name_sht,
                broker: broker,
                year: year,
                age: age,
                industrial : industrial,
                date_start:date_start,
                date_end:date_end
            };

            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(jsondata,"BondBenefitsReport/search", RenderSearch);

        });


        // <!-- Toggle -->
        $('#btn_chart').on('click',function(e) { 
            
            //$('#btn-cancel-settings').closest(".dropdown-menu").prev().dropdown("toggle");
            $("body").trigger("click");


            showByFlag(document.getElementById('page-size-search').id, false);
            showByFlag(document.getElementById('widget_search_table').id, false); 
            
             var name_sht   = $('#securities_name').val();   // ชื่อบริษัท จัดการ "UOBAM"
            var symbol      = $('#symbol').val();            // ชื่อตราสาร symbol eg. "APCS"
            var year        = $('#years').val();
            var bondrange   = $('#bondrange').val();
            var broker      = $('#broker').val();            // ชื่อบริษัท Broker
            var age         = $('#age').val();               // อายุตราสาร
            var array       = symbol.split("|"); 

            if(array.length > 0)
               symbol = array[0].trim();
            // ประเภทตราสาร (use var industrial)
            var industrial  = $('#industrial').val();        // ประเภทตราสาร CATE_ID --> 21 
            
            // @@ IMPORTANT: don't use #date_start @@ //
            var date_start = $('#hd_date_start').val();      //  ระบุช่วงเวลา Start and End Date
            var date_end = $('#hd_date_end').val();

           

            var jsondata = {
                pagesize : val,
                PageNumber:1,
                symbol : symbol,
                name_sht : name_sht,
                broker: broker,
                year: year,
                age: age,
                industrial : industrial,
                date_start:date_start,
                date_end:date_end
            };

            console.log(jsondata);
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(jsondata,"BondBenefitsReport/chartsearch", RenderChart);

    
            if(document.getElementById('mea_chart').id) {
                showByFlag(document.getElementById('page-size-search').id, false);
                showByFlag(document.getElementById('widget_search_table').id, false);
                showByFlag(document.getElementById('widget_bar_chart').id, true);
            }
             
            return false;
        });  // btn_chart

        /**
         * Cutomize Table Header
         */
        
        $( '.dropdown-menu a' ).on( 'click', function( event ) {
            event.preventDefault(); // Stop navigation
            var $target = $( event.currentTarget ),
               val = $target.attr( 'data-value' ),
               $inp = $target.find( 'input' ),
               idx;

            if ( ( idx = menus.indexOf( val ) ) > -1 ) {
                menus.splice( idx, 1 );
                setTimeout( function() { 
                    
                    //selectall
                    if(val === 'selectall') {
                       $inp.prop( 'checked', false );
                       menus = [];
                        for(var xx=0; xx< 16; xx++) { 
                            $('#option'+ xx).prop('checked', false);
                        }
                    } else {
                        $inp.prop( 'checked', false );
                    }
              }, 0);
            } else {
                if(val !== undefined) {
                    if(val === 'selectall') {
                        // alert('selectall');
                         menus = [];
                         menus.push( val );
                         // alert('ooo');

                         setTimeout( function() { 
                            $inp.prop( 'checked', true );
                            for(var xx=0; xx< 16; xx++) { 
                               $('#option'+ xx).prop('checked', true);
                               menus.push('option'+ xx);
                            }
                          }, 0);
                       
                    } else {
                         menus.push( val );
                         // alert('ooo');

                         setTimeout( function() { 
                            $inp.prop( 'checked', true );
                            
                          }, 0);
                    }
                } 
            }

            $( event.target ).blur();
              
            console.log( menus );
            return false;
        });

        /*
        $('#btn-save-settings').on('click', function(e) {
           e.preventDefault();
           $(this).closest(".dropdown-menu").prev().dropdown("toggle");
           var jsondata = {
                menu_id : 63,
                submenu_id :3,
                options : menus};
                
           MeaAjax(jsondata,"BondTradingReport/saveheader", RenderSaveSettings);
           
        });

        $('#btn-cancel-settings').on('click', function(e) {
            //$('.dropdown-menu').toggle();
             $(this).closest(".dropdown-menu").prev().dropdown("toggle");
        });
            
        
        // POST to read a current settings
        var jsondata = {
                menu_id : 63,
                submenu_id :3,
                options : menus};
                
        MeaAjax(jsondata,"BondBenefitsReport/readheader", RenderReadSettings);
        */

        // Initialize autocomplete with custom appendTo:
        var symbolsArray = $.map(symbols, function (value, key) { return { value: value, data: key }; });
        $('#symbol').autocomplete({
            lookup: symbolsArray
        });

    });  // jquery ready

    function RenderSaveSettings(json) {
        // TODO: handle settings results
    }

    function RenderReadSettings(json) {
        // clear menus
        menus = [];
        var options= json.result[ 0 ].options;
        if(options !== null) { 
            var size = options.filter(function(value) { return value !== undefined }).length;
            if(size < 1) return;
            for(var i = 0; i < size ; i++) {
               console.log('RenderReadSettings() : ' + options[i]);
               $('#'+options[i]).prop('checked', true);
               menus.push(options[i]);
            }
        }
    }


    
    function RenderChart(json) {

        var title    = '{{getMenutitle($arrSidebar)}}';
        
        console.log(json.result);
        var subtitle = ''; 
        if(json.result.date_start && json.result.date_end && (json.result.date_start != json.result.date_end)) {
            subtitle = 'ข้อมูล ระหว่าง วันที่ ' + json.result.date_start + ' ถึง ' + json.result.date_end;
        } else if(json.result.date_start){
            subtitle = 'ข้อมูล วันที่ ' + json.result.date_start + ' เวลา 00:00 ถึง 23:59';
        } else {
            subtitle = 'รายงานข้อมูล ณ วันที่ {{get_date_notime(date("Y-m-d H:i:s"))}}';
        }

        subtitle   = json.result.pretty_date;
        console.log('DATASOURCE=' + json.result.data);
        renderBenefitsChart1('mea_chart', title , subtitle, json.result.data);

         
       /* showhideByFlag(document.getElementById('widget_bar_chart').id, false);
        showhideByFlag(document.getElementById('trading-date').id, false);
        showhideByFlag(document.getElementById('mea_chart').id, false);*/
  
    }

    function RenderSearch(data){
        
        $("#serch_data").html(data.html);
        $("#serch_data").fadeIn('300');

        $("#page_click_search li a").on('click',PageRenderSearch);
         
        showByFlag(document.getElementById('widget_bar_chart').id, false); 
        showByFlag(document.getElementById('page-size-search').id, true);
        showByFlag(document.getElementById('widget_search_table').id, true); 
/*
        var table = $('#trading_table');
         // Get the column API object
        var column = $('#col0').attr('data-column');
 
        // Toggle the visibility
        column.visible( ! column.visible() );

        column = $('#col1').attr('data-column') );
 
        // Toggle the visibility
        column.visible( ! column.visible() );
  */
        /* remove Chart */
       /* var element = document.getElementById("mea_chart");
        element.outerHTML = "";
        delete element;
         */    
       // RenderChart();
    }

    function PageRenderSearch(){

        var p = $(this).attr('data-page');
        var page_size = $('#page-size-search').val();
        var CurPage = $('#currentpage_search').val();
        $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');

        if(p == "pre"){
            p = parseInt(CurPage) - 1;
        }

        if(p == "next"){
            p = parseInt(CurPage) + 1;
        }

        $('#currentpage_search').val(p);

        var name_sht    = $('#securities_name').val();   // ชื่อบริษัท จัดการ "UOBAM"
        var symbol      = $('#symbol').val();            // ชื่อตราสาร symbol eg. "APCS"
        var broker      = $('#broker').val();            // ชื่อบริษัท Broker
        var year        = $('#years').val();
        var age         = $('#age').val();               // อายุตราสาร
        var date_start  = $('#hd_date_start').val();
        var date_end    = $('#hd_date_end').val();

        var jsondata = {
            pagesize : page_size,
            PageNumber:p,
            symbol : symbol,
            broker :broker,
            year: year,
            age : age,
            date_start:date_start,
            date_end:date_end
        };

        MeaAjax(jsondata,"BondBenefitsReport/search",RenderSearch);
    };

    ////////////////////
    // TOOLS 
    function showhide(id) {
        var e = document.getElementById(id);
        e.style.display = (e.style.display == 'block') ? 'none' : 'block';
    }

 
    function showhideByFlag(id, flag) {
        var e = document.getElementById(id);
        e.style.display = flag ? 'block': 'none' ;

    }
    function showByFlag(id, flag) {
        var e = document.getElementById(id);
        e.style.display = flag ? 'block': 'none' ;

    }

    /*-----------------------------------------------------------------------------------*/
    /*  Hourly Chart
    /*-----------------------------------------------------------------------------------*/
    /*
    // BAR charts 
                    handleDashFlotChart(start_hour, 
                                        json.results.barchart_good, 
                                        json.results.barchart_bad);
    */


    
    function renderBenefitsChart1(tagid, title, subtitle, datasource) //, trading_label, trading_data) 
    {
        var myChart1;
        var domCode = document.getElementById('sidebar-code');
        var domGraphic = document.getElementById('graphic');
        var domMain = document.getElementById(tagid);
        var domMessage = document.getElementById('wrong-message');
        var iconResize = document.getElementById('icon-resize');
        var needRefresh = false;
       
        var idx = 1;
        var isExampleLaunched;
        var curTheme;
        var option;

        var data1 = [];
        var data2 = [];

       
        var trading_label = [];
        for(var i=0; i<datasource.length; i++) {
           trading_label.push(datasource[i].INDUSTRIAL);
           // convert to ล้านบาท
           data1.push( (datasource[i].J / 1000000).toFixed(2) );
           data2.push( (datasource[i].N / 1000000).toFixed(2) );
        }
       
        var yasix_label = 'จำนวณ (ล้านบาท)';
        var captions = ['','ม.ค.','ก.พ','มี.ค.', 'เม.ย', 'พ.ค.', 'มิ.ย.',
                        'ก.ค','ส.ค.','ก.ย.','ต.ค.','พ.ย','ธ.ค.',
                        'รวม'
                       ];
        option = {
            title : {
                text: title,
                subtext: subtitle, 

                x:'center',
                textStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 23
                },
                subtextStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 18
                }
            },

            tooltip : {
                trigger: 'axis',
                axisPointer : {             
                    type : 'shadow'        //  'line' | 'shadow'
                },
                formatter: function (params){
                     return params[0].name + '<br/>'
                     + params[0].seriesName + ' : ' + (params[0].value-0).toFixed(2).toLocaleString() + ' ล้านบาท <br/>'
                     + params[1].seriesName + ' : ' + (params[1].value-0).toFixed(2).toLocaleString() + ' ล้านบาท';
                }
               
               // formatter: "{a} <br/>{b} : {c} ( {d}% )"
               /* formatter : function (params) {               
                                    if (params.name == equity_label[equity_label.length - 1]) {          
                                       return  params.name + '<br/>' + params.value + ' : ( -' + (params.percent - 0).toFixed(2) + '% )'; 
                                        //params.name + ' ( ' +  (params.percent - 0).toFixed(0) + '% )';
                                    } else {
                                        //var total = equity_data[0].value + equity_data[1].value + equity_data[2].value;

                                        //var v = params.value  + (equity_data[3].value / 3);
                                        var pc = (params.value * 100) / total;  //+ (equity_label[3].value / 3);
                                        return params.name + ' = ' + params.value + ' : ( ' + (pc).toFixed(2) + '% )';
                                    }
                                }*/
            },

            label : {
                
                formatter: function (value){
                     return (value).toFixed(2).toLocaleString() + ' ล้านบาท';
                     //+ params[0].seriesName + ' : ' + (params[0].value-0).toFixed(2).toLocaleString() + 'ล้านบาท <br/>'
                     //+ params[1].seriesName + ' : ' + (params[1].value-0).toFixed(2).toLocaleString() + 'ล้านบาท';
                }
            },

            grid: {
               borderColor: '#ccc'
            },

            legend: {
                //orient : 'vertical',
                x : 'left',
                y : 'top',
                textStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 14
                },
                data: ['ซื้อ','ขาย'] 
            },

            toolbox: {
                    show : true,
                    orient : 'vertical',
                    x: 'right', 
                    y: 'top',
                    feature : {
                        mark : {show: false},
                        dataView : {show: true, readOnly: true},
                        magicType : {
                            show: true, 
                            type: ['line', 'bar'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1700
                                }
                            }
                        },
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },

            //calculable : true,

            xAxis : [
                {
                    name :'หมวดหมู่ธุรกิจ', 
                    nameTextStyle: {
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        },
                    type : 'category',
                    show : true,
                    data : trading_label,  

                    axisLine: {
                        show : true
                    },
                    axisTick: {
                        show: true,
                        interval : 0,
                        length: 1
                    },
                    axisLabel: {
                        show: true,
                        interval: 0,
                        textStyle: {
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        }
                    },

                }
            ],

            yAxis : [
                {
                    name : 'จำนวณเงิน (ล้านบาท)',
                    nameTextStyle: {
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        },

                    type : 'value',
                    show : true,
                    boundaryGap: [0, 0.1],
                    
                    axisLabel: {
                        show: true,
                        interval: 0,
                        textStyle: {
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        }
                    },
                }
            ],

            series : [
                {
                    name: 'ซื้อ',
                    type: 'bar',
                    data: data1,
                    barWidth: 30, 
                    barGap:"10%",
    
                    formatter: function (value) {
                        return (value).toFixed(2).toLocaleString() + ' ล้านบาท';
                    },
                    markPoint : {
                        data : [
                            {type : 'max', name: 'ค่าสูงสุด'},
                            {type : 'min', name: 'ค่าต่ำสุด'}
                        ],

                        itemStyle: {
                            normal: {

                                label : {
                                    show: true, 
                                    // position: 'top', //'insideTop'
                                    textStyle: {
                                        //color: '#04b8ce',
                                        fontFamily : 'DB Ozone X',
                                        fontSize : 14
                                    }
                                }
                            }
                        },
                    },

                    itemStyle: {
                        normal: {
                            
                            label : {
                                
                                show: true, 
                                position: 'top', //'insideTop'
                                textStyle: {
                                    color: '#04b8ce',
                                    fontFamily : 'DB Ozone X',
                                    fontSize : 14
                                }
                            }
                        }
                    },
                    
                },
                {
                    name: 'ขาย',
                    type: 'bar',
                    data: data2,
                    barWidth: 30,
                    barGap:"10%",
                    formatter: function (value) {
                        return (value).toFixed(2).toLocaleString() + ' ล้านบาท';
                    },
                    markPoint : {
                        data : [
                            {type : 'max', name: 'ค่าสูงสุด'},
                            {type : 'min', name: 'ค่าต่ำสุด'}
                        ],

                        itemStyle: {
                            normal: {
                                label : {
                                    show: true, 
                                   // position: 'top', //'insideTop'
                                    textStyle: {
                                        //color: '#04b8ce',
                                        fontFamily : 'DB Ozone X',
                                        fontSize : 14
                                    }
                                }
                            }
                        },
                    },

                    itemStyle: {
                        normal: {
                            //color: 'tomato',
                            //barBorderColor: 'tomato',
                            //barBorderWidth: 6,
                            //barBorderRadius:0,
                            label : {
                                show: true, 
                                position: 'top'
                                /*function(x, y) {
                                     if (y >0) {
                                        return 'top';
                                     } else {
                                        return 'insideTop';
                                     }
                                }*/
                                
                                , //'insideTop'
                                textStyle: {
                                    color: '#d32f2f',
                                    fontFamily : 'DB Ozone X',
                                    fontSize : 14
                                }
                            }
                        }
                    },
                }
            ]

        }; // EChart option

        myChart1 = echarts.init(document.getElementById(tagid));
        myChart1.setOption(option);
  
        curTheme = theme1;
        myChart1.setTheme(curTheme);
    }

    var showBarChart = function () {

        function drawBarChart() { 
            var data1 = [], 
            data2 = [], 
            n = 8,  
            i = 0,
            st = n,
            ticks = [];

            var labels = []; 
            var suffix = 'ล้านบาท';
            var yasix_label = 'จำนวณ (ล้านบาท)';
    
            for (i=0; i<12; i++) {
                ticks.push(i);
            }

            labels.push('พลังงาน');
            labels.push('ท่องเที่ยว');
            labels.push('บริการ');
            labels.push('ทรัพยากร');
            labels.push('ธุรกิจการเงิน');
            labels.push('สินค้าอุตสาหกรรม');
            labels.push('อสังหาริมทรัพย์');
            labels.push('เกษตร');
            labels.push('เทคโนโลยี');
            labels.push('อุตสาหกรรมอาหาร');
            labels.push('ก่อสร้าง');
            labels.push('อุปโภคบริโภค');


            data1.push([ticks[0],  20]);
            data1.push([ticks[1],  30]);
            data1.push([ticks[2],  60]);
            data1.push([ticks[3],  -40]);
            data1.push([ticks[4],  80]);
            data1.push([ticks[5],  40]);
            data1.push([ticks[6],  53]);
            data1.push([ticks[7],  23]);
            data1.push([ticks[8],  15]);
            data1.push([ticks[9],  17]);
            data1.push([ticks[10], -97]);
            data1.push([ticks[11], 86]);

            data2.push([ticks[0],  40]);
            data2.push([ticks[1],  60]);
            data2.push([ticks[2],  70]);
            data2.push([ticks[3],  -80]);
            data2.push([ticks[4],  70]);
            data2.push([ticks[5],  20]);
            data2.push([ticks[6],  33]);
            data2.push([ticks[7],  13]);
            data2.push([ticks[8],  45]);
            data2.push([ticks[9],  87]);
            data2.push([ticks[10], 57]);
            data2.push([ticks[11], 26]);

            var plot = $.plot($("#trading-bar-chart"), [{
                    data: data1,
                    bars: {
                        show: true,
                        barWidth: 0.28,
                        fill: true,
                        fillColor : '#02a6ba', //'#8bc34a', //
                        //align: "right",
                        lineWidth: 1,
                        order: 1
                    },
                    label: "ซื้อ"

                }, {
                    data: data2,
                    bars: {
                        show: true,
                        barWidth: 0.28,
                        fill: true,
                        fillColor : '#d32f2f',
                        //align: "left",
                        lineWidth: 1,
                        order: 2
                    },
                    label: "ขาย" 

                }], {
                    series: {
                        bars: {
                             barWidth: 0.25,
                            // align: 'right',
                             show: true
                            // order: 2
                    }
                },
                // Legend
                colors: ['#04b8ce','#d32f2f','#8bcE4a',"#FE0E1C", '#d32f2f','#e91e63', 
                         '#FF5733','#0033FF', "#FF02FC0", "#FE0E1C"],


                grid: {
                    hoverable: true,
                    clickable: true,
                    borderWidth: 0.1,
                    markings: [
                        { color: '#12ea15', lineWidth: 1, yaxis: { from: 800, to: 800 } },
                        { color: '#ea1215', lineWidth: 1, yaxis: { from: 400, to: 400 } }
                    ]
                },

                legend: {
                    show: true
                },

                xaxis: {
                    autoscaleMargin: .035,
                    mode: "categories",
                    tickLength: 0,
                    ticks: ticks,
                    tickDecimals: 0,
                    tickColor: "#e1e1e1",
                    tickFormatter: function (x) { 
                    /*    if (x > 23)  {
                            return (x - 24).toFixed(2).replace(".", ":")  ;
                        } else  { 
                            return x.toFixed(2).replace(".", ":") ;
                        }*/ 
                        return labels[x];
                    }
                },

                yaxis: {
                    ticks: 12,
                    tickColor: "#eaeaea",
                    tickDecimals: 0,
                    min: 0,
                    tickFormatter: function(val, axis) { return val < axis.max ? val.toFixed(2) : yasix_label + '     ' + val.toFixed(2);
                    
                    }/*,
                    font:{
                      size:14,
                      style:"italic",
                      weight:"bold",
                      family:"sans-serif",
                      variant:"small-caps"
                    }*/
                    
                },

            });  //END:  $.plot

            function showTooltip(x, y, contents) {
                $('<div id="tooltip">' + contents + '</div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y + 5,
                    left: x + 10,
                    
                    'box-shadow': '0 0 10px #555',
                    padding: '8px',
                    color: '#fff',
                    'border-radius': '4px',
                    '-webkit-border-radius': '4px',
                    '-moz-border-radius': '4px',
                    'border-radius': '4px',
                    'background-color': '#333',
                    border: '1px solid #fff',
                    opacity: 0.82
                }).appendTo("body").fadeIn(200);
            }

            var previousPoint = null;

            $("#trading-bar-chart").bind("plothover", function (event, pos, item) {
                $("#x").text(pos.x.toFixed(2));
                $("#y").text(pos.y.toFixed(2));
                if (item) {
                    if (previousPoint != item.dataIndex) {
                        previousPoint = item.dataIndex;
                        $("#tooltip").remove();
                        var x = item.datapoint[0].toFixed(0),//.toFixed(2),
                            y = item.datapoint[1].toFixed(0);//.toFixed(2);
                        showTooltip(item.pageX, item.pageY, 
                            item.series.label + " - " + labels[x] + "=" + y + " " + suffix);
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            });
        }
        
        /* Run the charts */
        drawBarChart();
    }



</script>

@stop
