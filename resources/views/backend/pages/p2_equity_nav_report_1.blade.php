@extends('backend.layouts.default')
@section('content')
<?php
$data = getmemulist();
$arrSidebar =getSideBar($data);
?>

<!--link rel="stylesheet" href="{{asset('backend/js/plugin/chartist/chartist.min.css')}}"/> 
<link rel="stylesheet" href="{{asset('backend/js/plugin/chartist/ct-customize.css')}}">
<link rel="stylesheet" href="{{asset('backend/js/plugin/chartist/plugin/chartist-plugin-tooltip.css')}}"/-->
<!-- ECHART CSS -->

<!--link href="{{asset('backend/js/plugin/echart/asset/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{asset('backend/js/plugin/echart/asset/css/bootstrap.css')}}" rel="stylesheet"-->
<link href="{{asset('backend/js/plugin/echart/asset/css/carousel.css')}}" rel="stylesheet">


<style type="text/css">
 #graph_label .tickLabel{

    font-size: 100%;
 }
/*
 #flot-placeholder {
    width:650px;
    height:540px;
}*/
</style>
<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>

                {{getMenutitle($arrSidebar)}}

            </h1>
        </div>

    </div>


    <div class="row" id="widget-grid" >

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <ul class="nav nav-tabs pull-left in">

                        <li class="active">

                            <a data-toggle="tab" href="#hr1"> <i class="fa fa-lg fa-arrow-circle-o-down"></i> <span class="hidden-mobile hidden-tablet"> ระบุเงื่อนไขการค้นหา </span> </a>

                        </li>

                        <!--li>
                            <a data-toggle="tab" href="#hr2"> <i class="fa fa-lg fa-arrow-circle-o-up"></i> <span class="hidden-mobile hidden-tablet"> รายงานสรุปของตราสารทุน  </span> </a>
                        </li-->

                    </ul>
                </header>

                <!-- widget div-->
                <div>


                    <!-- widget content -->
                    <div class="widget-body">

                        <div class="tab-content">
                            <div class="tab-pane active" id="hr1">

                                <div class="widget-body no-padding" style="width: 100% ;margin: 0 auto;background-color: #000000;border: 1px solid #E1E8F3">
                                    <header style="color: #fff; height: 40px; line-height: 40px; font-size: 18px; background-color: #a90329;padding-left: 20px;"> กล่องค้นหา </header>

                                    <form  id="comment-form_darkman" class="smart-form" novalidate="novalidate">

                                        <fieldset>

                                            <div class="row" style="display:none;">
                                                <section class="col col-4">
                                                    <label class="label"><input type="checkbox" id="check_name"  value="name">   ชื่อหลักทรัพย์</label>
                                                    <label class="input">
                                                        <select name="plan" id="name" class="form-control">
                                                            <option value="">ชื่อหลักทรัพย์</option>
                                                            @if($equitylist)
                                                                @foreach($equitylist as $item)
                                                                    <option value="{{$item->NAME_SHT}}">{{$item->NAME_SHT}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </label>
                                                </section>
                                                <section class="col col-4">
                                                    <label class="label"><input type="checkbox" id="check_depart" value="depart">   หมวดหมู่หลักทรัพย์</label>
                                                    <label class="input">
                                                        
                                                        <select name="depart" id="depart" class="form-control">
                                                            <option value="">หมวดหมู่หลักทรัพย์</option>
                                                            @if($equitycategory)
                                                                @foreach($equitycategory as $item)
                                                                    <option value="{{$item->INDUSTRIAL}}">{{$item->INDUSTRIAL}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>

                                                    </label>
                                                </section>
                                            </div>

                                            <div class="row">
                                                <section class="col col-4">
                                                    <label class="label"><input type="checkbox" id="check_plan"  value="plan">     นโยบายการลงทุน</label>
                                                    <label class="input">
                                                        <select name="plan" id="plan" class="form-control">
                                                            <option value="">เลือกนโยบายการลงทุน</option>
                                                            @if($equitylist)
                                                                @foreach($equitylist as $item)
                                                                    <option value="{{$item->NAME_SHT}}">{{$item->NAME_SHT}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </label>
                                                </section>

                                                <section class="col col-2">
                                                    <label class="label"><input type="checkbox" id="check_month" value="selected_month">     ระบุช่วงเวลา </label>
                                                    <label class="input">
                                                        <select name="selected_month" id="selected_month" class="form-control">
                                                            <option value="">เดือน</option>
                                                            <option value="ม.ค." >ม.ค.</option>
                                                            <option value="ก.พ." >ก.พ.</option>
                                                            <option value="มี.ค.">มี.ค.</option>
                                                            <option value="เม.ย" >ม.ค.</option>
                                                            <option value="มิ.ย.">ม.ค.</option>
                                                            <option value="ก.ค." >ม.ค.</option>
                                                            <option value="พ.ค." >พ.ค.</option>
                                                            <option value="ส.ค." >ส.ค.</option>
                                                            <option value="ก.ย." >ก.ย.</option>
                                                            <option value="ต.ค." >ต.ค.</option>
                                                            <option value="พ.ย." >พ.ย.</option>
                                                            <option value="ธ.ค." >ธ.ค.</option>                                                 
                                                        </select>
                                                    </label>
                                                </section>

                                                <section class="col col-2">
                                                    <label class="label" ><br/></label>
                                                    <label class="input">
                                                        <select name="selected_year" id="selected_year" class="form-control">
                                                            <option value="">ปี</option> 
                                                            <option value="2550">2550</option>
                                                            <option value="2551">2551</option>
                                                            <option value="2552">2552</option>
                                                            <option value="2553">2553</option>
                                                            <option value="2554">2554</option>
                                                            <option value="2555">2555</option>
                                                            <option value="2556">2556</option>
                                                            <option value="2557">2557</option>
                                                            <option value="2558">2558</option>
                                                            <option value="2559">2559</option>
                                                            <option value="2560">2560</option>
                                                            <option value="2561">2561</option>
                                                            <option value="2562">2562</option>
                                                            <option value="2563">2563</option>
                                                            <option value="2564">2564</option>
                                                            <option value="2565">2565</option>
                                                            <option value="2566">2566</option>
                                                        </select>
                                                    </label>
                                                </section>

                                                <section class="col col-1">
                                                    <button type="submit" id="btn_search" style="padding: 5px 20px 5px 20px; margin-top:20px;font-size: 18px;" name="submit" class="btn btn-primary">
                                                        ค้นหา
                                                    </button>
                                                </section>
                                             </div>   

                                             <div class="row" >
                                                <section class="col col-4" style="display:none;">
                                                    <label class="label"><input type="checkbox" id="check_date" value="date_start"> ระบุช่วงเวลา</label>
                                                    <label class="input"><i class="icon-append fa fa-calendar"></i>
                                                        <input type="text" class="mea_date_picker" id="date_start"  >
                                                    </label>
                                                </section>
                                                <section class="col col-4" style="display:none;">
                                                    <label class="label">ถึง</label>
                                                    <label class="input"><i class="icon-append fa fa-calendar"></i>
                                                        <input type="text" class="mea_date_picker" id="date_end" >
                                                    </label>
                                                </section>
                                                
                                            </div>

                                        </fieldset>

                                    </form>

                                </div>


                                <a href="#" id="export_search" style="margin-top: 30px;" class="btn btn-labeled btn-success mea-btn-export"> <span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i></span>Export </a>
                                <!--a href="#" id="btn_customize" style="margin-top: 30px;" class="btn btn-labeled btn-warning"> <span class="btn-label"><i class="glyphicon glyphicon-list"></i></span>Customize </a>
                                <a href="#" id="btn_chart"     style="margin-top: 30px;" class="btn btn-labeled btn-info"> <span class="btn-label"><i class="glyphicon glyphicon-signal"></i></span>Chart </a-->
                               

                                <!-- Widget ID (each widget will need unique ID)-->
                                
                                <div class="jarviswidget jarviswidget-color-blueDark" style="margin-top: 5px; display:block;" 
                                     id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
                                    <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                    data-widget-colorbutton="false"
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true"export_search
                                    data-widget-sortable="false"

                                    -->

                                    <select class="form-control mea-pagesize" id="page-size-search" style="display:none;">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>

                                    <header>
                                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    </header>

                                    <!-- widget div-->
                                    <div >

                                        <!-- widget edit box -->
                                        <div class="jarviswidget-editbox">
                                            <!-- This area used as dropdown edit box -->

                                        </div>
                                        <!-- end widget edit box -->

                                        <!-- widget content -->
                                        <div class="widget-body">
                                               
                                            <p class="report-title">
                                                
                                            </p>

                                            <!--p class="report-period"> </p-->
                                            <div class="table-responsive" >
                                                <div id="serch_data">

                                                </div>
                                            </div>
                                            

                                        <!--div class="tab-pane" id="tab2">    
                                            
                                            <div name="pie-chart" id="pie-chart" class="chart" 
                                                 style="display:block;">
    
                                            </div>
                                        </div-->
                                        <!-- Chart -->
                                         
                                            <div class="table-responsive">
                                                <div name="pie-chart" id="pie-chart" class="chart" style="width: 100%; padding: 10px;">

                                                </div>
                                            </div>
                                                
                                            <div class="table-responsive">
                                                         
                                                <div id="mea_chart" class="mea_chart" style="width: 100%; padding: 10px;">
                                                                
                                                </div>
                                                 
                                            </div>
                                            
                                            <div class="table-responsive">
                                                         
                                                <div id="main_chart2" class="mea_chart" style="width: 100%; padding: 10px;">
                                                                
                                                </div>
                                                 
                                            </div>
                                        </div>
                                        <!-- end widget content -->

                                    </div>
                                    
                                    <!-- end widget div -->

                                </div>
                                <!-- end widget -->

                            </div>
                            

                        </div>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- WIDGET END -->

    </div>



</div>
<!-- END MAIN CONTENT -->

<!-- FLOT CHARTS -->
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.time.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.selection.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.resize.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.pie.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.stack.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.crosshair.min.js')}}"></script>
<script type="text/javascript" language="javascript" src="{{asset('backend/js/plugin/flot_p2/jquery.flot.orderBars.js')}}"></script>

<!-- flot tooltips plugin-->
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.tooltip.min.js')}}"></script>

<!-- FLOT GROWRAF -->
<script src="{{asset('backend/js/plugin/flot-growraf/jquery.flot.growraf.min.js')}}"></script>



<!-- ECHART english -->
<script src="{{asset('backend/js/plugin/echart/www/js/echarts-all-english-v2.js')}}"></script>
<!--script src="{{asset('backend/js/plugin/echart/theme/helianthus.js')}}"></script-->

<link rel="stylesheet" type="text/css" href="{{asset('backend/js/plugin/echart/www/js/style.css')}}" />

<!-- END: ECHART -->
<script type="text/javascript">
    ////////
    // theme
    //////

    var theme = {
    backgroundColor: '#FFFFFF',//'#F2F2E6',
    // 默认色板
    color: [
        '#44B7D3','#E42B6D','#F4E24E','#FE9616','#8AED35',
        '#ff69b4','#ba55d3','#cd5c5c','#ffa500','#40e0d0',
        '#E95569','#ff6347','#7b68ee','#00fa9a','#ffd700',
        '#6699FF','#ff6666','#3cb371','#b8860b','#30e0e0'
    ],

    // 图表标题
    title: {
        backgroundColor: '#FFFFFF', //'#F2F2E6',
        itemGap: 10
       ,                
        textStyle: {
            color: '#8A826D'          
        },
        subtextStyle: {
            color: '#E877A3'           
        }
    },

    // 值域
    dataRange: {
        x:'right',
        y:'center',
        itemWidth: 5,
        itemHeight:25,
        color:['#E42B6D','#F9AD96'],
        text:['高','低'],          
        textStyle: {
            color: '#8A826D'           
        }
    },

    toolbox: {
        color : ['#E95569','#E95569','#E95569','#E95569'],
        effectiveColor : '#ff4500',
        itemGap: 8
    },

     
    tooltip: {
        backgroundColor: 'rgba(138,130,109,0.7)',      
        axisPointer : {             
            type : 'line',          
            lineStyle : {           
                color: '#6B6455',
                type: 'dashed'
            },
            crossStyle: {           
                color: '#A6A299'
            },
            shadowStyle : {                      
                color: 'rgba(200,200,200,0.3)'
            }
        }
    },

 
    dataZoom: {
        dataBackgroundColor: 'rgba(130,197,209,0.6)',            
        fillerColor: 'rgba(233,84,105,0.1)',    
        handleColor: 'rgba(107,99,84,0.8)'     
    },

     
    grid: {
        borderWidth:0
    },

   
    categoryAxis: {
        axisLine: {             
            lineStyle: {        
                color: '#6B6455'
            }
        },
        splitLine: {            
            show: false
        }
    },

     
    valueAxis: {
        axisLine: {             
            show: false
        },
        splitArea : {
            show: false
        },
        splitLine: {            
            lineStyle: {        
                color: ['#FFF'],
                type: 'dashed'
            }
        }
    },

    polar : {
        axisLine: {             
            lineStyle: {        
                color: '#ddd'
            }
        },
        splitArea : {
            show : true,
            areaStyle : {
                color: ['rgba(250,250,250,0.2)','rgba(200,200,200,0.2)']
            }
        },
        splitLine : {
            lineStyle : {
                color : '#ddd'
            }
        }
    },

    timeline : {
        lineStyle : {
            color : '#6B6455'
        },
        controlStyle : {
            normal : { color : '#6B6455'},
            emphasis : { color : '#6B6455'}
        },
        symbol : 'emptyCircle',
        symbolSize : 3
    },

    // 柱形图默认参数
    bar: {
        itemStyle: {
            normal: {
                barBorderRadius: 0
            },
            emphasis: {
                barBorderRadius: 0
            }
        }
    },

 
    line: {
        smooth : true,
        symbol: 'emptyCircle',   
        symbolSize: 3            
    },


     
    k: {
        itemStyle: {
            normal: {
                color: '#E42B6D',       
                color0: '#44B7D3',       
                lineStyle: {
                    width: 1,
                    color: '#E42B6D',    
                    color0: '#44B7D3'    
                }
            }
        }
    },

    // 散点图默认参数
    scatter: {
        itemStyle: {
            normal: {
                borderWidth:1,
                borderColor:'rgba(200,200,200,0.5)'
            },
            emphasis: {
                borderWidth:0
            }
        },
        symbol: 'circle',    // 图形类型
        symbolSize: 4        // 图形大小，半宽（半径）参数，当图形为方向或菱形则总宽度为symbolSize * 2
    },

    // 雷达图默认参数
    radar : {
        symbol: 'emptyCircle',    // 图形类型
        symbolSize:3
        //symbol: null,         // 拐点图形类型
        //symbolRotate : null,  // 图形旋转控制
    },

    map: {
        itemStyle: {
            normal: {
                areaStyle: {
                    color: '#ddd'
                },
                label: {
                    textStyle: {
                        color: '#E42B6D'
                    }
                }
            },
            emphasis: {                 // 也是选中样式
                areaStyle: {
                    color: '#fe994e'
                },
                label: {
                    textStyle: {
                        color: 'rgb(100,0,0)'
                    }
                }
            }
        }
    },

    force : {
        itemStyle: {
            normal: {
                nodeStyle : {
                    borderColor : 'rgba(0,0,0,0)'
                },
                linkStyle : {
                    color : '#6B6455'
                }
            }
        }
    },

    chord : {
        itemStyle : {
            normal : {
                chordStyle : {
                    lineStyle : {
                        width : 0,
                        color : 'rgba(128, 128, 128, 0.5)'
                    }
                }
            },
            emphasis : {
                chordStyle : {
                    lineStyle : {
                        width : 1,
                        color : 'rgba(128, 128, 128, 0.5)'
                    }
                }
            }
        }
    },

    gauge : {                  
        center:['50%','80%'],
        radius:'100%',
        startAngle: 180,
        endAngle : 0,
        axisLine: {            
            show: true,        
            lineStyle: {       
                color: [[0.2, '#44B7D3'],[0.8, '#6B6455'],[1, '#E42B6D']],
                width: '40%'
            }
        },
        axisTick: {            
            splitNumber: 2,    
            length: 5,         
            lineStyle: {       
                color: '#fff'
            }
        },
        axisLabel: {           
            textStyle: {       
                color: '#fff',
                fontWeight:'bolder'
            }
        },
        splitLine: {           
            length: '5%',      
            lineStyle: {       
                color: '#fff'
            }
        },
        pointer : {
            width : '40%',
            length: '80%',
            color: '#fff'
        },
        title : {
          offsetCenter: [0, -20],        
          textStyle: {        
            color: 'auto',
            fontSize: 20
          }
        },
        detail : {
            offsetCenter: [0, 0],       
            textStyle: {        
                color: 'auto',
                fontSize: 40
            }
        }
    }
    /*,

    textStyle: {
        fontFamily: '微软雅黑, Arial, Verdana, sans-serif'
    }*/
};

    $(document).ready(function(){

        

        $('#export_search').on('click',function() {
            var PageSizeAll = $('#page-size-search').val();
            var EmpID = $('#name').val();
            var depart = $('#depart').val();
            var plan = $('#plan').val();
            var date_start = $('#hd_date_start').val();
            var date_end =$('#hd_date_end').val();

            var check_name =$('#check_name').is(':checked');
            var check_depart =$('#check_depart').is(':checked');
            var check_plan =$('#check_plan').is(':checked');
            var check_date =$('#check_date').is(':checked');

            window.location.href =  "EquityNavReport/exportsearch1?EmpID=" + EmpID +"&depart=" + depart +
                                    "&plan=" + plan + "&date_start=" +date_start + "&date_end=" + date_end+ 
                                    '&check_name=' + check_name+ '&check_depart=' + check_depart+ '&check_plan=' + 
                                    check_plan+ '&check_date=' + check_date;
            return false;
        });

        meaDatepicker("date_start","date_end");
        meaDatepicker("date_end");

        $('html').append('<input type="hidden" value="1" id="currentpage_search" />');

        $('#btn_search').on('click',function(e) {
            e.preventDefault();

            //showhideByFlag(document.getElementById('trading-date').id, false);
            //showhideByFlag(document.getElementById('trading-bar-chart').id, false);
            //showhideByFlag(document.getElementById('wid-id-0').id, true);

            var PageSizeAll = $('#page-size-search').val();
            var EmpID = $('#name').val();
            var depart = $('#depart').val();
            var plan = $('#plan').val();
            var date_start = $('#hd_date_start').val();
            var date_end =$('#hd_date_end').val();

            var check_name =$('#check_name').is(':checked');
            var check_depart =$('#check_depart').is(':checked');
            var check_plan =$('#check_plan').is(':checked');
            var check_date =$('#check_date').is(':checked');


            if(check_date && date_start != "" && date_end != "" ){
                var str =  'ในช่วงวันที่ ' + GetDateFormat(date_start) + ' ถึง ' + GetDateFormat(date_end);
                $('.report-period').html(str);
            }

            var jsondata = {
                pagesize : PageSizeAll,
                PageNumber:1,
                emp_id : EmpID,
                depart :depart,
                plan : plan,
                date_start:date_start,
                date_end:date_end,
                check_name :check_name,
                check_depart:check_depart,
                check_plan:check_plan,
                check_date:check_date

            };

            console.log(jsondata);
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(jsondata,"EquityNavReport/search1", RenderSearch);

            return false;
        });

        $("#page-size-search").on('change',function() {
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            var val = $(this).val();

            var EmpID = $('#name').val();
            var depart = $('#depart').val();
            var plan = $('#plan').val();
            var date_start = $('#hd_date_start').val();
            var date_end =$('#hd_date_end').val();

            var check_name =$('#check_name').is(':checked');
            var check_depart =$('#check_depart').is(':checked');
            var check_plan =$('#check_plan').is(':checked');
            var check_date =$('#check_date').is(':checked');

            var jsondata = {
                pagesize : val,
                PageNumber:1,
                emp_id : EmpID,
                depart :depart,
                plan : plan,
                date_start:date_start,
                date_end:date_end,
                check_name :check_name,
                check_depart:check_depart,
                check_plan:check_plan,
                check_date:check_date

            };
 
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(jsondata,"EquityNavReport/search1",RenderSearch);

        });


        $('#btn_chart').on('click',function(e) { 
            e.preventDefault();
            showhide(document.getElementById('pie-good-bad').id);

            $("body, html").animate({ 
                scrollTop: $( $(this).attr('href') ).offset().top 
            }, 600);
        });


        renderPieChart1('mea_chart');
        renderPieChart2('main_chart2');
          
    }); // jquery ready

    function RenderChart() {
        
        //renderPieChart1('main2');
        showhideByFlag(document.getElementById('pie-chart').id, true);
    }

    function RenderSearch(data){
        
        $("#serch_data").html(data.html);
        $("#serch_data").fadeIn('300');

        $("#page_click_search").on('click',PageRenderSearch);
/*
        showhideByFlag(document.getElementById('trading-chart-title').id, true);
        showhideByFlag(document.getElementById('widget_bar_chart').id, true);
        showhideByFlag(document.getElementById('trading-date').id, true);
        showhideByFlag(document.getElementById('trading-bar-chart').id, true);
        showBarChart();
        showhideByFlag(document.getElementById('trading-chart-title').id, false);
        showhideByFlag(document.getElementById('widget_bar_chart').id, false);
        showhideByFlag(document.getElementById('trading-date').id, false);
        showhideByFlag(document.getElementById('trading-bar-chart').id, false);
        */
        showPieChart();
    }

    function PageRenderSearch(){

        var p = $(this).attr('data-page');
        var page_size = $('#page-size-search').val();
        var CurPage = $('#currentpage_search').val();
        $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');

        if(p == "pre"){
            p = parseInt(CurPage) - 1;
        }

        if(p == "next"){
            p = parseInt(CurPage) + 1;
        }
        $('#currentpage_search').val(p);

        var EmpID = $('#name').val();
        var depart = $('#depart').val();
        var plan = $('#plan').val();
        var date_start = $('#hd_date_start').val();
        var date_end =$('#hd_date_end').val();

        var check_name =$('#check_name').is(':checked');
        var check_depart =$('#check_depart').is(':checked');
        var check_plan =$('#check_plan').is(':checked');
        var check_date =$('#check_date').is(':checked');


        var jsondata = {
            pagesize : page_size,
            PageNumber:p,
            emp_id : EmpID,
            depart :depart,
            plan : plan,
            date_start:date_start,
            date_end:date_end,
            check_name :check_name,
            check_depart:check_depart,
            check_plan:check_plan,
            check_date:check_date

        };

        MeaAjax(jsondata,"EquityNavReport/search1",RenderSearch);
    };

    ////////////////////
    // TOOLS 
    function showhide(id) {
        var e = document.getElementById(id);
        e.style.display = (e.style.display == 'block') ? 'none' : 'block';
    }

 
    function showhideByFlag(id, flag) {
        var e = document.getElementById(id);
        e.style.display = flag ? 'block': 'none' ;
    }

   
    function showPieChart() {
        var colors = [];
        var data = [];
        
        var t1 = 83.00;
        var t2 = -0.99;
        var t3 = 17.99;
        
        /* prepare data for 1st pie chart */
        data[0] = {
            label: "พันธบัตรรัฐบาล ธปท. และรัฐวิสาหกิจ",
            data: t1 
        }

        data[1] = {
            label: "เงินฝากและตราสารหนี้ ธนาคารพาณิชย์",
            data: t2
        }

        data[2] = {
            label: "อื่นๆ (ลูกหนี้และเจ้าหนี้จากการซื้อ/ขายหลักทรัพย์)",
            data: t3
        }
          
         
        $.plot($("#pie-chart"), data, {
            series: {
                pie: {
                    //innerRadius: 0.6,
                    show: true
                }
            },

            legend: {
                show: false
            },

            grid: {
                hoverable: true,
                clickable: true
            },

            tooltip: {
                show: true,
                content: " %s = %n % ", //(%p.0%) ", /* show percentages, rounding to 2 decimal places */
                shifts: {
                    x: 20,
                    y: 0
                },
                defaultTheme: false
            },      

            colors: ['#8bc34a', '#d32f2f','#028fa0','#e91e63','#FF5733',
                     '#FE0E1C', '#d32f2f','#e91e63'] 
        });
    }



    // ECHART

    function renderPieChart1(tagid) 
    {
    var myChart;
    var domCode = document.getElementById('sidebar-code');
    var domGraphic = document.getElementById('graphic');
    var domMain = document.getElementById(tagid);
    var domMessage = document.getElementById('wrong-message');
    var iconResize = document.getElementById('icon-resize');
    var needRefresh = false;
   
    var idx = 1;
    var isExampleLaunched;
    var curTheme;
    var option;

    var captions = ['','ม.ค.','ก.พ','มี.ค.', 'เม.ย', 'พ.ค.', 'มิ.ย.',
                    'ก.ค','ส.ค.','ก.ย.','ต.ค.','พ.ย','ธ.ค.',
                    'รวม'
                   ];
        option = {
        
        timeline : {
            data : [
                '2016-01-01', '2016-02-01', '2016-03-01', '2016-04-01', '2016-05-01', 
                { name:'2016-06-01', symbol:'emptyStar6', symbolSize:8 },
                '2016-07-01','2016-08-01', '2016-09-01', '2016-10-01', '2016-11-01',
                { name:'2016-12-01', symbol:'star6', symbolSize:8 }
            ],
            label : {
                formatter : function(s) {
                    return captions[parseInt(s.slice(5, 7))];
                }
            }
        },

        options : [
            {
                title : {
                    text: 'การลงทุนในตราสารทุน',
                    subtext: 'แบบรายดือน ปี 2559 ',
                    x:'center'
                },
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient : 'vertical',
                    x : 'left',
                    y : 'top',
                    data:['Chrome','Firefox','Safari','IE9+','IE8-']
                },
                toolbox: {
                    show : true,
                    feature : {
                        mark : {show: true},
                        dataView : {show: true, readOnly: true},
                        magicType : {
                            show: true, 
                            type: ['pie', 'funnel'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1700
                                }
                            }
                        },
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                // Jan
                series : [
                    {
                        name:'มกราคม',
                        type:'pie',
                        center: ['50%', '45%'],
                        radius: '50%',

                        itemStyle : {
                            normal : {
                                label : {
                                    position : 'outer',
                                    formatter : function (params) {                         
                                        return params.name + ' ( ' +  (params.percent - 0).toFixed(0) + '% )'
                                    }
                                },
                                labelLine : {
                                    show : true
                                }
                            },

                            emphasis : {
                                label : {
                                    show : false,
                                    formatter : "{b}\n{d}%"
                                }
                            }
                
                        },

                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'กุมพาพันธ์',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'มีนาคม',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'เมษายน',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'พฤษภาคม',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'มิถุนายน',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'กรกฎาคม',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'สิงหาคม',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'กันยายน',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'ตุลาคม',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'พฤศจิกายน',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'ธันวาคม',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            }
           ,
          
            // all months
            {
                series : [
                    {
                        name:'รวม',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            }
        ]
    };


    myChart = echarts.init(document.getElementById(tagid));
    myChart.setOption(option);
    curTheme = theme;
    myChart.setTheme(curTheme);

}

////
function renderPieChart2(tagid) 
    {
    var myChart2;
    var domCode = document.getElementById('sidebar-code');
    var domGraphic = document.getElementById('graphic');
    var domMain = document.getElementById(tagid);
    var domMessage = document.getElementById('wrong-message');
    var iconResize = document.getElementById('icon-resize');
    var needRefresh = false;
   
    var idx = 1;
    var isExampleLaunched;
    var curTheme;
    var option;
     
    var captions = ['','ม.ค.','ก.พ','มี.ค.', 'เม.ย', 'พ.ค.', 'มิ.ย.',
                    'ก.ค','ส.ค.','ก.ย.','ต.ค.','พ.ย','ธ.ค.',
                    'รวม'
                   ];
        option = {
            title : {
                text: 'การลงทุนในตราสารทุน',
                subtext: 'แบบรวม ปี 2559 ',
                x:'center'
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient : 'vertical',
                x : 'left',
                data:['直接访问','邮件营销','联盟广告','视频广告','搜索引擎']
            },
            //calculable : true,
            series : [
                {
                    name:'访问来源',
                    type:'pie',
                    radius : '50%',
                    center: ['50%', '45%'],
                    data:[
                        {value:335, name:'直接访问'},
                        {value:310, name:'邮件营销'},
                        {value:234, name:'联盟广告'},
                        {value:135, name:'视频广告'},
                        {value:1548, name:'搜索引擎'}
                    ]
                }
            ]
        };


    myChart2 = echarts.init(document.getElementById(tagid));
    myChart2.setOption(option);
   // curTheme = theme;
    

    curTheme = theme;
    myChart2.setTheme(curTheme);
}


</script>

@stop
