@extends('backend.layouts.default')
@section('content')
<?php
$data = getmemulist();
$arrSidebar =getSideBar($data);
?>
<style type="text/css">
/* show/hide table header */
.hiddencol { display:none; };
.showcol {display:none;};

/* Scroll Menu */
#meacontainer {
     position:absolute;
     width:100%;
     top:0;
     bottom:0;
     /*left:154;*/
     z-index:-1;
     overflow:hidden;
     background: #FF0000;
 }

 #dpsettings {
     overflow-y: scroll;
     width:245px;
     background: #FFFFFF;
     float:left;
     height:350px;
     margin-top:-5px;
     margin-left:154px
     position: absolute;
 }
 .loginBtn span {
    display: block;
    padding-top: 22px;
    text-align: center;
    line-height: 1em;
}​

</style>


<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>

                {{getMenutitle($arrSidebar)}}

            </h1>
        </div>

    </div>


    <div class="row" id="widget-grid" >

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <ul class="nav nav-tabs pull-left in">

                        <li class="active">

                            <a data-toggle="tab" href="#hr1"> <i class="fa fa-lg fa-arrow-circle-o-down"></i> <span class="hidden-mobile hidden-tablet"> ระบุเงื่อนไขการค้นหา </span> </a>

                        </li>

                        <!--li>
                            <a data-toggle="tab" href="#hr2"> <i class="fa fa-lg fa-arrow-circle-o-up"></i> <span class="hidden-mobile hidden-tablet"> รายงานสรุปของตราสารทุน  </span> </a>
                        </li-->

                    </ul>
                </header>

                <!-- widget div-->
                <div>


                    <!-- widget content -->
                    <div class="widget-body">

                        <div class="tab-content">
                            <div class="tab-pane active" id="hr1">

                                <div class="widget-body no-padding" style="width: 100% ;margin: 0 auto;background-color: #000000;border: 1px solid #E1E8F3">
                                    <header style="color: #fff; height: 40px; line-height: 40px; font-size: 18px; background-color: #a90329;padding-left: 20px;"> กล่องค้นหา </header>

                                    <form  id="comment-form_darkman" class="smart-form" novalidate="novalidate">

                                        <fieldset>
                                            

                                            <div class="row">
                                                <section class="col col-4">
                                                    <label class="label"><input type="checkbox" id="check_date" value="date_start"> ระบุช่วงเวลา</label>
                                                    <label class="input"><i class="icon-append fa fa-calendar"></i>
                                                        <input type="text" class="mea_date_picker" id="date_start"  >
                                                    </label>
                                                </section>
                                                <section class="col col-4">
                                                    <label class="label">ถึง</label>
                                                    <label class="input"><i class="icon-append fa fa-calendar"></i>
                                                        <input type="text" class="mea_date_picker" id="date_end" >
                                                    </label>
                                                </section>
                                                <section class="col col-4">
                                                    <button type="submit" id="btn_search" style="padding: 5px 20px 5px 20px; margin-top:20px;font-size: 18px;" name="submit" class="btn btn-primary">
                                                        ค้นหา
                                                    </button>
                                                </section>
                                            </div>
                                        </fieldset>




                                    </form>

                                </div>


                                <!--a href="#" id="export_search" style="margin-top: 30px;" class="btn btn-labeled btn-success mea-btn-export"> <span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i></span>Export </a>
                                <a href="#" id="btn_customize" style="margin-top: 30px;" class="btn btn-labeled btn-warning"> <span class="btn-label"><i class="glyphicon glyphicon-list"></i></span>Customize </a>
                                <a href="#" id="btn_chart"     style="margin-top: 30px;" class="btn btn-labeled btn-info"> <span class="btn-label"><i class="glyphicon glyphicon-signal"></i></span>Chart </a-->
                               
                                <!-- -->
                                <div id="meacontainer"></div>
                                <div class="container" style="margin-left:-12px ; margin-top: 0px;">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="button-group">
                                                <!--button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span></button-->
                                                 <a href="#" id="export_search" style="margin-top: 30px;" class="btn btn-labeled btn-success mea-btn-export"> <span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i></span>Export </a>
                                                
                                                 <!--div class="dropdown" data-toggle="dropdown"-->
                                                 <a href="#" id="btn_customize" style="margin-top: 30px;" class="btn btn-labeled btn-warning dropdown-toggle" data-toggle="dropdown"> <span class="btn-label"><i class="glyphicon glyphicon-list"></i></span>Customize </a>
                                                 <!-- XXX -->
                                                 
                                                 <ul id='dpsettings' role="menu" class="dropdown-menu pull-center">
                                                    <li><a href="#" class="small" data-value="selectall" tabIndex="-1"><input id="selectall" type="checkbox"/>&nbsp;เลือกทุกค่า</a></li>
                                                   

                                                    <li class="divider"></li>
                                                    <p><a href="#">
                                                          <button class="btn btn-success" id='btn-save-settings'  style="text-align: center; line-height: 15px; margin-left:45px; margin-top:5px; margin-right:5px; padding: 8px 10px 5px 5px; ">&nbsp;&nbsp;จัดเก็บ</button></a>
                                                        <a href="#">
                                                          <button class="btn btn-danger" id='btn-cancel-settings' style="text-align: center; line-height: 15px; margin-left:45px; margin-top:5px; margin-right:5px; padding: 8px 10px 5px 5px; ">&nbsp;&nbsp;ยกเลิก</button></a></p>
                                                     <li class="divider"></li>
                                                    </a></p>
                                                    <li><a role="menuitem" href="#" class="small" data-value="option0" tabIndex="-1"><input id="option0" type="checkbox"/>&nbsp;ถึง</li>
                                                    <li><a role="menuitem" href="#" class="small" data-value="option1" tabIndex="-1"><input id="option1" type="checkbox"/>&nbsp;จำนวณวัน</a></li>
                                                    
                                                    <li><a role="menuitem"  href="#" class="small" data-value="option2" tabIndex="-1"><input id="option2" type="checkbox"/>&nbsp;จำนวณหน่วย</a></li>
                                                    <li><a role="menuitem"  href="#" class="small" data-value="option3" tabIndex="-1"><input id="option3" type="checkbox"/>&nbsp;ก่อนคำนวณ MGT FEE &amp; CUST FEE</a></li>
                                                    <li><a role="menuitem"  href="#" class="small" data-value="option4" tabIndex="-1"><input id="option4" type="checkbox"/>&nbsp;ค่าใช้จ่ายอื่น</a></li>
                                                    <li><a role="menuitem"  href="#" class="small" data-value="option5" tabIndex="-1"><input id="option5" type="checkbox"/>&nbsp;ค่าสอบบัญชี</a></li>
                                                     
                                                    <li class="divider"></li>
                                                     
                                                     
                                                    <li><a role="menuitem"  href="#" class="small" data-value="option6" tabIndex="-1"><input id="option6" type="checkbox"/>&nbsp;ค่าธรรมเนียมรับฝากสินทรัพย์</a></li>
                                                    <li><a role="menuitem"  href="#" class="small" data-value="option7" tabIndex="-1"><input id="option7" type="checkbox"/>&nbsp;ค่าธรรมเนียมการจัดการ</a></li>
                                                    <li><a role="menuitem"  href="#" class="small" data-value="option8" tabIndex="-1"><input id="option8" type="checkbox"/>&nbsp;ค่าใช้จ่ายสะสมระหว่างงวด</a></li>
                                                    
                                                    <li class="divider"></li>
                                                      
                                                    <li><a role="menuitem"  href="#" class="small" data-value="option9" tabIndex="-1"><input id="option9" type="checkbox"/>&nbsp;NAV สิ้นงวดก่อนหักค่าใช่จ่ายสะสม</a></li>
                                                    <li><a role="menuitem"  href="#" class="small" data-value="option10" tabIndex="-1"><input id="option10" type="checkbox"/>&nbsp;NAV Unit before Expense</a></li>
                                                    <li><a role="menuitem"  href="#" class="small" data-value="option11" tabIndex="-1"><input id="option11" type="checkbox"/>&nbsp;ผลตอบแทนจากการลงทุน (Yield Port (%))</a></li>
                                                    <li><a role="menuitem"  href="#" class="small" data-value="option12" tabIndex="-1"><input id="option12" type="checkbox"/>&nbsp;Management Fee Rate (%)</a></li>

                                                    <li class="divider"></li>
                                                                                                      
                                                    <li><a role="menuitem"  href="#" class="small" data-value="option13" tabIndex="-1"><input id="option13" type="checkbox"/>&nbsp;ค่าจัดการ</a></li>
                                                    <li><a role="menuitem"  href="#" class="small" data-value="option14" tabIndex="-1"><input id="option14" type="checkbox"/>&nbsp;VAT</a></li>
                                                    <li><a role="menuitem"  href="#" class="small" data-value="option15" tabIndex="-1"><input id="option15" type="checkbox"/>&nbsp;ค่าธรรมเนียมการจัดการ + VAT</a></li>
                                                    
                                                    <!--li class="divider"></li-->

                                                </ul>
                                                <!--/div--> 
                                                 <a href="#" id="btn_chart"     style="margin-top: 30px;" class="btn btn-labeled btn-info"> <span class="btn-label"><i class="glyphicon glyphicon-signal"></i></span>Chart </a>
                                                
                                                
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                



                                <!-- Widget ID (each widget will need unique ID)-->
                                <div class="jarviswidget jarviswidget-color-blueDark" style="margin-top: 5px;" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
                                    <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                    data-widget-colorbutton="false"
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true"export_search
                                    data-widget-sortable="false"

                                    -->

                                    <select class="form-control mea-pagesize" id="page-size-search">
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>

                                    <header>
                                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    </header>

                                    <!-- widget div-->
                                    <div>

                                        <!-- widget edit box -->
                                        <div class="jarviswidget-editbox">
                                            <!-- This area used as dropdown edit box -->

                                        </div>
                                        <!-- end widget edit box -->

                                        <!-- widget content -->
                                        <div class="widget-body">
                                            <p class="report-title">
                                                 

                                            </p>
                                            <p class="report-period"> </p>
                                            <div class="table-responsive">
                                                <div id="serch_data">
                                                      ไม่มีข้อมูล
                                                </div>
                                            </div>

                                        </div>
                                        <!-- end widget content -->

                                    </div>
                                    <!-- end widget div -->

                                </div>
                                <!-- end widget -->

                            </div>
                            

                        </div>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- WIDGET END -->

    </div>

</div>

<!-- END MAIN CONTENT -->


<script type="text/javascript">

    // checked menu
    var menus = []; 

    $(document).ready(function(){
        $('#export_search').on('click',function() {
            var PageSizeAll = $('#page-size-search').val();
            var EmpID = $('#name').val();
            var depart = $('#depart').val();
            var plan = $('#plan').val();
            var date_start = $('#hd_date_start').val();
            var date_end =$('#hd_date_end').val();


            var check_name =$('#check_name').is(':checked');
            var check_depart =$('#check_depart').is(':checked');
            var check_plan =$('#check_plan').is(':checked');
            var check_date =$('#check_date').is(':checked');

            window.location.href =  "EquityExpenseReport/exportsearch?EmpID=" + EmpID +"&depart=" + depart +
                                    "&plan=" + plan + "&date_start=" +date_start + "&date_end=" + date_end+ 
                                    '&check_name=' + check_name+ '&check_depart=' + check_depart+ '&check_plan=' + 
                                    check_plan+ '&check_date=' + check_date;
            return false;
        });

        meaDatepicker("date_start","date_end");
        meaDatepicker("date_end");

        $('html').append('<input type="hidden" value="1" id="currentpage_search" />');

        $('#btn_search').on('click',function() {
            $("body").trigger("click");

            var PageSizeAll = $('#page-size-search').val();
            var EmpID = $('#name').val();
            var depart = $('#depart').val();
            var plan = $('#plan').val();
            var date_start = $('#hd_date_start').val();
            var date_end =$('#hd_date_end').val();

            var check_name =$('#check_name').is(':checked');
            var check_depart =$('#check_depart').is(':checked');
            var check_plan =$('#check_plan').is(':checked');
            var check_date =$('#check_date').is(':checked');


            if(check_date && date_start != "" && date_end != "" ){
                var str =  'ในช่วงวันที่ ' + GetDateFormat(date_start) + ' ถึง ' + GetDateFormat(date_end);
                $('.report-period').html(str);
            }

            var jsondata = {
                pagesize : PageSizeAll,
                PageNumber:1,
                emp_id : EmpID,
                depart :depart,
                plan : plan,
                date_start:date_start,
                date_end:date_end,
                check_name :check_name,
                check_depart:check_depart,
                check_plan:check_plan,
                check_date:check_date

            };

            console.log(jsondata);
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(jsondata,"EquityExpenseReport/search",RenderSearch);

            return false;

        });


        $("#page-size-search").on('change',function() {
            $("body").trigger("click");

            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            var val = $(this).val();

            var EmpID = $('#name').val();
            var depart = $('#depart').val();
            var plan = $('#plan').val();
            var date_start = $('#hd_date_start').val();
            var date_end =$('#hd_date_end').val();

            var check_name =$('#check_name').is(':checked');
            var check_depart =$('#check_depart').is(':checked');
            var check_plan =$('#check_plan').is(':checked');
            var check_date =$('#check_date').is(':checked');

            var jsondata = {
                pagesize : val,
                PageNumber:1,
                emp_id : EmpID,
                depart :depart,
                plan : plan,
                date_start:date_start,
                date_end:date_end,
                check_name :check_name,
                check_depart:check_depart,
                check_plan:check_plan,
                check_date:check_date

            };
//            var jsondata = {pagesize : val,PageNumber:1};
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(jsondata,"EquityExpenseReport/search",RenderSearch);

        });

        /**
         * START: Cutomize Table Header
         */
        
        $( '.dropdown').hover(
            function(){
               $(this).children('.dropdown-menu').slideDown(200);
            },
            function(){
                $(this).children('.dropdown-menu').slideUp(200);
            }
        );

        $( '.dropdown-menu a' ).on( 'click', function( event ) {
            event.preventDefault(); // Stop navigation
            var $target = $( event.currentTarget ),
               val = $target.attr( 'data-value' ),
               $inp = $target.find( 'input' ),
               idx;

            if ( ( idx = menus.indexOf( val ) ) > -1 ) {
                menus.splice( idx, 1 );
                setTimeout( function() { 
                    if(val === 'selectall') {
                       $inp.prop( 'checked', false );
                       menus = [];
                        for(var xx=0; xx< 16; xx++) { 
                            $('#option'+ xx).prop('checked', false);
                        }
                    } else {
                        $inp.prop( 'checked', false );
                    }
              }, 0);
            } else {
                if(val !== undefined) {
                    if(val === 'selectall') {
                        menus = [];
                        menus.push( val );
                        setTimeout( function() { 
                            $inp.prop( 'checked', true );
                            for(var xx=0; xx< 16; xx++) { 
                               $('#option'+ xx).prop('checked', true);
                               menus.push('option'+ xx);
                            }
                        }, 0);
                    } else {
                        menus.push( val );
                        setTimeout( function() { 
                            $inp.prop( 'checked', true );
                            
                        }, 0);
                    }
                } 
            }

            $( event.target ).blur();
              
            console.log( menus );
            return false;
        });
        
        $('#btn-save-settings').on('click', function(e) {
           e.preventDefault();
           $(this).closest(".dropdown-menu").prev().dropdown("toggle");
           var jsondata = {
                menu_id : 62,
                submenu_id : 8,
                options : menus};
                
            // MeaAjax(jsondata,"EquityExpenseReport/saveheader", RenderSaveSettings);
           
        });

        $('#btn-cancel-settings').on('click', function(e) {
             $(this).closest(".dropdown-menu").prev().dropdown("toggle");
        });

        // POST to read a current settings
        var jsondata = {
                menu_id : 62,
                submenu_id :8,
                options : menus};
                
        // MeaAjax(jsondata,"EquityExpenseReport/readheader", RenderReadSettings);
        /* END: Cutomize Table Column */

    });  // jquery ready


    function RenderSearch(data){

        $("#serch_data").html(data.html);
        $("#serch_data").fadeIn('300');


        $("#page_click_search li a").on('click',PageRenderSearch);
    }

    function PageRenderSearch(){

        var p = $(this).attr('data-page');
        var page_size = $('#page-size-search').val();
        var CurPage = $('#currentpage_search').val();
        $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');

        if(p == "pre"){
            p = parseInt(CurPage) - 1;
        }

        if(p == "next"){
            p = parseInt(CurPage) + 1;
        }
        $('#currentpage_search').val(p);

        var EmpID = $('#name').val();
        var depart = $('#depart').val();
        var plan = $('#plan').val();
        var date_start = $('#hd_date_start').val();
        var date_end =$('#hd_date_end').val();

        var check_name =$('#check_name').is(':checked');
        var check_depart =$('#check_depart').is(':checked');
        var check_plan =$('#check_plan').is(':checked');
        var check_date =$('#check_date').is(':checked');


        var jsondata = {
            pagesize : page_size,
            PageNumber:p,
            emp_id : EmpID,
            depart :depart,
            plan : plan,
            date_start:date_start,
            date_end:date_end,
            check_name :check_name,
            check_depart:check_depart,
            check_plan:check_plan,
            check_date:check_date

        };

        MeaAjax(jsondata,"EquityExpenseReport/search",RenderSearch);
    };

</script>

@stop
