@extends('backend.layouts.default')
@section('content')
<?php
$data = getmemulist();
$arrSidebar =getSideBar($data);
?>

    <style type="text/css">
        #sort-by-year{
            /*position:absolute;*/
            width: 100%;
            margin-bottom: 10px;
            /*z-index: 999;*/
            /*left: 200px;*/
            /*top:5px*/
        }

        #sort-by-year select{
            width: 300px;

        }

        #datatable_fixed_column tbody tr td a i{
            font-size: 11px !important;
            line-height: 12px!important;
        }

        #datatable_fixed_column tbody tr td a{
            line-height: 12px!important;
        }

        #myProgressBar {
            height: 15px;
        }

        #progressMessage{
            border: 1px solid gray;
            margin-bottom:10px;
        }

    </style>
        <!-- MAIN CONTENT -->
<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>

                {{getMenutitle($arrSidebar)}}
            </h1>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="well well-sm">
                <!-- Timeline Content -->
                <div class="smart-timeline">
                    <ul class="smart-timeline-list">
                        <li>
                            <div class="smart-timeline-icon bg-color-greenDark">
                                <i class="fa fa-file-text"></i>
                            </div>
                            <div class="smart-timeline-time">
                                {{--<small>รูปแบบที่ 1</small>--}}
                            </div>
                            <div class="smart-timeline-content">
                                <p class="import_title">
                                    <a href="javascript:void(0);"><strong>เป็นเมนูนําเข้าข้อมูลด้านการลงทุนของสมาชิก เช่น ข้อมูลอัตราสมทบ ผลประโยชน์เงินสมทบ</strong></a>
                                </p>
                                <p>
                                    ชนิดไฟล์ที่อนุญาติให้นำเข้า:Exel <br/>

                                <label>31 คอลัม</label>
                                </p>
                                <a class="btn btn-xs btn-success" href="{{ action('UserManageFundController@dowloadsample')}}"> ดูตัวอย่างไฟล์</a>
                                <p></p>
                                <table style="width: 100px;" class="table table-bordered">
                                    <tr><td>EMP_ID</td><td>FULL_NAME </td><td>PATH_CODE </td><td>DEP_CODE  </td><td>.... </td><td>RECORD_DATE</td></tr>
                                    <tr>
                                        <td>1016934</td>
                                        <td>น.ส. นิ่มนวล อดุลย์เดช</td>
                                        <td>1222</td>
                                        <td>50</td>

                                        <td>....</td>
                                        <td>2015.01.31</td>
                                    </tr>
                                </table>



                                <p>
                                <p class="help-block">
                                    เลือกไฟล์<br/>
                                    <span style="color: red">จำนวน record ที่สามารถ import ได้สูงสุดต่อครั้งคือ <strong>12,000 record เท่านั้น!!!</strong></span>
                                </p>
                                <input type="file" class="btn btn-default" id="import1" name="import1">


                                </p>
                                <p id="progress_check" style="display: none;"><img src="{{asset('backend/img/shot.gif')}}"  /> กำลังตรวจสอบ ไฟล์</p>
                                <p id="progress_import" style="display: none;"><img src="{{asset('backend/img/shot.gif')}}"  /> กำลังนำเข้าข้อมูล</p>
                                <p id="check_ret" style="display: none"></p>
                                <div id='myProgressBar'></div>
                                <p>
                                    <a href="javascript:void(0);"  data-input="import1" data-import="1" class="btn_check btn btn-xs btn-primary"><i class="fa fa-download"></i> ตรวจสอบไฟล์</a>
                                    <a href="javascript:void(0);" style="display: none"  data-input="import1" data-import="1" class="btn_import btn btn-xs btn-primary"><i class="fa fa-download"></i> นำเข้าข้อมูล</a>
                                </p>

                                <div class="row">

                                </div>
                            </div>
                        </li>




                    </ul>
                </div>
                <!-- END Timeline Content -->

            </div>

        </div>

    </div>


</div>
<!-- END MAIN CONTENT -->



<script type="text/javascript">




    $(document).ready(function() {
        var percent = 0;
        $("#myProgressBar").progressbar({value:0});

        window.pollingPeriod = 1000;
        window.progressInterval;

        $('.btn_check').on('click',function(){

            $("#myProgressBar").progressbar({value:0});

            var dataimport = new FormData();
            var target = $(this).attr('data-input');

            var importType= $(this).attr('data-import');

            var extall="xlsx,xls.xlx";
            var files = $("#" + target).get(0).files;

            if(files.length){

                ext = files[0].name.split('.').pop().toLowerCase();
                if(parseInt(extall.indexOf(ext)) < 0)
                {
                    Alert("Import",'Extension support : ' + extall);
                    return false;
                } else {
                    $('#progress_check').show();
                    
                    if (files.length > 0) {
                        dataimport.append("exelimport", files[0]);
                    }

                    dataimport.append('type',importType);

                    $.ajax({

                            cache: false,
                            type: 'POST', 
                            contentType: false,
                            processData: false,
                            url: 'fund/check',
                            data: dataimport,

                            success: function(data) {
                                percent = 100;
                                clearInterval(window.progressInterval);
                                $("#myProgressBar .ui-progressbar-value").animate({width: percent + "%"}, 500);
                                if(data.success) {
                                    $('#progress_check').hide();
                                    if(parseInt(data.html) > 10,000) {
                                        Alert('Import', "จำนวน record สูงสุดที่อนุญาติให้ import คือ 10,000 record");
                                    } else {
                                        $('.btn_check').hide();
                                        $('.btn_import').show();

                                        $('#check_ret').show();
                                        $('#check_ret').html("ข้อมูลถูกต้อง มีจำนวนทั้งหมด " + data.html +" record  กรุณากดปุ่ม นำเข้าข้อมูล ด้านล่างเพื่อ ดำเนินการ import");
                                    }
                                }
                            },

                            error: function(xhr, textStatus, thrownError) {
                                clearInterval(window.progressInterval);
                                $("#myProgressBar .ui-progressbar-value").animate({width: "100%"}, 500);
                                Alert("Error", "การ import ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ");
                            }
                    });

                    ///////////// >> Progress Bar //////////////
                    //window.progressInterval = setInterval(updateProgress, window.pollingPeriod);
                    ///////////// Progress Bar << //////////////
                    function updateProgress() {
                        $.getJSON('fund/progress',function(data) {
                            // $('#output').html(data.percentComplete*100 + ' complete');
                            console.log('########## total:' + data.total + ' progress: ' + data.percent_progress + '%');
                            $("#myProgressBar .ui-progressbar-value").animate({width: data.percent_progress + "%"}, 500);

                        }).error(function(data) {
                            clearInterval(window.progressInterval);
                            //$('#output').html('Uh oh, something went wrong');
                            console.log('Uh oh, something went wrong' + data);
                            // Alert("Error", "Uh oh, somthing went wrong");
                        });
                    }
                }
            } else {
                Alert("Import","ท่านยังไม่ได้เลือกไฟล์");
            }

         



//

////            var files = $("#imageInput_" + id).get(0).files;
//
//
//


        });

        $('.btn_import').on('click',function(){

            $("#myProgressBar").progressbar({value:0});

            $('#check_ret').hide();
            $('#progress_import').show();

            var dataimport = new FormData();
            var target = $(this).attr('data-input');


            var files = $("#" + target).get(0).files;

            var importType= $(this).attr('data-import');


            if (files.length > 0) {
                dataimport.append("exelimport", files[0]);


            }

//            dataimport.append('type',importType);
//            var files = $("#imageInput_" + id).get(0).files;



            $.ajax({
                cache: false,
                type: 'POST', // or post?
//                dataType: 'json',
                contentType: false,
                processData: false,
                url: 'fund/import',
                data: dataimport,

                success: function(data){
                    /*if(data.success){
                        $('#progress_import').hide();
                        AlertSuccess("ข้อมูลได้ถูก update เรียบร้อยแล้ว", function(){
                            window.location.href = "fund";

                        });
                    }else {
                        Alert('Import','การ import ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ')
                    }*/
                    if(data.success){
                        $('#progress_import').hide();
                        $('#check_ret').show();
                        $('#check_ret').html("ข้อมูลได้ถูก update เรียบร้อยแล้ว" + data.html);
         
                        AlertSuccess("ข้อมูลได้ถูก update เรียบร้อยแล้ว",function(){
                           // window.location.href = "simple";
                        });
                        Alert('OK', "ข้อมูลได้ถูก update เรียบร้อยแล้ว : ");

                    } else {
                        $('#progress_import').hide();
                        $('#check_ret').hide();
                        Alert('Import','การ import ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ')
                    }


                },
                error: function(xhr, textStatus, thrownError) {
                    Alert('Error', "การ import ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ");
                }
            });

        });





    });




</script>

@stop