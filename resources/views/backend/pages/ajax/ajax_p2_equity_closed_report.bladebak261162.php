

<div>Showing {{$PageNumber}} to {{ (($PageSize * $PageNumber) > $totals? $totals:($PageSize * $PageNumber))  }} of {{ $totals }}</div>

<table class="table table-bordered">
   
    <thead>

    <!--tr>
        <th colspan="16" style="text-align: center">รายงานข้อมูล ณ วันที่ {{get_date_notime(date("Y-m-d H:i:s"))}}</th>
    </tr-->
    <tr>
           
        @php     
        $FOUND_RECORD = false; 
        $LAST_DAY = 30;
        @endphp

        @foreach($config as $index =>$field)  
            @php  
            $LAST_DAY = $field->LAST_DAY;
            @endphp
            @break 
        @endforeach

        @foreach($data as $index =>$field)     
            @php  
           
            $FOUND_RECORD = true; 
            $LAST_DAY = $field->LAST_DAY;
            @endphp
            @break 
        @endforeach
        <!--
            /********************************************************************************************************************************* */
            Author        : Chalermpol Chueayen (chalermpols@msn.com)
            Date Modified : 2019-03-19
            Purpose       : Adjust {{$LAST_DAY + 45}} to {{$LAST_DAY + 15}} for valid table colspan
            ********************************************************************************************************************************** */
        -->
        <th colspan="{{$LAST_DAY + 15}}" style="text-align: center">
            <strong style="font-size: 20px;">{{$TableTitle}} </strong> <!-- รายงานความเคลื่อนไหวตราสารทุน -->
            <br/>
            <strong style="font-size: 20px;">กองทุนสํารองเลี้ยงชีพพนักงานการไฟฟ้านครหลวง ซึ่งจดทะเบียนแล้ว </strong> <!-- รายงานความเคลื่อนไหวตราสารทุน -->
            <br/>
            <strong style="font-size: 18px;"> สรุปรายละเอียดหุ้น <strong style="font-size: 18px; color:red;">{{$period}}</strong></strong>
            <br/>
            <strong style="font-size: 18px; color:blue;"> {{$Subtitle}}</strong>
            <br/>
            <strong style="font-size: 16px;">รายงาน ณ.วันที่ {{get_date_notime(date("Y-m-d H:i:s"))}} </strong>
             <br/>
            <strong style="font-size: 16px;"> </strong>
        </th>
    </tr>
    <tr>
        <th rowspan="2" style="text-align: center;" nowrap>กลุ่มอุตสาหกรรม</th>
        <th rowspan="2" style="text-align: center;" nowrap>หลักทรัพย์</th>
        <th rowspan="2" style="text-align: center;" nowrap>จํานวนหน่วย</th>
        <th colspan="2" style="text-align: center;" nowrap>วันที่ลงทุน</th>
        <th colspan="2" style="text-align: center;" nowrap>ราคาเฉลี่ยต่อเดือน</th>

        <th rowspan="2" style="text-align: center">จํานวนเงินลงทุน(ล้านบาท)</th>
           
        <th colspan="{{$LAST_DAY}}" style="text-align: center">ราคาปิดของหุ้น{{$period}} </th>
           
        <th rowspan="1" style="text-align: center" nowrap></th>

        <th rowspan="2" style="text-align: center"nowrap>ร้อยล่ะของหุ้น</th>
        <th rowspan="2" style="text-align: center">เงินลงทุน(เพิ่ม/ลด)</th>
        <th rowspan="2" style="text-align: center">EARNING PURCHASE</th>
        <th rowspan="2" style="text-align: center">PRICE EARNING RATIO</th>
        <th rowspan="2" style="text-align: center">PRICE PER BLOCK VALUE</th>
        <th rowspan="2" style="text-align: center">DIVIDEND YIELD %</th>
    </tr>
        
    <tr>
        
        <th style="text-align: center" nowrap>เริ่มลงทุน</th>
        <th style="text-align: center" nowrap>วันสุดท้าย</th>
        <th style="text-align: center" nowrap>วันแรก</th>
        <th style="text-align: center" nowrap>วันล่าสุด</th>
        
        <!-- ... LOOP DAYS -->
        <th style="text-align: center">1</th>
        <th style="text-align: center">2</th>
        <th style="text-align: center">3</th>
        <th style="text-align: center">4</th>
        <th style="text-align: center">5</th>
        <th style="text-align: center">6</th>
        <th style="text-align: center">7</th>
        <th style="text-align: center">8</th>
        <th style="text-align: center">9</th>
        <th style="text-align: center">10</th>
        <th style="text-align: center">11</th>
        <th style="text-align: center">12</th>
        <th style="text-align: center">13</th>
        <th style="text-align: center">14</th>
        <th style="text-align: center">15</th>
        <th style="text-align: center">16</th>
        <th style="text-align: center">17</th>
        <th style="text-align: center">18</th>
        <th style="text-align: center">19</th>
        <th style="text-align: center">20</th>
        <th style="text-align: center">21</th>
        <th style="text-align: center">22</th>
        <th style="text-align: center">23</th>
        <th style="text-align: center">24</th>
        <th style="text-align: center">25</th>
        <th style="text-align: center">26</th>
        <th style="text-align: center">27</th>
        <th style="text-align: center">28</th>

        @if($LAST_DAY>=29)
        <th style="text-align: center">29</th>
        @endif
              
    
        @if($LAST_DAY>=30)
        <th style="text-align: center">30</th>
        @endif 
    

        @if($LAST_DAY>=31)
        <th style="text-align: center">31</th>
        @endif
        
        <th style="text-align: center">มูลค่ารวม</th> 

    </thead>
   
        <tbody>
    
            @php
            $SUMTotalPrice = 0; 
            $PercentToal = 0;
            foreach($data as $index =>$field) {
                $RMUnit = (float)$field->FINAL_REMAIN_UNIT;
                $PCloseLastDay = 0;
                $PCloseArray = array( $field->A1,  $field->A2, $field->A3, $field->A4, $field->A5, $field->A6, $field->A7, $field->A8, $field->A9, $field->A10, 
                             $field->A11, $field->A12, $field->A13, $field->A14, $field->A15, $field->A16, $field->A17, $field->A18, $field->A19, $field->A20, 
                             $field->A21, $field->A22, $field->A23, $field->A24, $field->A25, $field->A26, $field->A27, $field->A28, $field->A29, $field->A30,
                             $field->A31 );

                for ($x = 0; $x < $field->LAST_DAY; $x++) {
                    if ($PCloseArray[($field->LAST_DAY-1) - $x] > 0) {
                        $PCloseLastDay = $PCloseArray[($field->LAST_DAY-1) - $x];
                        break;
                    }
                }    

                $TOX = ($RMUnit * $PCloseLastDay) / 1000000;
                $SUMTotalPrice += round($TOX, 2); 
            
            }
            @endphp
            <!-- ################################# --> 
            @foreach($data as $index =>$field) 
                @php
                $ZZZ = $field->GIG / 1000000; 
                $INVEST_NET = abs($ZZZ) -  $field->TotalValueEOM;
              
                $LAST_PRICE_AVG =  $field->GIG /  $field->FINAL_REMAIN_UNIT;  
                 
                $FIRST_TRANS_DATE = toThaiDateTime($field->FIRST_TRANS_DATE, false);
                $LAST_TRANS_DATE = toThaiDateTime($field->LAST_TRANS_DATE, false);

                $RMUnit = (float)$field->FINAL_REMAIN_UNIT;
                $PCloseLastDay = 0;
                $PCloseArray = array( $field->A1,  $field->A2, $field->A3, $field->A4, $field->A5, $field->A6, $field->A7, $field->A8, $field->A9, $field->A10, 
                             $field->A11, $field->A12, $field->A13, $field->A14, $field->A15, $field->A16, $field->A17, $field->A18, $field->A19, $field->A20, 
                             $field->A21, $field->A22, $field->A23, $field->A24, $field->A25, $field->A26, $field->A27, $field->A28, $field->A29, $field->A30,
                             $field->A31 );

                for ($x = 0; $x < $field->LAST_DAY; $x++) {
                    if ($PCloseArray[($field->LAST_DAY-1) - $x] > 0) {
                        $PCloseLastDay = $PCloseArray[($field->LAST_DAY-1) - $x];
                        break;
                    }
                }    

                $TotalPrice = ($RMUnit * $PCloseLastDay) / 1000000;
                
                $PercentToal += ((float)($TotalPrice / $SUMTotalPrice) * 100);
                @endphp
            <tr>
                <!--td >ไม่พบรายการ</td-->  
                <td style="text-align: left" nowrap>{{$field->INDUSTRIAL}}</td> <!-- INDUSTRIAL -->
                <td style="text-align: right">{{$field->SYMBOL}}</td>       <!-- SYMBOL -->
                <td style="text-align: right">{{number_format((float)$field->FINAL_REMAIN_UNIT, 2, '.', ',')}}</td>   <!-- P/E -->
                <td style="text-align: right" nowrap>{{$FIRST_TRANS_DATE}}</td> <!--First date-->
                <td style="text-align: right" nowrap>{{$LAST_TRANS_DATE}}</td> <!-- Last date -->

                @if($field->FIRST_PRICE_PURCHASE>=0)
                   <td style="text-align: right">{{number_format((float)$field->FIRST_PRICE_PURCHASE, 2, '.', ',')}}</td>  
                @else
                   <td style="text-align: right;  color:red">{{number_format((float)$field->FIRST_PRICE_PURCHASE, 2, '.', ',')}}</td>
                @endif   
               
                @if($LAST_PRICE_AVG >=0)
                <td style="text-align: right">            {{number_format((float)$LAST_PRICE_AVG,  2, '.', ',')}}</td>       <!-- PClosed -->
                @else
                <td style="text-align: right;  color:red">{{number_format((float)$LAST_PRICE_AVG,  2, '.', ',')}}</td>       <!-- PClosed -->
                @endif

                @if($ZZZ > 0)
                <td style="text-align: right">{{number_format((float)$ZZZ,  2, '.', ',')}}</td>   <!-- จำนวนเงินลงทุน -->
                @else
                 <td style="text-align: right; color:red">{{number_format((float)$ZZZ,  2, '.', ',')}}</td>   <!-- จำนวนเงินลงทุน -->
                @endif
                <td style="text-align: right">{{number_format((float)$field->A1, 2, '.', ',')}}</td> <!--P/BV-->
                <td style="text-align: right">{{number_format((float)$field->A2, 2, '.', ',')}}</td> <!-- Div Yield -->
                <td style="text-align: right">{{number_format((float)$field->A3, 2, '.', ',')}}</td> <!-- EPS-->
                <td style="text-align: right">{{number_format((float)$field->A4, 2, '.', ',')}}</td>       <!-- PClosed -->
                <td style="text-align: right">{{number_format((float)$field->A5, 2, '.', ',')}}</td>   <!-- P/E -->
                <td style="text-align: right">{{number_format((float)$field->A3, 2, '.', ',')}}</td> <!--P/BV-->
                <td style="text-align: right">{{number_format((float)$field->A7, 2, '.', ',')}}</td> <!-- Div Yield -->
                <td style="text-align: right">{{number_format((float)$field->A8, 2, '.', ',')}}</td> <!-- EPS-->
                <td style="text-align: right">{{number_format((float)$field->A9, 2, '.', ',')}}</td> <!-- EPS-->
                <td style="text-align: right">{{number_format((float)$field->A10, 2, '.', ',')}}</td> <!-- EPS-->
                <td style="text-align: right">{{number_format((float)$field->A11, 2, '.', ',')}}</td> <!--P/BV-->
                <td style="text-align: right">{{number_format((float)$field->A12, 2, '.', ',')}}</td> <!-- Div Yield -->
                <td style="text-align: right">{{number_format((float)$field->A13, 2, '.', ',')}}</td> <!-- EPS-->
                <td style="text-align: right">{{number_format((float)$field->A14, 2, '.', ',')}}</td>       <!-- PClosed -->
                <td style="text-align: right">{{number_format((float)$field->A15, 2, '.', ',')}}</td>   <!-- P/E -->
                <td style="text-align: right">{{number_format((float)$field->A16, 2, '.', ',')}}</td> <!--P/BV-->
                <td style="text-align: right">{{number_format((float)$field->A17, 2, '.', ',')}}</td> <!-- Div Yield -->
                <td style="text-align: right">{{number_format((float)$field->A18, 2, '.', ',')}}</td> <!-- EPS-->
                <td style="text-align: right">{{number_format((float)$field->A19, 2, '.', ',')}}</td> <!-- EPS-->
                <td style="text-align: right">{{number_format((float)$field->A20, 2, '.', ',')}}</td> <!-- EPS-->
                <td style="text-align: right">{{number_format((float)$field->A21, 2, '.', ',')}}</td> <!-- EPS-->
                <td style="text-align: right">{{number_format((float)$field->A22, 2, '.', ',')}}</td> <!-- EPS-->
                <td style="text-align: right">{{number_format((float)$field->A23, 2, '.', ',')}}</td> <!-- EPS-->
                <td style="text-align: right">{{number_format((float)$field->A24, 2, '.', ',')}}</td> <!-- EPS-->
                <td style="text-align: right">{{number_format((float)$field->A25, 2, '.', ',')}}</td> <!-- EPS-->
                <td style="text-align: right">{{number_format((float)$field->A26, 2, '.', ',')}}</td> <!-- EPS-->
                <td style="text-align: right">{{number_format((float)$field->A27, 2, '.', ',')}}</td> <!-- EPS-->
                <td style="text-align: right">{{number_format((float)$field->A28, 2, '.', ',')}}</td> <!-- EPS-->
                @if($field->LAST_DAY>=29)
                <td style="text-align: right">{{number_format((float)$field->A29, 2, '.', ',')}}</td> <!-- EPS-->
                @endif
                
                @if($field->LAST_DAY>=30) 
                <td style="text-align: right">{{number_format((float)$field->A30, 2, '.', ',')}}</td> <!-- EPS-->
                @endif
                
                @if($field->LAST_DAY>=31)
                <td style="text-align: right">{{number_format((float)$field->A31, 2, '.', ',')}}</td> <!-- EPS-->
                @endif

                <td style="text-align: right">{{number_format((float)$TotalPrice, 2, '.', ',')}}</td> <!--มูลค่ารวม -->
                <td style="text-align: right"> {{number_format(round(($TotalPrice / $SUMTotalPrice) * 100, 1), 2, '.', ',')}}</td> <!-- ร้อนล่ะของหุ้น -->

                <!-- <td style="text-align: center">{{number_format((float)$field->TotalValueEOM, 2, '.', ',')}}</td> --> 
                <!-- <td style="text-align: right">{{number_format((float) ($field->TotalValueEOM / $SumTotalValueEOM) * 100, 2, '.', ',')}}</td> -->
                 

                @if($INVEST_NET > 0)
                    <td style="text-align: right">{{number_format((float) $INVEST_NET, 2, '.', ',')}}</td> <!-- เงินลงทุนเพิ่มลด-->
                @else
                    <td style="text-align: right; color:red;">{{number_format((float) $INVEST_NET, 2, '.', ',')}}</td> <!-- เงินลงทุนเพิ่มลด-->
                @endif 
                <td style="text-align: right">{{number_format((float)$field->EPS, 2, '.', ',')}}</td> <!-- EPS-->
                <td style="text-align: right">{{number_format((float)$field->PE, 2, '.', ',')}}</td> <!-- EPS-->
                <td style="text-align: right">{{number_format((float)$field->PBV, 2, '.', ',')}}</td> <!-- EPS-->
                <td style="text-align: right">{{number_format((float)$field->LAST_DIV_YIELD, 2, '.', ',')}}</td> <!-- EPS-->

            </tr>

            @endforeach
             
            
            
            <td colspan="{{$LAST_DAY + 8}}" style="text-align: right">
             
            รวม
            
            </td>
             
            <td colspan="1" style="text-align: right"> <!-- C -->
            
            {{ number_format((float)($SUMTotalPrice), 2, '.', ',') }}

            </td>
            @if($SUMTotalPrice > 0)
            <td colspan="1" style="text-align: right">  
            {{ round($PercentToal, 0) }}
            </td>
            @else
            <td colspan="1" style="text-align: right">  
          
            </td>
            @endif 
            <td colspan="1"> <!-- E -->
            </td>
            <td colspan="4"> <!-- F -->
            </td>
            
        </tr>
        </tbody>
    <tfoot>
        <tr>             
            <td colspan="{{$LAST_DAY + 15}}">{!! $htmlPaginate !!}</td>                                     
        </tr>
    </tfoot>
</table>

<div>


</div>