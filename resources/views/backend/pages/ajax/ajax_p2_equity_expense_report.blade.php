
<div>Showing {{$PageNumber}} to {{ (($PageSize * $PageNumber) > $totals? $totals:($PageSize * $PageNumber))  }} of {{ $totals }}</div>
<table class="table table-bordered">
    
    <thead>
    <tr>
        <th colspan="16" style="text-align: center">รายงานข้อมูล ณ วันที่ {{get_date_notime(date("Y-m-d H:i:s"))}}</th>
    </tr>
    <tr>
        <th colspan="16" style="text-align: center">
             <strong style="font-size: 18px;">กองทุนสำรองเลี้ยงชีพพนักงานการไฟฟ้านครหลวง ซึ่งจดทะเบียนแล้ว (MEAEQ)</strong>
             <br/>
            <strong style="font-size: 18px;">จัดการ โดยบริษิทิหลักทรัพย์ จัดการกองทุน ยูโอบี (ประเทศไทย) จำกัด </strong>
             <br/>
            <strong style="font-size: 18px;">เปรียบเทียบอัตราผลตอบแทนกับ Benchmark</strong>
        </th>
    </tr>
    <tr>
        <th style="text-align: center"> ถึง </th>
        <th style="text-align: center" width="10%"> จำนวณวัน </th>
        <th style="text-align: center" width="5%">จำนวณหน่วย</th>
        <th style="text-align: center" width="10%">ก่อนคำนวณ MGT FEE &amp; CUST FEE</th>
        <th style="text-align: center">ค่าใช้จ่ายอื่น</th>

        <th style="text-align: center">ค่าสอบบัญชี</th>

        <th style="text-align: center" width="5%">ค่าธรรมเนียมรับฝากสินทรัพย์</th>
        <th style="text-align: center" width="5%">ค่าธรรมเนียมการจัดการ</th>
        <th style="text-align: center" width="10%">ค่าใช้จ่ายสะสมระหว่างงวด</th>

        <th style="text-align: center" width="10%">NAV สิ้นงวดก่อนหักค่าใช่จ่ายสะสม</th>
        <th style="text-align: center" width="10%">NAV Unit before Expense</th>

        <th style="text-align: center" width="10%">ผลตอบแทนจากการลงทุน (Yield Port (%))</th>

        <th style="text-align: center">Management Fee Rate (%)</th>

        <th style="text-align: center">ค่าจัดการ</th>
        <th style="text-align: center">VAT</th>
        <th style="text-align: center" width="10%">ค่าธรรมเนียมการจัดการ + VAT</th>

    </tr>
    </thead>
   
    <tbody>
        <tr>
            <!--td >ไม่พบรายการ</td-->

            <!--START: FAKE DATA -->          
            <td style="text-align: right">13/1/2016</td> <!-- ถึง -->
            <td style="text-align: right">1</td>       <!-- จำนวณวัน -->
            <td style="text-align: right">64,096,873.3114</td>   <!-- จำนวณหน่วย -->
            <td style="text-align: right">62,164,324.6731</td> <!-- ก่อนคำนวณ MGT FEE &amp; CUST FEE -->
            <td style="text-align: right">25.00</td> <!-- ค่าใช้จ่ายอื่น -->
            <td style="text-align: right">86.82</td> <!-- ค่าสอบบัญชี-->
            <td style="text-align: right">100.99</td> <!-- ค่าะรรมเนียมรับฝากสินทรัพย์ -->
            <td style="text-align: right">11,767.46</td>      <!-- ค่าธรรมเนียมการจัดการ -->
            <td style="text-align: right">85,233.75</td>      <!-- ค่าใช้จ่ายสะสมระหว่างงวด -->
            <td style="text-align: right">672,838,121.14</td>      <!-- NAV สิ้นงวดก่อนหักค่าใช่จ่ายสะสม -->
            <td style="text-align: right">10.4972</td>      <!-- NAV Unit before Expense-->
            <td style="text-align: right">1.9591</td>         <!--ผลตอบแทนจากการลงทุน (Yield Port (%)) -->
            <td style="text-align: right">0.00</td>         <!-- Management Fee Rate (%)-->
            <td style="text-align: right"></td>   <!-- ค่าจัดการ -->
            <td style="text-align: right"></td>     <!-- VAT -->
            <td style="text-align: right">0.00</td>        <!-- ค่าธรรมเนียมการจัดการ + VAT-->
        </tr>

        <tr class="odd">
            <!--td >ไม่พบรายการ</td-->

            <td style="text-align: right">14/1/2016</td> <!-- ถึง -->
            <td style="text-align: right">1</td>       <!-- จำนวณวัน -->
            <td style="text-align: right">64,096,721.5062</td>   <!-- จำนวณหน่วย -->
            <td style="text-align: right">72,264,823.4150</td> <!-- ก่อนคำนวณ MGT FEE &amp; CUST FEE -->
            <td style="text-align: right">0.00</td> <!-- ค่าใช้จ่ายอื่น -->
            <td style="text-align: right">86.72</td> <!-- ค่าสอบบัญชี-->
            <td style="text-align: right">100.99</td> <!-- ค่าะรรมเนียมรับฝากสินทรัพย์ -->
            <td style="text-align: right">0.00</td>      <!-- ค่าธรรมเนียมการจัดการ -->
            <td style="text-align: right">73,384.82</td>      <!-- ค่าใช้จ่ายสะสมระหว่างงวด -->
            <td style="text-align: right">672,838,121.14</td>      <!-- NAV สิ้นงวดก่อนหักค่าใช่จ่ายสะสม -->
            <td style="text-align: right">10.4972</td>      <!-- NAV Unit before Expense-->
            <td style="text-align: right"><font color="#ff0000">-1.0584</font></td>         <!--ผลตอบแทนจากการลงทุน (Yield Port (%)) -->
            <td style="text-align: right">0.60</td>         <!-- Management Fee Rate (%)-->
            <td style="text-align: right">10,913.15</td>   <!-- ค่าจัดการ -->
            <td style="text-align: right">763.94</td>     <!-- VAT -->
            <td style="text-align: right">11,677.32</td>        <!-- ค่าธรรมเนียมการจัดการ + VAT-->
        </tr>

        <tr>
            <!--td >ไม่พบรายการ</td-->

            <td style="text-align: right">15/1/2016</td> <!-- ถึง -->
            <td style="text-align: right">1</td>       <!-- จำนวณวัน -->
            <td style="text-align: right">51,096,721.5062</td>   <!-- จำนวณหน่วย -->
            <td style="text-align: right">42,264,823.4150</td> <!-- ก่อนคำนวณ MGT FEE &amp; CUST FEE -->
            <td style="text-align: right">0.00</td> <!-- ค่าใช้จ่ายอื่น -->
            <td style="text-align: right">86.72</td> <!-- ค่าสอบบัญชี-->
            <td style="text-align: right">100.99</td> <!-- ค่าะรรมเนียมรับฝากสินทรัพย์ -->
            <td style="text-align: right">0.00</td>      <!-- ค่าธรรมเนียมการจัดการ -->
            <td style="text-align: right">73,384.82</td>      <!-- ค่าใช้จ่ายสะสมระหว่างงวด -->
            <td style="text-align: right">672,838,121.14</td>      <!-- NAV สิ้นงวดก่อนหักค่าใช่จ่ายสะสม -->
            <td style="text-align: right">10.4972</td>      <!-- NAV Unit before Expense-->
            <td style="text-align: right"><font color="#ff0000">-1.4584</font></td>         <!--ผลตอบแทนจากการลงทุน (Yield Port (%)) -->
            <td style="text-align: right">0.65</td>         <!-- Management Fee Rate (%)-->
            <td style="text-align: right">11,913.15</td>   <!-- ค่าจัดการ -->
            <td style="text-align: right">743.94</td>     <!-- VAT -->
            <td style="text-align: right">10,677.32</td>        <!-- ค่าธรรมเนียมการจัดการ + VAT-->
        </tr>
            <!-- END: FAKE -->  
    </tbody>
    <tfoot>
        <tr>
            <td colspan="16">
                {!! $htmlPaginate !!}
            </td>
        </tr>
    </tfoot>
</table>

<div>


</div>