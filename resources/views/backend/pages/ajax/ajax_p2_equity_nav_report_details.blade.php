 

 @if($data)        
 <input type="hidden" id="txt_details_plan"  value="{{$plan}}">   
 <input type="hidden" id="txt_details_month" value="{{$selected_month}}">   
 <input type="hidden" id="txt_details_year"  value="{{$selected_year}}">   
 
 <div id="mini_table3" style="{{$table3show}}">
    <table class="table table-bordered"  width="400px" id="table_bond_nav" cellspacing="0">
        <thead>
            @foreach($data['bond_nav'] as $rowset =>$item)
            <tr>
                <th colspan="6" style="text-align: center">การลงทุนตราสารหนี้</th>
            </tr>
            <tr>
                <th colspan="6" style="text-align: center"> การลงทุน ณ. <font color="red">{{intval($item->DD)}} {{toThaiShortMonth($item->MM)}} {{$item->YYYY}}</font></th>
            </tr>
            <tr>
               <th colspan="1" rowspan="2" style="text-align: center">ประเภทตราสาร</th>
               <th colspan="2" rowspan="1" style="text-align: center">นโยบายการลงทุนตราสารหนี้</th>
               <th colspan="3" rowspan="2" style="text-align: center"> กราฟ </th>
            </tr>   
            <tr>
                <th colspan="1" style="text-align: center">สัดส่วน (%)</th>
                <!--th colspan="1" style="text-align: center">ล้าน</th-->
                <th colspan="1" style="text-align: center"> บาท</th> 
            </tr>
            @break
            @endforeach
        </thead>

        <tbody>
           @foreach($data['bond_nav'] as $rowset =>$rs)
            <tr>       
                   <td colspan="1" width="30%" style="text-align: left"> พันธบัตรรัฐบาล ธปท.และรัฐวิสาหกิจ</td>    
                   
                   <td colspan="1" width="5%" style="text-align: right">{{--round($rs->GOV_BOND_RATIO, 2)--}}
                        {{number_format((float)$rs->GOV_BOND_RATIO, 2, '.', '')}}
                   </td>
                
                   <!--td colspan="1" width="10%" style="text-align: right">{{number_format($rs->GOV_BOND_MB, 2)}}</td-->  
                 
                   <td colspan="1" width="8%" style="text-align:  right">{{number_format($rs->GOV_BOND_B, 2)}}</td> 
                 
                   <td colspan="3" rowspan="10" style="text-align: center">
                       <div id="main_chart1" class="main_chart1" style="width: 100%; padding: 10px;"> 
                       </div>
                    </td> 
            </tr>
            <tr>
                   <td colspan="1" width="30%" style="text-align: left">เงินฝากตราสารหุ้นธนาคารพาณิชย์</td>  
                    
                      <td colspan="1" width="5%" style="text-align: right"><font color="{{($rs->DEPOSIT_RATIO < 0) ? 'red':'#000'}}">
                      
                       {{round($rs->DEPOSIT_RATIO, 2)}}</font></td>
                      
                   <!--td colspan="1" width="10%" style="text-align: right">{{number_format($rs->DEPOSIT_MB, 2)}}</td-->  
                   <td colspan="1" width="8%" style="text-align:  right">{{number_format($rs->DEPOSIT_B, 2)}}</td>   
            </tr>
            <tr>
                   <td colspan="1" width="30%" style="text-align: left">หุ้นกู้</td>    
                   <td colspan="1" width="10%" style="text-align: right"><font color="{{($rs->DEBENTURE_RATIO < 0) ? 'red':'#000'}}">{{number_format($rs->DEBENTURE_RATIO, 2)}}</font></td>
                   <!--td colspan="1" width="10%" style="text-align: right">{{number_format($rs->DEBENTURE_MB, 2)}}</td-->  
                   <td colspan="1" width="8%" style="text-align:  right"><font color="{{($rs->OTHER_B < 0) ? 'red':'#000'}}">{{number_format($rs->DEBENTURE_B, 2)}}</font></td>   
            </tr>
            
            <tr>
                   <td colspan="1" width="30%" style="text-align: left">อื่นๆ (ลูกหนี้และเจ้าหนี้จากการซื้อ-ขายหลักทรัพย์)</td>    
                   <td colspan="1" width="5%" style="text-align:  right"><font color="{{($rs->OTHER_RATIO < 0) ? 'red':'#000'}}">{{number_format($rs->OTHER_RATIO, 2)}}</font></td>
                   <!--td colspan="1" width="10%" style="text-align: right">{{number_format($rs->OTHER_MB, 2)}}</td-->  
                   <td colspan="1" width="8%" style="text-align:  right"><font color="{{($rs->OTHER_B < 0) ? 'red':'#000'}}">{{number_format($rs->OTHER_B, 2)}}</font></td>   
            </tr>
            <tr>
                   <td colspan="1" width="30%" style="text-align: left"><font size="4" color="#a5a5a5"><i>ได้แก่ลูกหนี้จากการซื้อ/ขายหลักทรัพย์/เงินปันผลลูกหนี้/เจ้าหนี้อื่นๆ และเงินสมทบรอการจัดสรร</i></font></td>    
                   <td colspan="1" width="5%" style="text-align:  right"></td>
                   <!--td colspan="1" width="10%" style="text-align: right"></td-->  
                   <td colspan="1" width="8%" style="text-align:  right"></td>   
            </tr>
            <tr>
                   <td colspan="1" width="30%" style="text-align: center"><b>รวม</b></td>    
                   <td colspan="1" width="5%" style="text-align:  right"><font color="{{($rs->SUM_RATIO < 0) ? 'red':'#000'}}"><b>{{number_format($rs->SUM_RATIO, 2)}}</b></font></td>
                   <!--td colspan="1" width="10%" style="text-align: right"><b>{{number_format($rs->SUM_MB, 2)}}</b></td-->  
                   <td colspan="1" width="8%" style="text-align:  right"><font color="{{($rs->SUM_B < 0) ? 'red':'#000'}}"><b>{{number_format($rs->SUM_B, 2)}}</b></font></td>   
            </tr>

            <tr>
                   <td colspan="1" width="30%" style="text-align: center">มูลค่าสินทรัพย์สุทธิ</td>    
                   <td colspan="1" width="5%" style="text-align:  right"></td>
                   <!--td colspan="1" width="10%" style="text-align: right">{{number_format($rs->NAV_MB, 2)}}</td-->  
                   <td colspan="1" width="8%" style="text-align:  right"><font color="{{($rs->NAV_B < 0) ? 'red':'#000'}}">{{number_format($rs->NAV_B, 2)}}</font></td>   
            </tr>
            <tr>
                   <td colspan="1" width="30%" style="text-align: center">มูลค่าสินทรัพย์สุทธิต่อหน่วย</td>    
                   <td colspan="1" width="5%" style="text-align:  right"></td>
                   <!--td colspan="1" width="10%" style="text-align: right"></td-->  
                   <td colspan="1" width="8%" style="text-align:  right"><font color="{{($rs->NAV_UNIT < 0) ? 'red':'#000'}}">{{number_format($rs->NAV_UNIT, 4)}}</font></td>   
            </tr>
            <tr>
                   <td colspan="1" width="30%" style="text-align: center">อัตราผลตอบแทนรายเดือน %</td>    
                   <td colspan="1" width="5%" style="text-align:  right"></td>
                   <!--td colspan="1" width="10%" style="text-align: right"></td-->  
                   <td colspan="1" width="8%" style="text-align:  right"><font color="{{($rs->YM < 0) ? 'red':'#000'}}">{{number_format($rs->YM, 2)}}</font></td>   
            </tr>
            <tr>
                   <td colspan="1" width="30%" style="text-align: center">อัตราผลตอบแทนสะสม %</td>    
                   <td colspan="1" width="5%" style="text-align:  right"></td>
                   <!--td colspan="1" width="10%" style="text-align: right"></td-->  
                   <td colspan="1" width="8%" style="text-align:  right"><font color="{{($rs->YC < 0) ? 'red':'#000'}}">{{number_format($rs->YC, 2)}}</font></td>   
            </tr>
            @break
            @endforeach
        </tbody>
    </table>           
</div>

<div id="mini_table4" style="{{$table4show}}">
     <table class="table table-bordered"  width="400px" id="table_bond_nav" cellspacing="0">
        <thead>
            @foreach($data['equity_nav'] as $rowset =>$item)
            <tr>
                <th colspan="6" style="text-align: center">การลงทุนตราสารทุน</th>
            </tr>
            <tr>
                <th colspan="6" style="text-align: center"> การลงทุน ณ. <font color="red">{{intval($item->DD)}} {{toThaiShortMonth($item->MM)}} {{$item->YYYY}}</font></th>
            </tr>
            <tr>
               <th colspan="1" rowspan="2" style="text-align: center">ประเภทตราสาร</th>
               <th colspan="2" rowspan="1" style="text-align: center">นโยบายการลงทุนตราสารทุน</th>
               <th colspan="3" rowspan="2" style="text-align: center"> กราฟ </th>
            </tr>   
            <tr>
                <th colspan="1" style="text-align: center">สัดส่วน (%)</th>
                <!--th colspan="1" style="text-align: center">ล้าน</th-->
                <th colspan="1" style="text-align: center"> บาท</th> 
            </tr>
            @break
            @endforeach
        </thead>

        <tbody>
           @foreach($data['equity_nav'] as $rowset =>$rs)
            <tr>
                   <td colspan="1" width="30%"  style="text-align: left">เงินฝากธนาคารพาณิชย์ </td>   
                   <td colspan="1" width="5%" style="text-align: right"><font color="{{($rs->DEPOSIT_RATIO < 0) ? 'red':'#000'}}"> 
                       {{number_format($rs->DEPOSIT_RATIO, 2)}}</font></td>
                   <!--td colspan="1" width="10%" style="text-align: right">{{number_format($rs->DEPOSIT_MB, 2)}}</td-->  
                   <td colspan="1" width="8%" style="text-align:  right">{{number_format($rs->DEPOSIT_B, 2)}}</td> 

                   <td colspan="3" rowspan="9" style="text-align: center">
                        
                       <div id="main_chart2" class="main_chart2"  style="width: 100%; padding: 10px;"> 
                       
                       </div>
                    </td>  
            </tr>
            <tr>
                   <td colspan="1" width="30%" style="text-align: left">ตราสารทุน(หุ้น)</td>    
                   <td colspan="1" width="10%" style="text-align: right"><font color="{{($rs->STOCK_RATIO < 0) ? 'red':'#000'}}">{{number_format($rs->STOCK_RATIO, 2)}}</font></td>
                   <!--td colspan="1" width="10%" style="text-align: right">{{number_format($rs->STOCK_MB, 2)}}</td-->  
                   <td colspan="1" width="8%" style="text-align:  right"><font color="{{($rs->STOCK_B < 0) ? 'red':'#000'}}">{{number_format($rs->STOCK_B, 2)}}</font></td>   
            </tr>
            
            <tr>
                   <td colspan="1" width="30%" style="text-align: left">อื่นๆ (ลูกหนี้และเจ้าหนี้จากการซื้อ-ขายหลักทรัพย์)</td>    
                   <td colspan="1" width="5%" style="text-align:  right"><font color="{{($rs->OTHER_RATIO < 0) ? 'red':'#000'}}">{{number_format($rs->OTHER_RATIO, 2)}}</font></td>
                   <!--td colspan="1" width="10%" style="text-align: right">{{number_format($rs->OTHER_MB, 2)}}</td-->  
                   <td colspan="1" width="8%" style="text-align:  right"><font color="{{($rs->OTHER_B < 0) ? 'red':'#000'}}">{{number_format($rs->OTHER_B, 2)}}</font></td>   
            </tr>
            <tr>
                   <td colspan="1" width="30%" style="text-align: left"><font size="4" color="#a5a5a5"><i>ได้แก่ลูกหนี้จากการซื้อ/ขายหลักทรัพย์/เงินปันผลลูกหนี้/เจ้าหนี้อื่นๆ และเงินสมทบรอการจัดสรร</i></font></td>    
                   <td colspan="1" width="5%" style="text-align:  right"></td>
                   <!--td colspan="1" width="10%" style="text-align: right"></td-->  
                   <td colspan="1" width="8%" style="text-align:  right"></td>   
            </tr>
            <tr>
                   <td colspan="1" width="30%" style="text-align: center"><b>รวม</b></td>    
                   <td colspan="1" width="5%" style="text-align:  right"><b>{{number_format($rs->SUM_RATIO, 2)}}</b></td>
                   <!--td colspan="1" width="10%" style="text-align: right"><b>{{number_format($rs->SUM_MB, 2)}}</b></td-->  
                   <td colspan="1" width="8%" style="text-align:  right"><font color="{{($rs->SUM_B < 0) ? 'red':'#000'}}"><b>{{number_format($rs->SUM_B, 2)}}</font></b></td>   
            </tr>

            <tr>
                   <td colspan="1" width="30%" style="text-align: center">มูลค่าสินทรัพย์สุทธิ</td>    
                   <td colspan="1" width="5%" style="text-align:  right"></td>
                   <!--td colspan="1" width="10%" style="text-align: right">{{number_format($rs->NAV_MB, 2)}}</td-->  
                   <td colspan="1" width="8%" style="text-align:  right"><font color="{{($rs->NAV_B < 0) ? 'red':'#000'}}">{{number_format($rs->NAV_B, 2)}}</font></td>   
            </tr>
            <tr>
                   <td colspan="1" width="30%" style="text-align: center">มูลค่าสินทรัพย์สุทธิต่อหน่วย</td>    
                   <td colspan="1" width="5%" style="text-align:  right"></td>
                   <!--td colspan="1" width="10%" style="text-align: right"></td-->  
                   <td colspan="1" width="8%" style="text-align:  right"><font color="{{($rs->NAV_UNIT < 0) ? 'red':'#000'}}">{{number_format($rs->NAV_UNIT, 4)}}</font></td>   
            </tr>
            <tr>
                   <td colspan="1" width="30%" style="text-align: center">อัตราผลตอบแทนรายเดือน %</td>    
                   <td colspan="1" width="5%" style="text-align:  right"></td>
                   <!--td colspan="1" width="10%" style="text-align: right"></td-->  
                   <td colspan="1" width="8%" style="text-align:  right"><font color="{{($rs->YM < 0) ? 'red':'#000'}}">{{number_format($rs->YM, 2)}}</font></td>   
            </tr>
            <tr>
                   <td colspan="1" width="30%" style="text-align: center">อัตราผลตอบแทนสะสม %</td>    
                   <td colspan="1" width="5%" style="text-align:  right"></td>
                   <!--td colspan="1" width="10%" style="text-align: right"></td-->  
                   <td colspan="1" width="8%" style="text-align:  right"><font color="{{($rs->YC < 0) ? 'red':'#000'}}">{{number_format($rs->YC, 2)}}</font></td>   
            </tr>
            @break
            @endforeach
        </tbody>
    </table>
                          
</div>
@endif 
 
