
<div>Showing {{$PageNumber}} to {{ (($PageSize * $PageNumber) > $totals? $totals:($PageSize * $PageNumber))  }} of {{ $totals }}</div>
<table class="table table-bordered"> 
   @if($data)
        <thead>
        <tr>
            <th colspan="10" style="text-align: center;">รายงานข้อมูล ณ วันที่ {{get_date_notime(date("Y-m-d H:i:s"))}}</th>
        </tr>
        <tr>
            <th  style="text-align: center;">#</th>
            <th  style="text-align: center;"></th>
            <th  style="text-align: left;">ประเภทรายงาน</th>
            <th  style="text-align: left;">ชื่อไฟล์</th>
            <th  style="text-align: left;">บริษัทจัดการ</th>
            <th  style="text-align: right; ">ขนาดไฟล์</th>        
        </tr>
        </thead>
    
        <tbody>
        @foreach($data as $index =>$field)
            @if(($index%2)==0)
                <tr>
            @else
                <tr class="odd">
            @endif
                    <td style="text-align: center"><input type="checkbox"  name="check_item_edit[]" value="{{$field->REFERENCE}}" class="item_checked" id="item_check_{{$field->REFERENCE}}" />
                    </td> 
                    <td style="text-align: center">
                       <a href="javascript:void(0);"  
                        data-id="{{$field->REFERENCE}}" class="mea_delete_by btn bg-color-red txt-color-white btn-xs">
                        <i class="glyphicon glyphicon-trash"></i></a>
                    </td>

                    <td>{{$field->REPORT_ID}}</td>    
                    <!--td><a href="{{$field->PATH}}">{{$field->REFERENCE}}</a></td-->    
                @if(File::exists(public_path('/contents/kcreport/' . $field->REFERENCE)))    
                    <td><a href="{{$field->PATH}}" target="_blank">{{$field->REFERENCE}}</a></td>   
                @else
                    <td>{{$field->REFERENCE}} <span style="color:red;">*** ไม่พบข้อมูลไฟล์ *** </span></td>  
                @endif     
                    <td style="text-align: left;">{{$field->SECURITIES_NAME}}</td>   
                @if(File::exists(public_path('/contents/kcreport/' . $field->REFERENCE)))
                    <td style="text-align: right;">{{ round(File::size(public_path('/contents/kcreport/' . $field->REFERENCE)) / 1024) }} KB</td>   
                @else
                    <td style="text-align: right;">0 KB</td>         
                @endif
                </tr>
        </tbody>
        @endforeach 
        <tfoot>
            <tr>
                <td colspan="10" style="text-align: center">
                    {!! $htmlPaginate !!}
                </td>
            </tr>
        </tfoot>
    @else
        <tbody>
            <tr>
                <td >ไม่พบรายการ</td>
            </tr>
        </tbody>
    @endif
</table>

<div>


</div>