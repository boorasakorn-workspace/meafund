
<div id="trading_table" style="display:block;">
<div>Showing {{$PageNumber}} to {{ (($PageSize * $PageNumber) > $totals? $totals:($PageSize * $PageNumber))  }} of {{ $totals }}</div>
<table class="table table-bordered">
    <thead>
    <tr>
        <th colspan="{{ ($max_columns + 4) }}" style="text-align: center">
            <p class="report-title">{{$TableTitle}} </p> <!-- รายงานผลประโยชน์การลงทุน -->
        </th>
    </tr>
    <tr>
        <th colspan="{{ ($max_columns + 4) }}" style="text-align: center">{!! $pretty_date !!}</th>
    </tr>
    <tr>
        <th rowspan="2" style="text-align: center">SECURITY NAME</th>
        <th rowspan="2" style="text-align: center">ISSUER</th>
        <th rowspan="2" style="text-align: center">MATURITY</th>
        <th rowspan="2" style="text-align: center">INVESTMENT TYPE</th>
        <th colspan="{{ $max_columns }}" style="text-align: center">UNREALIZED GAIN/ (LOSS) VALUE OF SECURITY (AS OF...)</th>
    </tr>
    <tr>
    <!-- เพิ่ม column วันเดอืนปีที่นี่ อย่าลืมเพิ่ม  ($max_columns + x)  -->
        <th>2016-03-30</th>
        <th>2016-04-23</th>
        <th>2016-05-8</th>
        <th>2017-05-1</th>
    </tr>
   
    </thead>
        <tbody>
            @php
            $J = 0.0;
            $N = 0.0;
            $O = 0.0;
            @endphp

            @foreach($data as $index =>$field)
            
            <tr>
                        
                <td class="showcol" style="text-align: center" nowrap>{{$field->SYMBOL}}</td>               <!-- SECURITIES_NAME บลจ -->
                <td class="showcol" style="text-align: center" nowrap>{{($field->ISSUER == null) ? '' : $field->ISSUER }}</td>       <!-- ISSUER -->
                <td class="showcol" style="text-align: left"   nowrap>{{$field->MAT}}</td>                 <!-- MATURITY -->

                @if($field->TYPE == 'P')
                    <td class="showcol" style="text-align: center" nowrap>Purchase</td> <!-- INVESTMENT TYPE -->
                @elseif($field->TYPE == 'S')
                    <td class="showcol" style="text-align: center" nowrap>Sale</td> <!-- INVESTMENT TYPE -->
                @elseif($field->TYPE == 'I')
                    <td class="showcol" style="text-align: center" nowrap>Interest</td> <!-- INVESTMENT TYPE -->
                @else
                    <td class="showcol" style="text-align: center" nowrap>Maturity</td> <!-- INVESTMENT TYPE -->
                @endif

                 
                <!-- Months Start Loop GAIN/ (LOSS) -->
                <td style="text-align: right" nowrap>2</td>  
                <td style="text-align: right" nowrap>3</td>   
                <td style="text-align: right" >4</td>  
                <td style="text-align: right" >5</td> 
               
            </tr>
            @endforeach 
        </tbody>
    <tfoot>

        <tr>
            <td colspan="{{ ($max_columns + 4) }}">
                {!! $htmlPaginate !!}
            </td>
        </tr>
    </tfoot>
</table>
</div>

