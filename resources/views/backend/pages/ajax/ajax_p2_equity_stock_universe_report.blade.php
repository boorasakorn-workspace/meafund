

<div>Showing {{$PageNumber}} to {{ (($PageSize * $PageNumber) > $totals? $totals:($PageSize * $PageNumber))  }} of {{ $totals }}</div>
<table class="table table-bordered"> 
   
    <thead>
    <tr>
        <th colspan="10" style="text-align: center">รายงานข้อมูล ณ วันที่ {{get_date_notime(date("Y-m-d H:i:s"))}}</th>
    </tr>
    <tr>
        <th colspan="1" style="text-align: center"></th>
        <th colspan="1" style="text-align: center"></th>
        <th colspan="1" style="text-align: center"></th>
        <th colspan="1" style="text-align: center"></th>
        <th colspan="1" rowspan="1" style="text-align: center ">SET 100</th>
        <th colspan="1" rowspan="1" style="text-align: center">SET 50</th>
        <th colspan="2" style="text-align: center">UOBAM</th>
        <th colspan="2" style="text-align: center">KTAM</th>        
    </tr>

    <tr>
        <th style="text-align: center">ลำดับ</th>
        <th style="text-align: center">หมวดธุรกิจ</th>
        <th style="text-align: center">ชื่อหลักทรัพย์</th>
        <th style="text-align: center">ตัวย่อ</th>
        <th style="text-align: center"> </th>
        <th style="text-align: center"> </th>
        <th style="text-align: center">2H2559</th>
        <th style="text-align: center">
            <div class="top_row_line">
                <div>31/05/2559</div>
            </div>
            <div class="top_row_line">
                EQ
            </div></th>
        <th style="text-align: center">2H2559</th>
        <th style="text-align: center">
            <div class="top_row_line">
                <div>31/05/2559</div>
            </div>
            <div class="top_row_line">
                EQ
            </div></th>
        
    </tr>
    </thead>
   
        <tbody>
             <tr>
               <!-- TBL_P2_EQUITY_INDEX->INDUSTRIAL -->
               <td colspan="10" style="text-align: center"> <strong style="font-size: 18px;">กลุ่มอุสาหกรรมเกษตรและอุสาหกรรมอาหาร </strong></td>
             </tr>
             <tr>
                <td style="text-align: center">1</td>  <!--  -->
                <td>อาหารและเครื่องดื่ม</td>   <!-- {TBL_P2_EQUITY_INDEX->BU} -->
                <td>บมจ. คาราบาวกรุ๊ป</td>   <!-- {TBL_P2_STOCK_UNIVERSE->COMPANY_NAME} -->
                <td style="text-align: center">CBG</td>   <!-- เอาค่าฟิลด์ SYMBOL ไปเช็คที่ TBL_P2_EQUITY_INDEX  แล้วเอาค่าในฟิลด์ COMP_NAME มาแสดง -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>   <!-- เอาค่าฟิลด์ SYMBOL ไปเช็คที่ TBL_P2_EQUITY_INDEX  หากค่าในฟิลด์ SET_INDEX50 = Y ให้แสดงผลเครื่องหมายถูก √ ในช่องนี้ -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>  <!-- เอาค่าฟิลด์ SYMBOL ไปเช็คที่ TBL_P2_EQUITY_INDEX  หากค่าในฟิลด์ SET_INDEX100 = Y ให้แสดงผลเครื่องหมายถูก √ ในช่องนี้ -->
                <td style="text-align: center"></td>  <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>  <!--  -->
                <td style="text-align: center"></td>  <!--  -->
                 <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></td>  <!--  -->


            </tr>

             <tr>
                <td style="text-align: center">2</td>  <!--  -->
                <td>อาหารและเครื่องดื่ม</td>           <!--  -->
                <td>บริษัท เจริญโภคภัณฑ์อาหาร จำกัด (มหาชน)</td>   <!--  -->
                <td style="text-align: center">CPF</td>   <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>   <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>  <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>  <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>  <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>  <!--  -->
                 <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></td>  <!--  -->


            </tr>
            <tr>
                <td style="text-align: center">3</td>  <!--  -->
                <td>อาหารและเครื่องดื่ม</td>           <!--  -->
                <td>บมจ. อิชิตัน กรุ๊ป</td>   <!--  -->
                <td style="text-align: center">ICHI</td>   <!--  -->
                <td style="text-align: center"> </td>   <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>  <!--  -->
                <td style="text-align: center"></td>  <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>  <!--  -->
                <td style="text-align: center"></td>  <!--  -->
                 <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></td>  <!--  -->

            </tr>
             <tr>
                <td style="text-align: center">4</td>  <!--  -->
                <td>อาหารและเครื่องดื่ม</td>           <!--  -->
                <td>บริษัท เอ็มเค เรสโตรองต์ กรุ๊ป (มหาชน)</td>   <!--  -->
                <td style="text-align: center">M</td>   <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>   <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>  <!--  -->
                <td style="text-align: center"></td>  <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>  <!--  -->
                <td style="text-align: center"></td>  <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></td>  <!--  -->

            </tr>
             <tr>
                <td style="text-align: center">5</td>  <!--  -->
                <td>อาหารและเครื่องดื่ม</td>           <!--  -->
                <td>บริษัท ไมเนอร์ อินเตอร์เนชั่นแนล จำกัด (มหาชน)</td>   <!--  -->
                <td style="text-align: center">MINT</td>   <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>   <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>  <!--  -->
                <td style="text-align: center"></td>  <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>  <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>  <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></td>  <!--  -->

            </tr>
             <tr>
                <td style="text-align: center">6</td>  <!--  -->
                <td>อาหารและเครื่องดื่ม</td>           <!--  -->
                <td>บริษัท ไทร-ยูเนี่ยน โฟรเซ่น โปรดักส์ (มหาชน)</td>   <!--  -->
                <td style="text-align: center">TU</td>   <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>   <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>  <!--  -->
                <td style="text-align: center"></td>  <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>  <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></i></td>  <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></td>  <!--  -->

            </tr>
            
            <tr>
               <td colspan="10" style="text-align: center"><strong style="font-size: 18px;"> กลุ่มอุสาหกรรมธุรกิจการเงิน </strong></td>
            </tr>
            <tr>
                <td style="text-align: center">7</td>  <!--  -->
                <td>เงินทุหลักทรัพย์</td>           <!--  -->
                <td>กรุ๊ปลีส จำกัด (มหาชน)</td>   <!--  -->
                <td style="text-align: center">GL</td>   <!--  -->
                <td style="text-align: center"> </td>   <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></td>  <!--  -->
                <td style="text-align: center"></td>  <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></td>  <!--  -->
                <td></td>  <!--  -->
                 <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></td>  <!--  -->


            </tr>
            <tr>
                <td style="text-align: center">8</td>  <!--  -->
                <td >เงินทุหลักทรัพย์</td>           <!--  -->
                <td>บริษ้ทิบัตรกรุงไทย จำกัด (มหาชน)</td>   <!--  -->
                <td style="text-align: center">KTC</td>   <!--  -->
                <td style="text-align: center"> </td>   <!--  --> 
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></td>  <!--  -->
                <td style="text-align: center"></td>  <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></td>  <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></td>  <!--  -->
                <td style="text-align: center"><i class="glyphicon glyphicon-ok glyphicon-lightgreen"></td>  <!--  -->

            </tr>
        </tbody>
    <tfoot>
        <tr>
            <td colspan="10" style="text-align: center">
                {!! $htmlPaginate !!}
            </td>
        </tr>
    </tfoot>
</table>

<div>


</div>