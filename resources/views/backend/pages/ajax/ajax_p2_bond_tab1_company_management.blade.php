<table class="table  table-bordered"  >

    <thead>

    <tr>
        <th style="width:5%;text-align: center" id="index_th">
            <input type="checkbox" id="mainCheck" />
        </th>
        <th style="width:8%;text-align: center">Action</th>
        <!--th data-class="expand">รหัสบริษัทจัดการหลักทรัพย์</th-->
        <th data-class="expand">ชื่อย่อ</th>
        <!--th data-hide="phone">ชื่อบริษัท</th-->
        <th data-class="expand">ชื่อบริษัท</th>
        <th data-class="expand">ที่อยู่</th>
        <th style="width:10%;">เบอร์โทรศัพท์</th>
        <th style="width:10%;">เบอร์โทรสาร</th>
        <th style="width:10%;">วันที่เริ่มต้น</th>
        <th style="width:10%;">วันที่สิ้นสุด</th>
        
        <!--th style="width:10%;">สัดส่วนตรา สารหนี้ (ขั้น ต่ำ)</th>
        <th style="width:10%;">สัดส่วนตรา สารหนี้ (ขั้น สูง)</th-->

    </tr>
    </thead>

    <tbody>
      @if($data)
        @foreach($data as $item)
            <tr>
                <td style="text-align: center"><input type="checkbox"  name="check_item_edit[]" value="{{$item->NAME_SHT}}" class="item_checked" id="item_check_{{$item->NAME_SHT}}" />
                </td>
                <td style="text-align: center">
                    <a href="/admin/BondCompany/edit/{{$item->NAME_SHT}}" class="btn btn-primary btn-xs"><i class="fa fa-gear"></i></a>
                    <a href="javascript:void(0);"  data-id="{{$item->NAME_SHT}}" class="mea_delete_by btn bg-color-red txt-color-white btn-xs"> <i class="glyphicon glyphicon-trash"></i></a>
                </td>
                <!--td>3</td-->
                <td>{{$item->NAME_SHT}}</td>
                <td>{{$item->SECURITIES_NAME}}</td>
                <td>{{$item->ADDRESS}}</td>
                <td>{{$item->PHONE}}</td>
                <td>{{$item->FAX}}</td>
                <td>{{toThaiDateTime($item->START_DATE, false)}}</td>
                <td>{{toThaiDateTime($item->END_DATE, false)}}</td>
    
    
            </tr>
        @endforeach

    @endif

    </tbody>
    <tfoot>
    <tr>
        <td colspan="11">
            {!! $htmlPaginate !!}
        </td>
    </tr>
    </tfoot>
</table>