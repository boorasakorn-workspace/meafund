

<div>Showing {{$PageNumber}} to {{ (($PageSize * $PageNumber) > $totals? $totals:($PageSize * $PageNumber))  }} of {{ $totals }}</div>
<table class="table table-bordered">
   

    <thead>
    <!--tr>
        <th colspan="16" style="text-align: center">รายงานข้อมูล ณ วันที่ {{get_date_notime(date("Y-m-d H:i:s"))}}</th>
    </tr-->
    <tr>
        <th colspan="46" style="text-align: center">
            <strong style="font-size: 20px;">{{$TableTitle}} </strong> <!-- รายงานความเคลื่อนไหวตราสารทุน -->
             <br/>
            <strong style="font-size: 16px;">วันที่ {{get_date_notime(date("Y-m-d H:i:s"))}} </strong>
             <br/>
            <strong style="font-size: 16px;"> </strong>
        </th>
    </tr>
    <tr>
        <th rowspan="2" style="text-align: center;" nowrap>กลุ่มอุตสาหกรรม</th>
        <th rowspan="2" style="text-align: center;" nowrap>หลักทรัพย์</th>
        <th rowspan="2" style="text-align: center;" nowrap>จํานวนหน่วย</th>
        <th colspan="2" style="text-align: center;" nowrap>วันที่ลงทุน</th>
        <th colspan="2" style="text-align: center;" nowrap>ราคาเฉลี่ยต่อเดือน</th>

        <th rowspan="2" style="text-align: center">จํานวนเงินลงทุน(ล้านบาท)</th>
        <th colspan="10" style="text-align: center">ราคาปิดของหุ้นเดือนมิถุนายน 2559 (วันท่ี)</th>

        <th style="text-align: center" nowrap>จำนวนเงินลงทุน</th>
        <th rowspan="2" style="text-align: center" nowrap>มูลค่ารวม</th>

        <th rowspan="2" style="text-align: center"nowrap>ร้อยล่ะของหุ้น</th>
        <th rowspan="2" style="text-align: center">เงินลงทุน(เพิ่ม/ลด)</th>
        <th rowspan="2" style="text-align: center">EARNING PURCHASE</th>
        <th rowspan="2" style="text-align: center">PRICE EARNING RATIO</th>
        <th rowspan="2" style="text-align: center">PRICE PER BLOCK VALUE</th>
        <th rowspan="2" style="text-align: center">DIVIDEND YIELD %</th>
    </tr>
        
    </tr>
    <tr>
        
        <th style="text-align: center">เริ่มลงทุน</th>
        <th style="text-align: center">วันสุดท้าย</th>
        <th style="text-align: center">วันแรก</th>
        <th style="text-align: center">วันล่าสุด</th>
        
        <!-- ... LOOP DAYS -->
        <th style="text-align: center">1</th>
        <th style="text-align: center">2</th>
        <th style="text-align: center">3</th>
        <th style="text-align: center">4</th>
        <th style="text-align: center">5</th>
        <th style="text-align: center">6</th>
        <th style="text-align: center">7</th>
        <th style="text-align: center">8</th>
        <th style="text-align: center">9</th>
        <th style="text-align: center">10</th>

        <!-- ... -->
        <th style="text-align: center">30 มิ.ย. 2560</th>
       
    </thead>
   
        <tbody>
            <!--START: FAKE DATA -->  
            <tr>
                <!--td >ไม่พบรายการ</td-->  
                <th>                          1</th> <!-- Symbol -->
                <th style="text-align: right">2</th>       <!-- PClosed -->
                <th style="text-align: right">3</th>   <!-- P/E -->
                <th style="text-align: right">4</th> <!--P/BV-->
                <th style="text-align: right">5</th> <!-- Div Yield -->
                <th style="text-align: right">6</th> <!-- EPS-->

                <th style="text-align: right">7</th>       <!-- PClosed -->
                <th style="text-align: right">8</th>   <!-- P/E -->
                <th style="text-align: right">9</th> <!--P/BV-->
                <th style="text-align: right">10</th> <!-- Div Yield -->
                <th style="text-align: right">11</th> <!-- EPS-->
                <th style="text-align: right">12</th>       <!-- PClosed -->
                <th style="text-align: right">13</th>   <!-- P/E -->
                <th style="text-align: right">14</th> <!--P/BV-->
                <th style="text-align: right">15</th> <!-- Div Yield -->
                <th style="text-align: right">16</th> <!-- EPS-->
                <th style="text-align: right">17</th> <!-- EPS-->
                <th style="text-align: right">18</th> <!-- EPS-->

                <th style="text-align: right">19</th> <!-- EPS-->
                <th style="text-align: right">20</th> <!-- EPS-->
                <th style="text-align: right">21</th> <!-- EPS-->
                <th style="text-align: right">22</th> <!-- EPS-->
                <th style="text-align: right">23</th> <!-- EPS-->
                <th style="text-align: right">24</th> <!-- EPS-->
                <th style="text-align: right">25</th> <!-- EPS-->
                <th style="text-align: right">26</th> <!-- EPS-->

            </tr>

           
            <!-- END: FAKE -->  
            
        </tbody>
    <tfoot>
        <tr>
            <td colspan="16">
                {!! $htmlPaginate !!}
            </td>
        </tr>
    </tfoot>
</table>

<div>


</div>