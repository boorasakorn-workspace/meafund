

<!--p class="report-period">
   {!! $pretty_date !!}
  
</p-->

<div id="trading_table" style="display:block;">
<div>Showing {{$PageNumber}} to {{ (($PageSize * $PageNumber) > $totals? $totals:($PageSize * $PageNumber))  }} of {{ $totals }}</div>
<table class="table table-bordered">
    <thead>
    <tr>
        <th colspan="19" style="text-align: center">
            <p class="report-title">{{$TableTitle}} </p> <!-- รายงานความเคลื่อนไหวตราสารทุน -->
        </th>
    </tr>
    
    <tr>
        <th colspan="19" style="text-align: center">{!! $pretty_date !!}</th>
    </tr>
    <tr>
        @if($fields[0] !='x' && $fields[1] !='x' && $fields[2] !='x' && $fields[3] !='x' && $fields[4] !='x')
           <th colspan="5" class="hiddencol" style="text-align: center">  </th>
        @else
           <th colspan="{{$fields_poperties[0]['length']}}" class="showcol" style="text-align: center">  </th>
        @endif

        @if($fields[5] !='x' && $fields[6] !='x')
           <th colspan="2" class="hiddencol" style="text-align: center">เงินปันผล</th>
        @else
           <th colspan="{{$fields_poperties[1]['length']}}" class="showcol" style="text-align: center">เงินปันผล</th>
           <!--th colspan="2" class="showcol" style="text-align: center">เงินปันผล</th-->
        @endif
        
        @if ($fields[7] !='x' && $fields[8] !='x' && $fields[9] !='x' && $fields[10] !='x')
           <th colspan="4" class="hiddencol"  style="text-align: center">ข้อมูลการซื้อ</th>
        @else
           <th colspan="{{$fields_poperties[2]['length']}}" class="showcol"  style="text-align: center">ข้อมูลการซื้อ</th>
           <!--th colspan="4" class="showcol"  style="text-align: center">ข้อมูลการซื้อ</th-->
        @endif
        
        @if($fields[11] !='x' && $fields[12] !='x' && $fields[13] !='x' && $fields[14] !='x' && $fields[15] !='x')
           <th colspan="5"  class="hiddencol" style="text-align: center" >ข้อมูลการขาย</th>
        @else
           <th colspan="{{$fields_poperties[3]['length']}}"  class="showcol" style="text-align: center" >ข้อมูลการขาย</th>
        @endif
        
        @if ($fields[7] !='x' && $fields[8] !='x' && $fields[9] !='x' && $fields[10] !='x')
           <th colspan="3" class="hiddencol"  style="text-align: center">คงเหลือราคาทุน</th>
        @else
        <th colspan="3" class="showcol"  style="text-align: center">คงเหลือราคาทุน</th>
           <!--th colspan="4" class="showcol"  style="text-align: center">ข้อมูลการซื้อ</th-->
        @endif
    </tr>
    <tr>
        @if($fields)
             
             
            <th class={{($fields[0] =='x') ? "showcol" : "hiddencol"}} >วันเดือนปี</th>
            <th class={{($fields[1] =='x') ? "showcol" : "hiddencol"}} >ชื่อ</th>
            <th class={{($fields[2] =='x') ? "showcol" : "hiddencol"}} >บลจ </th>
            <th class={{($fields[3] =='x') ? "showcol" : "hiddencol"}} >กลุ่มอุตสาหกรรม</th>
            <th class={{($fields[4] =='x') ? "showcol" : "hiddencol"}} >หมวดธุรกิจ</th>

            <th class={{($fields[5] =='x') ? "showcol" : "hiddencol"}} >ผลปันผล</th>
            <th class={{($fields[6] =='x') ? "showcol" : "hiddencol"}} >จำนวนเงิน</th>

            <th class={{($fields[7] =='x') ? "showcol" : "hiddencol"}} >หน่วยซื้อ</th>
            <th class={{($fields[8] =='x') ? "showcol" : "hiddencol"}} >ราคาซื้อต่อหน่วย </th>
            <th class={{($fields[9] =='x') ? "showcol" : "hiddencol"}} >ยอดซื้อ (บาท)</th>
            <th class={{($fields[10] =='x') ? "showcol" : "hiddencol"}} >คงเหลือ (หน่วย)</th>

            <th class={{($fields[11] =='x') ? "showcol" : "hiddencol"}} >หน่วยขาย</th>
            <th class={{($fields[12] =='x') ? "showcol" : "hiddencol"}} >ราคาขายต่อหน่วย</th>
            <th class={{($fields[13] =='x') ? "showcol" : "hiddencol"}} >ยอดขาย (บาท) </th>
            <th class={{($fields[14] =='x') ? "showcol" : "hiddencol"}} >กำไร-ขาดทุน</th>
            <th class={{($fields[15] =='x') ? "showcol" : "hiddencol"}} >คงเหลือ (หน่วย)</th>
            <th class={{($fields[7] =='x') ? "showcol" : "hiddencol"}} >จำนวนหน่วย</th>
            <th class={{($fields[8] =='x') ? "showcol" : "hiddencol"}} >ราคาซื้อต่อหน่วย </th>
            <th class={{($fields[9] =='x') ? "showcol" : "hiddencol"}} >จำนวนเงิน</th>
        @else

            <th>วันเดือนปี</th>
            <th>ชื่อ</th>
            <th>บลจ</th>
            <th>กลุ่มอุตสาหกรรม</th>
            <th>หมวดธุรกิจ</th>

            <th>ผลปันผล</th>
            <th>จำนวนเงิน</th>
            
            <th>หน่วยซื้อ</th>
            <th>ราคาซื้อต่อหน่วย </th>
            <th>ยอดซื้อ (บาท)</th>
            <th>คงเหลือ (หน่วย)</th>

            <th>หน่วยขาย</th>
            <th>ราคาขายต่อหน่วย</th>
            <th>ยอดขาย (บาท) </th>
            <th>กำไร-ขาดทุน</th>
            <th>คงเหลือ (หน่วย)</th>
            <th>จำนวนหน่วย</th>
            <th>ราคาซื้อต่อหน่วย </th>
            <th>จำนวนเงิน</th>
        @endif
          

    </tr>
    </thead>
   
        <tbody>
            @php
            $utotal = 0.0;
            $ptotal = 0.0;
            $J = 0.0;
            $N = 0.0;
            $O = 0.0;
            @endphp

            @foreach($data as $index =>$field)
            <tr>
                @php
                $utotal += $field->UNIT_PURCHASE;
                $utotal -=$field->UNIT_SALE;
                
                $ptotal += $field->TOTAL_PURCHASE;
                $ptotal -=$field->TOTAL_SALE;
                
                @endphp
                <td class={{($fields[0] =='x') ? "showcol" : "hiddencol"}} style="text-align: right" >{{$field->TRANS_DATE}}</td> <!-- TRANS_DATE วันเดือนปี -->
                <td class={{($fields[1] =='x') ? "showcol" : "hiddencol"}} style="text-align: center" >{{$field->SYMBOL}}</td>       <!-- SYMBOL ชื่อ -->
                <td class={{($fields[2] =='x') ? "showcol" : "hiddencol"}} style="text-align: left" >{{$field->SECURITIES_NAME}}</td>   <!-- SECURITIES_NAME บลจ -->
                <td class={{($fields[3] =='x') ? "showcol" : "hiddencol"}} style="text-align: left" >{{$field->INDUSTRIAL}}</td>   <!-- INDUSTRIAL กลุ่มอุตสาหกรรม -->
                <td class={{($fields[4] =='x') ? "showcol" : "hiddencol"}} style="text-align: left" >{{$field->BU}}</td> <!--หมวดธุรกิจ-->

                <td class={{($fields[5] =='x') ? "showcol" : "hiddencol"}} style="text-align: right" >{{$field->UNIT_DIV}}</td> <!-- ผลปันผล -->
                <td class={{($fields[6] =='x') ? "showcol" : "hiddencol"}} style="text-align: right" >{{$field->TOTAL_DIV}}</td> <!-- จำนวนเงิน -->

                <td class={{($fields[7] =='x') ? "showcol" : "hiddencol"}} style="text-align: right">{{number_format((float)$field->UNIT_PURCHASE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                <td class={{($fields[8] =='x') ? "showcol" : "hiddencol"}} style="text-align: right" >{{number_format((float)$field->PRICE_PURCHASE,4, '.', ',')}}</td>     <!-- าคาซื้อต่อหน่วย -->
                <td class={{($fields[9] =='x') ? "showcol" : "hiddencol"}} style="text-align: right" >{{number_format((float)$field->TOTAL_PURCHASE,2, '.', ',')}}</td>   <!-- ยอดซื้อ (บาท) -->
                @if($field->TYPE =='P')
                  <td class={{($fields[10] =='x') ? "showcol" : "hiddencol"}} style="text-align: right">{{number_format((float)$field->P_SUM,2, '.', ',')}}</td> <!-- ตงเหลือ (หน่วย) -->
                @else
                  <td class={{($fields[10] =='x') ? "showcol" : "hiddencol"}} style="text-align: right"> </td> <!-- ตงเหลือ (หน่วย) -->
                @endif 
                <td class={{($fields[11] =='x') ? "showcol" : "hiddencol"}} style="text-align: right">{{number_format((float)$field->UNIT_SALE,2, '.', ',')}}</td>        <!--หน่วยขาย -->
                <td class={{($fields[12] =='x') ? "showcol" : "hiddencol"}} style="text-align: right">{{number_format((float)$field->PRICE_SALE,4, '.', ',')}}</td>  <!-- ราคาขายต่อหน่วย-->
                <td class={{($fields[13] =='x') ? "showcol" : "hiddencol"}} style="text-align: right">{{number_format((float)$field->TOTAL_SALE,2, '.', ',')}}</td>   <!-- ยอดขาย (บาท) -->
                @if((float)$field->PROFIT_LOSS >=0) 
                   <td class={{($fields[14] =='x') ? "showcol" : "hiddencol"}} style="text-align: right">
                   {{number_format((float)$field->PROFIT_LOSS,2, '.', ',')}}</td>     <!-- กำไร-ขาดทุน -->
                @else
                   <td class={{($fields[14] =='x') ? "showcol" : "hiddencol"}} style="text-align: right; color: red">
                   {{number_format((float)$field->PROFIT_LOSS,2, '.', ',')}}</td>     <!-- กำไร-ขาดทุน -->

                @endif   
                
                @if($field->TYPE =='S')
                   <td class={{($fields[15] =='x') ? "showcol" : "hiddencol"}} style="text-align: right">{{number_format((float)$field->REMAIN_UNIT,2, '.', ',')}}</td>  <!-- คงเหลือ (หน่วย)-->
                @else
                   <td class={{($fields[15] =='x') ? "showcol" : "hiddencol"}} style="text-align: right"></td>  <!-- คงเหลือ (หน่วย)-->
             
                @endif

                <td class="showcol" style="text-align: right">{!! number_format((float)$utotal,2, '.', ',') !!}</td>        <!-- หน่วยซื้อ -->
                <td class="showcol" style={{(number_format((float)$field->PRICE_PURCHASE,4, '.', ',') > 0) ?"text-align: right":"text-align: right; color: red" }}>{{(number_format((float)$field->PRICE_PURCHASE,4, '.', ',') > 0) ? number_format((float)$field->PRICE_PURCHASE,4, '.', ',') : number_format((float)$field->PRICE_SALE,4, '.', ',')}}</td>
                <td class="showcol" style="text-align: right">{!! number_format((float)$ptotal,2, '.', ',') !!}</td>
                @php
                  $J += $field->TOTAL_PURCHASE;
                  $N += $field->TOTAL_SALE;
                  $O += $field->PROFIT_LOSS;
                @endphp
            </tr>
            @endforeach 

        <!-- ยอดรวม แต่ล่ะ Page --> 
        <tr>
            <td  class={{($fields[0] =='x') ? "showcol" : "hiddencol"}}>
            </td>

            <td  class={{($fields[1] =='x') ? "showcol" : "hiddencol"}}>
            </td>

            <td  class={{($fields[2] =='x') ? "showcol" : "hiddencol"}}>
            </td>

            <td  class={{($fields[3] =='x') ? "showcol" : "hiddencol"}}>
            </td>

            <td  class={{($fields[4] =='x') ? "showcol" : "hiddencol"}}>
            </td>

            <td  class={{($fields[5] =='x') ? "showcol" : "hiddencol"}}>     
            </td>
           
            <td  class={{($fields[6] =='x') ? "showcol" : "hiddencol"}}>     
            </td>

            <td  class={{($fields[7] =='x') ? "showcol" : "hiddencol"}}>     
            </td>
            
            <td  class={{($fields[8] =='x') ? "showcol" : "hiddencol"}} colspan="1" style="text-align: right; color: blue">
                <b>รวม</b>
            </td>

            @if($J < 0)
            <td  class={{($fields[9] =='x') ? "showcol" : "hiddencol"}} style="text-align: right; color: red">
                <b>{!! number_format((float)$J,2, '.', ',') !!}</b>
            </td>
            @else
            <td  class={{($fields[9] =='x') ? "showcol" : "hiddencol"}} style="text-align: right; color: blue">
                <b>{!! number_format((float)$J,2, '.', ',') !!}</b>
            </td>
            @endif

            
            <td  class={{($fields[10] =='x') ? "showcol" : "hiddencol"}}>     
            </td>
            <td  class={{($fields[11] =='x') ? "showcol" : "hiddencol"}}>     
            </td>
            <td  class={{($fields[12] =='x') ? "showcol" : "hiddencol"}}>     
            </td>
            
            @if($N < 0)
            <td  class={{($fields[13] =='x') ? "showcol" : "hiddencol"}} style="text-align: right; color: red">
                <b>{!! number_format((float)$N,2, '.', ',') !!}</b>
            </td>
            @else
            <td  class={{($fields[13] =='x') ? "showcol" : "hiddencol"}} style="text-align: right; color: blue">
                <b>{!! number_format((float)$N,2, '.', ',') !!}</b>
            </td>
            @endif
             
            @if($O < 0) 
            <td  class={{($fields[14] =='x') ? "showcol" : "hiddencol"}} style="text-align: right; color: red">
                <b>{!! number_format((float)$O,2, '.', ',') !!}</b>
            </td>
            @else
            <td  class={{($fields[14] =='x') ? "showcol" : "hiddencol"}} style="text-align: right; color: blue">
                <b>{!! number_format((float)$O,2, '.', ',') !!}</b>
            </td>
            @endif

            <td  class={{($fields[15] =='x') ? "showcol" : "hiddencol"}}>
            
            <td class="showcol" style="text-align: right"></td>

            <td class="showcol" style="text-align: right"></td>

            <td class="showcol" style="text-align: right"></td>
            </td>
        </tr>

        <!-- ยอดรวม ทั้งหมด Grand Total--> 
        @if($grandtoal) 
        @foreach($grandtoal as $rs =>$field)
        <tr>
            <td  class={{($fields[0] =='x') ? "showcol" : "hiddencol"}}>
            </td>

            <td  class={{($fields[1] =='x') ? "showcol" : "hiddencol"}}>
            </td>

            <td  class={{($fields[2] =='x') ? "showcol" : "hiddencol"}}>
            </td>

            <td  class={{($fields[3] =='x') ? "showcol" : "hiddencol"}}>
            </td>

            <td  class={{($fields[4] =='x') ? "showcol" : "hiddencol"}}>
            </td>

            <td  class={{($fields[5] =='x') ? "showcol" : "hiddencol"}}>     
            </td>
           
            <td  class={{($fields[6] =='x') ? "showcol" : "hiddencol"}}>     
            </td>

            <td  class={{($fields[7] =='x') ? "showcol" : "hiddencol"}}>     
            </td>
            
            <td  class={{($fields[8] =='x') ? "showcol" : "hiddencol"}} colspan="1" style="text-align: right; color: blue">
                <b>รวมทั้งหมด</b>
            </td>

            @if($field->J < 0)
            <td  class={{($fields[9] =='x') ? "showcol" : "hiddencol"}} style="text-align: right; color: red">
                <b>{{ number_format((float)$field->J,2, '.', ',') }}</b>
            </td>
            @else
            <td  class={{($fields[9] =='x') ? "showcol" : "hiddencol"}} style="text-align: right; color: blue">
                <b>{{ number_format((float)$field->J,2, '.', ',') }}</b>
            </td>
            @endif

            
            <td  class={{($fields[10] =='x') ? "showcol" : "hiddencol"}}>     
            </td>
            <td  class={{($fields[11] =='x') ? "showcol" : "hiddencol"}}>     
            </td>
            <td  class={{($fields[12] =='x') ? "showcol" : "hiddencol"}}>     
            </td>
            
            @if($field->N < 0)
            <td  class={{($fields[13] =='x') ? "showcol" : "hiddencol"}} style="text-align: right; color: red">
                <b>{{ number_format((float)$field->N,2, '.', ',') }}</b>
            </td>
            @else
            <td  class={{($fields[13] =='x') ? "showcol" : "hiddencol"}} style="text-align: right; color: blue">
                <b>{{ number_format((float)$field->N,2, '.', ',') }}</b>
            </td>
            @endif
             
            @if($field->O < 0) 
            <td  class={{($fields[14] =='x') ? "showcol" : "hiddencol"}} style="text-align: right; color: red">
                <b>{{ number_format((float)$field->O,2, '.', ',') }}</b>
            </td>
            @else
            <td  class={{($fields[14] =='x') ? "showcol" : "hiddencol"}} style="text-align: right; color: blue">
                <b>{{ number_format((float)$field->O,2, '.', ',') }}</b>
            </td>
            @endif

            <td  class={{($fields[15] =='x') ? "showcol" : "hiddencol"}}>
            <td class="showcol" style="text-align: right"></td>

            <td class="showcol" style="text-align: right"></td>

            <td class="showcol" style="text-align: right"></td>

            </td>
        </tr>
        @endforeach 
        @endif

        
        </tbody>
    <tfoot>

        <tr>
            <td colspan="16">
                {!! $htmlPaginate !!}
            </td>
        </tr>
    </tfoot>
</table>
</div>




<!-- Chart -->
<!--div id="bar-chart-good-bad" class="chart" style="display:block;">
    
</div-->

