<div id="trading_table" style="display:block;">
<div>Showing {{$PageNumber}} to {{ (($PageSize * $PageNumber) > $totals? $totals:($PageSize * $PageNumber))  }} of {{ $totals }}</div>
<table class="table table-bordered">
    <thead>
    <tr>
        <th colspan="16" style="text-align: center">
            <p class="report-title">{{$TableTitle}} </p> <!-- รายงานความเคลื่อนไหวตราสารทุน -->
        </th>
    </tr>
    @if(strlen($SubTitle) > 2)
    <tr>
        <th colspan="12" style="text-align: center">{!! $SubTitle !!}</th>
    </tr>
    @endif
    <tr>
        <th colspan="12" style="text-align: center">{!! $pretty_date !!}</th>
    </tr>

    <tr>
        <th colspan="1" class="showcol" style="text-align: center"> No. </th>
        <th colspan="1" class="showcol" style="text-align: center"> ประเภททรัพย์สิน </th>
        <th colspan="1" class="showcol" style="text-align: center"> ประเภทตราสารหนี้ </th>  
        <th colspan="1" class="showcol" style="text-align: center"> ชื่อย่อตราสารหนี้ </th>  
        <th colspan="1" class="showcol" style="text-align: center"> ชื่อเต็มตราสารหนี้ </th>  
        <th colspan="1" class="showcol" style="text-align: center"> จำนวนหุ้น / มูลค่าที่ตราไว้ </th>  
        <th colspan="1" class="showcol" style="text-align: center"> ราคาทุน </th>  
        <th colspan="1" class="showcol" style="text-align: center"> ดอกเบี้ยค้างรับ </th>  
        <th colspan="1" class="showcol" style="text-align: center"> ราคายุติธรรม </th>     
        <th colspan="1" class="showcol" style="text-align: center"> อัตราส่วนของราคายุติธรรม (%)  </th>  
        <th colspan="1" class="showcol" style="text-align: center"> อัตราดอกเบี้ย </th>   
        <th colspan="1" class="showcol" style="text-align: center"> วันครบกำหนด </th>
    </tr>

    </thead>
        <tbody>
        @if($data)
        
            @php
                $NO = 0;
                $SUM_DIRTY_MKT = 1;


                $PAGE_SUM_FACE_VALUE = 0.0;
                $PAGE_SUM_TOTAL_COST = 0.0;
                $PAGE_SUM_ACCRUED_INT = 0.0;
                $PAGE_SUM_DIRTY_MKT = 0.0;
            @endphp

            @if($grandtoal) 
                @foreach($grandtoal as $rs =>$sumfield)
                    @php
                    $SUM_DIRTY_MKT = $sumfield->SUM_DIRTY_MKT;
                    @endphp
                @endforeach
            @endif
             
            @foreach($data as $index =>$field)
                @php
                    $NO += 1;
                    $PERCENT_DIRTY_MKT = ($field->DIRTY_MKT / $SUM_DIRTY_MKT) * 100;
                @endphp
                <tr>      
                    <td style="text-align: center"  nowrap>{{$NO + (($PageNumber - 1) * $PageSize) }}</td>        <!-- No. -->
                    <td style="text-align: left"  nowrap>{{$field->BOND_CATE_THA}}</td>        <!-- ประเภททรัพย์สิน -->
                    <td style="text-align: left"  nowrap>{{$field->BOND_TYPE_THA}}</td>        <!-- ประเภทตราสารหนี้ -->
                    <td style="text-align: left"   nowrap>{{$field->SYMBOL}}</td>           <!-- ชื่อย่อตราสารหนี้ -->
                    <td style="text-align: left"  >{{($field->SYMBOL == null) ? "" : $field->FULL_NAME}}</td>       <!-- ชื่อเต็มตราสารหนี้  -->
                    <td style="text-align: right" nowrap>{{number_format((float)$field->FACE_VALUE,2, '.', ',')}}</td>      <!-- จำนวนหุ้น / มูลค่าที่ตราไว้  -->
                    <td style="text-align: right" nowrap>{{number_format((float)$field->TOTAL_COST,2, '.', ',')}}</td>              <!-- ราคาทุน  -->
                    <td style="text-align: right" nowrap>{{number_format((float)$field->ACCRUED_INT,2, '.', ',')}}</td>              <!-- ดอกเบี้ยค้างรับ  -->
                    <td style="text-align: right" nowrap>{{number_format((float)$field->DIRTY_MKT,2, '.', ',')}}</td>              <!-- ราคายุติธรรม  -->
                    @if($PERCENT_DIRTY_MKT < 100)
                      <td style="text-align: right" nowrap>{{number_format((float)$PERCENT_DIRTY_MKT,2, '.', ',')}}</td> <!-- อัตราส่วนของราคายุติธรรม -->
                    @else
                      <td style="text-align: right" nowrap>100</td> <!-- อัตราส่วนของราคายุติธรรม -->
                    @endif
                    <td style="text-align: right" nowrap>{{number_format((float)$field->COUPON_RATE,2, '.', ',')}}</td>    <!-- อัตราดอกเบี้ย -->
                    <td style="text-align: right" nowrap>{{$field->MAT}}</td>    <!-- วันครบกำหนด -->
                </tr>
                @php
                    $PAGE_SUM_FACE_VALUE +=  $field->FACE_VALUE;
                    $PAGE_SUM_TOTAL_COST +=  $field->TOTAL_COST;
                    $PAGE_SUM_ACCRUED_INT +=  $field->ACCRUED_INT;
                    $PAGE_SUM_DIRTY_MKT +=  $field->DIRTY_MKT;
                @endphp
            @endforeach
             <!-- ยอดรวม -->
             <tr>      
                    <td style="text-align: center"  nowrap> </td>        <!-- No. -->
                    <td style="text-align: left"  nowrap></td>        <!-- ประเภททรัพย์สิน -->
                    <td style="text-align: left"  nowrap> </td>        <!-- ประเภทตราสารหนี้ -->
                    <td style="text-align: left"   nowrap> </td>           <!-- ชื่อย่อตราสารหนี้ -->
                    <td style="text-align: left; color: blue;"  >รวม</td>       <!-- ชื่อเต็มตราสารหนี้  -->
                    <td style="text-align: right; color: blue;" nowrap>{{number_format((float)$PAGE_SUM_FACE_VALUE,2, '.', ',')}}</td>      <!-- จำนวนหุ้น / มูลค่าที่ตราไว้  -->
                    <td style="text-align: right; color: blue;" nowrap>{{number_format((float)$PAGE_SUM_TOTAL_COST,2, '.', ',')}}</td>       <!-- ราคาทุน  -->
                    <td style="text-align: right; color: blue;" nowrap>{{number_format((float)$PAGE_SUM_ACCRUED_INT,2, '.', ',')}}</td>      <!-- ดอกเบี้ยค้างรับ  -->
                    <td style="text-align: right; color: blue;" nowrap>{{number_format((float)$PAGE_SUM_DIRTY_MKT,2, '.', ',')}}</td>              <!-- ราคายุติธรรม  -->
                    
                    <td style="text-align: right" nowrap></td> <!-- อัตราส่วนของราคายุติธรรม -->
                   
                    <td style="text-align: right" nowrap></td>    <!-- อัตราดอกเบี้ย -->
                    <td style="text-align: right" nowrap></td>    <!-- วันครบกำหนด -->
            </tr>
        @endif       
         <!-- ยอดทั้งหมด -->
        @if($grandtoal) 
           @foreach($grandtoal as $rs =>$sumfield)
                <tr>      
                    <td style="text-align: center"  nowrap> </td>        <!-- No. -->
                    <td style="text-align: left"  nowrap></td>        <!-- ประเภททรัพย์สิน -->
                    <td style="text-align: left"  nowrap> </td>        <!-- ประเภทตราสารหนี้ -->
                    <td style="text-align: left"   nowrap> </td>           <!-- ชื่อย่อตราสารหนี้ -->
                    <td style="text-align: left; color: blue;"  >รวมทั้งหมด</td>       <!-- ชื่อเต็มตราสารหนี้  -->
                    <td style="text-align: right; color: blue;" nowrap>{{number_format((float)$sumfield->SUM_FACE_VALUE,2, '.', ',')}}</td>      <!-- จำนวนหุ้น / มูลค่าที่ตราไว้  -->
                    <td style="text-align: right; color: blue;" nowrap>{{number_format((float)$sumfield->SUM_TOTAL_COST,2, '.', ',')}}</td>              <!-- ราคาทุน  -->
                    <td style="text-align: right; color: blue;" nowrap>{{number_format((float)$sumfield->SUM_ACCRUED_INT,2, '.', ',')}}</td>              <!-- ดอกเบี้ยค้างรับ  -->
                    <td style="text-align: right; color: blue;" nowrap>{{number_format((float)$sumfield->SUM_DIRTY_MKT,2, '.', ',')}}</td>              <!-- ราคายุติธรรม  -->
                    
                    <td style="text-align: right" nowrap></td> <!-- อัตราส่วนของราคายุติธรรม -->
                   
                    <td style="text-align: right" nowrap></td>    <!-- อัตราดอกเบี้ย -->
                    <td style="text-align: right" nowrap></td>    <!-- วันครบกำหนด -->
                </tr>
            @endforeach    
        @endif

		</tbody>
    <tfoot>

        <tr>
            <td colspan="16">
                {!! $htmlPaginate !!}
            </td>
        </tr>
    </tfoot>
</table>
</div>




<!-- Chart -->
<!--div id="bar-chart-good-bad" class="chart" style="display:block;">
    
</div-->

