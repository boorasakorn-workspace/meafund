
<div id="trading_table" style="display:block;">
<div>Showing {{$PageNumber}} to {{ (($PageSize * $PageNumber) > $totals? $totals:($PageSize * $PageNumber))  }} of {{ $totals }}</div>
<table class="table table-bordered">
    <thead>
    <tr>
        <th colspan="16" style="text-align: center">
            <p class="report-title"> {!! $TableTitle !!} </p>
        </th>
    </tr>
    <tr>
        <th colspan="{{ 16 }}" style="text-align: center">ข้อมูล ปี <font color="#ff0000"> {!! $year !!} </font></th>
    </tr>
    <tr>
        <th rowspan="2" style="text-align: center">SECURITY NAME</th>
        <th rowspan="2" style="text-align: center">ISSUER</th>
        <th rowspan="2" style="text-align: center">MATURITY</th>
        <th rowspan="2" style="text-align: center">INVESTMENT TYPE</th>
        <th colspan="16" style="text-align: center">UNREALIZED GAIN/ (LOSS) VALUE OF SECURITY (AS OF...)</th>
    </tr>
    <tr>
    <!-- เพิ่ม column วันเดอืนปีที่นี่ อย่าลืมเพิ่ม  ($max_columns + x)  -->
        <th style="text-align: center" nowrap>JAN-{!! $year !!}</th>
        <th style="text-align: center" nowrap>FEB-{!! $year !!}</th>
        <th style="text-align: center" nowrap>MAR-{!! $year !!}</th>
        <th style="text-align: center" nowrap>APR-{!! $year !!}</th>
		<th style="text-align: center" nowrap>MAY-{!! $year !!}</th>
        <th style="text-align: center" nowrap>JUN-{!! $year !!}</th>
        <th style="text-align: center" nowrap>JUL-{!! $year !!}</th>
        <th style="text-align: center" nowrap>AUG-{!! $year !!}</th>
		<th style="text-align: center" nowrap>OCT-{!! $year !!}</th>
        <th style="text-align: center" nowrap>SEP-{!! $year !!}</th>
        <th style="text-align: center" nowrap>NOV-{!! $year !!}</th>
        <th style="text-align: center" nowrap>DEC-{!! $year !!}</th>
    </tr>
   
    </thead>
        <tbody>
            @php
            $J = 0.0;
            $N = 0.0;
            $O = 0.0;
            
            $Jan = 0.0; 
            $Feb = 0.0; 
            $Mar = 0.0; 
            $Apr = 0.0; 
            $May = 0.0; 
            $Jun = 0.0; 
            $Jul = 0.0; 
            $Aug = 0.0; 
            $Sep = 0.0; 
            $Oct = 0.0; 
            $Nov = 0.0; 
            $Dec = 0.0; 
            
            @endphp

            @foreach($data as $index =>$field)
            <tr>
                <td class="showcol" style="text-align: center" nowrap>{{$field->SECURITIES_NAME}}</td>                                    <!-- SECURITIES_NAME บลจ -->
                <td class="showcol" style="text-align: center" nowrap>{{($field->SYMBOL == null) ? '' : $field->SYMBOL }}</td>   <!-- ISSUER -->
                <td class="showcol" style="text-align: left"   nowrap>{{$field->MAT}}</td>                                       <!-- MATURITY -->
                <td class="showcol" style="text-align: center" nowrap>{{$field->BOND_TYPE_THA}}</td>	
                				         <!-- INVESTMENT TYPE -->
                <!-- Months Start Loop GAIN/ (LOSS) -->

				<td style="text-align: right" nowrap><font color={{$field->Jan >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$field->Jan, 2, '.', ',') }}</font></td> 
                <td style="text-align: right" nowrap><font color={{$field->Feb >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$field->Feb, 2, '.', ',') }}</font></td>  
                <td style="text-align: right" nowrap><font color={{$field->Mar >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$field->Mar, 2, '.', ',') }}</font></td>   
                <td style="text-align: right" nowrap><font color={{$field->Apr >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$field->Apr, 2, '.', ',') }}</font></td>  
                <td style="text-align: right" nowrap><font color={{$field->May >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$field->May, 2, '.', ',') }}</font></td> 
				<td style="text-align: right" nowrap><font color={{$field->Jun >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$field->Jun, 2, '.', ',') }}</font></td>  
                <td style="text-align: right" nowrap><font color={{$field->Jul >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$field->Jul, 2, '.', ',') }}</font></td>   
                <td style="text-align: right" nowrap><font color={{$field->Aug >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$field->Aug, 2, '.', ',') }}</font></td>  
                <td style="text-align: right" nowrap><font color={{$field->Sep >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$field->Sep, 2, '.', ',') }}</font></td> 
				<td style="text-align: right" nowrap><font color={{$field->Oct >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$field->Oct, 2, '.', ',') }}</font></td>  
                <td style="text-align: right" nowrap><font color={{$field->Nov >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$field->Nov, 2, '.', ',') }}</font></td>   
                <td style="text-align: right" nowrap><font color={{$field->Dec >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$field->Dec, 2, '.', ',') }}</font></td>  
            </tr>
            @php
            $Jan += $field->Jan; 
            $Feb += $field->Feb; 
            $Mar += $field->Mar; 
            $Apr += $field->Apr; 
            $May += $field->Jun; 
            $Jun += $field->Jul; 
            $Jul += $field->Jun; 
            $Aug += $field->Aug; 
            $Sep += $field->Sep; 
            $Oct += $field->Oct; 
            $Nov += $field->Nov; 
            $Dec += $field->Dec; 
            @endphp

            @endforeach
             
    
            @endphp
            <tr>
                
                <td class="showcol" style="text-align: center" nowrap></td>                                    <!-- SECURITIES_NAME บลจ -->
                <td class="showcol" style="text-align: center" nowrap></td>   <!-- ISSUER -->
                <td class="showcol" style="text-align: left"   nowrap></td>                                       <!-- MATURITY -->
                <td class="showcol" style="text-align: center" nowrap>รวม</td>                             <!-- INVESTMENT TYPE -->
                <!-- Months Start Loop GAIN/ (LOSS) -->

                <td style="text-align: right" nowrap><font color={{$Jan >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$Jan, 2, '.', ',') }}</font></td> 
                <td style="text-align: right" nowrap><font color={{$Feb >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$Feb, 2, '.', ',') }}</font></td>  
                <td style="text-align: right" nowrap><font color={{$Mar >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$Mar, 2, '.', ',') }}</font></td>   
                <td style="text-align: right" nowrap><font color={{$Apr >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$Apr, 2, '.', ',') }}</font></td>  
                <td style="text-align: right" nowrap><font color={{$May >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$May, 2, '.', ',') }}</font></td> 
                <td style="text-align: right" nowrap><font color={{$Jun >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$Jun, 2, '.', ',') }}</font></td>  
                <td style="text-align: right" nowrap><font color={{$Jul >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$Jul, 2, '.', ',') }}</font></td>   
                <td style="text-align: right" nowrap><font color={{$Aug >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$Aug, 2, '.', ',') }}</font></td>  
                <td style="text-align: right" nowrap><font color={{$Sep >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$Sep, 2, '.', ',') }}</font></td> 
                <td style="text-align: right" nowrap><font color={{$Oct >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$Oct, 2, '.', ',') }}</font></td>  
                <td style="text-align: right" nowrap><font color={{$Nov >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$Nov, 2, '.', ',') }}</font></td>   
                <td style="text-align: right" nowrap><font color={{$Dec >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$Dec, 2, '.', ',') }}</font></td>  
             
            </tr>

            @if($grandTotal)
        @foreach($grandTotal as $index =>$field)
        <tr>
                <td class="showcol" style="text-align: center" nowrap> </td>                                    <!-- SECURITIES_NAME บลจ -->
                <td class="showcol" style="text-align: center" nowrap> </td>   <!-- ISSUER -->
                <td class="showcol" style="text-align: left"   nowrap> </td>                                       <!-- MATURITY -->
                <td class="showcol" style="text-align: center" nowrap> รวมทั้งสิ้น </td>    
                                         <!-- INVESTMENT TYPE -->
                <!-- Months Start Loop GAIN/ (LOSS) -->

                <td style="text-align: right" nowrap><font color={{$field->Jan >= 0 ? "#0000ff":"#ff0000"}}>{{number_format((float)$field->Jan, 2, '.', ',') }}</font></td> 
                <td style="text-align: right" nowrap><font color={{$field->Feb >= 0 ? "#0000ff":"#ff0000"}}>{{number_format((float)$field->Feb, 2, '.', ',') }}</font></td>  
                <td style="text-align: right" nowrap><font color={{$field->Mar >= 0 ? "#0000ff":"#ff0000"}}>{{number_format((float)$field->Mar, 2, '.', ',') }}</font></td>   
                <td style="text-align: right" nowrap><font color={{$field->Apr >= 0 ? "#0000ff":"#ff0000"}}>{{number_format((float)$field->Apr, 2, '.', ',') }}</font></td>  
                <td style="text-align: right" nowrap><font color={{$field->May >= 0 ? "#0000ff":"#ff0000"}}>{{number_format((float)$field->May, 2, '.', ',') }}</font></td> 
                <td style="text-align: right" nowrap><font color={{$field->Jun >= 0 ? "#0000ff":"#ff0000"}}>{{number_format((float)$field->Jun, 2, '.', ',') }}</font></td>  
                <td style="text-align: right" nowrap><font color={{$field->Jul >= 0 ? "#0000ff":"#ff0000"}}>{{number_format((float)$field->Jul, 2, '.', ',') }}</font></td>   
                <td style="text-align: right" nowrap><font color={{$field->Aug >= 0 ? "#0000ff":"#ff0000"}}>{{number_format((float)$field->Aug, 2, '.', ',') }}</font></td>  
                <td style="text-align: right" nowrap><font color={{$field->Sep >= 0 ? "#0000ff":"#ff0000"}}>{{number_format((float)$field->Sep, 2, '.', ',') }}</font></td> 
                <td style="text-align: right" nowrap><font color={{$field->Oct >= 0 ? "#0000ff":"#ff0000"}}>{{number_format((float)$field->Oct, 2, '.', ',') }}</font></td>  
                <td style="text-align: right" nowrap><font color={{$field->Nov >= 0 ? "#0000ff":"#ff0000"}}>{{number_format((float)$field->Nov, 2, '.', ',') }}</font></td>   
                <td style="text-align: right" nowrap><font color={{$field->Dec >= 0 ? "#0000ff":"#ff0000"}}>{{number_format((float)$field->Dec, 2, '.', ',') }}</font></td>  
            </tr>

        @endforeach
        @endif
        </tbody>
    <tfoot>
        
        <tr>
            <td colspan="16">
                {!! $htmlPaginate !!}
            </td>
        </tr>
    </tfoot>
</table>
</div>

