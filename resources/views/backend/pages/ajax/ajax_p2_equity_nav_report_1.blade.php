<p class="report-period">
 
</p>

<div id="trading_table" style="margin-bottom: 10px; padding: 10px; display:block;">
 
 <table class="table table-bordered table-striped" id="table_equity_nav">
         
        <thead>
        <tr>
            <th colspan="5" style="text-align: center"> นโยบายการลงทุนตราสารทุน</th>
             
        </tr>
        <tr>
            <th colspan="5" style="text-align: center"> มูลค่าทรัพย์สินสุทธิและอัตราตอบแทน</th>
            <!--th colspan="5" style="text-align: center"> </th-->
        </tr>
        <tr>
            <th colspan="1" rowspan="2" style="vertical-align: middle; text-align: center">เดือน</th>
            <th colspan="2" style="text-align: center">มูลค่าทรัพย์สินสุทธิ</th>
            <th colspan="2" style="text-align: center">อัตราตอบแทน (%)</th>
            <!--th colspan="1" rowspan="12" style="vertical-align: middle; text-align: center">กราฟ</th-->
            
        </tr>
        <tr>
            <th colspan="1" style="text-align: center">ล้านบาท</th>
            <th colspan="1" style="text-align: center">ต่อหน่วย</th>
            <th colspan="1" style="text-align: center">ลายเดือน</th>
            <th colspan="1" style="text-align: center">สะสม</th> 
             <!--th colspan="5" rowspan="13" style="text-align: center">การลงทุนตราสารทุน</th-->
        </tr>
        </thead>
       
            <tbody>
                <tr>       
                    <td style="text-align: center">ม.ค. 2559</td>  
                    <td style="text-align: right">6,554.45</td>       
                    <td style="text-align: right">18.1039</td>    
                    <td style="text-align: right">0.49</td> 
                    <td style="text-align: right">0.49</td>  
                </tr>
                <tr>       
                    <td style="text-align: center">ก.พ. 2559</td>  
                    <td style="text-align: right">6,554.45</td>       
                    <td style="text-align: right">18.1954</td>    
                    <td style="text-align: right">0.36</td> 
                    <td style="text-align: right">0.85</td>  
                </tr>
                <tr>       
                    <td style="text-align: center">มี.ค. 2559</td>  
                    <td style="text-align: right">6,546.04</td>       
                    <td style="text-align: right">18.2798</td>    
                    <td style="text-align: right">0.46</td> 
                    <td style="text-align: right">0.46</td>  
                </tr>

                <tr>       
                    <td style="text-align: center">เม.ย. 2559</td>  
                    <td style="text-align: right">6,556.56</td>       
                    <td style="text-align: right">18.2913</td>    
                    <td style="text-align: right">0.06</td> 
                    <td style="text-align: right">1.38</td>  
                </tr>
                <tr>       
                    <td style="text-align: center">พ.ค. 2559</td>  
                    <td style="text-align: right">6,519.42</td>       
                    <td style="text-align: right">18.2220</td>    
                    <td style="text-align: right"><font color="#ff0000">-0.38</font></td> 
                    <td style="text-align: right">1.00</td>  
                </tr>

                <tr>       
                    <td style="text-align: center">มิ.ย. 2559</td>  
                    <td style="text-align: right">6,533.68</td>       
                    <td style="text-align: right">18.3143</td>    
                    <td style="text-align: right">0.51</td> 
                    <td style="text-align: right">1.51</td>  
                </tr>

                <tr>       
                    <td style="text-align: center">ก.ค. 2559</td>  
                    <td style="text-align: right">6,519.23</td>       
                    <td style="text-align: right">18.3417</td>    
                    <td style="text-align: right">0.15</td> 
                    <td style="text-align: right">1.66</td>  
                </tr>
                <tr>       
                    <td style="text-align: center">ส.ค. 2559</td>  
                    <td style="text-align: right"> </td>       
                    <td style="text-align: right"> </td>    
                    <td style="text-align: right"> </td> 
                    <td style="text-align: right"> </td>  
                </tr>
                <tr>       
                    <td style="text-align: center">ก.ย. 2559</td>  
                    <td style="text-align: right"> </td>       
                    <td style="text-align: right"> </td>    
                    <td style="text-align: right"> </td> 
                    <td style="text-align: right"> </td>  
                </tr>
                <tr>       
                    <td style="text-align: center">ต.ค. 2559</td>  
                    <td style="text-align: right"> </td>       
                    <td style="text-align: right"> </td>    
                    <td style="text-align: right"> </td> 
                    <td style="text-align: right"> </td>  
                </tr>

                <tr>       
                    <td style="text-align: center">พ.ย. 2559</td>  
                    <td style="text-align: right"> </td>       
                    <td style="text-align: right"> </td>    
                    <td style="text-align: right"> </td> 
                    <td style="text-align: right"> </td>  
                </tr>

                <tr>       
                    <td style="text-align: center">ธ.ค. 2559</td>  
                    <td style="text-align: right"> </td>       
                    <td style="text-align: right"> </td>    
                    <td style="text-align: right"> </td> 
                    <td style="text-align: right"> </td>  
                </tr>
               
            </tbody>
    </table>
    
</div>
 
   




 

