

<!--p class="report-period">
   {!! $pretty_date !!}
  
</p-->

<div id="trading_table" style="display:block;">
<div>Showing {{$PageNumber}} to {{ (($PageSize * $PageNumber) > $totals? $totals:($PageSize * $PageNumber))  }} of {{ $totals }}</div>
<table class="table table-bordered">
    <thead>
    <tr>
        <th colspan="14" style="text-align: center">
            <p class="report-title">{!! $TableTitle !!}</p> <!-- รายงาน ค่านายหน้าซื้อ-ขายหลักทรัพย์ -->
        </th>
    </tr>
    
    <tr>
        <th colspan="14" style="text-align: center">ตั้งแต่ {!! $pretty_date !!}</th>
    </tr>
     <tr>
        <th colspan="14" style="text-align: center">{!! $SecuritiesName !!}</th>
    </tr>
    <tr>
        <th colspan="1" rowspan="3" class="showcol" style="text-align: center; vertical-align: middle;"> ลำดับ </th>
         
        <th colspan="1" rowspan="3" class="showcol" style="text-align: center; vertical-align: middle;">บลจ.</th>
        <th colspan="2" rowspan="2" class="showcol" style="text-align: center; vertical-align: middle;">รายชื่อบริษัท Broker</th>
         
        <th colspan="6"  class="showcol" style="text-align: center">ค่านายหน้า</th>
         
        <th colspan="3"  rowspan="2" class="showcol"  style="text-align: center; vertical-align: middle;">รวม</th>
         
        <th colspan="1" width="10%" rowspan="3"  class="showcol"  style="text-align: center; vertical-align: top;">อัตราส่วนค่านายหน้า %</th>
    </tr>
    <tr>    
            <th colspan="3" style="text-align: center">ชื้อ</th>
            <th colspan="3"  style="text-align: center">ขาย</th>
    </tr>
    <tr>
         
            <th style="text-align: center">ชื่อย่อ</th>
            <th style="text-align: center">ชื่อ</th>
            <th style="text-align: center">จำนวนครั้ง</th>
            <th style="text-align: center">จำนวนเงิน</th>
            <th style="text-align: center">% Comm. </th>

            <th style="text-align: center">จำนวนครั้ง</th>
            <th style="text-align: center">จำนวนเงิน</th>
            <th style="text-align: center">% Comm. </th>

            <th style="text-align: center">จำนวนครั้ง</th>
            <th style="text-align: center">จำนวนเงิน</th>
            <th style="text-align: center">% Comm. </th>
             
    </tr>
    </thead>
    <tbody>
        @php
        $J = 0.0;
        $N = 0.0;
        $O = 0.0;

        $item = 0;

        $P_COUNT = 0;
        $S_COUNT = 0;
        $PURCHASE = 0.0;
        $SALE = 0.0;
        $C  = 0.0;
        $CSUM = 0.0;
        $PERCENT = 0.0;


        $SUM_GROSS = 0.0;


        $SUM_PURCHASE_BROKERAGE = 0.0;
        $SUM_GROSS_PURCHASE = 0.0;
        
        $SUM_SALE_BROKERAGE = 0.0;
        $SUM_GROSS_SALE = 0.0;

        $SUM_BROKERAGE = 0.0;   
        
        @endphp

        @foreach($data as $index =>$field)
            @php
            $SUM_BROKERAGE          += $field->SUM_BROKERAGE;
            @endphp
        @endforeach  


        @foreach($data as $index =>$field)
        <tr>  
           <!--[P], [S], [PX], [SX] -->
            @php
                  $item++;
                  $P_COUNT       += $field->P_COUNT;
                  $S_COUNT       += $field->S_COUNT;
                  $PURCHASE      += $field->PURCHASE;
                  $SALE          += $field->SALE;

                  $C             += ($field->P_COUNT + $field->S_COUNT);
                  $CSUM          += ($field->PURCHASE + $field->SALE); 
                  
                  $PERCENT       = ($sum_all / $sum_all) * 100;
                  $SUM_GROSS     += $field->GROSS;

                  $SUM_PURCHASE_BROKERAGE += $field->PURCHASE_BROKERAGE;
                  $SUM_GROSS_PURCHASE     += $field->GROSS_PURCHASE;

                  $SUM_SALE_BROKERAGE     += $field->SALE_BROKERAGE;
                  $SUM_GROSS_SALE         += $field->GROSS_SALE;

                 
                  // $SUM_BROKERAGE          += $field->SUM_BROKERAGE;
                  
            @endphp
            <td class="showcol" style="text-align: right;">{{$item}}</td>            <!-- ลำดับ -->
            <td class="showcol" style="text-align: center;">{{$field->SECURITIES_NAME}}</td>        <!-- ชื่อ SECURITIES_NAME UOBAM/KTAM -->
            <td class="showcol" style="text-align: center;">{{$field->NAME_SHT}}</td>   <!-- ชื่อ บลจ-->
            <td class="showcol" style="text-align: left;">{{$field->BROKER_NAME}}</td>            <!-- ชื่อ BROKER-->
            <!-- Purchase -->
            <td class="showcol" style="text-align: right">{{$field->P_COUNT}}</td>                                            <!-- จำนวณครั้ง -->
            <td class="showcol" style="text-align: right">{{number_format((float)$field->PURCHASE_BROKERAGE, 2, '.', ',')}}</td>        <!-- จำนวนเงิน -->
            <td class="showcol" style="text-align: right">{{number_format((float)$field->COMM_PURCHASE, 2, '.', ',')}}%</td>   <!-- % Commission Purchase -->

            <!-- Sale -->
            <td class="showcol" style="text-align: right">{{$field->S_COUNT}}</td>                                            <!-- จำนวณครั้ง -->
            <td class="showcol" style="text-align: right">{{number_format((float)$field->SALE_BROKERAGE, 2, '.', ',')}}</td>            <!-- จำนวนเงิน -->
            <td class="showcol" style="text-align: right">{{number_format((float)$field->COMM_SALE, 2, '.', ',')}}%</td>      <!-- % Commission Sale -->

            <!-- รวม -->
            <td class="showcol" style="text-align: right" >{{$field->P_COUNT + $field->S_COUNT}}</td>       <!-- จำนวณครั้ง -->
            <td class="showcol" style="text-align: right" >{{number_format((float)$field->SUM_BROKERAGE, 2, '.', ',')}}</td>      <!-- จำนวนเงิน -->
            <td class="showcol" style="text-align: right">{{number_format((float)$field->COMM_TOTAL, 2, '.', ',')}}%</td>      <!-- % Commission total --> 

            @if(($field->PURCHASE + $field->SALE) > 0) 
               <td class="showcol" style="text-align: right" >{{number_format(((($field->PURCHASE_BROKERAGE + $field->SALE_BROKERAGE) / $sum_all) * 100), 2, '.', ',') }}%</td>   <!-- อัตราค่าน่ายหน้า  -->
            @else
               <td class="showcol" style="text-align: right" >0.00</td>
            @endif 
             
           
        </tr>
        @endforeach  

        <!-- ยอดรวม แต่ล่ะ Page --> 
        <tr>
            <td class="showcol"  colspan="4" style="text-align: right" >รวม</td>  <!-- ชื่อย่อ  -->
            <td class="showcol" style="text-align: right" >{{$P_COUNT}}</td>           <!-- จำนวณครั้ง -->
            <td class="showcol" style="text-align: right" >{{number_format((float)$sum_grand_total_purchase_brokerage, 2, '.', ',')}}</td>   <!-- จำนวนเงิน-->
            <td class="showcol" style="text-align: right" ></td>       <!-- % Commision Purchase (Total)-->

            <td class="showcol" style="text-align: right" >{{$S_COUNT}}</td>      <!-- จำนวณครั้ง -->
            <td class="showcol" style="text-align: right" >{{number_format((float)$sum_grand_total_sale_brokerage, 2, '.', ',')}}</td>       <!-- จำนวนเงิน-->
            <td class="showcol" style="text-align: right" ></td>       <!-- % Commission Sale (Total)-->
           

            <td class="showcol" style="text-align: right" >{{$count_all}}</td>      <!-- จำนวณครั้ง -->
            <td class="showcol" style="text-align: right" >{{number_format((float)$sum_all, 2, '.', ',')}}</td>       <!-- จำนวนเงิน -->
            <td class="showcol" style="text-align: right" ></td>                                                  <!-- % Commission Grand Total -->

            <td class="showcol" style="text-align: right" >{{number_format($PERCENT, 2, '.', ',')}}%</td>      <!-- อัตราค่าน่ายหน้า  -->
        </tr>
    </tbody>
<!--
    <tfoot>
        <tr>
            <td colspan="13">
                {!! $htmlPaginate !!}
            </td>
        </tr>
    </tfoot>
-->
</table>
</div>




<!-- Chart -->
<!--div id="bar-chart-good-bad" class="chart" style="display:block;">
    
</div-->

