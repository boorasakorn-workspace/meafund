
<div id="trading_table" style="display:block;">
<div>Showing {{$PageNumber}} to {{ (($PageSize * $PageNumber) > $totals? $totals:($PageSize * $PageNumber))  }} of {{ $totals }}</div>
<table class="table table-bordered">
    <thead>
    <tr>
        <th colspan="29" style="text-align: center">
            <p class="report-title">{{$TableTitle}} </p> <!-- รายงานความเคลื่อนไหวตราสารทุน -->
        </th>
    </tr>
    <tr>
        <th colspan="29" style="text-align: center">{!! $pretty_date !!}</th>
    </tr>
	<tr>
        <th rowspan="4" style="text-align: center">TYPE</th>
        <th rowspan="4" style="text-align: center">SETTLE DATE</th>
        <th rowspan="4" style="text-align: center">SECURITES</th>
        <th rowspan="4" style="text-align: center">MATURITY</th>
        <th rowspan="4" style="text-align: center">FACE VALUE</th>
        <th rowspan="4" style="text-align: center">YIELD</th>

        <th colspan="3" style="text-align: center">Purchase</th>
        <th colspan="5" style="text-align: center">Sale</th>
        <th colspan="5" style="text-align: center">Maturity</th>
        <th colspan="5" style="text-align: center">Interest</th>
        
        <th rowspan="3" style="text-align: center">COUPON RATE</th>
        <th rowspan="3" style="text-align: center">NET AMOUNT</th>
        <th rowspan="1" colspan="3" style="text-align: center">อายุสารตรา</th>
        
    </tr>
	    <tr>
         <!-- Purchase -->
        <th rowspan="1" style="text-align: center">Clean Value</th>
        <th rowspan="1" style="text-align: center">Accrued Int.</th>
        <th rowspan="1" style="text-align: center">Total Value</th>

         <!-- Sale --> 
        <th rowspan="1" style="text-align: center">CLEAN_VAL_SALE</th>
        <th rowspan="1" style="text-align: center">ACCRUED_SALE</th>
        <th rowspan="1" style="text-align: center">TOTAL_VAL_SALE</th>
        <th rowspan="1" style="text-align: center">TOTAL_AMORT_SALE</th>
        <th rowspan="1" style="text-align: center">PROFIT_LOSS_SALE</th>

        <!--Maturity--> 
        <th rowspan="1" style="text-align: center">CLEAN_VAL_MAT</th>
        <th rowspan="1" style="text-align: center">ACCRUED_MAT</th>
        <th rowspan="1" style="text-align: center">TOTAL_VAL_MAT</th>
        <th rowspan="1" style="text-align: center">TOTAL_AMORT_MAT</th>
        <th rowspan="1" style="text-align: center">PROFIT_LOSS_MAT</th>

         <!-- Interest --> 
        <th rowspan="1" style="text-align: center">CLEAN_VAL_INT</th>
        <th rowspan="1" style="text-align: center">ACCRUED_INT</th>
        <th rowspan="1" style="text-align: center">TOTAL_VAL_INT</th>
        <th rowspan="1" style="text-align: center">TOTAL_AMORT_INT</th>
		<th rowspan="1" style="text-align: center">PROFIT_LOSS_INT</th>

        <th>YY</th>
        <th>MM</th>
        <th>DD</th>
        
    </tr>
    <!--tr>
        <th>YY</th>
        <th>MM</th>
        <th>DD</th>
    </tr-->

	
    </thead>
        <tbody>		
            @php

            $FACE_VALUE       = 0.0;

            $CLEAN_VAL_PUR    = 0.0;
            $ACCRUED_PUR      = 0.0;
            $TOTAL_VAL_PUR    = 0.0;

            $CLEAN_VAL_SALE   = 0.0;
            $ACCRUED_SALE     = 0.0;
            $TOTAL_VAL_SALE   = 0.0;
            $TOTAL_AMORT_SALE = 0.0;
            $PROFIT_LOSS_SALE  = 0.0;

            $CLEAN_VAL_MAT    = 0.0;
            $ACCRUED_MAT      = 0.0;
            $TOTAL_VAL_MAT    = 0.0;
            $TOTAL_AMORT_MAT  = 0.0;
            $PROFIT_LOSS_MAT   = 0.0;

            $CLEAN_VAL_INT    = 0.0;
            $ACCRUED_INT      = 0.0;
            $TOTAL_VAL_INT    = 0.0;
            $TOTAL_AMORT_INT  = 0.0;
            $PROFIT_LOSS_INT   = 0.0;

            $NET_AMOUNT       = 0.0;

            @endphp

            @foreach($data as $index =>$field)
            @inject('trading', 'App\Http\Controllers\AdminP2BondTradingReportController') 
            <tr>
                      
      				@if($field->TYPE == 'P')
      					<td class="showcol" style="text-align: center" nowrap>Purchase</td> <!-- TRANS_DATE วันเดือนปี -->
      				@elseif($field->TYPE == 'S')
      					<td class="showcol" style="text-align: center" nowrap>Sale</td> <!-- TRANS_DATE วันเดือนปี -->
      				@elseif($field->TYPE == 'I')
      					<td class="showcol" style="text-align: center" nowrap>Interest</td> <!-- TRANS_DATE วันเดือนปี -->
      				@else
      					<td class="showcol" style="text-align: center" nowrap>Maturity</td> <!-- TRANS_DATE วันเดือนปี -->
      				@endif
                <td class="showcol" style="text-align: center" nowrap>{{$field->SETTLE_DATE}}</td>       <!-- SYMBOL ชื่อ -->
                <td class="showcol" style="text-align: left" nowrap>{{$field->SYMBOL}}</td>   <!-- SECURITIES_NAME บลจ -->
                <td class="showcol" style="text-align: left" nowrap>{{$field->MAT}}</td>   <!-- MAT -->
                <td class="showcol" style="text-align: left" nowrap>{{number_format((float)$field->FACE_VALUE, 2, '.', ',')}}</td> <!--หมวดธุรกิจ-->
                <td class="showcol" style="text-align: left" nowrap>{{number_format((float)$field->YIELD_PERCENTAGE, 2, '.', ',')}}</td> <!--หมวดธุรกิจ-->
 
                <td class="showcol" style="text-align: right" nowrap>{{number_format((float)$field->CLEAN_VAL_PUR, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ --> 
                <td class="showcol" style="text-align: right" nowrap>{{number_format((float)$field->ACCRUED_PUR, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ --> 
                <td class="showcol" style="text-align: right" nowrap>{{number_format((float)$field->TOTAL_VAL_PUR, 2, '.', ',')}}</td>
                
                <td class="showcol" style="text-align: right">{{number_format((float)$field->CLEAN_VAL_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                <td class="showcol" style="text-align: right">{{number_format((float)$field->ACCRUED_SALE, 2, '.', ',')}}</td>
                <td class="showcol" style="text-align: right">{{number_format((float)$field->TOTAL_VAL_SALE, 2, '.', ',')}}</td>
				<td class="showcol" style="text-align: right">{{number_format((float)$field->TOTAL_AMORT_SALE, 2, '.', ',')}}</td>
       			<td class="showcol" style="text-align: right">{{number_format((float)$field->PROFIT_LOSS_SALE, 2, '.', ',')}}</td>  
				
                <td class="showcol" style="text-align: right">{{number_format((float)$field->CLEAN_VAL_MAT, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                <td class="showcol" style="text-align: right">{{number_format((float)$field->ACCRUED_MAT, 2, '.', ',')}}</td>
                <td class="showcol" style="text-align: right">{{number_format((float)$field->TOTAL_VAL_MAT, 2, '.', ',')}}</td>
				<td class="showcol" style="text-align: right">{{number_format((float)$field->TOTAL_AMORT_MAT, 2, '.', ',')}}</td>
       			<td class="showcol" style="text-align: right">{{number_format((float)$field->PROFIT_LOSS_MAT, 2, '.', ',')}}</td>  

				<td class="showcol" style="text-align: right">{{number_format((float)$field->CLEAN_VAL_INT, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                <td class="showcol" style="text-align: right">{{number_format((float)$field->ACCRUED_INT, 2, '.', ',')}}</td>
                <td class="showcol" style="text-align: right">{{number_format((float)$field->TOTAL_VAL_INT, 2, '.', ',')}}</td>
				<td class="showcol" style="text-align: right">{{number_format((float)$field->TOTAL_AMORT_INT, 2, '.', ',')}}</td>
       			<td class="showcol" style="text-align: right">{{number_format((float)$field->PROFIT_LOSS_INT, 2, '.', ',')}}</td>  	

                <td class="showcol" style="text-align: right" nowrap>{{number_format((float)$field->COUPON_RATE,2, '.', ',')}}</td> <!-- หน่วยขาย -->
                <td class="showcol" style="text-align: right" nowrap>{{number_format((float)$field->NET_AMOUNT,2, '.', ',')}}</td>  <!-- ราคาขายต่อหน่วย -->
    
                <td style="text-align: right" >{{ $trading->matGetYear(date('Y-m-d'),  $field->MAT)[0] }}</td>  
                <td style="text-align: right" >{{ $trading->matGetYear(date('Y-m-d'),  $field->MAT)[1] }}</td>  
                <td style="text-align: right" >{{ $trading->matGetYear(date('Y-m-d'),  $field->MAT)[2] }}</td>  
                
            </tr>

            @php
               
            $FACE_VALUE       += $field->FACE_VALUE;

            $CLEAN_VAL_PUR    += $field->CLEAN_VAL_PUR;
            $CLEAN_VAL_SALE   += $field->CLEAN_VAL_SALE;
            $CLEAN_VAL_MAT    += $field->CLEAN_VAL_MAT;
            $CLEAN_VAL_INT    += $field->CLEAN_VAL_INT;
            
            $TOTAL_AMORT_SALE += $field->TOTAL_AMORT_SALE;
            $TOTAL_AMORT_MAT  += $field->TOTAL_AMORT_MAT;
            $TOTAL_AMORT_INT  += $field->TOTAL_AMORT_INT;

            $TOTAL_VAL_PUR    += $field->TOTAL_VAL_PUR;
            $TOTAL_VAL_SALE   += $field->TOTAL_VAL_SALE;
            $TOTAL_VAL_MAT    += $field->TOTAL_VAL_MAT;
            $TOTAL_VAL_INT    += $field->TOTAL_VAL_INT;

            $ACCRUED_PUR      += $field->ACCRUED_PUR;
            $ACCRUED_SALE     += $field->TOTAL_AMORT_SALE;
            $ACCRUED_MAT      += $field->ACCRUED_MAT;
            $ACCRUED_INT      += $field->ACCRUED_INT;

            $PROFIT_LOSS_SALE  += $field->PROFIT_LOSS_SALE;
            $PROFIT_LOSS_MAT   += $field->PROFIT_LOSS_MAT;
            $PROFIT_LOSS_INT   += $field->PROFIT_LOSS_INT;

            $NET_AMOUNT       += $field->NET_AMOUNT;
            @endphp

            @endforeach 

            <tr>
                
                <td class="showcol" style="text-align: center; color:blue;" nowrap></td> <!-- TRANS_DATE วันเดือนปี -->
                <td class="showcol" style="text-align: center; color:blue;" nowrap> </td>       <!-- SYMBOL ชื่อ -->
                <td class="showcol" style="text-align: left; color:blue;" nowrap> </td>   <!-- SECURITIES_NAME บลจ -->
                <td class="showcol" style="text-align: left; color:blue;" nowrap> รวม </td>   <!-- MAT -->
                <td class="showcol" style="text-align: left; color:blue;" nowrap></td> <!--หมวดธุรกิจ-->
                <td class="showcol" style="text-align: left; color:blue;" nowrap></td> <!--หมวดธุรกิจ-->
 
                @if($CLEAN_VAL_PUR > 0)
                  <td class="showcol" style="text-align: right; color:blue;" nowrap>{{number_format((float)$CLEAN_VAL_PUR, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ --> 
                @else
                  <td class="showcol" style="text-align: right; color:red;" nowrap>{{number_format((float)$CLEAN_VAL_PUR, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ --> 
                @endif

                @if($CLEAN_VAL_SALE > 0)
                  <td class="showcol" style="text-align: right; color:blue;" nowrap>{{number_format((float)$CLEAN_VAL_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ --> 
                @else
                  <td class="showcol" style="text-align: right; color:red;" nowrap>{{number_format((float)$CLEAN_VAL_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ --> 
                @endif

                @if($CLEAN_VAL_MAT>0)
                   <td class="showcol" style="text-align: right; color:blue;" nowrap>{{number_format((float)$CLEAN_VAL_MAT, 2, '.', ',')}}</td>
                @else
                   <td class="showcol" style="text-align: right; color:red;" nowrap>{{number_format((float)$CLEAN_VAL_MAT, 2, '.', ',')}}</td>
                @endif

                @if($CLEAN_VAL_INT > 0) 
                   <td class="showcol" style="text-align: right; color:blue;" nowrap>{{number_format((float)$CLEAN_VAL_INT, 2, '.', ',')}}</td>
                @else
                   <td class="showcol" style="text-align: right; color:red;" nowrap>{{number_format((float)$CLEAN_VAL_INT, 2, '.', ',')}}</td>
                @endif

                @if($TOTAL_AMORT_SALE > 0)
                   <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$TOTAL_AMORT_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @else
                   <td class="showcol" style="text-align: right; color:red;">{{number_format((float)$TOTAL_AMORT_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @endif
                
                @if($TOTAL_AMORT_MAT > 0)
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$TOTAL_AMORT_MAT, 2, '.', ',')}}</td>
                @else
                <td class="showcol" style="text-align: right; color:red;">{{number_format((float)$TOTAL_AMORT_MAT, 2, '.', ',')}}</td>
                @endif

                @if($TOTAL_AMORT_INT > 0)  
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$TOTAL_AMORT_INT, 2, '.', ',')}}</td>
                @else
                <td class="showcol" style="text-align: right; color:red;">{{number_format((float)$TOTAL_AMORT_INT, 2, '.', ',')}}</td>
                @endif 

                @if($TOTAL_VAL_PUR > 0)
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$TOTAL_VAL_PUR, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @else
                <td class="showcol" style="text-align: right; color:red;">{{number_format((float)$TOTAL_VAL_PUR, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @endif

                @if($TOTAL_VAL_SALE > 0)
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$TOTAL_VAL_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @else
                <td class="showcol" style="text-align: right; color:red;">{{number_format((float)$TOTAL_VAL_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                
                @endif

                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$TOTAL_VAL_MAT, 2, '.', ',')}}</td>
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$TOTAL_VAL_INT, 2, '.', ',')}}</td>
                 
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$ACCRUED_PUR, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$ACCRUED_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$ACCRUED_MAT, 2, '.', ',')}}</td>
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$ACCRUED_INT, 2, '.', ',')}}</td>
                 
                <!-- PROFIT/LOSS -->
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$PROFIT_LOSS_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$PROFIT_LOSS_MAT, 2, '.', ',')}}</td>
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$PROFIT_LOSS_INT, 2, '.', ',')}}</td>
                
                <td class="showcol" style="text-align: right; color:blue;" nowrap></td> <!-- หน่วยขาย -->
                <td class="showcol" style="text-align: right; color:blue;" nowrap>{{number_format((float)$NET_AMOUNT, 2, '.', ',')}}</td>  <!-- ราคาขายต่อหน่วย -->
    
                <td style="text-align: right" ></td>  
                <td style="text-align: right" ></td>  
                <td style="text-align: right" ></td>  
                
            </tr>

            <!-- Grandtotal -->
            @if($grandtoal)
            @foreach($grandtoal as $index =>$field)
            <tr>
                
                <td class="showcol" style="text-align: center; color:blue;" nowrap></td> <!-- TRANS_DATE วันเดือนปี -->
                <td class="showcol" style="text-align: center; color:blue;" nowrap> </td>       <!-- SYMBOL ชื่อ -->
                <td class="showcol" style="text-align: left; color:blue;" nowrap> </td>   <!-- SECURITIES_NAME บลจ -->
                <td class="showcol" style="text-align: left; color:blue;" nowrap> รวมทั้งหมด </td>   <!-- MAT -->
                <td class="showcol" style="text-align: left; color:blue;" nowrap></td> <!--หมวดธุรกิจ-->
                <td class="showcol" style="text-align: left; color:blue;" nowrap></td> <!--หมวดธุรกิจ-->
 
                @if($field->SUM_CLEAN_VAL_PUR > 0)
                  <td class="showcol" style="text-align: right; color:blue;" nowrap>{{number_format((float)$field->SUM_CLEAN_VAL_PUR, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ --> 
                @else
                  <td class="showcol" style="text-align: right; color:red;" nowrap>{{number_format((float)$field->SUM_CLEAN_VAL_PUR, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ --> 
                @endif

                @if($field->SUM_CLEAN_VAL_SALE > 0)
                  <td class="showcol" style="text-align: right; color:blue;" nowrap>{{number_format((float)$field->SUM_CLEAN_VAL_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ --> 
                @else
                  <td class="showcol" style="text-align: right; color:red;" nowrap>{{number_format((float)$field->SUM_CLEAN_VAL_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ --> 
                @endif

                @if($field->SUM_CLEAN_VAL_MAT > 0)
                   <td class="showcol" style="text-align: right; color:blue;" nowrap>{{number_format((float)$field->SUM_CLEAN_VAL_MAT, 2, '.', ',')}}</td>
                @else
                   <td class="showcol" style="text-align: right; color:red;" nowrap>{{number_format((float)$field->SUM_CLEAN_VAL_MAT, 2, '.', ',')}}</td>
                @endif

                @if($field->SUM_CLEAN_VAL_INT > 0) 
                   <td class="showcol" style="text-align: right; color:blue;" nowrap>{{number_format((float)$field->SUM_CLEAN_VAL_INT, 2, '.', ',')}}</td>
                @else
                   <td class="showcol" style="text-align: right; color:red;" nowrap>{{number_format((float)$field->SUM_CLEAN_VAL_INT, 2, '.', ',')}}</td>
                @endif

                @if($field->SUM_TOTAL_AMORT_SALE > 0)
                   <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$field->SUM_TOTAL_AMORT_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @else
                   <td class="showcol" style="text-align: right; color:red;">{{number_format((float)$field->SUM_TOTAL_AMORT_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @endif
                
                @if($field->SUM_TOTAL_AMORT_MAT > 0)
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$field->SUM_TOTAL_AMORT_MAT, 2, '.', ',')}}</td>
                @else
                <td class="showcol" style="text-align: right; color:red;">{{number_format((float)$field->SUM_TOTAL_AMORT_MAT, 2, '.', ',')}}</td>
                @endif

                @if($field->SUM_TOTAL_AMORT_INT > 0)  
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$field->SUM_TOTAL_AMORT_INT, 2, '.', ',')}}</td>
                @else
                <td class="showcol" style="text-align: right; color:red;">{{number_format((float)$field->SUM_TOTAL_AMORT_INT, 2, '.', ',')}}</td>
                @endif 

                @if($field->SUM_TOTAL_VAL_PUR > 0)
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$field->SUM_TOTAL_VAL_PUR, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @else
                <td class="showcol" style="text-align: right; color:red;">{{number_format((float)$field->SUM_TOTAL_VAL_PUR, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @endif

                @if($field->SUM_TOTAL_VAL_SALE > 0)
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$field->SUM_TOTAL_VAL_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @else
                <td class="showcol" style="text-align: right; color:red;">{{number_format((float)$field->SUM_TOTAL_VAL_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @endif

                @if($field->SUM_TOTAL_VAL_MAT > 0)
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$field->SUM_TOTAL_VAL_MAT, 2, '.', ',')}}</td>
                @else
                <td class="showcol" style="text-align: right; color:red;">{{number_format((float)$field->SUM_TOTAL_VAL_MAT, 2, '.', ',')}}</td>
                @endif

                @if($field->SUM_TOTAL_VAL_INT > 0)
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$field->SUM_TOTAL_VAL_INT, 2, '.', ',')}}</td>
                @else
                <td class="showcol" style="text-align: right; color:red;">{{number_format((float)$field->SUM_TOTAL_VAL_INT, 2, '.', ',')}}</td>
                @endif
                 
                @if($field->SUM_ACCRUED_PUR > 0) 
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$field->SUM_ACCRUED_PUR, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @else
                <td class="showcol" style="text-align: right; color:red;">{{number_format((float)$field->SUM_ACCRUED_PUR, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @endif

                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$field->SUM_ACCRUED_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$field->SUM_ACCRUED_MAT, 2, '.', ',')}}</td>
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$field->SUM_ACCRUED_INT, 2, '.', ',')}}</td>
                 
                <!-- PROFIT/LOSS -->
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$field->SUM_PROFIT_LOSS_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$field->SUM_PROFIT_LOSS_MAT, 2, '.', ',')}}</td>
                <td class="showcol" style="text-align: right; color:blue;">{{number_format((float)$field->SUM_PROFIT_LOSS_INT, 2, '.', ',')}}</td>
                
                <td class="showcol" style="text-align: right; color:blue;" nowrap></td> <!-- หน่วยขาย -->

                @if($field->SUM_AMORT_COST > 0) 
                <td class="showcol" style="text-align: right; color:blue;" nowrap>{{number_format((float)$field->SUM_AMORT_COST, 2, '.', ',')}}</td>  <!-- ราคาขายต่อหน่วย -->
                @else
                <td class="showcol" style="text-align: right; color:red;" nowrap>{{number_format((float)$field->SUM_AMORT_COST, 2, '.', ',')}}</td>  <!-- ราคาขายต่อหน่วย -->
                @endif
    
                <td style="text-align: right" ></td>  
                <td style="text-align: right" ></td>  
                <td style="text-align: right" ></td>      
            </tr>
            @endforeach
            @endif

        </tbody>
    <tfoot>

        <tr>
            <td colspan="16">
                {!! $htmlPaginate !!}
            </td>
        </tr>

    </tfoot>
</table>
</div>




<!-- Chart -->
<!--div id="bar-chart-good-bad" class="chart" style="display:block;">
    
</div-->

