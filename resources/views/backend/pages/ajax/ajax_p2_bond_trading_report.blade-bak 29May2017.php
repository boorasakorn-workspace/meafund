
<div id="trading_table" style="display:block;">
<div>Showing {{$PageNumber}} to {{ (($PageSize * $PageNumber) > $totals? $totals:($PageSize * $PageNumber))  }} of {{ $totals }}</div>
<table class="table table-bordered">
    <thead>
    <tr>
        <th colspan="16" style="text-align: center">
            <p class="report-title">{{$TableTitle}} </p> <!-- รายงานความเคลื่อนไหวตราสารทุน -->
        </th>
    </tr>
    <tr>
        <th colspan="16" style="text-align: center">{!! $pretty_date !!}</th>
    </tr>
    <tr>
        <th rowspan="2" style="text-align: center">TYPE</th>
        <th rowspan="2" style="text-align: center">SETTLE DATE</th>
        <th rowspan="2" style="text-align: center">SECURITES</th>
        <th rowspan="2" style="text-align: center">MATURITY</th>
        <th rowspan="2" style="text-align: center">FACE VALUE</th>
        <th rowspan="2" style="text-align: center">YIELD</th>
        <th rowspan="2" style="text-align: center">Clean Value</th>
        <th rowspan="2" style="text-align: center">AMORT COST</th>
        <th rowspan="2" style="text-align: center">TOTAL VALUE/P.Cost </th>
        <th rowspan="2" style="text-align: center">INTEREST</th>
        <th rowspan="2" style="text-align: center">PROFIT/LOSS</th>
        <th rowspan="2" style="text-align: center">COUPON</th>
        <th rowspan="2" style="text-align: center">NET AMOUNT</th>
        <th colspan="3" style="text-align: center">อายุสารตรา</th>
    </tr>
    <tr>
        <th>YY</th>
        <th>MM</th>
        <th>DD</th>
    </tr>
   
    </thead>
        <tbody>		
            @php
            $J = 0.0;
            $N = 0.0;
            $O = 0.0;
            @endphp

            

            @foreach($data as $index =>$field)
            @inject('trading', 'App\Http\Controllers\AdminP2BondTradingReportController') 
            <tr>
                        
                <td class="showcol" style="text-align: right" nowrap>{{$field->TYPE}}</td> <!-- TRANS_DATE วันเดือนปี -->
                <td class="showcol" style="text-align: center" nowrap>{{$field->SETTLE_DATE}}</td>       <!-- SYMBOL ชื่อ -->
                <td class="showcol" style="text-align: left" nowrap>{{$field->SYMBOL}}</td>   <!-- SECURITIES_NAME บลจ -->
                <td class="showcol" style="text-align: left" nowrap>{{$field->MAT}}</td>   <!-- MAT -->
                <td class="showcol" style="text-align: left" nowrap>{{number_format((float)$field->FACE_VALUE, 2, '.', ',')}}</td> <!--หมวดธุรกิจ-->
                <td class="showcol" style="text-align: left" nowrap>{{number_format((float)$field->YIELD_PERCENTAGE, 2, '.', ',')}}</td> <!--หมวดธุรกิจ-->


                @if($field->TYPE == 'P') 
                   <td class="showcol" style="text-align: right" nowrap>{{number_format((float)$field->CLEAN_VAL_PUR, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @elseif($field->TYPE == 'S')
                   <td class="showcol" style="text-align: right" nowrap>{{number_format((float)$field->CLEAN_VAL_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @elseif($field->TYPE == 'M')
                   <td class="showcol" style="text-align: right" nowrap>{{number_format((float)$field->CLEAN_VAL_MAT, 2, '.', ',')}}</td>
                @elseif($field->TYPE == 'I')
                   <td class="showcol" style="text-align: right" nowrap>{{number_format((float)$field->CLEAN_VAL_INT, 2, '.', ',')}}</td>
                @else
                   <td class="showcol" style="text-align: right" nowrap>0.00</td>
                @endif
                   
                @if($field->TYPE == 'P') 
                   <td class="showcol" style="text-align: right">0.00</td>        <!-- หน่วยซื้อ -->
                @elseif($field->TYPE == 'S')
                   <td class="showcol" style="text-align: right">{{number_format((float)$field->TOTAL_AMORT_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @elseif($field->TYPE == 'M')
                   <td class="showcol" style="text-align: right">{{number_format((float)$field->TOTAL_AMORT_MAT, 2, '.', ',')}}</td>
                @elseif($field->TYPE == 'I')
                   <td class="showcol" style="text-align: right">{{number_format((float)$field->TOTAL_AMORT_INT, 2, '.', ',')}}</td>
                @else
                   <td class="showcol" style="text-align: right">0.00</td>
                @endif

                @if($field->TYPE == 'P') 
                   <td class="showcol" style="text-align: right">{{number_format((float)$field->TOTAL_VAL_PUR, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @elseif($field->TYPE == 'S')
                   <td class="showcol" style="text-align: right">{{number_format((float)$field->TOTAL_VAL_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @elseif($field->TYPE == 'M')
                   <td class="showcol" style="text-align: right">{{number_format((float)$field->TOTAL_VAL_MAT, 2, '.', ',')}}</td>
                @elseif($field->TYPE == 'I')
                   <td class="showcol" style="text-align: right">{{number_format((float)$field->TOTAL_VAL_INT, 2, '.', ',')}}</td>
                @else
                   <td class="showcol" style="text-align: right">0.00</td>
                @endif
               
                <!-- INTEREST -->
                @if($field->TYPE == 'P') 
                   <td class="showcol" style="text-align: right">{{number_format((float)$field->ACCRUED_PUR, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @elseif($field->TYPE == 'S')
                   <td class="showcol" style="text-align: right">{{number_format((float)$field->ACCRUED_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @elseif($field->TYPE == 'M')
                   <td class="showcol" style="text-align: right">{{number_format((float)$field->ACCRUED_MAT, 2, '.', ',')}}</td>
                @elseif($field->TYPE == 'I')
                   <td class="showcol" style="text-align: right">{{number_format((float)$field->ACCRUED_INT, 2, '.', ',')}}</td>
                @else
                   <td class="showcol" style="text-align: right">0.00</td>
                @endif
                
                <!-- PROFIT/LOSS -->
                @if($field->TYPE == 'P') 
                   <td class="showcol" style="text-align: right">0.00</td>        <!-- หน่วยซื้อ -->
                @elseif($field->TYPE == 'S')
                   <td class="showcol" style="text-align: right">{{number_format((float)$field->PROFIT_LOSS_SALE, 2, '.', ',')}}</td>        <!-- หน่วยซื้อ -->
                @elseif($field->TYPE == 'M')
                   <td class="showcol" style="text-align: right">{{number_format((float)$field->PROFIT_LOSS_MAT, 2, '.', ',')}}</td>
                @elseif($field->TYPE == 'I')
                   <td class="showcol" style="text-align: right">{{number_format((float)$field->PROFIT_LOSS_INT, 2, '.', ',')}}</td>
                @else
                   <td class="showcol" style="text-align: right">0.00</td>
                @endif

                    
                <td class="showcol" style="text-align: right" nowrap>{{number_format((float)$field->COUPON_RATE,2, '.', ',')}}</td> <!-- หน่วยขาย -->
                <td class="showcol" style="text-align: right" nowrap>{{number_format((float)$field->NET_AMOUNT,2, '.', ',')}}</td>  <!-- ราคาขายต่อหน่วย -->
                
                <td style="text-align: right" >{{ $trading->matGetYear(date('Y-m-d'),  $field->MAT)[0] }}</td>  
                <td style="text-align: right" >{{ $trading->matGetYear(date('Y-m-d'),  $field->MAT)[1] }}</td>  
                <td style="text-align: right" >{{ $trading->matGetYear(date('Y-m-d'),  $field->MAT)[2] }}</td>  
                
            </tr>
            @endforeach 
        </tbody>
    <tfoot>

        <tr>
            <td colspan="16">
                {!! $htmlPaginate !!}
            </td>
        </tr>
    </tfoot>
</table>
</div>




<!-- Chart -->
<!--div id="bar-chart-good-bad" class="chart" style="display:block;">
    
</div-->

