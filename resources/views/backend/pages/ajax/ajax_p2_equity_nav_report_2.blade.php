
<div id="trading_table" style="display:block;">
    <table class="table table-bordered">
       
        <thead>
        <tr>
            <th colspan="4" style="text-align: center">การลงทุน ณ. 31 กฎกฏาคม 2559</th>
        </tr>
        <tr>
            <th colspan="1" style="text-align: center">ประเภทสารตรา</th>
            <th colspan="3" style="text-align: center">นโยบายการลงทุนตราสารทุน</th>
              
        </tr>
        <tr>
            <th colspan="1" style="text-align: center"></th>
            <th colspan="1" style="text-align: center">สัดส่วน</th>
            <th colspan="1" style="text-align: center">ล้านบาท</th>
            <th colspan="1" style="text-align: center">บาท</th> 
        </tr>
        </thead>
       
            <tbody>
                <tr>       
                    <td style="text-align: left">เงินฝากธนาคารพาณิชย์</td>  
                    <td style="text-align: right">17.90</td>       
                    <td style="text-align: right">263.93</td>    
                    <td style="text-align: right">263,929,481.98</td>   
                </tr>
                <tr>  
                    <td style="text-align: left">ตราสารทุน (หุ้น)</td>  
                    <td style="text-align: right">83.00</td>       
                    <td style="text-align: right">1,217.44</td>    
                    <td style="text-align: right">1,217,440,964.40</td>   
                </tr>
                <tr>  
                    <td style="text-align: left">อื่นๆ (ลูกหนี้เจ้าหนี้จากการซื้อ/ ขายหลักทรัพย์)</td>  
                    <td style="text-align: right"><font color="#ff0000">-0.99</font></td>       
                    <td style="text-align: right"><font color="#ff0000">-14.54</font></td>    
                    <td style="text-align: right"><font color="#ff0000">-14,535,860,964.08</font></td>   
                </tr>
                <tr>  
                    <td style="text-align: left">รวม</td>  
                    <td style="text-align: right"><font color="#ff0000">100</font></td>       
                    <td style="text-align: right"><font color="#ff0000">-14.54</font></td>    
                    <td style="text-align: right"><font color="#ff0000">-14,535,860,964.08</font></td>   
                </tr>
            </tbody>
        <!--tfoot>
            <tr>
                <td colspan="16">
                    {!! $htmlPaginate !!}
                </td>
            </tr>
        </tfoot-->
    </table>
</div>




<!-- Chart -->
<!--div id="bar-chart-good-bad" class="chart" style="display:block;">
    
</div-->

