
<div id="trading_table" style="display:block;">

    <div>Showing {{$PageNumber}} to {{ (($PageSize * $PageNumber) > $totals? $totals:($PageSize * $PageNumber))  }} of {{ $totals }}</div>

    <table class="table table-bordered">       
        <thead>
        <tr>
            <th colspan="28" style="text-align: center">
                <p class="report-title">กองทุนสำรองเลี้ยงชีพพนักงานการไฟฟ้านครหลวง ซึ่งจดทะเบียนแล้ว</p>  
            </th>   
        </tr>
        <tr>
            <th colspan="28" style="text-align: center">
                <p class="report-title">สรุปรายละเอียดส่วนเพิ่ม(ลด) จากการซื้อขายหลักทรัพย์</p>  
            </th>
        </tr>
    
        <tr>
            <th colspan="28" style="text-align: center">{!! $pretty_date !!}</th>
        </tr>
        
        <tr>
            <th colspan="1"  rowspan="3" width="10%" style="text-align: center; vertical-align:middle">กลุ่มอุตสาหกรรม</th>
            <th colspan="1"  rowspan="3" width="10%" style="text-align: center; vertical-align:middle">หลักทรัพย์</th>
            <th colspan="12" rowspan="1" style="text-align: center">UOBAM</th>   
            <th colspan="1"  rowspan="3" style="text-align: center; vertical-align:middle">ACCUMULATE</th>
            <th colspan="12" rowspan="1" style="text-align: center">KTAM</th>
            <th colspan="1"  rowspan="3" style="text-align: center; vertical-align:middle">ACCUMULATE</th>       
        </tr>

        <tr>
            <th colspan="12" rowspan="1" style="text-align: center">MARK TO MARKET OF SECURITY INCOME</th>   
            <th colspan="12" rowspan="1" style="text-align: center">MARK TO MARKET OF SECURITY INCOME</th>
        </tr>

        <tr>
            
            <th style="text-align: center;">ม.ค.</th>
            <th style="text-align: center;">ก.พ.</th>
            <th style="text-align: center;">มี.ค.</th>
            <th style="text-align: center;">เม.ย.</th>
            <th style="text-align: center;">พ.ค.</th>
            <th style="text-align: center;">มิ.ย.</th>
            <th style="text-align: center;">ก.ค.</th>
            <th style="text-align: center;">ส.ค.</th>
            <th style="text-align: center;">ก.ย.</th>
            <th style="text-align: center;">ต.ค.</th>
            <th style="text-align: center;">พ.ย.</th>
            <th style="text-align: center;">ธ.ค.</th>
            
            <th style="text-align: center;">ม.ค.</th>
            <th style="text-align: center;">ก.พ.</th>
            <th style="text-align: center;">มี.ค.</th>
            <th style="text-align: center;">เม.ย.</th>
            <th style="text-align: center;">พ.ค.</th>
            <th style="text-align: center;">มิ.ย.</th>
            <th style="text-align: center;">ก.ค.</th>
            <th style="text-align: center;">ส.ค.</th>
            <th style="text-align: center;">ก.ย.</th>
            <th style="text-align: center;">ต.ค.</th>
            <th style="text-align: center;">พ.ย.</th>
            <th style="text-align: center;">ธ.ค.</th>
            
        </tr>
        </thead>
       
            <tbody>
            
            @if($data)
            <?php
             $PREV_INDUSTRIAL = "";
             $INDUSTRIAL = "";
            ?>
                @foreach($data as $item)

                    <?php $USUM = $item->U1+
                              $item->U2+
                              $item->U3+$item->U4+
                              $item->U5+$item->U6+
                              $item->U7+$item->U8+
                              $item->U9+$item->U10+
                              $item->U11+$item->U12;

                           $KSUM = $item->K1+
                              $item->K2+
                              $item->K3+$item->K4+
                              $item->K5+$item->K6+
                              $item->K7+$item->K8+
                              $item->K9+$item->K10+
                              $item->K11+$item->K12 ;

                        if($item->INDUSTRIAL!=$PREV_INDUSTRIAL) {
                            $PREV_INDUSTRIAL = $item->INDUSTRIAL;
                            $INDUSTRIAL = $item->INDUSTRIAL;
                        }else {
                            $INDUSTRIAL = "";
                        }

                    ?> 
      
                 <tr>
                    <td>{{ $INDUSTRIAL }}</td>
                    <td>{{$item->SYMBOL}}</td> 

                    <td style="text-align: right;"><font color={{$item->U1 >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$item->U1, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->U2 >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$item->U2, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->U3 >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$item->U3, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->U4 >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$item->U4, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->U5 >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$item->U5, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->U6 >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$item->U6, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->U7 >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$item->U7, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->U8 >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$item->U8, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->U9 >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$item->U9, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->U10 >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$item->U10, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->U11 >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$item->U11, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->U12 >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$item->U12, 2, '.', ',')}}</font></td>

                    <td style="text-align: right;"><font color={{$USUM >=0 ? "#000":"#ff0000"}}>{{number_format((float)($item->U1+
                                         $item->U2+
                                         $item->U3+$item->U4+
                                         $item->U5+$item->U6+
                                         $item->U7+$item->U8+
                                         $item->U9+$item->U10+
                                         $item->U11+$item->U12), 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->K1 >= 0 ?  "#000":"#ff0000"}}>{{number_format((float)$item->K1, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->K2 >= 0 ?  "#000":"#ff0000"}}>{{number_format((float)$item->K2, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->K3 >= 0 ?  "#000":"#ff0000"}}>{{number_format((float)$item->K3, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->K4 >= 0 ?  "#000":"#ff0000"}}>{{number_format((float)$item->K4, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->K5 >= 0 ?  "#000":"#ff0000"}}>{{number_format((float)$item->K5, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->K6 >= 0 ?  "#000":"#ff0000"}}>{{number_format((float)$item->K6, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->K7 >= 0 ?  "#000":"#ff0000"}}>{{number_format((float)$item->K7, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->K8 >= 0 ?  "#000":"#ff0000"}}>{{number_format((float)$item->K8, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->K9 >= 0 ?  "#000":"#ff0000"}}>{{number_format((float)$item->K9, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->K10 >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$item->K10, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->K11 >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$item->K11, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$item->K12 >= 0 ? "#000":"#ff0000"}}>{{number_format((float)$item->K12, 2, '.', ',')}}</font></td>
                    <td style="text-align: right;"><font color={{$KSUM >=0 ? "#000":"#ff0000"}}>{{number_format((float)($item->K1+
                                         $item->K2+
                                         $item->K3+$item->K4+
                                         $item->K5+$item->K6+
                                         $item->K7+$item->K8+
                                         $item->K9+$item->K10+
                                         $item->K11+$item->K12), 2, '.', ',')}}</font></td>        
                </tr>
            @endforeach
        @endif
                
                <!-- END: FAKE -->  
                
            </tbody>
        <tfoot>
            <tr>
                <td colspan="28">
                    {!! $htmlPaginate !!}
                </td>
            </tr>
        </tfoot>
    </table>

    <div> </div>

</div>