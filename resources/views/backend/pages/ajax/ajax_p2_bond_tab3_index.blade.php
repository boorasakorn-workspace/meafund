<!-- FILE: ajax_p2_bond_tab3_index.blade.php -->
@if(count($data) > 0) 
<table class="table  table-bordered"  >

    <thead>
    <tr>
         <th style="width:8%;text-align: center;"> </th>
         <th style="width:8%;text-align: center;"> </th>
         <th style="width:8%;text-align: center;"> </th>
         <th colspan="2" style="width:8%;text-align: center;">Local Issue Rating</th>
         <th colspan="4" style="width:8%;text-align: center;">International Issue Rating</th>
         <th style="width:8%;text-align: center;"> </th>
         <th style="width:8%;text-align: center;"> </th>
         <th style="width:8%;text-align: center;"> </th>
         <th style="width:8%;text-align: center;"> </th>
    
    </tr>

    <tr>
        <th style="width:5%;text-align: center" id="index_th">
            <input type="checkbox" id="mainCheck" />
        </th>
        <th style="width:8%;text-align: center;">Action</th>
        <th style="width:10%;text-align: center;">Symbol</th>
        <th style="text-align: center;" data-class="expand">TRIS</th>
        <th style="width:8%;text-align: center;">Fitch (Thailand)</th>
        <th style="width:8%;text-align: center;">Moody’s</th>
        <th style="text-align: center;" data-class="expand">S&P</th>
        <th style="text-align: center;" data-class="expand">Fitch Ratings</th>
        <th style="text-align: center;" data-class="expand">R&I</th>
        <th style="text-align: center;" data-class="expand">Coupon Type</th>
        <th style="text-align: center;" data-class="expand">Coupon Rate (%)</th>
        <th style="text-align: center;"data-class="expand">Issue Date</th>
        <th style="text-align: center;"data-class="expand">Maturity Date</th>
    </tr>
    </thead>
   
    <tbody>
    
        @foreach($data as $item)
            <tr>
                
                <td style="text-align: center;width:5px"   nowrap>
                  <input type="checkbox"  name="check_item_edit[]" value="{{$item->INDEX_ID}}" class="item_checked" id="item_check_{{$item->INDEX_ID}}" />
                </td>

                <td style="text-align: center" nowrap>
                    <a href="/admin/BondCompany/editBondIndex/{{$item->INDEX_ID}}" class="btn btn-primary btn-xs">
                        <i class="fa fa-gear"></i>
                    </a>
                    <a href="javascript:void(0);" data-id="{{$item->INDEX_ID}}" class="mea_delete_by btn bg-color-red txt-color-white btn-xs">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                </td>
                <td style="text-align: left">{{$item->SYMBOL}}</td>
                
                <td style="text-align: right" nowrap>{{$item->LIR1}}</td> 
                <td style="text-align: right" nowrap>{{$item->LIR2}}</td> 
                <td style="text-align: right" nowrap>{{$item->IIR1}}</td>   
                <td style="text-align: right" nowrap>{{$item->IIR2}}</td> 
                <td style="text-align: right" nowrap>{{$item->IIR3}}</td> 
                <td style="text-align: right" nowrap>{{$item->IIR4}}</td> 
                <td style="text-align: right" nowrap>{{$item->COUPON_TYPE}}</td> 
                <td style="text-align: right" nowrap>{{number_format((float)$item->COUPON_RATE, 2, '.', ',')}}</td> 
                <td style="text-align: right"  nowrap>{{$item->ISSUE_DATE}}</td> 
                <td style="text-align: right"  nowrap>{{$item->MATURITY_DATE}}</td>
            </tr>
      @endforeach

     
    </tbody>
    

    <tfoot>
        <tr>
            <td colspan="11">
                {!! $htmlPaginate !!}
            </td>
        </tr>
    </tfoot>
</table>
 @else
    <div class="list" style="margin-bottom: 10px; padding: 10px">
        <p style="font-size: 20px;padding: 10px;text-align: center;width: 100%;background-color: #f1f1f1;border: 1px solid #E1E8F3">ไม่พบข้อมูลในระบบ</p>
    </div>

    @endif






