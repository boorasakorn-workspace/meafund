<!-- FILE: ajax_p2_bond_tab3_index_form.blade.php -->
<link rel="stylesheet" href="{{asset('backend/css/jquery-ui.css')}}">
<style>
  .custom-combobox {
    position: relative;
    display: inline-block;
  }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
  }
  .custom-combobox-input {
    margin: 0;
    padding: 5px 10px;
  }

  optgroup {
    background-color: #FF8000; //#DCDCDC;
    color: white;
  }
  option {
    background-color: white;
    color: black;
  }

  /* auto complete */
   .container { width: 800px; margin: 0 auto; }

   .autocomplete-suggestions { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 1px solid #999; background: #FFF; cursor: default; overflow: auto; -webkit-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); -moz-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); }
   .autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
   .autocomplete-no-suggestion { padding: 2px 5px;}
   .autocomplete-selected { background: #F0F0F0; }
   .autocomplete-suggestions strong { font-weight: bold; color: #000; }
   .autocomplete-group { padding: 2px 5px; }
   .autocomplete-group strong { font-weight: bold; font-size: 18px; color: #000; display: block; border-bottom: 1px solid #000; }


</style>


<div class="widget-body no-padding" style="width: 100% ;margin: 0 auto;background-color: #000000;border: 1px solid #E1E8F3">
    <header style="color: #fff; height: 40px; line-height: 40px; font-size: 18px; background-color: #a90329;padding-left: 20px;"> กล่องค้นหา </header>

    <form  id="comment-form_darkman" class="smart-form" novalidate="novalidate">
        {!! csrf_field() !!}    
        <fieldset>
            <div class="row">
                    <section class="col col-4">
                      <label class="label">ชื่อย่อ</label>
                      <div class="ui-widget">
                          <label class="label"></label>
                            <label class="input">                     
                                <input type="text" name="symbol" placeholder="ระบุชื่อย่อ"
                                   id="symbol" style="width: 100%; border-width: 1px;"/>
                          </label>
                      </div>
                    </section>
            </div>

            <div class="row">
                <section class="col col-4">
                    <!--
                    <button type="submit" id="btn_search" 
                            style="padding: 5px 20px 5px 20px; margin-top:20px;font-size: 18px;" 
                            name="submit" class="btn btn-primary">
                        ค้นหา
                    </button>
                    -->
                    <button type="button"  id="btn_search"  name="btn_search" 
                            style="padding: 5px 20px 5px 20px; margin-top:20px;font-size: 18px;" 
                            class="btn btn-primary"> ค้นหา</button>
                </section>
            </div>
        </fieldset>
    </form>
</div>


