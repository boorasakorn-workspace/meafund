<!-- FILE: ajax_p2_equity_tab2_data_import.blade.php -->

<table class="table  table-bordered"  >

    <thead>

    <tr>
        <th style="width:5%;text-align: center" id="index_th">
            <input type="checkbox" id="mainCheck" />
        </th>

        <th style="width:8%;text-align: center">Action</th>
        <!--th data-class="expand">รหัส หมวดหมู่หลักทรัพย์</th-->
        <th style="width:10%;">ตลาด</th>
        <th data-class="expand">กลุ่มอุตสาหกรรม</th>
        <th data-class="expand">หมวดธุรกิจ</th>
    
    </tr>
    </thead>

    <tbody>
      @if($data)
        @foreach($data as $item)
            <tr>
                <td style="text-align: center"><input type="checkbox"  name="check_item_edit[]" value="{{$item->CATE_ID}}" class="item_checked" id="item_check_{{$item->CATE_ID}}" />
                </td>
                <td style="text-align: center">
                    <a href="/admin/BondCompany/editCategory/{{$item->CATE_ID}}" class="btn btn-primary btn-xs"><i class="fa fa-gear"></i></a>
                    <a href="javascript:void(0);"  data-id="{{$item->CATE_ID}}" class="mea_delete_by btn bg-color-red txt-color-white btn-xs"><i class="glyphicon glyphicon-trash"></i></a>
                </td>
                 
                <!--td>{{$item->CATE_ID}}</td-->
                <td title="รหัสหมวดหมู่หลักทรัพย์ : {{$item->CATE_ID}}">{{$item->MARKET}}</td>
                <td>{{$item->INDUSTRIAL}}</td>
                <td>{{$item->BU}}</td>
            </tr>
        @endforeach

    @endif

    </tbody>
    <tfoot>
    <tr>
        <td colspan="11">
            {!! $htmlPaginate !!}
        </td>
    </tr>
    </tfoot>
</table>