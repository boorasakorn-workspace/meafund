<!--
SECURITY    NAME  Maturity Date   TY RATE FACE VALUE/UNIT TOTAL COST      Red Yield   AMORTIZED COST  DISC/PREM   ACCRUED INT  DAY INT ENT(EX)  MKT PRICE   DIRTY VALUE %NAV          ALLOWANCE
-->

<div id="trading_table" class="floatThead-wrapper" style="position: relative; clear:both;">
{{-- <div id="trading_table" style="display: flex;"> --}}
{{-- <div id="trading_table" style="display:block;"> --}}
<div>Showing {{$PageNumber}} to {{ (($PageSize * $PageNumber) > $totals? $totals:($PageSize * $PageNumber))  }} of {{ $totals }}</div>
<table id="display_data_table" class="table table-bordered" style="table-layout: auto;">
    <thead>
    <tr>
        <th colspan="21" style="text-align: center">
            <p class="report-title">{{$TableTitle}} </p> <!-- รายงานความเคลื่อนไหวตราสารทุน -->
        </th>
    </tr>
    
    <tr>
        <th colspan="21" style="text-align: center">{!! $pretty_date !!}</th>
    </tr>

    <tr>
        <th rowspan="2" colspan="1" class="showcol" style="text-align: center"> REF. DATE </th>
        <th rowspan="2" colspan="1" class="showcol" style="text-align: center"> ASSET MGT.</th>
        <th rowspan="2" colspan="1" class="showcol" style="text-align: center"> NAME </th>  
        <th rowspan="2" colspan="1" class="showcol" style="text-align: center"> ISSUER </th>  
        <th rowspan="2" colspan="1" class="showcol" style="text-align: center"> MATURITY DATE </th>  
        
       
        
        <th rowspan="2" colspan="1" class="showcol" style="text-align: center"> FACE VALUE/UNIT </th> 
        <th rowspan="2" colspan="1" class="showcol" style="text-align: center"> RATE </th>   
        <th rowspan="2" colspan="1" class="showcol" style="text-align: center"> TOTAL COST </th>  
        <th rowspan="2" colspan="1" class="showcol" style="text-align: center"> Red Yield </th>  
        <th rowspan="2" colspan="1" class="showcol" style="text-align: center"> AMORTIZED COST </th>     
        <th rowspan="2" colspan="1" class="showcol" style="text-align: center"> DISC/PREM  </th>   
        <th rowspan="2" colspan="1" class="showcol" style="text-align: center"> ACCRUED INT  </th>   

        <th rowspan="2" colspan="1" class="showcol" style="text-align: center"> DAY </th>
        <th rowspan="2" colspan="1" class="showcol" style="text-align: center"> INT ENT(EX) </th>
        <th rowspan="2" colspan="1" class="showcol" style="text-align: center"> MKT PRICE </th>      
        <th rowspan="2" colspan="1" class="showcol" style="text-align: center"> DIRTY VALUE </th>   
        <th rowspan="2" colspan="1" class="showcol" style="text-align: center"> %NAV </th> 
        <th rowspan="2" colspan="1" class="showcol" style="text-align: center"> ALLOWANCE </th> 
        <th rowspan="1" colspan="3" class="showcol" style="text-align: center"> อายุตราสาร </th>       
    </tr>
    <tr>
        <th  colspan="1" class="showcol" style="text-align: center"> Year </th>  
        <th  colspan="1" class="showcol" style="text-align: center"> Month </th>  
        <th  colspan="1" class="showcol" style="text-align: center"> Day </th>  
    </tr>
    </thead>
        <tbody>

            @php

            $FACE_VALUE       = 0.0;
            $TOTAL_COST       = 0.0;
            $AMORT_VALUE      = 0.0;
            $DIS_PREM         = 0.0;
            $ACCRUED_INT      = 0.0;
            $INT_EX           = 0.0;
            $DIRTY_MKT        = 0.0;
            $NAV_PERCENTAGE   = 0.0;
            $ALLOWANCE        = 0.0;

            @endphp
          

            @if($data) 
            @foreach($data as $item) 
                    @inject('service', 'App\Http\Controllers\AdminP2BondPortInvestmentReportController')  

                    @php
                    $FACE_VALUE       += $item->FACE_VALUE;
                    $TOTAL_COST       += $item->TOTAL_COST;
                    $AMORT_VALUE      += $item->AMORT_VALUE;
                    $DIS_PREM         += $item->DIS_PREM;
                    $ACCRUED_INT      += $item->ACCRUED_INT;
                    $INT_EX           += $item->INT_EX;
                    $DIRTY_MKT        += $item->DIRTY_MKT;
                    $NAV_PERCENTAGE   += $item->NAV_PERCENTAGE;
                    $ALLOWANCE        += $item->ALLOWANCE;
                    @endphp

            <tr>      
                <td style="text-align: right"   nowrap>{{date('d-m-Y', strtotime($item->REFERENCE_DATE))}}</td>        <!-- REFERENCE_DATE -->

                <td style="text-align: right"   nowrap>{{$item->SECURITIES_NAME}}</td>       <!-- SECURITY -->
                <td style="text-align: center"  nowrap>{{$item->SYMBOL}}</td>                <!-- NAME -->
                <td style="text-align: right"   nowrap>{{$item->ISSUER}}</td>       <!-- ISSUER -->
                
                @if(($service->matGetYear(date('Y-m-d'),  $item->MAT)[0]) >= 118) 
                <td style="text-align: center"  nowrap>-</td>                                                         <!-- Maturity Date -->
                @else
                <td style="text-align: center"  nowrap>{{date('d-m-Y', strtotime($item->MAT))}}</td>                   <!-- Maturity Date -->
                @endif

               
                
                @if((float)$item->FACE_VALUE < 0)
                <td style="text-align: right" ><font color="#ff0000">{{number_format((float)$item->FACE_VALUE, 2, '.', ',')}}</font></td>  <!-- FACE VALUE/UNIT -->
                @else
                <td style="text-align: right" >{{number_format((float)$item->FACE_VALUE, 2, '.', ',')}}</td>  <!-- FACE VALUE/UNIT -->
                @endif

                @if((float)$item->RATE < 0)
                <td style="text-align: right" ><font color="#ff0000">{{number_format((float)$item->RATE, 2, '.', ',')}}</font></td>  <!-- RATE -->
                @else
                <td style="text-align: right" >{{number_format((float)$item->RATE, 2, '.', ',')}}</td>  <!-- RATE-->
                @endif
                 
                 
                <td style="text-align: right" >{{number_format((float)$item->TOTAL_COST, 2, '.', ',')}}</td>  <!-- TOTAL COST -->
                <td style="text-align: right" >{{number_format((float)$item->R_YIELD, 6, '.', ',')}}</td>     <!-- Red Yield -->

                <td style="text-align: right" >{{number_format((float)$item->AMORT_VALUE, 2, '.', ',')}}</td> <!-- AMORTIZED COST -->
                @if((float)$item->DIS_PREM < 0)
                <td style="text-align: right" ><font color="#ff0000">{{number_format((float)$item->DIS_PREM, 2, '.', ',')}}</font></td>     <!-- DISC/PREM -->
                @else
                <td style="text-align: right" >{{number_format((float)$item->DIS_PREM,2, '.', ',')}}</td>     <!-- DISC/PREM -->
                @endif

                @if((float)$item->ACCRUED_INT < 0)
                <td style="text-align: right" ><font color="#ff0000">{{number_format((float)$item->ACCRUED_INT,2, '.', ',')}}</font></td>  <!-- ACCRUED INT -->
                @else
                <td style="text-align: right" >{{number_format((float)$item->ACCRUED_INT,2, '.', ',')}}</td>  <!-- ACCRUED INT -->
                @endif

                <td style="text-align: right"   nowrap>{{$item->DAY}}</td>       <!-- DAY -->

                                                                  
                <td style="text-align: right" >{{number_format((float)$item->INT_EX, 2, '.', ',')}}</td>          <!-- INT ENT(EX)-->
                <td style="text-align: right" >{{number_format((float)$item->MKTP_YIELD, 2, '.', ',')}}</td>      <!-- MKT PRICE -->
                
                @if((float)$item->DIRTY_MKT < 0)
                <td style="text-align: right" ><font color="#ff0000">{{number_format((float)$item->DIRTY_MKT,2, '.', ',')}}</font></td>        <!-- DIRTY VALUE -->
                @else
                <td style="text-align: right" >{{number_format((float)$item->DIRTY_MKT,2, '.', ',')}}</td>        <!-- DIRTY VALUE -->
                @endif

                <td style="text-align: right" >{{number_format((float)$item->NAV_PERCENTAGE, 2, '.', ',')}}</td>  <!-- %NAV -->
                @if((float)$item->ALLOWANCE < 0)
                <td style="text-align: right" ><font color="#ff0000">{{number_format((float)$item->ALLOWANCE,2, '.', ',')}}</font></td>        <!-- ALLOWANCE -->
                @else
                <td style="text-align: right" >{{number_format((float)$item->ALLOWANCE,2, '.', ',')}}</td>        <!-- ALLOWANCE -->
                @endif

                {{-- @if(($service->matGetYear(date('Y-m-d'),  $item->MAT)[0]) >= 118)  --}}
                @if(($service->matGetYear($real_date_end,  $item->MAT)[0]) >= 118)
                <td style="text-align: right" >NUM</td>  
                <td style="text-align: right" >NUM</td>  
                <td style="text-align: right" >NUM</td>  
                @else
                {{-- <td style="text-align: right" >{{ $service->matGetYear(date('Y-m-d'),  $item->MAT)[0] }}</td>  
                <td style="text-align: right" >{{ $service->matGetYear(date('Y-m-d'),  $item->MAT)[1] }}</td>  
                <td style="text-align: right" >{{ $service->matGetYear(date('Y-m-d'),  $item->MAT)[2] }}</td>  --}}
                <td style="text-align: right" >{{ $service->matGetYear($real_date_end,  $item->MAT)[0] }}</td>  
                <td style="text-align: right" >{{ $service->matGetYear($real_date_end,  $item->MAT)[1] }}</td>  
                <td style="text-align: right" >{{ $service->matGetYear($real_date_end,  $item->MAT)[2] }}</td>   
                @endif
            </tr>
            @endforeach
            @endif
            {{-- 
            <!-- total -->
            <tr>      
                <td style="text-align: right"  nowrap></td>                 <!--  -->
                <td style="text-align: right"  nowrap></td>                 <!--  -->
                <td style="text-align: center" nowrap></td>                <!--  -->

                <td style="text-align: center"  nowrap></td>                <!--  -->
                <td style="text-align: right"  nowrap><font color="#2f1eff">รวม</font></td>            <!-- รวม -->

                @if($FACE_VALUE < 0)
                <td style="text-align: right" ><font color="#ff0000">{{number_format((float)$FACE_VALUE, 2, '.', ',')}}</font></td>  <!-- FACE VALUE/UNIT -->
                @else
                <td style="text-align: right" ><font color="#2f1eff">{{number_format((float)$FACE_VALUE, 2, '.', ',')}}</font></td>  <!-- FACE VALUE/UNIT -->
                @endif

                
                <td style="text-align: right" ></td>  <!-- RATE-->
                
                 
                 
                <td style="text-align: right" ><font color="#2f1eff">{{number_format((float)$TOTAL_COST, 2, '.', ',')}}</font></td>  <!-- TOTAL COST -->
                <td style="text-align: right" ></td>     <!-- Red Yield -->

                <td style="text-align: right" ><font color="#2f1eff">{{number_format((float)$AMORT_VALUE, 2, '.', ',')}}</font></td> <!-- AMORTIZED COST -->
                @if((float)$DIS_PREM < 0) 

                <td style="text-align: right" ><font color="#ff0000">{{number_format((float)$DIS_PREM, 2, '.', ',')}}</font></td>     <!-- DISC/PREM -->
                @else
                <td style="text-align: right" ><font color="#2f1eff">{{number_format((float)$DIS_PREM,2, '.', ',')}}</font></td>     <!-- DISC/PREM -->
                @endif

                @if((float)$ACCRUED_INT < 0)
                <td style="text-align: right" ><font color="#ff0000">{{number_format((float)$ACCRUED_INT, 2, '.', ',')}}</font></td>  <!-- ACCRUED INT -->
                @else
                <td style="text-align: right" ><font color="#2f1eff">{{number_format((float)$ACCRUED_INT,2, '.', ',')}}</font></td>  <!-- ACCRUED INT -->
                @endif

                <td style="text-align: right"></td>                                                              <!-- DAY -->
                <td style="text-align: right" ><font color="#2f1eff">{{number_format((float)$INT_EX, 2, '.', ',')}}</font></td>          <!-- INT ENT(EX)-->
                <td style="text-align: right" ></td>      <!-- MKT PRICE -->
                
                @if((float)$DIRTY_MKT < 0)
                <td style="text-align: right" ><font color="#ff0000">{{number_format((float)$DIRTY_MKT, 2, '.', ',')}}</font></td>        <!-- DIRTY VALUE -->
                @else
                <td style="text-align: right" ><font color="#2f1eff">{{number_format((float)$DIRTY_MKT, 2, '.', ',')}}</font></td>        <!-- DIRTY VALUE -->
                @endif

                <td style="text-align: right" ><font color="#2f1eff">{{number_format((float)$NAV_PERCENTAGE, 2, '.', ',')}}</font></td>  <!-- %NAV -->

                @if((float)$ALLOWANCE < 0)
                <td style="text-align: right" ><font color="#ff0000">{{number_format((float)$ALLOWANCE, 2, '.', ',')}}</font></td>        <!-- ALLOWANCE -->
                @else
                <td style="text-align: right" ><font color="#2f1eff">{{number_format((float)$ALLOWANCE, 2, '.', ',')}}</font></td>        <!-- ALLOWANCE -->
                @endif
                <td style="text-align: right" > </td>  
                <td style="text-align: right" > </td>  
                <td style="text-align: right" > </td>  
            </tr> 
            --}}

            <!-- Grand toal-->
            @if($grandtoal) 
            @foreach($grandtoal as $item)  
            <tr>      
                <td style="text-align: right"   nowrap></td>                 <!--  -->
                <td style="text-align: right"   nowrap></td>                 <!--  -->
                <td style="text-align: center"  nowrap></td>                <!--  -->
               
                <td style="text-align: center"  nowrap></td>                <!--  -->
                <td style="text-align: right"   nowrap><font color="#2f1eff">รวมทั้งสิ้น</font></td>            <!-- รวมทั้งสิ้น -->

                @if($item->SUM_FACE_VALUE < 0)
                <td style="text-align: right" ><font color="#ff0000">{{number_format((float)$item->SUM_FACE_VALUE, 2, '.', ',')}}</font></td>  <!-- FACE VALUE/UNIT -->
                @else
                <td style="text-align: right" ><font color="#2f1eff">{{number_format((float)$item->SUM_FACE_VALUE, 2, '.', ',')}}</font></td>  <!-- FACE VALUE/UNIT -->
                @endif

                <td style="text-align: right" ></td>     <!-- RATE-->
                <td style="text-align: right" ><font color="#2f1eff">{{number_format((float)$item->SUM_TOTAL_COST, 2, '.', ',')}}</font></td>  <!-- TOTAL COST -->
                <td style="text-align: right" ></td>     <!-- Red Yield -->
                <td style="text-align: right" ><font color="#2f1eff">{{number_format((float)$item->SUM_AMORT_VALUE, 2, '.', ',')}}</font></td> <!-- AMORTIZED COST -->

                @if((float)$item->SUM_DIS_PREM < 0)
                <td style="text-align: right" ><font color="#ff0000">{{number_format((float)$item->SUM_DIS_PREM, 2, '.', ',')}}</font></td>     <!-- DISC/PREM -->
                @else
                <td style="text-align: right" ><font color="#2f1eff">{{number_format((float)$item->SUM_DIS_PREM,2, '.', ',')}}</font></td>     <!-- DISC/PREM -->
                @endif

                @if((float)$item->SUM_ACCRUED_INT < 0)
                <td style="text-align: right" ><font color="#ff0000">{{number_format((float)$item->SUM_ACCRUED_INT, 2, '.', ',')}}</font></td>  <!-- ACCRUED INT -->
                @else
                <td style="text-align: right" ><font color="#2f1eff">{{number_format((float)$item->SUM_ACCRUED_INT,2, '.', ',')}}</font></td>  <!-- ACCRUED INT -->
                @endif

                <td style="text-align: right" ></td>                                                              <!-- DAY -->
                <td style="text-align: right" ><font color="#2f1eff">{{number_format((float)$item->SUM_INT_EX, 2, '.', ',')}}</font></td>          <!-- INT ENT(EX)-->
                <td style="text-align: right" ></td>      <!-- MKT PRICE -->

                @if((float)$item->SUM_DIRTY_MKT < 0)
                <td style="text-align: right" ><font color="#ff0000">{{number_format((float)$item->SUM_DIRTY_MKT,2, '.', ',')}}</font></td>        <!-- DIRTY VALUE -->
                @else
                <td style="text-align: right" ><font color="#2f1eff">{{number_format((float)$item->SUM_DIRTY_MKT,2, '.', ',')}}</font></td>        <!-- DIRTY VALUE -->
                @endif

                <td style="text-align: right" ><font color="#2f1eff">{{number_format((float)$item->SUM_NAV_PERCENTAGE, 2, '.', ',')}}</font></td>  <!-- %NAV -->

                @if((float)$item->SUM_ALLOWANCE < 0)
                <td style="text-align: right" ><font color="#ff0000">{{number_format((float)$item->SUM_ALLOWANCE,2, '.', ',')}}</font></td>        <!-- ALLOWANCE -->
                @else
                <td style="text-align: right" ><font color="#2f1eff">{{number_format((float)$item->SUM_ALLOWANCE,2, '.', ',')}}</font></td>        <!-- ALLOWANCE -->
                @endif               

                <td style="text-align: right" > </td>  
                <td style="text-align: right" > </td>  
                <td style="text-align: right" > </td>  
            </tr> 
            @endforeach   
            @endif
		</tbody>


    <tfoot>

        <tr>
            <td colspan="21">
                {!! $htmlPaginate !!}
            </td>
        </tr>
    </tfoot>
</table>
</div>

