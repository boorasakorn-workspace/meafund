
<div>Showing {{$PageNumber}} to {{ (($PageSize * $PageNumber) > $totals? $totals:($PageSize * $PageNumber))  }} of {{ $totals }}</div>
<table class="table table-bordered">
    
    <thead>
    <tr>
        <th colspan="15" style="text-align: center; color:#ffffff;"  bgcolor="#393939">รายงานข้อมูล ณ วันที่ {{get_date_notime(date("Y-m-d H:i:s"))}}</th>
    </tr>
    <tr>
        <th colspan="15" style="text-align: center; color:#ffffff;" bgcolor="#393939">
             <strong style="font-size: 18px;">กองทุนสำรองเลี้ยงชีพพนักงานการไฟฟ้านครหลวง ซึ่งจดทะเบียนแล้ว (MEAEQ)</strong>
        </th>
    </tr>
    <tr>

        <th rowspan="2" style="text-align: center;" id="index_th1" bgcolor="#D3D3D3">
            <input type="checkbox" id="mainCheckBond" />
        </th>
        <th rowspan="2" style="text-align: center; vertical-align: text-top;" bgcolor="#D3D3D3"></th>
        <th rowspan="2" style="text-align: center; " width="5%" bgcolor="#D3D3D3"> วันที่ </th>
        <th rowspan="2" style="text-align: center; " bgcolor="#D3D3D3"> บลจ </th>
        
        <th colspan="7" style="text-align: center; vertical-align: text-top;" bgcolor="#D3D3D3">
             <strong style="font-size: 18px;" >นโยบายตราสารหนี้</strong>
        </th>
        <!--
        <th colspan="8" style="text-align: center; vertical-align: text-top;" bgcolor="#D3D3D3">
             <strong style="font-size: 18px;" >นโยบายตราสารทุน</strong>
        </th>
        -->
    </tr>

    <tr>
        
        <th style="text-align: center; vertical-align: text-top;" bgcolor="#D3D3D3">มูลค่าทรัพย์สินสุทธิ (บาท) </th>
        <th style="text-align: center; vertical-align: text-top;" bgcolor="#D3D3D3">จำนวนหน่วยลงทุน</th>
        <th style="text-align: center; vertical-align: text-top;" bgcolor="#D3D3D3">มูลค่าทรัพย์สินสุทธิต่อหน่วย(บาท) (12 ตำแหน่ง)</th>
        <th style="text-align: center; vertical-align: text-top;" bgcolor="#D3D3D3">มูลค่าทรัพย์สินสุทธิต่อหน่วย(บาท) (4 ตำแหน่ง)</th>
        <th style="text-align: center; vertical-align: text-top;" bgcolor="#D3D3D3">อัตรา ผลตอบแทนรายเดือน (%)</th>
        <th style="text-align: center; vertical-align: text-top;" bgcolor="#D3D3D3">อัตรา ผลตอบแทนสะสม (%)</th>
     <!--
        <th style="text-align: center" id="index_th2" bgcolor="#D3D3D3">
            <input type="checkbox" id="mainCheckEquity" />
        </th>
        <th style="text-align: center; vertical-align: text-top;" bgcolor="#D3D3D3"></th>
        <th style="text-align: center; vertical-align: text-top;" bgcolor="#D3D3D3">มูลค่าทรัพย์สิน สุทธิ (บาท) </th>
        <th style="text-align: center; vertical-align: text-top;" bgcolor="#D3D3D3">จำนวนหน่วยลงทุน</th>
        <th style="text-align: center; vertical-align: text-top;" bgcolor="#D3D3D3">มูลค่าทรัพย์สินสุทธิต่อหน่วย(บาท) (12 ตำแหน่ง)</th>
        <th style="text-align: center; vertical-align: text-top;" bgcolor="#D3D3D3">มูลค่าทรัพย์สินสุทธิต่อหน่วย(บาท) (4 ตำแหน่ง)</th>
        <th style="text-align: center; vertical-align: text-top;" bgcolor="#D3D3D3">อัตรา ผลตอบแทนรายเดือน (%)</th>
        <th style="text-align: center; vertical-align: text-top;" bgcolor="#D3D3D3">อัตรา ผลตอบแทนสะสม (%)</th>
      -->  
    </tr>
    </thead>
   
    <tbody>
    @if($data)
        @foreach($data as $item)
        <tr>
            <!--td >ไม่พบรายการ</td-->
            @if($item->REF2 != null)
            <td style="text-align: center"><input type="checkbox"  name="check_item_edit[]" 
                value="B.{{$item->JOB2}}" class="item_checked" id="item_check" />
            </td>     
            @endif
            <!-- ตราสารหนี้ทุน -->   
            @if($item->REF2 != null)
                <td style="text-align: center;" nowrap>
                    <a href="/admin/BondDataImport/editUnitVal/B.{{$item->JOB2}}" class="btn btn-primary btn-xs"><i class="fa fa-gear"></i></a>
                    <a href="javascript:void(0);"  data-id="B.{{$item->JOB2}}" class="mea_delete_by btn bg-color-red txt-color-white btn-xs"> <i class="glyphicon glyphicon-trash"></i></a>
                </td>
                <td style="text-align: right" nowrap>{{toThaiDateTime($item->REF2, false)}}</td>               <!-- ถึง -->
                <td style="text-align: center">{{$item->SEC2}}</td>                                            <!-- จำนวณวัน -->
                <td style="text-align: right">{{number_format((float)$item->B_NAV_B, 2, '.', ',')}}</td>       <!-- จำนวณหน่วย -->
                <td style="text-align: right">{{number_format((float)$item->B_UNIT, 4, '.', ',')}}</td>        <!-- ค่าใช้จ่ายอื่น -->
                <td style="text-align: right">{{number_format((float)$item->B_NAV_UNIT, 12, '.', ',')}}</td>       <!-- ค่าสอบบัญชี-->
                <td style="text-align: right">{{number_format((float)$item->B_NAV_UNIT, 4, '.', ',')}}</td>    <!-- ก่อนคำนวณ MGT FEE &amp; CUST FEE -->
                <td style="text-align: right">{{number_format((float)$item->B_YIELD_MONTH, 2, '.', ',')}}</td> <!-- ค่าะรรมเนียมรับฝากสินทรัพย์ -->
                <td style="text-align: right">{{number_format((float)$item->B_YIELD_CUMULATIVE, 2, '.', ',')}}</td>      <!-- ค่าธรรมเนียมการจัดการ -->
            @endif
        </tr>
        @endforeach
    @endif

        <!-- END: FAKE -->  
    </tbody>
    <tfoot>
        <tr>
            <td colspan="15">
                {!! $htmlPaginate !!}
            </td>
        </tr>
    </tfoot>
</table>

<div>


</div>