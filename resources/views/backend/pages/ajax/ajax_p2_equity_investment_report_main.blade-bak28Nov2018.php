 <!-- FILE: ajax_p2_equity_port_investment.blade.php -->
 
    <div id="mini_table1"  >
        <table class="table table-bordered"  width="400px" id="table_bond_nav" cellspacing="0">
            <thead>
                <tr>
                    <th colspan="19" style="text-align: center">กองทุนสํารองเล้ียงชีพพนักงานการไฟฟ้านครหลวง ซึ่งจดทะเบียนแล้ว</th>
                </tr>
                <tr>
                    <th colspan="19" style="text-align: center"> รายงานข้อมูลมูลค่าหน่วยลงทุนและอัตราผลตอบแทน</th>
                </tr>
                <tr>
                    <th colspan="19" style="text-align: center">  {!! $pretty_date !!}</th>
                </tr>
                <tr>
                    <th colspan="1" rowspan="2" style="text-align: center">นโยบายการลงทุน</th>
                    <th colspan="1" rowspan="2" style="text-align: center">ว./ด./ป.</th>
                    <th colspan="3" style="text-align: center">UOBAM</th>
                    <th colspan="1" style="text-align: center"> </th>
                    <th colspan="1" style="text-align: center"> ล้านบาท </th> 
                    <th colspan="2" style="text-align: center"> Benchmark </th> 

                    <th colspan="3" style="text-align: center">KTAM</th>
                    <th colspan="1" style="text-align: center"> </th>
                    <th colspan="1" style="text-align: center"> ล้านบาท </th> 

                    <th colspan="3" style="text-align: center">CONSOLIDATE</th>
                    <th colspan="1" style="text-align: center"> </th>
                    <th colspan="1" style="text-align: center"> ล้านบาท </th> 
                </tr>

                <tr>
                    <th colspan="1" style="text-align: center">NAV (ล้านบาท)</th>
                    <th colspan="1" style="text-align: center">จำนวณหน่วย (ล้านบาท)</th>
                    <th colspan="1" style="text-align: center">NAV ต่อหน่วย</th>
                    <th colspan="1" style="text-align: center">อัตราตอบแทนรายเดือน</th> 
                    <th colspan="1" style="text-align: center">อัตราตอบแทนสะสม (%)</th> 
                    <th colspan="1" style="text-align: center">รายเดือน (%)</th> 
                    <th colspan="1" style="text-align: center">สะสม (%)</th> 
                    
                    <th colspan="1" style="text-align: center">NAV (ล้านบาท)</th>
                    <th colspan="1" style="text-align: center">จำนวณหน่วย (ล้านบาท)</th>
                    <th colspan="1" style="text-align: center">NAV ต่อหน่วย</th>
                    <th colspan="1" style="text-align: center">อัตราตอบแทนรายเดือน</th> 
                    <th colspan="1" style="text-align: center">อัตราตอบแทนสะสม (%)</th> 
                    
                    <th colspan="1" style="text-align: center">NAV (ล้านบาท)</th>
                    <th colspan="1" style="text-align: center">จำนวณหน่วย (ล้านบาท)</th>
                    <th colspan="1" style="text-align: center">NAV ต่อหน่วย</th>
                    <th colspan="1" style="text-align: center">อัตราตอบแทนรายเดือน</th> 
                    <th colspan="1" style="text-align: center">อัตราตอบแทนสะสม (%)</th> 
                </tr>
            </thead>

            <tbody>
            @if($data)
                @php 
                $old = "";
                $last_NAV_B_UOBAM = 0;
                $last_NAV_B_KTAM = 0;
                $sum_NAV_B_UOBAM = 0;
                $sum_NAV_B_KTAM = 0;
                @endphp
                @foreach($data as $index =>$field)
                
                
                <tr>
                @if($old!= $field->POLICY_ID)
                    @php
                    if ($old != "") {
                        $sum_NAV_B_UOBAM = $last_NAV_B_UOBAM;
                        $sum_NAV_B_KTAM = $last_NAV_B_KTAM;
                    }  
                    @endphp
                    <td colspan="1" style="text-align: left">{{($field->POLICY_ID == 1) ? "ตราสารทุน" : "ตราสารหนี้"}}</td>
                @else
                <td colspan="1" style="text-align: left"></td>
                @endif 
                    <td colspan="1" style="text-align: center">{{$field->REFERENCE_DATE}}</td>
                    <td colspan="1" style="text-align: right">{{number_format((float)$field->NAV_B_UOBAM, 2, '.', ',')}}</td>
                    <td colspan="1" style="text-align: right">{{number_format((float)$field->UNIT_UOBAM, 2, '.', ',')}}</td>
                    <td colspan="1" style="text-align: right">{{number_format((float)$field->NAV_UNIT_UOBAM, 2, '.', ',')}}</td>
                    <td colspan="1" style="text-align: right">{{number_format((float)$field->YIELD_MONTH_UOBAM, 2, '.', ',')}}</td> 
                    <td colspan="1" style="text-align: right">{{number_format((float)$field->YIELD_CUMULATIVE_UOBAM, 2, '.', ',')}}</td> 
                    <td colspan="1" style="text-align: right">{{number_format((float)$field->BM_MONTH_UOBAM, 2, '.', ',')}}</td> 
                    <td colspan="1" style="text-align: right">{{number_format((float)$field->BM_CUMULATIVE_UOBAM, 2, '.', ',')}}</td> 

                    <td colspan="1" style="text-align: right">{{number_format((float)$field->NAV_B_KTAM, 2, '.', ',')}}</td>
                    <td colspan="1" style="text-align: right">{{number_format((float)$field->UNIT_KTAM, 2, '.', ',')}}</td>
                    <td colspan="1" style="text-align: right">{{number_format((float)$field->NAV_UNIT_KTAM, 2, '.', ',')}}</td>
                    <td colspan="1" style="text-align: right">{{number_format((float)$field->YIELD_MONTH_KTAM, 2, '.', ',')}}</td> 
                    <td colspan="1" style="text-align: right">{{number_format((float)$field->YIELD_CUMULATIVE_KTAM, 2, '.', ',')}}</td> 
                    
                    <td colspan="1" style="text-align: right">{{number_format((float)$field->NAV_B_CUMULATIVE, 2, '.', ',')}}</td>
                    <td colspan="1" style="text-align: right">{{number_format((float)$field->UNIT_CUMULATIVE, 2, '.', ',')}}</td>
                    <td colspan="1" style="text-align: right">{{number_format((float)$field->NAV_UNIT_CUMULATIVE, 2, '.', ',')}}</td>
                    <td colspan="1" style="text-align: right">{{number_format((float)$field->YIELD_MONTH_CUMULATIVE, 2, '.', ',')}}</td> 
                    <td colspan="1" style="text-align: right">{{number_format((float)$field->YIELD_CUMULATIVE_CUMULATIVE, 2, '.', ',')}}</td> 
                    
                </tr>
                @php 
                $old = $field->POLICY_ID;
                
                $last_NAV_B_UOBAM = $field->NAV_B_UOBAM;
                @endphp
                @endforeach     


                <!-- -->
                
                @php
                    $sumEquityK = 0;
                    $sumEquityU = 0;
                    $sumBondK = 0;
                    $sumBondU = 0;
                    $sumCumulativeEquity = 0;
                    $sumCumulativeBond = 0;

                    $A1 = 0;
                    $B1 = 0;
                    $C1 = 0;
                    $G1 = 0;
                    $H1 = 0;


                    $L1 = 0;
                    $L2 = 0;
                    $L3 = 0;

                    $M1 = 0;
                    $M2 = 0;
                    $M3 = 0;
            
                    $I1 = 0;
                    $I2 = 0;
                    $I3 = 0; 

                    foreach($equitytop as $index =>$field) {
                        $sumEquityU = (float)$field->NAV_B_UOBAM;
                        $sumEquityK = (float)$field->NAV_B_KTAM;
                        $sumCumulativeEquity = (float)$field->NAV_B_CUMULATIVE;
                        $G1 = $sumCumulativeEquity; 

                        $L1 = (float)$field->YIELD_CUMULATIVE_UOBAM;
                        $L2 = (float)$field->YIELD_CUMULATIVE_KTAM;
                        $L3 = (float)$field->YIELD_CUMULATIVE_CUMULATIVE;
                        break;
                    }

                    foreach($bondtop as $index =>$field) {
                        $sumBondU += ((float)$sumEquityU + (float)$field->NAV_B_UOBAM);
                        $sumBondK += ((float)$sumEquityK + (float)$field->NAV_B_KTAM);
                        $sumCumulativeBond = (float)$field->NAV_B_CUMULATIVE;

                        $A1 = $sumBondU;
                        $B1 = $sumBondK;
                        $C1 = ($sumBondU + $sumBondK);
                        $H1 = $sumCumulativeBond; 

                        $M1 = (float)$field->YIELD_CUMULATIVE_UOBAM;
                        $M2 = (float)$field->YIELD_CUMULATIVE_KTAM;
                        $M3 = (float)$field->YIELD_CUMULATIVE_CUMULATIVE;

                        break;
                    }
                @endphp

                <tr>    
                    <td colspan="1" style="text-align: left"></td>
                    <td colspan="1" style="text-align: center">รวม: </td>
                    <td colspan="1" style="text-align: right">{{number_format((float)$A1, 2, '.', ',')}}</td>
                    <td colspan="1" style="text-align: right"> </td>
                    <td colspan="1" style="text-align: right"> </td>
                    <td colspan="1" style="text-align: right"> </td> 
                    <td colspan="1" style="text-align: right"> </td> 
                    <td colspan="1" style="text-align: right"> </td> 
                    <td colspan="1" style="text-align: right"> </td> 

                    <td colspan="1" style="text-align: right">{{number_format((float)$B1, 2, '.', ',')}}</td>
                    <td colspan="1" style="text-align: right"> </td>
                    <td colspan="1" style="text-align: right"> </td>
                    <td colspan="1" style="text-align: right"> </td> 
                    <td colspan="1" style="text-align: right"> </td> 
                    
                    <td colspan="1" style="text-align: right">{{number_format((float)$C1, 2, '.', ',')}}</td>
                    <td colspan="1" style="text-align: right"> </td>
                    <td colspan="1" style="text-align: right"> </td>
                    <td colspan="1" style="text-align: right"> </td> 
                    <td colspan="1" style="text-align: right"> </td>   
                </tr>

                
            @endif    
            </tbody>
        </table>      
    </div>
 
<div class="row">
   <div id="mini_table2"  class ="col-sm-2 col-md-4 col-lg-4">
    <table class="table fixed table-bordered"  width="150px" id="table_summary" cellspacing="0.5">
    <thead>
            <tr>
                    <th colspan="1" style="text-align: left; " nowrap>สัดส่วนการลงทุน ณ.วันที่</th>
                    <th colspan="1" style="text-align: right">{{date("d-m-Y")}}</th>
                </tr>
                <tr>
                    <th colspan="1" style="text-align: left">นโยบายตราสารหนี้ %</th>
                    @if($G1 > 0) 
                    <th colspan="1" style="text-align: right">{{number_format((float)(($G1 / $C1) * 100), 2, '.', ',')}}</th>
                    @else
                    <th colspan="1" style="text-align: right">0.00</td>
                    @endif
                </tr>
            
                <tr>
                    <th colspan="1" style="text-align: left">นโยบายตราสารทุน %</th>
                    @if($H1 > 0) 
                    <th colspan="1" style="text-align: right">{{number_format((float)(($H1 / $C1) * 100), 2, '.', ',')}}</th>
                    @else
                    <th colspan="1" style="text-align: right">0.00</th>
                    @endif
                </tr>
                
        </thead>        
    </table>
   </div>
</div> 

<div class="row">
    <div id="mini_table3" class ="col-sm-2 col-md-4 col-lg-4" >
        <table class="table fixed table-bordered"  width="150px" id="table_summary2" cellspacing="0.5">
                <thead>
                    
                    <tr>
                            <th colspan="1" style="text-align: left; " nowrap>รายงานผลตอบแทนแยกตามสัดส่วนการลงทุน</th>
                            <th colspan="1" style="text-align: right">UOBAM</th>
                            <th colspan="1" style="text-align: right">KTAM</th>
                            <th colspan="1" style="text-align: right">สะสม</th>
                    </tr>
                </thead>  

                @php
                $I1 = ($L1*0.90) + ($M1*0.10);
                $I2 = ($L1*0.85) + ($M1*0.15);
                $I3 = ($L1*0.80) + ($M1*0.20);
                $I4 = ($L1*0.70) + ($M1*0.30);
                $I5 = ($L1*0.60) + ($M1*0.40);
                $I6 = ($L1*0.50) + ($M1*0.50);

                $J1 = ($L2*0.90) + ($M2*0.10);
                $J2 = ($L2*0.85) + ($M2*0.15);
                $J3 = ($L2*0.80) + ($M2*0.20);
                $J4 = ($L2*0.70) + ($M2*0.30);
                $J5 = ($L2*0.60) + ($M2*0.40);
                $J6 = ($L2*0.50) + ($M2*0.50);

                $K1 = ($L3*0.90) + ($M3*0.10);
                $K2 = ($L3*0.85) + ($M3*0.15);
                $K3 = ($L3*0.80) + ($M3*0.20);
                $K4 = ($L3*0.70) + ($M3*0.30);
                $K5 = ($L3*0.60) + ($M3*0.40);
                $K6 = ($L3*0.50) + ($M3*0.50);
                @endphp

                <tbody> 
                    <tr>
                        <td colspan="1" style="text-align: left">1. ตราสารหนี้ 90% ตราสารทุน 10% L1={{$L1}}, M1={{$M1}}</td>
                        <td colspan="1" style="text-align: right">{{number_format((float)$I1, 2, '.', ',')}}</td>
                        <td colspan="1" style="text-align: right">{{number_format((float)$J1, 2, '.', ',')}}</td>
                        <td colspan="1" style="text-align: right">{{number_format((float)$K1, 2, '.', ',')}}</td>
                    </tr>   
                    <tr>
                        <td colspan="1" style="text-align: left">2. ตราสารหนี้ 85% ตราสารทุน 15%</td>
                        <td colspan="1" style="text-align: right">{{number_format((float)$I2, 2, '.', ',')}}</td>
                        <td colspan="1" style="text-align: right">{{number_format((float)$J2, 2, '.', ',')}}</td>
                        <td colspan="1" style="text-align: right">{{number_format((float)$K2, 2, '.', ',')}}</td>
                    </tr>   
                    <tr>
                        <td colspan="1" style="text-align: left">3. ตราสารหนี้ 80% ตราสารทุน 20%</td>
                        <td colspan="1" style="text-align: right">{{number_format((float)$I3, 2, '.', ',')}}</td>
                        <td colspan="1" style="text-align: right">{{number_format((float)$J3, 2, '.', ',')}}</td>
                        <td colspan="1" style="text-align: right">{{number_format((float)$K3, 2, '.', ',')}}</td>
                    </tr>   
                    <tr>
                        <td colspan="1" style="text-align: left">4. ตราสารหนี้ 70% ตราสารทุน 30%</td>
                        <td colspan="1" style="text-align: right">{{number_format((float)$I4, 2, '.', ',')}}</td>
                        <td colspan="1" style="text-align: right">{{number_format((float)$J4, 2, '.', ',')}}</td>
                        <td colspan="1" style="text-align: right">{{number_format((float)$K4, 2, '.', ',')}}</td>
                    </tr>   
                    <tr>
                        <td colspan="1" style="text-align: left">5. ตราสารหนี้ 60% ตราสารทุน 40%</td>
                        <td colspan="1" style="text-align: right">{{number_format((float)$I5, 2, '.', ',')}}</td>
                        <td colspan="1" style="text-align: right">{{number_format((float)$J5, 2, '.', ',')}}</td>
                        <td colspan="1" style="text-align: right">{{number_format((float)$K5, 2, '.', ',')}}</td>
                       
                    </tr>  
                    <tr>
                        <td colspan="1" style="text-align: left">6. ตราสารหนี้ 50% ตราสารทุน 50%</td>
                        <td colspan="1" style="text-align: right">{{number_format((float)$I6, 2, '.', ',')}}</td>
                        <td colspan="1" style="text-align: right">{{number_format((float)$J6, 2, '.', ',')}}</td>
                        <td colspan="1" style="text-align: right">{{number_format((float)$K6, 2, '.', ',')}}</td>
                    </tr>    
                </tbody>       
        </table>
    </div>
</div>