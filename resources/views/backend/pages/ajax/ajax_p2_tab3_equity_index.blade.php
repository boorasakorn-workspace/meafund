<!-- FILE: ajax_p2_tab3_equity_index.blade.php -->
@if(count($data) > 0) 
<table class="table  table-bordered"  >

    <thead>

    <tr>
        <th style="width:5%;text-align: center" id="index_th">
            <input type="checkbox" id="mainCheck" />
        </th>

        <th style="width:8%;text-align: center;">Action</th>
        <th style="width:10%;text-align: center;">ชื่อย่อ</th>
        <th style="text-align: center;" data-class="expand">ชื่อบริษ้ทิ</th>
        <th style="width:8%;text-align: center;">ตลาด</th>
        <th style="width:8%;text-align: center;">SET Index</th>
        <th style="text-align: center;" data-class="expand">กลุ่มอุตสาหกรรม</th>
        <th style="text-align: center;"data-class="expand">หมวดธุรกิจ</th>
        <!--th style="width:5%;">บลจ.</th>
        <th style="width:12%;">วันที่เริ่มต้น</th>
        <th style="width:12%;">วันที่สิ้นสุด</th-->
    
    </tr>
    </thead>
   
    <tbody>
    
        @foreach($data as $item)
            <tr>
                
                <td style="text-align: center">
                  <input type="checkbox"  name="check_item_edit[]" value="{{$item->INDEX_ID}}" class="item_checked" id="item_check_{{$item->INDEX_ID}}" />
                </td>

                <td style="text-align: center" nowrap>
                    <a href="/admin/EquityCompany/editEquityIndex/{{$item->INDEX_ID}}" class="btn btn-primary btn-xs">
                        <i class="fa fa-gear"></i>
                    </a>
                    <a href="javascript:void(0);" data-id="{{$item->INDEX_ID}}" class="mea_delete_by btn bg-color-red txt-color-white btn-xs">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                    
                </td>
                <td style="text-align: center">{{$item->SYMBOL}}</td>  

                <td>{{$item->COMP_NAME}}</td>
                <td style="text-align: center">{{$item->MARKET}}</td>
                
                <td style="text-align: center">
                @if(($item->SET_INDEX50 ==null) && ($item->SET_INDEX100==null))
                    - <!-- แสดง - -->
                @elseif ((strtoupper($item->SET_INDEX50) =='Y') && ($item->SET_INDEX100==null))
                    SET50  <!-- แสดง SET50 -->
                @elseif (($item->SET_INDEX50 == null) && (strtoupper($item->SET_INDEX100)=='Y'))
                    SET100  <!-- แสดง SET100 -->
                @elseif ((strtoupper($item->SET_INDEX50) == 'Y') && (strtoupper($item->SET_INDEX100)=='Y'))
                    SET50  <!-- แสดง SET50 -->
                @endif
                </td>

                <td>{{$item->INDUSTRIAL }}</td>
                <td>{{$item->BU}}</td>
                <!--td>{{$item->COMP_NAME}}</td>
                <td>{{$item->COMP_NAME}}</td>
                <td>{{$item->COMP_NAME}}</td-->
            </tr>
      @endforeach

     
    </tbody>
    

    <tfoot>
        <tr>
            <td colspan="11">
                {!! $htmlPaginate !!}
            </td>
        </tr>
    </tfoot>
</table>
 @else
    <div class="list" style="margin-bottom: 10px; padding: 10px">
        <p style="font-size: 20px;padding: 10px;text-align: center;width: 100%;background-color: #f1f1f1;border: 1px solid #E1E8F3">ไม่พบข้อมูลในระบบ</p>
    </div>

    @endif






