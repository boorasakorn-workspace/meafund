<!--
SECURITY    NAME  Maturity Date   TY RATE FACE VALUE/UNIT TOTAL COST      Red Yield   AMORTIZED COST  DISC/PREM   ACCRUED INT  DAY INT ENT(EX)  MKT PRICE   DIRTY VALUE %NAV          ALLOWANCE
-->

<div id="trading_table" style="display:block;">
<div>Showing {{$PageNumber}} to {{ (($PageSize * $PageNumber) > $totals? $totals:($PageSize * $PageNumber))  }} of {{ $totals }}</div>
<table class="table table-bordered">
    <thead>
    <tr>
        <th colspan="16" style="text-align: center">
            <p class="report-title">{{$TableTitle}} </p> <!-- รายงานความเคลื่อนไหวตราสารทุน -->
        </th>
    </tr>
    
    <tr>
        <th colspan="12" style="text-align: center">{!! $pretty_date !!}</th>
    </tr>

    <tr>
        <th colspan="1" class="showcol" style="text-align: center"> No. </th>
        <th colspan="1" class="showcol" style="text-align: center"> ประเภททรัพย์สิน </th>
        <th colspan="1" class="showcol" style="text-align: center"> ประเภทตราสารหนี้ </th>  
        <th colspan="1" class="showcol" style="text-align: center"> ชื่อย่อตราสารหนี้ </th>  
        <th colspan="1" class="showcol" style="text-align: center"> ชื่อเต็มตราสารหนี้ </th>  
        <th colspan="1" class="showcol" style="text-align: center"> จำนวนหุ้น / มูลค่าที่ตราไว้ </th>  
        <th colspan="1" class="showcol" style="text-align: center"> ราคาทุน </th>  
        <th colspan="1" class="showcol" style="text-align: center"> ดอกเบี้ยค้างรับ </th>  
        <th colspan="1" class="showcol" style="text-align: center"> ราคายุติธรรม </th>     
        <th colspan="1" class="showcol" style="text-align: center"> อัตราส่วนของราคายุติธรรม </th>  
        <th colspan="1" class="showcol" style="text-align: center"> อัตราดอกเบี้ย </th>   
        <th colspan="1" class="showcol" style="text-align: center"> วันครบกำหนด </th>
    </tr>

    </thead>
        <tbody>
            <tr>      
                <td style="text-align: right"  nowrap>1</td>        <!-- No. -->
                <td style="text-align: center" nowrap>ประเภททรัพย์สิน</td>        <!-- ประเภททรัพย์สิน -->
                <td style="text-align: left"   nowrap>ประเภทตราสารหนี้</td>          <!-- ประเภทตราสารหนี้ -->
                <td style="text-align: left"   nowrap>2.51</td>           <!-- ชื่อย่อตราสารหนี้ -->
                <td style="text-align: left"   nowrap>10000000</td>       <!-- ชื่อเต็มตราสารหนี้  -->
                <td style="text-align: right" >9693886.7</td>      <!-- จำนวนหุ้น / มูลค่าที่ตราไว้  -->
                <td style="text-align: right" >3</td>              <!-- ราคาทุน  -->
                <td style="text-align: right" >3</td>              <!-- ดอกเบี้ยค้างรับ  -->
                <td style="text-align: right" >3</td>              <!-- ราคายุติธรรม  -->

                <td style="text-align: right" >{{number_format((float)9705657.8, 2, '.', ',')}}</td> <!-- อัตราส่วนของราคายุติธรรม -->
                <td style="text-align: right" >{{number_format((float)11771.1,4, '.', ',')}}</td>    <!-- อัตราดอกเบี้ย -->
                <td style="text-align: right" nowrap>2017-05-30</td>    <!-- วันครบกำหนด -->
            </tr>   

            <tr>      
                <td style="text-align: right"  nowrap>2</td>        <!-- No. -->
                <td style="text-align: center" nowrap>ประเภททรัพย์สิน</td>        <!-- ประเภททรัพย์สิน -->
                <td style="text-align: left"   nowrap>ประเภทตราสารหนี้</td>          <!-- ประเภทตราสารหนี้ -->
                <td style="text-align: left"   nowrap>2.51</td>           <!-- ชื่อย่อตราสารหนี้ -->
                <td style="text-align: left"   nowrap>10000000</td>       <!-- ชื่อเต็มตราสารหนี้  -->
                <td style="text-align: right" >9693886.7</td>      <!-- จำนวนหุ้น / มูลค่าที่ตราไว้  -->
                <td style="text-align: right" >4</td>              <!-- ราคาทุน  -->
                <td style="text-align: right" >4</td>              <!-- ดอกเบี้ยค้างรับ  -->
                <td style="text-align: right" >5</td>              <!-- ราคายุติธรรม  -->

                <td style="text-align: right" >{{number_format((float)9705657.8, 2, '.', ',')}}</td> <!-- อัตราส่วนของราคายุติธรรม -->
                <td style="text-align: right" >{{number_format((float)11771.1,4, '.', ',')}}</td>    <!-- อัตราดอกเบี้ย -->
                <td style="text-align: right" nowrap>2017-05-31</td>    <!-- วันครบกำหนด -->
            </tr>   
		</tbody>
    <tfoot>

        <tr>
            <td colspan="16">
                {!! $htmlPaginate !!}
            </td>
        </tr>
    </tfoot>
</table>
</div>




<!-- Chart -->
<!--div id="bar-chart-good-bad" class="chart" style="display:block;">
    
</div-->

