 @if($data)
 
 <div id="mini_table1" style="{{$table1show}}">
    <table class="table table-bordered"  width="400px" id="table_bond_nav" cellspacing="0">
        <thead>
            <tr>
                <th colspan="8" style="text-align: center">นโยบายการลงทุนตราสารหนี้</th>
            </tr>
            <tr>
                <th colspan="8" style="text-align: center"> มูลค่าทรัพย์สินสุทธิและอัตราผลตอบแทน</th>
            </tr>
            <tr>
                <th colspan="1" rowspan="2" style="text-align: center">เดือน</th>
                <th colspan="2" style="text-align: center">มูลค่าทรัพย์สินสุทธิ</th>
                <th colspan="2" style="text-align: center">อัตราผลตอบแทน (%)</th>
                <th colspan="3" rowspan="2" style="text-align: center"> กราฟ </th> 
            </tr>
            <tr>
                <th colspan="1" style="text-align: center">ล้านบาท</th>
                <th colspan="1" style="text-align: center">ต่อหน่วย</th>
                <th colspan="1" style="text-align: center">รายเดือน</th>
                <th colspan="1" style="text-align: center">สะสม</th> 
                
            </tr>
        </thead>

        <tbody>
           @foreach($data['bond_nav'] as $rowset =>$rs)
           {{--/admin/EquityNavReport/bonddetails/{{$rs->yyyy - 543}}-{{$rs->item}}--}}
            <tr>       
                <!--td colspan="1" width="8%" style="text-align: center"><a href="javascript:void(0);" data-id="{{$rs->yyyy-543}}-{{$rs->item}}" class="bond_details_by btn bg-color-green txt-color-white btn-xs"> {{ toThaiShortMonth($rs->item)}} {{$rs->yyyy}}</a></td-->
                @if (($rs->MB <= 0) && ($rs->NU <= 0) && ($rs->YM <= 0) &&  ($rs->YC <= 0))
                   <td colspan="1" width="8%" style="text-align: center">{{toThaiShortMonth($rs->item)}} {{$rs->yyyy}}</td>  
                @else
                    <td colspan="1" width="8%" style="text-align: center"><a href="javascript:void(0);" data-id="{{$rs->yyyy - 543}}-{{$rs->item}}" class="bond_details_by btn bg-color-green txt-color-white btn-xs">{{toThaiShortMonth($rs->item)}} {{$rs->yyyy}}</a></td>  
                @endif  

                @if ($rs->MB < 0) 
                   <td colspan="1" width="10%" style="text-align: right"><font color="#ff0000">{{number_format((float)$rs->MB, 2, '.', '')}}</font></td>
                @else  
                   <td colspan="1" width="10%" style="text-align: right">{{number_format((float)$rs->MB, 2)}}</td> 
                @endif
                
                @if ($rs->NU < 0)          
                   <td colspan="1" width="10%" style="text-align: right"><font color="#ff0000">{{number_format((float)$rs->NU, 4, '.', '')}}</font></td>  
                @else
                   <td colspan="1" width="10%" style="text-align: right">{{number_format((float)$rs->NU, 4)}}</td>  
                @endif

                @if ($rs->YM < 0)  
                   <td colspan="1" width="8%" style="text-align:  right"><font color="#ff0000">{{number_format((float)$rs->YM, 2,  '.', '')}}</font></td> 
                @else
                   <td colspan="1" width="8%" style="text-align:  right">{{number_format((float)$rs->YM, 2, '.', '')}} {{--round($rs->YM, 2)--}}</td> 
                @endif
                
                @if ($rs->YC < 0)  
                   <td colspan="1" width="8%" style="text-align:  right"><font color="#ff0000">{{number_format((float)$rs->YC, 2,  '.', '')}}</font></td>  
                @else
                   <td colspan="1" width="8%" style="text-align:  right">{{number_format((float)$rs->YC, 2)}} {{--round($rs->YC, 2)--}}</td>     
                @endif
                

                @if((($rs->item)==1) && !($selected_month))
                <td colspan="3" rowspan="12" style="text-align: center">
                    <div id="main_chart1" class="main_chart1" style="width: 100%; padding: 10px;"> 
                    </div>
                </td> 
                @elseif ((($rs->item)!=1) && ($selected_month)) 
                <td colspan="3" rowspan="12" style="text-align: center">
                    <div id="main_chart1" class="main_chart1" style="width: 100%; padding: 10px;"> 
                    </div>
                </td> 
                 @elseif ((($rs->item)==1) && ($selected_month)) 
                <td colspan="3" rowspan="12" style="text-align: center">
                    <div id="main_chart1" class="main_chart1" style="width: 100%; padding: 10px;"> 
                    </div>
                </td> 

                @endif

                
            </tr>
             @endforeach
            
        </tbody>
    </table>
                          
</div>
 
                                        
<div id="mini_table2" style="{{$table2show}}">
    <table class="table table-bordered"  width="400px" id="table_bond_nav" cellspacing="0">
        <thead>
            <tr>
                <th colspan="8" style="text-align: center">นโยบายการลงทุนตราสารทุน</th>
            </tr>
            <tr>
                <th colspan="8" style="text-align: center"> มูลค่าทรัพย์สินสุทธิและอัตราผลตอบแทน</th>
            </tr>
            <tr>
                <th colspan="1" rowspan="2" style="text-align: center">เดือน</th>
                <th colspan="2" style="text-align: center">มูลค่าทรัพย์สินสุทธิ</th>
                <th colspan="2" style="text-align: center">อัตราผลตอบแทน (%)</th>
                <th colspan="3" rowspan="2" style="text-align: center"> กราฟ </th> 
            </tr>
            <tr>
                <th colspan="1" style="text-align: center">ล้านบาท</th>
                <th colspan="1" style="text-align: center">ต่อหน่วย</th>
                <th colspan="1" style="text-align: center">รายเดือน</th>
                <th colspan="1" style="text-align: center">สะสม</th> 
                
            </tr>
        </thead>

        <tbody>
            @foreach($data['equity_nav'] as $rowset =>$rs)
            <tr>       
                @if (($rs->MB <= 0) && ($rs->NU <= 0) && ($rs->YM <= 0) &&  ($rs->YC <= 0))
                   <td colspan="1" width="8%" style="text-align: center">{{toThaiShortMonth($rs->item)}} {{$rs->yyyy}}</td>  
                @else
                    <td colspan="1" width="8%" style="text-align: center"><a href="javascript:void(0);" data-id="{{$rs->yyyy - 543}}-{{$rs->item}}" class="equity_details_by btn bg-color-green txt-color-white btn-xs">{{toThaiShortMonth($rs->item)}} {{$rs->yyyy}}</a></td>  
              
                @endif

                @if ($rs->MB < 0) 
                    <td colspan="1" width="10%" style="text-align: right"><font color="#ff0000">{{number_format((float)$rs->MB, 2, '.', '')}}</font></td> 
                @else     
                    <td colspan="1" width="10%" style="text-align: right">{{number_format((float)$rs->MB, 2)}}</td>      
                @endif

                @if ($rs->NU < 0) 
                    <td colspan="1" width="10%" style="text-align: right"><font color="#ff0000">{{number_format((float)$rs->NU, 4,  '.', '')}}</font></td>
                @else
                    <td colspan="1" width="10%" style="text-align: right">{{number_format((float)$rs->NU, 4, '.', '')}}</td> 
                @endif
                
                @if ($rs->YM < 0) 
                    <td colspan="1" width="8%" style="text-align:  right"><font color="#ff0000">{{number_format($rs->YM, 2,  '.', '')}}</font></td> 
                @else
                    <td colspan="1" width="8%" style="text-align:  right">{{number_format((float)$rs->YM, 2, '.', '')}}</td> 
                @endif

                @if ($rs->YC < 0)  
                    <td colspan="1" width="8%" style="text-align:  right"><font color="#ff0000">{{number_format($rs->YC, 2,  '.', '')}}</font></td>  
                @else
                    <td colspan="1" width="8%" style="text-align:  right">{{number_format((float)$rs->YC, 2)}}</td>  
                @endif

                

                @if((($rs->item)==1) && !($selected_month))
                <td colspan="3" rowspan="12" style="text-align: center">
                    <div id="main_chart2" class="main_chart2" style="width: 100%; padding: 10px;"> 
                    </div>
                </td> 
                @elseif ((($rs->item)!=1) && ($selected_month)) 
                <td colspan="3" rowspan="12" style="text-align: center">
                    <div id="main_chart2" class="main_chart2" style="width: 100%; padding: 10px;"> 
                    </div>
                </td> 
                @elseif ((($rs->item)==1) && ($selected_month)) 
                <td colspan="3" rowspan="12" style="text-align: center">
                    <div id="main_chart2" class="main_chart2" style="width: 100%; padding: 10px;"> 
                    </div>
                </td> 
                @endif
            </tr>
            @endforeach

        </tbody>
    </table>
    
</div>

 @endif
 
