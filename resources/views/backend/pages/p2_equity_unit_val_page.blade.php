@extends('backend.layouts.default')
@section('content')
<?php
/**
 * Menu items
 */
$data = getmemulist();
/**
 * Menu Sidebar
 */
$arrSidebar =getSideBar($data);
?>

<!-- MAIN CONTENT -->
<div id="content">

    

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">  
                        &nbsp;&nbsp;&nbsp; เป็นเมนูสำหรับนําเข้าข้อมูล มูลค่า หน่วยลงทุน และอัตราผลตอบแทนเข้าสู่ระบบกองทุน ฯ                  
                    {{--getMenutitle($arrSidebar)--}} 
                </h1>
                
        </div>
    </div>

    <!-- BEGINE: search from -->
    <div id="search_form" name="search_form" style="width: 100%; padding: 0px;">
                                
    </div>
    <!-- END: search form -->

    <div class="row">
        
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">  
                                  
                 
            </h1>
            
        </div>
         
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            <ul id="sparks" class="">
                <li class="sparks-success">
                    <a href="#" id="export_search" class="btn bg-color-green txt-color-white">
                    <i class="fa fa-upload"></i> Export</a>

                  
                </li>

                <li class="sparks-info">
                    <a href="{{action('EquityCompanyManagementController@getimportEquityIndex')}}" class="btn bg-color-orange txt-color-white"><i class="fa fa-download"></i> นำเข้า</a>
                </li>

                <li class="sparks-info">
                    <a href="{{action('EquityCompanyManagementController@postAddEquityIndex')}}" class="btn bg-color-green txt-color-white"><i class="fa fa-plus"></i> เพิ่ม</a>
                </li>
                <li class="sparks-info">
                    <a href="{{action('EquityCompanyManagementController@postEditEquityIndex')}}" id="mea_edit"  class="btn bg-color-blueDark txt-color-white"><i class="fa fa-gear fa-lg"></i> แก้ไข</a>
                </li>
                <li class="sparks-info">
                    <!--EquityCompanyManagementController@deleteEquityIndex'-->

                    <a href="javascript:void(0);" id="mea_delete" class="btn bg-color-red txt-color-white"><i class="glyphicon glyphicon-trash"></i> ลบ</a>
                </li>

            </ul>
        </div>
    </div>

    


    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false"
                     data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false">
                    <!-- widget options:
						usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

						data-widget-colorbutton="false"
						data-widget-editbutton="false"
						data-widget-togglebutton="false"
						data-widget-deletebutton="false"
						data-widget-fullscreenbutton="false"
						data-widget-custombutton="false"
						data-widget-collapsed="true"
						data-widget-sortable="false"

						-->
                     {!! csrf_field() !!}    
                    <header>


                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                                
                        </div>
                        <!-- end widget edit box -->
                        

                        <!-- widget content -->
                        <div class="widget-body no-padding">

                            <div class="table-responsive">
                                <div class="result" style="width: 100%; padding: 10px;">
                                
                                </div>
                            </div>

                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- WIDGET END -->

        </div>

        <!-- end row -->

        <!-- end row -->

    </section>
    <!-- end widget grid -->


</div>
<!-- END MAIN CONTENT -->


<!-- PAGE RELATED PLUGIN(S) -->


<script src="{{asset('backend/js/plugin/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/datatables/dataTables.colVis.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/datatables/dataTables.tableTools.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>

<!-- Auto Complete -->
<script src="{{asset('backend/js/plugin/jquery-autocomplete/scripts/jquery.mockjax.js')}}"></script> 
<script src="{{asset('backend/js/plugin/jquery-autocomplete/src/jquery.autocomplete.js')}}"></script> 


<script type="text/javascript">
   
    var symbols =  { 
            @foreach($symbols as $item)
                '{{$item->SYMBOL}}' : '{{$item->SYMBOL}} | {{$item->COMP_NAME}}', 
            @endforeach
    };

    function RenderForm(data) {
        // display search form
        $("#search_form").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
        $("#search_form").html(data.html);
        $("#search_form").fadeIn('300');

        // hanlde button search clicked
        $('#btn_search').on('click',function() {
            //var PageSizeAll = $('#page-size-search').val();

            var page_size = 20;
            var page = 1;
         
             // see: ajax_p2_tab3_index_form.blade.php
            var symbol  = $("#autocomplete-dynamic").val(); //symbol
            var market  = $("#market").val(); // market
            var cate_id = $("#industrial").val();  // cate_id 

           // alert(market);
            var parameters = {pagesize : page_size, PageNumber:page, symbol:symbol, market:market, cate_id:cate_id};


            console.log(parameters);
           
            var method = "getallEquityIndex";
            MeaAjax(parameters, method, Render);

            //MeaAjax(jsondata,"EquityStockUniverseReport/search",RenderSearch);

            return false;

        });
    }   

    function Render(data) {
        $(".result").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
        $(".result").html(data.html);
        $(".result").fadeIn('300');

        // Initialize autocomplete with custom appendTo:
        var symbolsArray = $.map(symbols, function (value, key) { return { value: value, data: key }; });
        $('#autocomplete-dynamic').autocomplete({
            lookup: symbolsArray
        });

        //add current Pageing
        //$('html').append('<input type="hidden" value="1" id="currentpage_search" />');
        //$('html').append('<input type="hidden" value="0" id="hd_NEWS_TOPIC_FLAG" />');
        // var NEWS_CATE_ID = $("#news_topice_select").val();
        // var NEWS_TOPIC_FLAG = 0;
       

        $("#mainCheck").on("click",function(){$(".item_checked").not(this).prop('checked', this.checked);});



        $(".mea_delete_by").on('click',function(){

            var id = $(this).attr("data-id");
            $.SmartMessageBox({
                title : "คำเตือน",
                content : "ท่านแน่ใจที่ต้องการจะลบ รายการที่ท่านเลือก",
                buttons : '[ยกเลิก][OK]'

            }, function(ButtonPressed) {
                if (ButtonPressed === "OK") {

                    //var plan_id = $("#plan_id").val();
                    //var jsondata = {group_id : id, plan_id : plan_id};
                    
                    var jsondata = {group_id : id};
 
                    $.ajax({

                        type: 'post', 
                        dataType: 'json',
                        url: '/admin/EquityCompany/deleteEquityIndex',
                        data: jsondata,

                        success: function(data) {

                            if(data.ret == "1"){
                                $.smallBox({
                                    title: "Congratulations! Your form was submitted",
                                    content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                                    color: "#5F895F",
                                    iconSmall: "fa fa-check bounce animated",
                                    timeout: 4000
                                });

                                window.location.href = '/admin/EquityCompany/equityindex';
                            }

                        },

                        error: function(xhr, textStatus, thrownError) {
//                                alert(xhr.status);
//                                alert(thrownError);
//                                alert(textStatus);
                        }
                    });
                }
                if (ButtonPressed === "ยกเลิก") {

                }

            });

        });

        $("#mea_delete").on('click',function(){

            var checkcount = $(".item_checked:checked").length;

            if(checkcount > 0){
                var checked = "";
                $(".item_checked").each(function(){

                    if($(this).is(":checked")){
                        checked = checked + $(this).val() + ",";
                    }

                });
                var jsondata = {group_id : checked};

                $.ajax({

                    type: 'post', 
                    dataType: 'json',
                    url: '/admin/EquityCompany/deleteEquityIndex',
                    data: jsondata,

                    success: function(data) {

                        if(data.ret == "1"){
                            $.smallBox({
                                title: "Congratulations! Your form was submitted",
                                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                                color: "#5F895F",
                                iconSmall: "fa fa-check bounce animated",
                                timeout: 4000
                            });

                            window.location.href = '/admin/EquityCompany/equityindex';
                        }



                    },
                    error: function(xhr, textStatus, thrownError) {
//                                alert(xhr.status);
//                                alert(thrownError);
//                                alert(textStatus);
                    }
                });
            }else {
                $.SmartMessageBox({
                    title : "Error!",
                    content : "ท่านยังไม่ได้เลือกรายการ",
                    buttons : '[OK]'
                }, function(ButtonPressed) {
                    if (ButtonPressed === "OK") {


                    }
                    if (ButtonPressed === "No") {

                    }

                });
            }


        });

        $("#mea_edit").on('click',function(){

            var checkcount = $(".item_checked:checked").length;

            if(checkcount > 1 || checkcount  == 0){
                $.SmartMessageBox({
                    title : "Error!",
                    content : "ไม่สามารถแก้ไขได้ กรุณาเลือกรายการเพียงรายการเดียว",
                    buttons : '[OK]'
                }, function(ButtonPressed) {
                    if (ButtonPressed === "OK") {

                         return false;
                    }
                    if (ButtonPressed === "No") {

                    }

                });
            }else {
                window.location.href = "/admin/EquityCompany/editEquityIndex/" + $(".item_checked:checked").val();
            }
        })

        //page click
        $("#page_click_search li a").on('click',PageRender);

/*
        // Initialize autocomplete with custom appendTo:
        var symbolsArray = $.map(symbols, function (value, key) { return { value: value, data: key }; });
        $('#autocomplete-dynamic').autocomplete({
            lookup: symbolsArray
        });
*/
    }// Render
  
  //function Pageclick
    function PageRender(){
        var p = $(this).attr('data-page');
        var page_size = 20;
        var CurPage = $('#currentpage_search').val();
        $("#all_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');

        if(p == "pre"){
            p = parseInt(CurPage) - 1;
        }

        if(p == "next"){
            p = parseInt(CurPage) + 1;
        }
        //$('#currentpage_all').val(p);
        $('#currentpage_search').val(p);

        // see: ajax_p2_tab3_index_form.blade.php
        var symbol  = $("#autocomplete-dynamic").val(); //symbol
        var market  = $("#market").val(); //market
        var cate_id = $("#industrial").val();  


        var jsondata = {pagesize : page_size, PageNumber:p, symbol:symbol, market:market, cate_id:cate_id};

         MeaAjax(jsondata,"getallEquityIndex",Render);
    };

  $( function() {
    $.widget( "custom.combobox", {
      _create: function() {
        this.wrapper = $( "<span>" )
          .addClass( "custom-combobox" )
          .insertAfter( this.element );
 
        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
      },
 
      _createAutocomplete: function() {
        var selected = this.element.children( ":selected" ),
          value = selected.val() ? selected.text() : "";
 
        this.input = $( "<input>" )
          .appendTo( this.wrapper )
          .val( value )
          .attr( "title", "" )
          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
          .autocomplete({
            delay: 0,
            minLength: 0,
            source: $.proxy( this, "_source" )
          })
          .tooltip({
            classes: {
              "ui-tooltip": "ui-state-highlight"
            }
          });
 
        this._on( this.input, {
          autocompleteselect: function( event, ui ) {
            ui.item.option.selected = true;
            this._trigger( "select", event, {
              item: ui.item.option
            });
          },
 
          autocompletechange: "_removeIfInvalid"
        });
      },
 
      _createShowAllButton: function() {
        var input = this.input,
          wasOpen = false;
 
        $( "<a>" )
          .attr( "tabIndex", -1 )
          .attr( "title", "Show All Items" )
          .tooltip()
          .appendTo( this.wrapper )
          .button({
            icons: {
              primary: "ui-icon-triangle-1-s"
            },
            text: false
          })
          .removeClass( "ui-corner-all" )
          .addClass( "custom-combobox-toggle ui-corner-right" )
          .on( "mousedown", function() {
            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
          })
          .on( "click", function() {
            input.trigger( "focus" );
 
            // Close if already visible
            if ( wasOpen ) {
              return;
            }
 
            // Pass empty string as value to search for, displaying all results
            input.autocomplete( "search", "" );
          });
      },
 
      _source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        response( this.element.children( "option" ).map(function() {
          var text = $( this ).text();
          if ( this.value && ( !request.term || matcher.test(text) ) )
            return {
              label: text,
              value: text,
              option: this
            };
        }) );
      },
 
      _removeIfInvalid: function( event, ui ) {
 
        // Selected an item, nothing to do
        if ( ui.item ) {
          return;
        }
 
        // Search for a match (case-insensitive)
        var value = this.input.val(),
          valueLowerCase = value.toLowerCase(),
          valid = false;
        this.element.children( "option" ).each(function() {
          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
            this.selected = valid = true;
            return false;
          }
        });
 
        // Found a match, nothing to do
        if ( valid ) {
          return;
        }
 
        // Remove invalid value
        this.input
          .val( "" )
          .attr( "title", value + " didn't match any item" )
          .tooltip( "open" );
        this.element.val( "" );
        this._delay(function() {
          this.input.tooltip( "close" ).attr( "title", "" );
        }, 2500 );
        this.input.autocomplete( "instance" ).term = "";
      },
 
      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      }
    });
 
    $( "#combobox" ).combobox();
    $( "#toggle" ).on( "click", function() {
      $( "#combobox" ).toggle();
    });
  } );
  

    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    //$.noConflict();
    
    

    $(document).ready(function() {

        $('html').append('<input type="hidden" value="1" id="currentpage_search" />');
       
       // see: ajax_p2_tab3_index_form.blade.php
       


//        var jsondata = {pagesize : page_size, PageNumber:p, symbol:symbol, market:market, cate_id:cate_id};

//pagesize: 20, PageNumber: 1, symbol: "A | บริษัท อารียา พรอพเพอร์ตี้ จำกั…", market: "", cate_id: "10" }  equityindex:1751:13
        var parameters = {pagesize : 20, PageNumber:1 , symbol:'', market:'', cate_id:''};
        var method = "getallEquityIndex";
         MeaAjax(parameters, method, Render);

      //  var parameters = {pagesize : 20, PageNumber:1};
      //  var method = "getallEquityIndex";

       

//        MeaAjax(parameters, method, Render);

        method = "getEquityIndexSearchForm";
        MeaAjax(parameters, method, RenderForm);

        
        $('#export_search').on('click',function() {
            // hide dropdown menu
            $("body").trigger("click");

            var tmp_symbol  = $("#autocomplete-dynamic").val();           // "APCS"
            var market      = $("#market").val(); //symbol
            var cate_id     = $("#industrial").val();  
            var array       = tmp_symbol.split("|"); 
            if(array.length > 0)
               symbol = array[0].trim();
    //   {{action('EquityCompanyManagementController@dowloadsampleEquityIndex')}}
         //EquityCompanyManagementController@ajax_report_search_export
          //  window.location.href =  "EquityCompany/exportsearch?symbol=" + symbol +"&cate_id=" + cate_id +
          //                          "&market=" + market;

            window.location.href =  "exportsearch?symbol=" + symbol +"&cate_id=" + cate_id +
                                    "&market=" + market;

            return false;
        });


    })

</script>

@stop