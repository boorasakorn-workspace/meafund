@extends('backend.layouts.default')
@section('content')
<?php
$data = getmemulist();
$arrSidebar =getSideBar($data);
?>

<!--[if lt IE 9]> 
    <script async src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
<![endif]-->


<!--link rel="stylesheet" href="{{asset('backend/js/plugin/chartist/chartist.min.css')}}"/> 
<link rel="stylesheet" href="{{asset('backend/js/plugin/chartist/ct-customize.css')}}">
<link rel="stylesheet" href="{{asset('backend/js/plugin/chartist/plugin/chartist-plugin-tooltip.css')}}"/-->
<!-- ECHART CSS -->

<!--link href="{{asset('backend/js/plugin/echart/asset/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{asset('backend/js/plugin/echart/asset/css/bootstrap.css')}}" rel="stylesheet"-->
<!--link href="{{asset('backend/js/plugin/echart/asset/css/carousel.css')}}" rel="stylesheet"-->

 

<style type="text/css">
    #graph_label .tickLabel{

        font-size: 100%;
    }
     
    #sort-by-year{
        /*position:absolute;*/
        width: 100%;
        margin-bottom: 10px;
        /*z-index: 999;*/
        /*left: 200px;*/
        /*top:5px*/
    }
    #sort-by-year select{
        width: 300px;

    }
    #datatable_fixed_column tbody tr td a i{
        font-size: 11px !important;
        line-height: 12px!important;
    }
    #datatable_fixed_column tbody tr td a{
        line-height: 12px!important;

    }

    .isa_info, .isa_success, .isa_warning, .isa_error {
        margin: 10px 0px;
        padding:12px;
    }
    
    .isa_info {
        color: #00529B;
        background-color: #BDE5F8;
    }
    .isa_success {
        color: #4F8A10;
        background-color: #DFF2BF;
    }
    .isa_warning {
        color: #9F6000;
        background-color: #FEEFB3;
    }
    .isa_error {
        color: #D8000C;
        background-color: #FFBABA;
    }
    .isa_info i, .isa_success i, .isa_warning i, .isa_error i {
        margin:10px 22px;
        font-size:2em;
        vertical-align:middle;
    }
  
  .prism {
  white-space: pre-wrap;
  line-height: 20px;
  border-left: 4px solid #05A5D1;
  padding: 5px 10px;
  background-color: rgba(5, 165, 209, 0.05);
  overflow: auto;
}

.prism + .prism {
  margin-top: 10px;
}

}
</style>

<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>

                {{getMenutitle($arrSidebar)}}

            </h1>
        </div>

    </div>


    <div class="row" id="widget-grid" >

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <ul class="nav nav-tabs pull-left in">

                        <li class="active">

                            <a data-toggle="tab" href="#hr1"> <i class="fa fa-lg fa-arrow-circle-o-down"></i> <span class="hidden-mobile hidden-tablet"> ระบุเงื่อนไขการค้นหา </span> </a>

                        </li>

                        <!--li>
                            <a data-toggle="tab" href="#hr2"> <i class="fa fa-lg fa-arrow-circle-o-up"></i> <span class="hidden-mobile hidden-tablet"> รายงานสรุปของตราสารทุน  </span> </a>
                        </li-->

                    </ul>
                </header>

                <!-- widget div-->
                <div>


                    <!-- widget content -->
                    <div class="widget-body">

                        <div class="tab-content">
                            <div class="tab-pane active" id="hr1">

                                <div class="widget-body no-padding" style="width: 100% ;margin: 0 auto;background-color: #000000;border: 1px solid #E1E8F3">
                                    <header style="color: #fff; height: 40px; line-height: 40px; font-size: 18px; background-color: #a90329;padding-left: 20px;"> กล่องค้นหา </header>

                                    <form  id="comment-form_darkman" class="smart-form" novalidate="novalidate">
                                        {{--{!! csrf_field() !!} --}}
                                        <fieldset>

                                            <div class="row" style="display:none;">
                                                <section class="col col-4">
                                                    <label class="label"><input type="checkbox" id="check_name"  value="name">   ชื่อหลักทรัพย์</label>
                                                    <label class="input">
                                                        <select name="plan" id="name" class="form-control">
                                                            <option value="">ชื่อหลักทรัพย์</option>
                                                            @if($equitylist)
                                                                @foreach($equitylist as $item)
                                                                    <option value="{{$item->NAME_SHT}}">{{$item->NAME_SHT}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </label>
                                                </section>
                                                <section class="col col-4">
                                                    <label class="label"><input type="checkbox" id="check_depart" value="depart">   หมวดหมู่หลักทรัพย์</label>
                                                    <label class="input">
                                                        
                                                        <select name="depart" id="depart" class="form-control">
                                                            <option value="">หมวดหมู่หลักทรัพย์</option>
                                                            @if($equitycategory)
                                                                @foreach($equitycategory as $item)
                                                                    <option value="{{$item->INDUSTRIAL}}">{{$item->INDUSTRIAL}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </label>
                                                </section>
                                            </div>

                                            <div class="row">
                                                <section class="col col-4">
                                                    <label class="label">     นโยบายการลงทุน</label>
                                                    <label class="input">
                                                        <select name="plan" id="plan" class="form-control">
                                                            <option value="">&nbsp;เลือกนโยบายการลงทุน</option>
                                                            <option value="ตราสารหนี้">&nbsp;ตราสารหนี้  </option>
                                                            <option value="ตราสารทุน"> &nbsp;ตราสารทุน  </option>
                                                        </select>
                                                    </label>
                                                </section>

                                                <section class="col col-2">
                                                    <label class="label">   ระบุช่วงเวลา </label>
                                                    <label class="input">
                                                        <select name="select_month" id="select_month" class="form-control">
                                                            <option value="">&nbsp;เดือน</option>
                                                            @for($i =1; $i<=12; $i++)
                                                                <option value="{{$i}}" >&nbsp;{{getThaiMonth($i)}} </option>   
                                                            @endfor                                          
                                                        </select>
                                                    </label>
                                                </section>

                                                <section class="col col-2">
                                                    <label class="label" ><br/></label>
                                                    <label class="input">
                                                            <select name="select_year" id="select_year"  class="form-control">
                                                                <option value="">&nbsp;ปี</option> 
                                                                @if($years)
                                                                    @foreach($years as $year)
                                                                        <option value="{{$year}}">&nbsp;{{$year + 543}}  </option>
                                                                    @endforeach
                                                                @endif  
                                                            </select>
                                                         
                                                    </label>

                                                </section>

                                                <section class="col col-1">
                                                    <button type="submit" id="btn_search" style="padding: 5px 20px 5px 20px; margin-top:20px;font-size: 18px;" name="submit" class="btn btn-primary">
                                                        ค้นหา
                                                    </button>
                                                </section>
                                             </div>   

                                             <div class="row" >
                                                <section class="col col-4" style="display:none;">
                                                    <label class="label"><input type="checkbox" id="check_date" value="date_start"> ระบุช่วงเวลา</label>
                                                    <label class="input"><i class="icon-append fa fa-calendar"></i>
                                                        <input type="text" class="mea_date_picker" id="date_start"  >
                                                    </label>
                                                </section>
                                                <section class="col col-4" style="display:none;">
                                                    <label class="label">ถึง</label>
                                                    <label class="input"><i class="icon-append fa fa-calendar"></i>
                                                        <input type="text" class="mea_date_picker" id="date_end" >
                                                    </label>
                                                </section>
                                                
                                            </div>

                                        </fieldset>

                                    </form>

                                </div>


                                <!--a href="#" id="export_search" style="margin-top: 30px;" 
                                    class="btn btn-labeled btn-success mea-btn-export"> 
                                    <span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i></span>Export </a-->
                                <div class="button-group bullet">
                                    <a href="#" id="export_search" style="margin-top: 30px;" class="btn btn-labeled btn-success mea-btn-export"> <span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i></span>Export </a>
                                    
                                    <a href="javascript: void(0);" id="previous" 
                                            style="margin-top: 30px; margin-right: 5px; width:110px; display: none;" 
                                            class="btn btn-labeled btn-danger">
                                            <span class="btn-label"><i class="glyphicon glyphicon-chevron-left"></i></span>ย้อนกลับ</a>
                  
                                    <a href="#" id="export_search_details" style="margin-top: 30px; display: none;" 
                                          class="btn btn-labeled btn-success mea-btn-export"> <span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i></span>Export Details</a>

                                </div>        
                                <!--a href="#" id="btn_customize" style="margin-top: 30px;" class="btn btn-labeled btn-warning"> <span class="btn-label"><i class="glyphicon glyphicon-list"></i></span>Customize </a>
                                <a href="#" id="btn_chart"     style="margin-top: 30px;" class="btn btn-labeled btn-info"> <span class="btn-label"><i class="glyphicon glyphicon-signal"></i></span>Chart </a-->
                               

                                <!-- Widget ID (each widget will need unique ID)-->
                                
                                <div class="jarviswidget jarviswidget-color-blueDark" style="margin-top: 5px; display:block;" 
                                     id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false" 
                                     data-widget-colorbutton="true" data-widget-fullscreenbutton="false" 
                                     data-widget-custombutton="true"
                                     data-widget-togglebutton="false" data-widget-sortable="true">
                                    <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                    data-widget-colorbutton="false"
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true"export_search
                                    data-widget-sortable="false"

                                    -->
                                    
                                    <!--select class="form-control mea-pagesize" id="page-size-search" style="display:none;">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select-->
                                         
                                    <header>
                                         
                                         <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                          
                                    </header>

                                    <!-- widget div-->
                                    <div >
                                          
                                        <!-- widget edit box -->
                                        <div class="jarviswidget-editbox">
                                            <!-- This area used as dropdown edit box -->

                                        </div>
                                        <!-- end widget edit box -->

                                        <!-- widget content -->
                                        <div class="widget-body">
                                               
                                            <p class="report-title">
                                                
                                            </p>
                                            

                                            <!--p class="report-period"> </p-->
                                            <div class="table-responsive" >
                                                <div id="serch_data">

                                                </div>
                                            </div>
                                            
                                            
                                            <!-- ?????  -->
                                            <!--div class="table-responsive">
                                                <div name="pie-chart" id="pie-chart" class="chart" style="width: 100%; padding: 10px;">

                                                </div>
                                            </div-->
                                                
                                            
                                            
                                            <!--div class="table-responsive">
                                                         
                                                <div id="main_chart2" class="mea_chart" style="width: 100%; padding: 10px;">
                                                                
                                                </div>
                                                 
                                            </div-->

                                            
                                        </div>
                                        <!-- end widget content -->

                                    </div>
                                    
                                    <!-- end widget div -->

                                </div>
                                <!-- end widget -->

                            </div>
                            

                        </div>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- WIDGET END -->

    </div>
</div>
<!-- END MAIN CONTENT -->

<!-- FLOT CHARTS -->
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.time.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.selection.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.resize.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.pie.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.stack.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.crosshair.min.js')}}"></script>
<script type="text/javascript" language="javascript" src="{{asset('backend/js/plugin/flot_p2/jquery.flot.orderBars.js')}}"></script>

<!-- flot tooltips plugin-->
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.tooltip.min.js')}}"></script>

<!-- FLOT GROWRAF -->
<script src="{{asset('backend/js/plugin/flot-growraf/jquery.flot.growraf.min.js')}}"></script>



<!-- ECHART english -->
<script src="{{asset('backend/js/plugin/echart/www/js/echarts-all-english-v2.js')}}"></script>
<!--script src="{{asset('backend/js/plugin/echart/theme/helianthus.js')}}"></script-->

<link rel="stylesheet" type="text/css" href="{{asset('backend/js/plugin/echart/www/js/style.css')}}" />



<!-- END: ECHART -->
<script type="text/javascript">
    ////////
    // theme
    //////

    var theme = {
    backgroundColor: '#FFFFFF',//'#F2F2E6',
     
    color: [
        '#0ea50f', '#1beef1', '#ffdb00','#082c44',

        '#f5b041','#3498db','#8AED35','#ef3430','#8AED35','#30b193','#8AED35','#F4E24E','#FE9616','#ff69b4',
        '#E42B6D','#ba55d3','#cd5c5c','#ffa500','#40e0d0',
        '#E95569','#ff6347','#7b68ee','#00fa9a','#ffd700',
        '#6699FF','#ff6666','#3cb371','#b8860b','#30e0e0'
    ],

     
    title: {
        backgroundColor: '#FFFFFF', //'#F2F2E6',
        itemGap: 10
       ,                
        textStyle: {
            color: '#8A826D',
            fontFamily : 'DB Ozone X'
           // fontfamily: '\'TH SarabunPSK\', \'DB Ozone X\', \'DB Ozone X\'', // i.e. 'Times New Roman'

           // fontSize: 16

        },
        subtextStyle: {
            color: '#E877A3',
            fontFamily : 'DB Ozone X'           
        }
    },

     
    dataRange: {
        x:'right',
        y:'center',
        itemWidth: 5,
        itemHeight:25,
        color:['#E42B6D','#F9AD96'],
        text:['高','低'],          
        textStyle: {
            color: '#8A826D'           
        }
    },

    toolbox: {
        color : ['#E95569','#E95569','#E95569','#E95569'],
        effectiveColor : '#ff4500',
        itemGap: 8
    },

     
    tooltip: {
        backgroundColor: 'rgba( 27, 38, 49, 0.8)', //'rgba(138,130,109,0.7)',      
        axisPointer : {             
            type : 'line',          
            lineStyle : {           
                color: '#6B6455',
                type: 'dashed'
            },
            crossStyle: {           
                color: '#A6A299'
            },
            shadowStyle : {                      
                color: 'rgba(200,200,200,0.3)'
            }
        }
    },

 
    dataZoom: {
        dataBackgroundColor: 'rgba(130,197,209,0.6)',            
        fillerColor: 'rgba(233,84,105,0.1)',    
        handleColor: 'rgba(107,99,84,0.8)'     
    },

     
    grid: {
        borderWidth:0
    },

   
    categoryAxis: {
        axisLine: {             
            lineStyle: {        
                color: '#6B6455'
            }
        },
        splitLine: {            
            show: false
        }
    },

     
    valueAxis: {
        axisLine: {             
            show: false
        },
        splitArea : {
            show: false
        },
        splitLine: {            
            lineStyle: {        
                color: ['#FFF'],
                type: 'dashed'
            }
        }
    },

    polar : {
        axisLine: {             
            lineStyle: {        
                color: '#ddd'
            }
        },
        splitArea : {
            show : true,
            areaStyle : {
                color: ['rgba(250,250,250,0.2)','rgba(200,200,200,0.2)']
            }
        },
        splitLine : {
            lineStyle : {
                color : '#ddd'
            }
        }
    },

    timeline : {
        lineStyle : {
            color : '#6B6455'
        },
        controlStyle : {
            normal : { color : '#6B6455'},
            emphasis : { color : '#6B6455'}
        },
        symbol : 'emptyCircle',
        symbolSize : 3
    },

     
    bar: {
        itemStyle: {
            normal: {
                barBorderRadius: 0
            },
            emphasis: {
                barBorderRadius: 0
            }
        }
    },

 
    line: {
        smooth : true,
        symbol: 'emptyCircle',   
        symbolSize: 3            
    },


     
    k: {
        itemStyle: {
            normal: {
                color: '#E42B6D',       
                color0: '#44B7D3',       
                lineStyle: {
                    width: 1,
                    color: '#E42B6D',    
                    color0: '#44B7D3'    
                }
            }
        }
    },

     
    scatter: {
        itemStyle: {
            normal: {
                borderWidth:1,
                borderColor:'rgba(200,200,200,0.5)'
            },
            emphasis: {
                borderWidth:0
            }
        },
        symbol: 'circle',   
        symbolSize: 4        
    },

     
    radar : {
        symbol: 'emptyCircle',     
        symbolSize:3
        //symbol: null,          
        //symbolRotate : null,   
    },

    map: {
        itemStyle: {
            normal: {
                areaStyle: {
                    color: '#ddd'
                },
                label: {
                    textStyle: {
                        color: '#E42B6D'
                    }
                }
            },
            emphasis: {                 
                areaStyle: {
                    color: '#fe994e'
                },
                label: {
                    textStyle: {
                        color: 'rgb(100,0,0)'
                    }
                }
            }
        }
    },

    force : {
        itemStyle: {
            normal: {
                nodeStyle : {
                    borderColor : 'rgba(0,0,0,0)'
                },
                linkStyle : {
                    color : '#6B6455'
                }
            }
        }
    },

    chord : {
        itemStyle : {
            normal : {
                chordStyle : {
                    lineStyle : {
                        width : 0,
                        color : 'rgba(128, 128, 128, 0.5)'
                    }
                }
            },
            emphasis : {
                chordStyle : {
                    lineStyle : {
                        width : 1,
                        color : 'rgba(128, 128, 128, 0.5)'
                    }
                }
            }
        }
    },

    gauge : {                  
        center:['50%','80%'],
        radius:'100%',
        startAngle: 180,
        endAngle : 0,
        axisLine: {            
            show: true,        
            lineStyle: {       
                color: [[0.2, '#44B7D3'],[0.8, '#6B6455'],[1, '#E42B6D']],
                width: '40%'
            }
        },
        axisTick: {            
            splitNumber: 2,    
            length: 5,         
            lineStyle: {       
                color: '#fff'
            }
        },
        axisLabel: {           
            textStyle: {       
                color: '#fff',
                fontWeight:'bolder'
            }
        },
        splitLine: {           
            length: '5%',      
            lineStyle: {       
                color: '#fff'
            }
        },
        pointer : {
            width : '40%',
            length: '80%',
            color: '#fff'
        },
        title : {
          offsetCenter: [0, -20],        
          textStyle: {        
            color: 'auto',
            fontSize: 20
          }
        },
        detail : {
            offsetCenter: [0, 0],       
            textStyle: {        
                color: 'auto',
                fontSize: 40
            }
        }
    }
    /*,

    textStyle: {
        fontFamily: '微软雅黑, Arial, Verdana, sans-serif'
    }*/
};


 var theme2 = {
    backgroundColor: '#FFFFFF',//'#F2F2E6',
     
    /*color: [
        '#f5b041','#3498db','#ef3430','#8AED35','#8AED35','#30b193','#8AED35','#F4E24E','#FE9616','#ff69b4',
        '#E42B6D','#ba55d3','#cd5c5c','#ffa500','#40e0d0',
        '#E95569','#ff6347','#7b68ee','#00fa9a','#ffd700',
        '#6699FF','#ff6666','#3cb371','#b8860b','#30e0e0'
    ],*/

    color: [
        '#1beef1', '#ff0081', '#082c44','#ff5c5c',
       
        '#3498db','#8AED35','#ef3430','#8AED35','#30b193','#8AED35','#F4E24E','#FE9616','#ff69b4',
        '#E42B6D','#ba55d3','#cd5c5c','#ffa500','#40e0d0',
        '#E95569','#ff6347','#7b68ee','#00fa9a','#ffd700',
        '#6699FF','#ff6666','#3cb371','#b8860b','#30e0e0'
    ],

     
    title: {
        backgroundColor: '#FFFFFF', //'#F2F2E6',
        itemGap: 10
       ,                
        textStyle: {
            color: '#8A826D',
            fontFamily : 'DB Ozone X'
           // fontfamily: '\'TH SarabunPSK\', \'DB Ozone X\', \'DB Ozone X\'', // i.e. 'Times New Roman'

           // fontSize: 16

        },
        subtextStyle: {
            color: '#E877A3',

            fontFamily : 'DB Ozone X'
        }
    },

     
    dataRange: {
        x:'right',
        y:'center',
        itemWidth: 5,
        itemHeight:25,
        color:['#E42B6D','#F9AD96'],
        text:['高','低'],          
        textStyle: {
            color: '#8A826D'           
        }
    },

    toolbox: {
        color : ['#E95569','#E95569','#E95569','#E95569'],
        effectiveColor : '#ff4500',
        itemGap: 8
    },

     
    tooltip: {
        backgroundColor: 'rgba( 27, 38, 49, 0.8)', //'rgba(138,130,109,0.4)',      
        axisPointer : {             
            type : 'line',          
            lineStyle : {           
                color: '#6B6455',
                type: 'dashed'
            },
            crossStyle: {           
                color: '#A6A299'
            },
            shadowStyle : {                      
                color: 'rgba(200,200,200,0.3)'
            }
        }
    },

 
    dataZoom: {
        dataBackgroundColor: 'rgba(130,197,209,0.6)',            
        fillerColor: 'rgba(233,84,105,0.1)',    
        handleColor: 'rgba(107,99,84,0.8)'     
    },

     
    grid: {
        borderWidth:0
    },

   
    categoryAxis: {
        axisLine: {             
            lineStyle: {        
                color: '#6B6455'
            }
        },
        splitLine: {            
            show: false
        }
    },

     
    valueAxis: {
        axisLine: {             
            show: false
        },
        splitArea : {
            show: false
        },
        splitLine: {            
            lineStyle: {        
                color: ['#FFF'],
                type: 'dashed'
            }
        }
    },

    polar : {
        axisLine: {             
            lineStyle: {        
                color: '#ddd'
            }
        },
        splitArea : {
            show : true,
            areaStyle : {
                color: ['rgba(250,250,250,0.2)','rgba(200,200,200,0.2)']
            }
        },
        splitLine : {
            lineStyle : {
                color : '#ddd'
            }
        }
    },

    timeline : {
        lineStyle : {
            color : '#6B6455'
        },
        controlStyle : {
            normal : { color : '#6B6455'},
            emphasis : { color : '#6B6455'}
        },
        symbol : 'emptyCircle',
        symbolSize : 3
    },

     
    bar: {
        itemStyle: {
            normal: {
                barBorderRadius: 0
            },
            emphasis: {
                barBorderRadius: 0
            }
        }
    },

 
    line: {
        smooth : true,
        symbol: 'emptyCircle',   
        symbolSize: 3            
    },


     
    k: {
        itemStyle: {
            normal: {
                color: '#E42B6D',       
                color0: '#44B7D3',       
                lineStyle: {
                    width: 1,
                    color: '#E42B6D',    
                    color0: '#44B7D3'    
                }
            }
        }
    },

     
    scatter: {
        itemStyle: {
            normal: {
                borderWidth:1,
                borderColor:'rgba(200,200,200,0.5)'
            },
            emphasis: {
                borderWidth:0
            }
        },
        symbol: 'circle',    
        symbolSize: 4        
    },

     
    radar : {
        symbol: 'emptyCircle',     
        symbolSize:3
        //symbol: null,          
        //symbolRotate : null,   
    },

    map: {
        itemStyle: {
            normal: {
                areaStyle: {
                    color: '#ddd'
                },
                label: {
                    textStyle: {
                        color: '#E42B6D'
                    }
                }
            },
            emphasis: {                  
                areaStyle: {
                    color: '#fe994e'
                },
                label: {
                    textStyle: {
                        color: 'rgb(100,0,0)'
                    }
                }
            }
        }
    },

    force : {
        itemStyle: {
            normal: {
                nodeStyle : {
                    borderColor : 'rgba(0,0,0,0)'
                },
                linkStyle : {
                    color : '#6B6455'
                }
            }
        }
    },

    chord : {
        itemStyle : {
            normal : {
                chordStyle : {
                    lineStyle : {
                        width : 0,
                        color : 'rgba(128, 128, 128, 0.5)'
                    }
                }
            },
            emphasis : {
                chordStyle : {
                    lineStyle : {
                        width : 1,
                        color : 'rgba(128, 128, 128, 0.5)'
                    }
                }
            }
        }
    },

    gauge : {                  
        center:['50%','80%'],
        radius:'100%',
        startAngle: 180,
        endAngle : 0,
        axisLine: {            
            show: true,        
            lineStyle: {       
                color: [[0.2, '#44B7D3'],[0.8, '#6B6455'],[1, '#E42B6D']],
                width: '40%'
            }
        },
        axisTick: {            
            splitNumber: 2,    
            length: 5,         
            lineStyle: {       
                color: '#fff'
            }
        },
        axisLabel: {           
            textStyle: {       
                color: '#fff',
                fontWeight:'bolder'
            }
        },
        splitLine: {           
            length: '5%',      
            lineStyle: {       
                color: '#fff'
            }
        },
        pointer : {
            width : '40%',
            length: '80%',
            color: '#fff'
        },
        title : {
          offsetCenter: [0, -20],        
          textStyle: {        
            color: 'auto',
            fontSize: 20
          }
        },
        detail : {
            offsetCenter: [0, 0],       
            textStyle: {        
                color: 'auto',
                fontSize: 40
            }
        }
    }
    /*,

    textStyle: {
        fontFamily: ', Arial, Verdana, sans-serif'
    }*/
};

    $(document).ready(function(){
        
        $('#export_search').on('click',function() {
            var plan         = $('#plan').val();
            var select_month = $('#select_month').val();  // 0 - 12
            var select_year  = $('#select_year').val();   // now - (now-20) 
            // alert('MONTH=' + $('#select_month').val() + ' ,YEAR=' + $('#select_year').val());
            window.location.href =  "EquityNavReport/exportsearchmain?plan=" + plan + 
                                  "&select_month=" + select_month + "&select_year=" + select_year;
            return false;
        });

        $('#export_search_details').on('click',function() {
            
            // use criteria from ajax form
            var plan         = $('#txt_details_plan').val();
            var select_month = $('#txt_details_month').val();  
            var select_year  = $('#txt_details_year').val();  
            var lnk =   "EquityNavReport/exportsearchdetails?plan=" + plan + 
                                   "&select_month=" + select_month + "&select_year=" + select_year;
            
            window.location.href = lnk; //"EquityNavReport/exportsearchdetails?plan=" + plan + 
                                   //"&select_month=" + select_month + "&select_year=" + select_year;
            return false;
        });

        meaDatepicker("date_start","date_end");
        meaDatepicker("date_end");

        $('html').append('<input type="hidden" value="1" id="currentpage_search" />');

        $('#btn_search').on('click',function(e) {
            //e.preventDefault();
            $('#export_search').show();
            $('#export_search_details').hide();
            $('#previous').hide(); 
            var plan         = $('#plan').val();
            var select_month = $('#select_month').val();  // 0 - 12
            var select_year  = $('#select_year').val();   // now - (now-20) 

            //alert('plan=>' + plan + ', select_month =>' + select_month + ", select_year=>" + select_year)  ;
            console.log('Search: plan=>' + plan + ', select_month =>' + select_month + ", select_year=>" + select_year);
            var jsondata = {
                plan  : plan,
                month : select_month,
                year  : select_year
            };
            console.log(jsondata);
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(jsondata, "EquityNavReport/searchmain", RenderMainPieChart);

            return false;
        });

        $('#previous').on('click',function(e) { 
           $( "#btn_search" ).trigger( "click" );
        });
           
        var jsondata = {
            plan : '',
            month : '',
            year : ''
        };
        $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />'); 
        MeaAjax(jsondata,"EquityNavReport/searchmain", RenderMainPieChart);     

    }); // jquery ready

  
    function RenderMainPieChart(data) {
        var method = {
            UNSPECIFIED : {value: 0, name: "UNSPECIFIED"}, 
            SPECIFIED_YEAR_AND_LAST_MONTH: {value: 1, name: "SPECIFIED_YEAR_AND_LAST_MONTH"}, 
            CURRENT_YEAR_AND_SPECIFIED_MONTH : {value: 2, name: "CURRENT_YEAR_AND_SPECIFIED_MONTH"},
            SPECIFIED_YEAR_AND_MONTH : {value: 3, name: "SPECIFIED_YEAR_AND_MONTH"}
        };

        // var UNSPECIFIED                      = 0; // 0 - display TABLE current year and PIE last month 
        // var SPECIFIED_YEAR_AND_LAST_MONTH    = 1; // 1 - display TABLE selected year and PIE last month
        // var CURRENT_YEAR_AND_SPECIFIED_MONTH = 2; // 2 - display TABLE /PIE with selected month 
        // var SPECIFIED_YEAR_AND_MONTH         = 3; // 3 - display PIE chart upon selected year and month
        // alert('plan:' +  data.plan + ', method:' + data.renderMethod);

        var label1 = ['พันธบัตรรัฐบาล ธปท.และรัฐวิสาหกิจ',
                      'เงินฝากและตราสารหนี้ธนาตารพาณิชย์',
                      'หุ้นกู้',
                      'อื่นๆ ลูกหนี้และเจ้าหนี้จากการซื้อ/ ขายหลักทรัพย์'];

        var label2 = [// 'พันธบัตรรัฐบาล ธปท.และรัฐวิสาหกิจ',
                      'เงินฝากธนาคารพาณิชย์', //'เงินฝากและตราสารหนี้ธนาตารพาณิชย์',
                      'ตราสารทุน(หุ้น)',
                      'อื่นๆ ลูกหนี้และเจ้าหนี้จากการซื้อ/ ขายหลักทรัพย์'];
        
        switch(data.renderMethod) {
            case method.UNSPECIFIED:
                handleRenderMehod0(data, label1, label2);
                break;

            case method.SPECIFIED_YEAR_AND_LAST_MONTH:
                handleRenderMehod0(data, label1, label2);
                break;

            case method.CURRENT_YEAR_AND_SPECIFIED_MONTH:
                handleRenderMehod0(data, label1, label2);
                break;

            case method.SPECIFIED_YEAR_AND_MONTH:
                handleRenderMehod0(data, label1, label2);
                break;

            default:
                handleRenderMehod0(data, label1, label2);
                break;    

        }

        /* BOND details report by month*/
        $(".bond_details_by").on('click',function(){
          
            var payload = $(this).attr("data-id");
            var yy =  payload.substring(0, 4);
            var mm =  payload.substring(5, 7);
            //var yy = str.substring(0,4);
            //var mm = str.substring(5,7);
            
            var jsondata = {
                plan : 'ตราสารหนี้',
                month : mm,
                year : yy
            };

            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />'); 
            MeaAjax(jsondata,"EquityNavReport/bonddetails", RenderDetailsReport);   
            /*console.log('bond_details_by clicked =>' + payload);
            $.SmartMessageBox({
                title : "Detils [" + payload + " ]",
                content : "ท่านแน่ใจที่ต้องการจะดู รายการที่ท่านเลือก",
                buttons : '[ยกเลิก][OK]'
            }, function(ButtonPressed) {
                console.log('bond_details_by clicked');
            });*/
        });

        /* EQUITY details report by month*/  
        $(".equity_details_by").on('click',function(){
          
            var payload = $(this).attr("data-id");
            var yy =  payload.substring(0, 4);
            var mm =  payload.substring(5, 7);
            console.log('equity_details_by clicked => ' + yy);
            var jsondata = {
                plan : 'ตราสารทุน',
                month : mm,
                year : yy
            };

            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />'); 
            MeaAjax(jsondata,"EquityNavReport/equitydetails", RenderDetailsReport);   
        });
    }



    /**
     *  Method0: display TABLE current year and PIE last month 
     */
    function handleRenderMehod0(data, label1, label2) {
        console.log('handleMehod0()');
        $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
        $("#serch_data").html(data.html);
        $("#serch_data").fadeIn('300');
        
        console.log('Data PIE1: ' + data.pie1[0].value + ", " + data.pie1[1].value + ", " + data.pie1[2].value + "," + data.pie1[3].value );

        console.log('Data PIE2: ' + data.pie2[0].value + ", " + data.pie2[1].value + ", " + data.pie2[2].value);
        
           var data1 = [
                        {value:data.pie1[0].value,  name: label1[0]},
                        {value:data.pie1[1].value,  name: label1[1]},
                        {value:data.pie1[2].value,  name: label1[2]},
                        {value:data.pie1[3].value,  name: label1[3]}];

           var data2 = [
                        {value:data.pie2[0].value,  name: label2[0]},
                        {value:data.pie2[1].value,  name: label2[1]},
                        {value:data.pie2[2].value,  name: label2[2]}];

        var ref_date1 = data.reference_date1;
        var ref_date2 = data.reference_date2;
        $('#main_chart1').html('');
        $('#main_chart2').html('');

        if (data.plan == 'ตราสารหนี้') {
           renderPieChartBondNAV('main_chart1', label1, data1, ref_date1);
            renderPieChartEquityNAV('main_chart2', label2, data2, ref_date2);
           //$('#main_chart2').html('');
        } else if(data.plan == 'ตราสารทุน'){
           renderPieChartEquityNAV('main_chart2', label2, data2, ref_date2);
           renderPieChartBondNAV('main_chart1', label1, data1, ref_date1);
           //$('#main_chart1').html('');
        } else {
           renderPieChartBondNAV('main_chart1', label1, data1, ref_date1);
           renderPieChartEquityNAV('main_chart2', label2, data2, ref_date2);
        }
    }

    /**
     * Method1: handle render data current year & last month 
     */
   /* function handleRenderMehod1(data, label1, label2) {
        console.log('handleMehod1() ');
        $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
        $("#serch_data").html(data.html);
        $("#serch_data").fadeIn('300');
        
        console.log('Data PIE1: ' + data.pie1[1].value);

        console.log('Data PIE2: ' + data.pie2[1].value);
         
        var data1 = [
                        {value:data.pie1[0].value,  name: label1[0]},
                        {value:data.pie1[1].value,  name: label1[1]},
                        {value:data.pie1[2].value,  name: label1[2]},
                        {value:data.pie1[3].value,  name: label1[3]}];

        var label2 = ['พันธบัตรรัฐบาล ธปท.และรัฐวิสาหกิจ',
                      'เงินฝากและตราสารหนี้ธนาตารพาณิชย์',
                      'อื่นๆ ลูกหนี้และเจ้าหนี้จากการซื้อ/ ขายหลักทรัพย์'];
        var data2 = [
                        {value:data.pie2[0].value,  name: label2[0]},
                        {value:data.pie2[1].value,  name: label2[1]},
                        {value:data.pie2[2].value,  name: label2[2]}];

         
        if (data.plan == 'ตราสารหนี้') {
           renderPieChartBondNAV('main_chart1', label1, data1);
           $('#main_chart2').html('');
        } else if(data.plan == 'ตราสารทุน'){
           renderPieChartEquityNAV('main_chart2', label2, data2);
           $('#main_chart1').html('');
        } else {
           renderPieChartBondNAV('main_chart1', label1, data1);
           renderPieChartEquityNAV('main_chart2', label2, data2);
        }
    }*/

    /**
     * Method2: handle render data current year & last month 
     */
    /*function handleRenderMehod2(data, label1, label2) {
        console.log('handleMehod2() ');
        $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
        $("#serch_data").html(data.html);
        $("#serch_data").fadeIn('300');
        
        console.log('Data PIE1: ' + data.pie1[1].value);

        console.log('Data PIE2: ' + data.pie2[1].value);
         
        var data1 = [
                        {value:data.pie1[0].value,  name: label1[0]},
                        {value:data.pie1[1].value,  name: label1[1]},
                        {value:data.pie1[2].value,  name: label1[2]},
                        {value:data.pie1[3].value,  name: label1[3]}];

         
        var data2 = [
                        {value:data.pie2[0].value,  name: label2[0]},
                        {value:data.pie2[1].value,  name: label2[1]},
                        {value:data.pie2[2].value,  name: label2[2]}];

        if (data.plan == 'ตราสารหนี้') {
           renderPieChartBondNAV('main_chart1', label1, data1);
           $('#main_chart2').html('');
        } else if(data.plan == 'ตราสารทุน'){
           renderPieChartEquityNAV('main_chart2', label2, data2);
           $('#main_chart1').html('');
        } else {
           renderPieChartBondNAV('main_chart1', label1, data1);
           renderPieChartEquityNAV('main_chart2', label2, data2);
        }
    }
    */
    /**
     * Method3: display PIE chart with selected year and month
     */
     /*
    function handleRenderMehod3(data, label1, label2) {
        console.log('handleMehod3() ');
        $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
        $("#serch_data").html(data.html);
        $("#serch_data").fadeIn('300');
        
        console.log('Data PIE1: ' + data.pie1[1].value);

        console.log('Data PIE2: ' + data.pie2[1].value);
         
        var data1 = [
                        {value:data.pie1[0].value,  name: label1[0]},
                        {value:data.pie1[1].value,  name: label1[1]},
                        {value:data.pie1[2].value,  name: label1[2]},
                        {value:data.pie1[3].value,  name: label1[3]}];

         
        var data2 = [
                        {value:data.pie2[0].value,  name: label2[0]},
                        {value:data.pie2[1].value,  name: label2[1]},
                        {value:data.pie2[2].value,  name: label2[2]}];

        $('#main_chart1').html('');
        $('#main_chart2').html('');
        if (data.plan == 'ตราสารหนี้') {
           renderPieChartBondNAV('main_chart1', label1, data1);
           renderPieChartEquityNAV('main_chart2', label2, data2);
           //$('#main_chart2').html('');
        } else if(data.plan == 'ตราสารทุน'){

           renderPieChartEquityNAV('main_chart2', label2, data2);
           renderPieChartBondNAV('main_chart1', label1, data1);
           //$('#main_chart1').html('');
        } else {
           renderPieChartBondNAV('main_chart1', label1, data1);
           renderPieChartEquityNAV('main_chart2', label2, data2);
        }
    }
*/
    //////////////////////////////////////////////////////////////////////////////

    function RenderChart() {
        
        showhideByFlag(document.getElementById('pie-chart').id, true);
    }

    function RenderSearch(data){
        
        $("#serch_data").html(data.html);
        $("#serch_data").fadeIn('300');

        $("#page_click_search").on('click',PageRenderSearch);
/*
        showhideByFlag(document.getElementById('trading-chart-title').id, true);
        showhideByFlag(document.getElementById('widget_bar_chart').id, true);
        showhideByFlag(document.getElementById('trading-date').id, true);
        showhideByFlag(document.getElementById('trading-bar-chart').id, true);
        showBarChart();
        showhideByFlag(document.getElementById('trading-chart-title').id, false);
        showhideByFlag(document.getElementById('widget_bar_chart').id, false);
        showhideByFlag(document.getElementById('trading-date').id, false);
        showhideByFlag(document.getElementById('trading-bar-chart').id, false);
        */
        showPieChart();
    }

    function PageRenderSearch(){

        var p = $(this).attr('data-page');
        var page_size = $('#page-size-search').val();
        var CurPage = $('#currentpage_search').val();
        $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');

        if(p == "pre"){
            p = parseInt(CurPage) - 1;
        }

        if(p == "next"){
            p = parseInt(CurPage) + 1;
        }
        $('#currentpage_search').val(p);

        var EmpID = $('#name').val();
        var depart = $('#depart').val();
        var plan = $('#plan').val();
        var date_start = $('#hd_date_start').val();
        var date_end =$('#hd_date_end').val();

        var check_name =$('#check_name').is(':checked');
        var check_depart =$('#check_depart').is(':checked');
        var check_plan =$('#check_plan').is(':checked');
        var check_date =$('#check_date').is(':checked');


        var jsondata = {
            pagesize : page_size,
            PageNumber:p,
            emp_id : EmpID,
            depart :depart,
            plan : plan,
            date_start:date_start,
            date_end:date_end,
            check_name :check_name,
            check_depart:check_depart,
            check_plan:check_plan,
            check_date:check_date

        };

        MeaAjax(jsondata,"EquityNavReport/searchmain",RenderSearch);
    };

    ////////////////////
    // TOOLS 
    function showhide(id) {
        var e = document.getElementById(id);
        e.style.display = (e.style.display == 'block') ? 'none' : 'block';
    }

 
    function showhideByFlag(id, flag) {
        var e = document.getElementById(id);
        e.style.display = flag ? 'block': 'none' ;
    }

   /*
    function showPieChart() {
        var colors = [];
        var data = [];
        
        var t1 = 83.00;
        var t2 = -0.99;
        var t3 = 17.99;
        
       
        data[0] = {
            label: "พันธบัตรรัฐบาล ธปท. และรัฐวิสาหกิจ",
            data: t1 
        }

        data[1] = {
            label: "เงินฝากและตราสารหนี้ ธนาคารพาณิชย์",
            data: t2
        }

        data[2] = {
            label: "อื่นๆ (ลูกหนี้และเจ้าหนี้จากการซื้อ/ขายหลักทรัพย์)",
            data: t3
        }
          
         
        $.plot($("#pie-chart"), data, {
            series: {
                pie: {
                    //innerRadius: 0.6,
                    show: true
                }
            },

            legend: {
                show: false
            },

            grid: {
                hoverable: true,
                clickable: true
            },

            tooltip: {
                show: true,
                content: " %s = %n % ", //(%p.0%) ", 
                shifts: {
                    x: 20,
                    y: 0
                },
                defaultTheme: false
            },      

            colors: ['#8bc34a', '#d32f2f','#028fa0','#e91e63','#FF5733',
                     '#FE0E1C', '#d32f2f','#e91e63'] 
        });
    }
*/
    // ECHART
    function renderPieChartTimeline(tagid) 
    {
    var myChart;
    var domCode = document.getElementById('sidebar-code');
    var domGraphic = document.getElementById('graphic');
    var domMain = document.getElementById(tagid);
    var domMessage = document.getElementById('wrong-message');
    var iconResize = document.getElementById('icon-resize');
    var needRefresh = false;
   
    var idx = 1;
    var isExampleLaunched;
    var curTheme;
    var option;

    var captions = ['','ม.ค.','ก.พ','มี.ค.', 'เม.ย', 'พ.ค.', 'มิ.ย.',
                    'ก.ค','ส.ค.','ก.ย.','ต.ค.','พ.ย','ธ.ค.',
                    'รวม'
                   ];
        option = {
        
        timeline : {
            data : [
                '2016-01-01', '2016-02-01', '2016-03-01', '2016-04-01', '2016-05-01', 
                { name:'2016-06-01', symbol:'emptyStar6', symbolSize:8 },
                '2016-07-01','2016-08-01', '2016-09-01', '2016-10-01', '2016-11-01',
                { name:'2016-12-01', symbol:'star6', symbolSize:8 }
            ],
            label : {
                formatter : function(s) {
                    return captions[parseInt(s.slice(5, 7))];
                }
            }
        },

        options : [
            {
                title : {
                    text: '<h3>การลงทุนในตราสารทุน</h3>',
                    subtext: 'แบบรายดือน ปี 2559 ',
                    x:'center'
                },
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient : 'vertical',
                    x : 'left',
                    y : 'top',
                    data:['Chrome','Firefox','Safari','IE9+','IE8-']
                },
                toolbox: {
                    show : true,
                    feature : {
                        mark : {show: true},
                        dataView : {show: false, readOnly: true},
                        magicType : {
                            show: true, 
                            type: ['pie'], //, 'funnel'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1700
                                }
                            }
                        },
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                // Jan
                series : [
                    {
                        name:'มกราคม',
                        type:'pie',
                        center: ['50%', '45%'],
                        radius: '50%',

                        itemStyle : {
                            normal : {
                                label : {
                                    position : 'outer',
                                    formatter : function (params) {                         
                                        return params.name + ' ( ' +  (params.percent - 0).toFixed(0) + '% )'
                                    }
                                },
                                labelLine : {
                                    show : true
                                }
                            },

                            emphasis : {
                                label : {
                                    show : false,
                                    formatter : "{b}\n{d}%"
                                }
                            }
                
                        },

                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'กุมพาพันธ์',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'มีนาคม',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'เมษายน',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'พฤษภาคม',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'มิถุนายน',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'กรกฎาคม',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'สิงหาคม',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'กันยายน',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'ตุลาคม',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'พฤศจิกายน',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            },
            {
                series : [
                    {
                        name:'ธันวาคม',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            }
           ,
          
            // all months
            {
                series : [
                    {
                        name:'รวม',
                        type:'pie',
                        data:[
                            {value: idx * 128 + 80,  name:'Chrome'},
                            {value: idx * 64  + 160,  name:'Firefox'},
                            {value: idx * 32  + 320,  name:'Safari'},
                            {value: idx * 16  + 640,  name:'IE9+'},
                            {value: idx++ * 8  + 1280, name:'IE8-'}
                        ]
                    }
                ]
            }
        ]
    };


    myChart = echarts.init(document.getElementById(tagid));
    myChart.setOption(option);
    curTheme = theme;
    myChart.setTheme(curTheme);

}

function renderPieChartBondNAV(tagid, equity_label, equity_data, ref_date) 
    {
    var myChart1;
    var domCode = document.getElementById('sidebar-code');
    var domGraphic = document.getElementById('graphic');
    var domMain = document.getElementById(tagid);
    var domMessage = document.getElementById('wrong-message');
    var iconResize = document.getElementById('icon-resize');
    var needRefresh = false;
   
    var idx = 1;
    var isExampleLaunched;
    var curTheme;
    var option;

    var t_array = equity_data.slice(0, equity_data.length);
 
    console.log('Data 1: ' + equity_data[0].value + ", " + equity_data[1].value + ", " + equity_data[2].value + "," + equity_data[3].value );

    console.log('equity_data.length=' + equity_data.length);
    console.log('t_array.length=' + t_array.length);
    
    var total = 0;

    var bondPieData = [
                        {value: Math.abs(equity_data[0].value),  name: equity_label[0]},
                        {value: Math.abs(equity_data[1].value),  name: equity_label[1]},
                        {value: Math.abs(equity_data[2].value),  name: equity_label[2]},
                        {value: Math.abs(equity_data[3].value),  name: equity_label[3]}];

    for (var i= 0; i< t_array.length; i++) {
        total += Math.abs(t_array[i].value);
        var v = { value: Math.abs(equity_data[i].value),  name: equity_label[i]};
    }
     
    console.log('Data 1: ' + equity_data[0].value + ", " + equity_data[1].value + ", " + equity_data[2].value + "," + equity_data[3].value );
    console.log('Data 1: ' + bondPieData[0].value + ", " + bondPieData[1].value + ", " + bondPieData[2].value + "," + bondPieData[3].value );      
    
    var captions = ['','ม.ค.','ก.พ','มี.ค.', 'เม.ย', 'พ.ค.', 'มิ.ย.',
                    'ก.ค','ส.ค.','ก.ย.','ต.ค.','พ.ย','ธ.ค.',
                    'รวม'
                   ];
    var cyear = new Date().getFullYear() ;
    var mm = parseInt( isNaN( $('#select_month').val()) ? 0 : $('#select_month').val());
    var yy = parseInt( isNaN($('#select_year').val()) ? 0: $('#select_year').val());
    console.log('MM:' + mm+ ', YY:' + yy);

    var subtext = (mm > 0) ? captions[mm] : '';  
    var selyear = (yy > 0) ? yy + 543 : ''; 
 
        option = { 
            title : {
                text: 'การลงทุนในตราสารหนี้',
                subtext: ref_date,  
                x:'center',
                textStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 18
                },
                subtextStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 14
                }
            },
            tooltip : {
                trigger: 'item',
               // formatter: "{a} <br/>{b} : {c} ( {d}% )"

                formatter : function (params) {               
                                   var value = 0;
                                    for(var i = 0; i < bondPieData.length; i++) {
                                        if (params.name == equity_label[i]) {
                                            value = equity_data[i].value;
                                            break;
                                        }
                                    } 
                                    return  params.name + ' <BR/> ' +(value - 0).toFixed(2) + '% '; 
                                }
            },
            legend: {
                orient : 'vertical',
                x : 'left',
                y : 'bottom',
                textStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 14
                },
                data:equity_label  
            },
            toolbox: {
                    show : true,
                    orient : 'vertical',
                    x: 'right', 
                    y: 'top',
                    feature : {
                        mark : {show: true},
                        dataView : {show: false, readOnly: true},
                        magicType : {
                            show: false, 
                            type: ['pie'], //, 'funnel'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1700
                                }
                            }
                        },
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },

            //calculable : true,
            series : [
                {
                    name:'การลงทุนในตราสารหนี้',
                    type:'pie',
                    selectedMode: 'single',
                    radius : '40%',
                    center: ['50%', '50%'],
                    itemStyle : {
                        normal : {
                            label : {
                                position : 'outer',
                                /*formatter : function (params) {               
                                    if (params.name == equity_label[equity_label.length - 1]) {          
                                       return  ' -' + (params.percent - 0).toFixed(2) + '% ' //params.name + ' ( ' +  (params.percent - 0).toFixed(0) + '% )'
                                    } else {
                                        var pc = (params.value * 100) / total;  //+ (equity_label[3].value / 3);
                                        return (pc).toFixed(2) + '% ';

                                        //return (params.percent - 0).toFixed(0) + '% ';
                                    }
                                }*/
                                formatter : function (params) {  
                                    var value = 0;
                                    for(var i = 0; i < bondPieData.length; i++) {
                                        if (params.name == equity_label[i]) {
                                            value = equity_data[i].value;
                                            //console.log(params.name + ' => ' + value);
                                            break;
                                               //(equity_label[i].value < 0) ?
                                        }
                                    } 
                                    
                                    return   (value - 0).toFixed(2) + '% '; //params.name + ' ( ' +  (params.percent - 0).toFixed(0) + '% )'
                                }
                            },
                            labelLine : {
                                show : true
                            }
                        },

                        emphasis : {
                            label : {
                                show : false,
                                position : 'center',
                                textStyle : {
                                    fontSize : '30',
                                    fontWeight : 'bold'
                                },
                                formatter : function (params) {  
                                    var value = 0;
                                    for(var i = 0; i < bondPieData.length; i++) {
                                        if (params.name == equity_label[i]) {
                                            value = equity_data[i].value; 
                                            break;
                                        }
                                    } 
                                    return   (value - 0).toFixed(2) + '% '; //params.name + ' ( ' +  (params.percent - 0).toFixed(0) + '% )'
                                }
                            }
                        }
                
                    },

                    data: bondPieData //equity_data 
                }
            ]
        };

    myChart1 = echarts.init(document.getElementById(tagid));
    myChart1.setOption(option);
  
    curTheme = theme;
    myChart1.setTheme(curTheme);
}

function renderPieChartEquityNAV(tagid, equity_label, equity_data, ref_date) 
{
    var myChart2;
    var domCode = document.getElementById('sidebar-code');
    var domGraphic = document.getElementById('graphic');
    var domMain = document.getElementById(tagid);
    var domMessage = document.getElementById('wrong-message');
    var iconResize = document.getElementById('icon-resize');
    var needRefresh = false;
   
    var idx = 1;
    var isExampleLaunched;
    var curTheme;
    var option;
    console.log('equity_data.length = ' +  equity_data.length + " [0]=" + equity_data[0].value + ', [1]='  + equity_data[1].value + ", [2]=" + equity_data[2].value);

    var t_array = equity_data.slice(0, equity_data.length - 1);
    var total = 0;
    for (var i=t_array.length; i--;) {
        total += t_array[i].value;
    }
   
    var equityPieData = [
                        {value: Math.abs(equity_data[0].value),  name: equity_label[0]},
                        {value: Math.abs(equity_data[1].value),  name: equity_label[1]},
                        {value: Math.abs(equity_data[2].value),  name: equity_label[2]}
                        //{value: Math.abs(equity_data[3].value),  name: equity_label[3]}
                        ];

   

    //////////// HACK

    //equity_data[2].value = 0.0;

    var captions = ['','ม.ค.','ก.พ','มี.ค.', 'เม.ย', 'พ.ค.', 'มิ.ย.',
                    'ก.ค','ส.ค.','ก.ย.','ต.ค.','พ.ย','ธ.ค.',
                    'รวม'
                   ];

        option = {
            title : {
                text: 'การลงทุนในตราสารทุน',
                subtext: ref_date, //'แบบรวม ปี 2559 ',
                x:'center',
                textStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 18
                },
                subtextStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 14
                }
            },
            tooltip : {
                trigger: 'item',
                formatter : function (params) {  
                                    var value = 0;
                                    for(var i = 0; i < equityPieData.length; i++) {
                                        if (params.name == equity_label[i]) {
                                            value = equity_data[i].value;
                                            //console.log(params.name + ' => ' + value);
                                            break;
                                               //(equity_label[i].value < 0) ?
                                        }
                                    } 
                                    
                                    return   params.name + ' <BR/>' + (value - 0).toFixed(2) + '% '; //params.name + ' ( ' +  (params.percent - 0).toFixed(0) + '% )'
                                }
            },
            legend: {
                orient : 'vertical',
                x : 'left',
                y : 'bottom',
                textStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 14
                },
                data:equity_label  
            },
            toolbox: {
                    show : true,
                    orient : 'vertical',
                    x: 'right', 
                    y: 'top',
                    feature : {
                        mark : {show: true},
                        dataView : {show: false, readOnly: true},
                        magicType : {
                            show: false, 
                            type: ['pie'], //, 'funnel'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1700
                                }
                            }
                        },
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },

            //calculable : true,
            series : [
                {
                    name:'การลงทุนในตราสารทุน',
                    type:'pie',
                    selectedMode: 'single',
                    radius : '40%',
                    center: ['50%', '50%'],
                    itemStyle : {
                        normal : {
                            label : {
                                position : 'outer',
                                formatter : function (params) {  
                                    var value = 0;
                                    for(var i = 0; i < equityPieData.length; i++) {
                                        if (params.name == equity_label[i]) {
                                            value = equity_data[i].value; 
                                            break;
                                        }
                                    } 
                                    return   (value - 0).toFixed(2) + '% '; //params.name + ' ( ' +  (params.percent - 0).toFixed(0) + '% )'
                                }
                            },
                            labelLine : {
                                show : true
                            }
                        },

                        emphasis : {
                            label : {
                                show : false,
                                position : 'center',
                                textStyle : {
                                    fontSize : '30',
                                    fontWeight : 'bold'
                                },
                                formatter : function (params) {  
                                    var value = 0;
                                    for(var i = 0; i < equityPieData.length; i++) {
                                        if (params.name == equity_label[i]) {
                                            value = equity_data[i].value; 
                                            break;
                                        }
                                    } 
                                    return   (value - 0).toFixed(2) + '% '; //params.name + ' ( ' +  (params.percent - 0).toFixed(0) + '% )'
                                }
                            }
                        }
                
                    },

                    data: equityPieData 
                }
            ]
        };


    myChart2 = echarts.init(document.getElementById(tagid));
    myChart2.setOption(option);
  
    //curTheme = theme2;
    myChart2.setTheme(theme2);
}

function RenderDetailsReport(data) {
    console.log('RenderDetailsReport');
    $('#export_search').hide();
    $('#export_search_details').show();
    $('#previous').show();
      
    //$("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
    $("#serch_data").html(data.html);
    $("#serch_data").fadeIn('300');    
    

    //$('#txt_details_date').val((data.selected_year-543) + '-' + data.selected_month + '-01' );

    RenderMainPieChart(data); 
}

</script>

@stop
