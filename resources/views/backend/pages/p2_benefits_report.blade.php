@extends('backend.layouts.default')
@section('content')
<?php
$data = getmemulist();
$arrSidebar =getSideBar($data);
?>


<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>

                {{getMenutitle($arrSidebar)}}

            </h1>
        </div>

    </div>


    <div class="row" id="widget-grid" >

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <ul class="nav nav-tabs pull-left in">

                        <li class="active">

                            <a data-toggle="tab" href="#hr1"> <i class="fa fa-lg fa-arrow-circle-o-down"></i> <span class="hidden-mobile hidden-tablet"> ระบุเงื่อนไขการค้นหา </span> </a>

                        </li>

                        <!--li>
                            <a data-toggle="tab" href="#hr2"> <i class="fa fa-lg fa-arrow-circle-o-up"></i> <span class="hidden-mobile hidden-tablet"> รายงานสรุปของตราสารทุน  </span> </a>
                        </li-->

                    </ul>
                </header>

                <!-- widget div-->
                <div>


                    <!-- widget content -->
                    <div class="widget-body">

                        <div class="tab-content">
                            <div class="tab-pane active" id="hr1">

                                <div class="widget-body no-padding" style="width: 100% ;margin: 0 auto;background-color: #000000;border: 1px solid #E1E8F3">
                                    <header style="color: #fff; height: 40px; line-height: 40px; font-size: 18px; background-color: #a90329;padding-left: 20px;"> กล่องค้นหา </header>

                                    <form id="comment-form_darkman" class="smart-form" novalidate="novalidate">
                                        {{-- {!! csrf_field() !!} --}}
                                        <fieldset>
                                            <div class="row">
                                                <section class="col col-4">
                                                    <label class="label">  ระบุรายงาน</label>
                                                    <label class="input">
                                                        <select name="reporttype" id="reporttype" class="form-control">
                                                            <option value="">&nbsp;กรุณาเลือกรายงาน</option>
                                                            <option value="1">&nbsp;รายงานผลประโยชน์ กำไร-ขาดทุน (Gain-Loss)</option>
                                                            <option value="2">&nbsp;รายงานผลประโยชน์ เงินปันผล (Dividend)</option>
                                                            <option value="3">&nbsp;รายงานผลประโยชน์ส่วนเพิ่ม-ลด (Mark to Market)</option>
                                                        </select>
                                                    </label>
                                                </section>
                                            </div>

                                            <div class="row">
                                                <section class="col col-4">
                                                    <label class="label">ระบุบริษัทจัดการ</label>
                                                    <label class="input">
                                                        <select name="equity_name" id="equity_name" class="form-control">
                                                            <option value=""> บริษัทจัดการ</option>
                                                            @if($equitylist)
                                                                @foreach($equitylist as $item)
                                                                    <option value="{{$item->NAME_SHT}}">&nbsp;{{$item->NAME_SHT}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </label>
                                                </section>
                                                <section class="col col-4">
                                                    <label class="label"> ระบุกลุ่มอุตสาหกรรม</label>
                                                    <label class="input">
                                                        <select name="category" id="category" class="form-control">
                                                            <option value="">กลุ่มอุตสาหกรรม</option>
                                                            @if($equitycategory)
                                                                @foreach($equitycategory as $item)
                                                                    <option value="{{$item->INDUSTRIAL}}">&nbsp;{{$item->INDUSTRIAL}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </label>
                                                </section> 
                                            </div>

                                            <div class="row">
                                                <section class="col col-4">
                                                    <label class="label">กรุณาเลือกปี</label>
                                                    <label class="input">
                                                         <select name="choose_year" id="choose_year" class="form-control">
                                                            <option value="">&nbsp;กรุณาเลือกปี</option>
                                                            @if($equitybroker)
                                                               @foreach($years as $item)
                                                                   <option value="{{$item}}">&nbsp;{{$item}}</option>
                                                               @endforeach 
                                                            @endif
                                                        </select>
                                                    </label>
                                                </section>
                                                <section class="col col-4">
                                                    <button type="submit" id="btn_search" 
                                                            style="padding: 5px 20px 5px 20px; margin-top:20px;font-size: 18px;"
                                                            name="btn_search" class="btn btn-primary">
                                                        ค้นหา
                                                    </button>
                                                </section>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>


                                <a href="#" id="export_search" style="margin-top: 30px;" class="btn btn-labeled btn-success mea-btn-export"> <span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i></span>Export </a>
                                <a href="#" id="btn_chart"     style="margin-top: 30px;" class="btn btn-labeled btn-warning"> <span class="btn-label"><i class="glyphicon glyphicon-signal"></i></span>Chart </a>
                               
                                <!-- Widget ID (each widget will need unique ID)-->
                                <div class="jarviswidget jarviswidget-color-blueDark" style="margin-top: 5px;" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
                                    <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                    data-widget-colorbutton="false"
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true"export_search
                                    data-widget-sortable="false"

                                    -->

                                    <select class="form-control mea-pagesize" id="page-size-search" style="display:block;">
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>

                                    <header>
                                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    </header>

                                    <!-- widget div-->
                                    <div id="widget_search_table" style="display:block;">

                                        <!-- widget edit box -->
                                        <div class="jarviswidget-editbox">
                                            <!-- This area used as dropdown edit box -->
                                        </div>
                                        <!-- end widget edit box -->
                                        
                                        <!-- widget content -->
                                        <div class="widget-body"  >
                                            <div class="table-responsive" >
                                                <div id="serch_data">

                                                </div>
                                            </div>
                                        </div>
                                        <!-- end widget content -->
                                    </div>
                                    
                                    <!-- end widget div -->

                                    <!-- bar chart -->
                                    <div class="table-responsive" id="widget_bar_chart" style="display:none;">

                                            @if((!empty($date_start)) && (!empty($date_end)))
                                            <p class="report-title" id="trading-chart-title" style="display:block;">
                
                                                {{getMenutitle($arrSidebar)}} แยกตาม กลุ่มอุตสาหกรรม
                                            </p>
                                            <p class="report-period">
                                                
                                                @if($date_start != $date_end)
                                                     ข้อมูล ระหว่าง วันที่ <font color="red">{{$date_start}}</font> ถึง <font color="red">{{$date_end}} </font>
                                                @else
                                                     ข้อมูล วันที่ <font color="red">{{$date_start}}</font> เวลา
                                                                <font color="red">00:00</font> ถึง <font color="red">23:59</font>     
                                                @endif

                                                
                                            </p>
                                            @else
                                             
                                            @endif
                                            <!--div id="mea_chart" class="mea_chart" style="width: 100%; padding: 0px; display:block;">

                                            </div-->
                                             <div id="main_chart1" class="mea_chart" style="width: 100%; padding: 0px; display:block;">
                                            </div>   
                                            
                                    </div>

                                    <!-- แสดงกราฟ รายละเอีด ของ BU  -->
                                    <div class="table-responsive" id="widget_bar_chart_two" style="display:none;">
                                            @if((!empty($date_start)) && (!empty($date_end)))
                                            <p class="report-title" id="trading-chart-title" style="display:block;">
                
                                                {{getMenutitle($arrSidebar)}} แยกตาม หมวดธุรกิจ
                                            </p>
                                            <p class="report-period">
                                                
                                                @if($date_start != $date_end)
                                                     ข้อมูล ระหว่าง วันที่ <font color="red">{{$date_start}}</font> ถึง <font color="red">{{$date_end}} </font>
                                                @else
                                                     ข้อมูล วันที่ <font color="red">{{$date_start}}</font> เวลา
                                                                <font color="red">00:00</font> ถึง <font color="red">23:59</font>     
                                                @endif

                                                
                                            </p>
                                            @else
                                             
                                            @endif
                                        <div id="main_chart2" class="mea_chart" style="width: 100%; padding: 0px; display:block;">
                                        </div>  
                                    </div> 



                                </div>
                                <!-- end widget -->

                            </div>
                         

                        </div>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->



        </article>
        <!-- WIDGET END -->



    </div>



</div>
<!-- END MAIN CONTENT -->


<!-- ECHART english -->
<script src="{{asset('backend/js/plugin/echart/www/js/echarts-all-english-v2.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('backend/js/plugin/echart/www/js/style.css')}}" />





<script type="text/javascript">
    
    /**
     * EChart theme
     */
    var theme1 = {
        backgroundColor: '#FFFFFF',//'#F2F2E6',
        //  
        color: [
            '#04b8ce', '#15b829', '#15b829','#8AED35','#ef3430','#8AED35','#30b193','#8AED35','#F4E24E','#FE9616','#ff69b4',
            '#E42B6D','#ba55d3','#cd5c5c','#ffa500','#40e0d0',
            '#E95569','#ff6347','#7b68ee','#00fa9a','#ffd700',
            '#6699FF','#ff6666','#3cb371','#b8860b','#30e0e0'
        ],

        //  
        title: {
            //backgroundColor: '#FFFFFF', //'#F2F2E6',
            itemGap: 0 ,                
            textStyle: {
               // color: '#000', //'#8A826D',
                fontFamily : 'DB Ozone X'
            },
            subtextStyle: {
                color: '#8A826D',//'#E877A3',
                fontFamily : 'DB Ozone X'           
            }
        },

        dataRange: {
            x:'right',
            y:'center',
            itemWidth: 5,
            itemHeight:25,
            color:['#E42B6D','#F9AD96'],
            text:['高','低'],          
            textStyle: {
                color: '#8A826D'           
            }
        },

        toolbox: {
            color : ['#E95569','#E95569','#E95569','#E95569'],
            effectiveColor : '#ff4500',
            itemGap: 8
        },

        tooltip: {
            backgroundColor: 'rgba(100,100,79,0.8)', //'rgba(138,130,109,0.5)',      
            axisPointer : {             
                type : 'line',          
                lineStyle : {           
                    color: '#6B6455',
                    type: 'dashed'
                },
                crossStyle: {           
                    color: '#A6A299'
                },
                shadowStyle : {                      
                    color: 'rgba(200,200,200,0.3)'
                }
            }
        },

     
        dataZoom: {
            dataBackgroundColor: 'rgba(130,197,209,0.5)',            
            fillerColor: 'rgba(233,84,105,0.1)',    
            handleColor: 'rgba(107,99,84,0.8)'     
        },

         
        grid: {
            borderWidth:0
        },

       
        categoryAxis: {
            axisLine: {             
                lineStyle: {        
                    color: '#6B6455'
                }
            },
            splitLine: {            
                show: false
            }
        },

         
        valueAxis: {
            axisLine: {             
                show: false
            },
            splitArea : {
                show: false
            },
            splitLine: {            
                lineStyle: {        
                    color: ['#FFF'],
                    type: 'dashed'
                }
            }
        },

        polar : {
            axisLine: {             
                lineStyle: {        
                    color: '#ddd'
                }
            },
            splitArea : {
                show : true,
                areaStyle : {
                    color: ['rgba(250,250,250,0.2)','rgba(200,200,200,0.2)']
                }
            },
            splitLine : {
                lineStyle : {
                    color : '#ddd'
                }
            }
        },

        timeline : {
            lineStyle : {
                color : '#6B6455'
            },
            controlStyle : {
                normal : { color : '#6B6455'},
                emphasis : { color : '#6B6455'}
            },
            symbol : 'emptyCircle',
            symbolSize : 3
        },

      
        bar: {
            itemStyle: {
                normal: {
                    barBorderRadius: 0
                },
                emphasis: {
                    barBorderRadius: 0
                }
            }
        },

     
        line: {
            smooth : true,
            symbol: 'emptyCircle',   
            symbolSize: 3            
        },


         
        k: {
            itemStyle: {
                normal: {
                    color: '#E42B6D',       
                    color0: '#44B7D3',       
                    lineStyle: {
                        width: 1,
                        color: '#E42B6D',    
                        color0: '#44B7D3'    
                    }
                }
            }
        },

        scatter: {
            itemStyle: {
                normal: {
                    borderWidth:1,
                    borderColor:'rgba(200,200,200,0.5)'
                },
                emphasis: {
                    borderWidth:0
                }
            },
            symbol: 'circle',   
            symbolSize: 4        
        },

         
        radar : {
            symbol: 'emptyCircle',     
            symbolSize:3 
        },

        map: {
            itemStyle: {
                normal: {
                    areaStyle: {
                        color: '#ddd'
                    },
                    label: {
                        textStyle: {
                            color: '#E42B6D'
                        }
                    }
                },
                emphasis: {                 
                    areaStyle: {
                        color: '#fe994e'
                    },
                    label: {
                        textStyle: {
                            color: 'rgb(100,0,0)'
                        }
                    }
                }
            }
        },

        force : {
            itemStyle: {
                normal: {
                    nodeStyle : {
                        borderColor : 'rgba(0,0,0,0)'
                    },
                    linkStyle : {
                        color : '#6B6455'
                    }
                }
            }
        },

        chord : {
            itemStyle : {
                normal : {
                    chordStyle : {
                        lineStyle : {
                            width : 0,
                            color : 'rgba(128, 128, 128, 0.5)'
                        }
                    }
                },
                emphasis : {
                    chordStyle : {
                        lineStyle : {
                            width : 1,
                            color : 'rgba(128, 128, 128, 0.5)'
                        }
                    }
                }
            }
        },

        gauge : {                  
            center:['50%','80%'],
            radius:'100%',
            startAngle: 180,
            endAngle : 0,
            axisLine: {            
                show: true,        
                lineStyle: {       
                    color: [[0.2, '#44B7D3'],[0.8, '#6B6455'],[1, '#E42B6D']],
                    width: '40%'
                }
            },
            axisTick: {            
                splitNumber: 2,    
                length: 5,         
                lineStyle: {       
                    color: '#fff'
                }
            },
            axisLabel: {           
                textStyle: {       
                    color: '#fff',
                    fontWeight:'bolder'
                }
            },
            splitLine: {           
                length: '5%',      
                lineStyle: {       
                    color: '#fff'
                }
            },
            pointer : {
                width : '40%',
                length: '80%',
                color: '#fff'
            },
            title : {
              offsetCenter: [0, -20],        
              textStyle: {        
                color: 'auto',
                fontSize: 20
              }
            },
            detail : {
                offsetCenter: [0, 0],       
                textStyle: {        
                    color: 'auto',
                    fontSize: 40
                }
            }
        }
    };




    var theme2 = {
        backgroundColor: '#FFFFFF',
        //  
        color: [
            '#04b8ce', '#15b829', '#15b829','#8AED35','#ef3430','#8AED35','#30b193','#8AED35','#F4E24E','#FE9616','#ff69b4',
            '#E42B6D','#ba55d3','#cd5c5c','#ffa500','#40e0d0',
            '#E95569','#ff6347','#7b68ee','#00fa9a','#ffd700',
            '#6699FF','#ff6666','#3cb371','#b8860b','#30e0e0'
        ],

        title: {
            itemGap: 0 ,                
            textStyle: {
                fontFamily : 'DB Ozone X'
            },
            subtextStyle: {
                color: '#8A826D',
                fontFamily : 'DB Ozone X'           
            }
        },

        dataRange: {
            x:'right',
            y:'center',
            itemWidth: 5,
            itemHeight:25,
            color:['#E42B6D','#F9AD96'],
            text:['高','低'],          
            textStyle: {
                color: '#8A826D'           
            }
        },

        toolbox: {
            color : ['#E95569','#E95569','#E95569','#E95569'],
            effectiveColor : '#ff4500',
            itemGap: 8
        },

        tooltip: {
            backgroundColor: 'rgba(100,100,79,0.8)',      
            axisPointer : {             
                type : 'line',          
                lineStyle : {           
                    color: '#6B6455',
                    type: 'dashed'
                },
                crossStyle: {           
                    color: '#A6A299'
                },
                shadowStyle : {                      
                    color: 'rgba(200,200,200,0.3)'
                }
            }
        },

     
        dataZoom: {
            dataBackgroundColor: 'rgba(130,197,209,0.5)',            
            fillerColor: 'rgba(233,84,105,0.1)',    
            handleColor: 'rgba(107,99,84,0.8)'     
        },
         
        grid: {
            borderWidth:0
        },

        categoryAxis: {
            axisLine: {             
                lineStyle: {        
                    color: '#6B6455'
                }
            },
            splitLine: {            
                show: false
            }
        },

         
        valueAxis: {
            axisLine: {             
                show: false
            },
            splitArea : {
                show: false
            },
            splitLine: {            
                lineStyle: {        
                    color: ['#FFF'],
                    type: 'dashed'
                }
            }
        },

        polar : {
            axisLine: {             
                lineStyle: {        
                    color: '#ddd'
                }
            },
            splitArea : {
                show : true,
                areaStyle : {
                    color: ['rgba(250,250,250,0.2)','rgba(200,200,200,0.2)']
                }
            },
            splitLine : {
                lineStyle : {
                    color : '#ddd'
                }
            }
        },

        timeline : {
            lineStyle : {
                color : '#6B6455'
            },
            controlStyle : {
                normal : { color : '#6B6455'},
                emphasis : { color : '#6B6455'}
            },
            symbol : 'emptyCircle',
            symbolSize : 3
        },

      
        bar: {
            itemStyle: {
                normal: {
                    barBorderRadius: 0
                },
                emphasis: {
                    barBorderRadius: 0
                }
            }
        },

     
        line: {
            smooth : true,
            symbol: 'emptyCircle',   
            symbolSize: 3            
        },

         
        k: {
            itemStyle: {
                normal: {
                    color: '#E42B6D',       
                    color0: '#44B7D3',       
                    lineStyle: {
                        width: 1,
                        color: '#E42B6D',    
                        color0: '#44B7D3'    
                    }
                }
            }
        },

        scatter: {
            itemStyle: {
                normal: {
                    borderWidth:1,
                    borderColor:'rgba(200,200,200,0.5)'
                },
                emphasis: {
                    borderWidth:0
                }
            },
            symbol: 'circle',   
            symbolSize: 4        
        },

         
        radar : {
            symbol: 'emptyCircle',     
            symbolSize:3 
        },

        map: {
            itemStyle: {
                normal: {
                    areaStyle: {
                        color: '#ddd'
                    },
                    label: {
                        textStyle: {
                            color: '#E42B6D'
                        }
                    }
                },

                emphasis: {                 
                    areaStyle: {
                        color: '#fe994e'
                    },
                    label: {
                        textStyle: {
                            color: 'rgb(100,0,0)'
                        }
                    }
                }
            }
        },

        force : {
            itemStyle: {
                normal: {
                    nodeStyle : {
                        borderColor : 'rgba(0,0,0,0)'
                    },
                    linkStyle : {
                        color : '#6B6455'
                    }
                }
            }
        },

        chord : {
            itemStyle : {
                normal : {
                    chordStyle : {
                        lineStyle : {
                            width : 0,
                            color : 'rgba(128, 128, 128, 0.5)'
                        }
                    }
                },
                emphasis : {
                    chordStyle : {
                        lineStyle : {
                            width : 1,
                            color : 'rgba(128, 128, 128, 0.5)'
                        }
                    }
                }
            }
        },

        gauge : {                  
            center:['50%','80%'],
            radius:'100%',
            startAngle: 180,
            endAngle : 0,
            axisLine: {            
                show: true,        
                lineStyle: {       
                    color: [[0.2, '#44B7D3'],[0.8, '#6B6455'],[1, '#E42B6D']],
                    width: '40%'
                }
            },
            axisTick: {            
                splitNumber: 2,    
                length: 5,         
                lineStyle: {       
                    color: '#fff'
                }
            },
            axisLabel: {           
                textStyle: {       
                    color: '#fff',
                    fontWeight:'bolder'
                }
            },
            splitLine: {           
                length: '5%',      
                lineStyle: {       
                    color: '#fff'
                }
            },
            pointer : {
                width : '40%',
                length: '80%',
                color: '#fff'
            },
            title : {
              offsetCenter: [0, -20],        
              textStyle: {        
                color: 'auto',
                fontSize: 20
              }
            },
            detail : {
                offsetCenter: [0, 0],       
                textStyle: {        
                    color: 'auto',
                    fontSize: 40
                }
            }
        }
    };
    ////////////////////////////

    $(document).ready(function(){
        $('#export_search').on('click',function() {

            var PageSizeAll = $('#page-size-search').val();
            var reporttype  = $('#reporttype').val();
            var category    = $('#category').val();
            var equity_name = $('#equity_name').val();
            var choose_year = $('#choose_year').val();
            // var url="BenefitsReport/exportsearch?reporttype=" + reporttype +"&category=" + category +
            //                         "&equity_name=" + equity_name + "&choose_year=" +choose_year;
            var url="BenefitsReport/export?reporttype=" + reporttype +"&category=" + category +
                                    "&equity_name=" + equity_name + "&choose_year=" +choose_year;

            if(reporttype == "") {
                 Alert('Error', "ท่านยังไม่ได้เลือกรายงาน ");
                return false;
            }
            //alert(url);
            window.location.href =   url;
            return false;
        });

        //meaDatepicker("date_start","date_end");
        //meaDatepicker("date_end");

        $('html').append('<input type="hidden" value="1" id="currentpage_search" />');

        $('#btn_search').on('click',function(e) {
             $("body").trigger("click");
            var PageSizeAll = $('#page-size-search').val();
            var reporttype  = $('#reporttype').val();
            var category    = $('#category').val();
            var equity_name = $('#equity_name').val();
            var choose_year = $('#choose_year').val();
        
            if(reporttype == "") {
                 Alert('Error', "ท่านยังไม่ได้เลือกรายงาน ");
                return false;
            }

            var jsondata = {
                pagesize : PageSizeAll,
                PageNumber:1,
                reporttype :reporttype,
                category : category,
                equity_name :equity_name,
                choose_year:choose_year
            };

            console.log(jsondata);
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            switch(reporttype) {
               case "":
                  break;
               case "1":  
                  MeaAjax(jsondata,"BenefitsReport/profitsearch", RenderSearch);
                  break;
               case "2":  
                  MeaAjax(jsondata,"BenefitsReport/dividendsearch", RenderSearch);     
                  break;         
               case "3":  
                  MeaAjax(jsondata,"BenefitsReport/gainlosssearch", RenderSearch);
                  break;
            } 
            return false;

        });


        $("#page-size-search").on('change',function() {
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            var val = $(this).val();
           

            var PageSizeAll = $('#page-size-search').val();
            var reporttype  = $('#reporttype').val();
            var category    = $('#category').val();
            var equity_name = $('#equity_name').val();
            var choose_year = $('#choose_year').val();
            
        
            if(reporttype == "") {
                 Alert('Error', "ท่านยังไม่ได้เลือกรายงาน ");
                 
                return false;
            }
            
            var jsondata = {
                pagesize : val,
                PageNumber:1,
                   
                reporttype :reporttype,
                category : category,
                equity_name :equity_name,
                choose_year:choose_year

            };
//            var jsondata = {pagesize : val,PageNumber:1};
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            //MeaAjax(jsondata,"BenefitsReport/search",RenderSearch);
             switch(reporttype) {
               case "":
                  break;
               case "1":  
                  MeaAjax(jsondata,"BenefitsReport/profitsearch", RenderSearch);
                  break;
               case "2":  
                  MeaAjax(jsondata,"BenefitsReport/dividendsearch", RenderSearch);     
                  break;         
               case "3":  
                  MeaAjax(jsondata,"BenefitsReport/gainlosssearch", RenderSearch);
                  break;
            } 

        });

        // <!-- Toggle -->
        $('#btn_chart').on('click',function(e) { 
            
            showByFlag(document.getElementById('page-size-search').id, false);
            showByFlag(document.getElementById('widget_search_table').id, false); 
            
         /*   var EmpID = $('#name').val();
            var depart = $('#depart').val();
            var plan = $('#plan').val();

            // @@ IMPORTANT: don't use #date_start @@ //
            var date_start = $('#hd_date_start').val();
            var date_end = $('#hd_date_end').val();
*/
            $("body").trigger("click");
            // Alert('Clicked', "#btn_search ");
            var PageSizeAll = $('#page-size-search').val();
            var reporttype  = $('#reporttype').val();
            var category    = $('#category').val();
            var equity_name = $('#equity_name').val();
            var choose_year = $('#choose_year').val();
            
        
            if(reporttype == "") {
                 Alert('Error', "ท่านยังไม่ได้เลือกรายงาน ");
                return false;
            }

            var jsondata = {
                pagesize : PageSizeAll,
                PageNumber:1,
                reporttype :reporttype,
                category : category,
                equity_name :equity_name,
                choose_year:choose_year
            };

         /*   var jsondata = {   
                reporttype :reporttype,
                category : category,
                equity_name :equity_name,
                choose_year:choose_year
            };
*/
            //console.log('MEAAJAX--> ' + jsondata);
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(jsondata, "BenefitsReport/chartsearch", RenderChart);

        /*

            if(document.getElementById('main_chart2').id) {
                showByFlag(document.getElementById('page-size-search').id, false);
                showByFlag(document.getElementById('widget_search_table').id, false);
                showByFlag(document.getElementById('widget_bar_chart_two').id, false);
                
            }

    
            if(document.getElementById('main_chart1').id) {
                showByFlag(document.getElementById('page-size-search').id, false);
                showByFlag(document.getElementById('widget_search_table').id, false);
                showByFlag(document.getElementById('widget_bar_chart').id, true);
                
            }*/ 
            return false;
        });  // btn_chart  

    });  // jquery ready


    function RenderChart(json) {

        var title  = '{{getMenutitle($arrSidebar)}}';
        console.log('RenderChart');

        console.log(json.result);

      /*   
        var subtitle = ''; 
        if(json.result.date_start && json.result.date_end && (json.result.date_start != json.result.date_end)) {
            subtitle = 'ข้อมูล ระหว่าง วันที่ ' + json.result.date_start + ' ถึง ' + json.result.date_end;
        } else if(json.result.date_start){
            subtitle = 'ข้อมูล วันที่ ' + json.result.date_start + ' เวลา 00:00 ถึง 23:59';
        } else {
            subtitle = 'รายงานข้อมูล ณ วันที่ {{get_date_notime(date("Y-m-d H:i:s"))}}';
        }
*/
        subtitle   = json.result.pretty_date;
     //   var datasource = json.result.data;
  /*      var labels = json.result.labels;
        */
        renderBenefitsChart1( title  + ' แยกตาม กลุ่มอุตสาหกรรม', subtitle,  json.result);
        renderBenefitsChart2( title  + ' แยกตาม หมวดธุรกิจ', subtitle,  json.result);
    }


    function RenderSearch(data){

        $("#serch_data").html(data.html);
        $("#serch_data").fadeIn('300');
        $("#page_click_search li a").on('click',PageRenderSearch);

        showByFlag(document.getElementById('widget_bar_chart').id, false); 
        showByFlag(document.getElementById('page-size-search').id, true);
        showByFlag(document.getElementById('widget_search_table').id, true); 


        if(document.getElementById('main_chart2').id) {
               // showByFlag(document.getElementById('page-size-search').id, false);
               // showByFlag(document.getElementById('widget_search_table').id, false);
                showByFlag(document.getElementById('widget_bar_chart_two').id, false);
                
        }
    
        if(document.getElementById('main_chart1').id) {
                //showByFlag(document.getElementById('page-size-search').id, false);
                //showByFlag(document.getElementById('widget_search_table').id, false);
                showByFlag(document.getElementById('widget_bar_chart').id, false);
                
        } 
    }

    function PageRenderSearch(){

        var p = $(this).attr('data-page');
        var page_size = $('#page-size-search').val();
        var CurPage = $('#currentpage_search').val();
        $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');

        if(p == "pre"){
            p = parseInt(CurPage) - 1;
        }

        if(p == "next"){
            p = parseInt(CurPage) + 1;
        }
        $('#currentpage_search').val(p);

      
        var reporttype = $('#reporttype').val();
        var category = $('#category').val();
        var equity_name = $('#equity_name').val();
        var choose_year =$('#choose_year').val();

         


        var jsondata = {
            pagesize : page_size,
            PageNumber:p,
            reporttype :reporttype,
            category : category,
            equity_name :equity_name,
            choose_year:choose_year

        };

        //MeaAjax(jsondata,"BenefitsReport/search",RenderSearch);
        switch(reporttype) {
               case "":
                  break;
               case "1":  
                  MeaAjax(jsondata,"BenefitsReport/profitsearch", RenderSearch);
                  break;
               case "2":  
                  MeaAjax(jsondata,"BenefitsReport/dividendsearch", RenderSearch);     
                  break;         
               case "3":  
                  MeaAjax(jsondata,"BenefitsReport/gainlosssearch", RenderSearch);
                  break;
        } 
        return false;
    };

   
    function renderBenefitsChart1(title, subtitle, ds)  
    {
        var myChart1;
        var domCode = document.getElementById('sidebar-code');
        var domGraphic = document.getElementById('graphic');
       
        var domMessage = document.getElementById('wrong-message');
        var iconResize = document.getElementById('icon-resize');
        var needRefresh = false;
       
        var idx = 1;
        var isExampleLaunched;
        var curTheme;
        var option;

        var data1 = [];
        var data2 = [];

        var tagid = "main_chart1";
        var domMain = document.getElementById(tagid);
        var datasource = ds.data;
        var lseries = [];

        var trading_label = [];
        var lengend = [];

        if(datasource.length <=0) {
           
            if(document.getElementById('main_chart1').id) {
                showByFlag(document.getElementById('page-size-search').id, false);
                showByFlag(document.getElementById('widget_search_table').id, false);
                showByFlag(document.getElementById('widget_bar_chart').id, false);
                
            } 
            Alert('คำเตือน', 'ไม่พบข้อมูล');
            return;

        } else {

            /*if(document.getElementById('main_chart2').id) {
                showByFlag(document.getElementById('page-size-search').id, false);
                showByFlag(document.getElementById('widget_search_table').id, false);
                showByFlag(document.getElementById('widget_bar_chart_two').id, true);
                
            }*/
    
            if(document.getElementById('main_chart1').id) {
                showByFlag(document.getElementById('page-size-search').id, false);
                showByFlag(document.getElementById('widget_search_table').id, false);
                showByFlag(document.getElementById('widget_bar_chart').id, true);
                
            } 

        }
        for(var i=0; i<datasource.length; i++) {
            
            // convert to ล้านบาท
            if(ds.equity_name === 'KTAM') {
                trading_label.push(datasource[i].INDUS_SHT);
                data1.push((datasource[i].KTAM / 1000000).toFixed(2));
                lengend = ['KTAM'];
                lseries = [
                {
                    name: 'KTAM',
                    type: 'bar',
                    data: data2,
                    barWidth: 30,
                    barGap:"10%",
                    formatter: function (value) {
                        return (value).toFixed(2).toLocaleString() + ' ล้านบาท';
                    },

                    itemStyle: {
                        normal: {
                            label : {
                                show: true, 
                                position: 'top'
                                , //'insideTop'
                                textStyle: {
                                    color: '#15b829',
                                    fontFamily : 'DB Ozone X',
                                    fontSize : 14
                                }
                            }
                        }
                    },
                }];

            }  else if(ds.equity_name === 'UOBAM') {
                trading_label.push(datasource[i].INDUS_SHT);
                data1.push((datasource[i].UOB_SUM / 1000000).toFixed(2));
                lengend = ['UOBAM'] ;
                lseries = [{
                    name: 'UOBAM',
                    type: 'bar',
                    data: data1,
                    barWidth: 30, 
                    barGap:"10%",
    
                    formatter: function (value) {
                        return (value).toFixed(2).toLocaleString() + ' ล้านบาท';
                    },
                    
                    itemStyle: {
                        normal: {
                            label : {
                                show: true, 
                                position: 'top', //'insideTop'
                                textStyle: {
                                    color: '#04b8ce',
                                    fontFamily : 'DB Ozone X',
                                    fontSize : 14
                                }
                            }
                        }
                    },
                }];

            } else  {

                trading_label.push(datasource[i].INDUS_SHT);
                data1.push( (datasource[i].UOB_SUM / 1000000).toFixed(2) );
                data2.push( (datasource[i].KTAM_SUM / 1000000).toFixed(2) );
                lengend = ['UOBAM','KTAM'] ;
                lseries = [
                {
                    name: 'UOBAM',
                    type: 'bar',
                    data: data1,
                    barWidth: 30, 
                    barGap:"10%",
                    formatter: function (value) {
                        return (value).toFixed(2).toLocaleString() + ' ล้านบาท';
                    },
                    itemStyle: {
                        normal: {
                            
                            label : {
                                
                                show: true, 
                                position: 'top', //'insideTop'
                                textStyle: {
                                    color: '#04b8ce',
                                    fontFamily : 'DB Ozone X',
                                    fontSize : 14
                                }
                            }
                        }
                    },
                },

                {
                    name: 'KTAM',
                    type: 'bar',
                    data: data2,
                    barWidth: 30,
                    barGap:"10%",
                    formatter: function (value) {
                        return (value).toFixed(2).toLocaleString() + ' ล้านบาท';
                    },

                    itemStyle: {
                        normal: {
                            label : {
                                show: true, 
                                position: 'top'
                                , //'insideTop'
                                textStyle: {
                                    color: '#15b829',
                                    fontFamily : 'DB Ozone X',
                                    fontSize : 14
                                }
                            }
                        }
                    },
                }];
            }
        }
       
        var yasix_label = 'จำนวณ (ล้านบาท)';
      /*  var captions = ['','ม.ค.','ก.พ','มี.ค.', 'เม.ย', 'พ.ค.', 'มิ.ย.',
                        'ก.ค','ส.ค.','ก.ย.','ต.ค.','พ.ย','ธ.ค.',
                        'รวม'
                       ];*/

        option = {
            title : {
                text: title,
                subtext: subtitle,  

                x:'center',
                textStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 23
                },
                subtextStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 18
                }
            },

            tooltip : {
                trigger: 'axis',
                axisPointer : {             
                    type : 'shadow'     
                },
                formatter: function (params) {
                    if(ds.equity_name =='UOBAM') {
                        return params[0].name + '<br/>'
                         + params[0].seriesName + ' : ' + (params[0].value-0).toFixed(2).toLocaleString() + ' ล้านบาท <br/>';
                     
                    } else if(ds.equity_name =='KTAM') {
                        return params[0].name + '<br/>'
                         + params[0].seriesName + ' : ' + (params[0].value-0).toFixed(2).toLocaleString() + ' ล้านบาท <br/>';
                     
                    } else {
                         return params[0].name + '<br/>'
                         + params[0].seriesName + ' : ' + (params[0].value-0).toFixed(2).toLocaleString() + ' ล้านบาท <br/>'
                         + params[1].seriesName + ' : ' + (params[1].value-0).toFixed(2).toLocaleString() + ' ล้านบาท';
                    }
                }
            },

            label : {
                /*formatter : function(s) {
                    return s.slice(0, 4);
                }*/
                
                formatter: function (value){
                     return (value).toFixed(2).toLocaleString() + ' ล้านบาท';
                     //+ params[0].seriesName + ' : ' + (params[0].value-0).toFixed(2).toLocaleString() + 'ล้านบาท <br/>'
                     //+ params[1].seriesName + ' : ' + (params[1].value-0).toFixed(2).toLocaleString() + 'ล้านบาท';
                }
            },

            grid: {
               borderColor: '#ccc'
            },

            legend: {
                //orient : 'vertical',
                x : 'left',
                y : 'top',
                textStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 14
                },
                data: lengend //['ซื้อ','ขาย'] 
            },

            toolbox: {
                    show : true,
                    orient : 'vertical',
                    x: 'right', 
                    y: 'top',
                    feature : {
                        mark : {show: false},
                        dataView : {show: true, readOnly: true},
                        magicType : {
                            show: true, 
                            type: ['line', 'bar'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1700
                                }
                            }
                        },
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },

            //calculable : true,

            xAxis : [
                {
                    name :'กลุ่มอุตสาหกรรม', 
                    nameTextStyle: {
                            color: '#000000',
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        },
                    type : 'category',
                    show : true,
                    data : trading_label, 

                    axisLine: {
                        show : true
                    },
                    axisTick: {
                        show: true,
                        interval : 0,
                        length: 1
                    },
                    axisLabel: {
                        show: true,
                        interval: 0,
                        textStyle: {
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        }
                    },

                }
            ],

            yAxis : [
                {
                    name : 'จำนวณเงิน (ล้านบาท)',
                    nameTextStyle: {
                             color: '#000000',
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        },

                    type : 'value',
                    show : true,
                    boundaryGap: [0, 0.1],
                    
                    axisLabel: {
                        show: true,
                        interval: 0,
                        textStyle: {
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        }
                    },
                }
            ],

            series : lseries
            /*
            series : [
                {
                    name: 'UOBAM',
                    type: 'bar',
                    data: data1,
                    barWidth: 30, 
                    barGap:"10%",
    
                    formatter: function (value) {
                        return (value).toFixed(2).toLocaleString() + ' ล้านบาท';
                    },
                    

                    itemStyle: {
                        normal: {
                            
                            label : {
                                
                                show: true, 
                                position: 'top', //'insideTop'
                                textStyle: {
                                    color: '#04b8ce',
                                    fontFamily : 'DB Ozone X',
                                    fontSize : 14
                                }
                            }
                        }
                    },
                    
                },
                {
                    name: 'KTAM',
                    type: 'bar',
                    data: data2,
                    barWidth: 30,
                    barGap:"10%",
                    formatter: function (value) {
                        return (value).toFixed(2).toLocaleString() + ' ล้านบาท';
                    },
                    

                    itemStyle: {
                        normal: {
                            label : {
                                show: true, 
                                position: 'top'
                                , //'insideTop'
                                textStyle: {
                                    color: '#d32f2f',
                                    fontFamily : 'DB Ozone X',
                                    fontSize : 14
                                }
                            }
                        }
                    },
                }
            ]*/

        }; // EChart option

        myChart1 = echarts.init(document.getElementById(tagid));
        myChart1.setOption(option);
  
        curTheme = theme1;
        myChart1.setTheme(curTheme);

        /*// Handle chart ckick
        myChart1.on('click', function (param) {
            var mes = '[' + param.type + ']';
            if (typeof param.seriesIndex != 'undefined') {
                mes += '  seriesIndex : ' + param.seriesIndex;
                mes += '  dataIndex : ' + param.dataIndex;
            }
            

            // print  param.name => 'AGRO'
            console.log(param.name);
            // TODO: POST to WS:
            var industrial = $('#industrial').val();
            var EmpID = $('#name').val();
            var depart = $('#depart').val();
            var plan = $('#plan').val();

            var name_sht    = $('#securities_name').val();   // "UOBAM"
            var symbol      = $('#symbol').val();           // "APCS"
            var array       = symbol.split("|"); 
            if(array.length > 0)
               symbol = array[0].trim();

            var industrial  = $('#industrial').val();        // CATE_ID --> 21 

            // @@ IMPORTANT: don't use #date_start @@ //
            var date_start = $('#hd_date_start').val();
            var date_end =$('#hd_date_end').val();

            var jsondata = {
                emp_id : EmpID,
                depart :depart,
                plan : plan,
                date_start: date_start,
                date_end: date_end,
        
                symbol : symbol,
                name_sht : name_sht,       
                industrial : industrial, // CATE 21 
                indus_name : param.name  // eg.AGRO
            };

            console.log(jsondata);
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(jsondata,"EquityTradingReport/BusinessChart", function(json){
                var title    = '{{getMenutitle($arrSidebar)}} ของกลุ่มอุตสาหกรรม [ ' + param.name + ' ]';
        
                console.log(json.result);
                var subtitle = ''; 
                if(json.result.date_start && json.result.date_end && (json.result.date_start != json.result.date_end)) {
                    subtitle = 'ข้อมูล ระหว่าง วันที่ ' + json.result.date_start + ' ถึง ' + json.result.date_end;
                } else if(json.result.date_start){
                    subtitle = 'ข้อมูล วันที่ ' + json.result.date_start + ' เวลา 00:00 ถึง 23:59';
                } else {
                    subtitle = 'รายงานข้อมูล ณ วันที่ {{get_date_notime(date("Y-m-d H:i:s"))}}';
                }

                subtitle   = json.result.pretty_date;
                console.log('DATASOURCE=' + json.result.data);
                console.log('BU LIST=' + json.result.bu_list);
                for(var i=0; i<json.result.bu_list.length; i++) {

                    console.log(json.result.bu_list[i].BU);
                }

                if(json.result.data) {
                   renderTradingChart2('main_chart2', title , subtitle, json.result.data);
                   $("html, body").animate({ scrollTop: $(document).height() }, "slow");
                } else {
                   var divChart =  document.getElementById('main_chart2')
                   Alert("ERROR", "ไม่พบข้อมูล", null, null);
                }

            });
       });*/
    } // Echart
   

   function renderBenefitsChart2(title, subtitle, ds)  
    {
        var myChart2;
        var domCode    = document.getElementById('sidebar-code');
        var domGraphic  = document.getElementById('graphic');
       
        var domMessage  = document.getElementById('wrong-message');
        var iconResize  = document.getElementById('icon-resize');
        var needRefresh = false;
       
        var idx = 1;
        var isExampleLaunched;
        var curTheme;
        var option;

        var data1 = [];
        var data2 = [];

        var tagid = "main_chart2";
        var domMain = document.getElementById(tagid);
        var datasource = ds.bu_data;
        var lseries = [];
        //var equity_name = $('#equity_name').val();

        var trading_label = [];
        var lengend = [];

        if(datasource.length <=0) {

            if(document.getElementById('main_chart2').id) {
                showByFlag(document.getElementById('page-size-search').id, false);
                showByFlag(document.getElementById('widget_search_table').id, false);
                showByFlag(document.getElementById('widget_bar_chart_two').id, false);
            } 
            Alert('คำเตือน', 'ไม่พบข้อมูล');
            return;

        } else {

            if(document.getElementById('main_chart2').id) {
                showByFlag(document.getElementById('page-size-search').id, false);
                showByFlag(document.getElementById('widget_search_table').id, false);
                showByFlag(document.getElementById('widget_bar_chart_two').id, true);
            }
    
        }
        for(var i=0; i<datasource.length; i++) {
            
            // convert to ล้านบาท
            if(ds.equity_name === 'KTAM') {
                trading_label.push(datasource[i].BU_SHT);
                data1.push((datasource[i].KTAM / 1000000).toFixed(2));
                lengend = ['KTAM'];
                lseries = [
                {
                    name: 'KTAM',
                    type: 'bar',
                    data: data2,
                    barWidth: 30,
                    barGap:"10%",
                    formatter: function (value) {
                        return (value).toFixed(2).toLocaleString() + ' ล้านบาท';
                    },

                    itemStyle: {
                        normal: {
                            label : {
                                show: true, 
                                position: 'top',
                                textStyle: {
                                    color: '#15b829',
                                    fontFamily : 'DB Ozone X',
                                    fontSize : 14
                                }
                            }
                        }
                    },
                }];

            }  else if(ds.equity_name === 'UOBAM') {
                trading_label.push(datasource[i].BU_SHT);
                data1.push((datasource[i].UOB_SUM / 1000000).toFixed(2));
                lengend = ['UOBAM'] ;
                lseries = [{
                    name: 'UOBAM',
                    type: 'bar',
                    data: data1,
                    barWidth: 30, 
                    barGap: "10%",
                    formatter: function (value) {
                        return (value).toFixed(2).toLocaleString() + ' ล้านบาท';
                    },
                    
                    itemStyle: {
                        normal: {
                            label : {
                                show: true, 
                                position: 'top',  
                                textStyle: {
                                    color: '#04b8ce',
                                    fontFamily : 'DB Ozone X',
                                    fontSize : 14
                                }
                            }
                        }
                    },
                }];

            } else  {

                trading_label.push(datasource[i].BU_SHT);
                data1.push( (datasource[i].UOB_SUM / 1000000).toFixed(2) );
                data2.push( (datasource[i].KTAM_SUM / 1000000).toFixed(2) );
                lengend = ['UOBAM','KTAM'] ;
                lseries = [
                {
                    name: 'UOBAM',
                    type: 'bar',
                    data: data1,
                    barWidth: 30, 
                    barGap:"10%",
                    formatter: function (value) {
                        return (value).toFixed(2).toLocaleString() + ' ล้านบาท';
                    },
                    itemStyle: {
                        normal: {
                            label : {
                                show: true, 
                                position: 'top', 
                                textStyle: {
                                    color: '#04b8ce',
                                    fontFamily : 'DB Ozone X',
                                    fontSize : 14
                                }
                            }
                        }
                    },
                },

                {
                    name: 'KTAM',
                    type: 'bar',
                    data: data2,
                    barWidth: 30,
                    barGap:"10%",
                    formatter: function (value) {
                        return (value).toFixed(2).toLocaleString() + ' ล้านบาท';
                    },

                    itemStyle: {
                        normal: {
                            label : {
                                show: true, 
                                position: 'top',
                                textStyle: {
                                    color: '#15b829',
                                    fontFamily : 'DB Ozone X',
                                    fontSize : 14
                                }
                            }
                        }
                    },
                }];
            }
        }   

        var yasix_label = 'จำนวณ (ล้านบาท)';
        
        option = {
            title : {
                text: title,
                subtext: subtitle,
                x:'center',
                textStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 23
                },
                subtextStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 18
                }
            },

            tooltip : {
                trigger: 'axis',
                axisPointer : {             
                    type : 'shadow'     
                },
                formatter: function (params) {
                    if(ds.equity_name =='UOBAM') {
                        return params[0].name + '<br/>'
                         + params[0].seriesName + ' : ' + (params[0].value-0).toFixed(2).toLocaleString() + ' ล้านบาท <br/>';
                     
                    } else if(ds.equity_name =='KTAM') {
                        return params[0].name + '<br/>'
                         + params[0].seriesName + ' : ' + (params[0].value-0).toFixed(2).toLocaleString() + ' ล้านบาท <br/>';
                     
                    } else {
                         return params[0].name + '<br/>'
                         + params[0].seriesName + ' : ' + (params[0].value-0).toFixed(2).toLocaleString() + ' ล้านบาท <br/>'
                         + params[1].seriesName + ' : ' + (params[1].value-0).toFixed(2).toLocaleString() + ' ล้านบาท';
                    }
                }
            },

            label : {
                formatter: function (value){
                     return (value).toFixed(2).toLocaleString() + ' ล้านบาท';
                     //+ params[0].seriesName + ' : ' + (params[0].value-0).toFixed(2).toLocaleString() + 'ล้านบาท <br/>'
                     //+ params[1].seriesName + ' : ' + (params[1].value-0).toFixed(2).toLocaleString() + 'ล้านบาท';
                }
            },

            grid: {
                borderColor: '#ccc'
            },

            legend: {
                x : 'left',
                y : 'top',
                textStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 14
                },
                data: lengend  
            },

            toolbox: {
                    show : true,
                    orient : 'vertical',
                    x: 'right', 
                    y: 'top',
                    feature : {
                        mark : {show: false},
                        dataView : {show: true, readOnly: true},
                        magicType : {
                            show: true, 
                            type: ['line', 'bar'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1700
                                }
                            }
                        },
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },

            xAxis : [
                {
                    name :'หมวดธุรกิจ', 
                    nameTextStyle: {
                            color: '#000000',
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        },
                    type : 'category',
                    show : true,
                    data : trading_label, 

                    axisLine: {
                        show : true
                    },

                    axisTick: {
                        show: true,
                        interval : 0,
                        length: 1
                    },

                    axisLabel: {
                        show: true,
                        interval: 0,
                        textStyle: {
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        }
                    },
                }
            ],

            yAxis : [
                {
                    name : 'จำนวณเงิน (ล้านบาท)',
                    nameTextStyle: {
                             color: '#000000',
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        },

                    type : 'value',
                    show : true,
                    boundaryGap: [0, 0.1],
                    
                    axisLabel: {
                        show: true,
                        interval: 0,
                        textStyle: {
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        }
                    },
                }
            ],
            series : lseries

        }; // EChart option

        myChart2 = echarts.init(document.getElementById(tagid));
        myChart2.setOption(option);

        curTheme = theme2;
        myChart2.setTheme(curTheme);

        /*// Handle chart ckick
        myChart1.on('click', function (param) {
            var mes = '[' + param.type + ']';
            if (typeof param.seriesIndex != 'undefined') {
                mes += '  seriesIndex : ' + param.seriesIndex;
                mes += '  dataIndex : ' + param.dataIndex;
            }
            

            // print  param.name => 'AGRO'
            console.log(param.name);
            // TODO: POST to WS:
            var industrial = $('#industrial').val();
            var EmpID = $('#name').val();
            var depart = $('#depart').val();
            var plan = $('#plan').val();

            var name_sht    = $('#securities_name').val();   // "UOBAM"
            var symbol      = $('#symbol').val();           // "APCS"
            var array       = symbol.split("|"); 
            if(array.length > 0)
               symbol = array[0].trim();

            var industrial  = $('#industrial').val();        // CATE_ID --> 21 

            // @@ IMPORTANT: don't use #date_start @@ //
            var date_start = $('#hd_date_start').val();
            var date_end =$('#hd_date_end').val();

            var jsondata = {
                emp_id : EmpID,
                depart :depart,
                plan : plan,
                date_start: date_start,
                date_end: date_end,
        
                symbol : symbol,
                name_sht : name_sht,       
                industrial : industrial, // CATE 21 
                indus_name : param.name  // eg.AGRO
            };

            console.log(jsondata);
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(jsondata,"EquityTradingReport/BusinessChart", function(json){
                var title    = '{{getMenutitle($arrSidebar)}} ของกลุ่มอุตสาหกรรม [ ' + param.name + ' ]';
        
                console.log(json.result);
                var subtitle = ''; 
                if(json.result.date_start && json.result.date_end && (json.result.date_start != json.result.date_end)) {
                    subtitle = 'ข้อมูล ระหว่าง วันที่ ' + json.result.date_start + ' ถึง ' + json.result.date_end;
                } else if(json.result.date_start){
                    subtitle = 'ข้อมูล วันที่ ' + json.result.date_start + ' เวลา 00:00 ถึง 23:59';
                } else {
                    subtitle = 'รายงานข้อมูล ณ วันที่ {{get_date_notime(date("Y-m-d H:i:s"))}}';
                }

                subtitle   = json.result.pretty_date;
                console.log('DATASOURCE=' + json.result.data);
                console.log('BU LIST=' + json.result.bu_list);
                for(var i=0; i<json.result.bu_list.length; i++) {

                    console.log(json.result.bu_list[i].BU);
                }

                if(json.result.data) {
                   renderTradingChart2('main_chart2', title , subtitle, json.result.data);
                   $("html, body").animate({ scrollTop: $(document).height() }, "slow");
                } else {
                   var divChart =  document.getElementById('main_chart2')
                   Alert("ERROR", "ไม่พบข้อมูล", null, null);
                }

            });
       });*/
    } // Echart
    function showByFlag(id, flag) {
        var e = document.getElementById(id);
        e.style.display = flag ? 'block': 'none' ;

    }
</script>

@stop
