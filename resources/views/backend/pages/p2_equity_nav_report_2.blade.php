@extends('backend.layouts.default')
@section('content')
<?php
$data = getmemulist();
$arrSidebar =getSideBar($data);
?>

<!--link rel="stylesheet" href="{{asset('backend/js/plugin/chartist/chartist.min.css')}}"/> 
<link rel="stylesheet" href="{{asset('backend/js/plugin/chartist/ct-customize.css')}}">
<link rel="stylesheet" href="{{asset('backend/js/plugin/chartist/plugin/chartist-plugin-tooltip.css')}}"/-->
<style type="text/css">
 #graph_label .tickLabel{

    font-size: 100%;
 }
/*
 #flot-placeholder {
    width:650px;
    height:540px;
}*/


</style>
<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>

                {{getMenutitle($arrSidebar)}}

            </h1>
        </div>

    </div>


    <div class="row" id="widget-grid" >

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <ul class="nav nav-tabs pull-left in">

                        <li class="active">

                            <a data-toggle="tab" href="#hr1"> <i class="fa fa-lg fa-arrow-circle-o-down"></i> <span class="hidden-mobile hidden-tablet"> ระบุเงื่อนไขการค้นหา </span> </a>

                        </li>

                        <!--li>
                            <a data-toggle="tab" href="#hr2"> <i class="fa fa-lg fa-arrow-circle-o-up"></i> <span class="hidden-mobile hidden-tablet"> รายงานสรุปของตราสารทุน  </span> </a>
                        </li-->

                    </ul>
                </header>

                <!-- widget div-->
                <div>


                    <!-- widget content -->
                    <div class="widget-body">

                        <div class="tab-content">
                            <div class="tab-pane active" id="hr1">

                                <div class="widget-body no-padding" style="width: 100% ;margin: 0 auto;background-color: #000000;border: 1px solid #E1E8F3">
                                    <header style="color: #fff; height: 40px; line-height: 40px; font-size: 18px; background-color: #a90329;padding-left: 20px;"> กล่องค้นหา </header>

                                    <form  id="comment-form_darkman" class="smart-form" novalidate="novalidate">

                                        <fieldset>
                                            <div class="row">
                                                <section class="col col-4">
                                                    <label class="label"><input type="checkbox" id="check_name"  value="name">   ชื่อหลักทรัพย์</label>
                                                    <label class="input">
                                                        <select name="plan" id="name" class="form-control">
                                                            <option value="">ชื่อหลักทรัพย์</option>
                                                            @if($equitylist)
                                                                @foreach($equitylist as $item)
                                                                    <option value="{{$item->NAME_SHT}}">{{$item->NAME_SHT}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </label>
                                                </section>
                                                <section class="col col-4">
                                                    <label class="label"><input type="checkbox" id="check_depart" value="depart">   หมวดหมู่หลักทรัพย์</label>
                                                    <label class="input">
                                                        
                                                        <select name="depart" id="depart" class="form-control">
                                                            <option value="">หมวดหมู่หลักทรัพย์</option>
                                                            @if($equitycategory)
                                                                @foreach($equitycategory as $item)
                                                                    <option value="{{$item->INDUSTRIAL}}">{{$item->INDUSTRIAL}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>

                                                    </label>
                                                </section>
                                                <section class="col col-4">
                                                    <label class="label"><input type="checkbox" id="check_plan" value="plan">   ชื่อบริษัทฺหลักทรัพย์</label>
                                                    <label class="input">
                                                        <select name="plan" id="plan" class="form-control">
                                                            <option value="">ชื่อบริษัทฺหลักทรัพย์</option>
                                                            @if($equitylist)
                                                                @foreach($equitylist as $item)
                                                                    <option value="{{$item->SECURITIES_NAME}}">{{$item->SECURITIES_NAME}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                        {{--<input type="text" name="plan" id="plan">--}}
                                                    </label>
                                                </section>
                                            </div>

                                            <div class="row">
                                                <section class="col col-4">
                                                    <label class="label"><input type="checkbox" id="check_date" value="date_start"> ระบุช่วงเวลา</label>
                                                    <label class="input"><i class="icon-append fa fa-calendar"></i>
                                                        <input type="text" class="mea_date_picker" id="date_start"  >
                                                    </label>
                                                </section>
                                                <section class="col col-4">
                                                    <label class="label">ถึง</label>
                                                    <label class="input"><i class="icon-append fa fa-calendar"></i>
                                                        <input type="text" class="mea_date_picker" id="date_end" >
                                                    </label>
                                                </section>
                                                <section class="col col-4">
                                                    <button type="submit" id="btn_search" style="padding: 5px 20px 5px 20px; margin-top:20px;font-size: 18px;" name="submit" class="btn btn-primary">
                                                        ค้นหา
                                                    </button>
                                                </section>
                                            </div>
                                        </fieldset>

                                    </form>

                                </div>


                                <a href="#" id="export_search" style="margin-top: 30px;" class="btn btn-labeled btn-success mea-btn-export"> <span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i></span>Export </a>
                                <a href="#" id="btn_customize" style="margin-top: 30px;" class="btn btn-labeled btn-warning"> <span class="btn-label"><i class="glyphicon glyphicon-list"></i></span>Customize </a>
                                <a href="#" id="btn_chart"     style="margin-top: 30px;" class="btn btn-labeled btn-info"> <span class="btn-label"><i class="glyphicon glyphicon-signal"></i></span>Chart </a>
                               

                                <!-- Widget ID (each widget will need unique ID)-->
                                
                                <div class="jarviswidget jarviswidget-color-blueDark" style="margin-top: 5px; display:block;" 
                                     id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
                                    <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                    data-widget-colorbutton="false"
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true"export_search
                                    data-widget-sortable="false"

                                    -->

                                    <select class="form-control mea-pagesize" id="page-size-search">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>

                                    <header>
                                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    </header>

                                    <!-- widget div-->
                                    <div >

                                        <!-- widget edit box -->
                                        <div class="jarviswidget-editbox">
                                            <!-- This area used as dropdown edit box -->

                                        </div>
                                        <!-- end widget edit box -->

                                        <!-- widget content -->
                                        <div class="widget-body">

                                            <p class="report-title">
                                                 {{getMenutitle($arrSidebar)}}
                                            </p>

                                            <!--p class="report-period"> </p-->
                                            <div class="table-responsive" >
                                                <div id="serch_data">

                                                </div>
                                            </div>


                                        </div>
                                        <!-- end widget content -->

                                    </div>
                                    
                                    <!-- end widget div -->

                                </div>
                                <!-- end widget -->

                                <!-- start widget -->  

                                 
                                    <!-- bar chart -->
                                <div class="table-responsive" id="widget_bar_chart" style="display:none;">
                                        <p class="report-title" id="trading-chart-title" style="display:none;">
            
                                            {{getMenutitle($arrSidebar)}}
                                        </p>
                                        
                                        <p class="report-title" id="trading-date" style="display:none;">
            
                                            รายงานข้อมูล ณ วันที่ {{get_date_notime(date("Y-m-d H:i:s"))}}
                                        </p>
                                        <div id="trading-bar-chart" class="chart" style="display:none;">
    
                                        </div>
                                </div>
                                 
                                <!-- end widget -->
                            </div>
                            

                        </div>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->



        </article>
        <!-- WIDGET END -->



    </div>



</div>
<!-- END MAIN CONTENT -->

<!-- FLOT CHARTS -->
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.time.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.selection.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.resize.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.pie.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.stack.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.crosshair.min.js')}}"></script>
<script type="text/javascript" language="javascript" src="{{asset('backend/js/plugin/flot_p2/jquery.flot.orderBars.js')}}"></script>

<!-- flot tooltips plugin-->
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.tooltip.min.js')}}"></script>

<!-- FLOT GROWRAF -->
<script src="{{asset('backend/js/plugin/flot-growraf/jquery.flot.growraf.min.js')}}"></script>


<script type="text/javascript">

    $(document).ready(function(){
        $('#export_search').on('click',function() {
            var PageSizeAll = $('#page-size-search').val();
            var EmpID = $('#name').val();
            var depart = $('#depart').val();
            var plan = $('#plan').val();
            var date_start = $('#hd_date_start').val();
            var date_end =$('#hd_date_end').val();

            var check_name =$('#check_name').is(':checked');
            var check_depart =$('#check_depart').is(':checked');
            var check_plan =$('#check_plan').is(':checked');
            var check_date =$('#check_date').is(':checked');

            window.location.href =  "EquityTradingReport/exportsearch?EmpID=" + EmpID +"&depart=" + depart +
                                    "&plan=" + plan + "&date_start=" +date_start + "&date_end=" + date_end+ 
                                    '&check_name=' + check_name+ '&check_depart=' + check_depart+ '&check_plan=' + 
                                    check_plan+ '&check_date=' + check_date;
            return false;
        });

        meaDatepicker("date_start","date_end");
        meaDatepicker("date_end");

        $('html').append('<input type="hidden" value="1" id="currentpage_search" />');

        $('#btn_search').on('click',function(e) {
            e.preventDefault();

            showhideByFlag(document.getElementById('trading-date').id, false);
            showhideByFlag(document.getElementById('trading-bar-chart').id, false);
            showhideByFlag(document.getElementById('wid-id-0').id, true);

            var PageSizeAll = $('#page-size-search').val();
            var EmpID = $('#name').val();
            var depart = $('#depart').val();
            var plan = $('#plan').val();
            var date_start = $('#hd_date_start').val();
            var date_end =$('#hd_date_end').val();

            var check_name =$('#check_name').is(':checked');
            var check_depart =$('#check_depart').is(':checked');
            var check_plan =$('#check_plan').is(':checked');
            var check_date =$('#check_date').is(':checked');


            if(check_date && date_start != "" && date_end != "" ){
                var str =  'ในช่วงวันที่ ' + GetDateFormat(date_start) + ' ถึง ' + GetDateFormat(date_end);
                $('.report-period').html(str);
            }

            var jsondata = {
                pagesize : PageSizeAll,
                PageNumber:1,
                emp_id : EmpID,
                depart :depart,
                plan : plan,
                date_start:date_start,
                date_end:date_end,
                check_name :check_name,
                check_depart:check_depart,
                check_plan:check_plan,
                check_date:check_date

            };

            console.log(jsondata);
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(jsondata,"EquityTradingReport/search", RenderSearch);

            
            return false;

        });


        $("#page-size-search").on('change',function() {
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            var val = $(this).val();

            var EmpID = $('#name').val();
            var depart = $('#depart').val();
            var plan = $('#plan').val();
            var date_start = $('#hd_date_start').val();
            var date_end =$('#hd_date_end').val();

            var check_name =$('#check_name').is(':checked');
            var check_depart =$('#check_depart').is(':checked');
            var check_plan =$('#check_plan').is(':checked');
            var check_date =$('#check_date').is(':checked');

            var jsondata = {
                pagesize : val,
                PageNumber:1,
                emp_id : EmpID,
                depart :depart,
                plan : plan,
                date_start:date_start,
                date_end:date_end,
                check_name :check_name,
                check_depart:check_depart,
                check_plan:check_plan,
                check_date:check_date

            };
//            var jsondata = {pagesize : val,PageNumber:1};
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(jsondata,"EquityTradingReport/search",RenderSearch);

        });


        // <!-- Toggle -->
        $('#btn_chart').on('click',function(e) { 
            e.preventDefault();
            //RenderChart();
            if(document.getElementById('trading-bar-chart').id) {
               
                showhide(document.getElementById('trading-chart-title').id);
                showhide(document.getElementById('widget_bar_chart').id);
                showhide(document.getElementById('trading-date').id);
                showhide(document.getElementById('trading-bar-chart').id);
                showhide(document.getElementById('wid-id-0').id);
            
                $("body, html").animate({ 
                    scrollTop: $( $(this).attr('href') ).offset().top 
                }, 600);
            }
        });



    });  // jquery ready

    function RenderChart() {

        if(document.getElementById('trading-bar-chart').id) {     
            // $("#trading-bar-chart").remove(); 
            var element = document.getElementById("trading-bar-chart");
            element.outerHTML = "";
            delete element;
            $("#widget_bar_chart").append('<div id="trading-bar-chart" class="chart" style="display:none;">');
   
        } else {

           $("#widget_bar_chart").append('<div id="trading-bar-chart" class="chart" style="display:none;">');
        }

        showhideByFlag(document.getElementById('trading-chart-title').id, true);
        showhideByFlag(document.getElementById('widget_bar_chart').id, true);
        showhideByFlag(document.getElementById('trading-date').id, true);
        showhideByFlag(document.getElementById('trading-bar-chart').id, true);
        showBarChart();
        showhideByFlag(document.getElementById('trading-chart-title').id, false);
        showhideByFlag(document.getElementById('widget_bar_chart').id, false);
        showhideByFlag(document.getElementById('trading-date').id, false);
        showhideByFlag(document.getElementById('trading-bar-chart').id, false);
    }

    function RenderSearch(data){
        
        $("#serch_data").html(data.html);
        $("#serch_data").fadeIn('300');


        $("#page_click_search li a").on('click',PageRenderSearch);
/*
        showhideByFlag(document.getElementById('trading-chart-title').id, true);
        showhideByFlag(document.getElementById('widget_bar_chart').id, true);
        showhideByFlag(document.getElementById('trading-date').id, true);
        showhideByFlag(document.getElementById('trading-bar-chart').id, true);
        showBarChart();
        showhideByFlag(document.getElementById('trading-chart-title').id, false);
        showhideByFlag(document.getElementById('widget_bar_chart').id, false);
        showhideByFlag(document.getElementById('trading-date').id, false);
        showhideByFlag(document.getElementById('trading-bar-chart').id, false);
        */
        RenderChart();
    }

    function PageRenderSearch(){

        var p = $(this).attr('data-page');
        var page_size = $('#page-size-search').val();
        var CurPage = $('#currentpage_search').val();
        $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');

        if(p == "pre"){
            p = parseInt(CurPage) - 1;
        }

        if(p == "next"){
            p = parseInt(CurPage) + 1;
        }
        $('#currentpage_search').val(p);

        var EmpID = $('#name').val();
        var depart = $('#depart').val();
        var plan = $('#plan').val();
        var date_start = $('#hd_date_start').val();
        var date_end =$('#hd_date_end').val();

        var check_name =$('#check_name').is(':checked');
        var check_depart =$('#check_depart').is(':checked');
        var check_plan =$('#check_plan').is(':checked');
        var check_date =$('#check_date').is(':checked');


        var jsondata = {
            pagesize : page_size,
            PageNumber:p,
            emp_id : EmpID,
            depart :depart,
            plan : plan,
            date_start:date_start,
            date_end:date_end,
            check_name :check_name,
            check_depart:check_depart,
            check_plan:check_plan,
            check_date:check_date

        };

        MeaAjax(jsondata,"EquityTradingReport/search",RenderSearch);
    };

    ////////////////////
    // TOOLS 
    function showhide(id) {
        var e = document.getElementById(id);
        e.style.display = (e.style.display == 'block') ? 'none' : 'block';
    }

 
    function showhideByFlag(id, flag) {
        var e = document.getElementById(id);
        e.style.display = flag ? 'block': 'none' ;


    }
    /*-----------------------------------------------------------------------------------*/
    /*  Hourly Chart
    /*-----------------------------------------------------------------------------------*/
    /*
    // BAR charts 
                    handleDashFlotChart(start_hour, 
                                        json.results.barchart_good, 
                                        json.results.barchart_bad);
    */
    var showBarChart = function () {

        function drawBarChart() { 
            var data1 = [], 
            data2 = [], 
            n = 8,  /* day Shift 8:00 - 20:00 */
            i = 0,
            st = n,
            ticks = [];

            var labels = []; 
            var suffix = 'ล้านบาท';
            var yasix_label = 'จำนวณ (ล้านบาท)';
/*
            if (n >= 8 && n<20 ) {
                data1 = [];
                data2 = [];
                st = 8; 
                for (i=0; i<12; i++) {
                    /// var g1 = [st, Math.random()*100];  
                    data1.push([st, vdata1[i]]);
                    /// var g2 = [st, Math.random()*100]; 
                    data2.push([st, vdata2[i]]);
                    ticks.push(st);
                    st++;
                }
            } else {
                st = 20;
                for (i=0; i<12; i++) {
                    //var g1 = [st, Math.random()*100]; 
                    data1.push([st, vdata1[i]]);
                    // var g2 = [st, Math.random()*100];   
                    data2.push([st, vdata2[i]]);
                    ticks.push(st);
                    st++;
                } 
            }
  */         
            for (i=0; i<12; i++) {
                ticks.push(i);
            }

            labels.push('พลังงาน');
            labels.push('ท่องเที่ยว');
            labels.push('บริการ');
            labels.push('ทรัพยากร');
            labels.push('ธุรกิจการเงิน');
            labels.push('สินค้าอุตสาหกรรม');
            labels.push('อสังหาริมทรัพย์');
            labels.push('เกษตร');
            labels.push('เทคโนโลยี');
            labels.push('อุตสาหกรรมอาหาร');
            labels.push('ก่อสร้าง');
            labels.push('อุปโภคบริโภค');


            data1.push([ticks[0],  20]);
            data1.push([ticks[1],  30]);
            data1.push([ticks[2],  60]);
            data1.push([ticks[3],  40]);
            data1.push([ticks[4],  80]);
            data1.push([ticks[5],  40]);
            data1.push([ticks[6],  53]);
            data1.push([ticks[7],  23]);
            data1.push([ticks[8],  15]);
            data1.push([ticks[9],  17]);
            data1.push([ticks[10], 97]);
            data1.push([ticks[11], 86]);

            data2.push([ticks[0],  40]);
            data2.push([ticks[1],  60]);
            data2.push([ticks[2],  70]);
            data2.push([ticks[3],  80]);
            data2.push([ticks[4],  70]);
            data2.push([ticks[5],  20]);
            data2.push([ticks[6],  33]);
            data2.push([ticks[7],  13]);
            data2.push([ticks[8],  45]);
            data2.push([ticks[9],  87]);
            data2.push([ticks[10], 57]);
            data2.push([ticks[11], 26]);

            var plot = $.plot($("#trading-bar-chart"), [{
                    data: data1,
                    bars: {
                        show: true,
                        barWidth: 0.28,
                        fill: true,
                        fillColor : '#02a6ba', //'#8bc34a', //
                        //align: "right",
                        lineWidth: 1,
                        order: 1
                    },
                    label: "ซื้อ"

                }, {
                    data: data2,
                    bars: {
                        show: true,
                        barWidth: 0.28,
                        fill: true,
                        fillColor : '#d32f2f',
                        //align: "left",
                        lineWidth: 1,
                        order: 2
                    },
                    label: "ขาย" 

                }], {
                    series: {
                        bars: {
                             barWidth: 0.25,
                            // align: 'right',
                             show: true
                            // order: 2
                    }
                },
                // Legend
                colors: ['#04b8ce','#d32f2f','#8bcE4a',"#FE0E1C", '#d32f2f','#e91e63', 
                         '#FF5733','#0033FF', "#FF02FC0", "#FE0E1C"],


                grid: {
                    hoverable: true,
                    clickable: true,
                    borderWidth: 0.1,
                    markings: [
                        { color: '#12ea15', lineWidth: 1, yaxis: { from: 800, to: 800 } },
                        { color: '#ea1215', lineWidth: 1, yaxis: { from: 400, to: 400 } }
                    ]
                },

                legend: {
                    show: true
                },

                xaxis: {
                    autoscaleMargin: .035,
                    mode: "categories",
                    tickLength: 0,
                    ticks: ticks,
                    tickDecimals: 0,
                    tickColor: "#e1e1e1",
                    tickFormatter: function (x) { 
                    /*    if (x > 23)  {
                            return (x - 24).toFixed(2).replace(".", ":")  ;
                        } else  { 
                            return x.toFixed(2).replace(".", ":") ;
                        }*/ 
                        return labels[x];
                    }
                },

                yaxis: {
                    ticks: 12,
                    tickColor: "#eaeaea",
                    tickDecimals: 0,
                    min: 0,
                    tickFormatter: function(val, axis) { return val < axis.max ? val.toFixed(2) : yasix_label + '     ' + val.toFixed(2);
                    
                    }/*,
                    font:{
                      size:14,
                      style:"italic",
                      weight:"bold",
                      family:"sans-serif",
                      variant:"small-caps"
                    }*/
                    
                },

            });  //END:  $.plot

            function showTooltip(x, y, contents) {
                $('<div id="tooltip">' + contents + '</div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y + 5,
                    left: x + 10,
                    
                    'box-shadow': '0 0 10px #555',
                    padding: '8px',
                    color: '#fff',
                    'border-radius': '4px',
                    '-webkit-border-radius': '4px',
                    '-moz-border-radius': '4px',
                    'border-radius': '4px',
                    'background-color': '#333',
                    border: '1px solid #fff',
                    opacity: 0.82
                }).appendTo("body").fadeIn(200);
            }

            var previousPoint = null;

            $("#trading-bar-chart").bind("plothover", function (event, pos, item) {
                $("#x").text(pos.x.toFixed(2));
                $("#y").text(pos.y.toFixed(2));
                if (item) {
                    if (previousPoint != item.dataIndex) {
                        previousPoint = item.dataIndex;
                        $("#tooltip").remove();
                        var x = item.datapoint[0].toFixed(0),//.toFixed(2),
                            y = item.datapoint[1].toFixed(0);//.toFixed(2);
                        showTooltip(item.pageX, item.pageY, 
                            item.series.label + " - " + labels[x] + "=" + y + " " + suffix);
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            });
        }
        
        /* Run the charts */
        drawBarChart();
    }

</script>

@stop
