@extends('backend.layouts.default')
@section('content')
<?php
$data = getmemulist();
$arrSidebar =getSideBar($data);
?>

    <style type="text/css">
        #sort-by-year{
            /*position:absolute;*/
            width: 100%;
            margin-bottom: 10px;
            /*z-index: 999;*/
            /*left: 200px;*/
            /*top:5px*/
        }
        #sort-by-year select{
            width: 300px;

        }
        #datatable_fixed_column tbody tr td a i{
            font-size: 11px !important;
            line-height: 12px!important;
        }
        #datatable_fixed_column tbody tr td a{
            line-height: 12px!important;

        }

        .isa_info, .isa_success, .isa_warning, .isa_error {

        margin: 10px 0px;
        padding:12px;
        }
.isa_info {
    color: #00529B;
    background-color: #BDE5F8;
}
.isa_success {
    color: #4F8A10;
    background-color: #DFF2BF;
}
.isa_warning {
    color: #9F6000;
    background-color: #FEEFB3;
}
.isa_error {
    color: #D8000C;
    background-color: #FFBABA;
}
.isa_info i, .isa_success i, .isa_warning i, .isa_error i {
    margin:10px 22px;
    font-size:2em;
    vertical-align:middle;
}

    </style>

<!-- MAIN CONTENT -->
<div id="content">

    <div class="widget-body fuelux">
         <div class="wizard" >
            <ul class="steps form-wizard">
                <!--li style="font-size:18px" >
                    <a href="#"  class="badge badge-info">1</a>
                    <a href="#">ข้อมูล ประวัติซื้อ -ขาย</a> 
                    <span class="chevron"></span>
                </li-->

                <li style="font-size:18px" class="active">
                    <a href="#"  class="badge">1</a>
                    <a href="#"> นําเข้าข้อมูล</a>
                    <span class="chevron"></span>
                </li>
            </ul>

        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>
                 นำเข้าข้อมูล
            </h1>
        </div>
    </div>

    <!-- NEW COL START -->
    <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                <!--header>
                    <span class="widget-body widget-icon"> <i class="fa fa-download"></i> </span>
                    <span style="font-size: 18px">เป็นเมนูสําหรับนําเข้าข้อมูล มูลค่าทรัพย์สนิ สุทธิและอัตราผลตอบแทน </span>
                </header-->

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                       
                        <!--form id="smart-form-register-main" action="" style="margin-bottom: 20px"  -->
                            {{--{!! csrf_field() !!} --}}
                            
                            <div class="smart-form">
                                <fieldset>

                                    <section class="col col-6">
                                        
                                        <label class="input">
                                            <span style="font-size: 20px">เลือกประเภทข้อมูล</span>
                                            <select name="datatype" id="datatype" class="form-control" onchange="reanderImportForm(this);">
                                                <option value="">&nbsp;เลือกประเภทข้อมูล</option>
                                                @if($typelist)
                                                    @foreach($typelist as $item)
                                                        <option value="{{$item->ID}}">&nbsp;{{$item->DESCRIPTION}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </label>
                                    </section>

                                </fieldset>
                            </div>

                            <div class="smart-form" id="mainform" style="display:block;">

                                <!-- Form [1] -->
                                <div class="smart-form"  id="form1-trading" style="display:none;">
                                    <fieldset>
                                        <section class="col col-6">
                                            <span style="font-size: 20px"> ข้อมูล Trading Transactions</span>
                                            <div class="inline-group">  
                                                <a class="btn btn-xs btn-success" href="{{action('BondDataImportController@T2_downloadSampleTradingTransactions')}}"> ดูตัวอย่างไฟล์ </a>
                                            </div>
                                        </section>
                                    </fieldset>

                                    <!-- form [1] -->
                                    <form id="form1-trading-register" action="" >
                                         {!! csrf_field() !!}
                                        <fieldset>
                                              
                                                <header style="font-size: 24px">
                                                 เป็นเมนูสําหรับนําเข้าข้อมูล Trading Transactions (รูปแบบไฟล์นามสกลุ .xls)
                                                </header>
                                                <br/>
                                                <section class="col col-6" >
                                                     
                                                    <label class="input">
                                                            <span style="font-size: 20px">
                                                                บริษัท จัดการ
                                                             </span>
                                                             <select name="broker" id="broker" class="form-control">
                                                                <option value="">  เลือกบริษัท จัดการ </option>
                                                                @if($securities)
                                                                    @foreach($securities as $item)
                                                                        <option value="{{$item->NAME_SHT}}">&nbsp;{{$item->SECURITIES_NAME}}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                    </label>
                                                </section>
                                                <br/>
                                        </fieldset>   
                                    </form>   
                                </div>     
                                
                                <!-- FORM 2 (Portfolio) -->
                                
                                <div class="smart-form"  id="form2-portfolio" style="display:none;">

                                    <fieldset>
                                        <section class="col col-6">
                                            <span style="font-size: 20px"> ข้อมูล Portfolio</span>
                                            <div class="inline-group">  
                                                <a class="btn btn-xs btn-success" href="{{action('BondDataImportController@T2_downloadSamplePortfolio')}}"> ดูตัวอย่างไฟล์ </a>
                                            </div>
                                        </section>
                                    </fieldset>

                                    <!-- form [2] -->
                                    <form id="form2-portfolio-register" action="" >
                                         {!! csrf_field() !!}
                                        <fieldset>
                                              
                                                <header style="font-size: 24px">
                                                 เป็นเมนูสําหรับนําเข้าข้อมูล Portfolio เข้าสู่ระบบกองทุน ฯ (รูปแบบไฟล์นามสกลุ .xls)
                                                </header>

                                                <br/>
                                                <section class="col col-6" >
                                                     
                                                    <label class="input">
                                                            <span style="font-size: 20px">
                                                                บริษัท จัดการ
                                                             </span>
                                                             <select name="form2broker" id="form2broker" class="form-control">
                                                                <option value="">  เลือกบริษัท จัดการ </option>
                                                                @if($securities)
                                                                    @foreach($securities as $item)
                                                                        <option value="{{$item->NAME_SHT}}">&nbsp;{{$item->SECURITIES_NAME}}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                    </label>
                                                </section>
                                                <br/>
                                        </fieldset>   
                                    </form>   
                                </div>     
                            
                                <!-- START: FORM 3 - เป็นเมนูสำหรับ นําเข้าข้อมูล มูลค่า หน่วยลงทุน และอัตราผลตอบแทนเข้าสู่ระบบกองทุน ฯ  -->
                                <div class="smart-form" id="form3-bond-unit-val" style="display:none;">
                                        <fieldset> 
                                           <section style="margin-top: 0px;margin-left: 5px; margin-right: 5px;">
                                               <!-- BEGINE: search from -->
                                               <div class="table-responsive" id="form4-search-form" name="form4-search-form" style="width: 100%; padding: 0px;">
                                        
                                               </div>
                                               <!-- END: search form -->
                                           </section>
                                        </fieldset>

                                        <!--form id="form3-bondnav-register" action="" -->
                                        <div clas="row">
                                            <section style="margin-top: 0px;margin-left: 15px; margin-right: 15px;">
                                               
                                                    <ul id="sparks" class="">
                                                        <li class="sparks-info">
                                                            <a href="{{action('BondDataImportController@getAddUnitVal')}}" class="btn bg-color-green txt-color-white">&nbsp;<i class="fa fa-plus"></i>&nbsp;&nbsp;เพิ่ม&nbsp;&nbsp;</a>
                                                        </li>
                                                        <!--
                                                        <li class="sparks-info">
                                                            <a href="javascript:void(0);" id="unit_val_edit"  class="btn bg-color-blueDark txt-color-white">&nbsp;<i class="fa fa-gear fa-lg"></i>&nbsp;&nbsp;แก้ไข&nbsp;&nbsp;</a>

                                                        </li>
                                                       
                                                        <li class="sparks-info">
                                                            <a href="javascript:void(0);" id="delete_record" class="btn bg-color-red txt-color-white">&nbsp;<i class="glyphicon glyphicon-trash"></i>&nbsp;ลบ&nbsp;</a>
                                                        </li>
                                                         -->
                                                        
                                                    </ul> 
                                                 
                                            </section>
                                        </div>

                                        
                                        <section style="margin-top: 0px;margin-left: 15px; margin-right: 15px;"> 
                                            

                                            <div class="widget-body no-padding" >
                                                <div class="table-responsive">
                                                    <div class="result" style="width: 100%; padding: 20px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <!--/form-->                                        
                                </div>
                                <!-- END: FORM 3 --> 

                                <!-- START: FORM 4 -  ทรัพย์สินสุทธิและอัตราผลตอบแทน (ตราสารหน้) BondNAV -->
                                <div class="smart-form"  id="form4-bond-nav" style="display:none;">
                                    <fieldset>
                                       
                                            <!--header style="font-size: 20px" style="margin-top: 0px;margin-left: 20px; margin-right: 20px;">
                                                ข้อมูลมูลค่าทรัพย์สิน สุทธิและอัตรา ผลตอบแทน
                                            </header-->

                                            <section class="col col-6">
                                                <span style="font-size: 20px"> ข้อมูลมูลค่าทรัพย์สิน สุทธิและอัตรา ผลตอบแทน</span>
                                                <div class="inline-group">  
                                                    <!--a class="btn btn-xs btn-success" href="{{action('EquityDataImportController@T2_dowloadsampleEquity')}}"> ดูตัวอย่างไฟล์ ตราสารทุน</a -->
                                                    <a class="btn btn-xs btn-success" href="{{action('BondDataImportController@T2_dowloadsampleBond')}}"> ดูตัวอย่างไฟล์ ตราสารหนี้</a>
                                                </div>
                                            </section>
                                        

                                    </fieldset>

                                    <!-- menu 4. form -->

                                    <form id="smart-form-register" action="" >
                                       {!! csrf_field() !!}
                                        <fieldset>
                                              
                                                <header style="font-size: 24px">
                                                 เป็นเมนูสําหรับนําเข้าข้อมูล มูลค่าทรัพย์สนิ สุทธิและอัตราผลตอบแทน (รูปแบบไฟล์นามสกลุ .xls)
                                                </header>
                                                <br/>
                                                <section class="col col-6" >
                                                     
                                                    <label class="input">
                                                            <!--span style="font-size: 20px">
                                                                เลือกประเภทข้อมูล
                                                             </span-->
                                                             <!-- TODO: fix POLICY_ID for Bond-Nav -->

                                                             <select name="policy" id="policy" class="form-control" disabled="true">
                                                                <!--option value="">  เลือกประเภทข้อมูล   </option-->
                                                                @if($policyList)
                                                                    @foreach($policyList as $item)
                                                                        @if($item->POLICY_ID =="2")
                                                                        <option  value="{{$item->POLICY_ID}}" selected>{{$item->POLICY_NAME}}</option>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                    </label>
                                                </section>
                                                <br/>
                                        </fieldset>   
                                    </form>   
                                </div>  
                                <!-- END: FORM 4 -->    

                                <div class="smart-form" id="footer" style="display:none;" >
                                    <fieldset>       

                                            <section style="margin-top: 0px;margin-left: 20px; margin-right: 20px;">
                                                <label class="input">
                                                    <div class="form-group">
                                                        <span style="font-size: 20px"> 
                                                            เลือกไฟล์ ที่ต้องการนำเข้า      
                                                        </span>

                                                        <input type="file" class="filestyle" data-buttonBefore="false" data-size="sm" 
                                                             data-buttonName="btn-primary" data-placeholder="No file"
                                                            data-buttonText="  เลือกไฟล์ " id="import1" name="import1">
                                                    </div>
                                                </label>
                                                
                                            </section>            
                                            <br/>

                                    </fieldset>
                                    

                                       
                                    <footer >
                                        <p id="progress_check1" style="display: none;"><img src="{{asset('backend/img/shot.gif')}}"  /> กำลังตรวจสอบรูปแบบ ไฟล์</p>
                                        <p id="progress_import1" style="display: none;"><img src="{{asset('backend/img/shot.gif')}}"  /> กำลังนำเข้าข้อมูล</p>
                                         
                                        <span style="color: #4F8A10 ; font-size: 18px">
                                           <p  id="check_ret" style="display: none"> </p>
                                        </span>
                                     
                                        <div class="button-group">
                                             <p>
                                               <a href="javascript:void(0);" style="display: block;" class="btn_cancel btn btn-xs btn-primary"><i class="fa fa-reset"></i> ยกเลิก</a-->
                                               <!-- <a href="javascript:void(0);" style="display:none;" data-input="import1"  data-import="1" class="btn_check btn btn-xs btn-primary"><i class="fa fa-download"></i> ตรวจสอบไฟล์</a> -->
                                               <a href="javascript:void(0);" data-input="import1"  data-import="1" class="btn_check btn btn-xs btn-primary"><i class="fa fa-download"></i> ตรวจสอบไฟล์</a>
                                               <a href="javascript:void(0);" style="display: none"  data-input="import1" data-import="1" class="btn_import btn btn-xs btn-primary"><i class="fa fa-download"></i> นำเข้าข้อมูล</a>
                                             </p>
                                         </div>
                                    </footer>                                    
                                </div>
                            </div>
                                
                      <!--  </form> -->

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->
        </div>
            <!-- end widget -->
    </article>
</div>
<!-- END MAIN CONTENT -->


<!-- PAGE RELATED PLUGIN(S) -->
{{--<script src="{{asset('backend/js/plugin/datatables/jquery.dataTables.min.js')}}"></script>--}}
{{--<script src="{{asset('backend/js/plugin/datatables/dataTables.colVis.min.js')}}"></script>--}}
{{--<script src="{{asset('backend/js/plugin/datatables/dataTables.tableTools.min.js')}}"></script>--}}
{{--<script src="{{asset('backend/js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>--}}
{{--<script src="{{asset('backend/js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>--}}

<script src="{{asset('backend/js/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script> 
<script type="text/javascript">

    var error_msg = "การ ตรวจสอบไฟล์ ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ";

    var isValid=(function(){
        var rg1=/^[^\\/:\*\?"<>\|]+$/; // forbidden characters \ / : * ? " < > |
        var rg2=/^\./; // cannot start with dot (.)
        var rg3=/^(nul|prn|con|lpt[0-9]|com[0-9])(\.|$)/i; // forbidden file names
        var rg4=/^[a-zA-Z0-9-_]+$/; // allow
        return function isValid(fname){
            return rg1.test(fname)&&!rg2.test(fname)&&!rg3.test(fname) &&!rg4.test(fname);
        }
    })();

   
    $(document).ready(function() {

        if (navigator.userAgent.indexOf("MSIE") > 0) {
            $("#import1").mousedown(function() {
                $(this).trigger('click');
            })
        } else if($.browser.mozilla) {
           
        }


        $.validator.addMethod("valueNotEquals", function(value, element, arg){
                return arg != value;
        }, "Please Choose one");


        
        /* Form-1 trading transactions */
        $("#form1-trading-register").validate({

            // Rules for form validation
            rules : {
                broker: {
                    required: true
                }
            },

            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            }
        });

        /* Form 2 */
        $("#form2-portfolio-register").validate({

            // Rules for form validation
            rules : {
                form2broker: {
                    required: true
                }
            },

            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            }
        });
        
        // Cancel click event
        $('.btn_cancel').on('click', function(){
            $(":file").filestyle('clear');
            $('#check_ret').hide();
            $('.btn_check').show();
            $('.btn_import').hide();
        });
        
        $('#datatype').change(function() {
            var val = parseInt($('#datatype option:selected').val());
            if(val == 3) {
                  $('#footer').hide(); 
                  $('#form1-trading').hide();
                  $('#form2-portfolio').hide();
                  $('#form3-bond-unit-val').show();
                  $('#form4-bond-nav').hide();
                
                  // var parameters = {pagesize : 20, PageNumber:1 , symbol:'', market:'', cate_id:''};
                  // var method = "BondDataImport/bondUnitValIndexSearchForm";

                  var investment  = $("#investment").val();  // investment
                  var securities  = $("#securities").val();  // securities 

                  var date_start  = $('#hd_date_start').val();
                  var date_end    = $('#hd_date_end').val();
                  var page_size   = $('#page-size-search').val();

                  var parameters  = {
                                pagesize: page_size, 
                                PageNumber: 1, 
                                investment: investment, 
                                securities: securities, 
                                date_start: date_start,
                                date_end:   date_end,
                               };
                  var method = "BondDataImport/bondUnitValIndexSearchForm";
                  MeaAjax(parameters, method, RenderForm);
            } else {
                $('#form3-bond-unit-val').hide();
            }

            /* switch(val) {
                case 1:
                   $('#form2-trading').show();
                   $('#form2-portfolio').hide();
                   break;

                case 2:
                   $('#form2-portfolio').show();
                   $('#form2-trading').hide();
                   break;

                case 3:
                   $('.btn_check').hide();
                   $('.btn_import').hide();
                   $('#form2-portfolio').hide();
                   $('#form2-trading').hide();
                   $('#form3-dataimport').show();
                   break;   
            } */
        });


        $('#datatype').on('click', function(){ 
            $(":file").filestyle('clear');
            $('#check_ret').hide();
            $('.btn_check').show();
            $('.btn_import').hide();
            var val = parseInt($('#datatype option:selected').val());
            console.log('#datatype option:selected:' + val);
            switch(val) {
                case 1:
                   //$('.btn_check').hide();
                   $('#form1-trading').show();
                   $('#form2-portfolio').hide();
                   $('#form3-bond-unit-val').hide();
                   $('#form4-bond-nav').hide();
                   break;

                case 2:
                   //$('.btn_check').hide();
                   $('#form1-trading').hide();
                   $('#form2-portfolio').show();
                   $('#form3-bond-unit-val').hide();
                   $('#form4-bond-nav').hide();
                   break;

                case 3:
                   // เป็นเมนูสำหรับ นําเข้าข้อมูล มูลค่า หน่วยลงทุน และอัตราผลตอบแทนเข้าสู่ระบบกองทุน
                   $('.btn_check').hide();
                   $('.btn_import').hide();
                   $('#form1-trading').hide();
                   $('#form2-portfolio').hide();
                   $('#form3-bond-unit-val').show();
                   $('#form4-bond-nav').hide();
                   $('#footer').hide(); 
                   break;  

                case 4:
                   // มูลค่าหน่วยลงทุนและอัตราผลตอบแทน
                   $('.btn_check').show();
                   $('.btn_import').hide();
                   $('#footer').hide(); 
                   $('#form1-trading').hide();
                   $('#form2-portfolio').hide();
                   $('#form3-bond-unit-val').hide();
                   $('#form4-bond-nav').show();
                   $('#footer').show(); 
                   break;  

                default:
                   $('#check_ret').hide();
                   $('.btn_check').hide();
                   $('.btn_import').hide();
                   $('#form2-trading').hide();
                   $('#form2-portfolio').hide();
                   $('#form3-bond-unit-val').hide();
                   $('#form4-bond-nav').hide();
                   break;    
            } 

           /* if(val === 3) {
                 $('.btn_check').hide();
                 $('#form2-portfolio').hide();
                 $('#form4-dataimport').show();
               
              
            } if(val === 2) {
                 $('#form4-dataimport').hide();
                  $('.btn_check').hide();
                 $('#form2-portfolio').show();

            } else {
                 $('#form4-dataimport').hide();
            }*/
        });
         


        $('.btn_check').on('click',function(){
            var target = $(this).attr('data-input');
            var importType= $(this).attr('data-import');

            var functions = ["","validateTradingFile","validatePortfolioFile"];
            var method = functions[parseInt($('#datatype').val())];
            console.log('selected:' + parseInt($('#datatype').val()));

            switch(parseInt($('#datatype').val())) {
                case 1:
                   validateTradingFile(target, importType);
                   break;

                case 2:
                   validatePortfolioFile(target, importType); 
                   break;

                case 4: //เมนูนำข้อข้อมูลทรัพย์สินสุทธิ (ตราสารหนี้)
                   validateBondNAVFile(target, importType);
                   break;

                case 5:
                   
                   break;

                case 7:
                    // GainLoss
                    var fullpath = $('#' + target).val();
                    var fname = fullpath.split(/(\\|\/)/g).pop();
                    var sarray = fname.split(/[-_.]/);
                    //console.log(sarray[3])
                   
                    var filedate = sarray[3]; //extractFile(fullpath);
                    if(filedate.length > 6) {
                        var fday   = filedate.substr(0,2); // eg.   31
                        var fmonth = filedate.substr(2,2); // eg.   12
                        var fyear  = filedate.substr(4,8); // eg. 2016
                        // sanity checking
                        if(!isNaN(parseInt(fyear)) && !isNaN(parseInt(fmonth)) && !isNaN(parseInt(fday))) {
                            console.log('OK., file name is valid');
                            filedate = fyear + '-' + fmonth + '-' + fday;
                            validateGainLossFile(target, importType, filedate, fullpath);
                        } else {
                            Alert("Error#1", "กรุณาตรวจสอบ รูปแบบข้อมูลของ ชื่อไฟล์ ตัวอย่างเช่น 'SecuritiesName_Gain_Loss_DDMMYYYY.xls'");
                        }

                    } else {
                        Alert("Error#2", "กรุณาตรวจสอบ รูปแบบข้อมูลของ ชื่อไฟล์ ตัวอย่างเช่น 'SecuritiesName_Gain_Loss_DDMMYYYY.xls'");
                    } 
                   
                   break;

                case 8:
                    // stip out c:/fakepath/..
                    var filename = $('#' + target).val().split(/(\\|\/)/g).pop();
                    var dateArray = filename.split(/[.,\_\/ -]/); 
                    console.log(dateArray  + " -> :Length=" + dateArray.length);
                    if( dateArray.length > 3) {
                        //alert(dateArray[0]);

                        var period_half_year = dateArray[3].substr(0,2);   // eg.  2H
                        var period_year =  dateArray[3].substr(2,6);   // eg. 2016

                        // sanity checking
                        if(!isNaN(parseInt(period_half_year)) &&
                           (dateArray[2].toLowerCase()=='universe') && 
                            // ($('#stockuniversebroker').val() == dateArray[0]) && 
                                ((period_half_year.toLowerCase()=='1h') || 
                                (period_half_year.toLowerCase()=='2h'))) {
                            // alert(period_half_year);
                            // accept only format: "UOBAM_Stock universe_1H2017" 
                            var file_security_name = dateArray[0].toUpperCase();
                            validateStockUniverseFile(target, importType, period_half_year, period_year, file_security_name);
                        } else {
                            Alert("Error", "กรุณาตรวจสอบ รูปแบบข้อมูลของ ชื่อไฟล์ ตัวอย่างเช่น 'UOBAM_Stock universe_1H2017.xls'");
                        }
                       
                    } else {
                        Alert("Error", error_msg);
                   }
                   break;
                case 9:
                   break;
                case 10:
                   break;  
                default:
                   break;                            
            }   
        });

        
        /**
         *  Handle Button Import Clicked
         */
        $('.btn_import').on('click', function() {
            var target = $(this).attr('data-input');
            var importType= $(this).attr('data-import');
            // alert($('#datatype').val());

            switch(parseInt($('#datatype').val())) {
                case 1:
                   importTradingFile(target, importType);
                   break;

                case 2:
                   importPortfolioFile(target, importType);
                   break;
                
                case 4:
                   importBenefitFile(target, importType);
                   break;

                case 7:
                   /*เป็นเมนสู าํ หรับนําเข้าข้อมลู สว่ นเพมิ  -ลดจากการตีราคาหลกั ทรัพย์เข้าสรู่ ะบบกองทนุ ฯ*/
                   // GainLoss
                    var fullpath = $('#' + target).val();
                    var fname = fullpath.split(/(\\|\/)/g).pop();
                    var sarray = fname.split(/[-_.]/);
                    //console.log(sarray[3])
                   
                    var filedate = sarray[3]; //extractFile(fullpath);
                    if(filedate.length > 6) {
                        var fday   = filedate.substr(0,2); // eg.   31
                        var fmonth = filedate.substr(2,2); // eg.   12
                        var fyear  = filedate.substr(4,8); // eg. 2016
                        // sanity checking
                        if(!isNaN(parseInt(fyear)) && !isNaN(parseInt(fmonth)) && !isNaN(parseInt(fday))) {
                            console.log('OK., file name is valid');
                            filedate = fyear + '-' + fmonth + '-' + fday;
                            importGainLossFile(target, importType, filedate);
                        } else {
                            Alert("Error#1", "กรุณาตรวจสอบ รูปแบบข้อมูลของ ชื่อไฟล์ ตัวอย่างเช่น 'SecuritiesName_Gain_Loss_DDMMYYYY.xls'");
                        }

                    } else {
                        Alert("Error#2", "กรุณาตรวจสอบ รูปแบบข้อมูลของ ชื่อไฟล์ ตัวอย่างเช่น 'SecuritiesName_Gain_Loss_DDMMYYYY.xls'");
                    } 
                   break;

                case 8:
                   // importStockUniverseFile(target, importType);
                   var filename = $('#' + target).val().split(/(\\|\/)/g).pop();
                    var dateArray = filename.split(/[.,\_\/ -]/); 
                    console.log(dateArray  + " -> :Length=" + dateArray.length);
                    if( dateArray.length > 3) {
                       //alert(dateArray[3]);

                        var period_half_year = dateArray[3].substr(0,2);   // eg.  2H
                        var period_year =  dateArray[3].substr(2,6);   // eg. 2016

                        // sanity checking
                        if(!isNaN(parseInt(period_half_year)) && (dateArray[2].toLowerCase()=='universe') 
                            && ((period_half_year.toLowerCase()=='1h') || 
                                (period_half_year.toLowerCase()=='2h'))) {
                            // accept only format: "UOBAM_Stock universe_1H2017" 
                            var file_security_name = dateArray[0].toUpperCase();
                            importStockUniverseFile(target, importType, period_half_year, period_year, file_security_name);
                        } else {
                            Alert("Error", "กรุณาตรวจสอบ รูปแบบข้อมูลของ ชื่อไฟล์ ตัวอย่างเช่น 'UOBAM_Stock universe_1H2017.xls'");
                        }
                       
                    } else {
                        //  showInfoMessage(false, 
                        //                "ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์",
                        //                "", ""));
                        Alert("Error", error_msg);
                   }
                   break;
                case 9:
                   break;
                case 10:
                   break;  
                default:
                   break;                            
            }
        });  


        ////////////////////////////////////////////////////////
        var eventName = function() {
            $.getJSON("progress", function(data) {
                // update the view with your fresh data
                console.log('########## total:' + data.total + ' progress: ' + data.percent_progress + '%');
                if (data.percent_progress < 100)
                     eventName();
            });
        };
        ///////////////////////////////////////////////////////

    });  // ready

    function showInfoMessage(status, title, subtitle, infomessage) {
        var msg = '';
        if(status) {
           msg = "<div class='isa_success'>" +
                 "<i class='fa fa-check-circle'></i>" +
                 "<font color='green'>" + title + " <br/>" +
                                        subtitle +
                 " </font> <font color='#047712'>" + infomessage + " </font>" +
                 "</div>";
         } else {
            msg = "<div class='isa_error'>" +
                  "<i class='fa fa-times-circle'></i>" + 
                  "<font color='red'> " + title + " <br/>" +
                                        subtitle + " </font>" +
                  "</div>";
         }
         return msg;
    }


    function reanderImportForm(sel) {

        var forms = ["", "#form1-trading", "#form2-portfolio", "#form3-bond-unit-val", "#form4-bond-nav" ];
        
        if(sel.value === '') {
            $('#footer').hide();  
            for(var i=0;i<forms.length; i++) {
                if(forms[i].length > 0 ) {
                    console.log(i);
                   $(forms[i]).hide(); 
                }
            }
            return;
        }
        if(forms[sel.value].length < 1 ) { 
            return;
        }

        
        for(var i=0;i<forms.length; i++) {
            if(forms[i].length > 0 ) {
                console.log(i);
               $(forms[i]).hide(); 
            }
        }

        $(forms[sel.value]).show();
        $('#footer').show(); 
        
    }

    function validateTradingFile(target, importType) {
        $('#check_ret').hide();

        var dataimport = new FormData();
        var extall="xlsx,xls.xlx";
        var files = $("#" + target).get(0).files;

        if(!files.length ) { 
            Alert("Import","ท่านยังไม่ได้เลือกไฟล์");
            return false;
        }

        ext = files[0].name.split('.').pop().toLowerCase();

        if(parseInt(extall.indexOf(ext)) < 0) {
            $('#progress_check' + importType).hide();
            Alert("Import",'Extension support : ' + extall);
            return false;
        } 

        if(files.length > 0) {
            dataimport.append("exelimport", files[0]);
        }

        ////////////////////////////////////////
        /// From 1: Trading Transactions File
        if ($('#datatype').val() == "1") {

            if($("#form1-trading-register").valid()) {  

                dataimport.append('type', $('#datatype').val());
                dataimport.append('broker', $('#broker').val());
                
                $('#progress_check' + importType).show();

                $.ajax({
                    cache: false,
                    type: 'POST', 
                    contentType: false,
                    processData: false,
                    url: 'BondDataImport/t2checkTreadingTransactionsFile',
                    data: dataimport,

                    success: function(data) {
                        percent = 100;
                        clearInterval(window.progressInterval);
                        $('#progress_check' + importType).hide();

                        if(data.success) {

                            if(parseInt(data.html) > 10,000) {
                                Alert('Import', "จำนวน record สูงสุดที่อนุญาติให้ import คือ 10,000 record");
                            } else {
                                $('.btn_check').hide();
                                $('.btn_import').show();

                                $('#check_ret').show();
                                $('#check_ret').html(
                                    showInfoMessage(data.success, 
                                    "รูปแบบไฟล์ ถูกตรวจสอบเรียบร้อย แล้ว",
                                    data.html, "กรุณากดปุ่ม นำเข้าข้อมูล ด้านล่างเพื่อ ดำเนินการ import"));
                            }
                        } else {
                                $('.btn_check').show();
                                $('.btn_import').hide();

                                $('#check_ret').show();
                                $('#check_ret').html(
                                   
                                showInfoMessage(data.success, 
                                    "ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์",
                                    data.html, ""));
                                Alert("Error", "การ ตรวจสอบไฟล์ ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ");
                        }
                    },

                    error: function(xhr, textStatus, thrownError) {
                        clearInterval(window.progressInterval);
                       
                        $('#progress_check' + importType).hide();

                        $('.btn_check').hide();
                        $('.btn_import').show();

                        $('#check_ret').show();
                        $('#check_ret').html(
                                showInfoMessage(false, 
                                    "ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์",
                                   textStatus, ""));
                        Alert("Error", "การ ตรวจสอบไฟล์ ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ");
                    }
                });

                return false;
            }

        }
    }

    /**
     * Validate Portfolio Menu 2
     */
    function validatePortfolioFile(target, importType) {
        $('#check_ret').hide();

        var dataimport = new FormData();
        var extall="xlsx,xls.xlx";
        var files = $("#" + target).get(0).files;

        if(!files.length ) { 
            Alert("Import","ท่านยังไม่ได้เลือกไฟล์");
            return false;
        }

        ext = files[0].name.split('.').pop().toLowerCase();

        if(parseInt(extall.indexOf(ext)) < 0) {
            $('#progress_check' + importType).hide();
            Alert("Import",'Extension support : ' + extall);
            return false;
        } 

        if(files.length > 0) {
            dataimport.append("exelimport", files[0]);
        }

        ////////////////////////////////////////
        /// From 2 (Menu ID=2)
        if ($('#datatype').val() == "2") {

            if($("#form2-portfolio-register").valid()) {  

                dataimport.append('datatype', $('#datatype').val());
                dataimport.append('broker', $('#form2broker').val());
                
                $('#progress_check' + importType).show();

                $.ajax({
                    cache: false,
                    type: 'POST', 
                    contentType: false,
                    processData: false,
                    url: 'BondDataImport/t2checkPortfolioFile',
                    data: dataimport,

                    success: function(data) {
                        percent = 100;
                        clearInterval(window.progressInterval);
                        $('#progress_check' + importType).hide();

                        if(data.success) {

                            if(parseInt(data.html) > 10,000) {
                                Alert('Import', "จำนวน record สูงสุดที่อนุญาติให้ import คือ 10,000 record");
                            } else {
                                $('.btn_check').hide();
                                $('.btn_import').show();

                                $('#check_ret').show();
                                $('#check_ret').html(
                                    showInfoMessage(data.success, 
                                    "รูปแบบไฟล์ ถูกตรวจสอบเรียบร้อย แล้ว",
                                    data.html, "กรุณากดปุ่ม นำเข้าข้อมูล ด้านล่างเพื่อ ดำเนินการ import"));
                            }
                        } else {
                                $('.btn_check').show();
                                $('.btn_import').hide();

                                $('#check_ret').show();
                                $('#check_ret').html(
                                   
                                showInfoMessage(data.success, 
                                    "ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์",
                                    data.html, ""));
                                Alert("Error", "การ ตรวจสอบไฟล์ ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ");
                        }
                    },

                    error: function(xhr, textStatus, thrownError) {
                        clearInterval(window.progressInterval);
                       
                        $('#progress_check' + importType).hide();

                        $('.btn_check').hide();
                        $('.btn_import').show();

                        $('#check_ret').show();
                        $('#check_ret').html(
                                showInfoMessage(false, 
                                    "ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์",
                                   textStatus, ""));
                        Alert("Error", "การ ตรวจสอบไฟล์ ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ");
                    }
                });

                return false;
            }

        }
    }

    // Validate Menu 4 - เมนูนำข้อข้อมูลทรัพย์สินสุทธิ (ตราสารหนี้)
    function validateBondNAVFile(target, importType) {
        $('#check_ret').hide();
        
        var dataimport = new FormData();
        
        var extall="xlsx,xls,xls.xlx";
        //var extall="csv,csv.csv";
        var files = $("#" + target).get(0).files;

        if(!files.length ) { 
            Alert("Import","ท่านยังไม่ได้เลือกไฟล์");
            return false;
        }

        ext = files[0].name.split('.').pop().toLowerCase();

        if(parseInt(extall.indexOf(ext)) < 0)
        {
            $('#progress_check' + importType).hide();
            Alert("Import",'Extension support : ' + extall);
            return false;
        } 

        if(files.length > 0) {
            dataimport.append("exelimport", files[0]);
        }

        ////////////////////////////////////////
        /// From 4
        if ($('#datatype').val() == "4") {
            if($("#smart-form-register").valid()) {  
                dataimport.append('type', importType);

               // dataimport.append('datatype', $('#datatype').val());
                dataimport.append('policy', $('#policy').val());
                
                $('#progress_check' + importType).show();
                $.ajax({
                    cache: false,
                    type: 'POST', 
                    contentType: false,
                    processData: false,
                    url: 'BondDataImport/t2check',
                    data: dataimport,

                    success: function(data) {
                        percent = 100;
                        clearInterval(window.progressInterval);
                        $('#progress_check' + importType).hide();

                        if(data.success) {
                           
                            if(parseInt(data.html) > 10,000) {
                                Alert('Import', "จำนวน record สูงสุดที่อนุญาติให้ import คือ 10,000 record");
                            } else {
                                $('.btn_check').hide();
                                $('.btn_import').show();
                                //$('.btn_check').show();
                                //$('.btn_import').hide();

                                $('#check_ret').show();
                                $('#check_ret').html(
                            
                                    showInfoMessage(data.success, 
                                    "รูปแบบไฟล์ ถูกตรวจสอบเรียบร้อย แล้ว",
                                    data.html, "กรุณากดปุ่ม นำเข้าข้อมูล ด้านล่างเพื่อ นำข้อมูลเข้า"));
                            }
                        } else {
                                $('.btn_check').show();
                                $('.btn_import').hide();

                                $('#check_ret').show();
                                $('#check_ret').html(
                                   
                                showInfoMessage(data.success, 
                                    "ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์",
                                    data.html, ""));
                                Alert("Error", "การ ตรวจสอบไฟล์ ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ");
                        }
                    },

                    error: function(xhr, textStatus, thrownError) {
                        clearInterval(window.progressInterval);
                       
                        $('#progress_check' + importType).hide();
                        
                        $('.btn_check').show();
                        $('.btn_import').hide();

                        $('#check_ret').show();
                        
                        $('#check_ret').html(
                                showInfoMessage(false, 
                                    "ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์",
                                   textStatus, ""));
                        Alert("Error", "การ ตรวจสอบไฟล์ ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ");
                    }
                });
                return false;
            };
        }
    }
    
    //////////////////////////////////////////////
    // Menu 1. Import ข้อมูลซื้อ - ขายหลักทรัพย์
    //////////////////////////////////////////////
    function importTradingFile(target, importType) {
        $('#check_ret').hide();

        var dataimport = new FormData();
        var extall="xlsx,xls.xlx";
        var files = $("#" + target).get(0).files;

        if(files.length) {

            ext = files[0].name.split('.').pop().toLowerCase();
            if(parseInt(extall.indexOf(ext)) < 0) {
                $('#progress_check' + importType).hide();
                Alert("Import",'Extension support : ' + extall);
                return false;

            } else {
                    
                if(files.length > 0) {
                    dataimport.append("exelimport", files[0]);
                }

                if($("#form1-trading-register").valid()) {  

                    dataimport.append('type', $('#datatype').val());
                    dataimport.append('broker', $('#broker').val());
           
                    $('#progress_check' + importType).show();

                    $.ajax({
                        cache: false,
                        type: 'POST', 
                        contentType: false,
                        processData: false,
                        url: 'BondDataImport/t2importTradingTransactionsFile',
                        data: dataimport,

                        success: function(data) {
                            percent = 100;
                            clearInterval(window.progressInterval);
                            $('#progress_check' + importType).hide();

                            if(data.success) {
                                $('.btn_check').hide();
                                $('.btn_import').show();

                                $('#check_ret').show();
                                $('#check_ret').html(
                                    showInfoMessage(data.success, 
                                    "นำเข้าไฟล์ เรียบร้อย แล้ว",
                                    data.html, ""));
                            } else {
                                $('.btn_check').show();
                                $('.btn_import').hide();

                                $('#check_ret').show();
                                $('#check_ret').html(
                                   
                                showInfoMessage(data.success, 
                                    "ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ทีจะนำเข้า",
                                    data.html, ""));
                                Alert("Error", "การ นำเข้าข้อมูล เกิดผิดพลาด กรุณาตรวจสอบรูปแบบข้อมูลของ ไฟล์ทีจะนำเข้า");
                            }
                        },

                        error: function(xhr, textStatus, thrownError) {
                            clearInterval(window.progressInterval);
                           
                            $('#progress_check' + importType).hide();
                            $('.btn_check').hide();
                            $('.btn_import').show();

                            $('#check_ret').show();
                            
                            $('#check_ret').html(
                                    showInfoMessage(false, 
                                        "ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์",
                                       textStatus, ""));
                            Alert("Error", "การ ตรวจสอบไฟล์ ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ");
                        }
                    });

                        return false;
                };
            }
        } else {
                Alert("Import","ท่านยังไม่ได้เลือกไฟล์");
        }
    }
    

    //////////////////////////////////////////////
    // Menu 2. Import Portfolio
    //////////////////////////////////////////////

    function importPortfolioFile(target, importType) {
        $('#check_ret').hide();

        var dataimport = new FormData();

        var extall = "xlsx,xls.xlx";
        var files = $("#" + target).get(0).files;

        if(files.length) {

            ext = files[0].name.split('.').pop().toLowerCase();
            if(parseInt(extall.indexOf(ext)) < 0) {
                $('#progress_check' + importType).hide();
                Alert("Import",'Extension support : ' + extall);
                return false;

            } else {
                    
                if(files.length > 0) {
                    dataimport.append("exelimport", files[0]);
                }

                if($("#form2-portfolio-register").valid()) {  
                    
                    dataimport.append('type', $('#datatype').val());      // datatype == 2
                    dataimport.append('broker', $('#form2broker').val()); // SCB or UOBAM
                    
                    $('#progress_check' + importType).show();

                    $.ajax({
                        cache: false,
                        type: 'POST', 
                        contentType: false,
                        processData: false,
                        url: 'BondDataImport/t2importPortfolioFile',
                        data: dataimport,

                        success: function(data) {
                            console.log('SUCCESS==>' + data.success);
                            percent = 100;
                            clearInterval(window.progressInterval);
                            $('#progress_check' + importType).hide();
                            if(data.success) {
                                $('.btn_check').hide();
                                $('.btn_import').show();

                                $('#check_ret').show();
                                $('#check_ret').html(
                                    showInfoMessage(data.success, 
                                    "นำเข้าไฟล์ เรียบร้อย แล้ว",
                                    data.html, ""));
                            } else {
                                $('.btn_check').show();
                                $('.btn_import').hide();

                                $('#check_ret').show();
                                $('#check_ret').html(
                                   
                                showInfoMessage(data.success, 
                                    "ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ทีจะนำเข้า",
                                    data.html, ""));
                                Alert("Error", "การ นำเข้าข้อมูล เกิดผิดพลาด กรุณาตรวจสอบรูปแบบข้อมูลของ ไฟล์ทีจะนำเข้า");
                            }
                        },

                        error: function(xhr, textStatus, thrownError) {
                            
                            console.log('ERROR==>' + textStatus);
                            clearInterval(window.progressInterval);
                            $('#progress_check' + importType).hide();
                            $('.btn_check').hide();
                            $('.btn_import').show();

                            $('#check_ret').show();
                            
                            $('#check_ret').html(
                                    showInfoMessage(false, 
                                        "Unexpected ERROR : ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์",
                                       textStatus, ""));
                            Alert("Error", "การ ตรวจสอบไฟล์ ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ");
                        }
                    });

                    return false;
                };
            }
        } else {
                Alert("Import","ท่านยังไม่ได้เลือกไฟล์");
        }
    }
    
    
    
    
    /// 
     //////////////////////////////////////////////
    // Menu 8 - Check & Import Stock Universe
    //////////////////////////////////////////////

    function validateStockUniverseFile(target, importType, period_half_year, period_year, file_security_name) {
        $('#check_ret').hide();

        
        var dataimport = new FormData();
        var extall="xlsx,xls.xlx";
        var files = $("#" + target).get(0).files;

        if(!files.length ) { 
            Alert("Import","ท่านยังไม่ได้เลือกไฟล์");
            return false;
        }

        ext = files[0].name.split('.').pop().toLowerCase();

        if(parseInt(extall.indexOf(ext)) < 0) {
            $('#progress_check' + importType).hide();
            Alert("Import",'Extension support : ' + extall);
            return false;
        } 

        if(files.length > 0) {
            dataimport.append("exelimport", files[0]);
        }

        ////////////////////////////////////////
        /// From 8 stock-universe
        if ($('#datatype').val() == "8") {

            if($("#form8-stock-universe-register").valid()) {  
                dataimport.append('datatype', $('#datatype').val()); // menu
                dataimport.append('stockuniversebroker', $('#stockuniversebroker').val());

                dataimport.append('period_half_year', period_half_year); // 1st or 2rd half of the year ?
                dataimport.append('period_year', period_year);
                dataimport.append('file_security_name',file_security_name);
                $('#progress_check' + importType).show();

                $.ajax({
                    cache: false,
                    type: 'POST', 
                    contentType: false,
                    processData: false,
                    url: 'BondDataImport/t2checkStockUniverse',
                    data: dataimport,

                    success: function(data) {
                        percent = 100;
                        clearInterval(window.progressInterval);
                        $('#progress_check' + importType).hide();

                        if(data.success) {

                            if(parseInt(data.html) > 10,000) {
                                Alert('Import', "จำนวน record สูงสุดที่อนุญาติให้ import คือ 10,000 record");
                            } else {
                                $('.btn_check').hide();
                                $('.btn_import').show();

                                $('#check_ret').show();
                                $('#check_ret').html(
                                    showInfoMessage(data.success, 
                                    "รูปแบบไฟล์ ถูกตรวจสอบเรียบร้อย แล้ว",
                                    data.html, "กรุณากดปุ่ม นำเข้าข้อมูล ด้านล่างเพื่อ ดำเนินการ import"));
                            }
                        } else {
                                $('.btn_check').show();
                                $('.btn_import').hide();

                                $('#check_ret').show();
                                $('#check_ret').html(
                                   
                                showInfoMessage(data.success, 
                                    "ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์",
                                    data.html, ""));
                                Alert("Error", "การ ตรวจสอบไฟล์ ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ");
                        }
                    },

                    error: function(xhr, textStatus, thrownError) {
                        clearInterval(window.progressInterval);
                       
                        $('#progress_check' + importType).hide();

                        $('.btn_check').hide();
                        $('.btn_import').show();

                        $('#check_ret').show();
                        $('#check_ret').html(
                                showInfoMessage(false, 
                                    "ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์",
                                   textStatus, ""));
                        Alert("Error", "การ ตรวจสอบไฟล์ ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ");
                    }
                });

                return false;
            }

        }
    }


    /////////////////////////////////////////////////////////
    // Menu ภ. Import ข้อมูลมูลค่าทรัพย์สินสุทธิ และอัตราผลตอบแทน
    /////////////////////////////////////////////////////////
    function importBenefitFile(target, importType) {
        $('#check_ret').hide();

        var dataimport = new FormData();

        var extall="xlsx,xls,xls.xlx";
        /// var extall="csv,csv.csv";
        var files = $("#" + target).get(0).files;

        if(files.length) {
            ext = files[0].name.split('.').pop().toLowerCase();
            if(parseInt(extall.indexOf(ext)) < 0)
            {
                $('#progress_check' + importType).hide();
                Alert("Import",'Extension support : ' + extall);
                return false;

            } else {
                
                if(files.length > 0) {
                    dataimport.append("exelimport", files[0]);
                }

                if($("#smart-form-register").valid()) {  

                    dataimport.append('type', importType);
                    dataimport.append('policy', $('#policy').val());
                    
                    $('#progress_check' + importType).show();
                    $.ajax({
                        cache: false,
                        type: 'POST', 
                        contentType: false,
                        processData: false,
                        url: 'BondDataImport/t2import',
                        data: dataimport,

                        success: function(data) {
                            percent = 100;
                            clearInterval(window.progressInterval);
                            $('#progress_check' + importType).hide();

                            if(data.success) {
                                //$('.btn_check').hide();
                                //$('.btn_import').show();
                                $('.btn_check').show();
                                $('.btn_import').hide();

                                $('#check_ret').show();
                                $('#check_ret').html(
                                    showInfoMessage(data.success, 
                                    "นำเข้าไฟล์ เรียบร้อย แล้ว",
                                    data.html, ""));
                            } else {
                                $('.btn_check').show();
                                $('.btn_import').hide();

                                $('#check_ret').show();
                                $('#check_ret').html(
                                   
                                showInfoMessage(data.success, 
                                    "ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ทีจะนำเข้า",
                                    data.html, ""));
                                Alert("Error", "การ นำเข้าข้อมูล เกิดผิดพลาด กรุณาตรวจสอบรูปแบบข้อมูลของ ไฟล์ทีจะนำเข้า");
                            }
                        },

                        error: function(xhr, textStatus, thrownError) {
                            clearInterval(window.progressInterval);
                           
                            $('#progress_check' + importType).hide();
                            //$('.btn_check').hide();
                            //$('.btn_import').show();
                            $('.btn_check').show();
                            $('.btn_import').hide();

                            $('#check_ret').show();
                            
                            $('#check_ret').html(
                                    showInfoMessage(false, 
                                        "ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์",
                                       textStatus, ""));
                            Alert("Error", "การ ตรวจสอบไฟล์ ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ");
                        }
                    });

                    return false;
                };
            }
        } else {
            Alert("Import","ท่านยังไม่ได้เลือกไฟล์");
        }
    }

    function importStockUniverseFile(target, importType,period_half_year, period_year,file_security_name) {
        $('#check_ret').hide();

        var dataimport = new FormData();

        var extall="xlsx,xls.xlx";
        var files = $("#" + target).get(0).files;

        if(files.length) {

            ext = files[0].name.split('.').pop().toLowerCase();
            if(parseInt(extall.indexOf(ext)) < 0) {
                $('#progress_check' + importType).hide();
                Alert("Import",'Extension support : ' + extall);
                return false;

            } else {
                    
                if(files.length > 0) {
                    dataimport.append("exelimport", files[0]);
                }

                if($("#form8-stock-universe-register").valid()) {  

                    dataimport.append('datatype', $('#datatype').val()); 
                    dataimport.append('stockuniversebroker', $('#stockuniversebroker').val());  // บริษัทจัดการ (Securities Name $NAME_SHT)
                    
                    dataimport.append('period_half_year', period_half_year); // 1st or 2rd half of the year ?
                    dataimport.append('period_year', period_year);
                    dataimport.append('file_security_name',file_security_name);


                    $('#progress_check' + importType).show();

                    $.ajax({
                        cache: false,
                        type: 'POST', 
                        contentType: false,
                        processData: false,
                        url: 'BondDataImport/t2importStockUniverse',
                        data: dataimport,

                        success: function(data) {
                            percent = 100;
                            clearInterval(window.progressInterval);
                            $('#progress_check' + importType).hide();

                            if(data.success) {
                                $('.btn_check').show();
                                $('.btn_import').hide();

                                $('#check_ret').show();
                                $('#check_ret').html(
                                    showInfoMessage(data.success, 
                                    "นำเข้าไฟล์ เรียบร้อย แล้ว",
                                    data.html, ""));
                            } else {
                                $('.btn_check').show();
                                $('.btn_import').hide();

                                $('#check_ret').show();
                                $('#check_ret').html(
                                   
                                showInfoMessage(data.success, 
                                    "ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ทีจะนำเข้า",
                                    data.html, ""));
                                Alert("Error", "การ นำเข้าข้อมูล เกิดผิดพลาด กรุณาตรวจสอบรูปแบบข้อมูลของ ไฟล์ทีจะนำเข้า");
                            }
                        },

                        error: function(xhr, textStatus, thrownError) {
                            clearInterval(window.progressInterval);
                           
                            $('#progress_check' + importType).hide();
                            $('.btn_check').hide();
                            $('.btn_import').show();

                            $('#check_ret').show();
                            
                            $('#check_ret').html(
                                    showInfoMessage(false, 
                                        "ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์",
                                       textStatus, ""));
                            Alert("Error", "การ ตรวจสอบไฟล์ ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ");
                        }
                    });

                        return false;
                };
            }
        } else {
                Alert("Import","ท่านยังไม่ได้เลือกไฟล์");
        }
    }
    
    
    function RenderForm(data) {

        // display search form
        $("#form4-search-form").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
        $("#form4-search-form").html(data.html);
        $("#form4-search-form").fadeIn('300');

        
        meaDatepicker("date_start","date_end");
        meaDatepicker("date_end");

        // hanlde button search clicked
        $('#btn_search').on('click',function() {
            //var PageSizeAll = $('#page-size-search').val();

            var page_size = 20;
            var page = 1;
         
            // see: ajax_p2_equity_unit_val_form.blade.php
            //var symbol      = $("#autocomplete-dynamic").val(); //symbol
            var investment  = $("#investment").val();  // investment
            var securities  = $("#securities").val();  // securities 

            var date_start  = $('#hd_date_start').val();
            var date_end    = $('#hd_date_end').val();
            
            var parameters  = {
                                pagesize: page_size, 
                                PageNumber: page, 
                                investment: investment, 
                                securities: securities, 
                                date_start: date_start,
                                date_end: date_end
                            };


            console.log(parameters);
           
            var method = "BondDataImport/bondUnitValIndex";
            MeaAjax(parameters, method, Render);

            return false;

        });

        $("#unit_val_edit").on('click',function(){
             
            var checkcount = $(".item_checked:checked").length;
            console.log('unit_val_edit: ' + $(".item_checked:checked").val());
            if(checkcount > 1 || checkcount  == 0){
                $.SmartMessageBox({
                    title : "Error!",
                    content : "ไม่สามารถแก้ไขได้ กรุณาเลือกรายการเพียงรายการเดียว",
                    buttons : '[OK]'
                }, function(ButtonPressed) {
                    if (ButtonPressed === "OK") {

                         return false;
                    }
                    if (ButtonPressed === "No") {

                    }

                });
            }else {
                window.location.href = "/admin/BondDataImport/bondUnitVal/" + $(".item_checked:checked").val();
            }
        });

        


        
    }   

    /**
     * Form4
     */
    function Render(data) {
        $(".result").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
        $(".result").html(data.html);
        $(".result").fadeIn('300');


        $("#mainCheckEquity").on("click",function(){$(".item_checked").not(this).prop('checked', this.checked);});
        $("#mainCheckBond").on("click",function(){$(".item_checked").not(this).prop('checked', this.checked);});
 
        

       
        // Pagesize
        $("#page-size-search").on('change',function() {
            // hide dropdown menu
            $("body").trigger("click");

             
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            var val = $(this).val();

            var investment  = $("#investment").val();  // investment
            var securities  = $("#securities").val();  // securities 

            var date_start  = $('#hd_date_start').val();
            var date_end    = $('#hd_date_end').val();
            
            var parameters  = {
                                pagesize: val, 
                                PageNumber: 1, 
                                investment: investment, 
                                securities: securities, 
                                date_start: date_start,
                                date_end:   date_end,
                            };
           
            // $ArrParam
            var method = "BondDataImport/bondUnitValIndex";
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(parameters, method, Render);

        });

        $("#delete_record").on('click',function(){
            var checkcount = $(".item_checked:checked").length;

            if(checkcount > 0){
                var checked = "";
                $(".item_checked").each(function(){
                    if($(this).is(":checked")){
                        checked = checked + $(this).val() + ",";
                    }
                });
                var jsondata = {record_id : checked};
                //alert(checked);
                //return;


                $.ajax({
                    type: 'post', // or post?
                    dataType: 'json',
                    url: '/admin/BondDataImport/deleteUnitValRecords',
                    data: jsondata,
                    success: function(data) {
                        if(data.ret == "1"){
                            $.smallBox({
                                title: "Congratulations! Your form was submitted",
                                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                                color: "#5F895F",
                                iconSmall: "fa fa-check bounce animated",
                                timeout: 4000
                            });

                            $.SmartMessageBox({
                                title : "เรียบร้อย",
                                content : "ลบข้อมูลเรียบร้อยโดยไม่มีข้อผิดพลาด",
                                buttons : '[OK]'
                            }, function(ButtonPressed) {
                                if (ButtonPressed === "OK") {
                                    $("#btn_search").trigger('click');
                                }
                                if (ButtonPressed === "No") {

                                }
                            });
                            
                        }
                    },
                    error: function(xhr, textStatus, thrownError) {
                    }
                });
            }else {
                $.SmartMessageBox({
                    title : "Error!",
                    content : "ท่านยังไม่ได้เลือกรายการ",
                    buttons : '[OK]'
                }, function(ButtonPressed) {
                    if (ButtonPressed === "OK") {

                    }
                    if (ButtonPressed === "No") {

                    }
                });
            }
        });

        /* delete only 1 record */
        $(".mea_delete_by").on('click',function(){
            // [B.5908bb8727f40-2017-05-02]
            var id = $(this).attr("data-id");
            $.SmartMessageBox({
                title : "คำเตือน",
                content : "ท่านแน่ใจที่ต้องการจะลบ รายการที่ท่านเลือก [" + id + "]",
                buttons : '[ยกเลิก][OK]'

            }, function(ButtonPressed) {
                if (ButtonPressed === "OK") {
                    var policy_id = 1;
                    if(id.substring(0, 2) =='E.')
                       policy_id = 1;
                    else
                       policy_id = 2;
                        
                    //var jsondata = {group_id : id, plan_id : plan_id};
                    //alert(policy_id);
                    //return;

                    var jsondata = {policy_id: policy_id,
                                   record_id : id};
 
                    $.ajax({

                        type: 'post', 
                        dataType: 'json',
                        url: '/admin/BondDataImport/deleteUnitVal',
                        data: jsondata,

                        success: function(data) {

                            if(data.ret == "1"){
                                $.smallBox({
                                    title: "Congratulations! Your form was submitted",
                                    content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                                    color: "#5F895F",
                                    iconSmall: "fa fa-check bounce animated",
                                    timeout: 4000
                                });

                                //window.location.href = '/admin/BondDataImport/';
                                $.SmartMessageBox({
                                    title : "เรียบร้อย",
                                    content : "ลบข้อมูลเรียบร้อยโดยไม่มีข้อผิดพลาด",
                                    buttons : '[OK]'
                                }, function(ButtonPressed) {
                                    if (ButtonPressed === "OK") {
                                       $("#btn_search").trigger('click');
                                    }
                                    if (ButtonPressed === "No") {

                                    }
                                });
                            } else {
                                 $.SmartMessageBox({
                                    title : "มีข้อผิดพลาด!",
                                    content : "ไม่สามารถ ลบข้อมูล ได้",
                                    buttons : "[OK]"
                                }, function(ButtonPressed) {
                                    if (ButtonPressed === "OK") {
                                         return false;
                                    }
                                    if (ButtonPressed === "No") {
                                    }
                                });
                             }
                        },

                        error: function(xhr, textStatus, thrownError) {
//                                alert(xhr.status);
//                                alert(thrownError);
//                                alert(textStatus);
                        }
                    });
                }
                if (ButtonPressed === "ยกเลิก") {

                }

            });

        });
        

    }

    
</script>

@stop