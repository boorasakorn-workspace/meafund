
{{-- FILE: p2_bond_tab2_edit_category_page.blade.php --}}

@extends('backend.layouts.default')
@section('content')

<?php
/**
 * Menu items
 */
$data = getmemulist();
/**
 * Menu Sidebar
 */
$arrSidebar =getSideBar($data);
?>

<link rel="stylesheet" href="{{asset('backend/css/jquery-ui.css')}}">
<style>
  .custom-combobox {
    position: relative;
    display: inline-block;
  }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
  }
  .custom-combobox-input {
    margin: 0;
    padding: 5px 10px;
  }

  optgroup {
    background-color: #FF8000; //#DCDCDC;
    color: white;
  }
  option {
    background-color: white;
    color: black;
  }
   .disable { opacity : .35; background-color:lightgray; border:1px solid gray;}

</style>

    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-table fa-fw "></i>

                    {{getMenutitle($arrSidebar)}}
                    
                </h1>
            </div>

        </div>


        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                         
                        <form id="smart-form-register" action=""   class="smart-form">

                            {!! csrf_field() !!}
                            <fieldset>
                                <section>
                                    <lable style="font-size:18px">รหัสหมวดหมู่ตราสารหนี้</lable>
                                    <label class="input">
                                        <input type="text" class="disable" id="cate_id" name="cate_id" placeholder="รหัสหมวดหมู่ตราสารหนี้" value="{{$editdata->CATE_ID}}" readonly >
                                    </label>
                                </section>
                                
                            
                                
                                <section>
                                    <lable style="font-size:18px">กลุ่มอุตสาหกรรม(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                    <label class="input">
                                        <input type="text" id="industrial" name="industrial" placeholder="ระบุกลุ่มอุตสาหกรรม" value="{{$editdata->BOND_CATE_THA}}" >
                                        <b class="tooltip tooltip-bottom-right">ระบุกลุ่มอุตสาหกรรม</b> 
                                    </label>
                                </section>

                                <section>
                                    <lable style="font-size:18px">หมวดธุรกิจ(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                    <label class="input">
                                        <input type="text" id="bu" name="bu"  placeholder="&nbsp;ระบุหมวดธุรกิจ&nbsp;" value="{{$editdata->BOND_TYPE_THA}}" >
                                        <b class="tooltip tooltip-bottom-right">ระบุหมวดธุรกิจ</b> 
                                    </label>
                                </section>

                                <section>
                                    <lable style="font-size:18px">กลุ่มอุตสาหกรรม (<lable style="font-size:18px; color:blue;">ภาษาอังกฤษ</lable>)</lable>
                                    <label class="input">
                                        <input type="text" id="industrial_eng" name="industrial_eng" placeholder="ระบุกลุ่มอุตสาหกรรม" value="{{$editdata->BOND_CATE_ENG}}" >
                                        <b class="tooltip tooltip-bottom-right">ระบุกลุ่มอุตสาหกรรม</b> 
                                    </label>
                                </section>

                                <section>
                                    <lable style="font-size:18px">หมวดธุรกิจ (<lable style="font-size:18px; color:blue;">ภาษาอังกฤษ</lable>)</lable>
                                    <label class="input">
                                        <input type="text" id="bu_eng" name="bu_eng"  placeholder="&nbsp;ระบุหมวดธุรกิจ&nbsp;" value="{{$editdata->BOND_TYPE_ENG}}" >
                                        <b class="tooltip tooltip-bottom-right">ระบุหมวดธุรกิจ</b> 
                                    </label>
                                </section>

                            </fieldset>

                            <footer>
                                <button type="button"  id="btn_form" class="btn btn-primary">ยืนยัน
                                </button>
                                <button type="button" class="btn btn-default" onclick="window.history.back();">
                                    ยกเลิก
                                </button>
                            </footer>
                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->

    </div>


    <!-- PAGE RELATED PLUGIN(S) -->
    <script src="{{asset('backend/js/plugin/jquery-form/jquery-form.min.js')}}"></script>
    
    <script type="text/javascript">

        $(document).ready(function() {

            /* validator method */ 
            $.validator.addMethod("valueNotEquals", function(value, element, arg) {
                return arg != value;
            }, "Please Choose one");

            /* register validattion */
            $("#smart-form-register").validate({

                /* rules for form validation */
                rules : {
                    industrial : {
                        required : true
                    },

                    bu : {
                        required : true
                    }
                },

                errorPlacement : function(error, element) {
                    error.insertAfter(element.parent());

                }
            });


            $("#btn_form").on('click',function(){
             
                if($("#smart-form-register").valid()){
                    var r_cate_id       = $("#cate_id").val();
                    
                    var r_industrial    = $("#industrial").val();
                    var r_bu            = $("#bu").val();

                    var r_industrial_eng    = $("#industrial_eng").val();
                    var r_bu_eng            = $("#bu_eng").val();

                    var jsondata = {
                        cate_id: r_cate_id, /* for required if editing */
                        industrial: r_industrial,
                        bu: r_bu,
                        industrial_eng: r_industrial_eng,
                        bu_eng: r_bu_eng
                    };

                    console.log(' POST: /admin/BondCompany/editsCategory' +  cate_id);
                    MeaAjax(jsondata, "/admin/BondCompany/editsCategory", function(mresponse) {
                        if(mresponse.success){
                            AlertSuccess("บันทึกจัดการแก้ไขหมวดหมู่หลักทรัพย์ร้อยแล้ว",function(){
                                window.location.href = "/admin/BondCompany/category";
                            });

                        } else {
                           Alert("มีข้อผิดพลาด", mresponse.html, null, null);
                        }
                    });

                    return false;
                }

                return false;
            });

        });

    </script>

@stop
