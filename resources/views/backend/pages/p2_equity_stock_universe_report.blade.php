@extends('backend.layouts.default')
@section('content')
<?php
$data = getmemulist();
$arrSidebar =getSideBar($data);
?>

<style>
   .glyphicon-plus {
       color: #00FF00; 
   }
   .glyphicon-danger {
       color: #b41111; 
   }
   .glyphicon-lightgreen {
       color: #12940d; 
   }
   .glyphicon-green {
       color: #00FF00; 
   }
   .glyphicon-info {
       color: #5bc0de; 
   }
   .glyphicon-pink {
       color: #f20772; 
   }
   .glyphicon-luna {
       color: #c92456; 
   }

   /* table split row */
   .top_row_line {
    display: table;
    width: 100%;
   }

   .top_row_line > div {
    display: table-cell;
    width: 50%;
    border-bottom: 1px solid #ccc;
   }
   /* auto complete */
   .container { width: 800px; margin: 0 auto; }

   .autocomplete-suggestions { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 1px solid #999; background: #FFF; cursor: default; overflow: auto; -webkit-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); -moz-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); }
   .autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
   .autocomplete-no-suggestion { padding: 2px 5px;}
   .autocomplete-selected { background: #F0F0F0; }
   .autocomplete-suggestions strong { font-weight: bold; color: #000; }
   .autocomplete-group { padding: 2px 5px; }
   .autocomplete-group strong { font-weight: bold; font-size: 18px; color: #000; display: block; border-bottom: 1px solid #000; }

   /* input { font-size: 23px;  }*/

</style>

<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>

                {{getMenutitle($arrSidebar)}}

            </h1>
        </div>

    </div>


    <div class="row" id="widget-grid" >

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <ul class="nav nav-tabs pull-left in">

                        <li class="active">

                            <a data-toggle="tab" href="#hr1"> <i class="fa fa-lg fa-arrow-circle-o-down"></i> <span class="hidden-mobile hidden-tablet"> ระบุเงื่อนไขการค้นหา </span> </a>

                        </li>

                        <!--li>
                            <a data-toggle="tab" href="#hr2"> <i class="fa fa-lg fa-arrow-circle-o-up"></i> <span class="hidden-mobile hidden-tablet"> รายงานสรุปของตราสารทุน  </span> </a>
                        </li-->

                    </ul>
                </header>

                <!-- widget div-->
                <div>


                    <!-- widget content -->
                    <div class="widget-body">

                        <div class="tab-content">
                            <div class="tab-pane active" id="hr1">

                                <div class="widget-body no-padding" style="width: 100% ;margin: 0 auto;background-color: #000000;border: 1px solid #E1E8F3">
                                    <header style="color: #fff; height: 40px; line-height: 40px; font-size: 18px; background-color: #a90329;padding-left: 20px;"> กล่องค้นหา </header>

                                    <form  id="comment-form_darkman" class="smart-form" novalidate="novalidate">

                                        <fieldset>
                                            <div class="row">
                                                <section class="col col-4">
                                                    <label class="label"><!--input type="checkbox" id="check_name"  value="name"-->&nbsp;ชื่อหลักทรัพย์</label>
                                                    <label class="input">
                                                         
                                                          <input type="text" name="country" placeholder="ระบุชื่อหลักทรัพย์"
                                                          id="autocomplete-dynamic" 
                                                          style="width: 100%; border-width: 1px;"/>
         
                                                        <!--select name="plan" id="name" class="form-control">
                                                            <option value="">ชื่อหลักทรัพย์</option>
                                                            @if($equitylist)
                                                                @foreach($equitylist as $item)
                                                                    <option value="{{$item->NAME_SHT}}">&nbsp;{{$item->NAME_SHT}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select-->
                                                    </label>
                                                </section>
                                                <section class="col col-4">
                                                    <label class="label"><!--input type="checkbox" id="check_depart" value="depart"-->&nbsp;หมวดหมู่หลักทรัพย์</label>
                                                    <label class="input">
                                                        
                                                        <select name="depart" id="depart" class="form-control">
                                                            <option value="">หมวดหมู่หลักทรัพย์</option>
                                                            @if($equitycategory)
                                                                @foreach($equitycategory as $item)
                                                                    <option value="{{$item->INDUSTRIAL}}">&nbsp;{{$item->INDUSTRIAL}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>

                                                    </label>
                                                </section>
                                                <section class="col col-4">
                                                    <label class="label"><!--input type="checkbox" id="check_plan" value="plan"-->&nbsp;ชื่อบริษัท จัดการ</label>
                                                    <label class="input">
                                                        <select name="plan" id="plan" class="form-control">
                                                            <option value="">ชื่อบริษัท จัดการ</option>
                                                            @if($equitylist)
                                                                @foreach($equitylist as $item)
                                                                    <option value="{{$item->NAME_SHT}}">&nbsp;{{$item->NAME_SHT}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                        {{--<input type="text" name="plan" id="plan">--}}
                                                    </label>
                                                </section>
                                            </div>

                                            <div class="row">
                                                <section class="col col-4">
                                                    <label class="label"><input type="checkbox" id="check_period_1h" value="1H"> &nbsp;ครึ่งปีแรก ( ม.ค. - มิ.ย. )</label>
                                                    <label class="input"><!--i class="icon-append fa fa-calendar"></i-->
                                                        <!--input type="text" class="mea_date_picker" id="date_start"-->
                                                         <!--select name="period_half1" id="period_half1" class="form-control">
                                                             <option value="1H">&nbsp;ครึ่งปีแรก ( ม.ค. - มิ.ย. )</option>
                                                             <option value="2H">&nbsp;ครึ่งปีหลัง ( ก.ค. - ธ. ค. )</option>
                                                         </select-->
                                                         
                                                         <select name="period_year1" id="period_year1" class="form-control" style="display:none;">
                                                             <option value="">เลือกปี</option>
                                                             @foreach($years as $year)
                                                                <option value="{{$year}}">&nbsp;{{$year}}</option>
                                                             @endforeach
                                                         </select>
                                                    </label>
                                                </section>
                                                <section class="col col-4">
                                                    <label class="label"><input type="checkbox" id="check_period_2h" value="2H"> &nbsp;ครึ่งปีหลัง ( ก.ค. - ธ. ค. )</label>
                                                    <label class="input"><!--i class="icon-append fa fa-calendar"></i-->
                                                        <!--input type="text" class="mea_date_picker" id="date_end"-->
                                                         <!--select name="period_half2" id="period_half2" class="form-control">
                                                             <option value="1H">&nbsp;ครึ่งปีแรก ( ม.ค. - มิ.ย. )</option>
                                                             <option value="2H">&nbsp;ครึ่งปีหลัง ( ก.ค. - ธ. ค. )</option>
                                                         </select>
                                                         <br/--> 
                                                         <select name="period_year2" id="period_year2" class="form-control" style="display:none;">
                                                             <option value="">เลือกปี</option>
                                                             @foreach($years as $year)
                                                                <option value="{{$year}}">&nbsp;{{$year}}</option>
                                                             @endforeach
                                                         </select>
                                                    </label>
                                                </section>

                                                <section class="col col-4">

                                                    <label class="label"></label>
                                                    <label class="input">
                                                    <button type="submit" id="btn_search" style="padding: 5px 20px 5px 20px; margin-top:20px;font-size: 18px;" name="submit" class="btn btn-primary">
                                                        ค้นหา
                                                    </button>
                                                    </label>
                                                </section>
                                            </div>
                                        </fieldset>




                                    </form>

                                </div>


                                <a href="#" id="export_search" style="margin-top: 30px;" class="btn btn-labeled btn-success mea-btn-export"> <span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i></span>Export </a>
                                <!--a href="#" id="btn_chart"     style="margin-top: 30px;" class="btn btn-labeled btn-warning"> <span class="btn-label"><i class="glyphicon glyphicon-signal"></i></span>Chart </a-->
                               
                                <!-- Widget ID (each widget will need unique ID)-->
                                <div class="jarviswidget jarviswidget-color-blueDark" style="margin-top: 5px;" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
                                    <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                    data-widget-colorbutton="false"
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true"export_search
                                    data-widget-sortable="false"

                                    -->

                                    <select class="form-control mea-pagesize" id="page-size-search">
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>

                                    <header>
                                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    </header>

                                    <!-- widget div-->
                                    <div>

                                        <!-- widget edit box -->
                                        <div class="jarviswidget-editbox">
                                            <!-- This area used as dropdown edit box -->

                                        </div>
                                        <!-- end widget edit box -->

                                        <!-- widget content -->
                                        <div class="widget-body">
                                            <p class="report-title">
                                                  {{--getMenutitle($arrSidebar)--}}
                                            </p>
                                            <p class="report-period"> </p>
                                            <div class="table-responsive">
                                                <div id="serch_data">

                                                </div>
                                            </div>

                                        </div>
                                        <!-- end widget content -->

                                    </div>
                                    <!-- end widget div -->

                                </div>
                                <!-- end widget -->

                            </div>
                            

                        </div>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->



        </article>
        <!-- WIDGET END -->



    </div>



</div>
<!-- END MAIN CONTENT -->
<script src="{{asset('backend/js/plugin/jquery-autocomplete/scripts/jquery.mockjax.js')}}"></script> 
<script src="{{asset('backend/js/plugin/jquery-autocomplete/src/jquery.autocomplete.js')}}"></script> 
 
<script type="text/javascript">
var symbols = { 
    @foreach($symbols as $item)
    '{{$item->SYMBOL}}' : '{{$item->SYMBOL}} | {{$item->COMP_NAME}}', 
    @endforeach

}
    /*"AD": "Andorra",
    "A2": "Andorra Test",
    "AE": "United Arab Emirates",
    "AF": "Afghanistan",
    "AG": "Antigua and Barbuda",
    "AI": "Anguilla",
    "AL": "Albania",
    "AM": "Armenia",
    "AN": "Netherlands Antilles",
    "AO": "Angola",
    "AQ": "Antarctica",
    "AR": "Argentina",
    "AS": "American Samoa",
    "AT": "Austria",
    "AU": "Australia",
    "AW": "Aruba",
    "AX": "\u00c5land Islands",
    "AZ": "Azerbaijan",
    "BA": "Bosnia and Herzegovina",
    "BB": "Barbados",
    "BD": "Bangladesh",
    "BE": "Belgium",
    "BF": "Burkina Faso",
    "BG": "Bulgaria",
    "BH": "Bahrain",
    "BI": "Burundi",
    "BJ": "Benin",
    "BL": "Saint Barth\u00e9lemy",
    "BM": "Bermuda",
    "BN": "Brunei",
    "BO": "Bolivia",
    "BQ": "British Antarctic Territory",
    "BR": "Brazil",
    "BS": "Bahamas",
    "BT": "Bhutan",
    "BV": "Bouvet Island",
    "BW": "Botswana",
    "BY": "Belarus",
    "BZ": "Belize",
    "CA": "Canada",
    "CC": "Cocos [Keeling] Islands",
    "CD": "Congo - Kinshasa",
    "CF": "Central African Republic",
    "CG": "Congo - Brazzaville",
    "CH": "Switzerland",
    "CI": "C\u00f4te d\u2019Ivoire",
    "CK": "Cook Islands",
    "CL": "Chile",
    "CM": "Cameroon",
    "CN": "China",
    "CO": "Colombia",
    "CR": "Costa Rica",
    "CS": "Serbia and Montenegro",
    "CT": "Canton and Enderbury Islands",
    "CU": "Cuba",
    "CV": "Cape Verde",
    "CX": "Christmas Island",
    "CY": "Cyprus",
    "CZ": "Czech Republic",
    "DD": "East Germany",
    "DE": "Germany",
    "DJ": "Djibouti",
    "DK": "Denmark",
    "DM": "Dominica",
    "DO": "Dominican Republic",
    "DZ": "Algeria",
    "EC": "Ecuador",
    "EE": "Estonia",
    "EG": "Egypt",
    "EH": "Western Sahara",
    "ER": "Eritrea",
    "ES": "Spain",
    "ET": "Ethiopia",
    "FI": "Finland",
    "FJ": "Fiji",
    "FK": "Falkland Islands",
    "FM": "Micronesia",
    "FO": "Faroe Islands",
    "FQ": "French Southern and Antarctic Territories",
    "FR": "France",
    "FX": "Metropolitan France",
    "GA": "Gabon",
    "GB": "United Kingdom",
    "GD": "Grenada",
    "GE": "Georgia",
    "GF": "French Guiana",
    "GG": "Guernsey",
    "GH": "Ghana",
    "GI": "Gibraltar",
    "GL": "Greenland",
    "GM": "Gambia",
    "GN": "Guinea",
    "GP": "Guadeloupe",
    "GQ": "Equatorial Guinea",
    "GR": "Greece",
    "GS": "South Georgia and the South Sandwich Islands",
    "GT": "Guatemala",
    "GU": "Guam",
    "GW": "Guinea-Bissau",
    "GY": "Guyana",
    "HK": "Hong Kong SAR China",
    "HM": "Heard Island and McDonald Islands",
    "HN": "Honduras",
    "HR": "Croatia",
    "HT": "Haiti",
    "HU": "Hungary",
    "ID": "Indonesia",
    "IE": "Ireland",
    "IL": "Israel",
    "IM": "Isle of Man",
    "IN": "India",
    "IO": "British Indian Ocean Territory",
    "IQ": "Iraq",
    "IR": "Iran",
    "IS": "Iceland",
    "IT": "Italy",
    "JE": "Jersey",
    "JM": "Jamaica",
    "JO": "Jordan",
    "JP": "Japan",
    "JT": "Johnston Island",
    "KE": "Kenya",
    "KG": "Kyrgyzstan",
    "KH": "Cambodia",
    "KI": "Kiribati",
    "KM": "Comoros",
    "KN": "Saint Kitts and Nevis",
    "KP": "North Korea",
    "KR": "South Korea",
    "KW": "Kuwait",
    "KY": "Cayman Islands",
    "KZ": "Kazakhstan",
    "LA": "Laos",
    "LB": "Lebanon",
    "LC": "Saint Lucia",
    "LI": "Liechtenstein",
    "LK": "Sri Lanka",
    "LR": "Liberia",
    "LS": "Lesotho",
    "LT": "Lithuania",
    "LU": "Luxembourg",
    "LV": "Latvia",
    "LY": "Libya",
    "MA": "Morocco",
    "MC": "Monaco",
    "MD": "Moldova",
    "ME": "Montenegro",
    "MF": "Saint Martin",
    "MG": "Madagascar",
    "MH": "Marshall Islands",
    "MI": "Midway Islands",
    "MK": "Macedonia",
    "ML": "Mali",
    "MM": "Myanmar [Burma]",
    "MN": "Mongolia",
    "MO": "Macau SAR China",
    "MP": "Northern Mariana Islands",
    "MQ": "Martinique",
    "MR": "Mauritania",
    "MS": "Montserrat",
    "MT": "Malta",
    "MU": "Mauritius",
    "MV": "Maldives",
    "MW": "Malawi",
    "MX": "Mexico",
    "MY": "Malaysia",
    "MZ": "Mozambique",
    "NA": "Namibia",
    "NC": "New Caledonia",
    "NE": "Niger",
    "NF": "Norfolk Island",
    "NG": "Nigeria",
    "NI": "Nicaragua",
    "NL": "Netherlands",
    "NO": "Norway",
    "NP": "Nepal",
    "NQ": "Dronning Maud Land",
    "NR": "Nauru",
    "NT": "Neutral Zone",
    "NU": "Niue",
    "NZ": "New Zealand",
    "OM": "Oman",
    "PA": "Panama",
    "PC": "Pacific Islands Trust Territory",
    "PE": "Peru",
    "PF": "French Polynesia",
    "PG": "Papua New Guinea",
    "PH": "Philippines",
    "PK": "Pakistan",
    "PL": "Poland",
    "PM": "Saint Pierre and Miquelon",
    "PN": "Pitcairn Islands",
    "PR": "Puerto Rico",
    "PS": "Palestinian Territories",
    "PT": "Portugal",
    "PU": "U.S. Miscellaneous Pacific Islands",
    "PW": "Palau",
    "PY": "Paraguay",
    "PZ": "Panama Canal Zone",
    "QA": "Qatar",
    "RE": "R\u00e9union",
    "RO": "Romania",
    "RS": "Serbia",
    "RU": "Russia",
    "RW": "Rwanda",
    "SA": "Saudi Arabia",
    "SB": "Solomon Islands",
    "SC": "Seychelles",
    "SD": "Sudan",
    "SE": "Sweden",
    "SG": "Singapore",
    "SH": "Saint Helena",
    "SI": "Slovenia",
    "SJ": "Svalbard and Jan Mayen",
    "SK": "Slovakia",
    "SL": "Sierra Leone",
    "SM": "San Marino",
    "SN": "Senegal",
    "SO": "Somalia",
    "SR": "Suriname",
    "ST": "S\u00e3o Tom\u00e9 and Pr\u00edncipe",
    "SU": "Union of Soviet Socialist Republics",
    "SV": "El Salvador",
    "SY": "Syria",
    "SZ": "Swaziland",
    "TC": "Turks and Caicos Islands",
    "TD": "Chad",
    "TF": "French Southern Territories",
    "TG": "Togo",
    "TH": "Thailand",
    "TJ": "Tajikistan",
    "TK": "Tokelau",
    "TL": "Timor-Leste",
    "TM": "Turkmenistan",
    "TN": "Tunisia",
    "TO": "Tonga",
    "TR": "Turkey",
    "TT": "Trinidad and Tobago",
    "TV": "Tuvalu",
    "TW": "Taiwan",
    "TZ": "Tanzania",
    "UA": "Ukraine",
    "UG": "Uganda",
    "UM": "U.S. Minor Outlying Islands",
    "US": "United States",
    "UY": "Uruguay",
    "UZ": "Uzbekistan",
    "VA": "Vatican City",
    "VC": "Saint Vincent and the Grenadines",
    "VD": "North Vietnam",
    "VE": "Venezuela",
    "VG": "British Virgin Islands",
    "VI": "U.S. Virgin Islands",
    "VN": "Vietnam",
    "VU": "Vanuatu",
    "WF": "Wallis and Futuna",
    "WK": "Wake Island",
    "WS": "Samoa",
    "YD": "People's Democratic Republic of Yemen",
    "YE": "Yemen",
    "YT": "Mayotte",
    "ZA": "South Africa",
    "ZM": "Zambia",
    "ZW": "Zimbabwe",
    "ZZ": "Unknown or Invalid Region"
}*/
    $(document).ready(function(){
        /*
        if($("#check_period_1h").is(':checked')) {
           $("#period_year1").show();
        } else {
           $("#period_year1").hide();
        }
        */
        var check_period_1h = document.getElementById('check_period_1h');
        var period_year1 = document.getElementById('period_year1');
        check_period_1h.onchange = function() {
             // Check if the checkbox is checked, and show/hide the text field.
          //period_year1.hidden = this.checked ? false : true;
          if(this.checked) 
                $('#period_year1').show();
            else
                $('#period_year1').hide(); 

        };
        var check_period_2h = document.getElementById('check_period_2h');
        var period_year2 = document.getElementById('period_year2');
        check_period_2h.onchange = function() {
             // Check if the checkbox is checked, and show/hide the text field.
             //alert('checked');
            if(this.checked) 
                $('#period_year2').show();
            else
                $('#period_year2').hide(); 

        };

        

        $('#export_search').on('click',function() {
            var PageSizeAll = $('#page-size-search').val();
            var EmpID = $('#name').val();
            var depart = $('#depart').val();
            var plan = $('#plan').val();
            var date_start = $('#hd_date_start').val();
            var date_end =$('#hd_date_end').val();


            var check_name =$('#check_name').is(':checked');
            var check_depart =$('#check_depart').is(':checked');
            var check_plan =$('#check_plan').is(':checked');
            var check_date =$('#check_date').is(':checked');

            window.location.href =  "EquityStockUniverseReport/exportsearch?EmpID=" + EmpID +"&depart=" + depart +
                                    "&plan=" + plan + "&date_start=" +date_start + "&date_end=" + date_end+ 
                                    '&check_name=' + check_name+ '&check_depart=' + check_depart+ '&check_plan=' + 
                                    check_plan+ '&check_date=' + check_date;
            return false;
        });

        meaDatepicker("date_start","date_end");
        meaDatepicker("date_end");

        $('html').append('<input type="hidden" value="1" id="currentpage_search" />');

        $('#btn_search').on('click',function() {
            var PageSizeAll = $('#page-size-search').val();
            var EmpID = $('#name').val();
            var depart = $('#depart').val();
            var plan = $('#plan').val();
            var date_start = $('#hd_date_start').val();
            var date_end =$('#hd_date_end').val();

            var check_name =$('#check_name').is(':checked');
            var check_depart =$('#check_depart').is(':checked');
            var check_plan =$('#check_plan').is(':checked');
            var check_date =$('#check_date').is(':checked');


            if(check_date && date_start != "" && date_end != "" ){
                var str =  'ในช่วงวันที่ ' + GetDateFormat(date_start) + ' ถึง ' + GetDateFormat(date_end);
                $('.report-period').html(str);
            }

            var jsondata = {
                pagesize : PageSizeAll,
                PageNumber:1,
                emp_id : EmpID,
                depart :depart,
                plan : plan,
                date_start:date_start,
                date_end:date_end,
                check_name :check_name,
                check_depart:check_depart,
                check_plan:check_plan,
                check_date:check_date

            };

            console.log(jsondata);
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(jsondata,"EquityStockUniverseReport/search",RenderSearch);

            return false;

        });


        $("#page-size-search").on('change',function() {
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            var val = $(this).val();

            var EmpID = $('#name').val();
            var depart = $('#depart').val();
            var plan = $('#plan').val();
            var date_start = $('#hd_date_start').val();
            var date_end =$('#hd_date_end').val();

            var check_name =$('#check_name').is(':checked');
            var check_depart =$('#check_depart').is(':checked');
            var check_plan =$('#check_plan').is(':checked');
            var check_date =$('#check_date').is(':checked');

            var jsondata = {
                pagesize : val,
                PageNumber:1,
                emp_id : EmpID,
                depart :depart,
                plan : plan,
                date_start:date_start,
                date_end:date_end,
                check_name :check_name,
                check_depart:check_depart,
                check_plan:check_plan,
                check_date:check_date

            };
//            var jsondata = {pagesize : val,PageNumber:1};
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(jsondata,"EquityStockUniverseReport/search",RenderSearch);

        });


        // Initialize autocomplete with custom appendTo:
        var symbolsArray = $.map(symbols, function (value, key) { return { value: value, data: key }; });
        $('#autocomplete-dynamic').autocomplete({
            lookup: symbolsArray
         });

    });  // jquery ready


    function RenderSearch(data){

        $("#serch_data").html(data.html);
        $("#serch_data").fadeIn('300');


        $("#page_click_search li a").on('click',PageRenderSearch);
    }

    function PageRenderSearch(){

        var p = $(this).attr('data-page');
        var page_size = $('#page-size-search').val();
        var CurPage = $('#currentpage_search').val();
        $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');

        if(p == "pre"){
            p = parseInt(CurPage) - 1;
        }

        if(p == "next"){
            p = parseInt(CurPage) + 1;
        }
        $('#currentpage_search').val(p);

        var EmpID = $('#name').val();
        var depart = $('#depart').val();
        var plan = $('#plan').val();
        var date_start = $('#hd_date_start').val();
        var date_end =$('#hd_date_end').val();

        var check_name =$('#check_name').is(':checked');
        var check_depart =$('#check_depart').is(':checked');
        var check_plan =$('#check_plan').is(':checked');
        var check_date =$('#check_date').is(':checked');


        var jsondata = {
            pagesize : page_size,
            PageNumber:p,
            emp_id : EmpID,
            depart :depart,
            plan : plan,
            date_start:date_start,
            date_end:date_end,
            check_name :check_name,
            check_depart:check_depart,
            check_plan:check_plan,
            check_date:check_date

        };

        MeaAjax(jsondata,"EquityStockUniverseReport/search",RenderSearch);
    };

</script>

@stop
