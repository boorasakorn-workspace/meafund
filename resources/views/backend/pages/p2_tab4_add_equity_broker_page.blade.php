
<!-- FILE: p2_tab4_add_equity_broker_page.blade.php -->

@extends('backend.layouts.default')
@section('content')
    <?php
    $data = getmemulist();
    $arrSidebar = getSideBar($data);
    ?>

    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-table fa-fw "></i>
                    {{--Auth::user()->username--}} 
                    {{getMenutitle($arrSidebar)}}
                </h1>
            </div>
        </div>


        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"
                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                    </div>
                    <!-- end widget edit box -->


                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        
                        <form id="smart-form-register" action=""  class="smart-form">
                            {!! csrf_field() !!}

                            <fieldset>
                                <!-- 
                                <section> 
                                    <lable style="font-size:18px">รหัส บริษัท Broker</lable>
                                    <label class="input">
                                        <input type="hidden" id="record_id" name="record_id" placeholder="รหัส บริษัท Broker" readonly>
                                         
                                    </label>
                                </section>
                                -->
                                <section>
                                    <lable style="font-size:18px">ชื่อย่อ(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                    <label class="input">
                                        <input type="text" id="name_sht" name="name_sht" placeholder="ระบุชื่อย่อ">
                                        <b class="tooltip tooltip-bottom-right">ระบุชื่อย่อ</b> 
                                    </label>
                                </section>

                                <section>
                                    <lable style="font-size:18px">ชื่อบริษัท Broker(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                    <label class="input">
                                        <input type="text" id="company_name" name="company_name" placeholder="ระบุชื่อบริษัท Broker">
                                        <b class="tooltip tooltip-bottom-right">ระบุชื่อบริษัท Broker</b> 
                                    </label>
                                </section>

                                <section>
                                    <lable style="font-size:18px">ที่อยู่บริษัท Broker</lable>
                                    <label class="input">
                                        <textarea style="font-size:18px" class="form-control" rows="6" id="company_addr" name="company_addr" placeholder="  ระบุที่อยู่บริษัท Broker    "></textarea>
                                        <b class="tooltip tooltip-bottom-right">ระบุที่อยู่บริษัท Broker</b> 
                                    </label>
                                </section>

                            </fieldset>


                            <fieldset>
                                <div class="row">
                                    <section class="col col-6">
                                        <lable style="font-size:18px">เบอร์ โทรศัพท์</lable>
                                        <label class="input">
                                            <input type="text"  id="company_phone" name="company_phone" placeholder="ระบุเบอร์ โทรศัพท์">
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <lable style="font-size:18px">เบอร์ โทรสาร</lable>
                                        <label class="input">
                                            <input type="text" id="company_fax" name="company_fax" placeholder="ระบุเบอร์ โทรสาร">
                                        </label>
                                    </section>
                                </div>

                            </fieldset>

                            <fieldset>

                                <div class="row">

                                    <section class="col col-6">
                                        <lable style="font-size:18px">วันที่เริ่มต้น</lable>
                                        <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="equity_start"  class="mea_date_picker" id="equity_start" placeholder="ระบุวันที่เริ่มต้น" readonly>
                                           
                                            {{--class="mea_date_picker" data-dateformat='dd/mm/yy'--}}
                                        </label>
                                    </section>

                                    <section class="col col-6">
                                        <lable style="font-size:18px">วันที่สิ้นสุด</lable>
                                        <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="equity_end"  class="mea_date_picker" id="equity_end" placeholder="ระบุวันที่สิ้นสุด" readonly>

                                            {{--class="mea_date_picker" data-dateformat='dd/mm/yy'--}}
                                        </label>
                                    </section>
                                </div>


                            </fieldset>
                            <footer>
                                <div class="row">
                                   <lable style="font-size:18px; color:red;">&nbsp;&nbsp;&nbsp;&nbsp;* Required field ​(ห้ามเป็นค่าว่าง)</lable>   
                                </div>
                                <button type="button"  id="btn_form" class="btn btn-primary">ยืนยัน
                                </button>
                                <button type="button" class="btn btn-default" onclick="window.history.back();">
                                    ยกเลิก
                                </button>
                            </footer>
                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->

    </div>


    <!-- PAGE RELATED PLUGIN(S) -->
    <script src="{{asset('backend/js/plugin/jquery-form/jquery-form.min.js')}}"></script>
    <script src="{{asset('backend/js/plugin/summernote/summernote.min.js')}}"></script>

    <script type="text/javascript">

        $(document).ready(function() {

           /* $(".chk_expire").on('click',function(){

                var res = $(this).val();

                if(res == 1){
                    $("#expire_check").hide();
                }
                if(res == 0){
                    $("#expire_check").show();
                }

            });
           */

            $.validator.addMethod("valueNotEquals", function(value, element, arg) {
                return arg != value;
            }, "Please Choose one");

            


            $("#smart-form-register").validate({

                    // Rules for form validation
                    rules : {
                        
                        name_sht : {
                            required : true
                        },

                        company_name : {
                            required : true
                        },

                        company_addr : {
                            required : false
                        },

                        company_phone : {
                            required : false
                            
                        },
                        
                        company_fax : {
                            required : false
                              
                        },

                        equity_start : {
                            required : false
                        },

                        equity_end : {
                            required : false
                        }
                    },

                    errorPlacement : function(error, element) {
                        error.insertAfter(element.parent());

                    }
                });


            $("#btn_form").on('click',function(){

               /* var $registerForm = $("#smart-form-register").validate({

                    // Rules for form validation
                    rules : {
                        
                        company_code : {
                            required : true,
                        },

                        company_name : {
                            required : true,
                        },

                        company_addr : {
                            required : true,
                        },

                        company_phone : {
                            required : true,
                             // phhone : true,
                        },
                        
                        company_fax : {
                            required : true,
                             //  phone : true,
                        },

                        equity_start : {
                            required : true,
                        },

                        equity_end : {
                            required : true,
                        },
                    },

                    errorPlacement : function(error, element) {
                        error.insertAfter(element.parent());

                    }
                });
*/
                // if($registerForm.valid()){
                if($("#smart-form-register").valid()){
                    var name_sht  = $("#name_sht").val();
                    var company_name  = $("#company_name").val();
                    var company_addr  = $("#company_addr").val();
                    var company_phone = $("#company_phone").val();
                    var company_fax   = $("#company_fax").val();


                    // var equity_start = $('#equity_start').datepicker({ dateFormat: 'dd-mm-yy' }).val();
                    // var equity_start = $('#equity_end').datepicker({ dateFormat: 'dd-mm-yy' }).val();
                  //  var md = Date.parseDate(date:$("#equity_start").val(), pattern:"dd-mm-yyyy", loc:'th-th'));
                   // Alert("ERROR", md , null, null);
                   // return false;
                    var equity_start  = $("#equity_start").val();
                    var equity_end    = $("#equity_end").val();
                    

                    
                    //var user_id       = get_userID();
                    //var plan_status  = $('input[name=plan_status]:checked').val();

                    var jsondata = {
                        'record'    : 'yyyyuuuu',
                        company_name:  company_name,
                        company_addr:  company_addr,
                        company_phone: company_phone,
                        company_fax:   company_fax,
                        equity_start:  equity_start,
                        equity_end:    equity_end,
                        name_sht:  name_sht
                    };

// abort(403, equity_end);
//    $(".result").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
                    MeaAjax(jsondata, "addBroker", function(mresponse) {
                        if(mresponse.success){
                            AlertSuccess("บันทึกจัดการบริษ้ทเรียบร้อยแล้ว",function(){
                                window.location.href = "/admin/EquityCompany/equityBroker";
                            });

                        } else {
                            Alert("ERROR", mresponse.html, null, null);
                        }
                    });

                    return false;
                }
                return false;
            });

            //
            //  meaDatepicker("plan_start","plan_end");
            //  meaDatepicker("plan_end");
            //
            
            meaDatepicker("equity_start", "equity_end");
            meaDatepicker("equity_end");
        });

    </script>

@stop





