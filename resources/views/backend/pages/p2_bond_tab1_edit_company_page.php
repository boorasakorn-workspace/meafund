@extends('backend.layouts.default')
@section('content')
    <?php
    $data = getmemulist();
    $arrSidebar =getSideBar($data);
    ?>

    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-table fa-fw "></i>

                    {{getMenutitle($arrSidebar)}}
                </h1>
            </div>

        </div>


        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>


                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        {{--action="{{action('UserController@postAddUser') }}"--}}

                         <form id="smart-form-register" action=""   class="smart-form">
                            {!! csrf_field() !!}

                            <fieldset>
                                <section>
                                    <lable style="font-size:18px">รหัส บริษัทจัด การหลักทรัพย์</lable>
                                    <label class="input">
                                        <input type="text" id="record_id" name="record_id" placeholder="รหัส บริษัทจัด การหลักทรัพย์" value="{{$editdata->SEC_ID}}" readonly readonly>
                                    </label>
                                </section>
                                
                                <section>
                                    <lable style="font-size:18px">ชื่อย่อ</lable>
                                    <label class="input">
                                        <input type="text" id="name_sht" name="name_sht" placeholder="ระบุชื่อย่อ" value="{{$editdata->NAME_SHT}}" readonly>
                                        <b class="tooltip tooltip-bottom-right">ระบุชื่อย่อ</b> 
                                    </label>
                                </section>

                                <section>
                                    <lable style="font-size:18px">ชื่อบริษัทหลักทรัพย์</lable>
                                    <label class="input">
                                        <input type="text" id="company_name" name="company_name" placeholder="ระบุชื่อบริษัทหลักทรัพย์"
                                            value="{{$editdata->SECURITIES_NAME}}">
                                        <b class="tooltip tooltip-bottom-right">ระบุชื่อบริษัทหลักทรัพย์</b> 
                                    </label>
                                </section>

                                <section>
                                    <lable style="font-size:18px">ที่อยู่บริษัทหลักทรัพย์</lable>
                                    <label class="input">
                                        <textarea style="font-size:18px" class="form-control" rows="6" id="company_addr" name="company_addr" 
                                        placeholder="  ระบุที่อยู่บริษัทหลักทรัพย์    ">{{$editdata->ADDRESS}}</textarea>
                                        <b class="tooltip tooltip-bottom-right">ระบุที่อยู่บริษัทหลักทรัพย์</b> 
                                    </label>
                                </section>

                            </fieldset>


                            <fieldset>
                                <div class="row">
                                    <section class="col col-6">
                                        <lable style="font-size:18px">เบอร์ โทรศัพท์</lable>
                                        <label class="input">
                                            <input type="text"  id="company_phone" name="company_phone" placeholder="ระบุเบอร์ โทรศัพท์"
                                            value="{{$editdata->PHONE}}">
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <lable style="font-size:18px">เบอร์ โทรสาร</lable>
                                        <label class="input">
                                            <input type="text" id="company_fax" name="company_fax" placeholder="ระบุเบอร์ โทรสาร"
                                            value="{{$editdata->FAX}}">
                                        </label>
                                    </section>
                                </div>

                            </fieldset>

                            <fieldset>

                                <div class="row">

                                    <section class="col col-6">
                                        <lable style="font-size:18px">วันที่เริ่มต้น</lable>
                                        <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="equity_start"  class="mea_date_picker" id="equity_start" placeholder="ระบุวันที่เริ่มต้น"  
                                                   value="{{get_date_notime_en($editdata->START_DATE)}}" readonly>
                                           
                                            {{--class="mea_date_picker" data-dateformat='dd/mm/yy'--}}
                                        </label>
                                    </section>

                                    <section class="col col-6">
                                        <lable style="font-size:18px">วันที่สิ้นสุด</lable>
                                        <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="equity_end"  class="mea_date_picker" id="equity_end" placeholder="ระบุวันที่สิ้นสุด" 
                                                  value="{{get_date_notime_en($editdata->END_DATE)}}" readonly>

                                            {{--class="mea_date_picker" data-dateformat='dd/mm/yy'--}}
                                        </label>
                                    </section>
                                </div>


                            </fieldset>
                            <footer>
                                <button type="button"  id="btn_form" class="btn btn-primary">ยืนยัน
                                </button>
                                <button type="button" class="btn btn-default" onclick="window.history.back();">
                                    ยกเลิก
                                </button>
                            </footer>
                        </form>


                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->

    </div>






    <!-- PAGE RELATED PLUGIN(S) -->
    <script src="{{asset('backend/js/plugin/jquery-form/jquery-form.min.js')}}"></script>
    


    <script type="text/javascript">

        $(document).ready(function() {

           /* $(".chk_expire").on('click',function(){

                var res = $(this).val();

                if(res == 1){
                    $("#expire_check").hide();
                }
                if(res == 0){
                    $("#expire_check").show();
                }

            });
           */

            $.validator.addMethod("valueNotEquals", function(value, element, arg) {
                return arg != value;
            }, "Please Choose one");

            


            $("#smart-form-register").validate({

                    // Rules for form validation
                    rules : {
                        
                        name_sht : {
                            required : true
                        },

                        company_name : {
                            required : true
                        },

                        company_addr : {
                            required : true
                        },

                        company_phone : {
                            required : true
                            
                        },
                        
                        company_fax : {
                            required : true
                              
                        },

                        equity_start : {
                            required : true
                        },

                        equity_end : {
                            required : true
                        }
                    },

                    errorPlacement : function(error, element) {
                        error.insertAfter(element.parent());

                    }
                });


            $("#btn_form").on('click',function(){

                // if($registerForm.valid()){
                if($("#smart-form-register").valid()){
                    var name_sht      = $("#name_sht").val();
                    var company_name  = $("#company_name").val();
                    var company_addr  = $("#company_addr").val();
                    var company_phone = $("#company_phone").val();
                    var company_fax   = $("#company_fax").val();


                    // var equity_start = $('#equity_start').datepicker({ dateFormat: 'dd-mm-yy' }).val();
                    // var equity_start = $('#equity_end').datepicker({ dateFormat: 'dd-mm-yy' }).val();
                  //  var md = Date.parseDate(date:$("#equity_start").val(), pattern:"dd-mm-yyyy", loc:'th-th'));
                   // Alert("ERROR", md , null, null);
                   // return false;
                    var equity_start  = $("#equity_start").val();
                    var equity_end    = $("#equity_end").val();
                    

                    //alert(equity_start);
                    //var user_id       = get_userID();
                    //var plan_status  = $('input[name=plan_status]:checked').val();

                    var jsondata = {
                        
                        company_name:  company_name,
                        company_addr:  company_addr,
                        company_phone: company_phone,
                        company_fax:   company_fax,
                        equity_start:  equity_start,
                        equity_end:    equity_end,
                        name_sht:  name_sht
                    };


                    MeaAjax(jsondata, "/admin/BondCompany/edits", function(mresponse) {
                        if(mresponse.success){
                            AlertSuccess("บันทึกจัดการบริษ้ทเรียบร้อยแล้ว",function(){
                                window.location.href = "/admin/BondCompany";
                            });

                        } else {
                            Alert("ERROR", mresponse.html, null, null);
                        }
                    });

                    return false;
                }
                return false;
            });

            //
            //  meaDatepicker("plan_start","plan_end");
            //  meaDatepicker("plan_end");
            //
            
            meaDatepicker("equity_start", "equity_end");
            meaDatepicker("equity_end");
        });

    </script>

@stop