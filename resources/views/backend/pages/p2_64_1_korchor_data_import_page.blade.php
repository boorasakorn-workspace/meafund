<!--
FILE: p2_64_1_korchor_data_import_page.blade.php
Purpose: Upload PDF files to contents folder and
         save result into database
-->

@extends('backend.layouts.default')
@section('content')
<?php
$data = getmemulist();
$arrSidebar =getSideBar($data);
?>

    <style type="text/css">
        #sort-by-year{
            /*position:absolute;*/
            width: 100%;
            margin-bottom: 10px;
            /*z-index: 999;*/
            /*left: 200px;*/
            /*top:5px*/
        }
        #sort-by-year select{
            width: 300px;

        }
        #datatable_fixed_column tbody tr td a i{
            font-size: 11px !important;
            line-height: 12px!important;
        }
        #datatable_fixed_column tbody tr td a{
            line-height: 12px!important;

        }

        .isa_info, .isa_success, .isa_warning, .isa_error {
            margin: 10px 0px;
            padding:12px;
        }
        .isa_info {
            color: #00529B;
            background-color: #BDE5F8;
        }
        .isa_success {
            color: #4F8A10;
            background-color: #DFF2BF;
        }
        .isa_warning {
            color: #9F6000;
            background-color: #FEEFB3;
        }
        .isa_error {
            color: #D8000C;
            background-color: #FFBABA;
        }
        .isa_info i, .isa_success i, .isa_warning i, .isa_error i {
            margin:10px 22px;
            font-size:2em;
            vertical-align:middle;
        }

    </style>

<!-- MAIN CONTENT -->
<div id="content">

    <!--div class="widget-body fuelux">
         <div class="wizard" >
            <ul class="steps form-wizard">
                <li style="font-size:18px" >
                    <a href="#"  class="badge badge-info">1</a>
                    <a href="#">นําเข้าข้อมูล รายงาน กช. </a> 
                    <span class="chevron"></span>
                </li>

                <li style="font-size:18px" class="active">
                    <a href="#"  class="badge">2</a>
                    <a href="#"> นําเข้าข้อมูล รายงาน กช. </a>
                    <span class="chevron"></span>
                </li>
            </ul>

        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>
                 นำเข้าข้อมูล
            </h1>
        </div>
    </div-->

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>

                {{--getMenutitle($arrSidebar)--}} 
                นําเข้าข้อมูล รายงาน กช.

            </h1>
        </div>

    </div>


    <!-- NEW COL START -->
    <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                <!--header>
                    <span class="widget-body widget-icon"> <i class="fa fa-download"></i> </span>
                    <span style="font-size: 18px">เป็นเมนูสําหรับนําเข้าข้อมูล มูลค่าทรัพย์สนิ สุทธิและอัตราผลตอบแทน </span>
                </header-->

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                       
                        <div class="smart-form" id="mainform" style="display:block;">

                            <!-- ========= Form[ 1 ] ==============-->
                            <div class="smart-form"  id="form1-korchor">
                                <fieldset>
                                        <section style="margin-top: 0px;margin-left: 20px; margin-right: 20px;">
                                            <span style="font-size: 20px">
                                               เป็นเมนูสําหรับนําเข้าข้อมูล รายงาน กช. ประเภทต่างๆเข้าสู่ระบบกองทุนฯ (รูปแบบไฟล์นามสกลุ .pdf)
                                            </span>
                                            <div class="inline-group">  
                                                <a class="btn btn-xs btn-success" href="{{action('AdminKorchorReportController@T2_download_korchor')}}"> ดูตัวอย่างไฟล์ PDF  </a>
                                            </div>
                                        </section>
                                </fieldset>
                            </div>     



                           <!-- -->
                            <div class="text-content" style="display:inline-block;">
                                <div class="span7 offset1">
                                  
                                    <div class="secure"></div>

                                    {!! Form::open(array('url'=>'admin/AdminKorchorReport/upload', 'method'=>'POST', 'files'=>true)) !!}
                                    <div class="control-group">
                                        <div class="controls">
                                      
                                            <section style="margin-top: 0px;margin-left: 35px; margin-right: 35px;">
                                                <label class="input">
                                                    <div class="form-group">
                                                        <span style="font-size: 20px"> 
                                                            เลือกไฟล์ ที่ต้องการนำเข้า  (สูงสุดจำนวน 50 ไฟล์)    
                                                        </span>

                                                        <input type="file" name="images[]" class="filestyle"  
                                                            data-buttonBefore="false" data-size="sm" 
                                                            data-buttonName="btn-primary" data-placeholder="No file"
                                                            data-buttonText="  เลือกไฟล์ " id="import1" multiple>
                                                    </div>
                                                </label>
                                            </section>
                                            
                                            <p class="errors" style="margin-top: 0px;margin-left: 35px; margin-right: 35px;">{!!$errors->first('images')!!}</p>
                                            @if(Session::has('error'))
                                            <p class="errors" style="margin-top: 0px;margin-left: 35px; margin-right: 35px;">{!! Session::get('error') !!}</p>
                                            @endif
                                         </div> 
                                    </div>

                                    <section style="margin-top: 0px;margin-left: 35px; margin-right: 35px;"> 
                                    {!! Form::macro('submitIcon', function ($icon) {  
                                       
                                       return '<a href="javascript:void(0);" 
                                              style="display:inline-block;"  
                                              data-input="import1" 
                                              data-import="1" 
                                              class="btn_import btn btn-xs btn-primary"><i class="fa fa-download"></i> นำเข้าข้อมูล</a>';
                                        
                                     }) !!}
     
                                    {!! Form::button('Submit', ['type' => 'submit', 'style="display: none;"', 'class' => 'btn_import_pdf btn btn-sm btn-primary'] )  !!}
                                    {!! Form::submitIcon('download') !!}
                                    {!! Form::close() !!}
                                    </section>

                                    @if(Session::has('success'))
                                    <section style="margin-top: 0px;margin-left: 35px; margin-right: 35px;">
                                        <!--div class="alert-box success">
                                            <h2>{!! Session::get('success') !!}</h2>
                                        </div-->
                                         <div class='isa_success' id='isa_success' >
                                            <i class='fa fa-check-circle'></i>
                                            
                                            <span color='green' style="font-size: 18px; "> 
                                               นำเข้าข้อมูล
                                            </span> 
                                            <span  color='#047712' style="font-size: 18px; ">
                                               จำนวณ {!! Session::get('uploadcount') !!} จาก {!! Session::get('total') !!} ไฟล์
                                              <br/>
                                                
                                               @foreach(Session::get('files') as $f) 
                                                     
                                                        
                                                       <p>{{$f}}</p>
                                                    
                                               @endforeach
                                               
                                            </span>
                                        </div>
                                    </section>
                                    @endif

                                    <span style="margin-top: 0px;margin-left: 35px; margin-right: 35px; color: #4F8A10 ; font-size: 18px">
                                       <p  id="check_ret_pdf" style="display: none;"> </p>
                                    </span>
                                </div>
                           
                            <!-- -->             
                            <div class="smart-form" id="footer" style="display:none;" >
                            
                                <fieldset>       

                                        <section style="margin-top: 0px;margin-left: 20px; margin-right: 20px;">
                                            <p id="progress_import1" style="display: none;"><img src="{{asset('backend/img/shot.gif')}}"  /> กำลังนำเข้าข้อมูล</p>
                                    
                                            <label class="input">
                                                <div class="form-group">
                                                    <span style="font-size: 20px"> 
                                                        เลือกไฟล์ ที่ต้องการนำเข้า      
                                                    </span>

                                                    <input type="file" name="files[]" class="filestyle" 
                                                         data-buttonBefore="false" data-size="sm" 
                                                        data-buttonName="btn-primary" data-placeholder="No file"
                                                        data-buttonText="  เลือกไฟล์ " id="import2" multiple>
                                                </div>
                                            </label>
                                            
                                        </section>            
                                        <br/>

                                </fieldset>
                                

                                   
                                <footer >
                                    <p id="progress_check1" style="display: none;"><img src="{{asset('backend/img/shot.gif')}}"  /> กำลังตรวจสอบรูปแบบ ไฟล์</p>
                                     
                                    <span style="color: #4F8A10 ; font-size: 18px">
                                       <p  id="check_ret" style="display: none;"> </p>
                                    </span>
                                 
                                    <div class="button-group">
                                         <p>
                                           <a href="javascript:void(0);" style="display: block;" class="btn_cancel btn btn-xs btn-primary"><i class="fa fa-reset"></i> ยกเลิก</a-->
                                           <a href="javascript:void(0);" data-input="import1" data-import="1" class="btn_check btn btn-xs btn-primary"><i class="fa fa-download"></i> ตรวจสอบไฟล์</a>
                                           <a href="javascript:void(0);" style="display: none"  data-input="import1" data-import="1" class="btn_import btn btn-xs btn-primary"><i class="fa fa-download"></i> นำเข้าข้อมูล</a>
                                         </p>
                                     </div>
                                </footer>
                    
                            </div>

                            
                        </div>
                            
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->
        </div>
            <!-- end widget -->
    </article>

</div>
<!-- END MAIN CONTENT -->


<!-- PAGE RELATED PLUGIN(S) -->
{{--<script src="{{asset('backend/js/plugin/datatables/jquery.dataTables.min.js')}}"></script>--}}
{{--<script src="{{asset('backend/js/plugin/datatables/dataTables.colVis.min.js')}}"></script>--}}
{{--<script src="{{asset('backend/js/plugin/datatables/dataTables.tableTools.min.js')}}"></script>--}}
{{--<script src="{{asset('backend/js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>--}}
{{--<script src="{{asset('backend/js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>--}}

<script src="{{asset('backend/js/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js')}}"></script> 
<script type="text/javascript">

    var error_msg = "การ ตรวจสอบไฟล์ ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ";

    $(document).ready(function() {

        if (navigator.userAgent.indexOf("MSIE") > 0) {
            $("#import1").mousedown(function() {
               // alert('sdsd');
                var $div_success = $('#isa_success');
                if($div_success)
                    $('#isa_success').hide();
                $(this).trigger('click');
               
            })
        } else if($.browser.mozilla) {
           
            /*    var $div_success = $('#isa_success');
                if($div_success)
                    $('#isa_success').hide();*/
        }

        $('#import1').change(function(click) {
            var $div_success = $('#isa_success');
            var $isa_error = $('.isa_error');
            if($div_success) {
                $('#isa_success').hide();
            }

              
            if($('.isa_error'))
                $('.isa_error').hide();
            
        });



    
        // Cancel click event
        $('.btn_cancel').on('click', function(){
            $(":file").filestyle('clear');
            $('#check_ret').hide();
            $('.btn_check').show();
            $('.btn_import').hide();
        });
      

        $('.btn_check').on('click',function(){
            var target = $(this).attr('data-input');
            var importType= $(this).attr('data-import');

             validatePDFFile(target, importType);
        
        });

        /**
         * Import PDF
         */
        $('.btn_import').on('click', function() {
            var target = $(this).attr('data-input');
            var importType= $(this).attr('data-import');
            $('#progress_import1').show();
            var status = validatePDFFile(target, importType);
            if(status) {
               $('.btn_import_pdf').trigger('click');

            }
            else {
                $('#progress_import1').hide();
                $('#check_ret_pdf').show();
                $('#check_ret_pdf').html(
                showInfoMessage(false, 
                     "ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ที่ต้องการนำเข้า",
                     "", ""));
            }
        });  

    
        ////////////////////////////////////////////////////////
        var eventName = function() {
            $.getJSON("progress", function(data) {
                // update the view with your fresh data
                console.log('########## total:' + data.total + ' progress: ' + data.percent_progress + '%');
                if (data.percent_progress < 100)
                     eventName();
            });
            /*
            var dataimport = new FormData();
            dataimport.append("method", "checkProgress");

            $.ajax({
                cache: false,
                type: 'POST',
                contentType: false,
                processData: false,
                url: 'progress',    // UserController
                data: dataimport,

                success: function(data){
                    console.log('progress' + data.progress_count);
                    if (data.progress_count < 10)
                        eventName();
                },
                error: function(xhr, textStatus, thrownError) {
                    Alert('Error', "การ import ข้อมูลผิดพลาด กรุณาตรวจสอบ รูปแบบข้อมูลของ ไฟล์ ");
                }
            });
            */

        };
        ///////////////////////////////////////////////////////

    });  // ready

    function showInfoMessage(status, title, subtitle, infomessage) {
        var msg = '';
        if(status) {
           msg = "<div class='isa_success' style='margin-top: 0px;margin-left: 35px; margin-right: 35px;'>" +
                 "<i class='fa fa-check-circle'></i>" +
                 "<font color='green'>" + title + " <br/>" +
                                        subtitle +
                 " </font> <font color='#047712'>" + infomessage + " </font>" +
                 "</div>";
         } else {
            msg = "<div class='isa_error' style='margin-top: 0px;margin-left: 35px; margin-right: 35px;'>" +
                  "<i class='fa fa-times-circle'></i>" + 
                  "<font color='red'> " + title + " <br/>" +
                                        subtitle + " </font>" +
                  "</div>";
         }
         return msg;
    }


    function reanderImportForm(sel) {
        var forms = ["", "#form1-purchase", "", "", "", "#form5-benefit","","","#form8-stock-universe","",""];
        
        if(sel.value == '') {
           // alert(sel.value)
            $('#footer').hide();  
            for(var i=0;i<forms.length; i++) {
                
                if(forms[i].length > 0 ) {
                    console.log(i);
                   $(forms[i]).hide(); 
                }
            }
            return;
        }
        if(forms[sel.value].length < 1 ) { 
            return;
        }

        
        for(var i=0;i<forms.length; i++) {
            if(forms[i].length > 0 ) {
                console.log(i);
               $(forms[i]).hide(); 
            }
        }

        $(forms[sel.value]).show();
        $('#footer').show(); 
        
    }

    function validatePDFFile(target, importType) {
        $('#check_ret').hide();
        var status = false;
        var dataimport = new FormData();
        var extall="pdf";
        var files = $("#" + target).get(0).files; //images

        if(!files.length ) { 
            Alert("นำเข้าข้อมูล กช.","ท่านยังไม่ได้เลือกไฟล์ PDF");
            return false;
        }

        //for(var i=0; i < files.length; i++) {
        //
        $.each (files, function (idx, fobj) {   
            
            // ext = files[i].name.split('.').pop().toLowerCase();
            ext  = fobj.name.split('.').pop().toLowerCase();
            if(ext != 'pdf') {
                status = false;
                return status;
                //break;

            } else {
                var v = new Array();
                var ar = fobj.name.split('.'); //files[i].name.split('.');
                if(ar.length > 0) {
                    var sfile = ar[0];
                    v = sfile.split('_');
                    console.log(sfile);

                    // sanity checking 
                    if (v.length != 4) return status;

                    $.each( v, function( index, value ){
                       console.log(value);
                    });

                    var num = parseInt(v[3], 10);
                    status = $.isNumeric(num);
                    if(!status)
                        return status;
                }

            }
        }); 


        if(!status) {
            $('#progress_check' + importType).hide();
            Alert("มีข้อผิดพลาด",'รูปแบบไฟล์และนามสกุล ไม่ถูกต้อง');
            return false;
        } 
        return status;

    }

    
    
    
</script>

@stop