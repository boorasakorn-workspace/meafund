
<!--FILE: p2_bond_edit_unit_val_page.blade.php -->

@extends('backend.layouts.default')
@section('content')
    <?php
    $data = getmemulist();
    $arrSidebar = getSideBar($data);
    ?>

    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-table fa-fw "></i>
                    {{--Auth::user()->username--}} 
                    {{getMenutitle($arrSidebar)}}
                </h1>
            </div>
        </div>


        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"
                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                    </div>
                    <!-- end widget edit box -->


                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        
                        <form id="smart-form-register" action=""  class="smart-form">
                            {!! csrf_field() !!}
                            
                            <fieldset>
                                <section class="col col-6">
                                    <lable style="font-size:18px">Record ID</lable>
                                    <label class="input">
                                        <input type="text" id="job_id" name="job_id" value="{{$editdata->JOB_ID}}" placeholder="{{$editdata->JOB_ID}}" readonly>
                                    </label>
                                </section>
                            </fieldset>    
                            
                            <fieldset>
                                <section class="col col-6">
                                    <!-- TBL_P2_EQUITY_INVESTMENT_POLICY -->
                                    <lable style="font-size:18px">นโยบายลงทุน </lable>
                                    <label class="input">
                                        <select name="policy_id" id="policy_id" class="form-control" disabled="true">
                                         <!--
                                         <option value="">ระบุนโยบายลงทุน</option>
                                         -->
                                         @if($policyList)
                                             @foreach($policyList as $item)
                                             @if($editdata->POLICY_ID == $item->POLICY_ID)
                                                <option value="{{$item->POLICY_ID}}" selected>&nbsp;{{$item->POLICY_NAME}}</option>
                                             @else
                                                <option value="{{$item->POLICY_ID}}">&nbsp;{{$item->POLICY_NAME}}</option>
                                             @endif
                                             @endforeach
                                         @endif
                                        </select>
                                    </label>
                                </section>

                                <section class="col col-6">
                                    <!-- TBL_P2_EQUITY_SECURITIES -->
                                    <lable style="font-size:18px">บริษัทจัดการ</lable>
                                    <label class="input">
                                        <!--input type="text" id="securities_name" name="securities_name" placeholder="ระบุชื่อบริษัท บริษัทจัดการ">
                                        <b class="tooltip tooltip-bottom-right">กรุณาระบุชื่อบริษัท บริษัทจัดการ</b--> 
                                         <select name="name_sht" id="name_sht" class="form-control">
                                         <option value="">ระบุบริษัทจัดการ</option>
                                         @if($securitiesList)
                                            @foreach($securitiesList as $item)
                                                @if($editdata->SECURITIES_NAME == $item->NAME_SHT)
                                                    <option value="{{$item->NAME_SHT}}" selected>&nbsp;{{$item->SECURITIES_NAME}}</option>
                                                @else
                                                    <option value="{{$item->NAME_SHT}}">&nbsp;{{$item->SECURITIES_NAME}}</option>
                                                @endif
                                                <!--selected-->
                                            @endforeach
                                         @endif
                                        </select>
                                    </label>
                                </section>
                                
                                <section class="col col-6">
                                    <lable style="font-size:18px">วัน ที่อ้างอิงข้อมูล</lable>
                                    <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                        <input type="text" name="date_start" 
                                               value="{{toThaiDateTime($editdata->REFERENCE_DATE, false)}}"
                                           class="mea_date_picker" id="date_start" placeholder="{{toThaiDateTime($editdata->REFERENCE_DATE, false)}}" readonly>
                                        <b class="tooltip tooltip-bottom-right">กรุณาระบุวัน ที่อ้างอิงข้อมูล</b> 
                                    </label>
                                </section>

                            </fieldset>  

                            <fieldset>
                                <section class="col col-6">
                                    <lable style="font-size:18px">มูลคา่ ทรัพย์สินสุทธิ(บาท)</lable>
                                    <label class="input"> 
                                        <input type="text" id="nav_b" name="nav_b" 
                                               value="{{number_format((float)$editdata->NAV_B, 2, '.', ',')}}" 
                                               placeholder="กรุณาระบุตัวเลขมูลค่าทรัพย์สินสทุธิ">
                                        <b class="tooltip tooltip-bottom-right">กรุณาระบุตัวเลขมูลค่าทรัพย์สินสทุธิ</b> 
                                    </label>
                                </section>

                                <section class="col col-6">
                                    <lable style="font-size:18px">จํานวนหน่วยลงทุน</lable>
                                    <label class="input">
                                        <input type="text" id="unit" name="unit" 
                                               value="{{number_format((float)$editdata->UNIT, 4, '.', ',')}}"  
                                               placeholder="กรุณาระบุตัวเลขจํานวนหน่วยลงทุน">
                                        <b class="tooltip tooltip-bottom-right">กรุณาระบุตัวเลขจํานวนหน่วยลงทุน</b> 
                                    </label>
                                </section>

                                <section class="col col-6">
                                    <lable style="font-size:18px">มูลค่าทรัพย์สินสุทธิต่อหน่วย(บาท)</lable>
                                    <lable style="font-size:10px; color:red">*** ทศนิยม 12 ตําแหน่ง</lable>
                                    <label class="input">
                                        <input type="text" id="nav_unit12" name="nav_unit12" 
                                               value="{{number_format((float)$editdata->NAV_UNIT, 12, '.', ',')}}"
                                               placeholder=" กรุณาระบุตัวเลขมูลค่าทรัพย์สินสุทธิต่อหน่วย(บาท ทศนิยม 12 ตําแหน่ง)"> 
                                        <b class="tooltip tooltip-bottom-right">กรุณาระบุตัวเลขมูลค่าทรัพย์สินสุทธิต่อหน่วย(บาท)</b> 
                                    </label>
                                </section>

                                 <section class="col col-6">
                                    <lable style="font-size:18px">มูลค่าทรัพย์สินสุทธิต่อหน่วย(บาท)</lable>
                                    <lable style="font-size:10px; color:red">*** ทศนิยม 4 ตําแหน่ง</lable>
                                    <label class="input">
                                        <input type="text" id="nav_unit4" name="nav_unit4" 
                                               value="{{number_format((float)$editdata->NAV_UNIT, 4, '.', ',')}}"
                                               placeholder=" กรุณาระบุตัวเลขมูลค่าทรัพย์สินสุทธิต่อหน่วย(บาท ทศนิยม 4 ตําแหน่ง)"> 
                                        <b class="tooltip tooltip-bottom-right">กรุณาระบุตัวเลขมูลค่าทรัพย์สินสุทธิต่อหน่วย(บาท)</b> 
                                    </label>
                                </section>

                            </fieldset>

                            <fieldset>  
                                <section class="col col-6">
                                    <lable style="font-size:18px">อัตราผลตอบแทนรายเดือน (%)</lable>
                                    <label class="input">
                                        <input type="text" id="yield_month" name="yield_month" 
                                               value="{{number_format((float)$editdata->YIELD_MONTH, 2, '.', ',')}}" 
                                               placeholder="กรุณาระบุตัวเลขอัตราผลตอบแทนรายเดือน">
                                    </label>
                                </section>

                                <section class="col col-6">
                                    <lable style="font-size:18px">อัตราผลตอบแทนสะสม (%)</lable>
                                    <label class="input">
                                        <input type="text" id="yield_cumulative" name="yield_cumulative" 
                                               value="{{number_format((float)$editdata->YIELD_CUMULATIVE, 2, '.', ',')}}" 
                                               placeholder="กรุณาระบุตัวเลขอัตราผลตอบแทนสะสม (%)">
                                    </label>
                                </section>
                            
                                <section class="col col-6">
                                    <lable style="font-size:18px">Benchmark รายเดือน (%)</lable>
                                    <label class="input">
                                        <input type="text" id="bm_month" name="bm_month" 
                                               value="{{number_format((float)$editdata->BM_MONTH, 2, '.', ',')}}"  
                                               placeholder="กรุณาระบุตัวเลข Benchmark รายเดือน (%)">
                                    </label>
                                </section>

                                <section class="col col-6">
                                    <lable style="font-size:18px">Benchmark สะสม (%)</lable>
                                    <label class="input">
                                        <input type="text"  id="bm_cumulative" name="bm_cumulative" 
                                               value="{{number_format((float)$editdata->BM_CUMULATIVE, 2, '.', ',')}}" 
                                               placeholder="กรุณาระบุตัวเลข Benchmark สะสม (%)">
                                    </label>
                                </section>

                            </fieldset>
                            


                            <footer>
                                <button type="button"  id="btn_form" class="btn btn-primary">ยืนยัน
                                </button>
                                <button type="button" id="btn_cancel" class="btn btn-default">
                                    ยกเลิก
                                </button>
                            </footer>
                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->

    </div>


    <!-- PAGE RELATED PLUGIN(S) -->
    <script src="{{asset('backend/js/plugin/jquery-form/jquery-form.min.js')}}"></script>
    <script src="{{asset('backend/js/plugin/summernote/summernote.min.js')}}"></script>

    <script type="text/javascript">

        $(document).ready(function() {
            
            $('#nav_b').keypress(function(event) {
                  if(event.which == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) 
                     return true;
                  else if((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57))
                  event.preventDefault();

            });
            $('#nav_unit12').keypress(function(event) {
                  if(event.which == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) 
                     return true;
                  else if((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57))
                  event.preventDefault();

            });
            $('#unit').keypress(function(event) {
                  if(event.which == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) 
                     return true;
                  else if((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57))
                  event.preventDefault();

            });
            $('#yield_month').keypress(function(event) {
                  if(event.which == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) 
                     return true;
                  else if((event.which != 46) && (event.which < 48 || event.which > 57))
                  event.preventDefault();

            });
            $('#yield_cumulative').keypress(function(event) {
                  if(event.which == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) 
                     return true;
                  else if((event.which != 46) && (event.which < 48 || event.which > 57))
                  event.preventDefault();

            });

            $('#bm_month').keypress(function(event) {
                  if(event.which == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) 
                     return true;
                  else if((event.which != 46) && (event.which < 48 || event.which > 57))
                  event.preventDefault();

            });
            $('#bm_cumulative').keypress(function(event) {
                  if(event.which == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) 
                     return true;
                  else if((event.which != 46) && (event.which < 48 || event.which > 57))
                  event.preventDefault();

            });

            /**
             * auto sync 2 NAV_UNIT  
             */
            $("#nav_unit12").on("keyup paste", function() {
                var nav_unit4 = parseFloat($(this).val());
                $("#nav_unit4").val(nav_unit4.toFixed(4));
            });



            $.validator.addMethod("valueNotEquals", function(value, element, arg) {
                return arg != value;
            }, "Please Choose one");

            

 
            $("#smart-form-register").validate({
                    // Rules for form validation
                    rules : {
                        policy_id: {
                           required : true
                        },

                        name_sht : {
                            required : true
                        },

                        nav_b : {
                            required : true
                        },

                        nav_unit12 : {
                            required : true
                        },

                        unit : {
                            required : true
                        },
                        
                        yield_month : {
                            required : true
                              
                        },

                        yield_cumulative: {
                             required : true 
                        },

                        bm_month : {
                            required : true
                              
                        },
                          
                        bm_cumulative: {
                             required : true 
                        }
                    },

                    errorPlacement : function(error, element) {
                        error.insertAfter(element.parent());
                    }
                });

            $("#btn_cancel").on('click',function(){
               window.location.href = "/admin/BondDataImport/";
            });
            
            $("#btn_form").on('click',function(){
                
                var refdate = $("#date_start").val();
                if(refdate=="") {
                    refdate = $("#date_start").val();
                } 
                //alert(refdate);
                //return false;

                if($("#smart-form-register").valid()){
                    var record_id        = $("#job_id").val();
                    var name_sht         = $("#name_sht").val();
                    var policy_id        = $("#policy_id").val();
                    var reference_date   = refdate; //$("#hd_date_start").val();

                    var nav_b            = $("#nav_b").val();
                    var nav_unit12       = $("#nav_unit12").val();
                    var nav_unit4        = $("#nav_unit4").val();
                    var unit             = $("#unit").val();

                    var yield_month      = $("#yield_month").val();
                    var yield_cumulative = $("#yield_cumulative").val();
                    var bm_month         = $("#bm_month").val();
                    var bm_cumulative    = $("#bm_cumulative").val();
                    
                    //var user_id       = get_userID();
                    //var plan_status  = $('input[name=plan_status]:checked').val();

                    var jsondata = {
                        record_id : record_id,
                        policy_id:  policy_id,
                        name_sht:  name_sht,
                        reference_date:  reference_date,

                        nav_b: nav_b,
                        nav_unit12:   nav_unit12,
                        nav_unit4:   nav_unit4,
                        unit:   unit,

                        yield_month: yield_month,
                        yield_cumulative: yield_cumulative,
                        bm_month : bm_month,
                        bm_cumulative: bm_cumulative
                    };

                    // abort(403, equity_end);
                    //    $(".result").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
                    MeaAjax(jsondata, "/admin/BondDataImport/editsUnitVal", function(mresponse) {
                        if(mresponse.success){
                            AlertSuccess("ไก้ไข สำเร็จเรียบร้อย",function(){
                                window.location.href = "/admin/BondDataImport/";
                            });

                        } else {
                            Alert("ERROR", mresponse.html, null, null);
                        }
                    });

                    return false;
                }
                return false;
            });
            
            meaDatepicker("date_start", "");
            //meaDatepicker("equity_end");
        });
    </script>
@stop
