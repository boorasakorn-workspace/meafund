@extends('backend.layouts.default')
@section('content')
    <?php
    $data = getmemulist();
    $arrSidebar =getSideBar($data);
    ?>
<link rel="stylesheet" href="{{asset('backend/css/jquery-ui.css')}}">
<style>
  .custom-combobox {
    position: relative;
    display: inline-block;
  }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
  }
  .custom-combobox-input {
    margin: 0;
    padding: 5px 10px;
  }

  optgroup {
    background-color: #FF8000; //#DCDCDC;
    color: white;
  }
  option {
    background-color: white;
    color: black;
  }
   .disable { opacity : .35; background-color:lightgray; border:1px solid gray;}

</style>


    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-table fa-fw "></i>

                    {{getMenutitle($arrSidebar)}}
                </h1>
            </div>

        </div>


        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>


                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        
                        <form id="smart-form-register" action=""   class="smart-form">
                           

                            <fieldset>
                                <div class="row" >
                                     <section class="col col-4" >
                                        <lable style="font-size:18px">รหัส หลักทรัพย์</lable>
                                        <label class="input">
                                            <input type="text" class="disable" id="record_id" name="record_id" placeholder="รหัส บริษัทจัด การหลักทรัพย์"
                                                    value="{{$editdata->INDEX_ID}}"
                                                   readonly>
                                        </label>
                                    </section>

                                    
                                </div>

                                <div class="row"> 
                                    
                                    <section class="col col-4">
                                        <lable style="font-size:18px">ตลาด(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                        <label class="input">
                                            <select id="market" name="market" class="form-control">
                                                <option value="">&nbsp;กรุณาเลือก หนึ่งรายการ &nbsp;</option>
                                                @if($editdata->MARKET == 'SET')
                                                   <option selected value="SET">&nbsp;SET</option>
                                                @else
                                                   <option value="SET">&nbsp;SET</option>
                                                @endif
                                                
                                                @if($editdata->MARKET == 'mai')
                                                   <option selected value="mai">&nbsp;mai</option>
                                                @else
                                                   <option value="mai">&nbsp;mai</option>
                                                @endif
                                            </select>

                                            <!--b class="tooltip tooltip-bottom-right">ระบุ SET Index</b> 

                                            <input type="text" id="market" name="market" placeholder="ระบุตลาด">
                                            <b class="tooltip tooltip-bottom-right">ระบุตลาด</b--> 
                                        </label>
                                    </section>

                                    <section class="col col-4">
                                        <lable style="font-size:18px">SET Index(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                        <label class="input">
                                            <!--input type="text" id="setindex" name="setindex" placeholder="ระบุ SET Index"-->
                                            <select id="setindex" name="setindex" class="form-control">
                                                <option value="">&nbsp;กรุณาเลือก หนึ่งรายการ &nbsp;</option>
                                                @if(($editdata->SET_INDEX100 == 'Y') && ($editdata->SET_INDEX50 == null) || ($editdata->SET_INDEX50==''))
                                                   <option selected value="SET100">&nbsp;SET100</option>
                                                @else
                                                   <option value="SET100">&nbsp;SET100</option>
                                                @endif

                                                @if(($editdata->SET_INDEX100 == 'Y') && ($editdata->SET_INDEX50 == 'Y'))   
                                                   <option selected value="SET50">&nbsp;SET50</option>
                                                @else
                                                   <option  value="SET50">&nbsp;SET50</option>
                                                @endif   

                                                @if((($editdata->SET_INDEX100 == null) || ($editdata->SET_INDEX100=='')) && 
                                                    (($editdata->SET_INDEX50 == null) || ($editdata->SET_INDEX50==''))) 
                                                   <option selected value="None">&nbsp;None</option>
                                                @else
                                                   <option  value="None">&nbsp;None</option>
                                                @endif   
                                     
                                            </select>
                                             
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <!--section class="col col-10">
                                        <lable style="font-size:18px">กลุ่มอุตสาหกรรม</lable>
                                        <label class="input">
                                            
                                            <select id="industrial" name="industrial">
                                                <option value="">&nbsp;กรุณาเลือก หนึ่งรายการ &nbsp;</option>
                                    
                                                <option value="SET">&nbsp;SET100</option>
                                                <option value="mai">&nbsp;SET50</option>
                                                <option value="mai">&nbsp;None</option>
                                 
                                            </select>
                                        </label>
                                    </section-->

                                    <section class="col col-6">
                                         <label style="font-size:18px">กลุ่มอุตสาหกรรม(<lable style="font-size:18px; color:red;">*</lable>)</label>
                                         <label class="input">
                                            <select name="industrial" id="industrial" class="form-control">
                                                <option value="">&nbsp;กรุณาเลือก หนึ่งรายการ &nbsp;</option>
                                                @if($sectionList)
                                                  @foreach($sectionList as $key => $value)
                                                    <optgroup label="&nbsp;{{$key}}">
                                                        @foreach($value as $child)
                                                          
                                                             @if(($editdata->INDUSTRIAL == $child['INDUSTRIAL']) && ($editdata->BU == $child['BU']))
                                                                <option selected value="{{$child['CATE_ID']}}">&nbsp;&nbsp;&nbsp;{{$child['BU']}}</option>
                                                             
                                                             @else
                                                                <option value="{{$child['CATE_ID']}}">&nbsp;&nbsp;&nbsp;{{$child['BU']}}</option>
                                                             @endif
                                                        @endforeach
                                                    </optgroup> 
                                                  @endforeach   
                                                @endif
                                                
                                                
                                            </select>
                                        </label>
                                    </section>
                                </div>

                                <!--div class="row">
                                    <section class="col col-5">
                                        <lable style="font-size:18px">หมวดธุรกิจ</lable>
                                        <label class="input">
                                             <select id="bu" name="bu" class="form-control">
                                                <option value="">&nbsp;กรุณาเลือก หนึ่งรายการ &nbsp;</option>
                                    
                                                <option value="SET">&nbsp;SET100</option>
                                                <option value="mai">&nbsp;SET50</option>
                                                <option value="mai">&nbsp;None</option>
                                 
                                            </select>
                                            
                                        </label>
                                    </section>
                                </div-->
                                <div class="row">
                                    <section class="col col-5">
                                        <lable style="font-size:18px">ชื่อย่อ</lable>
                                        <label class="input">
                                            <input type="text" class="disable" id="name_sht" name="name_sht" value="{{$editdata->SYMBOL}}"
                                             placeholder="ระบุชื่อย่อ" readonly>
                                             <b class="tooltip tooltip-bottom-right">ชื่อย่อ</b> 
                                        </label>
                                        
                                    </section>

                                    <div id="div_check_change_name" style="display:inline-block;">
                                        <br/>
                                        <input type="checkbox"  name="check_change_name"  
                                            class="check_change_name" id="check_change_name" value="" />
                                            
                                        <lable style="font-size:18px">เปลี่ยนชื่อหลักทรัพย์</lable>
                                    </div>
                                </div>

                            </fieldset>


                            <fieldset id="change_name" name="change_name" style="display:none;">
                                    <div class="row">
                                    <section class="col col-5">
                                        <lable style="font-size:18px">ชื่อหลักทรัพย์ใหม่</lable>
                                        <label class="input">
                                            <input type="text"  id="new_symbol" name="new_symbol" placeholder="ชื่อหลักทรัพย์ใหม่"
                                                   value="">
                                            <b class="tooltip tooltip-bottom-right">ชื่อหลักทรัพย์ใหม่</b>
                                        </label>
                                    </section>

                                    <section  class="col col-5">
                                     <lable style="font-size:18px">วันที่มีผล</lable>
                                        <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="effective_date"  class="mea_date_picker" id="effective_date" placeholder="ระบุวันที่มีผล"
                                                   value="{{$editdata->EFFECTIVE_DATE}}">
                                            <b class="tooltip tooltip-bottom-right">ระบุวันที่มีผล</b>
                                        </label>
                                    </section>
                                    </div>
                             </fieldset>

                             <fieldset>
                                    <div class="row">
                                        <section class="col col-8" >
                                            <lable style="font-size:18px">ชื่อบริษัท(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                            <label class="input">
                                                <input type="text" id="company_name" name="company_name" placeholder="ระบุชื่อบริษัท"
                                                       value="{{$editdata->COMP_NAME}}">
                                                <b class="tooltip tooltip-bottom-right">ระบุชื่อบริษัท</b>
                                            </label>
                                        </section>
                                    </div>

                                    <div class="row">
                                        <section class="col col-8" >

                                            <lable style="font-size:18px">ที่อยู่บริษัท(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                            <label class="input">
                                               <textarea style="font-size:18px" class="form-control" rows="6" id="company_addr" name="company_addr"
                                                
                                                placeholder=" ระบุกลุ่มอุตสาหกรรม   ">{{$editdata->ADDRESS}}</textarea>
                                               <b class="tooltip tooltip-bottom-right">ระบุที่อยู่บริษัท</b> 
                                            </label>
                                        </section>
                                    </div>
                                    
                                    <div class="row">    
                                        <section class="col col-6" >
                                            <lable style="font-size:18px">รหัสไปรษณีย์</lable>
                                            <label class="input">
                                                <input type="text" id="zipcode" name="zipcode"  value="{{$editdata->ZIP_CODE}}"
                                                 onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"
                                                 placeholder="ระบุ รหัสไปรษณีย์">
                                                <b class="tooltip tooltip-bottom-right">ระบุ รหัสไปรษณีย์</b>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="row">    
                                        <section class="col col-10" >
                                            <lable style="font-size:18px">URL</lable>
                                            <label class="input">
                                                <input type="text" id="equity_url" name="equity_url" value="{{$editdata->URL}}"
                                                placeholder="ระบุ URL">
                                                <b class="tooltip tooltip-bottom-right">ระบุ URL</b>
                                            </label>
                                        </section>
                                    </div>

                            </fieldset>

                            <fieldset>

                                <div class="row">

                                    <section class="col col-6">
                                        <lable style="font-size:18px"> เบอร์โทรศัพท์</lable>
                                        <label class="input"></i>
                                            <input type="text" name="company_phone"  id="company_phone" 
                                             value="{{$editdata->PHONE}}"
                                            placeholder="ระบุเบอร์โทรศัพท์" >
                                            <b class="tooltip tooltip-bottom-right">ระบุเบอร์โทรศัพท์</b> 
                                        </label>
                                    </section>

                                    <section class="col col-6">
                                        <lable style="font-size:18px">เบอร์โทรสาร</lable>
                                        <label class="input"> </i>
                                            <input type="text" name="company_fax" id="company_fax" value="{{$editdata->FAX}}" 
                                            placeholder="ระบุเบอร์โทรสาร" >
                                            <b class="tooltip tooltip-bottom-right">ระบุเบอร์โทรสาร</b> 
                                        </label>
                                    </section>
                                </div>

                                


                            </fieldset>


                            <fieldset>

                                <div class="row">
                                    <section class="col col-6">
                                        <lable style="font-size:18px">วันที่เริ่มต้น</lable>
                                        <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="equity_start"  class="mea_date_picker" id="equity_start" 
                                            value="{{get_date_notime_en($editdata->START_DATE)}}"
                                            placeholder="ระบุวันที่เริ่มต้น" >
                                            <b class="tooltip tooltip-bottom-right">ระบุวันที่เริ่มต้น</b> 
                                        </label>
                                    </section>

                                    <section class="col col-6">
                                        <lable style="font-size:18px">วันที่สิ้นสุด</lable>
                                        <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="equity_end" value="{{get_date_notime_en($editdata->END_DATE)}}"  
                                             class="mea_date_picker" id="equity_end" placeholder="ระบุวันที่สิ้นสุด" >

                                           <b class="tooltip tooltip-bottom-right">ระบุวันที่สิ้นสุด</b> 
                                        </label>
                                    </section>
                                </div>


                            </fieldset>
                            <footer>
                                <div class="row">
                                    <lable style="font-size:18px; color:red;">&nbsp;&nbsp;&nbsp;&nbsp;* Required field ​(ห้ามเป็นค่าว่าง)</lable>   
                                </div>
                                <button type="button"  id="btn_form" class="btn btn-primary">
                                    ยืนยัน
                                </button>
                                <button type="button" class="btn btn-default" onclick="window.history.back();">
                                    ยกเลิก
                                </button>
                                
                            </footer>
                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->

    </div>






    <!-- PAGE RELATED PLUGIN(S) -->
    <script src="{{asset('backend/js/plugin/jquery-form/jquery-form.min.js')}}"></script>
    


    <script type="text/javascript">

        $(document).ready(function() {


            //$('#record_id').addClass('input-disabled');
            //$('#name_sht').addClass('input-disabled');
            $.validator.addMethod("valueNotEquals", function(value, element, arg) {
                return arg != value;
            }, "Please Choose one");

            


           $("#btn_form").on('click',function(){

               $("#smart-form-register").validate({

                    // Rules for form validation
                    rules : {
                        
                        name_sht : {
                            required : false
                        },

                        industrial : {
                            required : true
                        },

                        //bu : {
                        //    required : true
                        //},

                        market : {
                            required : true
                        },

                        setindex : {
                            required : true
                        },

                        new_symbol : {
                            required: false
                        },

                        zipcode : {
                            required: false
                        },

                        equity_url : {
                            required: false
                        },

                        effective_date: {
                            required: false 
                        },

                        company_name : {
                            required : true
                        },

                        company_addr : {
                            required : true
                        },

                        company_phone : {
                            required : false
                            
                        },
                        
                        company_fax : {
                            required : false
                              
                        },

                        equity_start : {
                            required : true
                        },

                        equity_end : {
                            required : true
                        }
                    },

                    errorPlacement : function(error, element) {
                        error.insertAfter(element.parent());

                    }
                });

                // if($registerForm.valid()){
                if($("#smart-form-register").valid()){

                    var record_id      = $('#record_id').val();
                    var market         = $('#market').val();
                    var setindex       = $('#setindex').val();
                    var industrial_cate_id        = $("#industrial").val(); //industrial     = $("#industrial").val();  // cate_id
                    //var bu             = $("#bu").val();
                    var name_sht       = $("#name_sht").val();
                    var new_symbol     = $("#new_symbol").val();
                    var effective_date = $("#effective_date").val();
                    
                    var company_name  = $("#company_name").val();
                    var company_addr  = $("#company_addr").val();
                    var company_phone = $("#company_phone").val();
                    var company_fax   = $("#company_fax").val();

                     
                    var zipcode       = $("#zipcode").val();
                    var equity_url    = $("#equity_url").val();


                    // var equity_start = $('#equity_start').datepicker({ dateFormat: 'dd-mm-yy' }).val();
                    // var equity_start = $('#equity_end').datepicker({ dateFormat: 'dd-mm-yy' }).val();
                    //  var md = Date.parseDate(date:$("#equity_start").val(), pattern:"dd-mm-yyyy", loc:'th-th'));
                    // Alert("ERROR", md , null, null);
                    // return false;
                    
                    var equity_start  = $("#equity_start").val();
                    var equity_end    = $("#equity_end").val();
                    

                    if(!$('#check_change_name').is(':checked'))
                    {
                        effective_date = '';
                        new_symbol = '';
                        //alert('unchecked');
                    }
                    //var user_id       = get_userID();
                    //var plan_status  = $('input[name=plan_status]:checked').val();

                    var jsondata = {
                        record_id:     record_id,
                        market:        market,
                        setindex:      setindex,
                        industrial_cate_id:       industrial_cate_id,  
                        name_sht:      name_sht,
                       // bu:            bu,
                        new_symbol:    new_symbol,
                        effective_date: effective_date,

                        zipcode: zipcode,
                        equity_url: equity_url,



                        company_name:  company_name,
                        company_addr:  company_addr,

                        company_phone: company_phone,
                        company_fax:   company_fax,

                        equity_start:  equity_start,
                        equity_end:    equity_end
                    };

                    console.log(jsondata);
                    // alert('POST editsEquityIndex');
                    MeaAjax(jsondata, "/admin/EquityCompany/editsEquityIndex", function(data) {
                        // alert('POST editsEquityIndex');
                        if(data.success){
                            AlertSuccess("บันทึกจัดการบริษ้ทเรียบร้อยแล้ว",function(){
                                window.location.href = "/admin/EquityCompany/equityindex";
                            });

                        } else {
                            Alert("ERROR", data.html, null, null);
                        }
                    });
                    
                    return false;
                }
                return false;
            });

            
            meaDatepicker("equity_start", "equity_end");
            meaDatepicker("equity_end");

            meaDatepicker("effective_date");


            // events handler
            $('#check_change_name').on( "click", function() {
                
                if($('#check_change_name').is(':checked'))
                  $("#change_name").show();
                else
                  $("#change_name").hide();  
            });
        });

    </script>

@stop