<!--
SECURITY    NAME  Maturity Date   TY RATE FACE VALUE/UNIT TOTAL COST      Red Yield   AMORTIZED COST  DISC/PREM   ACCRUED INT  DAY INT ENT(EX)  MKT PRICE   DIRTY VALUE %NAV          ALLOWANCE
-->

<div id="trading_table" style="display:block;">
<div>Showing {{$PageNumber}} to {{ (($PageSize * $PageNumber) > $totals? $totals:($PageSize * $PageNumber))  }} of {{ $totals }}</div>
<table class="table table-bordered">
    <thead>
    <tr>
        <th colspan="16" style="text-align: center">
            <p class="report-title">{{$TableTitle}} </p> <!-- รายงานความเคลื่อนไหวตราสารทุน -->
        </th>
    </tr>
    
    <tr>
        <th colspan="16" style="text-align: center">{!! $pretty_date !!}</th>
    </tr>
    <tr>
        <th colspan="1" class="showcol" style="text-align: center"> SECURITY </th>
        <th colspan="1" class="showcol" style="text-align: center"> NAME </th>  
        <th colspan="1" class="showcol" style="text-align: center"> Maturity Date </th>  
        <th colspan="1" class="showcol" style="text-align: center"> TY RATE </th>  
        <th colspan="1" class="showcol" style="text-align: center"> FACE VALUE/UNIT </th>  
        <th colspan="1" class="showcol" style="text-align: center"> TOTAL COST </th>  
        <th colspan="1" class="showcol" style="text-align: center"> Red Yield </th>  
        <th colspan="1" class="showcol" style="text-align: center"> AMORTIZED COST </th>     
        <th colspan="1" class="showcol" style="text-align: center"> DISC/PREM  </th>   
        <th colspan="1" class="showcol" style="text-align: center"> ACCRUED INT  </th>   
        <th colspan="1" class="showcol" style="text-align: center"> DAY </th>
        <th colspan="1" class="showcol" style="text-align: center"> INT ENT(EX) </th>
        <th colspan="1" class="showcol" style="text-align: center"> MKT PRICE </th>      
        <th colspan="1" class="showcol" style="text-align: center"> DIRTY VALUE </th>   
        <th colspan="1" class="showcol" style="text-align: center"> %NAV </th> 
        <th colspan="1" class="showcol" style="text-align: center"> ALLOWANCE </th>        
    </tr>
    </thead>
        <tbody>
         @if($data)
            @foreach($data as $item)
            <tr>      
                <td style="text-align: right" >{{$item->SECURITIES_NAME}}</td>        <!-- SECURITY -->
                <td style="text-align: center">{{$item->SECURITIES_NAME}}</td>        <!-- NAME -->
                <td style="text-align: left"  >11/5/2023</td>      <!-- Maturity Date -->
                <td style="text-align: left"  >2.51</td>           <!-- TY RATE บลจ -->
                <td style="text-align: left"  >{{number_format((float)$item->FACE_VALUE, 2, '.', ',')}}</td>       <!-- FACE VALUE/UNIT -->
                <td style="text-align: right" >{{number_format((float)$item->TOTAL_COST, 2, '.', ',')}}</td>      <!-- TOTAL COST -->
                <td style="text-align: right" >{{number_format((float)$item->R_YIELD, 2, '.', ',')}}</td>              <!-- Red Yield -->

                <td style="text-align: right" >{{number_format((float)$item->AMORT_VALUE, 2, '.', ',')}}</td> <!-- AMORTIZED COST -->
                <td style="text-align: right" >{{number_format((float)11771.1,4, '.', ',')}}</td>    <!-- DISC/PREM -->
                <td style="text-align: right" >{{number_format((float)$item->ACCRUED_INT,2, '.', ',')}}</td>    <!-- ACCRUED INT -->
       
                <td style="text-align: right" >{{number_format((float)132,2, '.', ',')}}</td>        <!-- DAY -->
                <td style="text-align: right" >{{number_format((float)$item->INT_EX, 4, '.', ',')}}</td>          <!-- INT ENT(EX)-->
                <td style="text-align: right" >{{number_format((float)$item->MKTP_YIELD, 2, '.', ',')}}</td>   <!-- MKT PRICE -->
                <td style="text-align: right" >{{number_format((float)$item->DIRTY_MKT,2, '.', ',')}}</td>  <!-- DIRTY VALUE -->
                <td style="text-align: right" >{{number_format((float)$item->NAV_PERCENTAGE, 2, '.', ',')}}</td>        <!-- %NAV -->
                <td style="text-align: right" >{{number_format((float)$item->ALLOWANCE,2, '.', ',')}}</td>   <!-- ALLOWANCE -->
            </tr>
            @endforeach
        @endif 
		<tr>      
                <td style="text-align: right" >BOT181A</td>        <!-- SECURITY -->
                <td style="text-align: center">BOT181A</td>        <!-- NAME -->
                <td style="text-align: left"  >26/1/2018</td>      <!-- Maturity Date -->
                <td style="text-align: left"  >2.2</td>           <!-- TY RATE บลจ -->
                <td style="text-align: left"  >133300000</td>       <!-- FACE VALUE/UNIT -->

                <td style="text-align: right" >135299426.18</td>      <!-- TOTAL COST -->
                <td style="text-align: right" >1.358916</td>              <!-- Red Yield -->

                <td style="text-align: right" >{{number_format((float)134782407.97, 2, '.', ',')}}</td> <!-- AMORTIZED COST -->
                <td style="text-align: right" >{{number_format((float)-517018.21,4, '.', ',')}}</td>    <!-- DISC/PREM -->
                <td style="text-align: right" >{{number_format((float)449932.82,2, '.', ',')}}</td>    <!-- ACCRUED INT -->
       
                <td style="text-align: right" >{{number_format((float)56,2, '.', ',')}}</td>        <!-- DAY -->
                <td style="text-align: right" >{{number_format((float)0,4, '.', ',')}}</td>          <!-- INT ENT(EX)-->
                <td style="text-align: right" >{{number_format((float)1.52,2, '.', ',')}}</td>   <!-- MKT PRICE -->
                <td style="text-align: right" >{{number_format((float)134944016.89,2, '.', ',')}}</td>  <!-- DIRTY VALUE -->
                <td style="text-align: right" >{{number_format((float)4.11,2, '.', ',')}}</td>        <!-- %NAV -->
                <td style="text-align: right" >{{number_format((float)-288323.9,2, '.', ',')}}</td>   <!-- ALLOWANCE -->
            </tr>  
		<tr>      
                <td style="text-align: right" >BOT187A</td>        <!-- SECURITY -->
                <td style="text-align: center">BOT187A</td>        <!-- NAME -->
                <td style="text-align: left"  >20/7/2018</td>      <!-- Maturity Date -->
                <td style="text-align: left"  >1.55</td>           <!-- TY RATE บลจ -->
                <td style="text-align: left"  >380200000</td>       <!-- FACE VALUE/UNIT -->

                <td style="text-align: right" >381614593.38</td>      <!-- TOTAL COST -->
                <td style="text-align: right" >1.387537</td>              <!-- Red Yield -->

                <td style="text-align: right" >{{number_format((float)381288398.54, 2, '.', ',')}}</td> <!-- AMORTIZED COST -->
                <td style="text-align: right" >{{number_format((float)-326194.84,4, '.', ',')}}</td>    <!-- DISC/PREM -->
                <td style="text-align: right" >{{number_format((float)1001020.98,2, '.', ',')}}</td>    <!-- ACCRUED INT -->
       
                <td style="text-align: right" >{{number_format((float)62,2, '.', ',')}}</td>        <!-- DAY -->
                <td style="text-align: right" >{{number_format((float)0,4, '.', ',')}}</td>          <!-- INT ENT(EX)-->
                <td style="text-align: right" >{{number_format((float)1.555,2, '.', ',')}}</td>   <!-- MKT PRICE -->
                <td style="text-align: right" >{{number_format((float)381140029.29,2, '.', ',')}}</td>  <!-- DIRTY VALUE -->
                <td style="text-align: right" >{{number_format((float)11.61,2, '.', ',')}}</td>        <!-- %NAV -->
                <td style="text-align: right" >{{number_format((float)-1149390.23,2, '.', ',')}}</td>   <!-- ALLOWANCE -->
            </tr>  
		<tr>      
                <td style="text-align: right" >GGLB16NA</td>        <!-- SECURITY -->
                <td style="text-align: center">GGLB16NA</td>        <!-- NAME -->
                <td style="text-align: left"  >29/11/2016</td>      <!-- Maturity Date -->
                <td style="text-align: left"  >3.53</td>           <!-- TY RATE บลจ -->
                <td style="text-align: left"  >212000000</td>       <!-- FACE VALUE/UNIT -->

                <td style="text-align: right" >217047159.44</td>      <!-- TOTAL COST -->
                <td style="text-align: right" >1.688787</td>              <!-- Red Yield -->

                <td style="text-align: right" >{{number_format((float)212709771.76, 2, '.', ',')}}</td> <!-- AMORTIZED COST -->
                <td style="text-align: right" >{{number_format((float)-4337387.68,4, '.', ',')}}</td>    <!-- DISC/PREM -->
                <td style="text-align: right" >{{number_format((float)2337344.52,2, '.', ',')}}</td>    <!-- ACCRUED INT -->
       
                <td style="text-align: right" >{{number_format((float)114,2, '.', ',')}}</td>        <!-- DAY -->
                <td style="text-align: right" >{{number_format((float)0,4, '.', ',')}}</td>          <!-- INT ENT(EX)-->
                <td style="text-align: right" >{{number_format((float)1.47,2, '.', ',')}}</td>   <!-- MKT PRICE -->
                <td style="text-align: right" >{{number_format((float)215136660.84,2, '.', ',')}}</td>  <!-- DIRTY VALUE -->
                <td style="text-align: right" >{{number_format((float)6.56,2, '.', ',')}}</td>        <!-- %NAV -->
                <td style="text-align: right" >{{number_format((float)89544.56,2, '.', ',')}}</td>   <!-- ALLOWANCE -->
            </tr> 
		<tr>      
                <td style="text-align: right" >GGLB183A</td>        <!-- SECURITY -->
                <td style="text-align: center">GGLB183A</td>        <!-- NAME -->
                <td style="text-align: left"  >16/3/2018</td>      <!-- Maturity Date -->
                <td style="text-align: left"  >2.45</td>           <!-- TY RATE บลจ -->
                <td style="text-align: left"  >161200000</td>       <!-- FACE VALUE/UNIT -->

                <td style="text-align: right" >162248468.54</td>      <!-- TOTAL COST -->
                <td style="text-align: right" >2.148034</td>              <!-- Red Yield -->

                <td style="text-align: right" >{{number_format((float)161923602.62, 2, '.', ',')}}</td> <!-- AMORTIZED COST -->
                <td style="text-align: right" >{{number_format((float)-324865.92,4, '.', ',')}}</td>    <!-- DISC/PREM -->
                <td style="text-align: right" >{{number_format((float)43280.59,2, '.', ',')}}</td>    <!-- ACCRUED INT -->
       
                <td style="text-align: right" >{{number_format((float)4,2, '.', ',')}}</td>        <!-- DAY -->
                <td style="text-align: right" >{{number_format((float)0,4, '.', ',')}}</td>          <!-- INT ENT(EX)-->
                <td style="text-align: right" >{{number_format((float)1.701181,2, '.', ',')}}</td>   <!-- MKT PRICE -->
                <td style="text-align: right" >{{number_format((float)163021880.79,2, '.', ',')}}</td>  <!-- DIRTY VALUE -->
                <td style="text-align: right" >{{number_format((float)4.97,2, '.', ',')}}</td>        <!-- %NAV -->
                <td style="text-align: right" >{{number_format((float)1054997.58,2, '.', ',')}}</td>   <!-- ALLOWANCE -->
            </tr>

		</tbody>
    <tfoot>

        <tr>
            <td colspan="16">
                {!! $htmlPaginate !!}
            </td>
        </tr>
    </tfoot>
</table>
</div>




<!-- Chart -->
<!--div id="bar-chart-good-bad" class="chart" style="display:block;">
    
</div-->

