@extends('backend.layouts.default')
@section('content')
    <?php
    $data = getmemulist();
    $arrSidebar =getSideBar($data);
    ?>

    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-table fa-fw "></i>
                    {{--Auth::user()->username--}} 
                    {{getMenutitle($arrSidebar)}}
                </h1>
            </div>

        </div>


        <!-- NEW COL START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
                <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>


                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        {{--action="{{action('UserController@postAddUser') }}"--}}
                        
                        <form id="smart-form-register" action=""   class="smart-form">
                            {!! csrf_field() !!}

                            <fieldset>
                                <section>
                                    <lable style="font-size:18px; display:none;">รหัส บริษัทจัด การหลักทรัพย์</lable>
                                    <label class="input">
                                        <!--
                                        <input type="text" id="record_id" name="record_id" placeholder="รหัส บริษัทจัด การหลักทรัพย์" readonly>
                                        -->
                                        <label style="font-size:18px" id="record_id" name="record_id" placeholder="รหัส บริษัทจัด การหลักทรัพย์"></label>
                                    </label>
                                </section>
                                
                                <section>
                                    <lable style="font-size:18px">ชื่อย่อ(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                    <label class="input">
                                        <input type="text" id="name_sht" name="name_sht" placeholder="ระบุชื่อย่อ">
                                        <b class="tooltip tooltip-bottom-right">ระบุชื่อย่อ</b> 
                                    </label>
                                </section>

                                <section>
                                    <lable style="font-size:18px">ชื่อบริษัทหลักทรัพย์(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                    <label class="input">
                                        <input type="text" id="company_name" name="company_name" placeholder="ระบุชื่อบริษัทหลักทรัพย์">
                                        <b class="tooltip tooltip-bottom-right">ระบุชื่อบริษัทหลักทรัพย์</b> 
                                    </label>
                                </section>

                                <section>
                                    <lable style="font-size:18px">ที่อยู่บริษัทหลักทรัพย์(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                    <label class="input">
                                        <textarea style="font-size:18px" class="form-control" rows="6" id="company_addr" name="company_addr" placeholder="  ระบุที่อยู่บริษัทหลักทรัพย์    "></textarea>
                                        <b class="tooltip tooltip-bottom-right">ระบุที่อยู่บริษัทหลักทรัพย์</b> 
                                    </label>
                                </section>

                                <!--section style="margin-top: 15px;margin-left: 20px;">
                                    <div class="inline-group">
                                        <label class="radio">
                                            <input type="radio" class="plan_status" name="plan_status" checked="" value="0">
                                            <i></i>เปิด</label>
                                        <label class="radio">
                                            <input type="radio" class="plan_status" name="plan_status" value="1">
                                            <i></i>ปิด</label>
                                    </div>
                                </section-->


                            </fieldset>


                            <fieldset>
                                <div class="row">
                                    <section class="col col-6">
                                        <lable style="font-size:18px">เบอร์ โทรศัพท์(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                        <label class="input">
                                            <input type="text"  id="company_phone" name="company_phone" placeholder="ระบุเบอร์ โทรศัพท์ x-xxxx-xxx">
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <lable style="font-size:18px">เบอร์ โทรสาร(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                        <label class="input">
                                            <input type="text" id="company_fax" name="company_fax" placeholder="ระบุเบอร์ โทรสาร x-xxxx-xxx">
                                        </label>
                                    </section>
                                </div>

                                <!--div class="row">

                                    <section class="col col-6">
                                        <lable style="font-size:18px">สัดส่วนตราสารหนี้ (ขั้นต่ำ)</lable>
                                        <label class="input">

                                            <input type="text" id="DEBT_MIN" name="DEBT_MIN" placeholder="สัดส่วนตราสารหนี้ (ขั้นต่ำ)">
                                             </label>
                                    </section>

                                    <section class="col col-6" id="expire_check">
                                        <lable style="font-size:18px">สัดส่วนตราสารหนี้ (ขั้นสูง)</lable>
                                        <label class="input state-success">
                                            <input type="text" id="DEBT_MAX" name="DEBT_MAX"  class="mea_date_picker" placeholder="สัดส่วนตราสารหนี้ (ขั้นสูง)"   >
                                        </label>
                                    </section>
                                </div-->

                            </fieldset>

                            <fieldset>

                                <div class="row">

                                    <section class="col col-6">
                                        <lable style="font-size:18px">วันที่เริ่มต้น(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                        <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="equity_start"  class="mea_date_picker" id="equity_start" placeholder="ระบุวันที่เริ่มต้น" readonly>
                                           
                                            {{--class="mea_date_picker" data-dateformat='dd/mm/yy'--}}
                                        </label>
                                    </section>

                                    <section class="col col-6">
                                        <lable style="font-size:18px">วันที่สิ้นสุด(<lable style="font-size:18px; color:red;">*</lable>)</lable>
                                        <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="equity_end"  class="mea_date_picker" id="equity_end" placeholder="ระบุวันที่สิ้นสุด" readonly>

                                            {{--class="mea_date_picker" data-dateformat='dd/mm/yy'--}}
                                        </label>
                                    </section>
                                </div>


                            </fieldset>
                            <footer>
                                <div class="row">
                                   <lable style="font-size:18px; color:red;">&nbsp;&nbsp;&nbsp;&nbsp;* Required field ​(ห้ามเป็นค่าว่าง)</lable>   
                                </div> 
                                <button type="button"  id="btn_form" class="btn btn-primary">ยืนยัน
                                </button>
                                <button type="button" class="btn btn-default" onclick="window.history.back();">
                                    ยกเลิก
                                </button>
                            </footer>
                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- END COL -->

    </div>


    <!-- PAGE RELATED PLUGIN(S) -->
    <script src="{{asset('backend/js/plugin/jquery-form/jquery-form.min.js')}}"></script>
    <script src="{{asset('backend/js/plugin/summernote/summernote.min.js')}}"></script>

    <script type="text/javascript">

        $(document).ready(function() {

           /* $(".chk_expire").on('click',function(){

                var res = $(this).val();

                if(res == 1){
                    $("#expire_check").hide();
                }
                if(res == 0){
                    $("#expire_check").show();
                }

            });
           */

            $.validator.addMethod("valueNotEquals", function(value, element, arg) {
                return arg != value;
            }, "Please Choose one");

            


            $("#smart-form-register").validate({

                    // Rules for form validation
                    rules : {
                        
                        name_sht : {
                            required : true
                        },

                        company_name : {
                            required : true
                        },

                        company_addr : {
                            required : true
                        },

                        company_phone : {
                            required : true,
                            maxlength : 40
                            
                        },
                        
                        company_fax : {
                            required : true,
                            maxlength : 40
                              
                        },

                        equity_start : {
                            required : true
                        },

                        equity_end : {
                            required : true
                        }
                    },

                    errorPlacement : function(error, element) {
                        error.insertAfter(element.parent());

                    }
                });
             
            function validatePhone(t) {
                var a = t; //document.getElementById(txtPhone).value;
                // var filter = /((\+[0-9]{1,1}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
               // var filter = /^[\+]?[0-9]{1}?[-\s]?[0-9]{4}[-\s]?[0-9]{3,6}$/
                var filter = /^[\*]?[0-9, -]+$/;
                if (filter.test(a)) {
                    return true;
                }
                else {
                   return false;
                }
            } 

            $("#btn_form").on('click',function(){

               /* var $registerForm = $("#smart-form-register").validate({

                    // Rules for form validation
                    rules : {
                        
                        company_code : {
                            required : true,
                        },

                        company_name : {
                            required : true,
                        },

                        company_addr : {
                            required : true,
                        },

                        company_phone : {
                            required : true,
                             // phhone : true,
                        },
                        
                        company_fax : {
                            required : true,
                             //  phone : true,
                        },

                        equity_start : {
                            required : true,
                        },

                        equity_end : {
                            required : true,
                        },
                    },

                    errorPlacement : function(error, element) {
                        error.insertAfter(element.parent());

                    }
                });
*/
                // if($registerForm.valid()){
                if($("#smart-form-register").valid()){
                    var name_sht  = $("#name_sht").val();
                    var company_name  = $("#company_name").val();
                    var company_addr  = $("#company_addr").val();
                    var company_phone = $("#company_phone").val();
                    var company_fax   = $("#company_fax").val();


                    // var equity_start = $('#equity_start').datepicker({ dateFormat: 'dd-mm-yy' }).val();
                    // var equity_start = $('#equity_end').datepicker({ dateFormat: 'dd-mm-yy' }).val();
                  //  var md = Date.parseDate(date:$("#equity_start").val(), pattern:"dd-mm-yyyy", loc:'th-th'));
                   // Alert("ERROR", md , null, null);
                   // return false;
                    var equity_start  = $("#equity_start").val();
                    var equity_end    = $("#equity_end").val();
                    
                    
                    if(!validatePhone(company_phone)) {
                        Alert("มีข้อผิดพลาด", "เบอร์ โทรศัพท์ไม่ถูกต้อง", null, null);
                        return false;
                    }
                    if(!validatePhone(company_fax)) {
                        Alert("มีข้อผิดพลาด", "เบอร์ โทรสารไม่ถูกต้อง", null, null);
                        return false;
                    }
                    
                    //var user_id       = get_userID();
                    //var plan_status  = $('input[name=plan_status]:checked').val();

                    var jsondata = {
                        'record'    : 'yyyyuuuu',
                        company_name:  company_name,
                        company_addr:  company_addr,
                        company_phone: company_phone,
                        company_fax:   company_fax,
                        equity_start:  equity_start,
                        equity_end:    equity_end,
                        name_sht:  name_sht
                    };

// abort(403, equity_end);
//    $(".result").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
                    MeaAjax(jsondata, "add", function(mresponse) {
                        if(mresponse.success){
                            AlertSuccess("บันทึกจัดการบริษ้ทเรียบร้อยแล้ว",function(){
                                window.location.href = "/admin/EquityCompany";
                            });

                        } else {
                            Alert("ERROR", mresponse.html, null, null);
                        }
                    });

                    return false;
                }
                return false;
            });

            //
            //  meaDatepicker("plan_start","plan_end");
            //  meaDatepicker("plan_end");
            //
            
            meaDatepicker("equity_start", "equity_end");
            meaDatepicker("equity_end");
        });

    </script>

@stop





