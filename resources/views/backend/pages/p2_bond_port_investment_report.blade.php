@extends('backend.layouts.default')
@section('content')
<?php
$data = getmemulist();
$arrSidebar =getSideBar($data);
?>

<!--link rel="stylesheet" href="{{asset('backend/js/plugin/chartist/chartist.min.css')}}"/> 
<link rel="stylesheet" href="{{asset('backend/js/plugin/chartist/ct-customize.css')}}">
<link rel="stylesheet" href="{{asset('backend/js/plugin/chartist/plugin/chartist-plugin-tooltip.css')}}"/-->

<!--link href="{{asset('backend/js/plugin/echart/asset/css/carousel.css')}}" rel="stylesheet"-->
<style type="text/css">
 #graph_label .tickLabel{

    font-size: 100%;
 }



 /*
ul.scroll-menu {
    position: relative;
    display: inherit !important;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
    -moz-overflow-scrolling: touch;
    -ms-overflow-scrolling: touch;
    -o-overflow-scrolling: touch;
    overflow-scrolling: touch;
    top: 0 !important;
    left: 0 !important;
    width: 100%;
    height: auto;
    max-height: 500px;
    margin: 0;
    border-left: none;
    border-right: none;
    -webkit-border-radius: 0 !important;
    -moz-border-radius: 0 !important;
    -ms-border-radius: 0 !important;
    -o-border-radius: 0 !important;
    border-radius: 0 !important;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    -ms-box-shadow: none;
    -o-box-shadow: none;
    box-shadow: none;
}
*/
/* CheckBox */ 
.hiddencol { display:none; };
.showcol {display:none;};
 
/* Scroll Menu */
#meacontainer {
     position:absolute;
     width:100%;
     top:0;
     bottom:0;
     /*left:154;*/
     z-index:-1;
     overflow:hidden;
     background: #FF0000;
 }

 #dpsettings {
     overflow-y: scroll;
     width:200px;
     background: #FFFFFF;
     float:left;
     height:350px;
     margin-top:-5px;
     margin-left:154px
     position: absolute;
 }

 .glyphicon-plus {
       color: #00FF00; 
   }
   .glyphicon-danger {
       color: #b41111; 
   }
   .glyphicon-lightgreen {
       color: #12940d; 
   }
   .glyphicon-green {
       color: #00FF00; 
   }
   .glyphicon-info {
       color: #5bc0de; 
   }
   .glyphicon-pink {
       color: #f20772; 
   }
   .glyphicon-luna {
       color: #c92456; 
   }

   /* table split row */
   .top_row_line {
    display: table;
    width: 100%;
   }

   .top_row_line > div {
    display: table-cell;
    width: 50%;
    border-bottom: 1px solid #ccc;
   }
   /* auto complete */
   .container { width: 800px; margin: 0 auto; }

   .autocomplete-suggestions { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 1px solid #999; background: #FFF; cursor: default; overflow: auto; -webkit-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); -moz-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); }
   .autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
   .autocomplete-no-suggestion { padding: 2px 5px;}
   .autocomplete-selected { background: #F0F0F0; }
   .autocomplete-suggestions strong { font-weight: bold; color: #000; }
   .autocomplete-group { padding: 2px 5px; }
   .autocomplete-group strong { font-weight: bold; font-size: 18px; color: #000; display: block; border-bottom: 1px solid #000; }

   /* Section Header*/
    .custom-combobox {
        position: relative;
        display: inline-block;
    }
    
    .custom-combobox-toggle {
        position: absolute;
        top: 0;
        bottom: 0;
        margin-left: -1px;
        padding: 0;
    }
      .custom-combobox-input {
        margin: 0;
        padding: 5px 10px;
      }

      optgroup {
        background-color: #FF8000; //#DCDCDC;
        color: white;
      }
      option {
        background-color: white;
        color: black;
      }

      #header-fixed {
        position: fixed;
        top: 0px; display:none;
        background-color:white;
      }
</style>

<div id="content">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>

                {{getMenutitle($arrSidebar)}}

            </h1>
        </div>

    </div>


    <div class="row" id="widget-grid" >

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-5" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <ul class="nav nav-tabs pull-left in">

                        <li class="active">

                            <a data-toggle="tab" href="#hr1"> <i class="fa fa-lg fa-arrow-circle-o-down"></i> <span class="hidden-mobile hidden-tablet"> ระบุเงื่อนไขการค้นหา </span> </a>

                        </li>

                        <!--li>
                            <a data-toggle="tab" href="#hr2"> <i class="fa fa-lg fa-arrow-circle-o-up"></i> <span class="hidden-mobile hidden-tablet"> รายงานสรุปของตราสารทุน  </span> </a>
                        </li-->

                    </ul>
                </header>

                <!-- widget div-->
                <div>


                    <!-- widget content -->
                    <div class="widget-body">

                        <div class="tab-content">
                            <div class="tab-pane active" id="hr1">

                                <div class="widget-body no-padding" style="width: 100% ;margin: 0 auto;background-color: #000000;border: 1px solid #E1E8F3">
                                    <header style="color: #fff; height: 40px; line-height: 40px; font-size: 18px; background-color: #a90329;padding-left: 20px;"> กล่องค้นหา </header>

                                    <form  id="comment-form_darkman" class="smart-form" novalidate="novalidate">
                                        {{-- {!! csrf_field() !!} --}}
                                        <fieldset>
                                            <div class="row">
                                                
                                                <section class="col col-4">
                                                    <label class="label">&nbsp;ชื่อตราสาร</label>
                                                    <label class="input">
                                                           
                                                          <input type="text"  name="symbol" placeholder="ระบุประเภทตราสาร"
                                                          id="symbol" 
                                                          style="width: 100%; border-width: 1px;"/>
        
                                                    </label>
                                                </section>
                                               
                                                <section class="col col-4">
                                                    <label class="label">&nbsp;ชื่อบริษัท จัดการ</label>
                                                    <label class="input">
                                                        <select name="securities_name" id="securities_name" class="form-control">
                                                            <option value="">ระบุชื่อบริษัท จัดการ</option>
                                                            @if($equitylist)
                                                                @foreach($equitylist as $item)
                                                                    <option value="{{$item->NAME_SHT}}">&nbsp;{{$item->NAME_SHT}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </label>
                                                </section>
                                               
                                            </div>

                                            <div class="row">
                                                <section class="col col-4">
                                                    <label class="label">&nbsp;อายุตราสาร</label>
                                                    <label class="input">
                                                        <select name="age" id="age" class="form-control">
                                                            <option value="">ระบุอายุตราสาร</option>
                                                            <!--
                                                            <option value="0">&nbsp;คงเหลือน้อยกว่า หรือเท่ากับ 6 เดือน</option>
                                                            <option value="1">&nbsp;คงเหลือมากกว่า 6 เดือน แต่น้อยกว่า หรือเท่ากับ 1 ปี</option>
                                                            <option value="2">&nbsp;คงเหลือมากกว่า 1 ปี แต่น้อยกว่า หรือเท่ากับ 3 ปี</option>
                                                            <option value="3">&nbsp;คงเหลือมากกว่า 3 ปี แต่น้อยกว่า หรือเท่ากับ 5 ปี</option>
                                                            <option value="4">&nbsp;คงเหลือมากกว่า 5 ปีขึ้นไป</option>
                                                            -->
                                                            <option value="0">คงเหลือน้อยกว่าหรือเท่ากับ 3 เดือน </option>
                                                            <option value="1">คงเหลือมากกว่า 3 เดือน แต่น้อยกว่า หรือเท่ากับ 6 เดือน</option>
                                                           
                                                            <option value="2">คงเหลือมากกว่า 6 เดือน แต่น้อยกว่า หรือเท่ากับ 1 ปี</option>
                                                            <option value="3">คงเหลือมากกว่า 1 ปี แต่น้อยกว่า หรือเท่ากับ 3 ปี</option>
                                                            <option value="4">คงเหลือมากกว่า 3 ปี แต่น้อยกว่า หรือเท่ากับ 5 ปี</option>
                                                            <option value="5">คงเหลือมากกว่า 5 ปีขึ้นไป</option>
                                                        </select>
                                                    </label>
                                                </section>

                                                <section class="col col-4">
                                                     <label style="font-size:18px">&nbsp;ประเภทตราสาร</label>
                                                     <label class="input">
                                                        <select name="industrial" id="industrial" class="form-control">
                                                            <option value="">&nbsp;กรุณาเลือก หนึ่งรายการ &nbsp;</option>
                                                            @if($sectionList)
                                                              @foreach($sectionList as $key => $value)
                                                                <optgroup label="&nbsp;{{$key}}">
                                                                    @foreach($value as $child)
                                                                      
                                                                          <option value="{{$child['CATE_ID']}}">&nbsp;&nbsp;&nbsp;{{$child['BOND_TYPE_THA']}}</option>

                                                                    @endforeach
                                                                </optgroup> 
                                                              @endforeach   
                                                            @endif
                                                            
                                                            
                                                        </select>
                                                    </label>
                                                </section>

                                            </div>    

                                            <div class="row">
                                                <section class="col col-4">
                                                    <!--label class="label"><input type="checkbox" id="check_date" value="date_start"> ระบุช่วงเวลา</label-->
                                                    <label class="label">&nbsp;ระบุช่วงเวลา  </label>
                                                    <label class="input"><i class="icon-append fa fa-calendar"></i>
                                                        <input type="text" class="mea_date_picker" id="date_start" >
                                                    </label>
                                                </section>
                                                <section class="col col-4">
                                                    <label class="label">ถึง</label>
                                                    <label class="input"><i class="icon-append fa fa-calendar"></i>
                                                        <input type="text" class="mea_date_picker" id="date_end">
                                                    </label>
                                                </section>
                                                <section class="col col-4">
                                                    <button type="submit" id="btn_search" style="padding: 5px 20px 5px 20px; margin-top:20px;font-size: 18px;" name="submit" class="btn btn-primary">
                                                        ค้นหา
                                                    </button>
                                                </section>
                                            </div>
                                        </fieldset>

                                    </form>

                                </div>


                                <div id="meacontainer"></div>
                                <div class="container" style="margin-left:-12px; margin-top: 0px;">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="button-group bullet">
                                                <!--button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span></button-->
                                                 <a href="#" id="export_search" name="export_search" style="margin-top: 30px;" 
                                                    class="btn btn-labeled btn-success mea-btn-export"> 
                                                    <span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i></span>Export </a>
                                                 <!--
                                                 <a href="#" id="btn_customize" style="margin-top: 30px;" class="btn btn-labeled btn-warning dropdown-toggle" data-toggle="dropdown"> <span class="btn-label"><i class="glyphicon glyphicon-list"></i></span>Customize </a>
                                                 -->                                               
                                             
                                                 <ul id='dpsettings' role="menu" class="dropdown-menu pull-center">
                                                    <li><a href="#" class="small" data-value="selectall" tabIndex="-1"><input id="selectall" type="checkbox"/>&nbsp;เลือกทุกค่า</a></li>
                                                   

                                                    <li class="divider"></li>
                                                    <p><a href="#"><button class="btn btn-success" id='btn-save-settings' style="margin-left:20px; margin-top:5px; margin-right:5px; padding: 5px 10px 5px 5px; ">&nbsp;จัดเก็บ</button></a>
                                                        <a href="#">
                                                          <button class="btn btn-danger" id='btn-cancel-settings' style="margin-left:20px; margin-top:5px; margin-right:5px; padding: 5px 10px 5px 5px; ">&nbsp;ยกเลิก</button></a></p>
                                                     <li class="divider"></li>
                                                    </a></p>
                                                     
                                                    <li><a href="#" class="small" data-value="option0" tabIndex="-1"><input id="option0" type="checkbox"/>&nbsp;วันเดือนปี</a></li>
                                                    <li><a href="#" class="small" data-value="option1" tabIndex="-1"><input id="option1" type="checkbox"/>&nbsp;ชื่อ</a></li>
                                                    <li><a href="#" class="small" data-value="option2" tabIndex="-1"><input id="option2" type="checkbox"/>&nbsp;บลจ.</a></li>
                                                    <li><a href="#" class="small" data-value="option3" tabIndex="-1"><input id="option3" type="checkbox"/>&nbsp;กลุ่มอุตสาหกรรม</a></li>
                                                    <li><a href="#" class="small" data-value="option4" tabIndex="-1"><input id="option4" type="checkbox"/>&nbsp;หมวดธุรกิจ</a></li>
                                                     
                                                    <li class="divider"></li>
                                                    <li class="disabled" label="เงินปันผล"><a href="#"><i class="icon-user"></i> <b> เงินปันผล</b></a></li>
                                                     
                                                    <li><a href="#" class="small" data-value="option5" tabIndex="-1"><input id="option5" type="checkbox"/>&nbsp;ผลปันผล</a></li>
                                                    <!--li><a href="#" class="small" data-value="option6" tabIndex="-1"><input id="option5" type="checkbox"/>&nbsp;ราคาต่อหน่วย</a></li-->
                                                    <li><a href="#" class="small" data-value="option6" tabIndex="-1"><input id="option6" type="checkbox"/>&nbsp;จำนวนเงิน</a></li>
                                                    
                                                    <li class="divider"></li>
                                                    <li class="disabled"><a href="#"><i class="icon-user"></i> <b> ข้อมูลการซื้อ</b></a></li>  
                                                    <li><a href="#" class="small" data-value="option7" tabIndex="-1"><input id="option7" type="checkbox"/>&nbsp;หน่วยซื้อ</a></li>
                                                    <li><a href="#" class="small" data-value="option8" tabIndex="-1"><input id="option8" type="checkbox"/>&nbsp;ราคาซื้อต่อหน่วย </a></li>
                                                    <li><a href="#" class="small" data-value="option9" tabIndex="-1"><input id="option9" type="checkbox"/>&nbsp;ยอดซื้อ (บาท)</a></li>
                                                    <li><a href="#" class="small" data-value="option10" tabIndex="-1"><input id="option10" type="checkbox"/>&nbsp;คงเหลือ (หน่วย)</a></li>

                                                    <li class="divider"></li>
                                                    <li class="disabled"><a href="#"><i class="icon-user"></i> <b> ข้อมูลการขาย</b></a></li>                                                       
                                                    <li><a href="#" class="small" data-value="option11" tabIndex="-1"><input id="option11" type="checkbox"/>&nbsp;หน่วยขาย</a></li>
                                                    <li><a href="#" class="small" data-value="option12" tabIndex="-1"><input id="option12" type="checkbox"/>&nbsp;ราคาขายต่อหน่วย</a></li>
                                                    <li><a href="#" class="small" data-value="option13" tabIndex="-1"><input id="option13" type="checkbox"/>&nbsp;ยอดขาย (บาท) </a></li>
                                                    <li><a href="#" class="small" data-value="option14" tabIndex="-1"><input id="option14" type="checkbox"/>&nbsp;กำไร-ขาดทุน</a></li>
                                                    <li><a href="#" class="small" data-value="option15" tabIndex="-1"><input id="option15" type="checkbox"/>&nbsp;คงเหลือ (หน่วย)</a></li>
                                                    
                                                    <!--li class="divider"></li-->

                                                </ul>
                                                
                                                <a href="#" id="btn_chart"     style="margin-top: 30px;" class="btn btn-labeled btn-info"> <span class="btn-label"><i class="glyphicon glyphicon-signal"></i></span>Chart </a>
 
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Widget ID (each widget will need unique ID)-->
                                
                                <div class="jarviswidget jarviswidget-color-blueDark" style="margin-top: 5px; display:block;" 
                                     id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
                                    <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                    data-widget-colorbutton="false"
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true"export_search
                                    data-widget-sortable="false"

                                    -->

                                    <select class="form-control mea-pagesize" id="page-size-search" style="display:none;">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                        <option value="3000" selected>3000</option>
                                    </select>

                                    <header>
                                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    </header>

                                    <!-- widget div-->
                                    <div id="widget_search_table" style="display:block;">

                                        <!-- widget edit box -->
                                        <div class="jarviswidget-editbox">
                                            <!-- This area used as dropdown edit box -->

                                        </div>
                                        <!-- end widget edit box -->
                                        
                                        
                                        <!-- widget content -->
                                        <div class="widget-body"  >
                                            <div class="table-responsive" >
                                                <!--p class="report-title">
                                                    {{getMenutitle($arrSidebar)}}
                                                </p-->
                                                <div id="serch_data">

                                                </div>
                                            </div>

                                        </div>
                                        <!-- end widget content -->
                                           
                                    </div>
                                    
                                    <!-- end widget div -->

                                

                                

                                 
                                    <!-- bar chart -->
                                    <div class="table-responsive" id="widget_bar_chart" style="display:none;">

                                            @if((!empty($date_start)) && (!empty($date_end)))
                                            <p class="report-title" id="trading-chart-title" style="display:block;">
                
                                                {{getMenutitle($arrSidebar)}} (ตามประเภทของสารตรา)
                                            </p>
                                            <p class="report-period">
                                                
                                                @if($date_start != $date_end)
                                                     ข้อมูล ระหว่าง วันที่ <span color="#ff0000">{{$date_start}}</span> ถึง <span color="#ff0000">{{$date_end}} </span>
                                                @else
                                                     ข้อมูล วันที่ <span color="#ff0000">{{$date_start}}</span> เวลา
                                                                <span color="#ff0000">00:00</span> ถึง <span color="#ff0000">23:59</span>     
                                                @endif
                                            </p>
                                            @else
                                              <!--p class="report-period">
                                              รายงานข้อมูล ณ วันที่ {{get_date_notime(date("Y-m-d H:i:s"))}}
                                              </p-->
                                            @endif
                                            <div id="main_chart1" class="mea_chart" style="width: 100%; padding: 0px; display:block;">

                                            </div>
                                            
                                    </div>

                                    <!-- bar chart AGE Jan/30/2018 -->
                                    <div class="table-responsive" id="widget_bar_chart_age" style="display:none;">

                                            @if((!empty($date_start)) && (!empty($date_end)))
                                            <p class="report-title" id="trading-chart-title" style="display:block;">
                
                                                {{getMenutitle($arrSidebar)}} (ตามอายุคงเหลือของสารตรา)
                                            </p>
                                            <p class="report-period">
                                                
                                                @if($date_start != $date_end)
                                                     ข้อมูล ระหว่าง วันที่ <span color="#ff0000">{{$date_start}}</span> ถึง <span color="#ff0000">{{$date_end}} </span>
                                                @else
                                                     ข้อมูล วันที่ <span color="#ff0000">{{$date_start}}</span> เวลา
                                                                <span color="#ff0000">00:00</span> ถึง <span color="#ff0000">23:59</span>     
                                                @endif
                                            </p>
                                            @else
                                              <!--p class="report-period">
                                              รายงานข้อมูล ณ วันที่ {{get_date_notime(date("Y-m-d H:i:s"))}}
                                              </p-->
                                            @endif
                                            <div id="main_chart2" class="mea_chart" style="width: 100%; padding: 0px; display:block;">

                                            </div>
                                            
                                    </div>

                                </div>
                                 
                                <!-- end widget -->
                            </div>
                            

                        </div>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->



        </article>
        <!-- WIDGET END -->



    </div>



</div>
<!-- END MAIN CONTENT -->

<!-- FLOT CHARTS -->
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.time.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.selection.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.resize.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.pie.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.stack.min.js')}}"></script>
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.crosshair.min.js')}}"></script>
<script type="text/javascript" language="javascript" src="{{asset('backend/js/plugin/flot_p2/jquery.flot.orderBars.js')}}"></script>

<!-- flot tooltips plugin-->
<script src="{{asset('backend/js/plugin/flot_p2/jquery.flot.tooltip.min.js')}}"></script>

<!-- FLOT GROWRAF -->
<script src="{{asset('backend/js/plugin/flot-growraf/jquery.flot.growraf.min.js')}}"></script>



<!-- ECHART english -->
<script src="{{asset('backend/js/plugin/echart/www/js/echarts-all-english-v2.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('backend/js/plugin/echart/www/js/style.css')}}" />

<!-- Auto Complete -->
<script src="{{asset('backend/js/plugin/jquery-autocomplete/scripts/jquery.mockjax.js')}}"></script> 
<script src="{{asset('backend/js/plugin/jquery-autocomplete/src/jquery.autocomplete.js')}}"></script> 
 
<script src="{{asset('backend/js/floatThead/jquery.floatThead.js')}}"></script> 

<script type="text/javascript">

    /**
     * Auto Complete
     * see BondPortInvestmentReportReport :: "SELECT SYMBOL, COMP_NAME FROM TBL_P2_EQUITY_INDEX ORDER BY SYMBOL ASC";
     */
    var symbols = { 
        @foreach($symbols as $item)
            '{{$item->SYMBOL}}' : '{{$item->SYMBOL}}', 
        @endforeach
    }

    // START: EChart 
    /**
     * EChart theme
     */
    var theme1 = {
    backgroundColor: '#FFFFFF',//'#F2F2E6',
    //  
    color: [
        '#04b8ce','#04b8ce','#8AED35','#ef3430','#8AED35','#30b193','#8AED35','#F4E24E','#FE9616','#ff69b4',
        '#E42B6D','#ba55d3','#cd5c5c','#ffa500','#40e0d0',
        '#E95569','#ff6347','#7b68ee','#00fa9a','#ffd700',
        '#6699FF','#ff6666','#3cb371','#b8860b','#30e0e0'
    ],

    //  
    title: {
        //backgroundColor: '#FFFFFF', //'#F2F2E6',
        itemGap: 0 ,                
        textStyle: {
           // color: '#000', //'#8A826D',
            fontFamily : 'DB Ozone X'
        },
        subtextStyle: {
            color: '#8A826D',//'#E877A3',
            fontFamily : 'DB Ozone X'           
        }
    },

    dataRange: {
        x:'right',
        y:'center',
        itemWidth: 5,
        itemHeight:25,
        color:['#E42B6D','#F9AD96'],
        text:['高','低'],          
        textStyle: {
            color: '#8A826D'           
        }
    },

    toolbox: {
        color : ['#E95569','#E95569','#E95569','#E95569'],
        effectiveColor : '#ff4500',
        itemGap: 8
    },

    tooltip: {
        backgroundColor: 'rgba(100,100,79,0.8)', //'rgba(138,130,109,0.5)',      
        axisPointer : {             
            type : 'line',          
            lineStyle : {           
                color: '#6B6455',
                type: 'dashed'
            },
            crossStyle: {           
                color: '#A6A299'
            },
            shadowStyle : {                      
                color: 'rgba(200,200,200,0.3)'
            }
        }
    },

 
    dataZoom: {
        dataBackgroundColor: 'rgba(130,197,209,0.5)',            
        fillerColor: 'rgba(233,84,105,0.1)',    
        handleColor: 'rgba(107,99,84,0.8)'     
    },

     
    grid: {
        borderWidth:0
    },

   
    categoryAxis: {
        axisLine: {             
            lineStyle: {        
                color: '#6B6455'
            }
        },
        splitLine: {            
            show: false
        }
    },

     
    valueAxis: {
        axisLine: {             
            show: false
        },
        splitArea : {
            show: false
        },
        splitLine: {            
            lineStyle: {        
                color: ['#FFF'],
                type: 'dashed'
            }
        }
    },

    polar : {
        axisLine: {             
            lineStyle: {        
                color: '#ddd'
            }
        },
        splitArea : {
            show : true,
            areaStyle : {
                color: ['rgba(250,250,250,0.2)','rgba(200,200,200,0.2)']
            }
        },
        splitLine : {
            lineStyle : {
                color : '#ddd'
            }
        }
    },

    timeline : {
        lineStyle : {
            color : '#6B6455'
        },
        controlStyle : {
            normal : { color : '#6B6455'},
            emphasis : { color : '#6B6455'}
        },
        symbol : 'emptyCircle',
        symbolSize : 3
    },

    bar: {
        itemStyle: {
            normal: {
                barBorderRadius: 0
            },
            emphasis: {
                barBorderRadius: 0
            }
        }
    },

 
    line: {
        smooth : true,
        symbol: 'emptyCircle',   
        symbolSize: 3            
    },


     
    k: {
        itemStyle: {
            normal: {
                color: '#E42B6D',       
                color0: '#44B7D3',       
                lineStyle: {
                    width: 1,
                    color: '#E42B6D',    
                    color0: '#44B7D3'    
                }
            }
        }
    },

    scatter: {
        itemStyle: {
            normal: {
                borderWidth:1,
                borderColor:'rgba(200,200,200,0.5)'
            },
            emphasis: {
                borderWidth:0
            }
        },
        symbol: 'circle',   
        symbolSize: 4        
    },

     
    radar : {
        symbol: 'emptyCircle',     
        symbolSize:3 
    },

    map: {
        itemStyle: {
            normal: {
                areaStyle: {
                    color: '#ddd'
                },
                label: {
                    textStyle: {
                        color: '#E42B6D'
                    }
                }
            },
            emphasis: {                 
                areaStyle: {
                    color: '#fe994e'
                },
                label: {
                    textStyle: {
                        color: 'rgb(100,0,0)'
                    }
                }
            }
        }
    },

    force : {
        itemStyle: {
            normal: {
                nodeStyle : {
                    borderColor : 'rgba(0,0,0,0)'
                },
                linkStyle : {
                    color : '#6B6455'
                }
            }
        }
    },

    chord : {
        itemStyle : {
            normal : {
                chordStyle : {
                    lineStyle : {
                        width : 0,
                        color : 'rgba(128, 128, 128, 0.5)'
                    }
                }
            },
            emphasis : {
                chordStyle : {
                    lineStyle : {
                        width : 1,
                        color : 'rgba(128, 128, 128, 0.5)'
                    }
                }
            }
        }
    },

    gauge : {                  
        center:['50%','80%'],
        radius:'100%',
        startAngle: 180,
        endAngle : 0,
        axisLine: {            
            show: true,        
            lineStyle: {       
                color: [[0.2, '#44B7D3'],[0.8, '#6B6455'],[1, '#E42B6D']],
                width: '40%'
            }
        },
        axisTick: {            
            splitNumber: 2,    
            length: 5,         
            lineStyle: {       
                color: '#fff'
            }
        },
        axisLabel: {           
            textStyle: {       
                color: '#fff',
                fontWeight:'bolder'
            }
        },
        splitLine: {           
            length: '5%',      
            lineStyle: {       
                color: '#fff'
            }
        },
        pointer : {
            width : '40%',
            length: '80%',
            color: '#fff'
        },
        title : {
          offsetCenter: [0, -20],        
          textStyle: {        
            color: 'auto',
            fontSize: 20
          }
        },
        detail : {
            offsetCenter: [0, 0],       
            textStyle: {        
                color: 'auto',
                fontSize: 40
            }
        }
    }
};

    // END: EChart theme

    // checked menu
    var menus = []; 

    (function($)  {
    $.fn.extend({
      check : function()  {
         return this.filter(":checkbox").attr("checked", true);
      },
      uncheck : function()  {
         return this.filter(":checkbox").removeAttr("checked");
      }
    });
    }(jQuery));


    $(document).ready(function(){
        
        var lastScrollLeft = 0;
        $('.table-responsive').scroll(async function() {
            var documentScrollLeft = $('.table-responsive').scrollLeft();
            if (lastScrollLeft != documentScrollLeft) {
                lastScrollLeft = documentScrollLeft;
                await window.scrollBy(0,1);
                await window.scrollBy(0,-1);
            }
        });     

        $('#export_search').on('click',function() {
            // ensure dropdown closed 
            
             // hide dropdown menu
            $("body").trigger("click");

            var PageSizeAll = $('#page-size-search').val();
            var name_sht    = $('#securities_name').val();   // "UOBAM"
            var symbol      = $('#symbol').val();           // "APCS"
            var age         = $('#age').val();              // "age"
            var array       = symbol.split("|"); 
            if(array.length > 0)
               symbol = array[0].trim();

            var industrial  = $('#industrial').val();        // CATE_ID --> 21 
            var date_start  = $('#hd_date_start').val();
            var date_end    = $('#hd_date_end').val();

            var href = "BondPortInvestmentReport/exportsearch?symbol=" + symbol +"&name_sht=" + name_sht +
                                    "&age=" + age +
                                    "&date_start=" + date_start + "&date_end=" + date_end + 
                                    "&industrial=" + industrial;
            console.log('export href=' + href);
          
            window.location.href =  href;



            return false;
        });

        meaDatepicker("date_start","date_end");
        meaDatepicker("date_end");

        // $('#datepicker').val('').datepicker('update');  
        
        //$('#date_start').val(''); 
        //$('#date_end').val('');



        $('html').append('<input type="hidden" value="1" id="currentpage_search" />');

        $('#btn_search').on('click',function(e) {
          
            // hide dropdown menu
            $("body").trigger("click");
           
            // e.preventDefault();
            showByFlag(document.getElementById('page-size-search').id, false);
            showByFlag(document.getElementById('widget_search_table').id, true); 
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            //$("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            /* showhideByFlag(document.getElementById('trading-date').id, false);
                // showhideByFlag(document.getElementById('mea_chart2').id, false);
                // showhideByFlag(document.getElementById('wid-id-0').id, true);
            */
            var PageSizeAll = $('#page-size-search').val();
            var name_sht    = $('#securities_name').val();   // "UOBAM"
            var symbol      = $('#symbol').val();           // "APCS"
            var age         = $('#age').val();

            var array       = symbol.split("|"); 
            if(array.length > 0)
               symbol = array[0].trim();

            var industrial  = $('#industrial').val();        // CATE_ID --> 21 
            
            var date_start = $('#hd_date_start').val();
            var date_end = $('#hd_date_end').val();

           
            var jsondata = {
                pagesize : PageSizeAll,
                PageNumber:1,

                symbol : symbol,
                age: age,
                name_sht : name_sht,
                industrial : industrial,
                date_start:date_start,
                date_end:date_end
            };

            console.log(jsondata);
           
            MeaAjax(jsondata,"BondPortInvestmentReport/search", RenderSearch);

            return false;

        });


        $("#page-size-search").on('change',function() {
            // hide dropdown menu
            $("body").trigger("click");

            showByFlag(document.getElementById('widget_bar_chart').id, false);
             showByFlag(document.getElementById('widget_bar_chart_age').id, false);
            showByFlag(document.getElementById('widget_search_table').id, true);
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            var val = $(this).val();

            var name_sht    = $('#securities_name').val();   // "UOBAM"
            var symbol      = $('#symbol').val();            // "APCS"
            var age         = $('#age').val();
            var array       = symbol.split("|"); 
            if(array.length > 0)
               symbol = array[0].trim();

            var industrial  = $('#industrial').val();        // CATE_ID --> 21 
            var date_start  = $('#hd_date_start').val();
            var date_end    = $('#hd_date_end').val();

            var jsondata = {
                pagesize : val,
                PageNumber:1,
                symbol : symbol,
                age:age,
                name_sht : name_sht,
                industrial : industrial,
                date_start:date_start,
                date_end:date_end
            };

            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(jsondata,"BondPortInvestmentReport/search", RenderSearch);

        });


        // <!-- Toggle -->
        $('#btn_chart').on('click',function(e) { 
            //$('#btn-cancel-settings').closest(".dropdown-menu").prev().dropdown("toggle");
            $("body").trigger("click");

            showByFlag(document.getElementById('page-size-search').id, false);
            showByFlag(document.getElementById('widget_search_table').id, false); 
            
            var name_sht    = $('#securities_name').val();   // "UOBAM"
            var symbol      = $('#symbol').val();            // "APCS"
            var age         = $('#age').val();               // "age"
            var array       = symbol.split("|"); 
            if(array.length > 0)
               symbol = array[0].trim();

            var industrial  = $('#industrial').val();        // CATE_ID --> 21 
            var date_start  = $('#hd_date_start').val();
            var date_end    = $('#hd_date_end').val();

            var jsondata = {
                symbol : symbol,
                age:age,
                name_sht : name_sht,
                industrial : industrial,
                date_start:date_start,
                date_end:date_end
            };

            console.log(jsondata);
            $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');
            MeaAjax(jsondata,"BondPortInvestmentReport/chartsearch", RenderChart);

    
            if(document.getElementById('main_chart1').id) {
                showByFlag(document.getElementById('page-size-search').id, false);
                showByFlag(document.getElementById('widget_search_table').id, false);
                showByFlag(document.getElementById('widget_bar_chart').id, true);
                showByFlag(document.getElementById('widget_bar_chart_age').id, true);
            }
            if(document.getElementById('main_chart2').id) {
                showByFlag(document.getElementById('page-size-search').id, false);
                showByFlag(document.getElementById('widget_search_table').id, false);
               // showByFlag(document.getElementById('widget_bar_chart').id, true);
                showByFlag(document.getElementById('widget_bar_chart_age').id, true);
            }
             
            return false;
        });  // btn_chart

        /**
         * Cutomize Table Header
         */
        
        $( '.dropdown-menu a' ).on( 'click', function( event ) {
            event.preventDefault(); // Stop navigation
            var $target = $( event.currentTarget ),
               val = $target.attr( 'data-value' ),
               $inp = $target.find( 'input' ),
               idx;

            if(( idx = menus.indexOf( val )) > -1) {
                menus.splice( idx, 1 );
                setTimeout( function() { 
                    //selectall
                    if(val === 'selectall') {
                       $inp.prop( 'checked', false );
                       menus = [];
                        for(var xx=0; xx< 16; xx++) { 
                            $('#option'+ xx).prop('checked', false);
                        }
                    } else {
                        $inp.prop( 'checked', false );
                    }
              }, 0);
            } else {
                if(val !== undefined) {
                    if(val === 'selectall') {
                        // alert('selectall');
                         menus = [];
                         menus.push( val );
                         // alert('ooo');

                         setTimeout( function() { 
                            $inp.prop( 'checked', true );
                            for(var xx=0; xx< 16; xx++) { 
                               $('#option'+ xx).prop('checked', true);
                               menus.push('option'+ xx);
                            }
                          }, 0);
                        //$(":checkbox").check();
                        //$('#selectall').prop('checked', true);
                        //for(var xx=0; xx< 16; xx++)  
                         //   $('#option'+ xx).prop('checked', true);
                            //menus.push('option'+ xx);
                        
                    } else {
                         menus.push( val );
                         // alert('ooo');

                         setTimeout( function() { 
                            $inp.prop( 'checked', true );
                            
                          }, 0);
                    }
                } 
            }

            $( event.target ).blur();
              
            console.log( menus );
            return false;
        });
        
        $('#btn-save-settings').on('click', function(e) {
           e.preventDefault();
           $(this).closest(".dropdown-menu").prev().dropdown("toggle");
           var jsondata = {
                menu_id : 62,
                submenu_id :1,
                options : menus};
                
           MeaAjax(jsondata,"BondPortInvestmentReport/saveheader", RenderSaveSettings);
           
        });

        $('#btn-cancel-settings').on('click', function(e) {
            //$('.dropdown-menu').toggle();
             $(this).closest(".dropdown-menu").prev().dropdown("toggle");
        });

        
        // POST to read a current settings
        /*var jsondata = {
                menu_id : 62,
                submenu_id :1,
                options : menus};
                
        MeaAjax(jsondata,"BondPortInvestmentReport/readheader", RenderReadSettings);
        
        */

        // Initialize autocomplete with custom appendTo:
        var symbolsArray = $.map(symbols, function (value, key) { return { value: value, data: key }; });
        $('#symbol').autocomplete({
            lookup: symbolsArray
        });

    });  // jquery ready

    function RenderSaveSettings(json) {
        // TODO: handle settings results
    }

    function RenderReadSettings(json) {
        // clear menus
        menus = [];
        var options= json.result[ 0 ].options;
        if(options !== null) { 
            var size = options.filter(function(value) { return value !== undefined }).length;
            if(size < 1) return;
            for(var i = 0; i < size ; i++) {
               console.log('RenderReadSettings() : ' + options[i]);
               $('#'+options[i]).prop('checked', true);
               menus.push(options[i]);
            }
        }
    }


    
    function RenderChart(json) {

        var title    = '{{getMenutitle($arrSidebar)}} (ตามประเภทของสารตรา)';
        var title2    = '{{getMenutitle($arrSidebar)}} (ตามอายุคงเหลือของสารตรา)';
        
        console.log(json.result);
        var subtitle = ''; 
        if(json.result.date_start && json.result.date_end && (json.result.date_start != json.result.date_end)) {
            subtitle = 'ข้อมูล ระหว่าง วันที่ ' + json.result.date_start + ' ถึง ' + json.result.date_end;
        } else if(json.result.date_start){
            subtitle = 'ข้อมูล วันที่ ' + json.result.date_start + ' เวลา 00:00 ถึง 23:59';
        } else {
            subtitle = 'รายงานข้อมูล ณ วันที่ {{get_date_notime(date("Y-m-d H:i:s"))}}';
        }

        subtitle   = json.result.pretty_date;
        console.log('DATASOURCE=' + json.result.data);
        renderPortInvestmentChart('main_chart1', title , subtitle, json.result.data);
        renderPortInvestmentChartByAge('main_chart2', title2 , subtitle, json.result.data_age);
         
       /* showhideByFlag(document.getElementById('widget_bar_chart').id, false);
        showhideByFlag(document.getElementById('trading-date').id, false);
        showhideByFlag(document.getElementById('mea_chart').id, false);*/
  
    }

    function RenderSearch (data){
        $("#serch_data").html(data.html);
        $("#serch_data").fadeIn('300');

        $("#page_click_search li a").on('click',PageRenderSearch);
         
        showByFlag(document.getElementById('widget_bar_chart').id, false); 
        showByFlag(document.getElementById('widget_bar_chart_age').id, false); 
        showByFlag(document.getElementById('page-size-search').id, true);
        showByFlag(document.getElementById('widget_search_table').id, true); 

        $('#display_data_table').floatThead();


    }

    function PageRenderSearch(){

        var p = $(this).attr('data-page');
        var page_size = $('#page-size-search').val();
        var CurPage = $('#currentpage_search').val();
        $("#serch_data").html('<img style="margin: 0 auto;" src="/backend/img/spiner.gif" />');

        if(p == "pre"){
            p = parseInt(CurPage) - 1;
        }

        if(p == "next"){
            p = parseInt(CurPage) + 1;
        }
        $('#currentpage_search').val(p);

        var EmpID = $('#name').val();
        var depart = $('#depart').val();
        var plan = $('#plan').val();
        var date_start = $('#hd_date_start').val();
        var date_end   = $('#hd_date_end').val();

        var check_name =$('#check_name').is(':checked');
        var check_depart =$('#check_depart').is(':checked');
        var check_plan =$('#check_plan').is(':checked');
        var check_date =$('#check_date').is(':checked');


        var jsondata = {
            pagesize : page_size,
            PageNumber:p,
			symbol: $('#symbol').val(),
            name_sht: $('#securities_name').val(),
            age:$('#age').val(),
            industrial:$('#industrial').val(),
            emp_id : EmpID,
            depart :depart,
            plan : plan,
            date_start:date_start,
            date_end:date_end,
            check_name :check_name,
            check_depart:check_depart,
            check_plan:check_plan,
            check_date:check_date

        };

        MeaAjax(jsondata,"BondPortInvestmentReport/search",RenderSearch);
    };

    ////////////////////
    // TOOLS 
    function showhide(id) {
        var e = document.getElementById(id);
        e.style.display = (e.style.display == 'block') ? 'none' : 'block';
    }

 
    function showhideByFlag(id, flag) {
        var e = document.getElementById(id);
        e.style.display = flag ? 'block': 'none' ;

    }
    function showByFlag(id, flag) {
        var e = document.getElementById(id);
        e.style.display = flag ? 'block': 'none' ;

    }

    /*-----------------------------------------------------------------------------------*/
    /*  Hourly Chart
    /*-----------------------------------------------------------------------------------*/
    /*
    // BAR charts 
                    handleDashFlotChart(start_hour, 
                                        json.results.barchart_good, 
                                        json.results.barchart_bad);
    */


    
    function renderPortInvestmentChart(tagid, title, subtitle, datasource) //, trading_label, trading_data) 
    {
        var myChart1;
        var domCode = document.getElementById('sidebar-code');
        var domGraphic = document.getElementById('graphic');
        var domMain = document.getElementById(tagid);
        var domMessage = document.getElementById('wrong-message');
        var iconResize = document.getElementById('icon-resize');
        var needRefresh = false;
       
        var idx = 1;
        var isExampleLaunched;
        var curTheme;
        var option;

        var data1 = [];
        var data2 = [];
        var tooltips_data = [];
         

        var trading_label = [];
/*
        // BOND_CATE_THA, SUM(DIRTY_MKT) as SUM_DIRTY_MKT, SUM(PERCENT_NAV) as SUM_PERCENT_MKT 
        trading_label.push("พันธบัตรรัฐบาล");
        trading_label.push("ตราสารหนี้ภาคเอกชน");
        trading_label.push("หุ้นกู้มีหลักประกัน");
        trading_label.push("หุ้นกู้แปลงสภาพ");



       // for(var i=0; i<datasource.length; i++) {
           // trading_label.push(datasource[i].INDUSTRIAL);
           // convert to ล้านบาท
           data1.push( (135299 / 1000000).toFixed(2) );
           data1.push( (234112 / 1000000).toFixed(2) );
           data1.push( (56312 / 1000000).toFixed(2) );
           data1.push( (66312 / 1000000).toFixed(2) );
       // }

*/      
        if((datasource==null) || (datasource.length <1)) { 
            /* AlertSuccess("ไม่พบข้อมูล",function(){
                           // window.location.href = "simple";
                        });*/
            showByFlag(document.getElementById('widget_bar_chart').id, false);
            showByFlag(document.getElementById('widget_bar_chart_age').id, false);

             Alert('มีข้อผิดพลาด', "ไม่พบข้อมูล เพื่อแสดงกราฟ");
            return;
        }

        for(var i=0; i<datasource.length; i++) {
            trading_label.push(datasource[i].BOND_CATE_THA);
            data1.push(parseFloat(datasource[i].SUM_PERCENT_MKT));
            tooltips_data.push(parseFloat(datasource[i].SUM_DIRTY_MKT));
            tooltips_data[ datasource[i].BOND_CATE_THA ] = parseFloat(datasource[i].SUM_DIRTY_MKT);
        }
        /*
        var trading_label = [];
        for(var i=0; i<datasource.length; i++) {
           trading_label.push(datasource[i].INDUSTRIAL);
           // convert to ล้านบาท
           data1.push( (datasource[i].J / 1000000).toFixed(2) );
           data2.push( (datasource[i].N / 1000000).toFixed(2) );
        }
        */
        /*
        trading_label.push('พลังงาน');
        trading_label.push('ท่องเที่ยว');
        trading_label.push('บริการ');
        trading_label.push('ทรัพยากร');
        trading_label.push('ธุรกิจการเงิน');
        trading_label.push('สินค้าอุตสาหกรรม');
        trading_label.push('อสังหาริมทรัพย์');
        trading_label.push('เกษตร');
        trading_label.push('เทคโนโลยี');
        trading_label.push('อุตสาหกรรมอาหาร');
        trading_label.push('ก่อสร้าง');
        trading_label.push('อุปโภคบริโภค');

        data1.push( 1420);
        data1.push( 1030);
        data1.push( 1060);
        data1.push( 1340);
        data1.push( 1480);
        data1.push( 2440);
        data1.push( -2353);
        data1.push( 4623);
        data1.push( -1215);
        data1.push( 3617);
        data1.push( -1297);
        data1.push( 5586);

        data2.push( 3440);
        data2.push( 6760);
        data2.push( -3540);
        data2.push( 5580);
        data2.push( 5570);
        data2.push( -5520);
        data2.push( -4433);
        data2.push( 1013);
        data2.push( 1555);
        data2.push( 1087);
        data2.push( -1676);
        data2.push( -6476);
        /// END: FAKE
        */
        var yasix_label = 'Percentage';
        var captions = ['',
                        'ม.ค.','ก.พ','มี.ค.', 'เม.ย', 'พ.ค.', 'มิ.ย.',
                        'ก.ค','ส.ค.','ก.ย.','ต.ค.','พ.ย','ธ.ค.',
                        'รวม'
                       ];
        option = {
            title : {
                text: title,
                subtext: subtitle, //yasix_label,

                x:'center',
                textStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 23
                },
                subtextStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 18
                }
            },
/*
            tooltip : {
                trigger: 'axis',
                axisPointer : {             
                    type : 'shadow'        
                },
                formatter: function (params){
                     return params[0].name + '<br/>'
                     + params[0].seriesName + ' : ' + (params[0].value - 0).toFixed(2).toLocaleString() + ' ล้านบาท <br/>'
                     + params[1].seriesName + ' : ' + (params[1].value - 0).toFixed(2).toLocaleString() + ' ล้านบาท';
                }
            },
*/
            label : {
                formatter: function (value){
                     return (value).toFixed(2).toLocaleString() + ' ล้านบาท';
                }
            },

            grid: {
               borderColor: '#ccc'
            },

            legend: {
                //orient : 'vertical',
                x : 'left',
                y : 'top',
                textStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 14
                },
                data: ['% NAV'] 
            },

            toolbox: {
                    show : true,
                    orient : 'vertical',
                    x: 'right', 
                    y: 'top',
                    feature : {
                        mark : {show: false},
                        dataView : {show: true, readOnly: true},
                        magicType : {
                            show: true, 
                            type: ['line', 'bar'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1700
                                }
                            }
                        },
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },

            //calculable : true,

            xAxis : [
                {
                    name :'หมวดหมู่', 
                    nameTextStyle: {
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        },
                    type : 'category',
                    show : true,
                    data : trading_label, 

                    axisLine: {
                        show : true
                    },
                    axisTick: {
                        show: true,
                        interval : 0,
                        length: 1
                    },
                    axisLabel: {
                        show: true,
                        interval: 0,
                        textStyle: {
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        }
                    },

                }
            ],

            yAxis : [
                {
                    name : 'Percentage',
                    nameTextStyle: {
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        },

                    type : 'value',
                    show : true,
                    boundaryGap: [0, 0.1],
                    
                    axisLabel: {
                        show: true,
                        interval: 0,
                        textStyle: {
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        }
                    },
                }
            ],

            series : [
                {
                    name: '% NAV',
                    type: 'bar',
                    data: data1,
                    barWidth: 30, 
                    barGap:"10%",
    
                    formatter: function (value) {
                        return (value).toFixed( 2 ).toLocaleString() + ' ';
                    },
                    /*
                    markPoint : {
                        data : [
                            {type : 'max', name: 'ค่าสูงสุด'},
                            {type : 'min', name: 'ค่าต่ำสุด'}
                        ],

                        itemStyle: {
                            normal: {

                                label : {
                                    show: true, 
                                   // position: 'top', //'insideTop'
                                    textStyle: {
                                        //color: '#04b8ce',
                                        fontFamily : 'DB Ozone X',
                                        fontSize : 14
                                    }
                                }
                            }
                        },
                    },*/

                    itemStyle: {
                        normal: {
                            label : {
                                
                                show: true, 
                                position: 'top', //'insideTop'
                                formatter : function (params) {                         
                                    return params.name + ' : ' +  (params.value - 0).toFixed(2) + '%  \n' +
                                               ' ( ' + ((tooltips_data[params.name] - 0) / 1000000.00).toFixed(2).toLocaleString() + ' ล้านบาท )';
                                },
                                textStyle: {
                                    color: '#191ee8',
                                    fontFamily : 'DB Ozone X',
                                    fontSize : 14
                                }
                            }
                        }
                    },
                    
                }
                /*,
                {
                    name: 'ขาย',
                    type: 'bar',
                    data: data2,
                    barWidth: 30,
                    barGap:"10%",
                    formatter: function (value) {
                        return (value).toFixed(2).toLocaleString() + ' ล้านบาท';
                    },
                    markPoint : {
                        data : [
                            {type : 'max', name: 'ค่าสูงสุด'},
                            {type : 'min', name: 'ค่าต่ำสุด'}
                        ],

                        itemStyle: {
                            normal: {
                                label : {
                                    show: true, 
                                   // position: 'top', //'insideTop'
                                    textStyle: {
                                        //color: '#04b8ce',
                                        fontFamily : 'DB Ozone X',
                                        fontSize : 14
                                    }
                                }
                            }
                        },
                    },

                    itemStyle: {
                        normal: {
                            //color: 'tomato',
                            //barBorderColor: 'tomato',
                            //barBorderWidth: 6,
                            //barBorderRadius:0,
                            label : {
                                show: true, 
                                position: 'top'
                              
                                , //'insideTop'
                                textStyle: {
                                    color: '#d32f2f',
                                    fontFamily : 'DB Ozone X',
                                    fontSize : 14
                                }
                            }
                        }
                    },
                }*/
            ]

        }; // EChart option

        myChart1 = echarts.init(document.getElementById(tagid));
        myChart1.setOption(option);
  
        curTheme = theme1;
        myChart1.setTheme(curTheme);
    }


function renderPortInvestmentChartByAge(tagid, title, subtitle, datasource)  
{
        var myChart2;
        var domCode     = document.getElementById('sidebar-code');
        var domGraphic  = document.getElementById('graphic');
        var domMain     = document.getElementById(tagid);
        var domMessage  = document.getElementById('wrong-message');
        var iconResize  = document.getElementById('icon-resize');
        var needRefresh = false;
       
        var idx = 1;
        var isExampleLaunched;
        var curTheme;
        var option;

        var data1 = [];
        var data2 = [];
        var tooltips_data = [];
         

        var trading_label = [];

        if((datasource==null) || (datasource.length <1)) { 
            
            return;
        }

        

/*
        var caption_old = ['<= 3 เดือน ',
                       '<= 6 เดือน', 
                       '<= 1 ปี',
                       '> 6 เดือน <= 1 ปี',
                       '> 1 ปี <= 3 ปี',
                       '> 3 ปี <= 5 ปี',
                       '> 5 ปี'];
*/
        var caption = ['<= 3 เดือน ',
                       '> 3 เดือน <= 6 เดือน', 
                     //  '<= 1 ปี',
                       '> 6 เดือน <= 1 ปี', 
                       '> 1 ปี <= 3 ปี',
                       '> 3 ปี <= 5 ปี',
                       '> 5 ปี'];
        

        for(var x=0; x< caption.length; x++) {
            data1.push(0);
            trading_label.push(caption[x]);
            tooltips_data[caption[x]] = 0.0001;
        }


/*
        if(datasource.length === 9){
           
                // <= 3 เดือน
                var i = 0;
                data1.push(parseFloat(datasource[i].SUM_DIRTY_MKT) / 1000000);
                trading_label.push(caption[datasource[i].YMD]);
                tooltips_data[caption[datasource[i].YMD]] = datasource[i].SUM_PERCENT_MKT;
                tooltips_data.push(datasource[i].SUM_PERCENT_MKT);
                // <= 6 เดือน
                i = 1;
                data1.push(parseFloat(datasource[i].SUM_DIRTY_MKT) / 1000000);
                trading_label.push(caption[datasource[i].YMD]);
                tooltips_data.push(parseFloat(datasource[i].SUM_PERCENT_MKT));
                tooltips_data[caption[datasource[i].YMD]] = datasource[i].SUM_PERCENT_MKT;

                // FIXED <= 1 ปี
                // <= 1 ปี
                //i++; 
                var mkt = parseFloat(datasource[i].SUM_DIRTY_MKT) + parseFloat(datasource[2].SUM_DIRTY_MKT);
                var mkt_percent = parseFloat(datasource[i].SUM_PERCENT_MKT) + parseFloat(datasource[2].SUM_PERCENT_MKT);

                console.log("SUM_DIRTY_MKT datasource:" + JSON.stringify(datasource[i]));
                console.log("SUM_DIRTY_MKT datasource:" + JSON.stringify(datasource[2]));
                console.log("SUM_DIRTY_MKT[1] + [2] ==> MKT: " + (mkt / 100000));
                console.log("SUM_DIRTY_MKT : " + JSON.stringify(datasource));
                data1.push(mkt / 1000000);
                trading_label.push(caption[2]);
                tooltips_data.push(mkt * 100);
                tooltips_data[caption[2]] = mkt_percent;

                
                // > 6 เดือน <= 1 ปี
                i = 2;
                data1.push(parseFloat(datasource[i].SUM_DIRTY_MKT) / 1000000);
                trading_label.push(caption[datasource[i].YMD]);
                tooltips_data.push(parseFloat(datasource[i].SUM_PERCENT_MKT));
                tooltips_data[caption[datasource[i].YMD]] = datasource[i].SUM_PERCENT_MKT;

                // > 1 ปี <= 3 ปี
                i = 3;
                data1.push(parseFloat(datasource[i].SUM_DIRTY_MKT) / 1000000);
                trading_label.push(caption[datasource[i].YMD]);
                tooltips_data.push(parseFloat(datasource[i].SUM_PERCENT_MKT));
                tooltips_data[caption[datasource[i].YMD]] = datasource[i].SUM_PERCENT_MKT;

                //> 3 ปี <= 5 ปี
                i = 4;
                data1.push(parseFloat(datasource[i].SUM_DIRTY_MKT) / 1000000);
                trading_label.push(caption[datasource[i].YMD]);
                tooltips_data.push(parseFloat(datasource[i].SUM_PERCENT_MKT));
                tooltips_data[caption[datasource[i].YMD]] = datasource[i].SUM_PERCENT_MKT;

                // > 5 ปี
                i = 5;
                data1.push(parseFloat(datasource[i].SUM_DIRTY_MKT) / 1000000);
                trading_label.push(caption[datasource[i].YMD]);
                tooltips_data.push(parseFloat(datasource[i].SUM_PERCENT_MKT));
                tooltips_data[caption[datasource[i].YMD]] = datasource[i].SUM_PERCENT_MKT;

            //tooltips_data[ trading_label[i] ] = datasource[i].SUM_PERCENT_MKT;
        } else {
        */

        for(var i=0; i<datasource.length; i++) {
            data1[datasource[i].YMD] = parseFloat(datasource[i].SUM_DIRTY_MKT) / 1000000;
            tooltips_data[caption[datasource[i].YMD]] = parseFloat(datasource[i].SUM_PERCENT_MKT);
            console.log('### DATA[' + datasource[i].YMD +'] = ' + data1[datasource[i].YMD] + 
                        ' SUM PERCENT:' + tooltips_data[caption[datasource[i].YMD]]);
        }
             // Handle 1 ปี
        /*if(datasource.length > 0) {
            data1[2]         = (parseFloat(datasource[0].SUM_DIRTY_MKT) + parseFloat(datasource[1].SUM_DIRTY_MKT) + parseFloat(datasource[3].SUM_DIRTY_MKT)) / 1000000;
            tooltips_data[caption[2]] = (parseFloat(datasource[0].SUM_PERCENT_MKT) + parseFloat(datasource[1].SUM_PERCENT_MKT) + parseFloat(datasource[3].SUM_PERCENT_MKT));
        }*/

        

        var yasix_label = 'ล้านบาท';
        var captions = ['',
                        'ม.ค.','ก.พ','มี.ค.', 'เม.ย', 'พ.ค.', 'มิ.ย.',
                        'ก.ค','ส.ค.','ก.ย.','ต.ค.','พ.ย','ธ.ค.',
                        'รวม'
                       ];
        option = {
            title : {
                text: title,
                subtext: subtitle,  

                x:'center',
                textStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 23
                },
                subtextStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 18
                }
            },

            label : {
                formatter: function (value){
                     //return (value).toFixed(2).toLocaleString() + ' ล้านบาท';
                     return (value).toLocaleString() + ' ล้านบาท';
                }
            },

            grid: {
               borderColor: '#ccc'
            },

            legend: {
                //orient : 'vertical',
                x : 'left',
                y : 'top',
                textStyle: {
                    fontFamily : 'DB Ozone X',
                    fontSize : 14
                },
                data: ['Total Dirty Value'] 
            },

            toolbox: {
                    show : true,
                    orient : 'vertical',
                    x: 'right', 
                    y: 'top',
                    feature : {
                        mark : {show: false},
                        dataView : {show: true, readOnly: true},
                        magicType : {
                            show: true, 
                            type: ['line', 'bar'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1700
                                }
                            }
                        },
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },

            //calculable : true,

            xAxis : [
                {
                    name :'อายุตราสารคงเหลือ่', 
                    nameTextStyle: {
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        },
                    type : 'category',
                    show : true,
                    data : trading_label, 

                    axisLine: {
                        show : true
                    },
                    axisTick: {
                        show: true,
                        interval : 0,
                        length: 1
                    },
                    axisLabel: {
                        show: true,
                        interval: 0,
                        textStyle: {
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        }
                    },

                }
            ],

            yAxis : [
                {
                    name : yasix_label,
                    nameTextStyle: {
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        },

                    type : 'value',
                    show : true,
                    boundaryGap: [0, 0.1],
                    
                    axisLabel: {
                        show: true,
                        interval: 0,
                        textStyle: {
                            fontFamily : 'DB Ozone X',
                            fontSize : 14
                        }
                    },
                }
            ],

            series : [
                {
                    name: 'Total Dirty Value',
                    type: 'bar',
                    data: data1,
                    barWidth: 30, 
                    barGap:"10%",
    
                    formatter: function (value) {
                        return ((value-0)).toFixed( 2 ).toLocaleString() + ' ';
                    },

                    itemStyle: {
                        normal: {
                            label : {
                                
                                show: true, 
                                position: 'top', //'insideTop'
                                /*formatter : function (params) {                           
                                    return params.name + ' : ' +  ((params.value - 0) ).toFixed(2).toLocaleString() + ' ล้านบาท \n' +
                                               ' ( ' + (tooltips_data[params.name] - 0).toFixed(2).toLocaleString() + ' % )';
                                },*/
                                formatter : function (params) {                           
                                    return (((params.value - 0) ) > 0) ? ((params.value - 0) ).toFixed(2).toLocaleString() + ' ล้านบาท \n' +
                                               ' ( ' + (tooltips_data[params.name] - 0).toFixed(2).toLocaleString() + ' % )' : "";
                                },
                                textStyle: {
                                    color: '#191ee8',
                                    fontFamily : 'DB Ozone X',
                                    fontSize : 14
                                }
                            }
                        }
                    },
                    
                }
            ]

        }; // EChart option

        myChart2 = echarts.init(document.getElementById(tagid));
        myChart2.setOption(option);
  
        curTheme = theme1;
        myChart2.setTheme(curTheme);
    }


    var showBarChart = function () {

        function drawBarChart() { 
            var data1 = [], 
            data2 = [], 
            n = 8,  
            i = 0,
            st = n,
            ticks = [];

            var labels = []; 
            var suffix = 'ล้านบาท';
            var yasix_label = 'จำนวณ (ล้านบาท)';
    
            for (i=0; i<12; i++) {
                ticks.push(i);
            }

            labels.push('พลังงาน');
            labels.push('ท่องเที่ยว');
            labels.push('บริการ');
            labels.push('ทรัพยากร');
            labels.push('ธุรกิจการเงิน');
            labels.push('สินค้าอุตสาหกรรม');
            labels.push('อสังหาริมทรัพย์');
            labels.push('เกษตร');
            labels.push('เทคโนโลยี');
            labels.push('อุตสาหกรรมอาหาร');
            labels.push('ก่อสร้าง');
            labels.push('อุปโภคบริโภค');

            data1.push([ticks[0],  20]);
            data1.push([ticks[1],  30]);
            data1.push([ticks[2],  60]);
            data1.push([ticks[3],  -40]);
            data1.push([ticks[4],  80]);
            data1.push([ticks[5],  40]);
            data1.push([ticks[6],  53]);
            data1.push([ticks[7],  23]);
            data1.push([ticks[8],  15]);
            data1.push([ticks[9],  17]);
            data1.push([ticks[10], -97]);
            data1.push([ticks[11], 86]);

            data2.push([ticks[0],  40]);
            data2.push([ticks[1],  60]);
            data2.push([ticks[2],  70]);
            data2.push([ticks[3],  -80]);
            data2.push([ticks[4],  70]);
            data2.push([ticks[5],  20]);
            data2.push([ticks[6],  33]);
            data2.push([ticks[7],  13]);
            data2.push([ticks[8],  45]);
            data2.push([ticks[9],  87]);
            data2.push([ticks[10], 57]);
            data2.push([ticks[11], 26]);

            var plot = $.plot($("#trading-bar-chart"), [{
                    data: data1,
                    bars: {
                        show: true,
                        barWidth: 0.28,
                        fill: true,
                        fillColor : '#02a6ba', //'#8bc34a', //
                        //align: "right",
                        lineWidth: 1,
                        order: 1
                    },
                    label: "ซื้อ"

                }, {
                    data: data2,
                    bars: {
                        show: true,
                        barWidth: 0.28,
                        fill: true,
                        fillColor : '#d32f2f',
                        //align: "left",
                        lineWidth: 1,
                        order: 2
                    },
                    label: "ขาย" 

                }], {
                    series: {
                        bars: {
                             barWidth: 0.25,
                            // align: 'right',
                             show: true
                            // order: 2
                    }
                },
                // Legend
                colors: ['#04b8ce','#d32f2f','#8bcE4a',"#FE0E1C", '#d32f2f','#e91e63', 
                         '#FF5733','#0033FF', "#FF02FC0", "#FE0E1C"],


                grid: {
                    hoverable: true,
                    clickable: true,
                    borderWidth: 0.1,
                    markings: [
                        { color: '#12ea15', lineWidth: 1, yaxis: { from: 800, to: 800 } },
                        { color: '#ea1215', lineWidth: 1, yaxis: { from: 400, to: 400 } }
                    ]
                },

                legend: {
                    show: true
                },

                xaxis: {
                    autoscaleMargin: .035,
                    mode: "categories",
                    tickLength: 0,
                    ticks: ticks,
                    tickDecimals: 0,
                    tickColor: "#e1e1e1",
                    tickFormatter: function (x) { 
                    /*    if (x > 23)  {
                            return (x - 24).toFixed(2).replace(".", ":")  ;
                        } else  { 
                            return x.toFixed(2).replace(".", ":") ;
                        }*/ 
                        return labels[x];
                    }
                },

                yaxis: {
                    ticks: 12,
                    tickColor: "#eaeaea",
                    tickDecimals: 0,
                    min: 0,
                    tickFormatter: function(val, axis) { return val < axis.max ? val.toFixed(2) : yasix_label + '     ' + val.toFixed(2);
                    
                    }/*,
                    font:{
                      size:14,
                      style:"italic",
                      weight:"bold",
                      family:"sans-serif",
                      variant:"small-caps"
                    }*/
                    
                },

            });  //END:  $.plot

            function showTooltip(x, y, contents) {
                $('<div id="tooltip">' + contents + '</div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y + 5,
                    left: x + 10,
                    
                    'box-shadow': '0 0 10px #555',
                    padding: '8px',
                    color: '#fff',
                    'border-radius': '4px',
                    '-webkit-border-radius': '4px',
                    '-moz-border-radius': '4px',
                    'border-radius': '4px',
                    'background-color': '#333',
                    border: '1px solid #fff',
                    opacity: 0.82
                }).appendTo("body").fadeIn(200);
            }

            var previousPoint = null;

            $("#trading-bar-chart").bind("plothover", function (event, pos, item) {
                $("#x").text(pos.x.toFixed(2));
                $("#y").text(pos.y.toFixed(2));
                if (item) {
                    if (previousPoint != item.dataIndex) {
                        previousPoint = item.dataIndex;
                        $("#tooltip").remove();
                        var x = item.datapoint[0].toFixed(0),//.toFixed(2),
                            y = item.datapoint[1].toFixed(0);//.toFixed(2);
                        showTooltip(item.pageX, item.pageY, 
                            item.series.label + " - " + labels[x] + "=" + y + " " + suffix);
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            });
        }
        
        /* Run the charts */
        drawBarChart();
    }



</script>

@stop
