<?php


namespace App\Package;

class Curl
{

    private $ch;
    private $data;
    private $method = 'POST';

    public function __construct($function, $parameters, $method = 'POST', $url = '')
    {
//        $data = array("function" => "Login", "parameters" => [
//            array(
//                "session_id"=> "S1234",
//                "username"=>"1234567",
//                "pwd" => "aabb1213",
//                "os" => "Windows",                                              //Windows, Linux
//                "browser" => "Chrome",                                       //IE, Chrome, Firefox
//                "ip_address" => "202.168.55.199",
//                "access_channel" => "Mobile",                            //Mobile, Web
//                "device_id" => "712fewfwg1554fdwq",
//                "device_os" => "iOS"
//            )
//        ]);

        $this->method = $method;
        if(empty($url))
            //$this->url = 'http://172.16.12.13:8080/meaws/meafund';
            $this->url = 'https://mea.snapz.mobi/meaws/meafund';
        $this->data = array("function" => $function, "parameters" => array($parameters));
    }


    public function getResult()
    {        
        $this->ch = curl_init($this->url);
        $result = '{"result":[{"web_manual":"http:\/\/suntrue.sun-system.com:8847\/contents\/web_user_manual.pdf","investment_plan":"DIY","access_status_flag":"0","user_saving_rate":"11","equity":"20.75","fund_plan_change_per_year":"4","is_before_2538":"0","contribution_rate":"9","fund_plan_last_modify_date":"","risk_quiz_score":"21","user_privilege_id":"0","member_status":"Y","first_login_flag":"1","saving_rate_change_period_flag":"0","debt_rate":"","reference_date":"2016-06-16","equity_rate":"","web_admin_manual":"http:\/\/suntrue.sun-system.com:8847\/contents\/web_admin_manual.pdf","equity_funds":"891204.04","risk_quiz_date":"2017-02-20 13:26:56.0","fund_plan_change_period_flag":"0","dep_lng":"การไฟฟ้านครหลวง เขตสมุทรปราการ","current_fund_plan_modify_count":"0","risk_quiz_flag":"0","saving_rate_change_period":"01-31","mobile_manual":"http:\/\/suntrue.sun-system.com:8847\/contents\/mobile_manual.pdf","full_name":"Sun Support","investment_money":"4294504.87","fund_plan_change_period":"01-31","bond_funds":"3403300.83","debt":"79.25","emp_id":"9999999","access_menu_list":[{"group_id":"1","group_name":"ผลการดำเนินการ","menu_flag":"0","group_flag":"0","menu_name":"มูลค่ากองทุน","menu_id":"1"},{"group_id":"1","group_name":"ผลการดำเนินการ","menu_flag":"0","group_flag":"0","menu_name":"รายงานมูลค่าทรัพย์สิน","menu_id":"2"},{"group_id":"1","group_name":"ผลการดำเนินการ","menu_flag":"0","group_flag":"0","menu_name":"สรุปภาวะเศรษฐกิจและกลยุทธ์","menu_id":"3"},{"group_id":"2","group_name":"ประมวลระเบียบ","menu_flag":"0","group_flag":"0","menu_name":"ประกาศ","menu_id":"1"},{"group_id":"2","group_name":"ประมวลระเบียบ","menu_flag":"0","group_flag":"0","menu_name":"พระราชบัญญัติกองทุนสำรองเลี้ยงชีพ","menu_id":"2"},{"group_id":"2","group_name":"ประมวลระเบียบ","menu_flag":"0","group_flag":"0","menu_name":"คำสั่งแต่งตั้งคณะกรรมการ","menu_id":"3"},{"group_id":"2","group_name":"ประมวลระเบียบ","menu_flag":"0","group_flag":"0","menu_name":"ข้อบังคับกองทุน","menu_id":"4"},{"group_id":"3","group_name":"กองทุน","menu_flag":"0","group_flag":"0","menu_name":"คณะกรรมการกองทุน","menu_id":"1"},{"group_id":"3","group_name":"กองทุน","menu_flag":"0","group_flag":"0","menu_name":"โครงสร้างกองทุน","menu_id":"2"},{"group_id":"3","group_name":"กองทุน","menu_flag":"0","group_flag":"0","menu_name":"รายงานประจำปี","menu_id":"3"},{"group_id":"4","group_name":"ความรู้การลงทุน","menu_flag":"0","group_flag":"0","menu_name":"เอกสารดาวน์โหลด","menu_id":"1"},{"group_id":"4","group_name":"ความรู้การลงทุน","menu_flag":"0","group_flag":"0","menu_name":"แบบทดสอบ","menu_id":"2"},{"group_id":"5","group_name":"แบบฟอร์ม","menu_flag":"0","group_flag":"0","menu_name":"แบบฟอร์มกรณีพ้นสมาชิกภาพ","menu_id":"1"},{"group_id":"5","group_name":"แบบฟอร์ม","menu_flag":"0","group_flag":"0","menu_name":"แบบฟอร์มกรณีขอคงเงินหรือขอรับเงินเป็นงวด","menu_id":"2"},{"group_id":"5","group_name":"แบบฟอร์ม","menu_flag":"0","group_flag":"0","menu_name":"แบบฟอร์มและใบคำร้องอื่นๆ","menu_id":"3"},{"group_id":"6","group_name":"ถาม-ตอบ","menu_flag":"0","group_flag":"0","menu_name":"ถาม-ตอบ","menu_id":"1"},{"group_id":"7","group_name":"ข่าวประชาสัมพันธ์","menu_flag":"0","group_flag":"0","menu_name":"ข่าวประชาสัมพันธ์","menu_id":"1"},{"group_id":"7","group_name":"ข่าวประชาสัมพันธ์","menu_flag":"0","group_flag":"0","menu_name":"ข่าวกองทุน","menu_id":"2"},{"group_id":"8","group_name":"ติดต่อ กสช.","menu_flag":"0","group_flag":"0","menu_name":"ที่อยู่","menu_id":"1"},{"group_id":"8","group_name":"ติดต่อ กสช.","menu_flag":"0","group_flag":"0","menu_name":"สอบถามแนะนำบริการ","menu_id":"2"},{"group_id":"20","group_name":"ข้อมูลส่วนตัว","menu_flag":"0","group_flag":"0","menu_name":"แก้ไขข้อมูลส่วนตัว","menu_id":"1"},{"group_id":"20","group_name":"ข้อมูลส่วนตัว","menu_flag":"0","group_flag":"0","menu_name":"ข้อมูลผู้รับผลประโยชน์","menu_id":"2"},{"group_id":"20","group_name":"ข้อมูลส่วนตัว","menu_flag":"0","group_flag":"0","menu_name":"เปลี่ยนรหัสผ่าน","menu_id":"3"},{"group_id":"21","group_name":"ข้อมูลการลงทุน","menu_flag":"0","group_flag":"0","menu_name":"สัดส่วนการลงทุน","menu_id":"1"},{"group_id":"21","group_name":"ข้อมูลการลงทุน","menu_flag":"0","group_flag":"0","menu_name":"รายงานผลประโยชน์","menu_id":"2"},{"group_id":"21","group_name":"ข้อมูลการลงทุน","menu_flag":"0","group_flag":"0","menu_name":"รายงานเปรียบเทียบเงินกองทุนสำรองเลี้ยงชีพ กับเงินบำเหน็จ","menu_id":"3"},{"group_id":"22","group_name":"แผนการลงทุน","menu_flag":"0","group_flag":"0","menu_name":"เปลี่ยนแผนการลงทุน","menu_id":"1"},{"group_id":"22","group_name":"แผนการลงทุน","menu_flag":"0","group_flag":"0","menu_name":"ประวัติการเลือกแผนการลงทุน","menu_id":"2"},{"group_id":"23","group_name":"ข้อมูลอัตราสะสม","menu_flag":"0","group_flag":"0","menu_name":"การเปลี่ยนอัตราสะสม","menu_id":"1"},{"group_id":"23","group_name":"ข้อมูลอัตราสะสม","menu_flag":"0","group_flag":"0","menu_name":"ประวัติการเปลี่ยนอัตราสะสม","menu_id":"2"},{"group_id":"24","group_name":"แบบประเมินความเสี่ยง","menu_flag":"0","group_flag":"0","menu_name":"แบบประเมินความเสี่ยง","menu_id":"1"},{"group_id":"50","group_name":"จัดการผู้ใช้","menu_flag":"0","group_flag":"0","menu_name":"จัดการกลุ่มผู้ใช้","menu_id":"1"},{"group_id":"50","group_name":"จัดการผู้ใช้","menu_flag":"0","group_flag":"0","menu_name":"จัดการผู้ใช้","menu_id":"2"},{"group_id":"51","group_name":"จัดการสมาชิกกองทุน","menu_flag":"0","group_flag":"0","menu_name":"ข้อมูลทั่วไปของสมาชิก","menu_id":"1"},{"group_id":"51","group_name":"จัดการสมาชิกกองทุน","menu_flag":"0","group_flag":"0","menu_name":"ข้อมูลการเลือกแผนการลงทุนของสมาชิก","menu_id":"2"},{"group_id":"51","group_name":"จัดการสมาชิกกองทุน","menu_flag":"0","group_flag":"0","menu_name":"ข้อมูลการลงทุนของสมาชิก","menu_id":"3"},{"group_id":"51","group_name":"จัดการสมาชิกกองทุน","menu_flag":"0","group_flag":"0","menu_name":"ข้อมูลผู้รับผลประโยชน์ของสมาชิก","menu_id":"4"},{"group_id":"51","group_name":"จัดการสมาชิกกองทุน","menu_flag":"0","group_flag":"0","menu_name":"ข้อมูลสัดส่วนผลการลงทุน","menu_id":"5"},{"group_id":"51","group_name":"จัดการสมาชิกกองทุน","menu_flag":"0","group_flag":"0","menu_name":"ข้อมูลอัตราสมทบของสมาชิก","menu_id":"6"},{"group_id":"51","group_name":"จัดการสมาชิกกองทุน","menu_flag":"0","group_flag":"0","menu_name":"ข้อมูลอัตราสะสมของสมาชิก","menu_id":"7"},{"group_id":"52","group_name":"จัดการนโยบายลงทุน","menu_flag":"0","group_flag":"0","menu_name":"จัดการนโยบายลงทุน","menu_id":"1"},{"group_id":"53","group_name":"จัดการข่าวประชาสัมพันธ์","menu_flag":"0","group_flag":"0","menu_name":"สร้างหมวดหมู่ข่าว","menu_id":"1"},{"group_id":"53","group_name":"จัดการข่าวประชาสัมพันธ์","menu_flag":"0","group_flag":"0","menu_name":"สร้างหัวข้อข่าว","menu_id":"2"},{"group_id":"54","group_name":"จัดการถาม-ตอบ","menu_flag":"0","group_flag":"0","menu_name":"สร้างหมวดหมู่คำถาม-คำตอบ","menu_id":"1"},{"group_id":"54","group_name":"จัดการถาม-ตอบ","menu_flag":"0","group_flag":"0","menu_name":"สร้างหัวข้อคำถาม-คำตอบ","menu_id":"2"},{"group_id":"55","group_name":"จัดการช่องทางติดต่อ","menu_flag":"0","group_flag":"0","menu_name":"ข้อมูลที่อยู่","menu_id":"1"},{"group_id":"55","group_name":"จัดการช่องทางติดต่อ","menu_flag":"0","group_flag":"0","menu_name":"ตอบกลับการสอบถามข้อมูล","menu_id":"2"},{"group_id":"56","group_name":"จัดการแบบประเมินความเสี่ยง","menu_flag":"0","group_flag":"0","menu_name":"จัดการแบบประเมินความเสี่ยง","menu_id":"1"},{"group_id":"57","group_name":"จัดการมูลค่าทรัพย์สินสุทธิ","menu_flag":"0","group_flag":"0","menu_name":"จัดการมูลค่าทรัพย์สินสุทธิ","menu_id":"1"},{"group_id":"58","group_name":"รายงาน","menu_flag":"0","group_flag":"0","menu_name":"รายงานข้อมูลสมาชิกที่เปลี่ยนแผนการลงทุน","menu_id":"1"},{"group_id":"58","group_name":"รายงาน","menu_flag":"0","group_flag":"0","menu_name":"รายงานข้อมูลสมาชิกที่มีการเปลี่ยนแปลงอัตราสะสม","menu_id":"2"},{"group_id":"58","group_name":"รายงาน","menu_flag":"0","group_flag":"0","menu_name":"รายงานข้อมูลสมาชิกที่มีการเปลี่ยนแปลงอัตราสมทบ","menu_id":"3"},{"group_id":"58","group_name":"รายงาน","menu_flag":"0","group_flag":"0","menu_name":"รายงานแสดงข้อมูลเงินสะสม เงินสมทบ ประโยชน์เงินสะสม ประโยชน์เงินสมทบ","menu_id":"4"},{"group_id":"58","group_name":"รายงาน","menu_flag":"0","group_flag":"0","menu_name":"รายงานข้อมูลสมาชิกทุกประเภท","menu_id":"5"},{"group_id":"58","group_name":"รายงาน","menu_flag":"0","group_flag":"0","menu_name":"รายงานข้อมูลสมาชิกที่พ้นสภาพสมาชิก","menu_id":"6"},{"group_id":"58","group_name":"รายงาน","menu_flag":"0","group_flag":"0","menu_name":"รายงานข้อมูลสมาชิกที่พ้นสภาพพนักงาน แต่ยังเป็นสมาชิกแบบคงเงินหรือรับเงินเป็นงวด","menu_id":"7"},{"group_id":"58","group_name":"รายงาน","menu_flag":"0","group_flag":"0","menu_name":"รายงานผลการทำแบบประเมินความเสี่ยง","menu_id":"8"},{"group_id":"58","group_name":"รายงาน","menu_flag":"0","group_flag":"0","menu_name":"รายงานแสดงข้อมูลเปรียบเทียบผลประโยชน์และเงินบำเหน็จ","menu_id":"9"},{"group_id":"58","group_name":"รายงาน","menu_flag":"0","group_flag":"0","menu_name":"รายงานจำนวนผู้ติดตั้ง Application บน Mobile","menu_id":"10"},{"group_id":"58","group_name":"รายงาน","menu_flag":"0","group_flag":"0","menu_name":"รายงานสรุปผลการเลือกแผนการลงทุน","menu_id":"11"},{"group_id":"58","group_name":"รายงาน","menu_flag":"0","group_flag":"0","menu_name":"รายงานการเข้าใช้งานระบบ","menu_id":"12"},{"group_id":"58","group_name":"รายงาน","menu_flag":"0","group_flag":"0","menu_name":"รายงานสถิติ Search Keyword","menu_id":"13"},{"group_id":"58","group_name":"รายงาน","menu_flag":"0","group_flag":"0","menu_name":"รายงานตราสารทุน","menu_id":"14"},{"group_id":"58","group_name":"รายงาน","menu_flag":"0","group_flag":"0","menu_name":"รายงานตราสารหนี้","menu_id":"15"},{"group_id":"59","group_name":"ตั้งค่าระบบ","menu_flag":"0","group_flag":"0","menu_name":"ตั้งค่า Organization Link","menu_id":"1"},{"group_id":"59","group_name":"ตั้งค่าระบบ","menu_flag":"0","group_flag":"0","menu_name":"ตั้งค่า Slide Show","menu_id":"2"},{"group_id":"59","group_name":"ตั้งค่าระบบ","menu_flag":"0","group_flag":"0","menu_name":"ตั้งค่าข้อมูลข่าวสารและข้อมูลผู้รับผลประโยชน์","menu_id":"3"},{"group_id":"59","group_name":"ตั้งค่าระบบ","menu_flag":"0","group_flag":"0","menu_name":"ตั้งค่าการเปลี่ยนแผนและเปลี่ยนอัตราสะสม","menu_id":"4"},{"group_id":"60","group_name":"จัดการตราสารทุน","menu_flag":"0","group_flag":"0","menu_name":"ข้อมูลหลักทรัพย์ตราสารทุน","menu_id":"1"},{"group_id":"60","group_name":"จัดการตราสารทุน","menu_flag":"0","group_flag":"0","menu_name":"นำเข้าข้อมูลหลักทรัพย์ตราสารทุน","menu_id":"2"},{"group_id":"61","group_name":"จัดการตราสารหนี้","menu_flag":"0","group_flag":"0","menu_name":"ข้อมูลหลักทรัพย์ตราสารหนี้","menu_id":"1"},{"group_id":"61","group_name":"จัดการตราสารหนี้","menu_flag":"0","group_flag":"0","menu_name":"นำเข้าข้อมูลหลักทรัพย์ตราสารหนี้","menu_id":"2"},{"group_id":"62","group_name":"รายงานตราสารทุน","menu_flag":"0","group_flag":"0","menu_name":"รายงานซื้อ-ขายหลักทรัพย์","menu_id":"1"},{"group_id":"62","group_name":"รายงานตราสารทุน","menu_flag":"0","group_flag":"0","menu_name":"รายงานมูลค่าหน่วยลงทุนและอัตราผลตอบแทน","menu_id":"2"},{"group_id":"62","group_name":"รายงานตราสารทุน","menu_flag":"0","group_flag":"0","menu_name":"รายงานมูลค่าทรัพย์สินสุทธิอัตราผลตอบแทนและการลงทุนในหลักทรัพย์","menu_id":"3"},{"group_id":"62","group_name":"รายงานตราสารทุน","menu_flag":"0","group_flag":"0","menu_name":"รายงานค่านายหน้าซื้อ-ขายหลักทรัพย์","menu_id":"4"},{"group_id":"62","group_name":"รายงานตราสารทุน","menu_flag":"0","group_flag":"0","menu_name":"รายงานความเคลื่อนไหวราคาตราสารทุน","menu_id":"5"},{"group_id":"62","group_name":"รายงานตราสารทุน","menu_flag":"0","group_flag":"0","menu_name":"รายงานผลประโยชน์จากการลงทุน","menu_id":"6"},{"group_id":"62","group_name":"รายงานตราสารทุน","menu_flag":"0","group_flag":"0","menu_name":"รายงานเปรียบเทียบ Stock Universe","menu_id":"7"},{"group_id":"62","group_name":"รายงานตราสารทุน","menu_flag":"0","group_flag":"0","menu_name":"รายงานการคำนวณค่าใช้จ่าย","menu_id":"8"},{"group_id":"63","group_name":"รายงานตราสารหนี้","menu_flag":"0","group_flag":"0","menu_name":"รายงานพอร์ตการลงทุน","menu_id":"1"},{"group_id":"63","group_name":"รายงานตราสารหนี้","menu_flag":"0","group_flag":"0","menu_name":"รายงานการซื้อ-ขาย","menu_id":"2"},{"group_id":"63","group_name":"รายงานตราสารหนี้","menu_flag":"0","group_flag":"0","menu_name":"รายงานผลประโยชน์การลงทุน","menu_id":"3"},{"group_id":"63","group_name":"รายงานตราสารหนี้","menu_flag":"0","group_flag":"0","menu_name":"รายงานการคำนวณรายได้และค่าใช้จ่าย","menu_id":"4"},{"group_id":"63","group_name":"รายงานตราสารหนี้","menu_flag":"0","group_flag":"0","menu_name":"รายงานรายละเอียดทรัพย์สินสุทธิ","menu_id":"5"},{"group_id":"64","group_name":"รายงาน กช.","menu_flag":"0","group_flag":"0","menu_name":"จัดการข้อมูลรายงาน กช.","menu_id":"1"},{"group_id":"65","group_name":"เครื่องคำนวณตราสารหนี้","menu_flag":"0","group_flag":"0","menu_name":"เครื่องคำนวณตราสารหนี้","menu_id":"1"}]}],"redirect_link":"https:\/\/smarttrade.ktam.co.th\/smarttrade\/pvdweb\/","errDesc":"SUCCESS","redirect_flag":"1","errCode":"0","function":"Login"}';
        return json_decode($result);
        $data_string = json_encode($this->data);
        $this->ch = curl_init($this->url);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, $this->method);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($this->ch);
        return json_decode($result);
    }

    public function __destruct()
    {
        curl_close($this->ch);
    }

    public function objToArray($obj, &$arr){

        if(!is_object($obj) && !is_array($obj)){
            $arr = $obj;
            return $arr;
        }

        foreach ($obj as $key => $value)
        {
            if (!empty($value))
            {
                $arr[$key] = array();
                $this->objToArray($value, $arr[$key]);
            }
            else
            {
                $arr[$key] = $value;
            }
        }
        return $arr;
    }

}

