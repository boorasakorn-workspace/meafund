<?php
/**
 * Phase#2 Equity Company Management 
 * Created: 2016/11/27 00:06
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\UserGroup;
use Illuminate\Support\Facades\Validator;
use Jenssegers\Date\Date;

use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Input;
use Illuminate\Http\UploadedFile;


use App\User;
use Illuminate\Support\Facades\Log;
use App\Libraries\MEAUtils;


class EquityCompanyManagementController extends Controller
{
    

    public function getindex()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data, 60, 1) . ' | MEA'
        ] );

        return view('backend.pages.p2_equity_company_management');
    }
    
	


    public  function Ajax_Index(Request $request){

        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;

        $Datacount = $this->getCountAll();
        $Data = $this->getData($ArrParam);

        $totals = count($Datacount);

        $htmlPaginate =Paginatre_gen($totals,$PageSize,'page_click_search',$PageNumber);

        $returnHTML = view('backend.pages.ajax.ajax_p2_equity_company_management')->with([
            'htmlPaginate'=> $htmlPaginate,
            'data'=>$Data

        ])->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }

    public  function  getCountAll(){
        return DB::table('TBL_P2_EQUITY_SECURITIES')->orderby("Name_Sht")->get();
    }

    public  function  getData($ArrParam){

        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];

        $query =  "SELECT * FROM TBL_P2_EQUITY_SECURITIES ORDER BY NAME_SHT OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
        return DB::select(DB::raw($query));
    }

    /**
     * Handle request delete single/multiple records.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function delete(Request $request)
    {
        $deleted = false;
        $arrId = explode(',',$request->input('group_id'));

        foreach($arrId as $index => $item){

            if($item != ""){
                $deleted =  DB::table('TBL_P2_EQUITY_SECURITIES')->where('NAME_SHT',"=", $item)->delete();
            }
        }

        if($deleted)  {
            return response()->json(["ret" => "1"]);
        } else {
            return response()->json(["ret" => "0"]);
        }
    }


    /**
     * Get view equity company .
     * 
     * @param  None
     * @return view\backend\pages "p2_add_equity_company_page"
     */
    public function getAdd()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data,60, 1) . ' | MEA'
        ] );


        return view('backend.pages.p2_add_equity_company_page');
    }

    /**
     * Query equity company data by specifiled code as parameter id.
     * 
     * @param  Equity company code  $id
     * @return view backend.pages.p2_edit_equity_company_page
     */
    public function getEdit($id)
    {
        $data = getmemulist();
        $this->pageSetting([
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data, 60, 1) . ' | MEA'
        ]);


        $editdata = DB::table('TBL_P2_EQUITY_SECURITIES')->where("NAME_SHT" ,"=",  $id)->first();
        // $editdata = DB::table('TBL_P2_EQUITY_SECURITIES')->where("NAME_SHT" ,"=",  $id)->get()[0];
     
        //if(isset($id)) abort(404);
        if($editdata) {

            return view('backend.pages.p2_edit_equity_company_page')->with(['editdata'=>$editdata]);
        }
        
        abort(404);
    }


    /**
     * Receive POST command to add new equity company data
     * @param $request parameters list with fields 
     *        plan_start, plan_end, company_code, company_name,
     *        company_addr, company_phone, company_fax 
     */
    public function postAdd(Request $request)
    {
         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');
        

        Log::info('EquityCompanyManagementController::PoseAdd::=>' . $request);
        Log::info(' postAdd: equity_start=>' . toEnglishDate($request["equity_start"])); 

        $ret = false;
        
        $datestart  = new Date(toEnglishDate($request["equity_start"]));
        $datereq = $request["equity_end"];
         
        
        if ($request["company_addr"] == '') {

        }

        
        if ($request["equity_start"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันเริ่มต้น ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        if ($request["equity_end"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันสิ้นสุด ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }
        

/*
        if ($request["equity_end"] == "") {
            //$rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันสิ้นสุด ";
            //$datereq = "9999-12-31 00:00:00.000";
            //return response()->json(array('success' => $ret, 'html'=>$rethtml));
            $dateEnd = date('Y-m-d');
        } else {
            $dateEnd = new Date(toEnglishDate($datereq));
        }
        if ($request["equity_start"] == "") {
            //$rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันเริ่มต้น ";
            //$datereq = "9999-12-31 00:00:00.000";
            //return response()->json(array('success' => $ret, 'html'=>$rethtml));
             $datestart = date('Y-m-d');
        } else {
            $datestart  = new Date(toEnglishDate($request["equity_start"]));
        }
*/
        Log::info(' postAdd: equity_start=>' . toEnglishDate($datereq));   
        $dateEnd = new Date(toEnglishDate($datereq));

        $today   = new Date();
        $data    = array();

        array_push($data,array(
            'NAME_SHT' => $request["name_sht"],
            //Securities_Name
            'SECURITIES_NAME' =>$request["company_name"],
            'ADDRESS' => $request["company_addr"],

            'START_DATE' => $datestart,
            'END_DATE' => $dateEnd,

            'PHONE' => $request["company_phone"],
            'FAX' => $request["company_fax"],
            'CREATE_DATE' => $today,
            'CREATE_BY' => "Admin"
        ));


        $chk = "SELECT COUNT(NAME_SHT) As total FROM TBL_P2_EQUITY_SECURITIES WHERE NAME_SHT = '". $request["name_sht"]. "'";
        $all = DB::select(DB::raw($chk));
        $total =  $all[0]->total;

        $rethtml = "";


        if ($total > 0) {
           $rethtml = "Short code ที่ท่านใส่มีอยู่ในระบบแล้ว";

        } else {
           $insert = DB::table('TBL_P2_EQUITY_SECURITIES')->insert($data);
           $ret = $insert;
        }


        return response()->json(array('success' => $ret, 'html'=>$rethtml));

//        if($insert) {
//            return redirect()->action('UserGroupController@userGroups');
//        } else {
//            return redirect()->action('UserGroupController@getAddUserGroup')->with('submit_errors', 'ไม่สามารถเพิ่มข้อมูลกลุ่มผู้ใช้ได้');
//        }

    }


    /**
     * Receive POST command to update equity company data
     * @param $request parameters list with fields 
     *        plan_start, plan_end, company_code, company_name,
     *        company_addr, company_phone, company_fax 
     */
    public function postEdit(Request $request)
    {
        $ret = false;
        
        $datestart  = new Date(toEnglishDate($request["equity_start"])); 
        $datereq = $request["equity_end"];


        if ($request["equity_start"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันเริ่มต้น ";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        if ($request["equity_end"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันสิ้นสุด ";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }
        //

        Log::info(' postEdit: request =>' . $request);   

        $dateEnd = new Date(toEnglishDate($datereq));
        $today = new Date();

       
        $today = new Date();

        $data = array(

            //'NAME_SHT' => $request["name_sht"],
            'SECURITIES_NAME' =>$request["company_name"],
            'ADDRESS' => $request["company_addr"],

            'START_DATE' => $datestart,
            'END_DATE' => $dateEnd,

            'PHONE' => $request["company_phone"],
            'FAX' => $request["company_fax"],

            'CREATE_BY' => "Admin"
        );

        /**
         * update equity company to database
         */

        $update = DB::table('TBL_P2_EQUITY_SECURITIES')->where('NAME_SHT', "=", $request["name_sht"])->update($data);

        $ret = $update;

        return response()->json(array('success' => $ret, 'html'=>'OK'));

    }

    

    /*  EQUITY CATEGORY:
        ================
        Route::get('category',          'EquityCompanyManagementController@getindexCategory');  // -> VIEW: p2_tab2_equity_category_page.blade.php
        Route::get('addCategory',       'EquityCompanyManagementController@getAddCategory');    // -> VIEW: p2_tab2_add_equity_category_page.blade.php 
        Route::get('editCategory/{id}', 'EquityCompanyManagementController@getEditCategory');   // -> VIEW: p2_tab2_edit_equity_category_page.blade.php 
            
        Route::post('addCategory',    'EquityCompanyManagementController@postAddCategory');
        Route::post('editsCategory',  'EquityCompanyManagementController@postEditCategory');
        Route::post('deleteCategory', 'EquityCompanyManagementController@deleteCategory');
        Route::post('getallCategory', 'EquityCompanyManagementController@Ajax_Index_Category'); // -> VIEW: ajax_p2_equity_category_page.blade.php 
    */
       
    /**
     * TAB#2 : Equity Category  
     */
    public function getindexCategory()
    {
        $data = getmemulist();
        $this->pageSetting([
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data, 60, 1) . ' | MEA'
        ]);

        Log::info('EquitycompanyManagement::getindexCategory() => ' . getMenuName($data, 60, 1));
        // return view('backend.pages.p2_equity_company_management');    
        return view('backend.pages.p2_tab2_equity_category_page');
    }


    public  function Ajax_Index_Category(Request $request) 
    {
        Log::info('EquitycompanyManagement::Ajax_Index_Category' . $request);
 
        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;

        $Datacount = $this->getCountAllCategory();
        $Data = $this->getDataCategory($ArrParam);

        $totals = count($Datacount);

        $htmlPaginate =Paginatre_gen($totals,$PageSize,'page_click_search',$PageNumber);

        $returnHTML = view('backend.pages.ajax.ajax_p2_equity_category')->with([
            'htmlPaginate'=> $htmlPaginate,
            'data'=>$Data

        ])->render();

        Log::info('EquitycompanyManagement::Ajax_Index_Category()' );
        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }


    public  function  getCountAllCategory() 
    {
        /* limit execution timeout */
        //ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        //ini_set('memory_limit', '-1');

        Log::info('EquitycompanyManagement::getCountAllCategory() all records for TBL_P2_EQUITY_CATEGORY');
        //
        // return DB::table('TBL_P2_EQUITY_CATEGORY')->orderby("MARKET")->get();
        //
        $query =  "SELECT * FROM TBL_P2_EQUITY_CATEGORY ORDER BY MARKET, CATE_ID DESC ";
       
        return DB::select(DB::raw($query));
        
    }

    public  function  getDataCategory($ArrParam) 
    {

        Log::info('EquitycompanyManagement::getDataCategory()');
        /* limit execution timeout */
        //ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        //ini_set('memory_limit', '-1');

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];

        $query =  "SELECT * FROM TBL_P2_EQUITY_CATEGORY ORDER BY MARKET, CATE_ID DESC OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
        
        Log::info('EquitycompanyManagement::getDataCategory()- SQLSTR=' .  $query ); 
        return DB::select(DB::raw($query));
    }


    /**
     * Handle request delete category single/multiple records.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function deleteCategory(Request $request)
    {
        $deleted = false;
        $arrId = explode(',', $request->input('group_id'));

        foreach($arrId as $index => $item) {

            if($item != "") {
                $deleted =  DB::table('TBL_P2_EQUITY_CATEGORY')->where('CATE_ID',"=", $item)->delete();
            }
        }

        if($deleted)  {
            return response()->json(["ret" => "1"]);
        } else {
            return response()->json(["ret" => "0"]);
        }
    }


    /**
     * Get view equity category 
     * 
     * @param  None
     * @return view\backend\pages
     */
    public function getAddCategory()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data,60, 1) . ' | MEA'
        ] );


        return view('backend.pages.p2_tab2_add_equity_category_page');
    }

    /**
     * Get view equity category with data by matched with $id
     * 
     * @param  Equity company code  $id
     * @return view backend.pages.p2_edit_equity_company_page
     */
    public function getEditCategory($id)
    {
        $data = getmemulist();
        $this->pageSetting([
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data, 60, 1) . ' | MEA'
        ]);

        $editdata = DB::table('TBL_P2_EQUITY_CATEGORY')->where("CATE_ID" ,"=",  $id)->first();
       
        if($editdata) {

            return view('backend.pages.p2_tab2_edit_equity_category_page')->with(['editdata'=>$editdata]);
        }
        
        // any error occurred redirect to page 404
        abort(404);
    }


    /**
     * Receive POST command to add new equity category
     * @param $request parameters list with fields 
     *        plan_start, plan_end, company_code, company_name,
     *        company_addr, company_phone, company_fax 
     */
    public function postAddCategory(Request $request)
    {
         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');
        
        // Log::info('EquityCompanyManagementController::postAddCategory::=>' . $request);
         
        $ret = false;
        

        if ($request["market"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล ตลาด";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        if ($request["industrial"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล กลุ่มอุตสาหกรรม ";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        if ($request["bu"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล หมวดธุรกิจ";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        
        $data  = array();

        array_push($data, array(
            'MARKET' => $request["market"],
            'INDUSTRIAL' =>$request["industrial"],
            'BU' => $request["bu"],
            'INDUSTRIAL_ENG' => $request["industrial_eng"],
            'BU_ENG'         => $request["bu_eng"],
            'INDUS_SHT' => $request["short_industrial_eng"],
            'BU_SHT' => $request["short_bu_eng"]
        ));


        // $chk = "SELECT COUNT(INDUSTRIAL) As total FROM TBL_P2_EQUITY_CATEGORY WHERE INDUSTRIAL = '". $request["industrial"]. "' ";

        $chk = " SELECT " .
               "     COUNT(INDUSTRIAL) As total " . 
               " FROM ".
               "     TBL_P2_EQUITY_CATEGORY ". 
               " WHERE ". 
               "     INDUSTRIAL = '". $request["industrial"]. "' AND ".
               "     MARKET = '". $request["market"]. "' AND " .
               "     BU = '". $request["bu"] . "' " ;
       
        $all = DB::select(DB::raw($chk));
        $total =  $all[0]->total;

        $rethtml = "";

        if ($total > 0) {
           $rethtml = "หมวดหมู่หลักทรัพย์ ที่ท่านใส่มีอยู่ในระบบแล้ว";

        } else {
           Log::info('INDUSTRIAL: '. $request["industrial"]);  
           $insert = DB::table('TBL_P2_EQUITY_CATEGORY')->insert($data);
           $ret = $insert;
        }


        return response()->json(array('success' => $ret, 'html'=>$rethtml));
    }


    /**
     * Receive POST command to update equity company data
     * @param $request parameters list with fields 
     *        plan_start, plan_end, company_code, company_name,
     *        company_addr, company_phone, company_fax 
     */
    public function postEditCategory(Request $request)
    {
        $ret = false;
        Log::info('postEditCategory(): industrial='. $request["industrial"]);  

        if ($request["market"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันเริ่มต้น ";
            // $datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        if ($request["industrial"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันสิ้นสุด ";
            // $datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        if ($request["bu"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันสิ้นสุด ";
            // $datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        $data = array(
            // 'CATE_ID' => $request["cate_id"],
            'MARKET'         => $request["market"],
            'INDUSTRIAL'     => $request["industrial"],
            'BU'             => $request["bu"],
            'INDUSTRIAL_ENG' => $request["industrial_eng"],
            'BU_ENG'         => $request["bu_eng"],
            'INDUS_SHT'      => $request["short_industrial_eng"],
            'BU_SHT'         => $request["short_bu_eng"]
        );

        /**
         * update equity category to database
         */

        $update = DB::table('TBL_P2_EQUITY_CATEGORY')->where('CATE_ID', "=", $request["cate_id"])->update($data);

        $ret = $update;

        return response()->json(array('success' => $ret, 'html'=>'OK'));

    }

    public function getimportCategory()
    {
        $this->pageSetting( [
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => 'นำเข้าข้อมูล | MEA'
        ]);

        return view('backend.pages.p2_tab2_import_equity_category_page');
        //return view('backend.pages.users_import')->with([
        //    'user_group' => $user_group
        //]);
    }

    public function importCategory(Request $request) 
    {

        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        /* statistics */ 
        $count       = 0;
        $skipedCount = 0;
        $passedCount = 0;
        $total_records = 0;
        $htmlResult = '';
       
        $results = null;

        $file = $request->file('exelimport');

        $request->file('exelimport')->move(storage_path().'/public/import/' , 'import.xlsx');
       
        $inputfile = storage_path('/public/import/import.xlsx');


        /*/// FAKE: START
        return response()->json(array('success' => true, 'html'=>'<br>' . 
                ' Total : ' . $count . ' Record(s), '. 
                ' Passed: ' . $passedCount . ' Record(s), '. 
                ' Failed: ' . ($count - $passedCount) . ' Record(s), '));
        /// FAKE: END
*/
        $retdate = Excel::load($inputfile, function($reader) use(&$count, 
                                                                 &$skipedCount, 
                                                                 &$passedCount, 
                                                                 &$total_records, 
                                                                 &$htmlResult) {
            $results = $reader->get(); 
            $ret = $results->toArray();
            $htmlResult = '';

            /* LAM prepare for progress bar */
            /////////////////////////////////////
            $progress_count = 0;
            $total_records = $results->count();
            Session::put('progress_count',$progress_count);
            Session::put('total_records', $total_records);
            
            foreach($ret as $index => $value) {
                $count++;
                $data = array();
                
                if (($value == null) || ($value["industrial"] == null) || (strlen($value["industrial"]) < 2)) {
                    $skipedCount++;
                    continue;
                }
                
                Log::info(get_class($this) . '::' . __FUNCTION__ . '::' . print_r($value, true));
                /* update progress */
                $progress_count++;
                if($progress_count % 2 == 0)
                    Session::put('progress_count', $progress_count);
                $CATE_ID    = 0;
                $MARKET     = $value["market"]; 
                $INDUSTRIAL = $value["industrial"];
                $BU         = $value["bu"];
                $INDUSTRIAL_ENG = $value["industrial_eng"];
                $BU_ENG     = $value["bu_eng"];

                $INDUS_SHT  = $value["indus_sht"];
                $BU_SHT     = $value["bu_sht"];
                
                $allquery = "SELECT COUNT(CATE_ID) AS total FROM TBL_P2_EQUITY_CATEGORY  WHERE INDUSTRIAL= '". $INDUSTRIAL . "'" . 
                            "       AND MARKET='". $MARKET . "'" .
                            "       AND BU='". $BU . "' ";
                
                $all = DB::select(DB::raw($allquery));
                $total =  $all[0]->total;


                if ($total == 0) {
                    /* creste new record */
                    array_push($data,array(
                        'MARKET' => $MARKET,
                        'INDUSTRIAL' => $INDUSTRIAL,
                        'BU' => $BU,
                        'INDUSTRIAL_ENG' => $INDUSTRIAL_ENG,
                        'BU_ENG' => $BU_ENG,
                        'INDUS_SHT' => $INDUS_SHT,
                        'BU_SHT' => $BU_SHT
                    ));

                    $affected = DB::table('TBL_P2_EQUITY_CATEGORY')->insert($data);
                    if($affected < 1) {
                         if(strlen($htmlResult) < 500) 
                         {
                            $htmlResult .= '<tr>';
                            $htmlResult .= '    <td>' . $CATE_ID    . '</td>';
                            $htmlResult .= '    <td>' . $MARKET     . '</td>';
                            $htmlResult .= '    <td>' . $INDUSTRIAL . '</td>';
                            $htmlResult .= '    <td>' . $BU         . '</td>'; 
                            $htmlResult .= '    <td>' . $INDUSTRIAL_ENG . '</td>';
                            $htmlResult .= '    <td>' . $BU_ENG         . '</td>';
                            $htmlResult .= '    <td>' . $INDUS_SHT      . '</td>';
                            $htmlResult .= '    <td>' . $BU_SHT         . '</td></tr>';
                         }
                    } else {
                        $passedCount++; 
                    }

                } else {

                    /* update, if already exist */
                    $data = array('MARKET' => $MARKET,
                                  'INDUSTRIAL' => $INDUSTRIAL,
                                  'BU' => $BU,
                                  'INDUSTRIAL_ENG' => $INDUSTRIAL_ENG,
                                  'BU_ENG' => $BU_ENG,
                                  'INDUS_SHT' => $INDUS_SHT,
                                  'BU_SHT' => $BU_SHT);

                    $affected = DB::table('TBL_P2_EQUITY_CATEGORY')
                                    ->where('INDUSTRIAL', "=", $INDUSTRIAL)
                                    ->where('MARKET', "=", $MARKET)
                                    ->update($data);
                    
                    if($affected < 1) {
                         if(strlen($htmlResult) < 500) 
                         {
                            $htmlResult .= '<tr>';
                            $htmlResult .= '    <td>' . $CATE_ID    . '</td>';
                            $htmlResult .= '    <td>' . $MARKET     . '</td>';
                            $htmlResult .= '    <td>' . $INDUSTRIAL . '</td>';
                            $htmlResult .= '    <td>' . $BU         . '</td>';
                            $htmlResult .= '    <td>' . $BU_ENG     . '</td>';
                            $htmlResult .= '    <td>' . $INDUS_SHT  . '</td>';
                            $htmlResult .= '    <td>' . $BU_SHT     . '</td></tr>';
                         }
                    } else {
                        $passedCount++; 
                    }
                    // assumed that always success
                    //$passedCount++; 
                }  

            } // foreach

        });  // Excel::load

        $header = 'รายละเอียดข้อมูลที่ ไม่สำเร็จ <BR> '.
                  '<table style=\"width: 100px;\" class=\"table table-bordered\">' .
                  '<tr><td>รหัส หมวดหมู่หลักทรัพย์</td>' . 
                  '    <td>ตลาด</td>'. 
                  '    <td>กลุ่มอุตสาหกรรม</td>' . 
                  '    <td>หมวดธุรกิจ</td></tr>';
                  '    <td>กลุ่มอุตสาหกรรม(ตัวย่อ)</td></tr>';
                  '    <td>หมวดธุรกิจ(ตัวย่อ)</td></tr>';

        $footer = '</table>  ';

        if(strlen($htmlResult) < 1) {
            $header  = '';
            $footer = '';  
            $htmlResult = ' ###### ';
        }

        /* put 100% */ 
        Session::put('progress_count', $total_records);

        return response()->json(array('success' => true, 'html'=>'<br>' . 
                ' Total : ' . $count . ' Record(s), '. 
                ' Passed: ' . $passedCount . ' Record(s), '. 
                ' Failed: ' . ($count - $passedCount) . ' Record(s), '. 
                // ' progress_count:' . Session::get('progress_count') . 
                //  ' Skiped: ' . $skipedCount . ' Record(s)  ' .
                ' <BR>'. $header . $htmlResult . $footer));
        
    } // importdata


    public function dowloadsampleEquityCategory() {
        $file = 'contents/sample/p2_equity_category.xls';
        $headers = array(
            'Content-Type: application/pdf',
        );
        return \Response::download($file, 'p2_equity_category.xls', $headers);
    }
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    // >>>>
    // TAB#3 : Equity Index  
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////// 
    public function getindexEquityIndex()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data, 60, 1) . ' | MEA'
        ] );

        Log::info('EquitycompanyManagement::getindexEquityIndex() => ' . getMenuName($data, 60, 1));

        $symbols =  DB::table('TBL_P2_EQUITY_INDEX')->orderby("SYMBOL")->get(); 
     //   $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')->orderby("MARKET")->get();  
     //   $securitieslist =  DB::table('TBL_P2_EQUITY_SECURITIES')->orderby("NAME_SHT")->get(); 
/*

        $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')->get();  
        $securitieslist = DB::table('TBL_P2_EQUITY_SECURITIES')->get(); 

      

        ///if($categorylist && $securitieslist) {

        return view('backend.pages.p2_tab3_equity_index_page')->with([
              'categorylist'   => $categorylist,
              'securitieslist' => $securitieslist
              ]);
  */      
        return view('backend.pages.p2_tab3_equity_index_page')->with(['symbols' => $symbols]);
    }


    public  function Ajax_Index_EquityIndex(Request $request) 
    {
        Log::info('EquitycompanyManagement::Ajax_Index_EquityIndex' . $request);
        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $symbol = $request->input('symbol');
        $market = $request->input('market');
        $cate_id = $request->input('cate_id');

        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;

        $ArrParam["symbol"] = $symbol;
        $ArrParam["market"] = $market;
        $ArrParam["cate_id"] = $cate_id;


         
        $Datacount = $this->getCountAllEquityIndex( $ArrParam );
        $Data = $this->getDataEquityIndex($ArrParam);

        if($Datacount)
           $totals = count($Datacount);
        else
           $totals = 0;


        $htmlPaginate =Paginatre_gen($totals,$PageSize, 'page_click_search', $PageNumber);
    
        Log::info('view(backend.pages.ajax.ajax_p2_tab3_equity_index)');
        
 
        $returnHTML = view('backend.pages.ajax.ajax_p2_tab3_equity_index')->with([
            'htmlPaginate'=> $htmlPaginate,
            'data'=>$Data 

        ])->render();


        //Log::info('EquitycompanyManagement::Ajax_Index_EquityIndex()' . $returnHTML);
        return response()->json(array('success' => true, 'html'=>$returnHTML));

    }

    public  function Ajax_EquityIndexSearchForm(Request $request) 
    {
         //Log::info('EquitycompanyManagement::ajax_p2_tab3_equity_index_form ::' . $request);

         $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')->orderby("INDUSTRIAL")->get();  

         $symbols =  DB::table('TBL_P2_EQUITY_INDEX')->orderby("SYMBOL")->get(); 
         $sectionList = array( );
         $tmp = array();
         $key = "";

         $child = array();

         $tmp = $key; 
/*
         foreach ($categorylist as $row) {
            
            $tmp = trim($row->INDUSTRIAL);
            

            if($key != $tmp) {
                 $key = $tmp;
                 $child = array();
                 array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'BU'=>trim($row->BU),
                                     'INDUSTRIAL'=>trim($row->INDUSTRIAL)));
                 $sectionList[$key] = $child;
            } else {
                 array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'BU'=>trim($row->BU),
                                     'INDUSTRIAL'=>trim($row->INDUSTRIAL)));
                 $sectionList[$key] = $child;
                  //$child = array();
             }
             Log::info('PRINT_R: key:' . $key. ', VALUE:' . print_r($child, true));
        } 
  */          
         
          
          foreach ($categorylist as $row) {
            if($key != trim($row->INDUSTRIAL)) {
               if($key == "") {
                   $child = array();
               } else {
                  $sectionList[$key] = $child;
                  $child = array();
               }
            } 
            $key = trim($row->INDUSTRIAL);
           
            array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'BU'=>trim($row->BU),
                                     'INDUSTRIAL'=>trim($row->INDUSTRIAL)));
            
         }
          

         $returnHTML = view('backend.pages.ajax.ajax_p2_tab3_equity_index_form')->with([
            
            'categorylist'   => $categorylist,
            'symbols'        => $symbols,
            'sectionList'    => $sectionList

        ])->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML)); 
    }

/*
     public function Ajax_Index_Search(Request $request)
    {
        Log::info('EquitycompanyManagement::Ajax_Index_Search : ' . $request);

        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $emp_id = $request->input('emp_id');
        $depart = $request->input('depart');
        $plan = $request->input('plan');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $check_name = $request->input('check_name');
        $check_depart = $request->input('check_depart');
        $check_plan = $request->input('check_plan');
        $check_date = $request->input('check_date');

        $ArrParam = array();
        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;
        $ArrParam["emp_id"] =$emp_id;
        $ArrParam["depart"] =$depart;
        $ArrParam["plan"] =$plan;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;

        $ArrParam["check_name"] =$check_name;
        $ArrParam["check_depart"] =$check_depart;
        $ArrParam["check_plan"] =$check_plan;
        $ArrParam["check_date"] =$check_date;


        $data = $this->DataSource($ArrParam,true);

        $totals =  $this->DataSourceCount($ArrParam,true);

        $htmlPaginate =Paginatre_gen($totals,$PageSize,'page_click_search',$PageNumber);


        $returnHTML = view('backend.pages.ajax.ajax_report1')->with([
            'htmlPaginate'=> $htmlPaginate,
            'data' => $data,
            'totals' => $totals,
            'PageSize' =>$PageSize,
            'PageNumber' =>$PageNumber

        ])->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML));

    }

*/
    public  function  getCountAllEquityIndex($ArrParam) 
    {
        /* limit execution timeout */
        //ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        //ini_set('memory_limit', '-1');
        
        // $ArrParam["pagesize"] =$PageSize;
        //$ArrParam["PageNumber"] =$PageNumber;

       // $ArrParam["symbol"] = $symbol;
       // $ArrParam["market"] = $market;
       // $ArrParam["cate_id"] = $cate_id;


       /* $where = "  WHERE COMP_NAME <>'' ";
        if(trim($ArrParam["symbol"]) !="") {
              $ar = explode('|', $ArrParam["symbol"]);
              $where .= " AND SYMBOL ='" . $ar[0] . "' ";
        }

        if(trim($ArrParam["market"]) !="") {
              $where .= " AND MARKET ='" . $ArrParam["market"] . "' ";
        }

        if(trim($ArrParam["cate_id"]) !="") {
              $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')->where('CATE_ID', "=", trim($ArrParam["cate_id"]))->first();  
              
              $where .= " AND INDUSTRIAL ='" . $categorylist->INDUSTRIAL . "' " .
                       "  AND BU ='" . $categorylist->BU . "' ";
        }
        */
         $where = "  WHERE COMP_NAME <>'' ";
        if(trim($ArrParam["symbol"]) !="") {
              $ar = explode('|', $ArrParam["symbol"]);
              $where .= " AND SYMBOL ='" . trim($ar[0]) . "' ";
        }

        if(trim($ArrParam["market"]) !="") {
              $where .= " AND MARKET ='" . trim($ArrParam["market"]) . "' ";
        }

        if(trim($ArrParam["cate_id"]) !="") {
              $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')->where('CATE_ID', "=", trim($ArrParam["cate_id"]))->first();  
              
              $where .= " AND INDUSTRIAL ='" . $categorylist->INDUSTRIAL . "' " .
                       "  AND BU ='" . $categorylist->BU . "' ";
        }
       
        //Log::info('EquitycompanyManagement::getCountAllEquityIndex() all records for TBL_P2_EQUITY_INDEX');

        $query =  "SELECT * FROM TBL_P2_EQUITY_INDEX " . $where . " ORDER BY MARKET DESC; ";// OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
        
        Log::info('EquitycompanyManagement::getCountAllEquityIndex()- SQLSTR=' .  $query ); 
        return DB::select(DB::raw($query));

        //return DB::table('TBL_P2_EQUITY_INDEX')->orderby("SYMBOL")->get();
        
    }

    public  function  getDataEquityIndex($ArrParam) 
    {

        /* limit execution timeout */
        //ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        //ini_set('memory_limit', '-1');

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];

        $where = "  WHERE SYMBOL <> '' ";//$where = "  WHERE COMP_NAME <>'' ";
        if(trim($ArrParam["symbol"]) !="") {
              $ar = explode('|', $ArrParam["symbol"]);
              $where .= " AND SYMBOL ='" . trim($ar[0]) . "' ";
        }

        if(trim($ArrParam["market"]) !="") {
              $where .= " AND MARKET ='" . trim($ArrParam["market"]) . "' ";
        }

        if(trim($ArrParam["cate_id"]) !="") {
              $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')->where('CATE_ID', "=", trim($ArrParam["cate_id"]))->first();  
              
              $where .= " AND INDUSTRIAL ='" . $categorylist->INDUSTRIAL . "' " .
                       "  AND BU ='" . $categorylist->BU . "' ";
        }
        
        if($ArrParam["PageNumber"]!="")
           $query =  "SELECT * FROM TBL_P2_EQUITY_INDEX " . $where . " ORDER BY MARKET DESC OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
        else
            $query =  " SELECT  " .
                      "     INDEX_ID, SYMBOL, SYMBOL_RENAME_FLAG, " . 
                      "     OLD_SYMBOL, MODIFY_DATE, " . 
                      "     MODIFY_BY, EFFECTIVE_DATE, COMP_NAME,  ". 
                      "     MARKET, SET_INDEX50, SET_INDEX100, " . 
                      "     SET_HD, INDUSTRIAL, BU, ADDRESS, ZIP_CODE,  " . 
                      "     PHONE, FAX, URL, START_DATE,  " .
                      "     END_DATE, CREATE_DATE, CREATE_BY " .
                      " FROM TBL_P2_EQUITY_INDEX " . $where . " ORDER BY MARKET DESC "; // OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
         

        Log::info('EquitycompanyManagement::getDataEquityIndex()- SQLSTR=' .  $query ); 
        //echo $query;
        return DB::select(DB::raw($query));
    }


    public function getimportEquityIndex() {
        $data = getmemulist();
        $this->pageSetting([
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data, 60, 1) . ' | MEA'
        ]);

        Log::info('EquitycompanyManagement::getimportEquityIndex() => ' . getMenuName($data, 60, 1));
         
        return view('backend.pages.p2_tab3_import_equity_index_page');
   

    }

    /**
     *  Import  นำเข้าข้อมูล หลักทรัพย์ TAB3 
     *  2017-03-29
     */
    public function importEquityIndex(Request $request) 
    {

        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        /* statistics */ 
        $count       = 0;
        $skipedCount = 0;
        $passedCount = 0;
        $total_records = 0;
        $htmlResult = '';
        $status = false;
       
        $results = null;

        $file = $request->file('exelimport');

        $clientOriginalFileName = $request->file('exelimport')->getClientOriginalName();

        $request->file('exelimport')->move(storage_path().'/public/import/' , 'import_tab3.xlsx');
       
        $inputfile = storage_path('/public/import/import_tab3.xlsx');


        /// FAKE: START
        /*return response()->json(array('success' => true, 'html'=>'<br>' . 
                ' Total : ' . $count . ' Record(s), '. 
                ' Passed: ' . $passedCount . ' Record(s), '. 
                ' Failed: ' . ($count - $passedCount) . ' Record(s), '));
        *//// FAKE: END
        $user_data = Session::get('user_data');
        $CREATE_BY = $user_data->emp_id;     // 25 

        $retdate = Excel::load($inputfile, function($reader) use(&$CREATE_BY,
                                                                 &$status,
                                                                 &$clientOriginalFileName,
                                                                 &$count, 
                                                                 &$skipedCount, 
                                                                 &$passedCount, 
                                                                 &$total_records, 
                                                                 &$htmlResult) {
            $results = $reader->get(); 
            $ret = $results->toArray();
            $htmlResult = '';

            /* LAM prepare for progress bar */
            /////////////////////////////////////
            $progress_count = 0;

            $total_records = $results->count();
            $count = $total_records;
            //Session::put('progress_count',$progress_count);
            //Session::put('total_records', $total_records);
            
            foreach($ret as $index => $value) {
                
                $data = array();

                //if (($value == null) || ($value["COMP_NAME"] == null) || (strlen($value["COMP_NAME"]) < 2)) {
                //    $skipedCount++;
                //    $status = false; 
                //    break;
                //}
            
                // INDEX_ID    
                // SYMBOL  
                // SYMBOL_RENAME_FLAG  
                // OLD_SYMBOL  
                // MODIFY_DATE MODIFY_BY   
                // EFFECTIVE_DATE  
                // COMP_NAME   MARKET  SET_INDEX50 SET_INDEX100    SET_HD  INDUSTRIAL  BU  
                // ADDRESS ZIP_CODE    PHONE   FAX URL START_DATE  END_DATE    CREATE_DATE CREATE_BY
                Log::info('REC: ' .  print_r($value, true));
                $INDEX_ID             = 0;
                $SYMBOL               = ($value["symbol"] == null) ? "" : $value["symbol"]; 
                
                if($SYMBOL =='1') {
                    $SYMBOL = "TRUE";
                }
                if($SYMBOL =='0') {
                    $SYMBOL = "FALSE";
                }

                $SYMBOL_RENAME_FLAG   = ($value["symbol_rename_flag"] == null) ? "" : $value["symbol_rename_flag"]; 
                $OLD_SYMBOL           = ($value["old_symbol"] == null) ? "" : trim($value["old_symbol"]); 
                $MODIFY_DATE          = date('Y-m-d'); //($value["modify_date"] == null) ? "" : $value["modify_date"]; 
                $MODIFY_BY            = ($value["modify_by"] == null) ? "" : trim($value["modify_by"]); 
                $EFFECTIVE_DATE       = ($value["effective_date"] == null) ? "" : $value["effective_date"]; 
                $COMP_NAME            = ($value["comp_name"] == null) ? "" : trim($value["comp_name"]); 
                $MARKET               = ($value["market"] == null) ? "" : trim($value["market"]);
                $SET_INDEX50          = ($value["set_index50"] == null) ? "" : $value["set_index50"]; 
                $SET_INDEX100         = ($value["set_index100"] == null) ? "" : $value["set_index100"];  
                $SET_HD               = ($value["set_hd"] == null) ? "" : trim($value["set_hd"]); 
                $INDUSTRIAL           = ($value["industrial"] == null) ? "" : trim($value["industrial"]);
                $BU                   = ($value["bu"] == null) ? "" : trim($value["bu"]); 
                $ADDRESS              = ($value["address"] == null) ? "" : trim($value["address"]); 
                $ZIP_CODE             = ($value["zip_code"] == null) ? "" : trim($value["zip_code"]); 
                $FAX                  = ($value["fax"] == null) ? "" : trim($value["fax"]); 
                $URL                  = ($value["url"] == null) ? "" : trim($value["url"]); 
                $PHONE                = ($value["phone"] == null) ? "" : trim($value["phone"]); 
                $START_DATE           = ($value["start_date"] == null) ? "" : trim($value["start_date"]); 
                $END_DATE             = ($value["end_date"] == null) ? "" : $value["end_date"];

                $CREATE_DATE          = date('Y-m-d');


                
                $allquery = "SELECT COUNT(SYMBOL) AS total FROM TBL_P2_EQUITY_INDEX  WHERE SYMBOL= '". $SYMBOL . "'" . 
                            "       AND MARKET='". $MARKET . "'" .
                            "       AND BU='". $BU . "'";
                
                $all = DB::select(DB::raw($allquery));
                $total =  $all[0]->total;
                // Log::info('SQLSTR: ' .  $allquery);

                if ($total == 0) {
                    /* creste new record */
                    $data = array(
                        'SYMBOL'=> $SYMBOL,              
                        'SYMBOL_RENAME_FLAG' => $SYMBOL_RENAME_FLAG,
                        'OLD_SYMBOL'         => $OLD_SYMBOL,         
                        'MODIFY_DATE'        => $MODIFY_DATE,        
                        'MODIFY_BY'          => $MODIFY_BY,          
                        'EFFECTIVE_DATE'     => $EFFECTIVE_DATE,     
                        'COMP_NAME'          => $COMP_NAME,          
                        'MARKET'             => $MARKET,             
                        'SET_INDEX50'        => $SET_INDEX50,        
                        'SET_INDEX100'       => $SET_INDEX100,       
                        'SET_HD'             => $SET_HD,             
                        'INDUSTRIAL'         => $INDUSTRIAL,         
                        'BU'                 => $BU,                 
                        'ADDRESS'            => $ADDRESS,            
                        'ZIP_CODE'           => $ZIP_CODE,           
                        'FAX'                => $FAX,     
                        'PHONE'              => $PHONE,            
                        'URL'                => $URL,                
                        //$START_DATE => null,         
                        //$END_DATE => null,          
                        'CREATE_DATE'        => $CREATE_DATE,
                        'CREATE_BY'          => $CREATE_BY
                    );

                    $affected = DB::table('TBL_P2_EQUITY_INDEX')->insert($data);
                    if($affected < 1) {
                         if(strlen($htmlResult) < 500) 
                         {
                            $htmlResult .= '<tr>';
                            $htmlResult .= '    <td>' . $SYMBOL     . '</td>';
                            $htmlResult .= '    <td>' . $COMP_NAME  . '</td>';
                            $htmlResult .= '    <td>' . $INDUSTRIAL . '</td>';
                            $htmlResult .= '    <td>' . $BU         . '</td>';
                            $htmlResult .= '    <td>' . $ADDRESS    . '</td></tr>';   
                         }
                    } else {
                        $passedCount++; 
                    }

                } else {
                         /* update, if already exist */
                    $data = array(
                        'SYMBOL_RENAME_FLAG' => $SYMBOL_RENAME_FLAG,
                        'OLD_SYMBOL'         => $OLD_SYMBOL,         
                        'MODIFY_DATE'        => $MODIFY_DATE,        
                        'MODIFY_BY'          => $MODIFY_BY,          
                        'EFFECTIVE_DATE'     => $EFFECTIVE_DATE,     
                        'COMP_NAME'          => $COMP_NAME,          
                        'MARKET'             => $MARKET,             
                        'SET_INDEX50'        => $SET_INDEX50,        
                        'SET_INDEX100'       => $SET_INDEX100,       
                        'SET_HD'             => $SET_HD,             
                        'INDUSTRIAL'         => $INDUSTRIAL,         
                        'BU'                 => $BU,                 
                        'ADDRESS'            => $ADDRESS,            
                        'ZIP_CODE'           => $ZIP_CODE,           
                        'FAX'                => $FAX,  
                        'PHONE'              => $PHONE,                
                        'URL'                => $URL,                
                        //$START_DATE => null,         
                        //$END_DATE => null,          
                        'CREATE_DATE'        => $CREATE_DATE,
                        'CREATE_BY'          => $CREATE_BY
                    );

                    $affected = DB::table('TBL_P2_EQUITY_INDEX')
                                    ->where('SYMBOL', "=", $SYMBOL)
                                    ->update($data);
                    
                    if($affected < 1) {
                         if(strlen($htmlResult) < 500) {
                            $htmlResult .= ' <tr>';
                            $htmlResult .= '    <td>' . $SYMBOL     . '</td>';
                            $htmlResult .= '    <td>' . $COMP_NAME  . '</td>';
                            $htmlResult .= '    <td>' . $INDUSTRIAL . '</td>';
                            $htmlResult .= '    <td>' . $BU         . '</td>';
                            $htmlResult .= '    <td>' . $ADDRESS    . '</td> </tr>';   
                         }
                    } else {
                        $passedCount++; 
                    }
                    // assumed that always success
                    //$passedCount++; 
                }  
            } // foreach
        });  // Excel::load

        $header = 'รายละเอียดข้อมูลที่ ไม่สำเร็จ <BR> '.
                  '<table style=\"width: 100px;\" class=\"table table-bordered\">' .
                  '<tr><td>รหัส หมวดหมู่หลักทรัพย์</td>' . 
                  '    <td>ชื่อบริษัทิ</td>'. 
                  '    <td>กลุ่มอุตสาหกรรม</td>' . 
                  '    <td>หมวดธุรกิจ</td>'. 
                  '    <td>ที่อยู่</td></tr>';

        $footer = '</table>  ';

        if(strlen($htmlResult) < 1) {
            $header  = '';
            $footer = '';  
            $htmlResult = ' ###### ';
        }

        $status = (($count - $passedCount) == 0);
        /* put 100% */ 
        // Session::put('progress_count', $total_records);

        return response()->json(array('success' => $status, 'html'=>'<br>' . 
                ' Total : ' . $count . ' Record(s), '. 
                ' Passed: ' . $passedCount . ' Record(s), '. 
                ' Failed: ' . ($count - $passedCount) . ' Record(s), '. 
                ' <BR>'. $header . $htmlResult . $footer));
        
    } // importdata



    /**
     * Handle request delete category single/multiple records.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function deleteEquityIndex(Request $request)
    {

        Log::info('EquitycompanyManagement::deleteEquityIndex()' . $request); 

        $deleted = false;
        $arrId = explode(',', $request->input('group_id'));

        foreach($arrId as $index => $item) {

            if($item != "") {
                $deleted =  DB::table('TBL_P2_EQUITY_INDEX')->where('INDEX_ID',"=", $item)->delete();
            }
        }

        if($deleted)  {
            return response()->json(["ret" => "1"]);
        } else {
            return response()->json(["ret" => "0"]);
        }
    }


    /**
     * Get view equity securities 
     *  TAB#3
     * @param  None
     * @return view\backend\pages
     */
    public function getAddEquityIndex()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data,60, 1) . ' | MEA'
        ] );
        
        /////////////
        $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')->orderby("INDUSTRIAL")->get();  
        $securitieslist =  DB::table('TBL_P2_EQUITY_SECURITIES')->orderby("NAME_SHT")->get(); 
        $sectionList = array( );
        $tmp = array();
        $key = "";

        $child = array();
        foreach ($categorylist as $row) {
            $key = $row->INDUSTRIAL;
            if (array_key_exists($key, $sectionList)) {
                $child = $sectionList[$key];
            } else {
                $child = array();
            }
            array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'BU'=>$row->BU,
                                     'INDUSTRIAL'=>$row->INDUSTRIAL
                                     ));
            $sectionList[$key] = $child;
        }
        

        return view('backend.pages.p2_tab3_add_equity_index_page')->with([
            
            'categorylist'   => $categorylist,
            'securitieslist' => $securitieslist,
            'sectionList'    => $sectionList

        ]); //->render();

        ////////////
        //return view('backend.pages.p2_tab3_add_equity_index_page');
    }

    /**
     * Get view equity category with data by matched with $id
     * 
     * @param  Equity company code  $id
     * @return view backend.pages.p2_edit_equity_company_page
     */
    public function getEditEquityIndex($id)
    {
        Log::info('AAA # EquitycompanyManagement::getEditEquityIndex() -> id=' . $id . '\n'); 
        
        
        $data = getmemulist();
        $this->pageSetting([
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data, 60, 1) . ' | MEA'
        ]);

 /////////////
        $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')->orderby("INDUSTRIAL")->get();  
        $securitieslist =  DB::table('TBL_P2_EQUITY_SECURITIES')->orderby("NAME_SHT")->get(); 
        $sectionList = array( );
        $tmp = array();
        $key = "";

        $child = array();
/*
        foreach ($categorylist as $row) {
            if($key != $row->INDUSTRIAL) {
               if($key == "") {
                   $child = array();
               } else {
                  $sectionList[$key] = $child;
                  $child = array();
               }
            } 
            $key = $row->INDUSTRIAL;
            array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'BU'=>$row->BU,
                                     'INDUSTRIAL'=>$row->INDUSTRIAL));
        }
*/
        $tmp = ""; 
        foreach ($categorylist as $row) {
             $key = $row->INDUSTRIAL;

            /*if($key != $row->INDUSTRIAL) {

               if($key == "") {
                   $child = array();
                   $key = $row->INDUSTRIAL;
                   $sectionList[$key] = $child;
               } else {
                   $key = $row->INDUSTRIAL;
                   if (array_key_exists($key, $sectionList)) {
                       $child = $sectionList[$key]; // = $child;
                   } else {    
                       $child = array();
                   }
               }

            } 
            */
            if (array_key_exists($key, $sectionList)) {
                $child = $sectionList[$key];
            } else {
                $child = array();
            }
            array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'BU'=>$row->BU,
                                     'INDUSTRIAL'=>$row->INDUSTRIAL
                                     ));
            $sectionList[$key] = $child;
            

            // Log::info('PRINT_R :' . print_r($child, true));

        }
 
        $editdata = DB::table('TBL_P2_EQUITY_INDEX')->where("INDEX_ID" ,"=",  $id)->first();
        
        if($editdata) {

            return view('backend.pages.p2_tab3_edit_equity_index_page')->with([
                'editdata'       => $editdata,  //data at edit current record
                'categorylist'   => $categorylist,
                'securitieslist' => $securitieslist,
                'sectionList'    => $sectionList
                ]);
        }
        Log::info('BBBB # EquitycompanyManagement::getEditEquityIndex() -> id=' . $id . '\n'); 
        
        // any error occurred redirect to page 404
        abort(404);
    }

    public function dowloadsampleEquityIndex() {
        $file = 'contents/sample/p2_equity_index.xls';
        $headers = array(
            'Content-Type: application/pdf',
        );
        return \Response::download($file, 'p2_equity_index.xls', $headers);
    }

    public function postAddEquityIndex(Request $request) {
        $status = false;
        $html = "Error";
        $industrial_cate_id  = $request->input('industrial_cate_id');


         
        $record_id  = $request->input('record_id');
        $market     = $request->input('market');      
        $setindex   = $request->input('setindex');   
        $industrial_cate_id = $request->input('industrial_cate_id');                  
        $name_sht   = $request->input('name_sht'); // or symbol ?
        $new_symbol = $request->input('new_symbol');
        $effective_date = $request->input('effective_date');// effective_date,


        $zipcode = $request->input('zipcode');
        $equity_url = $request->input('equity_url');
        $company_name  = $request->input('company_name');
        $company_addr = $request->input('company_addr');

        $company_phone = $request->input('company_phone');
        $company_fax = $request->input('company_fax');

        $equity_start = $request->input('equity_start');
        $equity_end = $request->input('equity_end');

        Log::info(get_class() . ' -> ' . $request);

        

        if((!$setindex) || ($setindex == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ SET Index !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        if((!$market) || ($market == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ ตลาด !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        if((!$name_sht) || ($name_sht == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ ชื่อย่อหลักทรัพย์ !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        if((!$company_name) || ($company_name == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ ชื่อบริษัทิ !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        /*if((!$equity_start) || ($equity_start == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ วันเริ่มต้น !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }
        if((!$equity_end) || ($equity_end == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ วันสิ้นสุด!';
            return response()->json(array('success' => $status, 'html'=>$html));
        }
        */
        if ($request["equity_end"] == "") {
            
            
            $equity_end = '1 ม.ค. 2017';
        }  
        if ($request["equity_start"] == "") {
            
             $equity_start = '2 ม.ค. 2017';
        }  
        /*
        if((!$zipcode) || ($zipcode == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ รหัสไปรษณีย์!';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        if((!$equity_url) || ($equity_url == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ URL !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }*/

        $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')->where('CATE_ID', "=", trim($industrial_cate_id))->first();  
        if(!$categorylist) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ไม่พบ รหัส กลุ่มอุตสาหกรรม และกลุ่มธุรกิจ ที่ท่านป้อน !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        $BU = $categorylist->BU;
        $INDUSTRIAL = $categorylist->INDUSTRIAL;
        $utils = new MEAUtils();

        $equity_start = $utils->toEnglishDate($equity_start);
        $equity_end   = $utils->toEnglishDate($equity_end);

        $ar1 = explode('-',$equity_start);
        $ar2 = explode('-',$equity_end);
        $stdate = $ar1[2] . '-' . $ar1[1] . '-'. $ar1[0];
        $enddate = $ar2[2] . '-' . $ar2[1] . '-'. $ar2[0];


        $setindex100  = '';
        $setindex50   = '';
        switch ($setindex) {
            case 'SET100':
                $setindex100 = 'Y';
                break;

            case 'SET50':
                $setindex50 = 'Y';
                $setindex100 = 'Y';
                break;

            case 'None':
            case 'NONE':
               $setindex100  = '';
               $setindex50   = '';
               break;    
            
            default:
                # code...
                break;
        }
        Log::info(get_class() . ' -> equity_start=' . $equity_start . ', equity_end=' . $equity_end);

        // Retrive logged ID
        $user_data = Session::get('user_data');

        $data = array();

        array_push($data,array(

            'MARKET' => $market,
            
            'INDUSTRIAL' => $INDUSTRIAL,
            'BU' => $BU,
            'SYMBOL' => $name_sht,
            'COMP_NAME' => $company_name,
            'ADDRESS' => $company_addr,
            'ZIP_CODE' => $zipcode, 
            
            'SET_INDEX100' => $setindex100,
            'SET_INDEX50' => $setindex50,

            'PHONE' => $company_phone,
            'FAX' => $company_fax,
            'URL' => $equity_url,

            'START_DATE' => $stdate, //$equity_start,
            'END_DATE' => $enddate, //$equity_end,

            'CREATE_DATE' => date("Y-m-d H:i:s"),
            'CREATE_BY' => $user_data->emp_id,

            'MODIFY_DATE' => date("Y-m-d H:i:s"),
            'MODIFY_BY' => $user_data->emp_id

        ));

        Log::info(get_class() . ' data => ' .  print_r($data, true));
        //$status = false;
        $affected = DB::table('TBL_P2_EQUITY_INDEX')->insert($data);
        $status = ($affected > 0);

        return response()->json(array('success' => $status, 'html'=>$html));
                
    }

     public function postEditEquityIndex(Request $request) {
        $status = false;
        $html = "Error!!!";
        //$industrial_cate_id  = $request->input('record_id');

        $industrial_cate_id  = $request->input('industrial_cate_id');
         
        $record_id  = $request->input('record_id');
        $market     = $request->input('market');      
        $setindex   = $request->input('setindex');   
        $industrial_cate_id = $request->input('industrial_cate_id');                  
        $name_sht   = $request->input('name_sht'); // or symbol ?
        $new_symbol = $request->input('new_symbol');
        $effective_date = $request->input('effective_date');// effective_date,


        $zipcode = $request->input('zipcode');
        $equity_url = $request->input('equity_url');
        $company_name  = $request->input('company_name');
        $company_addr = $request->input('company_addr');

        $company_phone = $request->input('company_phone');
        $company_fax = $request->input('company_fax');

        $equity_start = $request->input('equity_start');
        $equity_end = $request->input('equity_end');

        Log::info(get_class() . ' -> ' . $request);

        $old_symbol = $name_sht;

        if((!$setindex) || ($setindex == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ SET Index !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        if((!$market) || ($market == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ ตลาด !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        if((!$name_sht) || ($name_sht == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ ชื่อย่อหลักทรัพย์ !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        if((!$company_name) || ($company_name == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ ชื่อบริษัทิ !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        if ($request["equity_end"] == "") {
            $equity_end = '1 ม.ค. 2017';
        }  
        if ($request["equity_start"] == "") {
             $equity_start = '2 ม.ค. 2017';
        }  

/*
        if((!$equity_start) || ($equity_start == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ วันเริ่มต้น !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }
        if((!$equity_end) || ($equity_end == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ วันสิ้นสุด!';
            return response()->json(array('success' => $status, 'html'=>$html));
        }
*/

        $newflag = (($new_symbol !="") && ($effective_date !=""));
        if($newflag) {
            $old_symbol = $name_sht;

        } else {
            $new_symbol = $name_sht;
        }
        //SYMBOL_RENAME_FLAG
        /*
        if((!$zipcode) || ($zipcode == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ รหัสไปรษณีย์!';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        if((!$equity_url) || ($equity_url == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ URL !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }*/

        $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')->where('CATE_ID', "=", trim($industrial_cate_id))->first();  
        if(!$categorylist) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ไม่พบ รหัส กลุ่มอุตสาหกรรม และกลุ่มธุรกิจ ที่ท่านป้อน !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        $BU = $categorylist->BU;
        $INDUSTRIAL = $categorylist->INDUSTRIAL;
        $utils = new MEAUtils();

        $equity_start = $utils->toEnglishDate($equity_start);
        $equity_end   = $utils->toEnglishDate($equity_end);

        $ar1 = explode('-',$equity_start);
        $ar2 = explode('-',$equity_end);
        $stdate = $ar1[2] . '-' . $ar1[1] . '-'. $ar1[0];
        $enddate = $ar2[2] . '-' . $ar2[1] . '-'. $ar2[0];


        $setindex100  = '';
        $setindex50   = '';
        switch ($setindex) {
            case 'SET100':
                $setindex100 = 'Y';
                break;

            case 'SET50':
                $setindex50 = 'Y';
                $setindex100 = 'Y';
                break;

            case 'None':
            case 'NONE':
               $setindex100  = '';
               $setindex50   = '';
               break;    
            
            default:
                # code...
                break;
        }
        Log::info(get_class() . ' -> equity_start=' . $equity_start . ', equity_end=' . $equity_end);

        // Retrive logged ID
        $user_data = Session::get('user_data');

        $data = array(
            'SYMBOL_RENAME_FLAG' => ($newflag ? "1" : "0"),
            'OLD_SYMBOL'  => $old_symbol,

            
            'MARKET' => $market,
            
            'INDUSTRIAL' => $INDUSTRIAL,
            'BU' => $BU,
            'SYMBOL' => $new_symbol, //$name_sht,
            'COMP_NAME' => $company_name,
            'ADDRESS' => $company_addr,
            'ZIP_CODE' => $zipcode, 
            
            'SET_INDEX100' => $setindex100,
            'SET_INDEX50' => $setindex50,

            'PHONE' => $company_phone,
            'FAX' => $company_fax,
            'URL' => $equity_url,

            'START_DATE' => $stdate, //$equity_start,
            'END_DATE' => $enddate, //$equity_end,

            'MODIFY_DATE' => date("Y-m-d H:i:s"),
            'MODIFY_BY' => $user_data->emp_id

        );

        Log::info(get_class() . ' data => ' .  print_r($data, true) . '\r\n');
        //$status = false;
        $affected = DB::table('TBL_P2_EQUITY_INDEX')->where('INDEX_ID', $record_id)->update($data);
        $status = ($affected > 0);

        return response()->json(array('success' => $status, 'html'=>$html));
                
    }



    //////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * TAB#4 
     */

    public function getindexBroker()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data, 60, 1) . ' | MEA'
        ] );

        return view('backend.pages.p2_tab4_equity_broker_page');
    }
    
    


    public  function Ajax_Index_Broker(Request $request){

        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;

        $Datacount = $this->getCountAllBroker();
        $Data = $this->getDataBroker($ArrParam);

        $totals = count($Datacount);

        $htmlPaginate =Paginatre_gen($totals,$PageSize,'page_click_search',$PageNumber);

        $returnHTML = view('backend.pages.ajax.ajax_p2_tab4_equity_broker')->with([
            'htmlPaginate'=> $htmlPaginate,
            'data'=>$Data

        ])->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }


    public  function  getCountAllBroker(){
        
        Log::info('EquitycompanyManagement::getCountAllBroker()');
        return DB::table('TBL_P2_EQUITY_BROKER')->orderby("NAME_SHT")->get();
    }

    public  function  getDataBroker($ArrParam){

        // limit execution timeout 
        ini_set('max_execution_time', 30000);
        // unlimit memory size 
        ini_set('memory_limit', '-1');

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];

        $query =  "SELECT * FROM TBL_P2_EQUITY_BROKER ORDER BY NAME_SHT OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
        return DB::select(DB::raw($query));
    }

    
    public function deleteBroker(Request $request)
    {
        $deleted = false;
        $arrId = explode(',',$request->input('group_id'));

        foreach($arrId as $index => $item){

            if($item != ""){
                $deleted =  DB::table('TBL_P2_EQUITY_BROKER')->where('NAME_SHT',"=", $item)->delete();
            }
        }

        if($deleted)  {
            return response()->json(["ret" => "1"]);
        } else {
            return response()->json(["ret" => "0"]);
        }
    }


   
    public function getAddBroker()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data,60, 1) . ' | MEA'
        ] );


        return view('backend.pages.p2_tab4_add_equity_broker_page');
    }

    
    public function getEditBroker($id)
    {
        $data = getmemulist();
        $this->pageSetting([
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data, 60, 1) . ' | MEA'
        ]);


        $editdata = DB::table('TBL_P2_EQUITY_BROKER')->where("NAME_SHT" ,"=",  $id)->first();
        // $editdata = DB::table('TBL_P2_EQUITY_SECURITIES')->where("NAME_SHT" ,"=",  $id)->get()[0];
     
        //if(isset($id)) abort(404);
        if($editdata) {

            return view('backend.pages.p2_tab4_edit_equity_broker_page')->with(['editdata'=>$editdata]);
        }
        
        abort(404);
    }


   
    public function postAddBroker(Request $request)
    {
        // limit execution timeout 
        ini_set('max_execution_time', 30000);
        // unlimit memory size 
        ini_set('memory_limit', '-1');
        

        Log::info('EquityCompanyManagementController::PoseAdd::=>' . $request);
        Log::info(' postAdd: equity_start=>' . toEnglishDate($request["equity_start"])); 

        $ret = false;
        $datestart  = new Date(toEnglishDate($request["equity_start"]));
        $datereq = $request["equity_end"];
         
        
        if ($request["company_addr"] == '') {

        }

        
        if ($request["equity_start"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันเริ่มต้น ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        if ($request["equity_end"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันสิ้นสุด ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }
        

        Log::info(' postAdd: equity_start=>' . toEnglishDate($datereq));   
        $dateEnd = new Date(toEnglishDate($datereq));

        $today   = new Date();
        $data    = array();

        array_push($data,array(
            'NAME_SHT' => $request["name_sht"],
            //Securities_Name
            'BROKER_NAME' =>$request["company_name"],
            'ADDRESS' => $request["company_addr"],

            'START_DATE' => $datestart,
            'END_DATE' => $dateEnd,

            'PHONE' => $request["company_phone"],
            'FAX' => $request["company_fax"],
            'CREATE_DATE' => $today,
            'CREATE_BY' => "Admin"
        ));


        $chk = "SELECT COUNT(NAME_SHT) As total FROM TBL_P2_EQUITY_BROKER WHERE NAME_SHT = '". $request["name_sht"]. "'";
        $all = DB::select(DB::raw($chk));
        $total =  $all[0]->total;

        $rethtml = "";


        if ($total > 0) {
           $rethtml = "Broker code name ที่ท่านใส่มีอยู่ในระบบแล้ว";

        } else {
           $insert = DB::table('TBL_P2_EQUITY_BROKER')->insert($data);
           $ret = $insert;
        }
        return response()->json(array('success' => $ret, 'html'=>$rethtml));
    }


    
    public function postEditBroker(Request $request)
    {
        $ret = false;
        $datestart  = new Date(toEnglishDate($request["equity_start"]));
        $datereq = $request["equity_end"];

        if ($request["equity_end"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันสิ้นสุด ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }
        if ($request["equity_start"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันเริ่มต้น ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        Log::info(' postEdit: request =>' . $request);   

        $dateEnd = new Date(toEnglishDate($datereq));
        $today = new Date();

        $data = array(

            'BROKER_NAME' =>$request["company_name"],
            'ADDRESS' => $request["company_addr"],

            'START_DATE' => $datestart,
            'END_DATE' => $dateEnd,

            'PHONE' => $request["company_phone"],
            'FAX' => $request["company_fax"],

            'CREATE_BY' => "Admin"
        );

        $update = DB::table('TBL_P2_EQUITY_BROKER')->where('NAME_SHT', "=", $request["name_sht"])->update($data);

        $ret = $update;

        return response()->json(array('success' => $ret, 'html'=>'OK'));

    }
    


    /**
     * Export to CSV file 
     */
    public function ajax_report_search_export(Request $request) {
       // Log::info(get_class($this) .'::'. __FUNCTION__);
    
         // limit execution timeout 
        ini_set('max_execution_time', 30000);
        // unlimit memory size 
        ini_set('memory_limit', '-1');

        $menu = getmemulist();
        $title = getMenuName($menu, 62, 1);

        //  window.location.href =  "EquityCompany/exportsearch?symbol=" + symbol +"&cate_id=" + cate_id +
        //                            "&market=" + market ;
        //                  

        $symbol = $request->input('symbol');
        $cate_id = $request->input('cate_id');
        $market = $request->input('market');
       
        $ArrParam = array();
        $ArrParam["pagesize"] ="";
        $ArrParam["PageNumber"] ="";

        $ArrParam["symbol"] = $symbol;
        $ArrParam["cate_id"] = $cate_id;
        $ArrParam["market"] = $market;

        //$data = $this->DataSource($ArrParam, false, false);
        $utils = new MEAUtils();
      
        Log::info('Export Parameters:' .  print_r($ArrParam, true));
        /*$all = $this->DataSourceCount($ArrParam, false, false);
        $totals = $all[0]->TOTAL; 
        $count_all = $all[0]->COUNT_ALL;
        $sum_all  =  $all[0]->SUM_ALL;
        $data = $this->DataSource($ArrParam, false, false);
*/
        $Datacount = $this->getCountAllEquityIndex( $ArrParam );
        $data = $this->getDataEquityIndex($ArrParam);

        if($Datacount)
           $totals = count($Datacount);
        else
           $totals = 0;

        $title = "listedcompanies_" . date('Y-m-d');
        $results = array();
        foreach ($data as $item) {
            $results[] = (array)$item;
        }

        Excel::create($title, function ($excel) use ($results, $ArrParam, $title,  $totals) {

            $excel->sheet("listedcompanies", function ($sheet) use ($results, $ArrParam, $title, $totals) {
                // first row styling and writing content
                $sheet->setStyle([
                    'borders' => [
                        'allborders' => [
                            'color' => [
                                'rgb' => 'b3b6b7'
                            ]
                        ]
                    ]
                ]);
                

                $header[]   = null;
                $header[0]  = "INDEX_ID";
                $header[1]  = "SYMBOL";
                $header[2]  = "SYMBOL_RENAME_FLAG";
                $header[3]  = "OLD_SYMBOL";
                $header[4]  = "MODIFY_DATE";
                $header[5]  = "MODIFY_BY";
                $header[6]  = "EFFECTIVE_DATE";
                $header[7]  = "COMP_NAME";
                $header[8]  = "MARKET";
                $header[9]  = "SET_INDEX50";
                $header[10]  = "SET_INDEX100";
                $header[11]  = "SET_HD";
                $header[12]  = "INDUSTRIAL";
                $header[13]  = "BU";
                $header[14]  = "ADDRESS";
                $header[15]  = "ZIP_CODE";
                $header[16]  = "PHONE";
                $header[17]  = "FAX";
                $header[19]  = "URL";
                $header[20]  = "START_DATE";
                $header[21]  = "END_DATE";
                $header[22]  = "CREATE_DATE";
                $header[23]  = "CREATE_BY";
                
                //$sheet->row(1, $header);

                // Set multiple column formats
               /* $sheet->setColumnFormat(array(
                        'A' => '@',
                        'B' => '@',
                        'C' => '@',
                        
                        //'D' => '@',
                        'E' => '#,##0.00',
                        //'F' => '@',
                        'G' => '#,##0.00', // SUM(TOTAL_PURCHASE)
                        //'H' => '@',  
                        'I' => '#,##0.00',
                        'J' => '#,##0.00', // SUM(TOTAL_SALE)
                        //'K' => '#,##0.00',
                        //'L' => '#,##0.00', // SUM(PROFIT_LOSS)
                        //'M' => '#,##0.00'  
                ));
                */
                $row = 2;
                $sheet->fromArray($results);
                /*
                foreach ($results as $item) {
                    $rowitem[]   = null;
                    $rowitem[0]   = $item["INDEX_ID"];
                    $rowitem[1]   = $item["SYMBOL"];
                    $rowitem[2]   = $item["SYMBOL_RENAME_FLAG"];
                    $rowitem[3]   = $item["OLD_SYMBOL"];
                    $rowitem[4]   = $item["MODIFY_DATE"];
                    $rowitem[5]   = $item["MODIFY_BY"];
                    $rowitem[6]   = $item["EFFECTIVE_DATE"];
                    $rowitem[7]   = $item["COMP_NAME"];
                    $rowitem[8]   = $item["MARKET"];
                    $rowitem[9]   = $item["SET_INDEX50"];
                    $rowitem[10]  = $item["SET_INDEX100"];
                    $rowitem[11]  = $item["SET_HD"];
                    $rowitem[12]  = $item["INDUSTRIAL"];
                    $rowitem[13]  = $item["BU"];
                    $rowitem[14]  = $item["ADDRESS"];
                    $rowitem[15]  = $item["ZIP_CODE"];
                    $rowitem[16]  = $item["PHONE"];
                    $rowitem[17]  = $item["FAX"];
                    $rowitem[19]  = $item["URL"];
                    $rowitem[20]  = $item["START_DATE"];
                    $rowitem[21]  = $item["END_DATE"];
                    $rowitem[22]  = $item["CREATE_DATE"];
                    $rowitem[23]  = $item["CREATE_BY"];
                    $sheet->row($row, $rowitem);
                    $row++;
                    if($row < 10)
                       Log::info('ITEM:' .  print_r($item, true));
                    
                }*/
            });
        })->download('xls');
   }

} // class EquityCompanyManagementController
 



