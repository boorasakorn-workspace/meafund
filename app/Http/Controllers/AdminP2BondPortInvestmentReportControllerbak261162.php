<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Jenssegers\Date\Date;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;


use App\Libraries\MEAUtils;
use Illuminate\Support\Facades\Log;

class AdminP2BondPortInvestmentReportController extends Controller
{

    public function matGetYearOrg($date2, $date1) {
        $diff = abs(strtotime($date2) - strtotime($date1));
        $years = floor($diff / (365.25*60*60*24));
        $months = floor(($diff - $years * 365.25*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365.25*60*60*24 - $months*30*60*60*24)/ (60*60*24));
        return  array($years , $months , $days);
    }

    public function matGetYear($date_1 , $date_2 ) {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);
   
        $interval = date_diff($datetime1, $datetime2);
        $ss = $interval->format('%y,%m,%d');

        $days = (int)$interval->format("%r%a");
        if($days < 0) {
            // fixed: minus value
           return explode(",", "0,0,0");
        }
        return explode(",", $ss);
      
    }

    /** 
     * ตรมสารหนี้ (BOND)
     * รายงานพอร์ตการลงทุน(Port Investment Report)
     */
    public function getreport()
    {
        $view_name = 'backend.pages.p2_bond_port_investment_report';
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 63,
            'menu_id' => 1,
            'title' => getMenuName($data,63,1) . '|  MEA FUND'
        ] );

      
        $allquery = "SELECT * FROM TBL_P2_BOND_SECURITIES";
        $equitylist = DB::select(DB::raw($allquery));

       /* $allquery = "SELECT * FROM TBL_P2_EQUITY_CATEGORY ORDER BY INDUSTRIAL";
        $equitycategory = DB::select(DB::raw($allquery));
*/
        $allquery = "SELECT * FROM TBL_P2_BOND_BROKER ORDER BY NAME_SHT ASC;";
        $equitybroker = DB::select(DB::raw($allquery));
/*
        $allquery = "SELECT SYMBOL FROM TBL_P2_BOND_INDEX ORDER BY SYMBOL ASC;";
        $equityindex = DB::select(DB::raw($allquery));
*/
        $allquery = "SELECT SYMBOL FROM TBL_P2_BOND_INDEX WHERE FLAG<>'N/A' ORDER BY SYMBOL ASC;";
        $equityindex = DB::select(DB::raw($allquery));
  
        $years = array();
        for($i= date('Y'); $i>date('Y')-25; $i--) {
           array_push($years, $i);
        }

        $categorylist = DB::table('TBL_P2_BOND_CATEGORY')->orderby("BOND_CATE_THA")->get();  
        $securitieslist =  DB::table('TBL_P2_BOND_SECURITIES')->orderby("NAME_SHT")->get(); 
        $sectionList = array( );
        $tmp = array();
        $key = "";

        $child = array();
        foreach ($categorylist as $row) {
            $key = $row->BOND_CATE_THA;

            if (array_key_exists($key, $sectionList)) {
                $child = $sectionList[$key];
            } else {
                $child = array();
            }

            array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'BOND_TYPE_THA'=>$row->BOND_TYPE_THA,
                                     'BOND_CATE_THA'=>$row->BOND_CATE_THA
                                     ));
            $sectionList[$key] = $child;
        
           /* if($key != $row->BOND_CATE_THA) {
               if($key == "") {
                   $child = array();
               } else {
                  $sectionList[$key] = $child;
                  $child = array();
               }
            } 
            $key = $row->BOND_CATE_THA;
            array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'BOND_TYPE_THA'=>$row->BOND_TYPE_THA,
                                     'BOND_CATE_THA'=>$row->BOND_CATE_THA));*/

        }
        $sectionList[$key] = $child;

        return view($view_name)->with([
            'equitylist'     => $equitylist,
            //'equitycategory' => $equitycategory,
            'equitybroker'   => $equitybroker,
            'years'          => $years,
            'symbols'        => $equityindex,
            'securitieslist' => $securitieslist,
            'sectionList'    => $sectionList
            ]);

    }

    public  function  DataSourceTradingReportCount($ArrParam, $IsCase){
        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];

        $symbol   = $ArrParam["symbol"] ;
        $name_sht = $ArrParam["name_sht"];
        $industrial = $ArrParam["industrial"];
        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

    }

   public  function  DataSourceCount($ArrParam, $IsCase){

        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];


        // Log::info(get_class($this) .'::'. __FUNCTION__);

        $symbol   = $ArrParam["symbol"] ;       //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"];      // eg. UOBAM, KTAM 
        $industrial = $ArrParam["industrial"];  // CATE_ID;
        $cateid = $ArrParam["industrial"];  // CATE_ID;
        $age = $ArrParam["age"];  // age;
        $bu = '';
        $indus = '';

        if(strlen($cateid) > 0) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
                           ->orderby("BOND_CATE_THA")
                           ->get();
            foreach ($categorylist as $row) {
                if($row->CATE_ID == $cateid){
                    $indus = $row->BOND_CATE_THA;
                    $bu    = $row->BOND_TYPE_THA;
                    break;  
                }
                
            }
        }
        
        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        
        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 5) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 5) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        
        // WHERE2
        $where2 = "" ;//
        if(strlen(trim($symbol)) > 0) {  
            $where2 .= " P.SYMBOL = '".$symbol."' "; 
        }

        if(strlen(trim($name_sht)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " P.SECURITIES_NAME = '".$name_sht."' ";
        }

        if(strlen(trim($cateid)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND C.CATE_ID = '".$cateid."' ";
            else
               $where2 .= " C.CATE_ID = '".$cateid."' ";
        }

        Log::info('1) #### AGE ##### =>[' . $age . "] LEN=" . strlen($age));
        if(strlen(trim($age)) > 0) {  
            $prefix = "";
            if(strlen($where2) > 1) {
                 $prefix = " AND ";
            }
            $where2 .= $prefix;
            Log::info('2) #### AGE ##### = ' . $age);
            $agefield = " DATEDIFF(MONTH,  GETDATE(), P.MAT ) as AGE_MONTH ";
            switch(intval(trim($age))) {
                         
                case 0;
                   // คงเหลือน้อยกว่าหรือเท่ากับ 3 เดือน (MAT เทียบกับ today >= 1 day <= 3 month)
                   $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/4)) ";

                   break;

                case 1:
                    // คงเหลือน้อยกว่า หรือเท่ากับ 6 เดือน
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365/4)) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/2)) ";
                    
                   break;
/*
                case 2:
                    // คงเหลือน้อยกว่าหรือเท่ากับ 1 ปี (MAT เทียบกับ today >= 1 day <= 1 year)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;  
*/
                case 2:
                    //  คงเหลือมากกว่า 6 เดือน แต่น้อยกว่า หรือเท่ากับ 1 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365/2)) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;

                case 3:
                    // คงเหลือมากกว่า 1 ปี แต่น้อยกว่า หรือเท่ากับ 3 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > 365) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*3)) ";
                    break;

                case 4:
                    // คงเหลือมากกว่า 3 ปี แต่น้อยกว่า หรือเท่ากับ 5 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*3)) " .
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*5)) ";
                    break;

                case 5:
                    // คงเหลือมากกว่า 5 ปีขึ้นไป
                    $where2  .= "  (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*5)) ";
                    break;

                default:
                   break;
                
            } // switch

        } // if age

        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
        // Log::info(__FUNCTION__ . ' ::#3 WHERE#2 ====>' . $where2 . ' date_start=' . $date_start);
        
       
        if(!$IsCase) {
            $where2 = "";

        }

        
        if(strlen($where2) > 1) {
            $where2 = " AND " . $where2  ; 
        }
         
         // Log::info(__FUNCTION__ . ' :: WHERE#2 ====>' . $where2 );

        // COUNT(SYMBOL) As total 
      
        //$query = " SELECT COUNT(*) as total FROM TBL_P2_BOND_PORTFOLIO  " .
        //          $where2 ;

        $query = " SELECT COUNT(P.SYMBOL) as total " . 
                 "     FROM TBL_P2_BOND_PORTFOLIO P, TBL_P2_BOND_INDEX I, TBL_P2_BOND_CATEGORY C " .
                 " WHERE P.SYMBOL = I.SYMBOL AND I.CATE_ID = C.CATE_ID ".
                 "   ".
                 $where2;

        
        $all = DB::select(DB::raw($query));
        Log::info(__FUNCTION__ . ' :: SQL=' . $query);

        Log::info(__FUNCTION__  .' :: TOAL=' . $all[0]->total );
        return  $all[0]->total;
    }

    public  function  DataSource($ArrParam, $IsCase, $ispageing= true) {
        Log::info(print_r($ArrParam, true));
        
        
         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        
        $where = "";
        if($ispageing){
            $PageSize = $ArrParam['pagesize'];
            $PageNumber = $ArrParam['PageNumber'];
        }

        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        $age = $ArrParam["age"];  // age;
        // NEED TO SPLIT BU + INDUSTRIAL 
        $industrial = $ArrParam["industrial"];  
        $cateid = $ArrParam["industrial"];
        $bu = '';
        $indus = '';

        
        if(strlen($cateid) > 0) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
            ->select('CATE_ID','BOND_CATE_THA','BOND_TYPE_THA')
                           ->orderby("BOND_CATE_THA")->get();
            foreach ($categorylist as $row) {
                if($cateid == $row->CATE_ID){
                    $indus = $row->BOND_CATE_THA;
                    $bu    = $row->BOND_TYPE_THA;
                    break;  
                }
            }
        }
        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        
        $where = "" ;//
     
       // WHERE2
        $where2 = "" ;//
        if(strlen(trim($symbol)) > 0) {  
            $where2 .= " P.SYMBOL = '".$symbol."' ";
        }

        if(strlen(trim($name_sht)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " P.SECURITIES_NAME = '".$name_sht."' ";
        }

        if(strlen(trim($cateid)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND C.CATE_ID = '".$cateid."' ";
            else
               $where2 .= " C.CATE_ID = '".$cateid."' ";
        }


        Log::info('1) #### AGE ##### =>[' . $age . "] LEN=" . strlen($age));
        if(strlen(trim($age)) > 0) {  
            $prefix = "";
            if(strlen($where2) > 1) {
                $prefix = " AND ";
            }
            $where2 .= $prefix;
            Log::info('2) #### AGE ##### = ' . $age);
            $agefield = " DATEDIFF(MONTH,  GETDATE(), P.MAT ) as AGE_MONTH ";
            switch(intval(trim($age))) {
                        
                case 0;
                   // คงเหลือน้อยกว่าหรือเท่ากับ 3 เดือน (MAT เทียบกับ today >= 1 day <= 3 month)
                   $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/4)) ";

                   break;

                case 1:
                    // คงเหลือน้อยกว่า หรือเท่ากับ 6 เดือน
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365/4)) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/2)) ";
                    
                   break;
/*
                case 2:
                    // คงเหลือน้อยกว่าหรือเท่ากับ 1 ปี (MAT เทียบกับ today >= 1 day <= 1 year)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;  
*/
                case 2:
                    //  คงเหลือมากกว่า 6 เดือน แต่น้อยกว่า หรือเท่ากับ 1 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365/2)) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;

                case 3:
                    // คงเหลือมากกว่า 1 ปี แต่น้อยกว่า หรือเท่ากับ 3 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > 365) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*3)) ";
                    break;

                case 4:
                    // คงเหลือมากกว่า 3 ปี แต่น้อยกว่า หรือเท่ากับ 5 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*3)) " .
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*5)) ";
                    break;

                case 5:
                    // คงเหลือมากกว่า 5 ปีขึ้นไป
                    $where2  .= "  (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*5)) ";
                    break;

                default:
                   break;
                
            } // switch
        } // if age


        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
        

        if(strlen($where2) > 1) {
            $where2 = " AND " . $where2  ; 
        }

        //   Log::info('indus='.$indus . ', bu=' .$bu . ', date_start=' . $date_start);
        $query =  " SELECT P.MAT as MATT, P.*, C.BOND_CATE_THA, C.BOND_TYPE_THA, I.FULL_NAME  " .
                  " FROM TBL_P2_BOND_PORTFOLIO P, TBL_P2_BOND_INDEX I, TBL_P2_BOND_CATEGORY C " .
                  " WHERE P.SYMBOL = I.SYMBOL AND I.CATE_ID = C.CATE_ID ".
                  $where2 .
                  "  ORDER BY P.SYMBOL ASC "; 
                //  "  ORDER BY  REFERENCE_DATE ASC " ;
                 
                
        if($ispageing){
            $query .= " OFFSET " . $PageSize . " * (" . $PageNumber . " - 1) ROWS FETCH NEXT " . $PageSize . " ROWS ONLY OPTION (RECOMPILE) ";
        }
        var_dump($query);exit();

        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }

    public  function  DataSourceExport($ArrParam, $IsCase, $ispageing= true) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        
        $where = "";
        if($ispageing){
            $PageSize   = $ArrParam['pagesize'];
            $PageNumber = $ArrParam['PageNumber'];
        }

        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        $age      = $ArrParam["age"];  // age;
         
        $industrial = $ArrParam["industrial"];  
        $cateid = $ArrParam["industrial"];
        $bu = '';
        $indus = '';

        if(strlen($cateid) > 0) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
                           ->orderby("BOND_CATE_THA")
                           ->where('CATE_ID','=', $cateid)->get();
            foreach ($categorylist as $row) {
                $indus = $row->BOND_CATE_THA;
                $bu    = $row->BOND_TYPE_THA;
                break;  
            }
        }

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        
        $where = "" ;
      
        // WHERE2
        $where2 = "" ;//
        if(strlen(trim($symbol)) > 0) {  
            $where2 .= " P.SYMBOL = '".$symbol."' ";
        }

        if(strlen(trim($name_sht)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " P.SECURITIES_NAME = '".$name_sht."' ";
        }

        if(strlen(trim($cateid)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND C.CATE_ID = '".$cateid."' ";
            else
               $where2 .= " C.CATE_ID = '".$cateid."' ";
        }

        Log::info('1) #### AGE ##### =>[' . $age . "] LEN=" . strlen($age));
        if(strlen(trim($age)) > 0) {  
            $prefix = "";
            if(strlen($where2) > 1) {
                $prefix = " AND ";
            }
            $where2 .= $prefix;
            Log::info('2) #### AGE ##### = ' . $age);
            $agefield = " DATEDIFF(MONTH,  GETDATE(), P.MAT ) as AGE_MONTH ";
            switch(intval(trim($age))) {
                         
                case 0;
                   // คงเหลือน้อยกว่าหรือเท่ากับ 3 เดือน (MAT เทียบกับ today >= 1 day <= 3 month)
                   $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/4)) ";

                   break;

                case 1:
                    // คงเหลือน้อยกว่า หรือเท่ากับ 6 เดือน
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365/4)) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/2)) ";
                    
                   break;
/*
                case 2:
                    // คงเหลือน้อยกว่าหรือเท่ากับ 1 ปี (MAT เทียบกับ today >= 1 day <= 1 year)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;  
*/
                case 2:
                    //  คงเหลือมากกว่า 6 เดือน แต่น้อยกว่า หรือเท่ากับ 1 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365/2)) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;

                case 3:
                    // คงเหลือมากกว่า 1 ปี แต่น้อยกว่า หรือเท่ากับ 3 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > 365) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*3)) ";
                    break;

                case 4:
                    // คงเหลือมากกว่า 3 ปี แต่น้อยกว่า หรือเท่ากับ 5 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*3)) " .
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*5)) ";
                    break;

                case 5:
                    // คงเหลือมากกว่า 5 ปีขึ้นไป
                    $where2  .= "  (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*5)) ";
                    break;

                default:
                   break;
                
            } // switch
        } // if age


        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }

        if(strlen($where2) > 1) {
            $where2 = " AND " . $where2  ; 
        }

        $query =  " SELECT P.MAT as MATT, P.*, C.BOND_CATE_THA, C.BOND_TYPE_THA, I.FULL_NAME  " .
                  " FROM TBL_P2_BOND_PORTFOLIO P, TBL_P2_BOND_INDEX I, TBL_P2_BOND_CATEGORY C " .
                  " WHERE P.SYMBOL = I.SYMBOL AND I.CATE_ID = C.CATE_ID ".
                  $where2 . 
                  "  ORDER BY  REFERENCE_DATE ASC " ;
                 
        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }


public  function  DataSourceGrandTotal($ArrParam) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        
        $where = "";
        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        $age      = $ArrParam["age"]; // eg. UOBAM, KTAM
        // NEED TO SPLIT BU + INDUSTRIAL 
        $industrial = $ArrParam["industrial"];  
        $cateid = $ArrParam["industrial"];

        $bu = '';
        $indus = '';

        if(strlen($cateid) > 0) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
                           ->orderby("BOND_CATE_THA")
                           ->where('CATE_ID','=', $cateid)->get();
            foreach ($categorylist as $row) {
                $indus = $row->BOND_CATE_THA;
                $bu    = $row->BOND_TYPE_THA;
                break;  
            }
        }

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        
        $where2 = "" ;//
        if(strlen(trim($symbol)) > 0) {  
            $where2 .= " P.SYMBOL = '".$symbol."' ";
        }

        if(strlen(trim($name_sht)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " P.SECURITIES_NAME = '".$name_sht."' ";
        }

         if(strlen(trim($cateid)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND C.CATE_ID = '".$cateid."' ";
            else
               $where2 .= " C.CATE_ID = '".$cateid."' ";
        }


         Log::info('1) #### AGE ##### =>[' . $age . "] LEN=" . strlen($age));
         if(strlen(trim($age)) > 0) {  
            $prefix = "";
            if(strlen($where2) > 1) {
                $prefix = " AND ";
            }
            $where2 .= $prefix;
            Log::info('2) #### AGE ##### = ' . $age);
            $agefield = " DATEDIFF(MONTH,  GETDATE(), P.MAT ) as AGE_MONTH ";
            switch(intval(trim($age))) {
                      
                case 0;
                   // คงเหลือน้อยกว่าหรือเท่ากับ 3 เดือน (MAT เทียบกับ today >= 1 day <= 3 month)
                   $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/4)) ";

                   break;

                case 1:
                    // คงเหลือน้อยกว่า หรือเท่ากับ 6 เดือน
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365/4)) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/2)) ";
                    
                   break;
/*
                case 2:
                    // คงเหลือน้อยกว่าหรือเท่ากับ 1 ปี (MAT เทียบกับ today >= 1 day <= 1 year)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;  
*/
                case 2:
                    //  คงเหลือมากกว่า 6 เดือน แต่น้อยกว่า หรือเท่ากับ 1 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365/2)) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;

                case 3:
                    // คงเหลือมากกว่า 1 ปี แต่น้อยกว่า หรือเท่ากับ 3 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > 365) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*3)) ";
                    break;

                case 4:
                    // คงเหลือมากกว่า 3 ปี แต่น้อยกว่า หรือเท่ากับ 5 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*3)) " .
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*5)) ";
                    break;

                case 5:
                    // คงเหลือมากกว่า 5 ปีขึ้นไป
                    $where2  .= "  (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*5)) ";
                    break;

                default:
                   break;
                
            } // switch
        } // if age


        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       
        if(strlen($where2) > 1) {
            $where2 = " AND " . $where2  ; 
        }

        //   Log::info('indus='.$indus . ', bu=' .$bu . ', date_start=' . $date_start);

        $query =  " SELECT 
                        SUM(FACE_VALUE)  as SUM_FACE_VALUE, 
                        SUM(TOTAL_COST)  as SUM_TOTAL_COST,
                        SUM(AMORT_VALUE) as SUM_AMORT_VALUE,
                        SUM(DIS_PREM)    as SUM_DIS_PREM,
                        SUM(ACCRUED_INT) as SUM_ACCRUED_INT,
                        SUM(INT_EX)      as SUM_INT_EX,
                        SUM(DIRTY_MKT)   as SUM_DIRTY_MKT,
                        SUM(NAV_PERCENTAGE) as SUM_NAV_PERCENTAGE,
                        SUM(ALLOWANCE) as SUM_ALLOWANCE
                    FROM TBL_P2_BOND_PORTFOLIO P, TBL_P2_BOND_INDEX I, TBL_P2_BOND_CATEGORY C  
                    WHERE P.SYMBOL = I.SYMBOL AND I.CATE_ID = C.CATE_ID ".
                  $where2;

        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }

public  function DataSourceBarChartFake($ArrParam) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];
        $cateid = $ArrParam["industrial"];
        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        
}

public  function DataSourceBarChart($ArrParam) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        
        $where = "";
        
        $symbol   = $ArrParam["symbol"] ;  // APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        $age      = $ArrParam["age"];      // age
        
        // NEED TO SPLIT BU + INDUSTRIAL 
        $industrial = $ArrParam["industrial"];  
        $cateid = $ArrParam["industrial"];
        $bu = '';
        $indus = '';

        if(strlen($cateid) > 0) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
                           ->orderby("BOND_CATE_THA")
                           ->where('CATE_ID','=', $cateid)->get();
            foreach ($categorylist as $row) {
                $indus = $row->BOND_CATE_THA;
                $bu    = $row->BOND_TYPE_THA;
                break;  
            }
        }

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        
          $where2 = "" ;//
        if(strlen(trim($symbol)) > 0) {  
            $where2 .= " P.SYMBOL = '".$symbol."' ";
        }

        if(strlen(trim($name_sht)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " P.SECURITIES_NAME = '".$name_sht."' ";
        }

         if(strlen(trim($cateid)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND I.CATE_ID = '".$cateid."' ";
            else
               $where2 .= " I.CATE_ID = '".$cateid."' ";
        }


         Log::info('1) #### AGE ##### =>[' . $age . "] LEN=" . strlen($age));
         if(strlen(trim($age)) > 0) {  
            $prefix = "";
            if(strlen($where2) > 1) {
                $prefix = " AND ";
            }
            $where2 .= $prefix;
            Log::info('2) #### AGE ##### = ' . $age);
            $agefield = " DATEDIFF(MONTH,  GETDATE(), P.MAT ) as AGE_MONTH ";
            switch(intval(trim($age))) {
                      
                case 0;
                   // คงเหลือน้อยกว่าหรือเท่ากับ 3 เดือน (MAT เทียบกับ today >= 1 day <= 3 month)
                   $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/4)) ";

                   break;

                case 1:
                    // คงเหลือน้อยกว่า หรือเท่ากับ 6 เดือน
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365/4)) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/2)) ";
                    
                   break;
/*
                case 2:
                    // คงเหลือน้อยกว่าหรือเท่ากับ 1 ปี (MAT เทียบกับ today >= 1 day <= 1 year)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;  
*/
                case 2:
                    //  คงเหลือมากกว่า 6 เดือน แต่น้อยกว่า หรือเท่ากับ 1 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365/2)) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;

                case 3:
                    // คงเหลือมากกว่า 1 ปี แต่น้อยกว่า หรือเท่ากับ 3 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > 365) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*3)) ";
                    break;

                case 4:
                    // คงเหลือมากกว่า 3 ปี แต่น้อยกว่า หรือเท่ากับ 5 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*3)) " .
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*5)) ";
                    break;

                case 5:
                    // คงเหลือมากกว่า 5 ปีขึ้นไป
                    $where2  .= "  (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*5)) ";
                    break;

                default:
                   break;
                
            } // switch
        } // if age


        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       
        if(strlen($where2) > 1) {
            $where2 = " AND " . $where2  ; 
        }


        Log::info('indus='.$indus . ', bu=' .$bu . ', date_start=' . $date_start);

        $query = ' SELECT BOND_CATE_THA, SUM(DIRTY_MKT) as SUM_DIRTY_MKT, SUM(PERCENT_NAV) as SUM_PERCENT_MKT  FROM ( ' .
                 '     SELECT ' .
                 '     P.*, C.*, ' .
                 '     (P.DIRTY_MKT / SUM(P.DIRTY_MKT) OVER () *100) AS PERCENT_NAV, ' .  
                 '     SUM(P.DIRTY_MKT) OVER () AS SumTotal ' .
                 ' FROM TBL_P2_BOND_PORTFOLIO P, ' .
                 '     TBL_P2_BOND_INDEX I, ' .
                 '     TBL_P2_BOND_CATEGORY C ' .
                 ' WHERE I.SYMBOL = P.SYMBOL ' .
                 '     AND I.CATE_ID = C.CATE_ID ' .
                 $where2 .
                 ') TBL_SUM_NAV ' .
                 '     GROUP BY BOND_CATE_THA ';    

        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }


    //////////////
    ///
    /// ให้แสดงผล(ตามประเภทตราสารและอายคุงเหลือของตราสาร)
    /// Jan/30/2018
    //
  /*  SELECT BOND_CATE_THA, 
          SUM(DIRTY_MKT) as SUM_DIRTY_MKT,
          SUM(PERCENT_NAV) as SUM_PERCENT_MKT 
          FROM (     
              SELECT  P.*, 
                      C.*, 
                      (P.DIRTY_MKT / SUM(P.DIRTY_MKT) OVER () *100) AS PERCENT_NAV,      
                      SUM(P.DIRTY_MKT) OVER () AS SumTotal  
              FROM TBL_P2_BOND_PORTFOLIO P,      
                  TBL_P2_BOND_INDEX I,      
                  TBL_P2_BOND_CATEGORY C  
              WHERE I.SYMBOL = P.SYMBOL      
                  AND I.CATE_ID = C.CATE_ID  AND      
                  (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0)  AND 
                  (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/4))  AND 
                  REFERENCE_DATE  BETWEEN '2016-11-01' AND '2018-02-10' ) 
              TBL_SUM_NAV      
      GROUP BY BOND_CATE_THA   
*/

    ////////////////////////////////////////////////////
    public function DataSourceBarChartByAge($ArrParam) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

    
        $where = "";
        
        $symbol   = $ArrParam["symbol"] ;  // APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        $age      = $ArrParam["age"];      // age
        
        // NEED TO SPLIT BU + INDUSTRIAL 
        $industrial = $ArrParam["industrial"];  
        $cateid = $ArrParam["industrial"];
        $bu = '';
        $indus = '';

        if(strlen($cateid) > 0) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
                           ->orderby("BOND_CATE_THA")
                           ->where('CATE_ID','=', $cateid)->get();
            foreach ($categorylist as $row) {
                $indus = $row->BOND_CATE_THA;
                $bu    = $row->BOND_TYPE_THA;
                break;  
            }
        }

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        
          $where2 = "" ;//
        if(strlen(trim($symbol)) > 0) {  
            $where2 .= " P.SYMBOL = '".$symbol."' ";
        }

        if(strlen(trim($name_sht)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " P.SECURITIES_NAME = '".$name_sht."' ";
        }

         if(strlen(trim($cateid)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND I.CATE_ID = '".$cateid."' ";
            else
               $where2 .= " I.CATE_ID = '".$cateid."' ";
        }


         Log::info('1) #### AGE ##### =>[' . $age . "] LEN=" . strlen($age));
         if(strlen(trim($age)) > 0) {  
            $prefix = "";
            if(strlen($where2) > 1) {
                $prefix = " AND ";
            }
            $where2 .= $prefix;
            Log::info('2) #### AGE ##### = ' . $age);
            $agefield = " DATEDIFF(MONTH,  GETDATE(), P.MAT ) as AGE_MONTH ";
            switch(intval(trim($age))) {
                
                case 0;
                   // คงเหลือน้อยกว่าหรือเท่ากับ 3 เดือน (MAT เทียบกับ today >= 1 day <= 3 month)
                   $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/4)) ";

                   break;

                case 1:
                    // คงเหลือน้อยกว่า หรือเท่ากับ 6 เดือน
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365/4)) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/2)) ";
                    
                   break;
/*
                case 2:
                    // คงเหลือน้อยกว่าหรือเท่ากับ 1 ปี (MAT เทียบกับ today >= 1 day <= 1 year)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;  
*/
                case 2:
                    //  คงเหลือมากกว่า 6 เดือน แต่น้อยกว่า หรือเท่ากับ 1 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365/2)) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;

                case 3:
                    // คงเหลือมากกว่า 1 ปี แต่น้อยกว่า หรือเท่ากับ 3 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > 365) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*3)) ";
                    break;

                case 4:
                    // คงเหลือมากกว่า 3 ปี แต่น้อยกว่า หรือเท่ากับ 5 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*3)) " .
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*5)) ";
                    break;

                case 5:
                    // คงเหลือมากกว่า 5 ปีขึ้นไป
                    $where2  .= "  (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*5)) ";
                    break;

                default:
                   break;
                
            } // switch

        } // if age


        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       
        if(strlen($where2) > 1) {
            $where2 = " AND " . $where2  ; 
        }


        Log::info('indus='.$indus . ', bu=' .$bu . ', date_start=' . $date_start);

       /* $query = ' SELECT BOND_CATE_THA, SUM(DIRTY_MKT) as SUM_DIRTY_MKT, SUM(PERCENT_NAV) as SUM_PERCENT_MKT  FROM ( ' .
                 '     SELECT ' .
                 '     P.*, C.*, ' .
                 '     (P.DIRTY_MKT / SUM(P.DIRTY_MKT) OVER () *100) AS PERCENT_NAV, ' .  
                 '     SUM(P.DIRTY_MKT) OVER () AS SumTotal ' .
                 ' FROM TBL_P2_BOND_PORTFOLIO P, ' .
                 '     TBL_P2_BOND_INDEX I, ' .
                 '     TBL_P2_BOND_CATEGORY C ' .
                 ' WHERE I.SYMBOL = P.SYMBOL ' .
                 '     AND I.CATE_ID = C.CATE_ID ' .
                 $where2 .
                 ') TBL_SUM_NAV ' .
                 '     GROUP BY BOND_CATE_THA ';   
*/
        $query = 'SELECT YMD, ' .
                 '       SUM(DIRTY_MKT) as SUM_DIRTY_MKT, ' . 
                 '       SUM(PERCENT_NAV) as SUM_PERCENT_MKT ' .         
                 'FROM ( SELECT P.*, '. 
                 '       C.*, ' . 
                 '       (P.DIRTY_MKT / SUM(P.DIRTY_MKT) OVER () *100) AS PERCENT_NAV, ' .     
                 '       SUM(P.DIRTY_MKT) OVER () AS SumTotal,  ' .             
                 '       YMD = ( ' .
                 '          CASE ' . 
                 '             WHEN  ((DATEDIFF(DAY, GETDATE(), P.MAT  ) >= 0)       AND (DATEDIFF(DAY, GETDATE(), P.MAT  ) <= (365/4))) THEN 0 ' .  
                 '             WHEN  ((DATEDIFF(DAY, GETDATE(), P.MAT  ) > (365/4))  AND (DATEDIFF(DAY, GETDATE(), P.MAT  ) <= (365/2))) THEN 1 ' .
         //        '             WHEN  ((DATEDIFF(DAY, GETDATE(), P.MAT  ) >= 0)       AND (DATEDIFF(DAY, GETDATE(), P.MAT  ) <= (365)))   THEN 2 ' .
                 '             WHEN  ((DATEDIFF(DAY, GETDATE(), P.MAT  ) > (365/2))  AND (DATEDIFF(DAY, GETDATE(), P.MAT  ) <= (365)))   THEN 2 ' .
                 '             WHEN  ((DATEDIFF(DAY, GETDATE(), P.MAT  ) > (365))    AND (DATEDIFF(DAY, GETDATE(), P.MAT  ) <= (365*3))) THEN 3 ' .
                 '             WHEN  ((DATEDIFF(DAY, GETDATE(), P.MAT  ) > (365*3))  AND (DATEDIFF(DAY, GETDATE(), P.MAT  ) <= (365*5))) THEN 4 ' .
                 '             WHEN   (DATEDIFF(DAY, GETDATE(), P.MAT  ) > (365*5))                                                      THEN 5 ' .
                 '             ELSE 5' .  
                 '          END '.
                 '          )  ' .
                 '       FROM TBL_P2_BOND_PORTFOLIO P, '.     
                 '            TBL_P2_BOND_INDEX I,     '.   
                 '            TBL_P2_BOND_CATEGORY C   '.  
                 '       WHERE I.SYMBOL = P.SYMBOL ' .      
                 '             AND I.CATE_ID = C.CATE_ID ' . 
                 $where2 . 
                 ') TBL_SUM_NAV ' .  
                 ' GROUP BY YMD ' .
                 ' ORDER BY YMD ASC ';          

        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }
    //////////

    public function ajax_report_search(Request $request)
    {

        Log::info(get_class($this) .'::'. __FUNCTION__);
        $data = getmemulist();
        $title = getMenuName($data,63,1);

        // $ar = $this->retrive_customize_field(63,1);

        $view_name = 'backend.pages.ajax.ajax_p2_bond_port_investment_report';
        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $symbol = $request->input('symbol');
        $name_sht = $request->input('name_sht');
        $age        = $request->input('age');
        $industrial = $request->input('industrial');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $ArrParam = array();

        //{ pagesize: "10", PageNumber: 1, symbol: "", name_sht: "", industrial: "", date_start: "", date_end: "" }
        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;

        $ArrParam["symbol"] =$symbol;
        $ArrParam["name_sht"] =$name_sht;
        $ArrParam["age"] = $age;
        $ArrParam["industrial"] =$industrial;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;


        $data =null;
        $totals= 0;

        $totals = $this->DataSourceCount($ArrParam, true);
        $data = $this->DataSource($ArrParam, true);
        
        $grandtoal = $this->DataSourceGrandTotal($ArrParam);

        // $chartds = $this->DataSourceBarChart($ArrParam);
        $htmlPaginate =Paginatre_gen($totals, $PageSize,'page_click_search', $PageNumber);

        $utils = new MEAUtils();
        Log::info(get_class($this) .'::'. __FUNCTION__. ' $utils =new MEAUtils()' );
        $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
        $thai_date_end =  (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";
        Log::info(get_class($this) .'::'. __FUNCTION__. ' ' .  
            ' converted date: ' . $date_start .' to ' . $thai_date_start . ', ' . $date_end . ' to '. $thai_date_end );
        
        $pretty_date = $utils->prettyPrintThaiDate($thai_date_start, $thai_date_end, "red");
      
/*
        // option
        $opt_array = $ar['result'];
        
        $sub_array =  $opt_array[0];
        // crate column output array 
        $column = array();
        for($i =0; $i< 50; $i++) {
             if (count($opt_array) == 0) {
                $column[$i] = 'x'; 
             } else {
                $column[$i] = 'o'; 
            }
        }

        if (count($sub_array) > 0) {
            $sub = $sub_array['options'];
            $idx  = 0;
            foreach($sub as $x => $item) {
                
                if(strrpos($item, 'option') >= 0) {
                    Log::info($item);
                    $idx = intval(substr($item, 6, 2));
                    $column[$idx] = 'x';
                }
                $idx++;
            }
        }
        
        $prop_hdr = array();
        $ln = 0;
        for($i = 0; $i< 5; $i++ ) {
            if($column[$i] == 'x') {
               $ln++;
            }
        }
        array_push($prop_hdr, array('length' =>$ln));

        $ln = 0;
        for($i = 5; $i< 7; $i++ ) {
            if($column[$i] == 'x') {
               $ln++;
            }
        }
        array_push($prop_hdr, array('length' =>$ln));

        $ln = 0;
        for($i = 7; $i< 11; $i++ ) {
            if($column[$i] == 'x') {
               $ln++;
            }
        }
        array_push($prop_hdr, array('length' =>$ln));

        $ln = 0;
        for($i = 11; $i< 16; $i++ ) {
            if($column[$i] == 'x') {
               $ln++;
            }
        }
        array_push($prop_hdr, array('length' =>$ln));



        //$opt_array = $results->options;
   */
        $returnHTML = view($view_name)->with([
                'htmlPaginate'=> $htmlPaginate,
                'data' => $data,
                'totals' => $totals,

                'grandtoal' => $grandtoal,   
                'PageSize' =>$PageSize,
                'PageNumber' =>$PageNumber,
                'date_start' => $thai_date_start,
                'date_end' => $thai_date_end,
                'pretty_date' => $pretty_date,
                'TableTitle' => $title
                //,
                //'fields' => $column,
                //'fields_poperties' => $prop_hdr

            ])->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }



    public function ajax_chart_search(Request $request) {
        Log::info(get_class($this) .'::'. __FUNCTION__);
         
        $data = getmemulist();
        $title = getMenuName($data,62,1);

        $ar = $this->retrive_customize_field(62, 1);

        $symbol = $request->input('symbol');
        $name_sht = $request->input('name_sht');
        $industrial = $request->input('industrial');
        $cateid     = $request->input('industrial');
        $age        = $request->input('age');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $ArrParam = array();

        //{ pagesize: "10", PageNumber: 1, symbol: "", name_sht: "", industrial: "", date_start: "", date_end: "" }
        $ArrParam["pagesize"] ="";
        $ArrParam["PageNumber"] ="";

        $ArrParam["symbol"] = $symbol;
        $ArrParam["name_sht"] = $name_sht;
        $ArrParam["age"] = $age;
        $ArrParam["industrial"] = $industrial;
        $ArrParam["cateid"] = $cateid;
        $ArrParam["date_start"] = $date_start;
        $ArrParam["date_end"] = $date_end;

        $data  = null;
        $totals = 0;

        // DEMO :
        //$chartds = $this->DataSourceBarChartFake($ArrParam);

        // ORIGINAL:  
        $chartds = $this->DataSourceBarChart($ArrParam);

        $chartdsAge =$this->DataSourceBarChartByAge($ArrParam);
      
        $utils = new MEAUtils();
        $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
        $thai_date_end =  (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";
        
        Log::info(get_class($this) .'::'. __FUNCTION__.
            ' converted date: ' . $date_start .' to ' . $thai_date_start . ', ' . $date_end . ' to '. $thai_date_end );
        $data  = array();
        $data1 = array();
        $data2 = array();
     
        $pretty_date = $utils->prettyPrintThaiDate($thai_date_start, $thai_date_end, "");
        $returnHTML = [
                'data' => $chartds,
                'data_age' => $chartdsAge,
                'date_start' => $thai_date_start,
                'date_end' => $thai_date_end,
                'pretty_date' => $pretty_date,
        ];
                      
        return response()->json(array('success' => true, 'result'=>$returnHTML));
    }


    public function ajax_report_search_export(Request $request) {
       // Log::info(get_class($this) .'::'. __FUNCTION__);
        
      /*  $ArrParam["emp_id"] =Input::get("EmpID");
        $ArrParam["depart"] =Input::get("depart");
        $ArrParam["plan"] =Input::get("plan");
        $ArrParam["date_start"] =Input::get("date_start");
        $ArrParam["date_end"] =Input::get("date_end");

        $ArrParam["check_name"] =Input::get("check_name");
        $ArrParam["check_depart"] =Input::get("check_depart");
        $ArrParam["check_plan"] =Input::get("check_plan");
        $ArrParam["check_date"] =Input::get("check_date");
        // $sql2 = "SELECT TOP  5 * FROM  TBL_MEMBER_BENEFITS WHERe EMP_ID = '".get_userID()."' ORDER BY RECORD_DATE ASC";
*/
       /* $symbol = Input::get('symbol');
        $name_sht = Input::get('name_sht');
        $industrial = Input::get('industrial');
        $cateid = Input::get('industrial');
        $age = Input::get('age');
        $date_start = Input::get('date_start');
        $date_end = Input::get('date_end');*/

        $PageSize   = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $name_sht   = $request->input('name_sht');
        $industrial = $request->input('industrial');
        $cateid     = $request->input('industrial');
        $symbol     = $request->input('symbol');
        $age        = $request->input('age');
        $year       = $request->input('year');

        $date_start = $request->input('date_start');
        $date_end   = $request->input('date_end');
         
        Log::info('1) #DATE TIME :: date_start:'.  $date_start . ', date_end:' . $date_end);
          
        $ArrParam = array();
        $ArrParam["pagesize"] = "";
        $ArrParam["PageNumber"] = "";
        $ArrParam["symbol"] = $symbol;
        $ArrParam["name_sht"] = $name_sht;
        $ArrParam["industrial"] = $industrial;
        $ArrParam["cateid"] = $cateid; 
        $ArrParam["age"] = $age;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;

        $data = $this->DataSourceExport($ArrParam, false, false);
        $results = array();
        foreach ($data as $item) {
//            $item->filed1 = 'some modification';
//            $item->filed2 = 'some modification2';
            $results[] = (array)$item;

           // $results[5] = get_date_notime($item->modify_new);

            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }



        Excel::create('รายงานพอร์ตการลงทุน', function ($excel) use ($results, $ArrParam){

            $excel->sheet('รายงานพอร์ตการลงทุน', function ($sheet) use ($results, $ArrParam){
                // first row styling and writing content
                $sheet->setStyle([
                    'borders' => [
                        'allborders' => [
                            'color' => [
                                'rgb' => 'b3b6b7'
                            ]
                        ]
                    ]
                ]);
                $sheet->mergeCells('A1:S1');
                $sheet->mergeCells('A2:S2');
                $sheet->mergeCells('A3:S3');

                $sheet->mergeCells('Q4:S4');

                $sheet->mergeCells('A4:A5');
                $sheet->mergeCells('B4:B5');
                $sheet->mergeCells('C4:C5');
                $sheet->mergeCells('D4:D5');
                $sheet->mergeCells('E4:E5');

                $sheet->mergeCells('F4:F5');
                $sheet->mergeCells('G4:G5');
                $sheet->mergeCells('H4:H5');

                $sheet->mergeCells('I4:I5');
                $sheet->mergeCells('J4:J5');
                $sheet->mergeCells('K4:K5');
                $sheet->mergeCells('L4:L5');
                $sheet->mergeCells('M4:M5');
                $sheet->mergeCells('N4:N5');
                $sheet->mergeCells('O4:O5');
                $sheet->mergeCells('P4:P5');
                 




                 
                 

                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Cordia New');
                    $row->setFontSize(20);
                    $row->setFontWeight('bold');
                    $row->setAlignment('center');
                    $row->setBackground('#b2babb');
                    // Set all borders (top, right, bottom, left)
                    $row->setBorder('thin', 'thin', 'none', 'thin');

                });

                $sheet->row(2, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'none', 'thin');
                });

                $sheet->row(3, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'thin', 'thin');

                });

                $sheet->row(4, function ($row) { 
                    
                    $row->setBackground('#e6f2ff');
                    $row->setBorder('none', 'thin', 'thin', 'thin');

                });

                 



                $sheet->row(1, array('รายงานพอร์ตการลงทุน'));

                if($ArrParam["date_start"] != "" && $ArrParam["date_end"] != ""){
                    $sheet->row(2, array('ในช่วงวันที่ ' . get_date_notime($ArrParam["date_start"]) . ' ถึง ' . get_date_notime($ArrParam["date_end"])   ));
                }


                $sheet->row(3, array('รายงานข้อมูล ณ วันที่ ' . get_date_notime(date("Y-m-d H:i:s"))));

               


                
              /*
                $sheet->cell('H4', function($cell) {
                    $cell->setValue('ข้อมูลการซื้อ');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                });

                $sheet->cell('L4', function($cell) {
                    $cell->setValue('ข้อมูลการขาย');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                });
                */
                 

                 
                $header[]   = null;
                $header[0]  = "REF. DATE";
                $header[1]  = "SECURITY";
                $header[2]  = "NAME";
                $header[3]  = "Maturity Date";
                $header[4]  = "FACE VALUE/UNIT";
                $header[5]  = "RATE";
                $header[6]  = "TOTAL COST";
                $header[7]  = "Red Yield";
                $header[8]  = "AMORTIZED COST";
                $header[9]  = "DISC/PREM";
                $header[10] = "ACCRUED INT ";
                 
                $header[11] = "INT ENT(EX)";
                $header[12] = "MKT PRICE";
                $header[13] = "DIRTY VALUE";
                $header[14] = "%NAV";
                $header[15] = "ALLOWANCE";
                //$header[16] = " ปี ";
                //$header[17] = " เดือน ";
                //$header[18] = " วัน ";

                $sheet->row(4, $header);


                $sheet->cell('Q4', function($cell) {
                    $cell->setValue('อายุตราสาร');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#e6f2ff');
                });
                $sheet->cell('Q5', function($cell) {
                    $cell->setValue('ปี');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#e6f2ff');
                });
                $sheet->cell('R5', function($cell) {
                    $cell->setValue('เดือน');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#e6f2ff');
                });
                $sheet->cell('S5', function($cell) {
                    $cell->setValue('วัน');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#e6f2ff');
                });


                $LAST_ROW = 5 + 1;
                // Set multiple column formats
                $sheet->setColumnFormat(array(
                        'A' => '@',
                        'E' => '#,##0.00',
                        'F' => '#,##0.00',
                        'G' => '#,##0.00',
                        'H' => '#,##0.00',
                        'I' => '#,##0.00',
                        'J' => '#,##0.00', // SUM(TOTAL_PURCHASE)
                        'K' => '#,##0.00',  
                        'L' => '#,##0.00',
                        'M' => '#,##0.00', // SUM(TOTAL_SALE)
                        'N' => '#,##0.00',
                        'O' => '#,##0.00', // SUM(PROFIT_LOSS)
                        'P' => '#,##0.00'  
                  ));
/*
  " SELECT TRANS_DATE, SYMBOL, SECURITIES_NAME, INDUSTRIAL, BU, ". 
                  "        UNIT_DIV, TOTAL_DIV, UNIT_PURCHASE, PRICE_PURCHASE, TOTAL_PURCHASE, ". 
                  "        P_SUM, UNIT_SALE, PRICE_SALE, TOTAL_SALE, PROFIT_LOSS, REMAIN_UNIT, ".
                  "        SUM(TOTAL_PURCHASE) as J,  " .
                  "        SUM(TOTAL_SALE) as N,  " .
                  "        SUM(PROFIT_LOSS) as O,  " .
*/         
                $rscount = count($results);   
                $idx = 0; 
                $N = 0.0;
                $J = 0.0;
                $O = 0.0;

                $FACE_VALUE = 0.0;    
                $TOTAL_COST = 0.0;    
                $AMORT_VALUE = 0.0;   
                $DIS_PREM = 0.0;      
                $ACCRUED_INT = 0.0;   
                $INT_EX = 0.0;        
                $DIRTY_MKT = 0.0;     
                $NAV_PERCENTAGE = 0.0;
                $ALLOWANCE = 0.0;

                foreach ($results as $item) {
                     $rs[] = null; 
                     $rs[0] = $item['REFERENCE_DATE']; // A
                     $rs[1] = $item['SECURITIES_NAME'];
                     $rs[2] = $item['SYMBOL']; 
                     $rs[4] = $item['MAT'];
                     $rs[5] = $item['FACE_VALUE'];
                     $rs[6] = $item['RATE'];
                     $rs[7] = $item['TOTAL_COST'];
                     $rs[8] = $item['R_YIELD'];
                     $rs[9] = $item['AMORT_VALUE'];
                     $rs[10] = $item['DIS_PREM'];
                     $rs[11] = $item['ACCRUED_INT'];
                     $rs[12] = $item['INT_EX'];
                     $rs[13] = $item['MKTP_YIELD'];
                     $rs[14] = $item['DIRTY_MKT'];
                     $rs[15] = $item['NAV_PERCENTAGE'];
                     $rs[16] = $item['ALLOWANCE'];
                     $rs[17] = $this->matGetYear(date('Y-m-d'),  $item['MAT'])[0];
                     $rs[18] = $this->matGetYear(date('Y-m-d'),  $item['MAT'])[1];
                     $rs[19] = $this->matGetYear(date('Y-m-d'),  $item['MAT'])[2];


                     
                     $FACE_VALUE       += $item['FACE_VALUE'];
                     $TOTAL_COST       += $item['TOTAL_COST'];
                     $AMORT_VALUE      += $item['AMORT_VALUE'];
                     $DIS_PREM         += $item['DIS_PREM'];
                     $ACCRUED_INT      += $item['ACCRUED_INT'];
                     $INT_EX           += $item['INT_EX'];
                     $DIRTY_MKT        += $item['DIRTY_MKT'];
                     $NAV_PERCENTAGE   += $item['NAV_PERCENTAGE'];
                     $ALLOWANCE        += $item['ALLOWANCE'];

                     $sheet->appendRow($rs);
                     $idx++;
                }

                $sheet->cell('D'. strval($idx+$LAST_ROW), function($cell) {
                        $cell->setValue("ยอดรวม");
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                });


                if($rscount > 0) {
                    
                    $sheet->cell('E'. strval($idx+$LAST_ROW), function($cell) use ($FACE_VALUE) {
                        $cell->setValue($FACE_VALUE);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                    });
                    $sheet->cell('G'. strval($idx+$LAST_ROW), function($cell) use ($TOTAL_COST) {
                        $cell->setValue($TOTAL_COST);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                    });
                    $sheet->cell('I'. strval($idx+$LAST_ROW), function($cell) use ($AMORT_VALUE) {
                        $cell->setValue( $AMORT_VALUE);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                    });

                    $sheet->cell('J'. strval($idx+$LAST_ROW), function($cell) use ($DIS_PREM) {
                        $cell->setValue($DIS_PREM);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                    });
                    $sheet->cell('K'. strval($idx+$LAST_ROW), function($cell) use ($ACCRUED_INT) {
                        $cell->setValue($ACCRUED_INT);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                    });

                    $sheet->cell('L'. strval($idx+$LAST_ROW), function($cell) use ($INT_EX) {
                        $cell->setValue($INT_EX);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                    });
                    $sheet->cell('N'. strval($idx+$LAST_ROW), function($cell) use ($DIRTY_MKT) {
                        $cell->setValue($DIRTY_MKT);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                    });
                    $sheet->cell('O'. strval($idx+$LAST_ROW), function($cell) use ($NAV_PERCENTAGE) {
                        $cell->setValue($NAV_PERCENTAGE);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                    });
                    $sheet->cell('P'. strval($idx+$LAST_ROW), function($cell) use ($ALLOWANCE) {
                        $cell->setValue($ALLOWANCE);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                    });
                } 

            });

        })->download('xls');
    }

    public function ajax_store_customize_field(Request $request) {
        $status = false;
        $result = array();
        $store_path = storage_path() .'/public/import';
        try {
            $menu_id = $request->input('menu_id');  //eg. 60
            $submenu_id = $request->input('submenu_id');
            $utils = new MEAUtils();
            $user = $utils->getLoggedInUser(); 
            $json = $request->input('options');
            $options = array('options'=>$json);
            $file = $store_path . '/' . $user. '-' . $menu_id . '-' . $submenu_id. '.json';
            file_put_contents($file, json_encode($options, TRUE));
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Saved:' . $file );
            array_push($results, $file);

        } catch (\Exception $e) {
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Exception:' .$e->getMessage());
            $status = false;
        } 
        return response()->json(array('success' => $status, 'result'=>$result));
    }

    
    public function retrive_customize_field($menu_id, $submenu_id) {
        $status = false;
        $result = array();
        $store_path = storage_path() .'/public/import';
        try {
            //$menu_id = $request->input('menu_id');  // eg. 60
            //$submenu_id = $request->input('submenu_id');
            $utils = new MEAUtils(); 
            $user = $utils->getLoggedInUser(); 
            $file = $store_path . '/' . $user . '-' . $menu_id . '-' . $submenu_id. '.json';
            $json = json_decode(file_get_contents($file), true);
            array_push($result, $json);

            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Retrived:' . $file );
            $status = true;
        } catch (\Exception $e) {
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Exception:' .$e->getMessage());
            $status = false;
        } 
        return array('success' => $status, 'result'=>$result);
    } 

    public function ajax_retrive_customize_field(Request $request) {
        /*$status = false;
        $result = array();
        $store_path = storage_path() .'/public/import';
        try {
            $menu_id = $request->input('menu_id');  // eg. 60
            $submenu_id = $request->input('submenu_id');
            $utils = new MEAUtils();// Session::get('user_data');
            $user = $utils->getLoggedInUser(); 
            $file = $store_path . '/' . $user . '-' . $menu_id . '-' . $submenu_id. '.json';
            $json = json_decode(file_get_contents($file), true);
            array_push($result, $json);

            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Retrived:' . $file );
            $status = true;
        } catch (\Exception $e) {
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Exception:' .$e->getMessage());
            $status = false;
        } 

        return response()->json(array('success' => $status, 'result'=>$result));*/
        $menu_id = $request->input('menu_id');  // eg. 60
        $submenu_id = $request->input('submenu_id');
          
        $r = $this->retrive_customize_field($menu_id, $submenu_id);
        return response()->json($r);
    }

    /* Utils */
   /* public function toThaiDateTime($strDate, $withTime)
    {
        $strYear = date("Y",strtotime($strDate)) + 543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        if ($withTime) {
            $strHour= date("H",strtotime($strDate));
            $strMinute= date("i",strtotime($strDate));
            $strSeconds= date("s",strtotime($strDate));
            
            return $strDay . " " . $strMonthThai. " ". $strYear .",  " . $strHour . ":" . $strMinute;
        } 
        return $strDay . " " . $strMonthThai. " ". $strYear ;
        
    }
    */

    /////////////////////////
    ///
    //
 /*   $opt_array = array('result'=>['option2', 'option1', 'option3']);
$sub_array = $opt_array['result']; 
$o = array();
foreach($sub_array as $x => $i ) {
    $str = substr($i, 6); // option0
    if(strrpos($str,'option') >= 0) {
        $idx = substr($i, 6, 1);
        $nr = array($idx => 'checked');
        array_push($o, $nr);
    }
   
}

print_r($o['0']);
 */

}
?>