<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Jenssegers\Date\Date;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

use App\Libraries\MEAUtils;
use Illuminate\Support\Facades\Log;
 

/**
 * FILE: AdminP2EquityInvestmentReportController.php
 * 2018-10-18 22:00
 */
class AdminP2EquityInvestmentReportController extends Controller
{

    const UNSPECIFIED                      = 0; // 0 - display TABLE current year and PIE last month 
    const SPECIFIED_YEAR_AND_LAST_MONTH    = 1; // 1 - display TABLE selected year and PIE last month
    const CURRENT_YEAR_AND_SPECIFIED_MONTH = 2; // 2 - display TABLE /PIE with selected month 
    const SPECIFIED_YEAR_AND_MONTH         = 3; // 3 - display PIE chart upon selected year and month


    /** 
     * (Equit/Bound Report)
     */
    public function getreport_main()
    {
        $view_name = 'backend.pages.p2_equity_investment_report_main';
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 62,
            'menu_id' => 2,
            'title' => getMenuName($data, 62, 2) . '|  MEA FUND (Investment)'
        ] );

        $allquery = "SELECT * FROM TBL_P2_EQUITY_INVESTMENT_POLICY";
        $equitypolicy = DB::select(DB::raw($allquery));

        $allquery = "SELECT * FROM TBL_P2_EQUITY_SECURITIES";
        $equitylist = DB::select(DB::raw($allquery));

        

        ////////////////////////////////////////////////

        $allquery      = "SELECT MAX(YEAR(REFERENCE_DATE)) as YYYY FROM TBL_P2_BOND_NAV";
        $bondMaxYear   = DB::select(DB::raw($allquery));
        $allquery      = "SELECT MIN(YEAR(REFERENCE_DATE)) as YYYY FROM TBL_P2_BOND_NAV";
        $bondMinYear   = DB::select(DB::raw($allquery));

        $allquery      = "SELECT MAX(YEAR(REFERENCE_DATE)) as YYYY FROM TBL_P2_EQUITY_NAV";
        $equityMaxYear = DB::select(DB::raw($allquery));
        $allquery      = "SELECT MIN(YEAR(REFERENCE_DATE)) as YYYY FROM TBL_P2_EQUITY_NAV";
        $equityMinYear = DB::select(DB::raw($allquery));

        ////////////////////////////////////////////////
        
        $currentYear = date("Y");
        $years = range($currentYear, $currentYear - 20);

        $BminY =  $currentYear;
        $BmaxY =  $currentYear;
        $EminY =  $currentYear;
        $EmaxY =  $currentYear;
        $tmp    = array();

        if($bondMinYear && $bondMaxYear) {
            $BminY = $bondMinYear[0]->YYYY;
            $BmaxY = $bondMaxYear[0]->YYYY;
           //$bondyears = range($bondMinYear[0]->YYYY, $bondMaxYear[0]->YYYY);

        }

        if($equityMinYear && $equityMaxYear) {
            $EminY = $bondMinYear[0]->YYYY;
            $EmaxY = $bondMaxYear[0]->YYYY;
        }

        $tmp = array($EminY, $EmaxY, $BminY, $BmaxY);
        $minYear = min($tmp);
        $maxYear = max($tmp);
        $years = range($minYear, $maxYear);
        
        Log::info(get_class($this) . '::' . __FUNCTION__ );
    

        return view($view_name)->with([
            'equitypolicy'   => $equitypolicy,
            'equitylist'     => $equitylist,
            'years'          => $years
            //'bondyears'      => $bondyears,
            //'equityyears'    => $equityyears
            ]);
    }

    public function ajax_report_search_main2(Request $request)
    {
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $view_name = 'backend.pages.ajax.ajax_p2_equity_investment_report2';
        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $emp_id = $request->input('emp_id');
        $depart = $request->input('depart');
        $plan = $request->input('plan');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $check_name = $request->input('check_name');
        $check_depart = $request->input('check_depart');
        $check_plan = $request->input('check_plan');
        $check_date = $request->input('check_date');

        $ArrParam = array();
        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;
        $ArrParam["emp_id"] =$emp_id;
        $ArrParam["depart"] =$depart;
        $ArrParam["plan"] =$plan;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;

        $ArrParam["check_name"] =$check_name;
        $ArrParam["check_depart"] =$check_depart;
        $ArrParam["check_plan"] =$check_plan;
        $ArrParam["check_date"] =$check_date;

        $data =null;
        $totals= 0;

        $data = $this->DataSource($ArrParam,true);

        $totals = 1;

        $htmlPaginate =Paginatre_gen($totals, $PageSize,'page_click_search',$PageNumber);

        Log::info(get_class($this) .'::'. __FUNCTION__. ' ->Return HTML ($PageSize, $PageNumber)' );
        $returnHTML = view($view_name)->with([
            'htmlPaginate'=> $htmlPaginate,
            'data' => $data,
            'totals' => $totals,
            'PageSize' =>$PageSize,
            'PageNumber' =>$PageNumber

        ])->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }

    public function ajax_report_search_export2() {
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $ArrParam = array();
        $ArrParam["pagesize"] ="";
        $ArrParam["PageNumber"] ="";
        $ArrParam["emp_id"] =Input::get("EmpID");
        $ArrParam["depart"] =Input::get("depart");
        $ArrParam["plan"] =Input::get("plan");
        $ArrParam["date_start"] =Input::get("date_start");
        $ArrParam["date_end"] =Input::get("date_end");

        $ArrParam["check_name"] =Input::get("check_name");
        $ArrParam["check_depart"] =Input::get("check_depart");
        $ArrParam["check_plan"] =Input::get("check_plan");
        $ArrParam["check_date"] =Input::get("check_date");
      
        $data = $this->DataSource($ArrParam,true,false);
        $results = array();
        foreach ($data as $item) {
            $results[] = (array)$item;

            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }



        Excel::create('ExcelExport', function ($excel) use ($results,$ArrParam){
            $excel->sheet('Sheetname', function ($sheet) use ($results,$ArrParam){
                $sheet->mergeCells('A1:J1');
                $sheet->mergeCells('A2:J2');
                $sheet->mergeCells('A3:J3');
                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Comic Sans MS');
                    $row->setFontSize(20);
                    $row->setAlignment('center');
                });

                $sheet->row(2, function ($row) {$row->setAlignment('center');});
                $sheet->row(3, function ($row) {$row->setAlignment('center');});
                $sheet->row(1, array('รายชื่อสมาชิกเปลี่ยนอัตราสะสม'));

                if($ArrParam["check_date"] == "true" && $ArrParam["date_start"] != "" && $ArrParam["date_end"] != ""){
                    $sheet->row(2, array('ในช่วงวันที่ ' . get_date_notime($ArrParam["date_start"]) . ' ถึง ' . get_date_notime($ArrParam["date_end"])   ));
                }

                $sheet->row(3, array('รายงานข้อมูล ณ วันที่ ' . get_date_notime(date("Y-m-d H:i:s"))));

                $header[] = null;
                $header[0] = 'รหัสพนักงาน';
                $header[1] = "ชื่อ-นามสกุล";
                $header[2] = "หน่วยงาน";
                $header[3] = "อัตราสะสม(เดิม)";
                $header[4] = "อัตราสะสม(ใหม่)";
                $header[5] = "วันที่ทำรายการ";
                $header[6] = "วันที่มีผล";
                $header[7] = "ผู้ทำรายการ";
                $header[8] = "สถานะ";
                $header[9] = "วันที่พ้นสภาพ";

                $sheet->row(4, $header);
                foreach ($results as $user) {
                    $sheet->appendRow($user);
                }
            });
        })->download('xls');
    }
 
    public function mainReportDatasource($tbl, $policy, $selected_month, $selected_year) {
        $current_year = date("Y");
        $current_month = date("n");
        Log::info(get_class($this) .'::'. __FUNCTION__);
        
          
        $where = '';
        $query = " SELECT * 
                FROM (
                    SELECT MAX(REFERENCE_DATE) as REFERENCE_DATE , 
                        SUM([UOBAM]) as [UOBAM], 
                        SUM([KTAM]) as [KTAM], 
                        (SUM([UOBAM]) + SUM([KTAM])) AS [CONSOLIDATE] 
                    FROM ( 
                        SELECT   REFERENCE_DATE , ISNULL([UOBAM], 0) as [UOBAM], ISNULL([KTAM], 0) as [KTAM]
                        FROM   [dbo].[TBL_P2_EQUITY_UNIT_VAL] E
                        PIVOT
                        (
                        SUM(NAV_B) 
                        FOR [SECURITIES_NAME] IN ([UOBAM], [KTAM]) 
                        ) AS P
                    ) AS a
                ) AS E 
                INNER JOIN (
                    SELECT MAX(REFERENCE_DATE) as REFERENCE_DATE , 
                        SUM([UOBAM]) as [UOBAM], 
                        SUM([KTAM]) as [KTAM], 
                        (SUM([UOBAM]) + SUM([KTAM])) AS [CONSOLIDATE] 
                    FROM ( 
                        SELECT   REFERENCE_DATE , ISNULL([UOBAM], 0) as [UOBAM], ISNULL([KTAM], 0) as [KTAM]
                        FROM   [dbo].[TBL_P2_BOND_UNIT_VAL] B
                        PIVOT
                        (
                        SUM(NAV_B) 
                        FOR [SECURITIES_NAME] IN ([UOBAM], [KTAM]) 
                        ) AS K
                    ) AS b
                ) AS G ON G.REFERENCE_DATE = E.REFERENCE_DATE ";

                  
        
        
        $query .= $where; 
        Log::info(get_class($this) .'::'. __FUNCTION__  . ' SQLSTR=' . $query);

        $resultset = DB::select(DB::raw($query));
        return $resultset;  
    }

    public function bondChartDatasource($policy, $selected_month, $selected_year) {
        $current_year = date("Y");
        $current_month = date("n");

        $s_month =  $current_month;
        $s_year=  $current_year;
        
        Log::info(get_class($this) .'::'. __FUNCTION__);
        
        $tbl = 'TBL_P2_BOND_NAV';
        $where = '';
       
        $query = " SELECT TOP 1 REF_DATE, GR, DR, DE, OT, RowRank " .
                 " FROM ( " .
                 "     SELECT lp.GOV_BOND_RATIO as GR, " . 
                 "        lp.DEPOSIT_RATIO as DR, " . 
                 "        lp.DEBENTURE_RATIO AS DE, " . 
                 "        lp.OTHER_RATIO AS OT, " . 
                 "        lp.REFERENCE_DATE AS REF_DATE, " .
                 "        ROW_NUMBER() OVER (PARTITION BY YEAR(REFERENCE_DATE)  ".
                 "               ORDER BY lp.REFERENCE_DATE DESC) 'RowRank' " .
                 "     FROM " . $tbl . "  lp " .
                 " WHERE ";      

                  
        if($selected_month && $selected_year && (strlen($selected_month) > 0) && (strlen($selected_year) >= 2))  {   
            $where = "    year(REFERENCE_DATE) = " . $selected_year . " AND " .
                     "    month(REFERENCE_DATE) = " . $selected_month . " "  .
                     " ) sub " . 
                     " WHERE RowRank = 1 " ; 
            
        } else if((strlen($selected_month) > 0) && (strlen($selected_year) < 2))  {   
            // CASE: 2 : current year and specified month     
            $where =  "    year(REFERENCE_DATE) = " . $current_year . " AND " .
                      "    month(REFERENCE_DATE) = " . $selected_month . " "  .
                      " ) sub " . 
                      " WHERE RowRank = 1 " ; 

        } else if((strlen($selected_month) < 1) && (strlen($selected_year) >= 2))  {  
            $where = "    year(REFERENCE_DATE) = " . $selected_year . "  " .
                     " ) sub " . 
                     " WHERE RowRank = 1 " ; 
     
        } else { 
            // ELSE:  not specified year/month, we will use current year      
            $where = "    year(REFERENCE_DATE) = " . $current_year . "  " . 
                     " ) sub " . 
                     " WHERE RowRank = 1 " ;   
        }
        
        $query .= $where; 
        Log::info(get_class($this) .'::'. __FUNCTION__  . ' SQLSTR=' . $query);

        $resultset = DB::select(DB::raw($query));
        return $resultset;  
    }
    
    public function equityChartDatasource($policy, $selected_month, $selected_year) {
        $current_year = date("Y");
        $current_month = date("n");
        Log::info(get_class($this) .'::'. __FUNCTION__);
        
        $tbl = 'TBL_P2_EQUITY_NAV';
        
        $where = '';

        $query = " SELECT TOP 1 REF_DATE, DR, ST, OT, RowRank " .
                 " FROM ( " .
                 "     SELECT lp.DEPOSIT_RATIO as DR, " . 
                 "        lp.STOCK_RATIO as ST, " . 
                 "        lp.OTHER_RATIO AS OT, " . 
                 "        lp.REFERENCE_DATE AS REF_DATE, " .
                 "        ROW_NUMBER() OVER (PARTITION BY YEAR(REFERENCE_DATE) ".
                 "               ORDER BY lp.REFERENCE_DATE DESC) 'RowRank' " .
                 "     FROM " . $tbl . "  lp " .
                 " WHERE " ;

                // "     WHERE YEAR(REFERENCE_DATE) = 2016 AND Month(REFERENCE_DATE) = 6 ".
                // " ) sub " . 
                //" WHERE RowRank = 1 " ;

                  
        if($selected_month && $selected_year && (strlen($selected_month) > 0) && (strlen($selected_year) >= 2))  {   
            $where = "    year(REFERENCE_DATE) = " . $selected_year . " AND " .
                     "    month(REFERENCE_DATE) = " . $selected_month . " "  . 
                     " ) sub " . 
                     " WHERE RowRank = 1 " ; 
            $s_year = $selected_year;
            $s_month = $selected_month;
            
        } else if((strlen($selected_month) > 0) && (strlen($selected_year) < 2))  {   
            // CASE: 2 : current year and specified month     
            $where = "    year(REFERENCE_DATE) = " . $current_year . " AND " .
                     "    month(REFERENCE_DATE) = " . $selected_month . " "  .
                     " ) sub " . 
                     " WHERE RowRank = 1 " ; 
            $s_year = $current_year;
            $s_month = $selected_month;

        } else if((strlen($selected_month) < 1) && (strlen($selected_year) >= 2))  {  
            $where = "    year(REFERENCE_DATE) = " . $selected_year . "  " .
                     " ) sub " . 
                     " WHERE RowRank = 1 " ;           
            $s_year = $selected_year;
            $s_month = 0;

     
        } else { 
            // ELSE:  not specified year/month, we will use current year      
            $where = "    year(REFERENCE_DATE) = " . $current_year . "  " .
                     " ) sub " . 
                     " WHERE RowRank = 1 " ;  
            $s_year = $selected_year;
            //$s_month = 0;
        }
        
        $query .= $where; 
        Log::info(get_class($this) .'::'. __FUNCTION__  . ' SQLSTR=' . $query);

        $resultset = DB::select(DB::raw($query));
        return $resultset;  
    }
    
    public function getTopOne($table, $policy, $name_sht, $date_start, $date_end) {
        Log::info(get_class($this) .'::'. __FUNCTION__ . ' == ENTER ==');
        //SELECT TOP(1) * FROM (
        $where = " WHERE (REFERENCE_DATE BETWEEN '" . $date_start . "' AND '" . $date_end . "') 
                 ";
        if(strlen($policy) > 0) {
            $where .= " AND POLICY_ID = " . $policy;
        }      
        $where2 = "";
        if(strlen($name_sht) > 0) {
            $where2 = " WHERE SECURITIES_NAME = '" . $name_sht . "'";
        }
         
        $query = " SELECT TOP(1) * FROM (
                SELECT   
                    POLICY_ID, 
                    REFERENCE_DATE as REFERENCE_DATE,
                    SUM([NAV_B_UOBAM]) as [NAV_B_UOBAM],
                    SUM([UNIT_UOBAM]) as [UNIT_UOBAM],
                    SUM([NAV_UNIT_UOBAM]) as [NAV_UNIT_UOBAM],
                    SUM([YIELD_MONTH_UOBAM]) as [YIELD_MONTH_UOBAM],
                    SUM([YIELD_CUMULATIVE_UOBAM]) as [YIELD_CUMULATIVE_UOBAM],
                    SUM([BM_MONTH_UOBAM]) as [BM_MONTH_UOBAM],
                    SUM([BM_CUMULATIVE_UOBAM]) as [BM_CUMULATIVE_UOBAM],
            
                    SUM([NAV_B_KTAM]) as [NAV_B_KTAM],
                    SUM([UNIT_KTAM]) as [UNIT_KTAM],
                    SUM([NAV_UNIT_KTAM]) as [NAV_UNIT_KTAM],
                    SUM([YIELD_MONTH_KTAM]) as [YIELD_MONTH_KTAM],
                    SUM([YIELD_CUMULATIVE_KTAM]) as [YIELD_CUMULATIVE_KTAM],
                    SUM([BM_MONTH_KTAM]) as [BM_MONTH_KTAM],
                    SUM([BM_CUMULATIVE_KTAM]) as [BM_CUMULATIVE_KTAM],
                    
                    (SUM([NAV_B_UOBAM]) +  SUM([NAV_B_KTAM])) as NAV_B_CUMULATIVE,
                    (SUM([UNIT_UOBAM]) +  SUM([UNIT_KTAM])) as UNIT_CUMULATIVE,
                    (SUM([NAV_UNIT_UOBAM]) + SUM([NAV_UNIT_KTAM])) as NAV_UNIT_CUMULATIVE,
                    (SUM([YIELD_MONTH_UOBAM]) + SUM([YIELD_MONTH_KTAM])) as [YIELD_MONTH_CUMULATIVE],
                    (SUM([YIELD_CUMULATIVE_UOBAM]) + SUM([YIELD_CUMULATIVE_KTAM])) as [YIELD_CUMULATIVE_CUMULATIVE],
                    (SUM([BM_MONTH_UOBAM])  + SUM([BM_MONTH_KTAM])) as [BM_MONTH_CUMULATIVE],
                    (SUM([BM_CUMULATIVE_UOBAM])  + SUM([BM_CUMULATIVE_KTAM])) as [BM_CUMULATIVE_CUMULATIVE]

                FROM ( 
                    SELECT   
                        REFERENCE_DATE, 
                        POLICY_ID,
                        ISNULL([NAV_B_UOBAM], 0) as [NAV_B_UOBAM],
                        ISNULL([NAV_B_KTAM], 0) as [NAV_B_KTAM],
            
                        ISNULL([UNIT_UOBAM], 0) as [UNIT_UOBAM],
                        ISNULL([UNIT_KTAM], 0) as [UNIT_KTAM],
            
                        ISNULL([NAV_UNIT_UOBAM], 0) as [NAV_UNIT_UOBAM],
                        ISNULL([NAV_UNIT_KTAM], 0) as [NAV_UNIT_KTAM],
            
                        ISNULL([YIELD_MONTH_UOBAM], 0) as [YIELD_MONTH_UOBAM],
                        ISNULL([YIELD_MONTH_KTAM], 0) as [YIELD_MONTH_KTAM],
                        
                        ISNULL([YIELD_CUMULATIVE_UOBAM], 0) as [YIELD_CUMULATIVE_UOBAM],
                        ISNULL([YIELD_CUMULATIVE_KTAM], 0) as [YIELD_CUMULATIVE_KTAM],
                
                        ISNULL([BM_MONTH_UOBAM], 0) as [BM_MONTH_UOBAM],
                        ISNULL([BM_MONTH_KTAM], 0) as [BM_MONTH_KTAM],
                        
                        ISNULL([BM_CUMULATIVE_UOBAM], 0) as [BM_CUMULATIVE_UOBAM],
                        ISNULL([BM_CUMULATIVE_KTAM], 0) as [BM_CUMULATIVE_KTAM]
                    FROM (
                        SELECT 
                            POLICY_ID,
                            REFERENCE_DATE, 
                            NAV_B, 
                            UNIT,
                            NAV_UNIT, 
                            YIELD_MONTH,
                            YIELD_CUMULATIVE,
                            BM_MONTH,
                            BM_CUMULATIVE,
                            'NAV_B_'            + SECURITIES_NAME as NAV_B_SECURITIES_NAME,
                            'UNIT_'             + SECURITIES_NAME as UNIT_SECURITIES_NAME,
                            'NAV_UNIT_'         + SECURITIES_NAME as NAV_UNIT_SECURITIES_NAME,
                            'YIELD_MONTH_'      + SECURITIES_NAME as YIELD_MONTH_SECURITIES_NAME,
                            'YIELD_CUMULATIVE_' + SECURITIES_NAME as YIELD_CUMULATIVE_SECURITIES_NAME,
                            'BM_MONTH_'         + SECURITIES_NAME as BM_MONTH_SECURITIES_NAME,
                            'BM_CUMULATIVE_'    + SECURITIES_NAME as BM_CUMULATIVE_SECURITIES_NAME    
                        FROM " . $table . " " . "
                        $where2 
                    ) s
                    PIVOT (
                        SUM(NAV_B)
                        FOR [NAV_B_SECURITIES_NAME] IN ([NAV_B_UOBAM], [NAV_B_KTAM]) 
                    ) AS P1
            
                    PIVOT (
                        SUM(UNIT)
                        FOR [UNIT_SECURITIES_NAME] IN ([UNIT_UOBAM], [UNIT_KTAM]) 
                    ) AS P2
                    
                    PIVOT (
                        SUM(NAV_UNIT)
                        FOR [NAV_UNIT_SECURITIES_NAME] IN ([NAV_UNIT_UOBAM], [NAV_UNIT_KTAM]) 
                    ) AS P3
                    
                    PIVOT (
                        SUM(YIELD_MONTH)
                        FOR [YIELD_MONTH_SECURITIES_NAME] IN ([YIELD_MONTH_UOBAM], [YIELD_MONTH_KTAM]) 
                    ) AS P4
                
                    PIVOT (
                        SUM(YIELD_CUMULATIVE)
                        FOR [YIELD_CUMULATIVE_SECURITIES_NAME] IN ([YIELD_CUMULATIVE_UOBAM], [YIELD_CUMULATIVE_KTAM]) 
                    ) AS P5
                
                    PIVOT (
                        SUM(BM_MONTH)
                        FOR [BM_MONTH_SECURITIES_NAME] IN ([BM_MONTH_UOBAM], [BM_MONTH_KTAM]) 
                    ) AS P6
                    
                    PIVOT (
                        SUM(BM_CUMULATIVE)
                        FOR [BM_CUMULATIVE_SECURITIES_NAME] IN ([BM_CUMULATIVE_UOBAM], [BM_CUMULATIVE_KTAM]) 
                    ) AS P7   
            ) AS a
            " . $where . "
            GROUP BY POLICY_ID, REFERENCE_DATE 
            ) M ORDER BY REFERENCE_DATE DESC;";
            Log::info(get_class($this) .'::'. __FUNCTION__  . ' SQLSTR=' . $query);

            $resultset = DB::select(DB::raw($query));
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' == LEAVE == ');
            return $resultset;  
    }

    public function datasource($policy, $name_sht, $date_start, $date_end) {      
        Log::info(get_class($this) .'::'. __FUNCTION__ . ' == ENTER ==');
        
        //$plan = $policy;              // นโยบายการลงทุน
        //$name_sht = Input::get('name_sht');      // บริษัทจัดการ
        //$date_start = Input::get('date_start');  // วันที่เริ่มต้น
        //$date_end =  Input::get('date_end');     // วันที่สิ่้นสุด
         
        $where = " WHERE (REFERENCE_DATE BETWEEN '" . $date_start . "' AND '" . $date_end . "') 
                 ";
        $where2 = "";
        if(strlen($policy) > 0) {
            $where .= " AND POLICY_ID = " . $policy;
        }      
        
        if(strlen($name_sht) > 0) {
            $where2 = " WHERE SECURITIES_NAME = '" . $name_sht . "'";
        }
        // {TBL_EQULITY_UNIT_VAL} UNION ALL {TBL_BOND_UNIT_VAL}
        $query = " 
                SELECT   
                    POLICY_ID, 
                    REFERENCE_DATE as REFERENCE_DATE,
                    SUM([NAV_B_UOBAM]) as [NAV_B_UOBAM],
                    SUM([UNIT_UOBAM]) as [UNIT_UOBAM],
                    SUM([NAV_UNIT_UOBAM]) as [NAV_UNIT_UOBAM],
                    SUM([YIELD_MONTH_UOBAM]) as [YIELD_MONTH_UOBAM],
                    SUM([YIELD_CUMULATIVE_UOBAM]) as [YIELD_CUMULATIVE_UOBAM],
                    SUM([BM_MONTH_UOBAM]) as [BM_MONTH_UOBAM],
                    SUM([BM_CUMULATIVE_UOBAM]) as [BM_CUMULATIVE_UOBAM],
            
                    SUM([NAV_B_KTAM]) as [NAV_B_KTAM],
                    SUM([UNIT_KTAM]) as [UNIT_KTAM],
                    SUM([NAV_UNIT_KTAM]) as [NAV_UNIT_KTAM],
                    SUM([YIELD_MONTH_KTAM]) as [YIELD_MONTH_KTAM],
                    SUM([YIELD_CUMULATIVE_KTAM]) as [YIELD_CUMULATIVE_KTAM],
                    SUM([BM_MONTH_KTAM]) as [BM_MONTH_KTAM],
                    SUM([BM_CUMULATIVE_KTAM]) as [BM_CUMULATIVE_KTAM],
                    
                    (SUM([NAV_B_UOBAM]) +  SUM([NAV_B_KTAM])) as NAV_B_CUMULATIVE,
                    (SUM([UNIT_UOBAM]) +  SUM([UNIT_KTAM])) as UNIT_CUMULATIVE,
                    (SUM([NAV_UNIT_UOBAM]) + SUM([NAV_UNIT_KTAM])) as NAV_UNIT_CUMULATIVE,
                    (SUM([YIELD_MONTH_UOBAM]) + SUM([YIELD_MONTH_KTAM])) as [YIELD_MONTH_CUMULATIVE],
                    (SUM([YIELD_CUMULATIVE_UOBAM]) + SUM([YIELD_CUMULATIVE_KTAM])) as [YIELD_CUMULATIVE_CUMULATIVE],
                    (SUM([BM_MONTH_UOBAM])  + SUM([BM_MONTH_KTAM])) as [BM_MONTH_CUMULATIVE],
                    (SUM([BM_CUMULATIVE_UOBAM])  + SUM([BM_CUMULATIVE_KTAM])) as [BM_CUMULATIVE_CUMULATIVE]

                FROM ( 
                    SELECT   
                        REFERENCE_DATE, 
                        POLICY_ID,
                        ISNULL([NAV_B_UOBAM], 0) as [NAV_B_UOBAM],
                        ISNULL([NAV_B_KTAM], 0) as [NAV_B_KTAM],
            
                        ISNULL([UNIT_UOBAM], 0) as [UNIT_UOBAM],
                        ISNULL([UNIT_KTAM], 0) as [UNIT_KTAM],
            
                        ISNULL([NAV_UNIT_UOBAM], 0) as [NAV_UNIT_UOBAM],
                        ISNULL([NAV_UNIT_KTAM], 0) as [NAV_UNIT_KTAM],
            
                        ISNULL([YIELD_MONTH_UOBAM], 0) as [YIELD_MONTH_UOBAM],
                        ISNULL([YIELD_MONTH_KTAM], 0) as [YIELD_MONTH_KTAM],
                        
                        ISNULL([YIELD_CUMULATIVE_UOBAM], 0) as [YIELD_CUMULATIVE_UOBAM],
                        ISNULL([YIELD_CUMULATIVE_KTAM], 0) as [YIELD_CUMULATIVE_KTAM],
                
                        ISNULL([BM_MONTH_UOBAM], 0) as [BM_MONTH_UOBAM],
                        ISNULL([BM_MONTH_KTAM], 0) as [BM_MONTH_KTAM],
                        
                        ISNULL([BM_CUMULATIVE_UOBAM], 0) as [BM_CUMULATIVE_UOBAM],
                        ISNULL([BM_CUMULATIVE_KTAM], 0) as [BM_CUMULATIVE_KTAM]
                    FROM (
                        SELECT 
                            POLICY_ID,
                            REFERENCE_DATE, 
                            NAV_B, 
                            UNIT,
                            NAV_UNIT, 
                            YIELD_MONTH,
                            YIELD_CUMULATIVE,
                            BM_MONTH,
                            BM_CUMULATIVE,
                            'NAV_B_'            + SECURITIES_NAME as NAV_B_SECURITIES_NAME,
                            'UNIT_'             + SECURITIES_NAME as UNIT_SECURITIES_NAME,
                            'NAV_UNIT_'         + SECURITIES_NAME as NAV_UNIT_SECURITIES_NAME,
                            'YIELD_MONTH_'      + SECURITIES_NAME as YIELD_MONTH_SECURITIES_NAME,
                            'YIELD_CUMULATIVE_' + SECURITIES_NAME as YIELD_CUMULATIVE_SECURITIES_NAME,
                            'BM_MONTH_'         + SECURITIES_NAME as BM_MONTH_SECURITIES_NAME,
                            'BM_CUMULATIVE_'    + SECURITIES_NAME as BM_CUMULATIVE_SECURITIES_NAME    
                        FROM TBL_P2_EQUITY_UNIT_VAL 
                        $where2
                    ) s
                    PIVOT (
                        SUM(NAV_B)
                        FOR [NAV_B_SECURITIES_NAME] IN ([NAV_B_UOBAM], [NAV_B_KTAM]) 
                    ) AS P1
            
                    PIVOT (
                        SUM(UNIT)
                        FOR [UNIT_SECURITIES_NAME] IN ([UNIT_UOBAM], [UNIT_KTAM]) 
                    ) AS P2
                    
                    PIVOT (
                        SUM(NAV_UNIT)
                        FOR [NAV_UNIT_SECURITIES_NAME] IN ([NAV_UNIT_UOBAM], [NAV_UNIT_KTAM]) 
                    ) AS P3
                    
                    PIVOT (
                        SUM(YIELD_MONTH)
                        FOR [YIELD_MONTH_SECURITIES_NAME] IN ([YIELD_MONTH_UOBAM], [YIELD_MONTH_KTAM]) 
                    ) AS P4
                
                    PIVOT (
                        SUM(YIELD_CUMULATIVE)
                        FOR [YIELD_CUMULATIVE_SECURITIES_NAME] IN ([YIELD_CUMULATIVE_UOBAM], [YIELD_CUMULATIVE_KTAM]) 
                    ) AS P5
                
                    PIVOT (
                        SUM(BM_MONTH)
                        FOR [BM_MONTH_SECURITIES_NAME] IN ([BM_MONTH_UOBAM], [BM_MONTH_KTAM]) 
                    ) AS P6
                    
                    PIVOT (
                        SUM(BM_CUMULATIVE)
                        FOR [BM_CUMULATIVE_SECURITIES_NAME] IN ([BM_CUMULATIVE_UOBAM], [BM_CUMULATIVE_KTAM]) 
                    ) AS P7   
            ) AS a
            " . $where . "
            GROUP BY POLICY_ID, REFERENCE_DATE

            UNION ALL 
                SELECT   
                    POLICY_ID, 
                    REFERENCE_DATE as REFERENCE_DATE,

                    SUM([NAV_B_UOBAM]) as [NAV_B_UOBAM],
                    SUM([UNIT_UOBAM]) as [UNIT_UOBAM],
                    SUM([NAV_UNIT_UOBAM]) as [NAV_UNIT_UOBAM],
                    SUM([YIELD_MONTH_UOBAM]) as [YIELD_MONTH_UOBAM],
                    SUM([YIELD_CUMULATIVE_UOBAM]) as [YIELD_CUMULATIVE_UOBAM],
                    SUM([BM_MONTH_UOBAM]) as [BM_MONTH_UOBAM],
                    SUM([BM_CUMULATIVE_UOBAM]) as [BM_CUMULATIVE_UOBAM],
                
                    SUM([NAV_B_KTAM]) as [NAV_B_KTAM],
                    SUM([UNIT_KTAM]) as [UNIT_KTAM],
                    SUM([NAV_UNIT_KTAM]) as [NAV_UNIT_KTAM],
                    SUM([YIELD_MONTH_KTAM]) as [YIELD_MONTH_KTAM],
                    SUM([YIELD_CUMULATIVE_KTAM]) as [YIELD_CUMULATIVE_KTAM],
                    SUM([BM_MONTH_KTAM]) as [BM_MONTH_KTAM],
                    SUM([BM_CUMULATIVE_KTAM]) as [BM_CUMULATIVE_KTAM],
                        
                    (SUM([NAV_B_UOBAM]) +  SUM([NAV_B_KTAM])) as NAV_B_CUMULATIVE,
                    (SUM([UNIT_UOBAM]) +  SUM([UNIT_KTAM])) as UNIT_CUMULATIVE,
                    (SUM([NAV_UNIT_UOBAM]) + SUM([NAV_UNIT_KTAM])) as NAV_UNIT_CUMULATIVE,
                    (SUM([YIELD_MONTH_UOBAM]) + SUM([YIELD_MONTH_KTAM])) as [YIELD_MONTH_CUMULATIVE],
                    (SUM([YIELD_CUMULATIVE_UOBAM]) + SUM([YIELD_CUMULATIVE_KTAM])) as [YIELD_CUMULATIVE_CUMULATIVE],
                    (SUM([BM_MONTH_UOBAM])  + SUM([BM_MONTH_KTAM])) as [BM_MONTH_CUMULATIVE],
                    (SUM([BM_CUMULATIVE_UOBAM])  + SUM([BM_CUMULATIVE_KTAM])) as [BM_CUMULATIVE_CUMULATIVE]

                FROM ( 
                    SELECT   
                        REFERENCE_DATE, 
                        POLICY_ID,
                    
                        ISNULL([NAV_B_UOBAM], 0) as [NAV_B_UOBAM],
                        ISNULL([NAV_B_KTAM], 0) as [NAV_B_KTAM],
                        
                        ISNULL([UNIT_UOBAM], 0) as [UNIT_UOBAM],
                        ISNULL([UNIT_KTAM], 0) as [UNIT_KTAM],
                        
                        ISNULL([NAV_UNIT_UOBAM], 0) as [NAV_UNIT_UOBAM],
                        ISNULL([NAV_UNIT_KTAM], 0) as [NAV_UNIT_KTAM],
                        
                        ISNULL([YIELD_MONTH_UOBAM], 0) as [YIELD_MONTH_UOBAM],
                        ISNULL([YIELD_MONTH_KTAM], 0) as [YIELD_MONTH_KTAM],
                        
                        ISNULL([YIELD_CUMULATIVE_UOBAM], 0) as [YIELD_CUMULATIVE_UOBAM],
                        ISNULL([YIELD_CUMULATIVE_KTAM], 0) as [YIELD_CUMULATIVE_KTAM],
                
                        ISNULL([BM_MONTH_UOBAM], 0) as [BM_MONTH_UOBAM],
                        ISNULL([BM_MONTH_KTAM], 0) as [BM_MONTH_KTAM],
                        
                        ISNULL([BM_CUMULATIVE_UOBAM], 0) as [BM_CUMULATIVE_UOBAM],
                        ISNULL([BM_CUMULATIVE_KTAM], 0) as [BM_CUMULATIVE_KTAM]
                    FROM (
                        SELECT 
                            POLICY_ID,
                            REFERENCE_DATE, 
                            NAV_B, 
                            UNIT,
                            NAV_UNIT, 
                            YIELD_MONTH,
                            YIELD_CUMULATIVE,
                            BM_MONTH,
                            BM_CUMULATIVE,
                            'NAV_B_'            + SECURITIES_NAME as NAV_B_SECURITIES_NAME,
                            'UNIT_'             + SECURITIES_NAME as UNIT_SECURITIES_NAME,
                            'NAV_UNIT_'         + SECURITIES_NAME as NAV_UNIT_SECURITIES_NAME,
                            'YIELD_MONTH_'      + SECURITIES_NAME as YIELD_MONTH_SECURITIES_NAME,
                            'YIELD_CUMULATIVE_' + SECURITIES_NAME as YIELD_CUMULATIVE_SECURITIES_NAME,
                            'BM_MONTH_'         + SECURITIES_NAME as BM_MONTH_SECURITIES_NAME,
                            'BM_CUMULATIVE_'    + SECURITIES_NAME as BM_CUMULATIVE_SECURITIES_NAME
                            
                        FROM TBL_P2_BOND_UNIT_VAL 
                        $where2
                    ) k
                    
                    PIVOT (
                        SUM(NAV_B)
                        FOR [NAV_B_SECURITIES_NAME] IN ([NAV_B_UOBAM], [NAV_B_KTAM]) 
                    ) AS P1
                    
                    PIVOT (
                        SUM(UNIT)
                        FOR [UNIT_SECURITIES_NAME] IN ([UNIT_UOBAM], [UNIT_KTAM]) 
                    ) AS P2
                    
                    PIVOT (
                        SUM(NAV_UNIT)
                        FOR [NAV_UNIT_SECURITIES_NAME] IN ([NAV_UNIT_UOBAM], [NAV_UNIT_KTAM]) 
                    ) AS P3
                    
                    PIVOT (
                        SUM(YIELD_MONTH)
                        FOR [YIELD_MONTH_SECURITIES_NAME] IN ([YIELD_MONTH_UOBAM], [YIELD_MONTH_KTAM]) 
                    ) AS P4
                
                    PIVOT (
                        SUM(YIELD_CUMULATIVE)
                        FOR [YIELD_CUMULATIVE_SECURITIES_NAME] IN ([YIELD_CUMULATIVE_UOBAM], [YIELD_CUMULATIVE_KTAM]) 
                    ) AS P5
                
                        PIVOT (
                        SUM(BM_MONTH)
                        FOR [BM_MONTH_SECURITIES_NAME] IN ([BM_MONTH_UOBAM], [BM_MONTH_KTAM]) 
                    ) AS P6
                    
                        PIVOT (
                        SUM(BM_CUMULATIVE)
                        FOR [BM_CUMULATIVE_SECURITIES_NAME] IN ([BM_CUMULATIVE_UOBAM], [BM_CUMULATIVE_KTAM]) 
                    ) AS P7   
            ) AS b
            " . $where . "
            GROUP BY POLICY_ID, REFERENCE_DATE ";
        //echo $query;
        Log::info(get_class($this) .'::'. __FUNCTION__  . ' SQLSTR=' . $query);
        
        $resultset = DB::select(DB::raw($query));
        Log::info(get_class($this) .'::'. __FUNCTION__ . ' == LEAVE == ');
        return $resultset;  
    }
    /**
     * Ajax search and generate report
     *
     * @return json with 
     * @comment This method filled data into 'ajax_p2_equity_investment_report_main.php'
     */
    public function ajax_report_search_main() {

        Log::info(get_class($this) .'::'. __FUNCTION__);
        
        $utils = new MEAUtils();

        $view_name = 'backend.pages.ajax.ajax_p2_equity_investment_report_main';

        $plan = Input::get('plan');              // นโยบายการลงทุน
        $name_sht = Input::get('name_sht');      // บริษัทจัดการ
        $date_start = Input::get('date_start');  // วันที่เริ่มต้น
        $date_end =  Input::get('date_end');     // วันที่สิ่้นสุด
        $utils = new MEAUtils();

        if($date_start==null) {
            $date_start = date('Y-m-d') . ' 00:00:01';
        }
        
        if($date_end==null) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }
        
        if(strlen($date_start) < 2) {
            $date_start = date('Y-m-d') . ' 00:00:01';
         }
 
        if(strlen($date_end) < 2) {
             $date_end = date('Y-m-d') . ' 23:59:59';
        }

       

        $table1_show = "display:none;";
        $table2_show = "display:none;";

        

        $table1_show = 'display:none;';
        $table2_show = 'display:none;';
        $tbl1 = 'TBL_P2_BOND_NAV';
        $tbl2 = 'TBL_P2_EQUITY_NAV';

        if($plan =='ตราสารหนี้') {
            $table1_show = 'display:block;';
            $table2_show = 'display:none;';
             
        } else if ($plan =='ตราสารทุน') {
            $table2_show = 'display:block;';
            $table1_show = 'display:none;';
             
        } else {
            $table1_show = 'display:block;';
            $table2_show = 'display:block;';
            // ไม่ระบุ table ดึงทั้ง TBL_P2_BOND_NAV,TBL_P2_EQUITY_NAV
        }

       
        $method = "0";
        // $bondRS   = $this->datasource($plan, $name_sht, $date_start, $date_end);
        $equityRS = $this->datasource($plan, $name_sht, $date_start, $date_end);
        $topOneEquityRS = $this->getTopOne("TBL_P2_EQUITY_UNIT_VAL", $plan, $name_sht, $date_start, $date_end);
        $topOneBondRS   = $this->getTopOne("TBL_P2_BOND_UNIT_VAL", $plan, $name_sht, $date_start, $date_end);
        

        Log::info(get_class($this) . '::' . __FUNCTION__ . 
               ':: plan = ' . $plan . 
               ', method = ' . $method .
               ', name_sht = ' . $name_sht .
               ', date_start = ' . $date_start . ', date_end = '. $date_end);
         
        $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
        $thai_date_end =  (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";
        Log::info(get_class($this) .'::'. __FUNCTION__. ' ' . ' converted date: ' . $date_start .' to ' . $thai_date_start . ', ' . $date_end . ' to '. $thai_date_end );
               
        $pretty_date = $utils->prettyPrintThaiDate($thai_date_start, $thai_date_end, "red");
        $pretty_date = str_replace( "ข้อมูล วันที่", "ระหว่าง วันที่", $pretty_date);

        $today = $utils->toThaiDateTime(date('d-m-Y'), false);

        $returnHTML = view($view_name)->with([
            'htmlPaginate' => '',
            'data' => $equityRS, //array('bond_nav' => $bondRS ,'equity' =>$equityRS),
            'equitytop' => $topOneEquityRS,
            'bondtop' => $topOneBondRS,
            
            'totals' => 1,
            'PageSize' => 1,
            'PageNumber' => 1,
            'pretty_date' => $pretty_date,
            'plan' => $plan,
            'today' => $today,
            'utils' =>$utils //toThaiDateTime
        ])->render();

        return response()->json(array('success' => true,
                                      'html' => $returnHTML));

    }

   /**
    * Export to Excel
    * Lastupdate: 2018-11-29
    * 
    */

    public function ajax_report_search_export_main() {
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $ArrParam = array();

       

        $plan = Input::get('plan');              // นโยบายการลงทุน
        $name_sht = Input::get('name_sht');      // บริษัทจัดการ
        $date_start = Input::get('date_start');  // วันที่เริ่มต้น
        $date_end =  Input::get('date_end');     // วันที่สิ่้นสุด

        $ArrParam["name_sht"]    = $name_sht;
        $ArrParam["date_start"]  = $date_start;
        $ArrParam["date_end"]    = $date_end;
       

        $utils = new MEAUtils();

        if($date_start==null) {
            $date_start = date('Y-m-d') . ' 00:00:01';
        }
        
        if($date_end==null) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }
        
        if(strlen($date_start) < 2) {
            $date_start = date('Y-m-d') . ' 00:00:01';
        }
 
        if(strlen($date_end) < 2) {
             $date_end = date('Y-m-d') . ' 23:59:59';
        }

        $method = "0";
        $equityRS = $this->datasource($plan, $name_sht, $date_start, $date_end);
        $topOneEquityRS = $this->getTopOne("TBL_P2_EQUITY_UNIT_VAL", $plan, $name_sht, $date_start, $date_end);
        $topOneBondRS   = $this->getTopOne("TBL_P2_BOND_UNIT_VAL", $plan, $name_sht, $date_start, $date_end);
        

        /// calculate summary
        $sumEquityK = 0;
        $sumEquityU = 0;
        $sumBondK = 0;
        $sumBondU = 0;
        $sumCumulativeEquity = 0;
        $sumCumulativeBond = 0;

        $A1 = 0;
        $B1 = 0;
        $C1 = 0;
        $G1 = 0;
        $H1 = 0;

        $L1 = 0;
        $L2 = 0;
        $L3 = 0;

        $M1 = 0;
        $M2 = 0;
        $M3 = 0;
        
        $I1 = 0;
        $I2 = 0;
        $I3 = 0; 

        foreach($topOneEquityRS as $index =>$field) {
            $sumEquityU = (float)$field->NAV_B_UOBAM;
            $sumEquityK = (float)$field->NAV_B_KTAM;
            $sumCumulativeEquity = (float)$field->NAV_B_CUMULATIVE;
            $G1 = $sumCumulativeEquity; 

            $L1 = (float)$field->YIELD_CUMULATIVE_UOBAM;
            $L2 = (float)$field->YIELD_CUMULATIVE_KTAM;
            $L3 = (float)$field->YIELD_CUMULATIVE_CUMULATIVE;
            break;
        }


        foreach($topOneBondRS as $index =>$field) {
            $sumBondU += ((float)$sumEquityU + (float)$field->NAV_B_UOBAM);
            $sumBondK += ((float)$sumEquityK + (float)$field->NAV_B_KTAM);
            $sumCumulativeBond = (float)$field->NAV_B_CUMULATIVE;

            $A1 = $sumBondU;
            $B1 = $sumBondK;
            $C1 = ($sumBondU + $sumBondK);
            $H1 = $sumCumulativeBond; 

            $M1 = (float)$field->YIELD_CUMULATIVE_UOBAM;
            $M2 = (float)$field->YIELD_CUMULATIVE_KTAM;
            $M3 = (float)$field->YIELD_CUMULATIVE_CUMULATIVE;

            break;
        }

        $I1 = ($L1*0.90) + ($M1*0.10);
        $I2 = ($L1*0.80) + ($M1*0.20);
        $I3 = ($L1*0.70) + ($M1*0.30);
        $I4 = ($L1*0.60) + ($M1*0.40);
        $I5 = ($L1*0.50) + ($M1*0.50);
        $I6 = ($L1*0.40) + ($M1*0.60);
        $I7 = ($L1*0.30) + ($M1*0.70);
        $I8 = ($L1*0.20) + ($M1*0.80);
        $I9 = ($L1*0.10) + ($M1*0.90);
    
        $J1 = ($L2*0.90) + ($M2*0.10);
        $J2 = ($L2*0.80) + ($M2*0.20);
        $J3 = ($L2*0.70) + ($M2*0.30);
        $J4 = ($L2*0.60) + ($M2*0.40);
        $J5 = ($L2*0.50) + ($M2*0.50);
        $J6 = ($L2*0.40) + ($M2*0.60);
        $J7 = ($L2*0.30) + ($M2*0.70);
        $J8 = ($L2*0.20) + ($M2*0.80);
        $J9 = ($L2*0.10) + ($M2*0.90);
        
        $K1 = ($L3*0.90) + ($M3*0.10);
        $K2 = ($L3*0.80) + ($M3*0.20);
        $K3 = ($L3*0.70) + ($M3*0.30);
        $K4 = ($L3*0.60) + ($M3*0.40);
        $K5 = ($L3*0.50) + ($M3*0.50);
        $K6 = ($L3*0.40) + ($M3*0.60);
        $K7 = ($L3*0.30) + ($M3*0.70);
        $K8 = ($L3*0.20) + ($M3*0.80);
        $K9 = ($L3*0.10) + ($M3*0.90);

        $summary = array();
        $summary['G1'] = $G1;
        $summary['L1'] = $L1;
        $summary['L2'] = $L2;
        $summary['L3'] = $L3;

        $summary['A1'] = $A1;
        $summary['B1'] = $B1;
        $summary['C1'] = $C1;
        $summary['H1'] = $H1;

        $summary['I1'] = $I1;
        $summary['I2'] = $I2;
        $summary['I3'] = $I3;
        $summary['I4'] = $I4;
        $summary['I5'] = $I5;
        $summary['I6'] = $I6;
        $summary['I7'] = $I7;
        $summary['I8'] = $I8;
        $summary['I9'] = $I9;

        $summary['J1'] = $J1;
        $summary['J2'] = $J2;
        $summary['J3'] = $J3;
        $summary['J4'] = $J4;
        $summary['J5'] = $J5;
        $summary['J6'] = $J6;
        $summary['J7'] = $J7;
        $summary['J8'] = $J8;
        $summary['J9'] = $J9;

        $summary['K1'] = $K1;
        $summary['K2'] = $K2;
        $summary['K3'] = $K3;
        $summary['K4'] = $K4;
        $summary['K5'] = $K5;
        $summary['K6'] = $K6;
        $summary['K7'] = $K7;
        $summary['K8'] = $K8;
        $summary['K9'] = $K9;
                         

        ///

        Log::info(get_class($this) . '::' . __FUNCTION__ . 
               ':: plan = ' . $plan . 
               ', method = ' . $method .
               ', name_sht = ' . $name_sht .
               ', date_start = ' . $date_start . ', date_end = '. $date_end );
         
        $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
        $thai_date_end =  (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";
        Log::info(get_class($this) .'::'. __FUNCTION__. ' ' . ' converted date: ' . $date_start .' to ' . $thai_date_start . ', ' . $date_end . ' to '. $thai_date_end  );
               
        $pretty_date = $utils->prettyPrintThaiDate($thai_date_start, $thai_date_end, "red");
        $pretty_date = str_replace( "ข้อมูล วันที่", "ระหว่าง วันที่", $pretty_date);
        
        /* create Excel file for download */ 
        
        return  Excel::create('อัตราตอบแทนตราสารทุน', function ($excel) use (
                   $equityRS, $topOneEquityRS, $topOneBondRS, 
                   $pretty_date, $thai_date_start, $thai_date_end,
                   $name_sht, $plan, $date_start, $date_end, $ArrParam, $summary, $utils) {

            $excel->sheet('อัตราตอบแทนตราสารทุน', function ($sheet) use ($equityRS, $topOneEquityRS, $topOneBondRS, 
                   $pretty_date, $thai_date_start, $thai_date_end,
                   $name_sht, $plan, $date_start, $date_end,  $ArrParam, $summary, $utils) {
                $sheet->setStyle([
                    'borders' => [
                        'allborders' => [
                            'color' => [
                                'rgb' => 'b3b6b7'
                            ]
                        ]
                    ]
                ]);

                $sheet->setFontFamily('Cordia New'); 
                $sheet->setFontSize(18);

                // first row styling and writing content
                $sheet->mergeCells('A1:S1');
                $sheet->mergeCells('A2:S2');
                $sheet->mergeCells('A3:S3');
                 
                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Cordia New');
                    $row->setFontSize(20);
                    $row->setFontWeight('bold');
                    $row->setAlignment('center');
                    $row->setBackground('#b2babb');
                    // Set all borders (top, right, bottom, left)
                    $row->setBorder('thin', 'thin', 'none', 'thin');

                });

                $sheet->row(2, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'none', 'thin');
                });

                $sheet->row(3, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'thin', 'thin');

                });

                $sheet->row(1, array('กองทุนสํารองเลี้ยงชีพพนักงานการไฟฟ้านครหลวง ซึ่งจดทะเบียนแล้ว'));
                $sheet->row(2, array('รายงานข้อมูลมูลค่าหน่วยลงทุนและอัตราผลตอบแทน'));
                $sheet->row(3, array('ระหว่าง วันที่' . $thai_date_start . ' ถึง ' . $thai_date_end));
                
                
                $sheet->mergeCells('A4:A5');
                $sheet->mergeCells('B4:B5');
                $sheet->mergeCells('C4:F4');
                $sheet->mergeCells('J4:M4');
                $sheet->mergeCells('H4:I4');
                $sheet->mergeCells('O4:R4');

                $sheet->cell('A4', function($cell) {
                    // manipulate the cell
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                    // Set all borders (top, right, bottom, left)
                    //$cell->setBorder('thin', 'thin', 'thin', 'thin');
                
                });

                $sheet->cell('B4', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                 
            
                $sheet->cell('N4', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                $sheet->row(4, function ($row) {
                    $row->setAlignment('center');
                    
                });

                $header0[] = null;
                $header0[0] = 'นโยบายการลงทุน';
                $header0[1] = 'ว./ด./ป.';
                $header0[2] = 'UOBAM';
                $header0[3] = '';
                $header0[4] = '';
                $header0[5] = '';
                $header0[6] = 'ล้านบาท';
                $header0[7] = 'Benchmark';
                $header0[8] = '';
                //---------- 
                $header0[9] = 'KTAM';
                $header0[10] = '';
                $header0[11] = '';
                $header0[12] = '';
                $header0[13] = 'ล้านบาท';
                //----------  
                $header0[14] = 'CONSOLIDATE';
                $header0[15] = '';
                $header0[16] = '';
                $header0[17] = '';
                $header0[18] = 'ล้านบาท';
                 
                
                $sheet->row(4, $header0);// array('เดือน', 'มูลค่าทรัพย์สินสุทธิ', 'อัตราตอบแทน'));

                $header0[]   = null;
                $header0[0]  = 'นโยบายการลงทุน';
                $header0[1]  = '';
                $header0[2]  = 'NAV (ล้านบาท)';
                $header0[3]  = 'จำนวณหน่วย (ล้านบาท)';
                $header0[4]  = 'NAV ต่อหน่วย';
                $header0[5]  = 'อัตราตอบแทนรายเดือน';
                $header0[6]  = 'อัตราตอบแทนสะสม (%)';
                $header0[7]  = 'รายเดือน (%)';
                $header0[8]  = 'สะสม (%)';

                $header0[9]  = 'NAV (ล้านบาท)';
                $header0[10] = 'จำนวณหน่วย (ล้านบาท)';
                $header0[11] = 'NAV ต่อหน่วย';
                $header0[12] = 'อัตราตอบแทนรายเดือน';
                $header0[13] = 'อัตราตอบแทนสะสม (%)';

                $header0[14]  = 'NAV (ล้านบาท)';
                $header0[15] = 'จำนวณหน่วย (ล้านบาท)';
                $header0[16] = 'NAV ต่อหน่วย';
                $header0[17] = 'อัตราตอบแทนรายเดือน';
                $header0[18] = 'อัตราตอบแทนสะสม (%)';
               


                $sheet->row(5, function ($row) {
                    $row->setAlignment('center');
                    
                });
                $sheet->row(5, $header0);// array('เดือน', 'มูลค่าทรัพย์สินสุทธิ', 'อัตราตอบแทน'));

              // Set multiple column formats
                $sheet->setColumnFormat(array(
                   // 'A' => 'yyyy-mm-dd',
                    'B' => '#,##0.00',
                    'C' => '#,##0.00',
                    'D' => '#,##0.00',
                    'E' => '#,##0.00',
                    'F' => '#,##0.00',
                    'G' => '#,##0.00',
                    'H' => '#,##0.00',
                    'I' => '#,##0.00',
                    'J' => '#,##0.00',
                    'K' => '#,##0.00',
                    'L' => '#,##0.00',
                    'M' => '#,##0.00',
                    'N' => '#,##0.00',
                    'O' => '#,##0.00',
                    'P' => '#,##0.00',
                    'R' => '#,##0.00',
                    'S' => '#,##0.00',

                    'T' => '#,##0.00',
                    'U' => '#,##0.00'
                ));

                $rowCount = 6;
                // Append Data
                //
               if($equityRS) {
                 
                    $old = "";
                    $last_NAV_B_UOBAM = 0;
                    $last_NAV_B_KTAM = 0;
                    $sum_NAV_B_UOBAM = 0;
                    $sum_NAV_B_KTAM = 0;
                     $caption = "";
                    foreach($equityRS as $index =>$rs) {
                        if($old!= $rs->POLICY_ID) {
                            if ($old != "") {
                                $sum_NAV_B_UOBAM = $last_NAV_B_UOBAM;
                                $sum_NAV_B_KTAM = $last_NAV_B_KTAM;
                            }  
                            $caption = ($rs->POLICY_ID == 1) ? "ตราสารทุน" : "ตราสารหนี้";
                            Log::info($caption . "\n"); 
                        } else {
                            $caption = "";
                        }

                        $fields  = [];
                        $fields[0] = $caption;
                        $fields[1] =  $utils->toThaiDateTime($rs->REFERENCE_DATE, false); //toThaiShortMonth($rs->REFERENCE_DATE);
                        $fields[2] =  $rs->NAV_B_UOBAM;
                        $fields[3] =  $rs->UNIT_UOBAM;
                        $fields[4] =  $rs->NAV_UNIT_UOBAM;
                        $fields[5] =  $rs->YIELD_MONTH_UOBAM;

                       
                        $fields[6] = $rs->YIELD_CUMULATIVE_UOBAM;
                        $fields[7] = $rs->BM_MONTH_UOBAM;
                        $fields[8] = $rs->BM_CUMULATIVE_UOBAM;
                        $fields[9] = $rs->NAV_B_KTAM;
                        $fields[10] = $rs->UNIT_KTAM;
                        $fields[11] = $rs->NAV_UNIT_KTAM;
                        $fields[12] = $rs->YIELD_MONTH_KTAM; 
                        $fields[13] = $rs->YIELD_CUMULATIVE_KTAM;
                        $fields[14] = $rs->NAV_B_CUMULATIVE; 
                        $fields[15] = $rs->UNIT_CUMULATIVE;
                        $fields[16] = $rs->NAV_UNIT_CUMULATIVE;
                        $fields[17] = $rs->YIELD_MONTH_CUMULATIVE;
                        $fields[18] = $rs->YIELD_CUMULATIVE_CUMULATIVE;


                     
                        $sheet->appendRow($fields);

                        $old = $rs->POLICY_ID;
                        $last_NAV_B_UOBAM = $rs->NAV_B_UOBAM;
                        $rowCount++;

                     } // foreach 

                     $A1 = $summary['A1'];
                     $B1 = $summary['B1'];
                     $C1 = $summary['C1'];
                     $fields = [];
                     $fields[0] = '';
                     $fields[1] = 'รวม:';
                     $fields[2] = $A1;//number_format((float)$A1, 2, '.', ',');
                     $fields[3] = '';
                     $fields[4] = '';
                     $fields[5] = '';
                     $fields[6] = '';
                     $fields[7] = '';
                     $fields[8] = '';
                     $fields[9] = $B1; //number_format((float)$B1, 2, '.', ',');
                     $fields[10] = '';
                     $fields[11] = '';
                     $fields[12] = '';
                     $fields[13] = '';
                     $fields[16] = $C1; //number_format((float)$C1, 2, '.', ',');
                     $fields[17] = '';
                     $fields[18] = '';
                     $fields[19] = '';
                     $fields[20] = '';
                   
                     $sheet->appendRow($fields);  

                     // ตาราง สัดส่วนการลงทุน ณ.วันที่
                     $G1 = $summary['G1'];
                     $H1 = $summary['H1'];
                     $C1 = $summary['C1'];

                     $headerfields = [];
                     $headerfields[0] = '';  
                     $sheet->appendRow($headerfields);
                     
                     $prop = 'B1'.':B'.($rowCount+2);
                     $sheet->cell($prop, function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                     });

                     $prop = 'A'.($rowCount+2) . ':' . 'B'. ($rowCount+4);
                     $sheet->cell($prop, function($cell) {
                        $cell->setBackground('#e5e8e8');
                        $cell->setBorder('none', 'thin', 'thin', 'thin');
                     }); 

                     $headerfields[0] = ' สัดส่วนการลงทุน ณ.วันที่ ';
                     $headerfields[1] = $utils->toThaiDateTime(date("d-m-Y"), false);
                     $sheet->row(($rowCount+2),$headerfields);
                     $headerfields[0] = ' นโยบายตราสารหนี้ % ';
                     $headerfields[1] = ($G1 > 0) ? (number_format((float)(($G1 / $C1) * 100), 2, '.', ',')) : 0.00; //($G1 > 0) ? (($G1 / $C1) * 100) : 0.0;
                     
                     $sheet->row(($rowCount+3),$headerfields);
                     $headerfields[0] = ' นโยบายตราสารทุน % ';
                     $headerfields[1] = ($H1 > 0) ? (number_format((float)(($H1 / $C1) * 100), 2, '.', ',')) : 0.00; //($H1 > 0) ? (($H1 / $C1) * 100) : 0.0;
                     $sheet->row(($rowCount+4),$headerfields);

                     $fields = [];
                     $fields[0] = '';
                     $fields[1] = '';
                     //$sheet->appendRow($fields);  
                     
                     // ตาราง แยกสัดส่วนการลงทุน
                     $rowCount= $rowCount+6;
                     $headerfields = [];
                     $headerfields[0] = ' รายงานผลตอบแทนแยกตามสัดส่วนการลงทุน';
                     $headerfields[1] = 'UOBAM';
                     $headerfields[2] = 'KTAM';
                     $headerfields[3] = 'สะสม';
                     $prop = 'A'.($rowCount) . ':' . 'D'. ($rowCount);
                     $sheet->cell($prop, function($cell) {
                        $cell->setAlignment('right');
                        $cell->setBackground('#e5e8e8');
                        $cell->setBorder('none', 'thin', 'thin', 'thin');
                     }); 
                     // $prop = 'A'.($rowCount) . ':' . 'D'. ($rowCount+7);

                     $prop = 'A'.($rowCount) . ':' . 'A'. ($rowCount+7);
                     $sheet->cell($prop , function($cell) {
                        $cell->setAlignment('left');
                     });

                     $sheet->row($rowCount,$headerfields);

                     $rowCount++;
                     $headerfields = [];
                     $headerfields[0] = ' 1. ตราสารหนี้ 90% ตราสารทุน 10%';
                     $headerfields[1] = $summary['I1']; //number_format((float)$summary['I1'], 2, '.', ',');
                     $headerfields[2] = $summary['J1']; //number_format((float)$summary['J1'], 2, '.', ',');
                     $headerfields[3] = $summary['K1'];// number_format((float)$summary['K1'], 2, '.', ',');
                     $sheet->row($rowCount,$headerfields);
                     //$sheet->cell('A'.$rowCount , function($cell) {
                     //   $cell->setAlignment('left');
                     //});

                     $rowCount++;
                     $headerfields = [];
                     $headerfields[0] = ' 2. ตราสารหนี้ 80% ตราสารทุน 20%';
                     $headerfields[1] = $summary['I2']; 
                     $headerfields[2] = $summary['J2']; 
                     $headerfields[3] = $summary['K2']; 
                     $sheet->row($rowCount,$headerfields);

                     $rowCount++;
                     $headerfields = [];
                     $headerfields[0] = ' 3. ตราสารหนี้ 70% ตราสารทุน 30%';
                     $headerfields[1] = $summary['I3']; 
                     $headerfields[2] = $summary['J3']; 
                     $headerfields[3] = $summary['K3']; 
                     $sheet->row($rowCount,$headerfields);

                     $rowCount++;
                     $headerfields = [];
                     $headerfields[0] = ' 4. ตราสารหนี้ 60% ตราสารทุน 40%';
                     $headerfields[1] = $summary['I4']; 
                     $headerfields[2] = $summary['J4']; 
                     $headerfields[3] = $summary['K4']; 
                     $sheet->row($rowCount,$headerfields);

                     $rowCount++;
                     $headerfields = [];
                     $headerfields[0] = ' 5. ตราสารหนี้ 50% ตราสารทุน 50%';
                     $headerfields[1] = $summary['I5']; 
                     $headerfields[2] = $summary['J5']; 
                     $headerfields[3] = $summary['K5']; 
                     $sheet->row($rowCount,$headerfields);

                     $rowCount++;
                     $headerfields = [];
                     $headerfields[0] = ' 6. ตราสารหนี้ 40% ตราสารทุน 60%';
                     $headerfields[1] = $summary['I6']; 
                     $headerfields[2] = $summary['J6']; 
                     $headerfields[3] = $summary['K6']; 
                     $sheet->row($rowCount,$headerfields);

                     $rowCount++;
                     $headerfields = [];
                     $headerfields[0] = ' 7. ตราสารหนี้ 70% ตราสารทุน 30%';
                     $headerfields[1] = $summary['I7']; 
                     $headerfields[2] = $summary['J7']; 
                     $headerfields[3] = $summary['K7']; 
                     $sheet->row($rowCount,$headerfields);

                     $rowCount++;
                     $headerfields = [];
                     $headerfields[0] = ' 8. ตราสารหนี้ 80% ตราสารทุน 20%';
                     $headerfields[1] = $summary['I8']; 
                     $headerfields[2] = $summary['J8']; 
                     $headerfields[3] = $summary['K8']; 
                     $sheet->row($rowCount,$headerfields);

                      $rowCount++;
                     $headerfields = [];
                     $headerfields[0] = ' 9. ตราสารหนี้ 90% ตราสารทุน 10%';
                     $headerfields[1] = $summary['I9']; 
                     $headerfields[2] = $summary['J9']; 
                     $headerfields[3] = $summary['K9']; 
                     $sheet->row($rowCount,$headerfields);

               } // if $equityRS

            }); // END: $excel->sheet
        })->download('xls');; // END: Excel::create
    }

    ///////////////////

    public function NOT_USE_ajax_report_search_export_main() {

        $ArrParam = array();

        $ArrParam["select_year"] =Input::get("select_year");
        $ArrParam["select_month"] =Input::get("select_month");
        $ArrParam["plan"] = Input::get("plan");

        $plan = Input::get('plan');
        $select_month = Input::get('select_month');
        $select_year = Input::get('select_year');
        
       
       
        $tbl1 = 'TBL_P2_BOND_NAV';
        $tbl2 = 'TBL_P2_EQUITY_NAV';

        Log::info('ajax_report_search_export_main() :: plan=' . $plan . ' , select_month=' . $select_month . ' , select_year='. $select_year);
        $results = array();
        $results2 = array();
        $bondRS   = $this->mainReportDatasource('TBL_P2_BOND_NAV', $plan, $select_month, $select_year);
        $equityRS = $this->mainReportDatasource('TBL_P2_EQUITY_NAV', $plan, $select_month, $select_year);


        $bondChart   = $this->bondChartDatasource($plan, $select_month, $select_year);
        $equityChart = $this->equityChartDatasource($plan, $select_month, $select_year);

      
        $results = array();
        $ref_date1 = '';
        $ref_date2 = '';
       
        foreach ($bondRS as $rowset =>$rs) {
            $ref_date1 =  $rs->yyyy;
            break;
        }
        foreach ($equityRS as $rowset =>$rs) {
            $ref_date2 =  $rs->yyyy; //toThaiShortMonth($rs->item) . ' ' .
            break;
        }

        /* PIE Chart */
        $pie1 = array(); 
        $pie1_data1 = array();
        $reference_date1 = date('Y-m-d');
        $reference_date2 = date('Y-m-d');

        if ($bondChart && (count($bondChart) > 0 )) {
            $reference_date1 = getThaiMonth(intval(date('m', strtotime($bondChart[0]->REF_DATE)))) .' ปี '  . (date('Y',strtotime($bondChart[0]->REF_DATE)) +  543);
            $pie1_data1["value"] =  $bondChart[0]->GR;

            $pie1_data2 = array();
            $pie1_data2["value"] =  $bondChart[0]->DR;
            
            $pie1_data3 = array();
            $pie1_data3["value"] =  $bondChart[0]->DE;

            $pie1_data4 = array();
            $pie1_data4["value"] =  $bondChart[0]->OT;

        } else {
            if (($select_month) && ($select_year)) {
                $reference_date1 = getThaiMonth($select_month) .' ปี '  . ($select_year + 543) ;
            }
            else {
                 $reference_date1 = getThaiMonth(intval(date('m'))) . ' ปี ' . (date('Y') + 543);
            }
            $pie1_data1["value"] =  0;

            $pie1_data2 = array();
            $pie1_data2["value"] =  0;
            
            $pie1_data3 = array();
            $pie1_data3["value"] =  0;

            $pie1_data4 = array();
            $pie1_data4["value"] =  0;
        }


        array_push($pie1, $pie1_data1, $pie1_data2, $pie1_data3, $pie1_data4);

        $pie2 = array(); 
        $pie2_data1 = array();
        
        if ($equityChart && (count($equityChart) > 0 )) {
            //$reference_date2 = date('Y-m-d', strtotime($equityChart[0]->REF_DATE));
            $reference_date2 =  getThaiMonth(intval(date('m', strtotime($equityChart[0]->REF_DATE)))) .' ปี '  . (date('Y',strtotime($equityChart[0]->REF_DATE)) +  543);
           
            $pie2_data1["value"] = $equityChart[0]->DR;

            $pie2_data2 = array();
            $pie2_data2["value"] = $equityChart[0]->ST;
            
            $pie2_data3 = array();
            $pie2_data3["value"] = $equityChart[0]->OT;
        } else {
             if (($select_month) && ($select_year)) {
                $reference_date2 = getThaiMonth($select_month) .' ปี '  . ($select_year + 543) ;
            }
            else {
                 $reference_date2 = getThaiMonth(intval(date('m')))  . ' ปี ' . (date('Y') + 543) ;
            }

            $pie2_data1["value"] = 0;

            $pie2_data2 = array();
            $pie2_data2["value"] = 0;
            
            $pie2_data3 = array();
            $pie2_data3["value"] = 0;
        }
        array_push($pie2, $pie2_data1, $pie2_data2, $pie2_data3);

        
        /* create Excel file for download */ 
        return  Excel::create('ExcelExport', function ($excel) use ($bondRS, $equityRS, $ref_date1, $ref_date2, $pie1, $pie2, $ArrParam) {

            /**
             * Sheet ตารางตราสารหนี้
             */
            $excel->sheet('ตารางตราสารหนี้', function ($sheet) use ($bondRS, $ref_date1, $ref_date2, $pie1, $ArrParam) {

                $sheet->setStyle([
                    'borders' => [
                        'allborders' => [
                            'color' => [
                                'rgb' => 'b3b6b7'
                            ]
                        ]
                    ]
                ]);

                $sheet->setFontFamily('Cordia New'); 
                $sheet->setFontSize(18);
                // first row styling and writing content
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:E2');
                $sheet->mergeCells('A3:E3');
                 
                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Cordia New');
                    $row->setFontSize(20);
                    $row->setFontWeight('bold');
                    $row->setAlignment('center');
                    $row->setBackground('#b2babb');
                    // Set all borders (top, right, bottom, left)
                    $row->setBorder('thin', 'thin', 'none', 'thin');

                });

                $sheet->row(2, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'none', 'thin');
                });

                $sheet->row(3, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'thin', 'thin');

                });

                $sheet->row(1, array('รายงาน นโยบายการลงทุนตราสารหนี้'));
                $sheet->row(2, array('รายงานข้อมูล ณ. ' . $ref_date1));
                $sheet->row(3, array('มูลค่าทรัพย์สินสุทธิและอัตราตอบแทน'));
                
                $sheet->mergeCells('A4:A5');
                $sheet->mergeCells('B4:C4');
                $sheet->mergeCells('D4:E4');

                $sheet->cell('A4', function($cell) {

                    // manipulate the cell
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                    // Set all borders (top, right, bottom, left)
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                
                });

                $sheet->row(4, function ($row) {
                    $row->setAlignment('center');
                    
                });

                //$sheet->setBorder('solid', 'none', 'none', 'solid');

                $sheet->cell('B4', function($cell) {
                    $cell->setBackground('#f2f3f4');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
                $sheet->cell('C4', function($cell) {
                    $cell->setBackground('#f2f3f4');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
                $sheet->cell('D4', function($cell) {
                    $cell->setBackground('#f2f3f4');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
                $sheet->cell('E4', function($cell) {
                    $cell->setBackground('#f2f3f4');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $sheet->cell('A5', function($cell) {
                    
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $header0[] = null;
                $header0[0] = 'เดือน';
                $header0[1] = 'มูลค่าทรัพย์สินสุทธิ';
                $header0[2] = '';
                $header0[3] = 'อัตราผลตอบแทน (%)';
                $header0[4] = '';
                $sheet->row(4, $header0);// array('เดือน', 'มูลค่าทรัพย์สินสุทธิ', 'อัตราตอบแทน'));
               
                $sheet->row(5, function ($row) {$row->setAlignment('center');});
                $header[] = null;
                $header[0] = 'เดือน';
                $header[1] = 'ล้านบาท';
                $header[2] = "ต่อหน่วย";
                $header[3] = "รายเดือน";
                $header[4] = "สะสม";
              
                $sheet->row(5, $header);

                // Set multiple column formats
                $sheet->setColumnFormat(array(
                   // 'A' => 'yyyy-mm-dd',
                    'B' => '#,##0.00',
                    'C' => '#,##0.0000',
                    'D' => '#,##0.00',
                    'E' => '0.00',
                ));

               // {{ toThaiShortMonth($rs->item)}} {{$rs->yyyy}}
                foreach ($bondRS as $rows => $rs) {

                    $fields  = [];
                    $fields[0] = toThaiShortMonth($rs->item) . ' ' . $rs->yyyy;
                    $fields[1] = round($rs->MB, 2);
                    $fields[2] = round($rs->NU, 4);
                    $fields[3] = round($rs->YM, 2);
                    $fields[4] = round($rs->YC, 2);
                     
                    $sheet->appendRow($fields);
                }
                 for($i = 5 ; $i< 18; $i++) { 
                     
                    $sheet->setSize('A' . $i, 10, 18);
                    $sheet->setSize('B' . $i, 10, 18);
                    $sheet->setSize('C' . $i, 10, 18);
                    $sheet->setSize('D' . $i, 10, 18);
                    $sheet->setSize('E' . $i, 10, 18);
                    
                    $sheet->cell('A' . $i, function($cell) {
                       $cell->setAlignment('center');
                       
                    });

                    $sheet->row($i, function ($row) {
                        $row->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                }
                // ==================================================================//
                // PIE Chart ตราสารหนี้
                $ls = array('พันธบัตรรัฐบาล ธปท.และรัฐวิสาหกิจ',
                                'เงินฝากและตราสารหนี้ธนาคารพาณิชย์',
                                'หุ้นกู้',
                                'อื่นๆ ลูกหนี้และเจ้าหนี้จากการซื้อ/ ขายหลักทรัพย์');

                $sheet->mergeCells('G4:H4');
                $sheet->setSize('G6', 40, 18);
                $sheet->cell('G4', function($cell) {
                    $cell->setValue('การลงทุนในตราสารทุน');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#b2babb');
                    $cell->setFontWeight('bold');
                    $cell->setFontSize(20);
                });
                $sheet->cell('H4', function($cell) {
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                $sheet->cell('G5', function($cell) {
                    $cell->setValue('ประเภท');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                $sheet->setSize('H5', 15, 18);
                $sheet->cell('H5', function($cell) {
                    $cell->setValue('อัตราร้อยละ (%)');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                for($i = 0; $i < count($ls); $i++) {
                    $sheet->cell('G'. ($i + 6), function($cell) use($ls, $i) {
                        $cell->setValue($ls[$i]);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell('H'. ($i + 6), function($cell) use($ls, $i, $pie1) {
                        $cell->setValue( $pie1[$i]['value']);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                }



            });

            /**
             * Sheet ตารางตราสารทุน
             */
            $excel->sheet('ตารางตราสารทุน', function ($sheet) use ($equityRS, $ref_date1, $ref_date2, $pie2, $ArrParam) {
                $sheet->setStyle([
                    'borders' => [
                        'allborders' => [
                            'color' => [
                                'rgb' => 'b3b6b7'
                            ]
                        ]
                    ]
                ]);

                $sheet->setFontFamily('Cordia New'); 
                $sheet->setFontSize(18);
                // first row styling and writing content
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:E2');
                $sheet->mergeCells('A3:E3');
                 
                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Cordia New');
                    $row->setFontSize(20);
                    $row->setFontWeight('bold');
                    $row->setAlignment('center');
                    $row->setBackground('#b2babb');
                    // Set all borders (top, right, bottom, left)
                    $row->setBorder('thin', 'thin', 'none', 'thin');
                });

                $sheet->row(2, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'none', 'thin');
                });

                $sheet->row(3, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'thin', 'thin');

                });

                $sheet->row(1, array('รายงาน นโยบายการลงทุนตราสารทุน'));
                $sheet->row(2, array('รายงานข้อมูล ณ. ' . $ref_date1));
                $sheet->row(3, array('มูลค่าทรัพย์สินสุทธิและอัตราตอบแทน'));
                
                $sheet->mergeCells('A4:A5');
                $sheet->mergeCells('B4:C4');
                $sheet->mergeCells('D4:E4');

                $sheet->cell('A4', function($cell) {

                    // manipulate the cell
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                    // Set all borders (top, right, bottom, left)
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                
                });

                $sheet->row(4, function ($row) {
                    $row->setAlignment('center');
                    
                });

                //$sheet->setBorder('solid', 'none', 'none', 'solid');

                $sheet->cell('B4', function($cell) {
                    $cell->setBackground('#f2f3f4');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
                $sheet->cell('C4', function($cell) {
                    $cell->setBackground('#f2f3f4');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
                $sheet->cell('D4', function($cell) {
                    $cell->setBackground('#f2f3f4');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
                $sheet->cell('E4', function($cell) {
                    $cell->setBackground('#f2f3f4');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $sheet->cell('A5', function($cell) {
                    
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $header0[] = null;
                $header0[0] = 'เดือน';
                $header0[1] = 'มูลค่าทรัพย์สินสุทธิ';
                $header0[2] = '';
                $header0[3] = 'อัตราผลตอบแทน (%)';
                $header0[4] = '';
                $sheet->row(4, $header0);
               
                $sheet->row(5, function ($row) {$row->setAlignment('center');});
                $header[] = null;
                $header[0] = 'เดือน';
                $header[1] = 'ล้านบาท';
                $header[2] = "ต่อหน่วย";
                $header[3] = "รายเดือน";
                $header[4] = "สะสม";
              
                $sheet->row(5, $header);

                // Set multiple column formats
                $sheet->setColumnFormat(array(
                   // 'A' => 'yyyy-mm-dd',
                    'B' => '#,##0.00',
                    'C' => '#,##0.0000',
                    'D' => '#,##0.00',
                    'E' => '0.00',
                    'H' => '0.00'
                ));

               // {{ toThaiShortMonth($rs->item)}} {{$rs->yyyy}}
                foreach ($equityRS as $rows => $rs) {

                    $fields  = [];
                    $fields[0] = toThaiShortMonth($rs->item) . ' ' . $rs->yyyy;
                    $fields[1] = round($rs->MB, 2);
                    $fields[2] = round($rs->NU, 4);
                    $fields[3] = round($rs->YM, 2);
                    $fields[4] = round($rs->YC, 2);
                     
                    $sheet->appendRow($fields);
                }
                 for($i = 5 ; $i< 18; $i++) { 
                     
                    $sheet->setSize('A' . $i, 10, 18);
                    $sheet->setSize('B' . $i, 10, 18);
                    $sheet->setSize('C' . $i, 10, 18);
                    $sheet->setSize('D' . $i, 10, 18);
                    $sheet->setSize('E' . $i, 10, 18);
                    
                    $sheet->cell('A' . $i, function($cell) {
                       $cell->setAlignment('center');
                       
                    });

                    $sheet->row($i, function ($row) {
                        $row->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                }

                // ==================================================================//
                // PIE chart ตาสารทุน
                $ls = array( 'เงินฝากธนาคารพาณิชย์',//'พันธบัตรรัฐบาล ธปท.และรัฐวิสาหกิจ',
                             'ตราสารทุน(หุ้น)',
                             'อื่นๆ ลูกหนี้และเจ้าหนี้จากการซื้อ/ ขายหลักทรัพย์');
                $sheet->mergeCells('G4:H4');
                $sheet->setSize('G6', 40, 18);
                $sheet->cell('G4', function($cell) {
                    $cell->setValue('การลงทุนในตราสารทุน');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#b2babb');
                    $cell->setFontWeight('bold');
                    $cell->setFontSize(20);
                });
                $sheet->cell('H4', function($cell) {
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                $sheet->cell('G5', function($cell) {
                    $cell->setValue('ประเภท');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                $sheet->setSize('H5', 15, 18);
                $sheet->cell('H5', function($cell) {
                    $cell->setValue('อัตราร้อยละ (%)');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                for($i = 0; $i < count($ls); $i++) {
                    $sheet->cell('G'. ($i + 6), function($cell) use($ls, $i) {
                        $cell->setValue($ls[$i]);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell('H'. ($i + 6), function($cell) use($ls, $i, $pie2) {
                        $cell->setValue( $pie2[$i]['value']);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                }


            });

        })->download('xls');
    }

     /**
     * Details Report  & Export
     */
    public function detailsReportDatasource($tbl, $policy, $selected_month, $selected_year) {
        $current_year = date("Y");
        $current_month = date("n");
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $where = '';

        $query = "";
        if($tbl == "TBL_P2_BOND_NAV") {
            $query = " SELECT " . 
                     " year(REFERENCE_DATE)+543 as YYYY , " .   
                     "    REFERENCE_DATE, " .
                     "    day(REFERENCE_DATE) as DD, ".
                     "    month(REFERENCE_DATE) as MM, " .
                     "    month(REFERENCE_DATE) as item,  " .
                     "    datename(month, REFERENCE_DATE) [month], " .
                     "        GOV_BOND_RATIO , " .
                     "        GOV_BOND_MB, " .
                     "        GOV_BOND_B," .
                     "        DEPOSIT_RATIO," .
                     "        DEPOSIT_MB," .
                     "        DEPOSIT_B," .
                     "        DEBENTURE_RATIO," .
                     "        DEBENTURE_MB," .
                     "        DEBENTURE_B," .
                     "        OTHER_RATIO," .
                     "        OTHER_MB," .
                     "        OTHER_B," .
                     "        (GOV_BOND_RATIO + DEPOSIT_RATIO + DEBENTURE_RATIO + OTHER_RATIO) as SUM_RATIO, " .
                     "        (GOV_BOND_MB + DEPOSIT_MB + DEBENTURE_MB + OTHER_MB) as SUM_MB, " . 
                     "        (GOV_BOND_B + DEPOSIT_B + DEBENTURE_B + OTHER_B) AS SUM_B, " .
                     "        NAV_MB," .
                     "        NAV_B , ".
                     "        NAV_UNIT, ".
                     "        YIELD_MONTH as YM,".
                     "        YIELD_CUMULATIVE as YC".
                     " FROM " . 
                     "      " . "TBL_P2_BOND_NAV" . " " .
                     " WHERE  " .
                     "      month(REFERENCE_DATE) = " . $selected_month . " " .
                     "      AND year(REFERENCE_DATE) = " .  $selected_year;
        } else {
            $query = " SELECT " .
                    " year(REFERENCE_DATE)+543 as YYYY , " .
                    "    REFERENCE_DATE, " .
                    "    day(REFERENCE_DATE) as DD, ".
                    "    month(REFERENCE_DATE) as MM, " .
                    "    DEPOSIT_RATIO, " .
                    "    DEPOSIT_MB, " .
                    "    DEPOSIT_B, " .
                    "    STOCK_RATIO, " .
                    "    STOCK_MB, " .
                    "    STOCK_B, " .
                    "    OTHER_RATIO, " .
                    "    OTHER_MB, " .
                    "    OTHER_B, " .
                    "     ".
                    "    (DEPOSIT_RATIO + STOCK_RATIO + OTHER_RATIO) as SUM_RATIO, ".
                    "    (DEPOSIT_MB + STOCK_MB + OTHER_MB) as SUM_MB, " .
                    "    (DEPOSIT_B + STOCK_B + OTHER_B) as SUM_B, " .
                    "    NAV_MB, " .
                    "    NAV_B, " .
                    "    NAV_UNIT, " .
                    "    YIELD_MONTH AS YM," .
                    "    YIELD_CUMULATIVE as YC".
                    " FROM " . 
                    "      " . "TBL_P2_EQUITY_NAV" . " " .
                    " WHERE  " . 
                    "      month(REFERENCE_DATE) = " . $selected_month . " " .
                    "      AND year(REFERENCE_DATE) = " .  $selected_year;
        }
        Log::info(get_class($this) .'::'. __FUNCTION__  . ' SQLSTR=' . $query);

        $resultset = DB::select(DB::raw($query));
        return $resultset;  
    }

    /**
     * Ajax search and generate details report
     *
     * @return json with 
     * @comment This method filled data into 'ajax_p2_equity_investment_report_details.php'
     */
    public function ajax_report_details_search() {

        $view_name    = 'backend.pages.ajax.ajax_p2_equity_nav_report_details';
        /*$plan         = Input::get('plan');
        $select_month = Input::get('select_month');
        $select_year  = Input::get('select_yearß');
        */
        $plan         = Input::get('plan');
        $select_month = Input::get('month');
        $select_year  = Input::get('year');
        

        Log::info(get_class($this) .'::'. __FUNCTION__ . ' \n' . ' plan= ' . $plan . '\n month=' .  $select_month . '\n year=' . $select_year);
        // HTML Render Method
        
        /*
        $UNSPECIFIED                      = 0; // 0 - display TABLE current year and PIE last month 
        $SPECIFIED_YEAR_AND_LAST_MONTH    = 1; // 1 - display TABLE selected year and PIE last month
        $CURRENT_YEAR_AND_SPECIFIED_MONTH = 2; // 2 - display TABLE /PIE with selected month 
        $SPECIFIED_YEAR_AND_MONTH         = 3; // 3 - display PIE chart upon selected year and month
        */

        $method      = self::UNSPECIFIED;
        $table1_show = "display:none;";
        $table2_show = "display:none;";

        if (($select_month == '') && ($select_year=='')) {
           $method = self::UNSPECIFIED;
        
        } else if(($select_month != '') && ($select_year != '')) {
           $method = self::SPECIFIED_YEAR_AND_MONTH;
        
        } else if(($select_month != '') && ($select_year == '')) {
           $method = self::CURRENT_YEAR_AND_SPECIFIED_MONTH;

        } else if(($select_month == '') && ($select_year != '')) {
           $method = self::SPECIFIED_YEAR_AND_LAST_MONTH;
        }

        $table1_show = 'display:none;';
        $table2_show = 'display:none;';
        $tbl1 = 'TBL_P2_BOND_NAV';
        $tbl2 = 'TBL_P2_EQUITY_NAV';

        if($plan =='ตราสารหนี้') {
            $table3_show = 'display:block;';
            $table4_show = 'display:none;';
             
        } else if ($plan =='ตราสารทุน') {
            $table4_show = 'display:block;';
            $table3_show = 'display:none;';
             
        } else {
            $table3_show = 'display:block;';
            $table4_show = 'display:block;';
        }

        
        $bondRS   = $this->detailsReportDatasource('TBL_P2_BOND_NAV', $plan, $select_month, $select_year);
        $equityRS = $this->detailsReportDatasource('TBL_P2_EQUITY_NAV', $plan, $select_month, $select_year);
        
        $bondChart   = $this->bondChartDatasource($plan, $select_month, $select_year);
        $equityChart = $this->equityChartDatasource($plan, $select_month, $select_year);

        Log::info(get_class($this) . '::' . __FUNCTION__ . ':: plan = ' . $plan . ', method = ' . $method . ', select_month = ' . $select_month . ', select_year = '. $select_year);
        $returnHTML = view($view_name)->with([
            'htmlPaginate' => '',
            'data' => array('bond_nav' => $bondRS ,'equity_nav' =>$equityRS),
            'totals' => 1,
            'PageSize' => 1,
            'PageNumber' => 1,
            'plan' => $plan,
            'renderMethod' => $method,
            'table3show' => $table3_show,
            'table4show' => $table4_show,
            'selected_year' => $select_year,
            'selected_month' => ($select_month ? $select_month: 0)
        ])->render();

    
        $pie1 = array(); 
        $pie1_data1 = array();
        $reference_date1 = date('Y-m-d');
        $reference_date2 = date('Y-m-d');

        if ($bondChart && (count($bondChart) > 0 )) {
            $reference_date1 =  getThaiMonth(intval(date('m', strtotime($bondChart[0]->REF_DATE)))) .' ปี '  . (date('Y',strtotime($bondChart[0]->REF_DATE)) +  543);
            $pie1_data1["value"] =  $bondChart[0]->GR;

            $pie1_data2 = array();
            $pie1_data2["value"] =  $bondChart[0]->DR;
            
            $pie1_data3 = array();
            $pie1_data3["value"] =  $bondChart[0]->DE;

            $pie1_data4 = array();
            $pie1_data4["value"] =  $bondChart[0]->OT;
        } else {
            if (($select_month) && ($select_year)) {
                $reference_date1 = getThaiMonth($select_month) .' ปี '  . ($select_year + 543) ;
            }
            else {
                 $reference_date1 = getThaiMonth(intval(date('m'))) . ' ปี ' . (date('Y') + 543);
            }
            $pie1_data1["value"] =  0;

            $pie1_data2 = array();
            $pie1_data2["value"] =  0;
            
            $pie1_data3 = array();
            $pie1_data3["value"] =  0;

            $pie1_data4 = array();
            $pie1_data4["value"] =  0;
        }


        array_push($pie1, $pie1_data1, $pie1_data2, $pie1_data3, $pie1_data4);

        $pie2 = array(); 
        $pie2_data1 = array();
        
        if ($equityChart && (count($equityChart) > 0 )) {
            //$reference_date2 = date('Y-m-d', strtotime($equityChart[0]->REF_DATE));
            $reference_date2 =  getThaiMonth(intval(date('m', strtotime($equityChart[0]->REF_DATE)))) .' ปี '  . (date('Y',strtotime($equityChart[0]->REF_DATE)) +  543);
           
            $pie2_data1["value"] = $equityChart[0]->DR;

            $pie2_data2 = array();
            $pie2_data2["value"] = $equityChart[0]->ST;
            
            $pie2_data3 = array();
            $pie2_data3["value"] = $equityChart[0]->OT;
        } else {
             if (($select_month) && ($select_year)) {
                $reference_date2 = getThaiMonth($select_month) .' ปี '  . ($select_year + 543) ;
            }
            else {
                 $reference_date2 = getThaiMonth(intval(date('m')))  . ' ปี ' . (date('Y') + 543) ;
            }

            $pie2_data1["value"] = 0;

            $pie2_data2 = array();
            $pie2_data2["value"] = 0;
            
            $pie2_data3 = array();
            $pie2_data3["value"] = 0;
        }
        array_push($pie2, $pie2_data1, $pie2_data2, $pie2_data3);

        return response()->json(array('success' => true,
                                      'html' => $returnHTML, 
                                      'pie1' => $pie1,
                                      'pie2' => $pie2,
                                      'plan' => $plan,
                                      'table3show' => $table1_show,
                                      'table4show' => $table2_show,
                                      'reference_date1' => $reference_date1, // ref date for pie1
                                      'reference_date2' => $reference_date2, // ref date for pie2
                                      'selected_year' => $select_year,
                                      'selected_month' => ($select_month ? $select_month: 0),
                                      'renderMethod' => $method));


    } // 

    // Export details to file
    /**
     * Ajax search and export details report
     *
     * @return json with 
     * @comment This method filled data into 'ajax_p2_equity_investment_report_details.php'
     */
    public function ajax_report_search_export_details() {

        $view_name    = 'backend.pages.ajax.ajax_p2_equity_nav_report_details';
        $plan         = Input::get('plan');
        $select_month = Input::get('select_month');
        $select_year  = Input::get('select_year');
        
        Log::info(get_class($this) .'::'. __FUNCTION__ . ' \n' . ' plan= ' . $plan . '\n month=' .  $select_month . '\n year=' . $select_year);
        
        /*
        $UNSPECIFIED                      = 0; // 0 - display TABLE current year and PIE last month 
        $SPECIFIED_YEAR_AND_LAST_MONTH    = 1; // 1 - display TABLE selected year and PIE last month
        $CURRENT_YEAR_AND_SPECIFIED_MONTH = 2; // 2 - display TABLE /PIE with selected month 
        $SPECIFIED_YEAR_AND_MONTH         = 3; // 3 - display PIE chart upon selected year and month
        */

        $method      = self::UNSPECIFIED;
        $table1_show = "display:none;";
        $table2_show = "display:none;";

        if (($select_month == '') && ($select_year=='')) {
           $method = self::UNSPECIFIED;
        
        } else if(($select_month != '') && ($select_year != '')) {
           $method = self::SPECIFIED_YEAR_AND_MONTH;
        
        } else if(($select_month != '') && ($select_year == '')) {
           $method = self::CURRENT_YEAR_AND_SPECIFIED_MONTH;

        } else if(($select_month == '') && ($select_year != '')) {
           $method = self::SPECIFIED_YEAR_AND_LAST_MONTH;
        }

        $table1_show = 'display:none;';
        $table2_show = 'display:none;';
        $tbl1 = 'TBL_P2_BOND_NAV';
        $tbl2 = 'TBL_P2_EQUITY_NAV';

        if($plan =='ตราสารหนี้') {
            $table3_show = 'display:block;';
            $table4_show = 'display:none;';
             
        } else if ($plan =='ตราสารทุน') {
            $table4_show = 'display:block;';
            $table3_show = 'display:none;';
             
        } else {
            $table3_show = 'display:block;';
            $table4_show = 'display:block;';
        }

        Log::info(get_class($this) . '::' . __FUNCTION__ . ':: plan = ' . $plan . ', method = ' . $method . ', select_month = ' . $select_month . ', select_year = '. $select_year);
      
        $bondRS   = $this->detailsReportDatasource('TBL_P2_BOND_NAV', 'ตราสารหนี้', $select_month, $select_year);
        $equityRS = $this->detailsReportDatasource('TBL_P2_EQUITY_NAV', 'ตราสารทุน', $select_month, $select_year);
        

        $bondChart   = $this->bondChartDatasource('ตราสารหนี้', $select_month, $select_year);
        $equityChart = $this->equityChartDatasource('ตราสารทุน', $select_month, $select_year);

         $returnHTML = view($view_name)->with([
            'htmlPaginate' => '',
            'data' => array('bond_nav' => $bondRS ,'equity_nav' =>$equityRS),
            'totals' => 1,
            'PageSize' => 1,
            'PageNumber' => 1,
            'plan' => $plan,
            'renderMethod' => $method,
            'table3show' => $table3_show,
            'table4show' => $table4_show,
            'selected_year' => $select_year,
            'selected_month' => ($select_month ? $select_month: 0)
        ])->render();

    
        $pie1 = array(); 
        $pie1_data1 = array();
        $reference_date1 = date('Y-m-d');
        $reference_date2 = date('Y-m-d');

        if ($bondChart && (count($bondChart) > 0 )) {
            $reference_date1 =  getThaiMonth(intval(date('m', strtotime($bondChart[0]->REF_DATE)))) .' ปี '  . (date('Y',strtotime($bondChart[0]->REF_DATE)) +  543);
            $pie1_data1["value"] =  $bondChart[0]->GR;

            $pie1_data2 = array();
            $pie1_data2["value"] =  $bondChart[0]->DR;
            
            $pie1_data3 = array();
            $pie1_data3["value"] =  $bondChart[0]->DE;

            $pie1_data4 = array();
            $pie1_data4["value"] =  $bondChart[0]->OT;
        } else {
            if (($select_month) && ($select_year)) {
                $reference_date1 = getThaiMonth($select_month) .' ปี '  . ($select_year + 543) ;
            }
            else {
                 $reference_date1 = getThaiMonth(intval(date('m'))) . ' ปี ' . (date('Y') + 543);
            }
            $pie1_data1["value"] =  0;

            $pie1_data2 = array();
            $pie1_data2["value"] =  0;
            
            $pie1_data3 = array();
            $pie1_data3["value"] =  0;

            $pie1_data4 = array();
            $pie1_data4["value"] =  0;
        }


        array_push($pie1, $pie1_data1, $pie1_data2, $pie1_data3, $pie1_data4);

        $pie2 = array(); 
        $pie2_data1 = array();
        
        if ($equityChart && (count($equityChart) > 0 )) {
            //$reference_date2 = date('Y-m-d', strtotime($equityChart[0]->REF_DATE));
            $reference_date2 =  getThaiMonth(intval(date('m', strtotime($equityChart[0]->REF_DATE)))) .' ปี '  . (date('Y',strtotime($equityChart[0]->REF_DATE)) +  543);
           
            $pie2_data1["value"] = $equityChart[0]->DR;

            $pie2_data2 = array();
            $pie2_data2["value"] = $equityChart[0]->ST;
            
            $pie2_data3 = array();
            $pie2_data3["value"] = $equityChart[0]->OT;
        } else {
             if (($select_month) && ($select_year)) {
                $reference_date2 = getThaiMonth($select_month) .' ปี '  . ($select_year + 543) ;
            }
            else {
                 $reference_date2 = getThaiMonth(intval(date('m')))  . ' ปี ' . (date('Y') + 543) ;
            }

            $pie2_data1["value"] = 0;

            $pie2_data2 = array();
            $pie2_data2["value"] = 0;
            
            $pie2_data3 = array();
            $pie2_data3["value"] = 0;
        }
        array_push($pie2, $pie2_data1, $pie2_data2, $pie2_data3);

        $results = array();
        $ref_date1 = '';
        $ref_date2 = '';
       
        $ref_date1 =  $reference_date1;  
        $ref_date2 =  $reference_date2;  
         
        /* create Excel file for download */ 
        return  Excel::create('นโยบายการลงทุน' . $plan, function ($excel) use ($plan, $bondRS, $equityRS, $ref_date1, $ref_date2, $pie1, $pie2) {
        
            if($plan == 'ตราสารหนี้') {
               $excel->sheet('นโยบายการลงทุน' . $plan, function ($sheet) use ($bondRS, $ref_date1, $ref_date2, $pie1) {

                    $sheet->setStyle([
                        'borders' => [
                            'allborders' => [
                                'color' => [
                                    'rgb' => 'b3b6b7'
                                ]
                            ]
                        ]
                    ]);

                $sheet->setFontFamily('Cordia New'); 
                $sheet->setFontSize(18);
                // first row styling and writing content
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
               
                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Cordia New');
                    $row->setFontSize(20);
                    $row->setFontWeight('bold');
                    $row->setAlignment('center');
                    $row->setBackground('#b2babb');
                    $row->setBorder('thin', 'thin', 'none', 'thin');
                });

                $sheet->row(2, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'none', 'thin');
                });

                foreach ($bondRS as $rowset => $rs) {
                    $datadate = intval($rs->DD) . " " . toThaiShortMonth($rs->MM) . " " . $rs->YYYY;
                    $sheet->row(1, array('การลงทุนตราสารหนี้'));
                    $sheet->row(2, array('การลงทุน ณ. ' . $datadate));
                    
                    $sheet->mergeCells('A3:A4');
                    $sheet->mergeCells('B3:D3');

                    $header0[ ] = null;
                    $header0[0] = "ประเภทตราสาร";
                    $header0[1] = "นโยบายการลงทุนตราสารหนี้";
                    $header0[2] = "";
                    $header0[3] = "";                     
                    $sheet->row(3, $header0);

                    $sheet->row(4, function ($row) { $row->setAlignment('center'); });
                    $header[] = null;
                    $header[0] = "";
                    $header[1] = "สัดส่วน (%)";
                    $header[2] = "ล้าน";
                    $header[3] = "  บาท  ";
                    $sheet->row(4, $header);

                    // Set multiple column formats
                    $sheet->setColumnFormat(array(
                        'A' => '@',
                        'B' => '0.00',
                        'C' => '#,##0.00',
                        'D' => '#,##0.00'
                    ));

                    // Set width for multiple cells
                    $sheet->setWidth(array(
                        'A'  => 40,
                        'B'  => 15,
                        'C'  => 15,
                        'D'  => 15,
                        'E'  => 5,
                        'F'  => 5
                    ));

                    $fields  = [];
                    $fields[0] = "พันธบัตรรัฐบาล ธปท.และรัฐวิสาหกิจ";
                    $fields[1] = $rs->GOV_BOND_RATIO;
                    $fields[2] = $rs->GOV_BOND_MB;
                    $fields[3] = $rs->GOV_BOND_B;
                    $sheet->appendRow($fields);
                    $sheet->row(5, function ($row) {
                        $row->setAlignment('right');
                    });
                    
                    $fields[0] = "เงินฝากตราสารหุ้นธนาคารพาณิชย์";
                    $fields[1] = $rs->DEPOSIT_RATIO;
                    $fields[2] = $rs->DEPOSIT_MB;
                    $fields[3] = $rs->DEPOSIT_B;
                    $sheet->appendRow($fields);
                    $sheet->row(6, function ($row) {
                        $row->setAlignment('right');
                    });
                    
                    $fields[0] = "หุ้นกู้";
                    $fields[1] = $rs->DEBENTURE_RATIO;
                    $fields[2] = $rs->DEBENTURE_MB;
                    $fields[3] = $rs->DEBENTURE_B;
                    $sheet->appendRow($fields);
                    $sheet->row(7, function ($row) {
                        $row->setAlignment('right');
                    });

                    $fields[0] = "อื่นๆ (ลูกหนี้และเจ้าหนี้จากการซื้อ-ขายหลักทรัพย์)";
                    $fields[1] = $rs->OTHER_RATIO;
                    $fields[2] = $rs->OTHER_MB;
                    $fields[3] = $rs->OTHER_B;
                    $sheet->appendRow($fields);
                    $sheet->row(8, function ($row) {
                        $row->setAlignment('right');
                    });

                    
                    $fields[0] = "ได้แก่ลูกหนี้จากการซื้อ / ขายหลักทรัพย์ / เงินปันผลลูกหนี้ / เจ้าหนี้อื่นๆ และเงินสมทบรอการจัดสรร";
                    $fields[1] = "";
                    $fields[2] = "";
                    $fields[3] = "";
                    

                    $sheet->appendRow($fields);
                     $sheet->setColumnFormat(array(
                        'A' => '@',
                        'B' => '@',
                        'C' => '@',
                        'D' => '@'
                    )); 
                    $sheet->row(9, function ($row) {
                        $row->setFontFamily('Cordia New');
                        $row->setFontSize(12);
                        $row->setFontWeight('italic');
                        $row->setAlignment('left');
                        $row->setFontColor('#a5a5a5');
                        //$row->setBorder('thin', 'thin', 'none', 'thin');
                    });

                        
                    $sheet->setColumnFormat(array(
                        'A' => '@', 
                        'B' => '0.00',
                        'C' => '#,##0.00',
                        'D' => '#,##0.00'
                    ));

                    $fields[0] = "รวม";
                    $fields[1] = $rs->SUM_RATIO;
                    $fields[2] = $rs->SUM_MB;
                    $fields[3] = $rs->SUM_B;
                    $sheet->appendRow($fields);
                    $sheet->row(10, function ($row) {
                        $row->setAlignment('right');
                    });  
                    
                    $fields[0] = "มูลค่าสินทรัพย์สุทธิ";
                    $fields[1] = "";
                    $fields[2] = $rs->NAV_MB;
                    $fields[3] = $rs->NAV_B;
                    $sheet->appendRow($fields);
                    $sheet->row(11, function ($row) {
                        $row->setAlignment('right');
                    });  


                    $fields[0] = "มูลค่าสินทรัพย์สุทธิต่อหน่วย";
                    $fields[1] = "";
                    $fields[2] = "";
                    $fields[3] = $rs->NAV_UNIT; 
                    $sheet->appendRow($fields);
                    $sheet->row(12, function ($row) {
                        $row->setAlignment('right');
                    });  
                    


                    $fields[0] = "อัตราผลตอบแทนรายเดือน %";
                    $fields[1] = "";
                    $fields[2] = "";
                    $fields[3] = $rs->YM;
                    $sheet->appendRow($fields);
                    $sheet->row(13, function ($row) {
                        $row->setAlignment('right');
                    });  


                    $fields[0] = "อัตราผลตอบแทนสะสม %";
                    $fields[1] = "";
                    $fields[2] = "";
                    $fields[3] = $rs->YC;
                    $sheet->appendRow($fields);
                    $sheet->row(14, function ($row) {
                        $row->setAlignment('right');
                    });  


                



                    // needed only one record
                    break;
                }


                $sheet->cell('A3', function($cell) {
                         
                        $cell->setAlignment('center');
                    
                });
                $sheet->cell('B3', function($cell) {
                         
                        $cell->setAlignment('center');
                    
                });
                for($i = 5; $i < 10; $i++) { 
                    $sheet->cell('A' . $i, function($cell) {
                         
                        $cell->setAlignment('left');

                    });
                }

                for($i = 10; $i < 15; $i++) { 
                    $sheet->cell('A' . $i, function($cell) {
                         
                        $cell->setAlignment('center');

                    });
                }
                /////////////////////////
                // ==================================================================//
                // PIE Chart ตราสารหนี้
                $ls = array('พันธบัตรรัฐบาล ธปท.และรัฐวิสาหกิจ',
                                'เงินฝากและตราสารหนี้ธนาคารพาณิชย์',
                                'หุ้นกู้',
                                'อื่นๆ ลูกหนี้และเจ้าหนี้จากการซื้อ/ ขายหลักทรัพย์');

                $sheet->mergeCells('G4:H4');
                $sheet->setSize('G6', 40, 18);
                $sheet->cell('G4', function($cell) {
                    $cell->setValue('การลงทุนในตราสารหนี้');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#b2babb');
                    $cell->setFontWeight('bold');
                    $cell->setFontSize(20);
                });
                $sheet->cell('H4', function($cell) {
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                $sheet->cell('G5', function($cell) {
                    $cell->setValue('ประเภท');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                $sheet->setSize('H5', 15, 18);
                $sheet->cell('H5', function($cell) {
                    $cell->setValue('อัตราร้อยละ (%)');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                for($i = 0; $i < count($ls); $i++) {
                    $sheet->cell('G'. ($i + 6), function($cell) use($ls, $i) {
                        $cell->setValue($ls[$i]);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell('H'. ($i + 6), function($cell) use($ls, $i, $pie1) {
                        $cell->setValue( $pie1[$i]['value']);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                }

               });
            } // end if 'ตราสารหนี้'


            if($plan == 'ตราสารทุน') {  
                $excel->sheet('นโยบายการลงทุน'. $plan, function ($sheet) use ($equityRS, $ref_date1, $ref_date2, $pie2) {
                    $sheet->setStyle([
                        'borders' => [
                            'allborders' => [
                                'color' => [
                                    'rgb' => 'b3b6b7'
                                ]
                            ]
                        ]
                    ]);

                $sheet->setFontFamily('Cordia New'); 
                $sheet->setFontSize(18);

                
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
               
                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Cordia New');
                    $row->setFontSize(20);
                    $row->setFontWeight('bold');
                    $row->setAlignment('center');
                    $row->setBackground('#b2babb');
                    $row->setBorder('thin', 'thin', 'none', 'thin');
                });

                $sheet->row(2, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'none', 'thin');
                });

                foreach ($equityRS as $rowset => $rs) {
                    $datadate = intval($rs->DD) . " " . toThaiShortMonth($rs->MM) . " " . $rs->YYYY;
                    $sheet->row(1, array('การลงทุนตราสารทุน'));
                    $sheet->row(2, array('การลงทุน ณ. ' . $datadate));
                    
                    $sheet->mergeCells('A3:A4');
                    $sheet->mergeCells('B3:D3');

                    $header0[ ] = null;
                    $header0[0] = "ประเภทตราสาร";
                    $header0[1] = "นโยบายการลงทุนตราสารทุน";
                    $header0[2] = "";
                    $header0[3] = "";                     
                    $sheet->row(3, $header0);

                    $sheet->row(4, function ($row) { $row->setAlignment('center'); });
                    $header[] = null;
                    $header[0] = "";
                    $header[1] = "สัดส่วน (%)";
                    $header[2] = "ล้าน";
                    $header[3] = "  บาท  ";
                    $sheet->row(4, $header);

                     
                    $sheet->setColumnFormat(array(
                        'A' => '@',
                        'B' => '0.00',
                        'C' => '#,##0.00',
                        'D' => '#,##0.00'
                    ));

                     
                    $sheet->setWidth(array(
                        'A'  => 40,
                        'B'  => 15,
                        'C'  => 15,
                        'D'  => 15,
                        'E'  => 5,
                        'F'  => 5
                    ));

                    
                    
                    $fields[0] = "เงินฝากธนาคารพาณิชย์ ";
                    $fields[1] = $rs->DEPOSIT_RATIO;
                    $fields[2] = $rs->DEPOSIT_MB;
                    $fields[3] = $rs->DEPOSIT_B;
                    $sheet->appendRow($fields);
                    $sheet->row(5, function ($row) {
                        $row->setAlignment('right');
                    });
                    
                    $fields[0] = "ตราสารทุน(หุ้น)";
                    $fields[1] = $rs->STOCK_RATIO;
                    $fields[2] = $rs->STOCK_MB;
                    $fields[3] = $rs->STOCK_B;
                    $sheet->appendRow($fields);
                    $sheet->row(6, function ($row) {
                        $row->setAlignment('right');
                    });

                    $fields[0] = "อื่นๆ (ลูกหนี้และเจ้าหนี้จากการซื้อ-ขายหลักทรัพย์)";
                    $fields[1] = $rs->OTHER_RATIO;
                    $fields[2] = $rs->OTHER_MB;
                    $fields[3] = $rs->OTHER_B;
                    $sheet->appendRow($fields);
                    $sheet->row(7, function ($row) {
                        $row->setAlignment('right');
                    });

                    
                    $fields[0] = "ได้แก่ลูกหนี้จากการซื้อ / ขายหลักทรัพย์ / เงินปันผลลูกหนี้ / เจ้าหนี้อื่นๆ และเงินสมทบรอการจัดสรร";
                    $fields[1] = "";
                    $fields[2] = "";
                    $fields[3] = "";
                    

                    $sheet->appendRow($fields);
                     $sheet->setColumnFormat(array(
                        'A' => '@',
                        'B' => '@',
                        'C' => '@',
                        'D' => '@'
                    )); 
                    $sheet->row(8, function ($row) {
                        $row->setFontFamily('Cordia New');
                        $row->setFontSize(12);
                        $row->setFontWeight('italic');
                        $row->setAlignment('left');
                        $row->setFontColor('#a5a5a5');
                        
                    });

                        
                    $sheet->setColumnFormat(array(
                        'A' => '@', 
                        'B' => '0.00',
                        'C' => '#,##0.00',
                        'D' => '#,##0.00'
                    ));

                    $fields[0] = "รวม";
                    $fields[1] = $rs->SUM_RATIO;
                    $fields[2] = $rs->SUM_MB;
                    $fields[3] = $rs->SUM_B;
                    $sheet->appendRow($fields);
                    $sheet->row(9, function ($row) {
                        $row->setAlignment('right');
                    });  
                    
                    $fields[0] = "มูลค่าสินทรัพย์สุทธิ";
                    $fields[1] = "";
                    $fields[2] = $rs->NAV_MB;
                    $fields[3] = $rs->NAV_B;
                    $sheet->appendRow($fields);
                    $sheet->row(10, function ($row) {
                        $row->setAlignment('right');
                    });  


                    $fields[0] = "มูลค่าสินทรัพย์สุทธิต่อหน่วย";
                    $fields[1] = "";
                    $fields[2] = "";
                    $fields[3] = $rs->NAV_UNIT; 
                    $sheet->appendRow($fields);
                    $sheet->row(11, function ($row) {
                        $row->setAlignment('right');
                    });  
                    


                    $fields[0] = "อัตราผลตอบแทนรายเดือน %";
                    $fields[1] = "";
                    $fields[2] = "";
                    $fields[3] = $rs->YM;
                    $sheet->appendRow($fields);
                    $sheet->row(12, function ($row) {
                        $row->setAlignment('right');
                    });  


                    $fields[0] = "อัตราผลตอบแทนสะสม %";
                    $fields[1] = "";
                    $fields[2] = "";
                    $fields[3] = $rs->YC;
                    $sheet->appendRow($fields);
                    $sheet->row(13, function ($row) {
                        $row->setAlignment('right');
                    });  

                    break;
                } // foreach


                $sheet->cell('A3', function($cell) {
                         
                        $cell->setAlignment('center');
                    
                });
                $sheet->cell('B3', function($cell) {
                         
                        $cell->setAlignment('center');
                    
                });
                for($i = 5; $i < 9; $i++) { 
                    $sheet->cell('A' . $i, function($cell) {
                         
                        $cell->setAlignment('left');

                    });
                }

                for($i = 9; $i < 14; $i++) { 
                    $sheet->cell('A' . $i, function($cell) {
                         
                        $cell->setAlignment('center');

                    });
                }
                /////////////////////////
                // ==================================================================
                // PIE chart ตาสารทุน
                $ls = array( 'เงินฝากธนาคารพาณิชย์',//'พันธบัตรรัฐบาล ธปท.และรัฐวิสาหกิจ',
                             'ตราสารทุน(หุ้น)', //เงินฝากและตราสารหนี้ธนาคารพาณิชย์',
                             'อื่นๆ ลูกหนี้และเจ้าหนี้จากการซื้อ/ ขายหลักทรัพย์');
                $sheet->mergeCells('G4:H4');
                $sheet->setSize('G6', 40, 18);
                $sheet->cell('G4', function($cell) {
                    $cell->setValue('การลงทุนในตราสารทุน');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#b2babb');
                    $cell->setFontWeight('bold');
                    $cell->setFontSize(20);
                });
                $sheet->cell('H4', function($cell) {
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                $sheet->cell('G5', function($cell) {
                    $cell->setValue('ประเภท');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                $sheet->setSize('H5', 15, 18);
                $sheet->cell('H5', function($cell) {
                    $cell->setValue('อัตราร้อยละ (%)');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                for($i = 0; $i < count($ls); $i++) {
                    $sheet->cell('G'. ($i + 6), function($cell) use($ls, $i) {
                        $cell->setValue($ls[$i]);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell('H'. ($i + 6), function($cell) use($ls, $i, $pie2) {
                        $cell->setValue( $pie2[$i]['value']);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                }


               });
            } // end if
             
        })->download('xls');
    } // 

} // class AdminP2EquityInvestmentReportController
?>