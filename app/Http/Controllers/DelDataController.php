<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Package\Curl;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class DelDataController extends Controller
{


    public function getIndex(Request $r)
    {
        if(isset($r->del)){
            DB::table($r->type)->where('REFERENCE',$r->del)->delete();
        }

        $view['trans']  = DB::select("select DISTINCT REFERENCE from TBL_P2_BOND_TRANS");
        $view['port']   = DB::select("select DISTINCT REFERENCE from TBL_P2_BOND_PORTFOLIO");
        $view['eq']     = DB::select("select DISTINCT REFERENCE from TBL_P2_EQUITY_TRANS");
        
        return view('mea')->with($view);
    }


}
