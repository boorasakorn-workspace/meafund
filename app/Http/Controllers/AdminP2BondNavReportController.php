<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Jenssegers\Date\Date;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;


use App\Libraries\MEAUtils;
use Illuminate\Support\Facades\Log;

class AdminP2BondNavReportController extends Controller
{

    /** 
     * ตรมสารหนี้ (BOND)
     * รายงาน กำไรสุทธิ (NAV Report)
     */
   
    public function matGetYear($date_1 , $date_2 ) {
      $datetime1 = date_create($date_1);
      $datetime2 = date_create($date_2);
     
      $interval = date_diff($datetime1, $datetime2);
      $ss = $interval->format('%y,%m,%d');
      return explode(",", $ss);
    }

    public function getreport()
    {
        $view_name = 'backend.pages.p2_bond_nav_report';
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 63,
            'menu_id' => 5,
            'title' => getMenuName($data,63,5) . '|  MEA FUND'
        ] );

      
        $allquery = "SELECT * FROM TBL_P2_BOND_SECURITIES";
        $equitylist = DB::select(DB::raw($allquery));

        $allquery = "SELECT * FROM TBL_P2_BOND_CATEGORY ORDER BY CATE_ID";
        $equitycategory = DB::select(DB::raw($allquery));

        $allquery = "SELECT * FROM TBL_P2_BOND_BROKER ORDER BY NAME_SHT ASC;";
        $equitybroker = DB::select(DB::raw($allquery));

        //$allquery = "SELECT SYMBOL, COMP_NAME FROM TBL_P2_EQUITY_INDEX ORDER BY SYMBOL ASC;";
        $allquery = "SELECT SYMBOL FROM TBL_P2_BOND_INDEX ORDER BY SYMBOL ASC;";
        $equityindex = DB::select(DB::raw($allquery));
  
        $years = array();
        for($i= date('Y'); $i>date('Y')-25; $i--) {
           array_push($years, $i);
        }

        /////////////
        $categorylist = DB::table('TBL_P2_BOND_CATEGORY')->orderby("BOND_CATE_THA")->get();  
        $securitieslist =  DB::table('TBL_P2_BOND_SECURITIES')->orderby("NAME_SHT")->get(); 
        $sectionList = array( );
        $tmp = array();
        $key = "";
        $child = array();

        foreach ($categorylist as $row) {
            if($key != $row->BOND_CATE_THA) {
               if($key == "") {
                   $child = array();
               } else {
                  $sectionList[$key] = $child;
                  $child = array();
               }
            } 
            $key = $row->BOND_CATE_THA;
            array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'BOND_TYPE_THA'=>$row->BOND_TYPE_THA,
                                     'BOND_CATE_THA'=>$row->BOND_CATE_THA));
        }
        $sectionList[$key] = $child;

        return view($view_name)->with([
            'equitylist'     => $equitylist,
            'equitycategory' => $equitycategory,
            'equitybroker'   => $equitybroker,
            'years'          => $years,
            'symbols'        => $equityindex,
            'securitieslist' => $securitieslist,
            'sectionList'    => $sectionList
            ]);

    }

    /*
    public  function  DataSourceTradingReportCount($ArrParam, $IsCase){
        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];



        $symbol   = $ArrParam["symbol"] ;
        $name_sht = $ArrParam["name_sht"];
        $industrial = $ArrParam["industrial"];
        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

    }
   */
   public  function  DataSourceCount($ArrParam, $IsCase){

        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];


        // Log::info(get_class($this) .'::'. __FUNCTION__);

        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM 
        $cateid   = $ArrParam["industrial"];  // CATE_ID;
        
        $bu = '';
        $indus = '';

        if((!empty($cateid)) && (strlen($cateid) > 0)) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
                           ->orderby("BOND_CATE_THA")
                           ->where('CATE_ID','=', $cateid)->get();
            foreach ($categorylist as $row) {
                $indus = $row->BOND_CATE_THA;
                $bu    = $row->BOND_TYPE_THA;
                break;  
            }
        }
        
        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        
        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 5) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 5) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

      
        // WHERE2
        $where2 = "" ;//
        if((!empty($symbol)) && (strlen($symbol) > 0)) { // && $check_name== "true"){
            $where2 .= " P.SYMBOL = '".$symbol."' ";
            //$where_inner .= " x.SYMBOL = '" . $symbol .  "' "; 
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " P.SECURITIES_NAME = '".$name_sht."' ";
        }

        if((!empty($cateid)) && (strlen($cateid) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND C.CATE_ID  = ". $cateid ." ";
            else
               $where2 .= " C.CATE_ID  = ". $cateid ." "; 
        }

        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND  P.REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " P.REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
        
        //if(!$IsCase) {
        //    $where2 = "";
        //}

        
        if(strlen($where2) > 0) {
            $where2 = " AND " . $where2; 
        }
      
        $query = " SELECT COUNT(P.SYMBOL) as total " . 
                 "     FROM TBL_P2_BOND_PORTFOLIO P, TBL_P2_BOND_INDEX I, TBL_P2_BOND_CATEGORY C " .
                 " WHERE P.SYMBOL = I.SYMBOL AND I.CATE_ID = C.CATE_ID ".
                 "   ".
                 $where2;

        $all = DB::select(DB::raw($query));
        Log::info(__FUNCTION__ . ' :: SQL=' . $query);

        Log::info(__FUNCTION__  .' :: TOAL=' . $all[0]->total );
        return  $all[0]->total;
    }

    public  function  DataSource($ArrParam, $IsCase, $ispageing= true) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        
        $where = "";
        if($ispageing){
            $PageSize = $ArrParam['pagesize'];
            $PageNumber = $ArrParam['PageNumber'];
        }

        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM 
        $cateid = $ArrParam["industrial"];  // CATE_ID;
        
        $bu = '';
        $indus = '';

        if((!empty($industrial)) && (strlen($industrial) > 0)) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
                           ->orderby("BOND_CATE_THA")
                           ->where('CATE_ID','=', $cateid)->get();
            foreach ($categorylist as $row) {
                $indus = $row->BOND_CATE_THA;
                $bu    = $row->BOND_TYPE_THA;
                break;  
            }
        }
        
        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        
        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 5) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 5) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        // WHERE2
        $where2 = "" ;
        if((!empty($symbol)) && (strlen($symbol) > 0)) { // && $check_name== "true"){
            $where2 .= " P.SYMBOL = '".$symbol."' ";
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " P.SECURITIES_NAME = '".$name_sht."' ";
        }

        if((!empty($cateid)) && (strlen($cateid) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND I.CATE_ID  = ". $cateid ." ";
            else
               $where2 .= " I.CATE_ID  = '". $cateid ."' "; 
        }

        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND  P.REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " P.REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
        
        //if(!$IsCase) {
        //    $where2 = "";
        //}

        
        if(strlen($where2) > 0) {
            $where2 = " AND " . $where2 . " ORDER BY P.REFERENCE_DATE "; 
        }
        Log::info('indus='.$indus . ', bu=' .$bu . ', date_start=' . $date_start);

        $query = " SELECT P.*, C.BOND_CATE_THA, C.BOND_TYPE_THA, I.FULL_NAME " . 
                 "     FROM TBL_P2_BOND_PORTFOLIO P, TBL_P2_BOND_INDEX I, TBL_P2_BOND_CATEGORY C " .
                 " WHERE P.SYMBOL = I.SYMBOL AND I.CATE_ID = C.CATE_ID ".
                 "   ".
                 $where2;


        if($ispageing){
            $query .= " OFFSET " . $PageSize . " * (" . $PageNumber . " - 1) ROWS FETCH NEXT " . $PageSize . " ROWS ONLY OPTION (RECOMPILE) ";
        }

        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
  }

  public  function  DataSourceExport($ArrParam,  $ispageing= false) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        
        $where = "";
        if($ispageing){
            $PageSize = $ArrParam['pagesize'];
            $PageNumber = $ArrParam['PageNumber'];
        }

        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM 
        $cateid = $ArrParam["industrial"];  // CATE_ID;
        
        $bu = '';
        $indus = '';

        if((!empty($industrial)) && (strlen($industrial) > 0)) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
                           ->orderby("BOND_CATE_THA")
                           ->where('CATE_ID','=', $cateid)->get();
            foreach ($categorylist as $row) {
                $indus = $row->BOND_CATE_THA;
                $bu    = $row->BOND_TYPE_THA;
                break;  
            }
        }
        
        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        
        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 5) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 5) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

      
        // WHERE2
        $where2 = "" ;//
        if((!empty($symbol)) && (strlen($symbol) > 0)) { // && $check_name== "true"){
            $where2 .= " P.SYMBOL = '".$symbol."' ";
            //$where_inner .= " x.SYMBOL = '" . $symbol .  "' "; 
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " P.SECURITIES_NAME = '".$name_sht."' ";
        }

        if((!empty($cateid)) && (strlen($cateid) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND C.CATE_ID  = ". $cateid ." ";
            else
               $where2 .= " C.CATE_ID  = ". $cateid ." "; 
        }

        
        
        if((!empty($date_start)) && (!empty($date_end))) 
        { 
            if(strlen($where2) > 1) 
               $where2 .= " AND  P.REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " P.REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
        
       
        
        if(strlen($where2) > 0) {
            $where2 = " AND " . $where2; 
        }
        Log::info('indus='.$indus . ', bu=' .$bu . ', date_start=' . $date_start);

         $query = " SELECT P.*, C.BOND_CATE_THA, C.BOND_TYPE_THA, I.FULL_NAME " . 
                 "     FROM TBL_P2_BOND_PORTFOLIO P, TBL_P2_BOND_INDEX I, TBL_P2_BOND_CATEGORY C " .
                 " WHERE P.SYMBOL = I.SYMBOL AND I.CATE_ID = C.CATE_ID ".
                 "   ".
                 $where2;


        //if($ispageing){
        //    $query .= " OFFSET " . $PageSize . " * (" . $PageNumber . " - 1) ROWS FETCH NEXT " . $PageSize . " ROWS ONLY OPTION (RECOMPILE) ";
        //}

        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
  }


  public  function  DataSourceGrandTotal($ArrParam) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        
        $where = "";
        

        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM 
        $cateid = $ArrParam["industrial"];  // CATE_ID;
        
        $bu = '';
        $indus = '';

        if((!empty($industrial)) && (strlen($industrial) > 0)) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
                           ->orderby("BOND_CATE_THA")
                           ->where('CATE_ID','=', $cateid)->get();
            foreach ($categorylist as $row) {
                $indus = $row->BOND_CATE_THA;
                $bu    = $row->BOND_TYPE_THA;
                break;  
            }
        }
        
        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        
        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 5) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 5) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        // WHERE2
        $where2 = "" ;
        if((!empty($symbol)) && (strlen($symbol) > 0)) { // && $check_name== "true"){
            $where2 .= " P.SYMBOL = '".$symbol."' ";
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " P.SECURITIES_NAME = '".$name_sht."' ";
        }

        if((!empty($cateid)) && (strlen($cateid) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND C.CATE_ID  = ". $cateid ." ";
            else
               $where2 .= " C.CATE_ID  = ". $cateid ." "; 
        }

        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND  P.REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " P.REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
        
        
        
        if(strlen($where2) > 0) {
            $where2 = " AND " . $where2 . " "; 
        }
        $query =  " SELECT SUM(P.FACE_VALUE)  as SUM_FACE_VALUE, " .
                  "        SUM(P.TOTAL_COST)  as SUM_TOTAL_COST, " .
                  "        SUM(P.ACCRUED_INT) as SUM_ACCRUED_INT, " . 
                  "        SUM(P.DIRTY_MKT)   as SUM_DIRTY_MKT,".
                  "        SUM(P.COUPON_RATE) as SUM_COUPON_RATE  " .  
                  " FROM TBL_P2_BOND_PORTFOLIO P, TBL_P2_BOND_INDEX I, TBL_P2_BOND_CATEGORY C " .
                  "   WHERE P.SYMBOL = I.SYMBOL AND I.CATE_ID = C.CATE_ID ".
                  "   ".
                  $where2;
                  
        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }


public  function DataSourceBarChart($ArrParam) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        
        $where = "";
        
        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        $cateid = $ArrParam["industrial"];  

        $bu = '';
        $indus = '';

        if((!empty($industrial)) && (strlen($industrial) > 0)) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
                           ->orderby("BOND_CATE_THA")
                           ->where('CATE_ID','=', $cateid)->get();
            foreach ($categorylist as $row) {
                $indus = $row->BOND_CATE_THA;
                $bu    = $row->BOND_TYPE_THA;
                break;  
            }
        }

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];


        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 5) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 5) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        // WHERE2
        $where2 = "" ;
        if((!empty($symbol)) && (strlen($symbol) > 0)) { // && $check_name== "true"){
            $where2 .= " P.SYMBOL = '".$symbol."' ";
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " P.SECURITIES_NAME = '".$name_sht."' ";
        }

        if((!empty($cateid)) && (strlen($cateid) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND C.CATE_ID  = ". $cateid ." ";
            else
               $where2 .= " C.CATE_ID  = ". $cateid ." "; 
        }

        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND  P.REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " P.REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
        
        if(strlen($where2) > 0) {
            $where2 = " AND " . $where2 . " GROUP BY C.BOND_CATE_THA "; 
        }
        
        $query = " SELECT  C.BOND_CATE_THA, ".
                 "         (SUM(P.DIRTY_MKT) / 1000000) as SUM_DIRTY_MK  " .
                 " FROM TBL_P2_BOND_PORTFOLIO P, TBL_P2_BOND_INDEX I, " .
                 "      TBL_P2_BOND_CATEGORY C  ". 
                 " WHERE P.SYMBOL = I.SYMBOL AND I.CATE_ID = C.CATE_ID  ".
                 "  " .
                 $where2;
                  
        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }

    public function ajax_report_search(Request $request)
    {
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $data = getmemulist();
        $title = getMenuName($data, 63,5);

        $ar = $this->retrive_customize_field(63,5);

        $view_name = 'backend.pages.ajax.ajax_p2_bond_nav_report';
        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $symbol = $request->input('symbol');
        $name_sht = $request->input('name_sht');
        $industrial = $request->input('industrial');
        $cateid = $request->input('industrial');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $ArrParam = array();

        //{ pagesize: "10", PageNumber: 1, symbol: "", name_sht: "", industrial: "", date_start: "", date_end: "" }
        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;

        $ArrParam["symbol"] =$symbol;
        $ArrParam["name_sht"] = $name_sht;
        $ArrParam["industrial"] =$industrial;
        $ArrParam["cateid"] =$cateid;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;


        $data =null;
        $totals= 0;

        $totals = $this->DataSourceCount($ArrParam, true);
        $data = $this->DataSource($ArrParam, true);
        $grandtoal = $this->DataSourceGrandTotal($ArrParam);

        
        $htmlPaginate =Paginatre_gen($totals, $PageSize,'page_click_search', $PageNumber);

        
        $utils = new MEAUtils();
        Log::info(get_class($this) .'::'. __FUNCTION__. ' $utils =new MEAUtils()' );
        $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
        $thai_date_end =  (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";
        Log::info(get_class($this) .'::'. __FUNCTION__. ' ' .  
            ' converted date: ' . $date_start .' to ' . $thai_date_start . ', ' . $date_end . ' to '. $thai_date_end );
        
        $pretty_date = $utils->prettyPrintThaiDate($thai_date_start, $thai_date_end, "red");

        $allquery = "SELECT * FROM TBL_P2_BOND_SECURITIES WHERE NAME_SHT='" . $name_sht . "' ";
        $securitieslist = DB::select(DB::raw($allquery));

        $securities_name = "";
        foreach ($securitieslist as $row) {
           $securities_name = $row->SECURITIES_NAME;
           break;
        }
      
/*
        // option
        $opt_array = $ar['result'];
        
        $sub_array =  $opt_array[0];
        // crate column output array 
        $column = array();
        for($i =0; $i< 50; $i++) {
             if (count($opt_array) == 0) {
                $column[$i] = 'x'; 
             } else {
                $column[$i] = 'o'; 
            }
        }

        if (count($sub_array) > 0) {
            $sub = $sub_array['options'];
            $idx  = 0;
            foreach($sub as $x => $item) {
                
                if(strrpos($item, 'option') >= 0) {
                    Log::info($item);
                    $idx = intval(substr($item, 6, 2));
                    $column[$idx] = 'x';
                }
                $idx++;
            }
        }
        
        $prop_hdr = array();
        $ln = 0;
        for($i = 0; $i< 5; $i++ ) {
            if($column[$i] == 'x') {
               $ln++;
            }
        }
        array_push($prop_hdr, array('length' =>$ln));

        $ln = 0;
        for($i = 5; $i< 7; $i++ ) {
            if($column[$i] == 'x') {
               $ln++;
            }
        }
        array_push($prop_hdr, array('length' =>$ln));

        $ln = 0;
        for($i = 7; $i< 11; $i++ ) {
            if($column[$i] == 'x') {
               $ln++;
            }
        }
        array_push($prop_hdr, array('length' =>$ln));

        $ln = 0;
        for($i = 11; $i< 16; $i++ ) {
            if($column[$i] == 'x') {
               $ln++;
            }
        }
        array_push($prop_hdr, array('length' =>$ln));



        //$opt_array = $results->options;
   */
        $returnHTML = view($view_name)->with([
                'htmlPaginate'=> $htmlPaginate,
                'data' => $data,
                'totals' => $totals,
                'grandtoal' => $grandtoal,  
                'PageSize' =>$PageSize,
                'PageNumber' =>$PageNumber,
                'date_start' => $thai_date_start,
                'date_end' => $thai_date_end,
                'pretty_date' => $pretty_date,
                'TableTitle' => $title,
                'SubTitle' => $securities_name
                //,
                //'fields' => $column,
                //'fields_poperties' => $prop_hdr

            ])->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }



    public function ajax_chart_search(Request $request) {
        Log::info(get_class($this) .'::'. __FUNCTION__);
         
        $data = getmemulist();
        $title = getMenuName($data, 63, 5);


        $symbol = $request->input('symbol');
        $name_sht = $request->input('name_sht');
        $industrial = $request->input('industrial');
        $cateid = $request->input('industrial');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $ArrParam = array();

        //{ pagesize: "10", PageNumber: 1, symbol: "", name_sht: "", industrial: "", date_start: "", date_end: "" }
        $ArrParam["pagesize"] ="";
        $ArrParam["PageNumber"] ="";

        $ArrParam["symbol"] = $symbol;
        $ArrParam["name_sht"] = $name_sht;
        $ArrParam["industrial"] = $industrial;
        $ArrParam["cateid"] = $cateid;
        $ArrParam["date_start"] = $date_start;
        $ArrParam["date_end"] = $date_end;

        $data = null;
        $totals = 0;

        $chartds = $this->DataSourceBarChart($ArrParam);
      
        $utils = new MEAUtils();
        $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
        $thai_date_end =  (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";
        
        Log::info(get_class($this) .'::'. __FUNCTION__.
            ' converted date: ' . $date_start .' to ' . $thai_date_start . ', ' . $date_end . ' to '. $thai_date_end );
        $data  = array();
        $data1 = array();
        $data2 = array();
     
        $pretty_date = $utils->prettyPrintThaiDate($thai_date_start, $thai_date_end, "");

        $allquery = "SELECT * FROM TBL_P2_BOND_SECURITIES WHERE NAME_SHT='" . $name_sht . "' ";
        $securitieslist = DB::select(DB::raw($allquery));

        $securities_name = "";
        foreach ($securitieslist as $row) {
           $securities_name = $row->SECURITIES_NAME;
           break;
        }

        $returnHTML = [
                'data' => $chartds,
                'date_start' => $thai_date_start,
                'date_end' => $thai_date_end,
                'pretty_date' => $pretty_date,
                'SubTitle' => $securities_name,

        ];

        return response()->json(array('success' => true, 'result'=>$returnHTML));
    }


    public function ajax_report_search_export() {
        Log::info(get_class($this) .'::'. __FUNCTION__);
        
        $symbol = Input::get('symbol');
        $name_sht = Input::get('name_sht');
        $industrial = Input::get('industrial');
        $cateid =  Input::get('industrial');
        $date_start = Input::get('date_start');
        $date_end = Input::get('date_end');

        $ArrParam = array();
        $ArrParam["pagesize"] ="";
        $ArrParam["PageNumber"] ="";
        $ArrParam["symbol"] =$symbol;
        $ArrParam["name_sht"] =$name_sht;
        $ArrParam["industrial"] =$industrial;
        
        $ArrParam["cateid"] = $cateid;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;

        $data = $this->DataSourceExport($ArrParam, false);
        $grandtoal = $this->DataSourceGrandTotal($ArrParam);


        $allquery = "SELECT * FROM TBL_P2_BOND_SECURITIES WHERE NAME_SHT='" . $name_sht . "' ";
        $securitieslist = DB::select(DB::raw($allquery));

        $securities_name = "";
        foreach ($securitieslist as $row) {
           $securities_name = $row->SECURITIES_NAME;
           break;
        }

        $results = array();
        foreach ($data as $item) {
            $results[] = (array)$item;
        }

        $SUM_DIRTY_MKT = 1;

        foreach($grandtoal as $rs =>$sumfield)        
            $SUM_DIRTY_MKT = ($sumfield->SUM_DIRTY_MKT==null) ? 1: $sumfield->SUM_DIRTY_MKT;
                     
         Log::info('SUM_DIRTY_MKT==>' . $SUM_DIRTY_MKT);

        
        
  

        Excel::create('รายงานรายละเอียดทรัพย์สินสุทธิ', function ($excel) use ($results, $SUM_DIRTY_MKT, $ArrParam, $securities_name){

            $excel->sheet('รายงานรายละเอียดทรัพย์สินสุทธิ', function ($sheet) use ($results, $SUM_DIRTY_MKT, $ArrParam, $securities_name){
                // first row styling and writing content
                $sheet->setStyle([
                    'borders' => [
                        'allborders' => [
                            'color' => [
                                'rgb' => 'b3b6b7'
                            ]
                        ]
                    ]
                ]);
                $sheet->mergeCells('A1:L1');
                $sheet->mergeCells('A2:L2');
                $sheet->mergeCells('A3:L3');
                $sheet->mergeCells('A4:L4');
 
                 

                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Cordia New');
                    $row->setFontSize(20);
                    $row->setFontWeight('bold');
                    $row->setAlignment('left');
                    $row->setBackground('#b2babb');
                    $row->setBorder('thin', 'thin', 'none', 'thin');
                });

                $sheet->row(2, function ($row) { 
                    $row->setAlignment('left');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'none', 'thin');
                });

                $sheet->row(3, function ($row) { 
                    $row->setAlignment('left');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'none', 'thin');
                });

                $sheet->row(4, function ($row) { 
                    $row->setAlignment('left');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'thin', 'thin');

                });

                $sheet->row(1, array('รายงานรายละเอียดทรัพย์สินสุทธิ'));
                $sheet->row(2, array($securities_name));

                if($ArrParam["date_start"] != "" && $ArrParam["date_end"] != ""){
                    $sheet->row(3, array('ในช่วงวันที่ ' . get_date_notime($ArrParam["date_start"]) . ' ถึง ' . get_date_notime($ArrParam["date_end"])   ));
                }

                $sheet->row(4, array('รายงานข้อมูล ณ วันที่ ' . get_date_notime(date("Y-m-d H:i:s"))));

                $header[]   = null;
                $header[0]  = '#';
                $header[1]  = "ประเภททรัพย์สิน";
                $header[2]  = "ประเภทตราสารหนี้";
                $header[3]  = "ชื่อย่อตราสารหนี้";
                $header[4]  = "ชื่อเต็มตราสารหนี้";
                $header[5]  = "จำนวนหุ้น / มูลค่าที่ตราไว้";
                $header[6]  = "ราคาทุน";
                $header[7]  = "ดอกเบี้ยค้างรับ";
                $header[8]  = "ราคายุติธรรม";
                $header[9]  = "อัตราส่วนของราคายุติธรรม (%)";

                $header[10]  = "อัตราดอกเบี้ย";
                $header[11] = "วันครบกำหนด";
                
                $sheet->row(5, $header);

                $sheet->row(5, function ($row) { 
                   $row->setBackground('#b2babb');
                });

                $LAST_ROW = 5 + 1;

                // Set multiple column formats
                $sheet->setColumnFormat(array(
                  'A' => '@',
                  'B' => '@',
                  'C' => '@',
                  'D' => '@',
                  'E' => '@',
                  'F' => '#,##0.00',  
                  'G' => '#,##0.00',  
                  'H' => '#,##0.00',
                  'I' => '#,##0.00',  
                  'J' => '@',
                  'K' => '#,##0.00', // อัตราดอกเบี้ย
                  'L' => '@'         // วันครบกำหนด   
                ));

                $rscount = count($results);   
                $idx = 0; 
                $N = 0.0;
                $J = 0.0;
                $O = 0.0;
                $NO = 0;
                 

                $PAGE_SUM_FACE_VALUE  =  0;
                $PAGE_SUM_TOTAL_COST  =  0;
                $PAGE_SUM_ACCRUED_INT =  0;
                $PAGE_SUM_DIRTY_MKT   =  0;

                $PERCENT_DIRTY_MKT = 0; 

                /* 
                foreach($grandtoal as $rs =>$sumfield) {
                  $SUM_DIRTY_MKT = $sumfield->SUM_DIRTY_MKT;
                }
                */

                foreach ($results as $item) {
                    $NO += 1;

                     $PERCENT_DIRTY_MKT = ($item['DIRTY_MKT'] / $SUM_DIRTY_MKT) * 100;
                

                     $rs[] = null; 
                     $rs[0] = $NO;
                     $rs[1] = $item['BOND_CATE_THA']; // A
                     $rs[2] = $item['BOND_TYPE_THA']; 
                     $rs[3] = $item['SYMBOL'];
                     $rs[4] = $item['FULL_NAME'];

                     $rs[5] = $item['FACE_VALUE'];
                     $rs[6] = $item['TOTAL_COST'];
                     $rs[7] = $item['ACCRUED_INT'];
                     $rs[8] = $item['DIRTY_MKT'];

                     $rs[9] = $PERCENT_DIRTY_MKT; 

                     $rs[10] = $item['COUPON_RATE'];
                     $rs[11] = $item['MAT'];

                     
                     $PAGE_SUM_FACE_VALUE  +=  $item['FACE_VALUE'];
                     $PAGE_SUM_TOTAL_COST  +=  $item['TOTAL_COST'];
                     $PAGE_SUM_ACCRUED_INT +=  $item['ACCRUED_INT'];
                     $PAGE_SUM_DIRTY_MKT   +=  $item['DIRTY_MKT'];

                     $sheet->appendRow($rs);
                     $idx++;
                }

                $sheet->cell('E'. strval($idx+$LAST_ROW), function($cell) {
                        $cell->setValue("ยอดรวม");
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                });


                if($rscount > 0) {
                    
                    $sheet->cell('F'. strval($idx+$LAST_ROW), function($cell) use ($PAGE_SUM_FACE_VALUE) {
                        $cell->setValue($PAGE_SUM_FACE_VALUE);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                    });
                    $sheet->cell('G'. strval($idx+$LAST_ROW), function($cell) use ($PAGE_SUM_TOTAL_COST) {
                        $cell->setValue($PAGE_SUM_TOTAL_COST);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                    });

                    $sheet->cell('H'. strval($idx+$LAST_ROW), function($cell) use ($PAGE_SUM_ACCRUED_INT) {
                        $cell->setValue($PAGE_SUM_ACCRUED_INT);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                     
                    });
                    $sheet->cell('I'. strval($idx+$LAST_ROW), function($cell) use ($PAGE_SUM_DIRTY_MKT) {
                        $cell->setValue($PAGE_SUM_DIRTY_MKT);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                     
                    });

                } 

            });

        })->download('xls');
    }

    public function ajax_store_customize_field(Request $request) {
        $status = false;
        $result = array();
        $store_path = storage_path() .'/public/import';
        try {
            $menu_id = $request->input('menu_id');  //eg. 60
            $submenu_id = $request->input('submenu_id');
            $utils = new MEAUtils();
            $user = $utils->getLoggedInUser(); 
            $json = $request->input('options');
            $options = array('options'=>$json);
            $file = $store_path . '/' . $user. '-' . $menu_id . '-' . $submenu_id. '.json';
            file_put_contents($file, json_encode($options, TRUE));
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Saved:' . $file );
            array_push($results, $file);

        } catch (\Exception $e) {
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Exception:' .$e->getMessage());
            $status = false;
        } 
        return response()->json(array('success' => $status, 'result'=>$result));
    }

    
    public function retrive_customize_field($menu_id, $submenu_id) {
        $status = false;
        $result = array();
        $store_path = storage_path() .'/public/import';
        try {
            //$menu_id = $request->input('menu_id');  // eg. 60
            //$submenu_id = $request->input('submenu_id');
            $utils = new MEAUtils(); 
            $user = $utils->getLoggedInUser(); 
            $file = $store_path . '/' . $user . '-' . $menu_id . '-' . $submenu_id. '.json';
            $json = json_decode(file_get_contents($file), true);
            array_push($result, $json);

            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Retrived:' . $file );
            $status = true;
        } catch (\Exception $e) {
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Exception:' .$e->getMessage());
            $status = false;
        } 
        return array('success' => $status, 'result'=>$result);
    } 

    public function ajax_retrive_customize_field(Request $request) {
        /*$status = false;
        $result = array();
        $store_path = storage_path() .'/public/import';
        try {
            $menu_id = $request->input('menu_id');  // eg. 60
            $submenu_id = $request->input('submenu_id');
            $utils = new MEAUtils();// Session::get('user_data');
            $user = $utils->getLoggedInUser(); 
            $file = $store_path . '/' . $user . '-' . $menu_id . '-' . $submenu_id. '.json';
            $json = json_decode(file_get_contents($file), true);
            array_push($result, $json);

            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Retrived:' . $file );
            $status = true;
        } catch (\Exception $e) {
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Exception:' .$e->getMessage());
            $status = false;
        } 

        return response()->json(array('success' => $status, 'result'=>$result));*/
        $menu_id = $request->input('menu_id');  // eg. 60
        $submenu_id = $request->input('submenu_id');
          
        $r = $this->retrive_customize_field($menu_id, $submenu_id);
        return response()->json($r);
    }

    /* Utils */
   /* public function toThaiDateTime($strDate, $withTime)
    {
        $strYear = date("Y",strtotime($strDate)) + 543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        if ($withTime) {
            $strHour= date("H",strtotime($strDate));
            $strMinute= date("i",strtotime($strDate));
            $strSeconds= date("s",strtotime($strDate));
            
            return $strDay . " " . $strMonthThai. " ". $strYear .",  " . $strHour . ":" . $strMinute;
        } 
        return $strDay . " " . $strMonthThai. " ". $strYear ;
        
    }
    */

    /////////////////////////
    ///
    //
 /*   $opt_array = array('result'=>['option2', 'option1', 'option3']);
$sub_array = $opt_array['result']; 
$o = array();
foreach($sub_array as $x => $i ) {
    $str = substr($i, 6); // option0
    if(strrpos($str,'option') >= 0) {
        $idx = substr($i, 6, 1);
        $nr = array($idx => 'checked');
        array_push($o, $nr);
    }
   
}

print_r($o['0']);
 */

}


/**
 * 1) STATEMENT SUM TOTAL:
 * =====================
 * V1:
 * SELECT  COALESCE(SUM(TOTAL_PURCHASE) / NULLIF(SUM(UNIT_PURCHASE), 0), 0) AS TMP_PRICE_PERCHASE,
 *       COALESCE (SUM(TOTAL_SALE) / NULLIF(SUM(UNIT_SALE), 0), 0) AS TMP_PRICE_SALE,
 *       COALESCE (SUM(TOTAL_DIV) / NULLIF(SUM(UNIT_DIV), 0), 0) AS TMP_PRICE_DIV
 *
 *     FROM TBL_P2_EQUITY_TRANS WHERE SYMBOL='CPALL'
 *     AND SECURITIES_NAME ='UOBAM' AND (TRANS_DATE BETWEEN '2017-05-01 00:00:01' AND  '2017-05-31 23:59:59');
 *
 */

 /* V2:
 SELECT  COALESCE(SUM(TOTAL_PURCHASE) / NULLIF(SUM(UNIT_PURCHASE), 0), 0) AS TMP_PRICE_PERCHASE, SUM(UNIT_PURCHASE) AS SUM_UNIT_PERCHASE, SUM(TOTAL_PURCHASE) AS SUM_TOAL_PURCHASE,
        COALESCE(SUM(TOTAL_SALE) / NULLIF(SUM(UNIT_SALE), 0), 0) AS TMP_PRICE_SALE, SUM(UNIT_SALE) AS SUM_UNIT_SALE, SUM(TOTAL_SALE) AS SUM_TOAL_SALE, SUM(TOTAL_COST) AS SUM_TOTAL_COST, SUM(PROFIT_LOSS) AS SUM_PROFIT_LOSS,

        COALESCE(SUM(TOTAL_DIV) / NULLIF(SUM(UNIT_DIV), 0), 0) AS TMP_PRICE_DIV,  SUM(UNIT_DIV) AS SUM_UNIT_DIV, SUM(TOTAL_DIV) AS SUM_TOAL_DIV

      FROM TBL_P2_EQUITY_TRANS WHERE SYMBOL='CPALL'
       AND SECURITIES_NAME ='UOBAM' AND (TRANS_DATE BETWEEN '2017-01-01 00:00:01' AND  '2017-01-31 23:59:59');


 */
       /*
       SELECT  COALESCE(SUM(TOTAL_PURCHASE) / NULLIF(SUM(UNIT_PURCHASE), 0), 0) AS TMP_PRICE_PERCHASE, 
        SUM(UNIT_PURCHASE) AS SUM_UNIT_PERCHASE, SUM(TOTAL_PURCHASE) AS SUM_TOAL_PURCHASE,  
        

        COALESCE(SUM(TOTAL_SALE) / NULLIF(SUM(UNIT_SALE), 0), 0) AS TMP_PRICE_SALE, 
        SUM(UNIT_SALE) AS SUM_UNIT_SALE, SUM(TOTAL_SALE) AS SUM_TOTAL_SALE, SUM(TOTAL_COST) AS SUM_TOTAL_COST, SUM(PROFIT_LOSS) AS SUM_PROFIT_LOSS,
        
        COALESCE(SUM(TOTAL_DIV) / NULLIF(SUM(UNIT_DIV), 0), 0) AS TMP_PRICE_DIV,  SUM(UNIT_DIV) AS SUM_UNIT_DIV, SUM(TOTAL_DIV) AS SUM_TOAL_DIV,

        SUM(UNIT_PURCHASE) - SUM(UNIT_SALE) as TOTAL_UNIT_DIFF

         

      FROM TBL_P2_EQUITY_TRANS WHERE SYMBOL='CPALL'
       AND SECURITIES_NAME ='UOBAM' AND (TRANS_DATE BETWEEN '2017-01-01 00:00:01' AND  '2017-01-31 23:59:59');


       */

       /* V4:
      SELECT   a.LAST_SYMBOL, 
        (CASE WHEN a.LAST_UNIT_PURCHASE >= 0 THEN 'IN PERCHASE' ELSE 'IN SALE' END) as T,
        --a.LAST_UNIT_PURCHASE as WOW, 
        COALESCE(SUM(b.TOTAL_PURCHASE) / NULLIF(SUM(b.UNIT_PURCHASE), 0), 0) AS TMP_PRICE_PERCHASE, 
        SUM(b.UNIT_PURCHASE) AS SUM_UNIT_PERCHASE, SUM(b.TOTAL_PURCHASE) AS SUM_TOAL_PURCHASE,  
        

        COALESCE(SUM(b.TOTAL_SALE) / NULLIF(SUM(b.UNIT_SALE), 0), 0) AS TMP_PRICE_SALE, 
        SUM(b.UNIT_SALE) AS SUM_UNIT_SALE, SUM(b.TOTAL_SALE) AS SUM_TOTAL_SALE,
       SUM(b.TOTAL_COST) AS SUM_TOTAL_COST, SUM(b.PROFIT_LOSS) AS SUM_PROFIT_LOSS,
        
        COALESCE(SUM(b.TOTAL_DIV) / NULLIF(SUM(b.UNIT_DIV), 0), 0) AS TMP_PRICE_DIV, 
                SUM(b.UNIT_DIV) AS SUM_UNIT_DIV, SUM(b.TOTAL_DIV) AS SUM_TOAL_DIV,

        SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as TOTAL_UNIT_DIFF


      FROM TBL_P2_EQUITY_TRANS b INNER JOIN  
         
         (select 
            TOP 1 TRANS_DATE as LAST_TRANS_DATE, 
            TYPE as LAST_TYPE, 
            UNIT_PURCHASE as LAST_UNIT_PURCHASE,
            SYMBOL as LAST_SYMBOL ,
            SECURITIES_NAME
        from 
            TBL_P2_EQUITY_TRANS
        WHERE 
            TRANS_DATE < '2017-01-31 23:59:59' AND SYMBOL='CPALL' AND SECURITIES_NAME='UOBAM') a 

         ON a.LAST_SYMBOL = b.SYMBOL 
           AND a.SECURITIES_NAME = b.SECURITIES_NAME
           AND b.SYMBOL='CPALL' AND b.SECURITIES_NAME ='UOBAM'
           AND (b.TRANS_DATE BETWEEN '2017-01-07 00:00:01' AND  '2017-01-31 23:59:59')  
         
          GROUP BY a.LAST_SYMBOL, a.LAST_UNIT_PURCHASE;
      


       */

          /*
          V5:
          ====
          select * FROM TBL_P2_EQUITY_TRANS,
 (SELECT   a.LAST_SYMBOL, 
        (CASE WHEN a.LAST_UNIT_PURCHASE > 0 THEN 'IN PURCHASE' ELSE 'IN SALE' END) as LAST_TR,
        --a.LAST_UNIT_PURCHASE as WOW, 
        COALESCE(SUM(b.TOTAL_PURCHASE) / NULLIF(SUM(b.UNIT_PURCHASE), 0), 0) AS TMP_PRICE_PURCHASE, 
        SUM(b.UNIT_PURCHASE) AS SUM_UNIT_PURCHASE, SUM(b.TOTAL_PURCHASE) AS SUM_TOAL_PURCHASE,  
        

        COALESCE(SUM(b.TOTAL_SALE) / NULLIF(SUM(b.UNIT_SALE), 0), 0) AS TMP_PRICE_SALE, 
        SUM(b.UNIT_SALE) AS SUM_UNIT_SALE, SUM(b.TOTAL_SALE) AS SUM_TOTAL_SALE,
       SUM(b.TOTAL_COST) AS SUM_TOTAL_COST, SUM(b.PROFIT_LOSS) AS SUM_PROFIT_LOSS,
        
        COALESCE(SUM(b.TOTAL_DIV) / NULLIF(SUM(b.UNIT_DIV), 0), 0) AS TMP_PRICE_DIV, 
                SUM(b.UNIT_DIV) AS SUM_UNIT_DIV, SUM(b.TOTAL_DIV) AS SUM_TOAL_DIV,

        ABS(SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE)) as TOTAL_UNIT_DIFF


      FROM TBL_P2_EQUITY_TRANS b INNER JOIN  
         
         (select 
            TOP 1 TRANS_DATE as LAST_TRANS_DATE, 
            TYPE as LAST_TYPE, 
            UNIT_PURCHASE as LAST_UNIT_PURCHASE,
            SYMBOL as LAST_SYMBOL ,
            SECURITIES_NAME
        from 
            TBL_P2_EQUITY_TRANS
        WHERE 
            TRANS_DATE < '2017-01-18 23:59:59' AND SYMBOL='AP' AND SECURITIES_NAME='UOBAM' ORDER BY TRANS_DATE DESC) a 

         ON a.LAST_SYMBOL = b.SYMBOL 
           AND a.SECURITIES_NAME = b.SECURITIES_NAME
           AND b.SYMBOL='AP' AND b.SECURITIES_NAME ='UOBAM'
           AND (b.TRANS_DATE BETWEEN '2017-01-11 00:00:01' AND  '2017-01-18 23:59:59')             
         
           GROUP BY a.LAST_SYMBOL, a.LAST_UNIT_PURCHASE ) C 

      WHERE 
           SYMBOL='AP' AND SECURITIES_NAME ='UOBAM' 
           AND (TRANS_DATE BETWEEN '2017-01-01 00:00:01' AND  '2017-01-18 23:59:59')
 

          */
/*
 V6:
 ===

SELECT x.TRANS_DATE, x.SYMBOL,  x.BU, x.INDUS, x.UNIT_PURCHASE, x.PRICE_PURCHASE, x.TOTAL_PURCHASE, x.P_SUM + COALESCE(SUM(y.P_SUM),0) SUM_UNIT_PURCHASE
      , x.UNIT_SALE, x.PRICE_SALE, x.TOTAL_SALE, x.PROFIT_LOSS
FROM
(
    SELECT 
        SUM(UNIT_PURCHASE) P_SUM, 
        SYMBOL, 
        D.BU as BU, 
        D.INDUSTRIAL as INDUS, 
        TRANS_DATE, 
        UNIT_PURCHASE, 
        PRICE_PURCHASE, 
        TOTAL_PURCHASE, 
        UNIT_SALE,
        PRICE_SALE, TOTAL_SALE, PROFIT_LOSS
    FROM 
        TBL_P2_EQUITY_TRANS, 
        (SELECT BU, INDUSTRIAL FROM TBL_P2_EQUITY_INDEX WHERE SYMBOL='APCS'  ) D
    
    
    GROUP BY TRANS_DATE, UNIT_PURCHASE, SYMBOL, PRICE_PURCHASE, TOTAL_PURCHASE, D.BU, D.INDUSTRIAL, UNIT_SALE,
          PRICE_SALE, TOTAL_SALE, PROFIT_LOSS
) x

LEFT OUTER JOIN
(
    SELECT SUM(UNIT_PURCHASE) P_SUM, TRANS_DATE, UNIT_PURCHASE
    FROM TBL_P2_EQUITY_TRANS
    GROUP BY TRANS_DATE, UNIT_PURCHASE
) y ON y.TRANS_DATE < x.TRANS_DATE

WHERE SYMBOL='APCS'

GROUP BY x.TRANS_DATE, x.P_SUM, x.SYMBOL, x.INDUS, x.BU, x.UNIT_PURCHASE, x.PRICE_PURCHASE, x.TOTAL_PURCHASE, x.UNIT_SALE,
         x.PRICE_SALE, x.TOTAL_SALE, x.PROFIT_LOSS
 ORDER BY x.TRANS_DATE



 */

/*
 V7:
 ...
 ;WITH cte AS (
    SELECT 
        row_number() OVER (ORDER BY TRANS_DATE, SYMBOL) AS rownum,*
    FROM 
        TBL_P2_EQUITY_TRANS
)
SELECT
    a.TRANS_DATE, a.SYMBOL,  a.UNIT_PURCHASE, a.UNIT_SALE, 
    SUM(b.UNIT_PURCHASE) AS P_SUM,
    SUM(b.UNIT_SALE) AS S_SUM,
    SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as REMAIN_UNIT
FROM  cte a
LEFT JOIN cte b 
ON a.SYMBOL = b.SYMBOL AND b.rownum <= a.rownum
GROUP BY a.SYMBOL, a.TRANS_DATE, a.rownum, a.UNIT_PURCHASE, a.UNIT_SALE
ORDER BY a.SYMBOL, a.TRANS_DATE

*/ 
/*
V8:
==


;WITH cte AS (
    SELECT row_number() OVER (ORDER BY TRANS_DATE) AS rownum, x.*, k.INDUSTRIAL, k.BU 
    FROM TBL_P2_EQUITY_TRANS x, TBL_P2_EQUITY_INDEX  k
    WHERE x.SYMBOL = k.SYMBOL
)
SELECT
    a.TRANS_DATE, a.SYMBOL, a.SECURITIES_NAME, a.INDUSTRIAL, a.BU,
    a.UNIT_PURCHASE, a.PRICE_PURCHASE,a.TOTAL_PURCHASE,
    a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS,
    a.UNIT_DIV, a.TOTAL_DIV, 
    SUM(b.UNIT_PURCHASE) AS P_SUM,
    SUM(b.UNIT_SALE) AS S_SUM,
    SUM(b.UNIT_DIV) AS D_SUM,
    SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as REMAIN_UNIT
     
FROM  cte a 
            
    
LEFT JOIN cte b 

ON a.SYMBOL = b.SYMBOL AND b.rownum <= a.rownum
GROUP BY a.SYMBOL, a.TRANS_DATE, a.rownum, 
    a.UNIT_PURCHASE, a.PRICE_PURCHASE, a.TOTAL_PURCHASE,
    a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS,
    a.UNIT_DIV, a.TOTAL_DIV, a.SECURITIES_NAME,
    a.INDUSTRIAL, a.BU
ORDER BY a.SYMBOL, a.TRANS_DATE


*/
?>
