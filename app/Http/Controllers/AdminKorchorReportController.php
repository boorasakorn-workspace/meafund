<?php
/**
 * FILE: AdminKorchorReportController.php
 * Phase#2 รายงาน กช และกรานำเข้า แบบหลาย ไฟล์
 * Created: 2017/01/19 21:21
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\UserGroup;
use Illuminate\Support\Facades\Validator;
use Jenssegers\Date\Date;

use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Input;
//use App\Http\Controllers\Redirect;
use Illuminate\Http\UploadedFile;

use App\User;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

use App\Libraries\MEAUtils;

class AdminKorchorReportController extends Controller
{
    
    public function getindex()
    {
        $viewname = 'backend.pages.p2_64_1_korchor_report_page';
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 64,
            'menu_id' => 1,
            'title' => getMenuName($data, 64, 1) . ' | MEA'
        ]);

        $allquery = "SELECT * FROM TBL_P2_EQUITY_SECURITIES";
        $equitylist = DB::select(DB::raw($allquery));


        //
        $allquery = "SELECT * FROM TBL_P2_KC_REPORT_INDEX ORDER BY REPORT_ID;";
        $kcindex = DB::select(DB::raw($allquery));

        return view($viewname)->with([
            'equitylist'     =>$equitylist,
            'kcindex'        =>$kcindex
            ]);
    }
    
	
    public  function Ajax_Index(Request $request){

      /*  $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;

        $Datacount = $this->getCountAll();
        $Data = $this->getData($ArrParam);

        $totals = count($Datacount);

        $htmlPaginate = Paginatre_gen($totals,$PageSize,'page_click_search',$PageNumber);

        $returnHTML = view('backend.pages.ajax.ajax_p2_64_1_korchor_report')->with([
            'htmlPaginate'=> $htmlPaginate,
            'data'=>$Data

        ])->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML));*/
         Log::info(get_class($this) .'::'. __FUNCTION__);
      
        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $emp_id = $request->input('emp_id');
        $depart = $request->input('depart');
        $plan = $request->input('plan');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $check_name = $request->input('check_name');
        $check_depart = $request->input('check_depart');
        $check_plan = $request->input('check_plan');
        $check_date = $request->input('check_date');

        $ArrParam = array();
        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;
        $ArrParam["emp_id"] =$emp_id;
        $ArrParam["depart"] =$depart;
        $ArrParam["plan"] =$plan;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;

        $ArrParam["check_name"] =$check_name;
        $ArrParam["check_depart"] =$check_depart;
        $ArrParam["check_plan"] =$check_plan;
        $ArrParam["check_date"] =$check_date;


        $data =null;
        $totals= 0;

        $data = $this->DataSource($ArrParam,true);

        $totals = 1;// $this->DataSourceCount($ArrParam,true);

        $htmlPaginate =Paginatre_gen($totals, $PageSize,'page_click_search',$PageNumber);

        Log::info(get_class($this) .'::'. __FUNCTION__ . ' - Return:' . $ajax_view_name);
        $returnHTML = view($ajax_view_name)->with([
            'htmlPaginate'=> $htmlPaginate,
            'data' => $data,
            'totals' => $totals,
            'PageSize' =>$PageSize,
            'PageNumber' =>$PageNumber

        ])->render();

         return response()->json(array('success' => true, 'html'=>$returnHTML));
    }

    public function getCountAll() {

        return DB::table('TBL_P2_KC_REPORT')->orderby("PERIOD")->get();    
    }

    public function getData($ArrParam){

        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];



        $query =  "SELECT * FROM TBL_P2_KC_REPORT ORDER BY PERIOD OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";

        //$query = "TBL_P2_KC_REPORT "   

        return DB::select(DB::raw($query));
    }


     public function getimport() {
        $viewname = 'backend.pages.p2_64_1_korchor_data_import_page';
        $this->pageSetting( [
            'menu_group_id' => 64,
            'menu_id' => 1,
            'title' => 'นำเข้าข้อมูล | MEA'
        ]);

        $user_group = DB::table('TBL_PRIVILEGE')->select('USER_PRIVILEGE_ID','USER_PRIVILEGE_DESC')->orderBy('USER_PRIVILEGE_ID', 'asc')->get();

        return view($viewname)->with([
            'user_group' => $user_group
        ]);
    }


    /**
     * Handle request delete single/multiple records.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function delete(Request $request)
    {
        $deleted = false;
        $arrId = explode(',', $request->input('group_id'));

        foreach($arrId as $index => $item){

            if($item != ""){
                $deleted =  DB::table('TBL_P2_KC_REPORT')->where('REFERENCE',"=", $item)->delete();
            }
        }

        if($deleted)  {
            return response()->json(["ret" => "1"]);
        } else {
            return response()->json(["ret" => "0"]);
        }
    }

   //Ajax_Index_Search

    /**
     * Get view equity company .
     * 
     * @param  None
     * @return view\backend\pages "p2_tab1_add_bond_company_page"
     */
    public function getAdd()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 64,
            'menu_id' => 1,
            'title' => getMenuName($data,64, 1) . ' | MEA'
        ] );
        return view('backend.pages.p2_64_1_korchor_import_page');
    }

   
    /**
     * Receive POST command to add new equity company data
     * @param $request parameters list with fields 
     *        plan_start, plan_end, company_code, company_name,
     *        company_addr, company_phone, company_fax 
     */
    public function postAdd(Request $request)
    {
         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        Log::info('BondCompanyManagementController::PoseAdd::=>' . $request);
        Log::info(' postAdd: equity_start=>' . toEnglishDate($request["equity_start"])); 

        $ret = false;
        $datestart  = new Date(toEnglishDate($request["equity_start"]));
        $datereq = $request["equity_end"];
        
        if ($request["company_addr"] == '') {

        }

        if ($request["equity_start"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันเริ่มต้น ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        if ($request["equity_end"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันสิ้นสุด ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }
        

        Log::info(' postAdd: equity_start=>' . toEnglishDate($datereq));   
        $dateEnd = new Date(toEnglishDate($datereq));

        $today   = new Date();
        $data    = array();

        array_push($data,array(
            'NAME_SHT' => $request["name_sht"],
            //Securities_Name
            'SECURITIES_NAME' =>$request["company_name"],
            'ADDRESS' => $request["company_addr"],

            'START_DATE' => $datestart,
            'END_DATE' => $dateEnd,

            'PHONE' => $request["company_phone"],
            'FAX' => $request["company_fax"],
            'CREATE_DATE' => $today,
            'CREATE_BY' => "Admin"
        ));


        $chk = "SELECT COUNT(NAME_SHT) As total FROM TBL_P2_EQUITY_SECURITIES WHERE NAME_SHT = '". $request["name_sht"]. "'";
        $all = DB::select(DB::raw($chk));
        $total =  $all[0]->total;

        $rethtml = "";


        if ($total > 0) {
           $rethtml = "Short code ที่ท่านใส่มีอยู่ในระบบแล้ว";

        } else {
           $insert = DB::table('TBL_P2_EQUITY_SECURITIES')->insert($data);
           $ret = $insert;
        }


        return response()->json(array('success' => $ret, 'html'=>$rethtml));

    }


    
    /**
     *  Check uploaded file format
     *  @param  request $request
     *  @return json with status, details
     */
    public function T2_checkfile(Request $request) {
        /* limit execution timeout */
        ini_set('max_execution_time', 50000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $EQUITY_TBL = "1"; // นโยบายตราสารทุน
        $BOND_TBL = "2"; // นโยบายตราสารทุน

        Log::info(get_class($this) .'::'. __FUNCTION__. ' datatype: ' . $request->input('datatype') . ',  policy' .  $request->input('policy'));
        $results = null;
        $error_msg = '';
        $htmlResult = '';


        $passedCount = 1;
        $count = 0;
        $passedCount = 1;


       // $datatype = $request->input('datatype');//$request->get('datatype');
       // $policy = $request->input('policy'); 
       /* $files = array();
        for($i=0; $i<100; $i++) {
            if($request->has($i)) {
                $file.name = $request->input($i);
              array_push($files, 
            }
           //$files = $request->input('files');

        }*/
       // $input = $request->all();

        $status = true;
        Log::info('files:' . print_r($request->file, true));
        // Log::info('input:' . print_r($input, true));
        $filedate = date('Y-m-d');
       /* $process_file = '60_2_checkimport.xlsx';
        $request->file('exelimport')->move(storage_path() . '/public/import/', $process_file);
       
        $inputfile = storage_path('/public/import/' . $process_file);
        

        
        
        Log::info(get_class($this) .'::'. __FUNCTION__. ' Success ' ); 
        $status = true;
        ///////////////////////////////////
        $firstrow = '';
        $filedate = '';
      
        $arr = array();  
        if($policy == $EQUITY_TBL) {
           $arr = $this->T2_ImportInsertEquityNAV($inputfile, true);
           $status = $arr['status'];
           $filedate = $arr['filedate']; 
        } else if($policy == $BOND_TBL){
           $arr    = $this->T2_ImportInsertBondNAV($inputfile, true); 
           $status = $arr['status'];
           $filedate = $arr['filedate']; 
        } else {
           $filedate = '### ERROR: Invalid file format. ###';
           $status = false; 
        }*/
        //////////////////////////////////
        return response()->json(array('success' =>  $status, 'html'=>'' . 

                //' policy   : ' . $policy .
                //'<br/> datatype : ' . $datatype . 
                '<br/> status : ' .   ($status ? "เรียบร้อยดี" : "มีข้อผิดพลาด") . 
                '<br/> date : '     . $filedate . " <br/>" ));
        
    } // T2_checkfile

    public function multiple_upload(Request $request) {

        /* limit execution timeout */
        ini_set('max_execution_time', 150000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        // getting all of the post data
        $files = $request->file('images'); //Input::file('images');

        // Making counting of uploaded images
        $file_count = count($files);

        $farray = array();

        $recordsets = array();
        // start count how many uploaded
        $uploadcount = 0;
        $CREATE_DATE = date('Y-m-d');
        
        // Retrive logged ID
        $user_data = Session::get('user_data');
        $CREATE_BY = $user_data->emp_id;
        // Log::info('SESSION: [user_data] ='. $user_data->emp_id);
       //  $query = "SELECT WEB_NEWS_ROOT_PATH FROM TBL_CONTROL_CFG ";
         
        $cfg = DB::table('TBL_CONTROL_CFG')->get();
        $rootpath = '';
        foreach ($cfg as $key => $value) {
            # code...
            $rootpath = $value->WEB_NEWS_ROOT_PATH;
            break;
        }


        foreach($files as $file) {
            
            $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
            $validator = Validator::make(array('file'=> $file), $rules);

            if($validator->passes()){
                $path = public_path();
                $destinationPath = public_path() . '/contents/kcreport';
                $filename = $file->getClientOriginalName();
                $upload_success = $file->move($destinationPath, $filename);
                $uploadcount++;
                // array_push($farray, $filename);
                $vf = explode(".", $filename);
                $pieces = explode("_", $vf[0]);
                if(count($pieces) < 4)
                   break;

                $yyyy = substr($pieces[3], 0, 4);
                $mm = substr($pieces[3], 4);
                $dd = '01';
                $period = $yyyy . '-' . $mm .'-' . $dd;
                
                $record = array(
                    'REPORT_ID'       => trim($pieces[0]),   // varchar
                    'SECURITIES_NAME' => trim($pieces[1]),   // varchar
                    'REPORT_TYPE'     => trim($pieces[2]),   // varchar
                    'PERIOD'          => $period,      // date  
                    'CREATE_DATE'     => $CREATE_DATE, // date
                    'CREATE_BY'       => $CREATE_BY,
                    'PATH'            => $rootpath . 'kcreport/' . $filename,
                    'REFERENCE'       => $filename
                );

                array_push($recordsets, $record);
            }
        }



        if($uploadcount == $file_count){

            $uploadcount = 0;
            // SAVE DB
            $status = false;
            foreach ($recordsets as $rows => $record) {
                
                $link_array = explode('/', $record['PATH']);
                $filename = end($link_array);

                $chk = " SELECT ". 
                       "     COUNT(PATH) As total ". 
                       " FROM ". 
                       "     TBL_P2_KC_REPORT ".
                       " WHERE ".
                       "     REPORT_ID = '". $record['REPORT_ID'] . "' AND " .
                       "     SECURITIES_NAME = '" . $record['SECURITIES_NAME'] . "' AND " .
                       "     REPORT_TYPE = '" . $record['REPORT_TYPE'] . "' AND " .
                       "     PERIOD = '" . $record['PERIOD'] . "'"  ;
                        
                       
                $all = DB::select(DB::raw($chk));
                $total =  $all[0]->total;
                try {
                    if ($total > 0) {
                        Log::info("Short code ที่ท่านใส่มีอยู่ในระบบแล้ว" . $filename);
                        $update = DB::table('TBL_P2_KC_REPORT')
                                 ->where('REPORT_ID', "=", $record['REPORT_ID'])
                                 ->where('SECURITIES_NAME', "=", $record['SECURITIES_NAME'])
                                 ->where('REPORT_TYPE', "=", $record['REPORT_TYPE'])
                                 ->where('PERIOD', "=", $record['PERIOD'])
                                 ->update($record);
                        $status = ($update > 0);
                         
                    } else {
                        $affected = DB::table('TBL_P2_KC_REPORT')->insert($record);
                        $status = ($affected > 0);
                         
                    }

                    $uploadcount++;

                } catch (Illuminate\Database\QueryException $e) {
                    $errorCode = $e->errorInfo[1];
                    Log::info('QueryException:' . $e->getMessage());
                    $status = false;
                    
                } catch(\Exception $e) {
                    Log::info('Exception:' . $e->getMessage());
                    $status = false;
                }

                if(!$status)
                   array_push($farray,  $filename . '&nbsp; ' . ' *** ไม่สำเร็จ *** '  );
                else
                   array_push($farray,  $filename . '&nbsp; ' . 'สำเร็จ'  );
                

            }

            Session::flash('success', 'Upload successfully'); 
            Session::flash('uploadcount', $uploadcount);
            Session::flash('total', $file_count);
            Session::flash('files', $farray);
            return redirect('admin/AdminKorchorReport/getimport'); //->with(['count'=>$uploadcount, 'totlal' => $file_count]);
        } 
        else {
          return redirect('admin/AdminKorchorReport/getimport')->withInput()->withErrors($validator);
        }
    }

    public function T2_download_korchor() {
        $file = 'contents/sample/KC12_UOB_CONSO_201607.pdf';
        $headers = array(
            'Content-Type: application/pdf',
        );
        return \Response::download($file, 'KC12_UOB_CONSO_201607.pdf', $headers);
    }


    public function ajax_index_search(Request $request){
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $report_id = $request->input('report_id');
        $name_sht  = $request->input('plan');  // equirty_security->name_sht
        $date_start = $request->input('date_start');
        $operator = $request->input('operator');

        $utils = new MEAUtils();
        if (!empty($date_start) && (strlen($date_start) > 0))
        {
            $dt = $utils->toEnglishDate($date_start);

            $v = explode('-', $dt);
            // convert format: 1 ม.ค. 2017 to 2017-01-01
            if(count($v) == 3)
               $dt = $v[2] . '-' . str_pad(strval($v[1]), 2, '0', STR_PAD_LEFT) . '-' . '01';
            else 
               // Handle format "YYYY-mm" 
               $dt = $v[1] . '-' . str_pad(strval($v[0]), 2, '0', STR_PAD_LEFT) . '-' . '01';  
        } else {
            $dt = $date_start;
        }
        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;
        $ArrParam["report_id"] =$report_id;
        $ArrParam["name_sht"] =$name_sht;
        $ArrParam["date_start"] =  $dt; //$date_start;

        $ajax_view_name = "";

        $data = $this->DataSource($ArrParam, true);

        $totals = $this->DataSourceCount($ArrParam, true);

        $htmlPaginate =Paginatre_gen($totals,$PageSize,'page_click_search',$PageNumber);

        $returnHTML = view('backend.pages.ajax.ajax_p2_64_1_korchor_report')->with([
            'htmlPaginate'=> $htmlPaginate,
            'data'=> $data,
            'totals' => $totals,
            'PageSize' =>$PageSize,
            'PageNumber' =>$PageNumber

        ])->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }
  

    public  function  DataSourceCount($ArrParam, $IsCase){

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];

        $report_id =$ArrParam['report_id'];
        $name_sht = $ArrParam['name_sht'];
        $date_start = $ArrParam['date_start'];
        
        $where = " WHERE kc.REPORT_ID IS NOT NULL " . " AND  kc.REPORT_ID = idx.REPORT_ID ";


        if(!empty($report_id) && (strlen($report_id) > 0)) {
            $where .= " AND kc.REPORT_ID = '". $report_id ."' AND  kc.REPORT_ID = idx.REPORT_ID ";
        }
        
        if(!empty($name_sht) && (strlen($name_sht) > 0)) {   // TBL_P2_KC_REPORT_INDEX.REPORT_DESC

             $where .= " AND kc.SECURITIES_NAME  LIKE '". ((strlen($name_sht) > 3) ? substr($name_sht, 0, 3) : $name_sht) ."%'";
        }
        if(!empty($date_start) && (strlen($date_start) > 0)){
            $where .= " AND kc.PERIOD  BETWEEN '".$date_start." 00:00:00' AND '".$date_start." 23:59:59' ";
        }


//        $where = " WHERE (focus.EMP_ID = '".$emp_id."' OR em.DEP_SHT  = '".$depart."' OR new.PLAN_ID = '".$plan."' OR new.MODIFY_DATE BETWEEN '".$date_start."' AND '".$date_end."')";

        $allquery = " SELECT " . 
                    "     COUNT(kc.REPORT_ID ) AS total " .
                    " FROM  ". 
                    "     TBL_P2_KC_REPORT kc, TBL_P2_KC_REPORT_INDEX idx " ;
                    
        if($IsCase){
            $allquery .= $where;
        }

        Log::info($allquery);
        $all = DB::select(DB::raw($allquery));
        return  $all[0]->total;
    }

    public  function  DataSource($ArrParam, $IsCase, $ispageing= true){

        $where = "";
        if($ispageing){
            $PageSize = $ArrParam['pagesize'];
            $PageNumber = $ArrParam['PageNumber'];
        }

        if($IsCase) {

            $report_id =$ArrParam['report_id'];
            $name_sht = $ArrParam['name_sht'];
            $date_start = $ArrParam['date_start'];
        
            
            $where = " WHERE kc.REPORT_ID IS NOT NULL " ." AND  kc.REPORT_ID = idx.REPORT_ID ";

            if(!empty($report_id) && (strlen($report_id) > 0)) {
                $where .= " AND kc.REPORT_ID = '". $report_id . "'" ;
            }
            
            if(!empty($name_sht) && (strlen($name_sht) > 0)) {   // TBL_P2_KC_REPORT_INDEX.REPORT_DESC
                // $where .= " AND kc.SECURITIES_NAME  = '". $name_sht ."'";
                $where .= " AND kc.SECURITIES_NAME  LIKE '". ((strlen($name_sht) > 3) ? substr($name_sht, 0, 3) : $name_sht) ."%'";
            }
            if(!empty($date_start) && (strlen($date_start) > 0)){
                $where .= " AND kc.PERIOD  BETWEEN '".$date_start." 00:00:00' AND '".$date_start." 23:59:59' ";
            }
        }


        $query= " SELECT " . 
                    "     kc.REPORT_ID, " .
                    "     idx.REPORT_DESC, " .
                    "     kc.SECURITIES_NAME, " . 
                    "     kc.REFERENCE, " .
                    "     kc.PATH, " .
                    "     kc.PERIOD " . 
                    " FROM  ". 
                    "     TBL_P2_KC_REPORT kc, TBL_P2_KC_REPORT_INDEX idx " ;

        if($IsCase){
            $query .= $where;
        }

        $query .= " ORDER BY kc.PERIOD DESC ";

        if($ispageing){
            $query .=  " OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
        }

        Log::info($query);
        return DB::select(DB::raw($query));
    }


} // class 
/*
SAMPLE TRANSACTIONS
=====================
DB::transaction(function()
{
    $newAcct = Account::create([
        'accountname' => Input::get('accountname')
    ]);

    $newUser = User::create([
        'username' => Input::get('username'),
        'account_id' => $newAcct->id,
    ]);

    if( !$newUser )
    {
        throw new \Exception('User not created for account');
    }
});
*/
?>