<?php
/**
 * FILE: AdminP2BenefitsReportController.php
 * รายงาน ผลประโยชน์ และการลงทุน (Benefits Report) *
*/
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Jenssegers\Date\Date;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

use App\Libraries\MEAUtils;
use Illuminate\Support\Facades\Log;

class AdminP2BondBenefitsReportController extends Controller
{

    /** 
     * รายงาน ผลประโยชน์ และการลงทุน (Benefits Report)
     */
    public function getreport()
    {
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $view_name = 'backend.pages.p2_bond_benefits_report';
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 63,
            'menu_id' => 3,
            'title' => getMenuName($data, 62, 3) . '|  MEA FUND'
        ] );
       

        $allquery = "SELECT * FROM TBL_P2_BOND_SECURITIES";
        $equitylist = DB::select(DB::raw($allquery));
      
        $allquery = "SELECT * FROM TBL_P2_BOND_BROKER ORDER BY NAME_SHT ASC;";
        $equitybroker = DB::select(DB::raw($allquery));

        $allquery = "SELECT SYMBOL FROM TBL_P2_BOND_INDEX ORDER BY SYMBOL ASC;";
        $equityindex = DB::select(DB::raw($allquery));

        $allquery = "SELECT MAX(YEAR(REFERENCE_DATE)+1)   AS YMAX, " .
                    "       MIN(YEAR(REFERENCE_DATE)-1) AS YMIN ".
                    "FROM TBL_P2_BOND_PORTFOLIO ";
        $rs = DB::select(DB::raw($allquery));
        
        
        $years = array();
        foreach ($rs as $k => $v) {
            for($i=$v->YMIN; $i <=$v->YMAX; $i++) {
                array_push($years, $i);
            }
            break;
        }

        $categorylist = DB::table('TBL_P2_BOND_CATEGORY')->orderby("BOND_CATE_THA")->get();  

        $securitieslist =  DB::table('TBL_P2_BOND_SECURITIES')->orderby("NAME_SHT")->get(); 
        $sectionList = array( );
        $tmp = array();
        $key = "";

        $child = array();

        foreach ($categorylist as $row) {
            if($key != $row->BOND_CATE_THA) {
               if($key == "") {
                   $child = array();
               } else {
                  $sectionList[$key] = $child;
                  $child = array();
               }
            } 
            $key = $row->BOND_CATE_THA;
            array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'BOND_TYPE_THA'=>$row->BOND_TYPE_THA,
                                     'BOND_CATE_THA'=>$row->BOND_CATE_THA));
        }
        $sectionList[$key] = $child;

        return view($view_name)->with([
            'equitylist'     => $equitylist,
            'equitybroker'   => $equitybroker,
            'years'          => $years,
            'symbols'        => $equityindex,
            'securitieslist' => $securitieslist,
            'sectionList'    => $sectionList
        ]);

    }

    public  function  DataSourceCount($ArrParam, $IsCase) {
        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        Log::info(print_r($ArrParam, true));

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];

        $symbol     = $ArrParam["symbol"] ;     // APCS, CPALL
        $name_sht   = $ArrParam["name_sht"];    // eg. UOBAM, KTAM 
        $industrial = $ArrParam["industrial"];  // CATE_ID;
        $cateid     = $ArrParam["industrial"];  // CATE_ID;
        $age        = $ArrParam["age"];         // age
        $year       = $ArrParam["year"];        // year

        $bu = '';
        $indus = '';

        if(strlen(trim($cateid)) > 0) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
                           ->orderby("BOND_CATE_THA")
                           ->where('CATE_ID','=', $cateid)->get();
            foreach ($categorylist as $row) {
                $indus = $row->BOND_CATE_THA;
                $bu    = $row->BOND_TYPE_THA;
                break;  
            }
        }
        
        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 5) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 5) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        // WHERE2
        $where2 = "";
    
       if(strlen(trim($symbol)) > 0) { 
            $where2 .= " P.SYMBOL = '".$symbol."' ";
        }

        if(strlen(trim($name_sht)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " P.SECURITIES_NAME = '".$name_sht."' ";
        }

        if(strlen(trim($cateid)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND C.CATE_ID  = ". $cateid ." ";
            else
               $where2 .= " C.CATE_ID  = ". $cateid ." "; 
        }
        
        Log::info('1) #### AGE ##### =>[' . $age . "] LEN=" . strlen(trim($age)));
         if(strlen(trim($age)) > 0) {  
            $prefix = "";
            if(strlen($where2) > 1) {
                $prefix = " AND ";
            }
            $where2 .= $prefix;
            Log::info('2) #### AGE ##### = ' . $age);
            $agefield = " DATEDIFF(MONTH, GETDATE(), P.MAT) as AGE_MONTH ";
            switch(intval(trim($age))) {
                
                case 0;
                    // คงเหลือน้อยกว่าหรือเท่ากับ 3 เดือน (MAT เทียบกับ today >= 1 day <= 3 month)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/4)) ";
                   break;

                case 1:
                    // คงเหลือน้อยกว่า หรือเท่ากับ 6 เดือน
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/2)) ";
                    
                   break;

                case 2:
                    // คงเหลือน้อยกว่าหรือเท่ากับ 1 ปี (MAT เทียบกับ today >= 1 day <= 1 year)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;  

                case 3:
                    //  คงเหลือมากกว่า 6 เดือน แต่น้อยกว่า หรือเท่ากับ 1 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= (365/2)) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;

                case 4:
                    // คงเหลือมากกว่า 1 ปี แต่น้อยกว่า หรือเท่ากับ 3 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 365) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*3)) ";
                    break;

                case 5:
                    // คงเหลือมากกว่า 3 ปี แต่น้อยกว่า หรือเท่ากับ 5 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= (365*3)) " .
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*5)) ";
                    break;

                case 6:
                    // คงเหลือมากกว่า 5 ปีขึ้นไป
                    $where2  .= "  (DATEDIFF(DAY, GETDATE(), P.MAT ) >= (365*5)) ";
                    break;

                default:
                   break;
                
            } // switch
        } // if age

        Log::info('3) #### AGE ##### where2 =>[' . $where2 . "] LEN=" . strlen(trim($where2))); 
        if(strlen(trim($year)) > 0) { 
             Log::info('4) #### AGE ##### year =>[' . $year . "] LEN=" . strlen(trim($year))); 
            if(strlen($where2) > 1) 
               $where2 .= " AND  YEAR(P.REFERENCE_DATE) = ". trim($year) ." ";
            else
               $where2 .= " YEAR(P.REFERENCE_DATE) = ". trim($year) . " ";
        } else {
            if(strlen($where2) > 1) 
               $where2 .= " AND  YEAR(P.REFERENCE_DATE) = " . date('Y') . " ";
            else
               $where2 .= " YEAR(P.REFERENCE_DATE) = " . date('Y') . " ";
        }
        
        //if(strlen($where2) > 0) {
        //    $where2 = " AND " . $where2; 
        //}
        Log::info('5) #### AGE ##### year =>[' . $year . "] LEN=" . strlen(trim($year))); 
        $query = " SELECT 
                   COUNT(SECURITIES_NAME) as total
                FROM (
                    SELECT
                        SYMBOL, 
                        UNREALIZED_PL,
                        TMonth,
                        SECURITIES_NAME,
                        TYPE,
                        MAT,               
                        ISSUER,
                        BOND_TYPE_THA
                    FROM ( 
                        SELECT
                            SYMBOL, 
                            BOND_TYPE_THA,
                            UNREALIZED_PL,
                            MONTH(REFERENCE_DATE) as TMonth,
                            SECURITIES_NAME,
                            TYPE,
                            MAT,               
                            ISSUER, 
                            ROW_NUMBER() OVER (PARTITION BY  MONTH(REFERENCE_DATE), DAY(REFERENCE_DATE) ORDER BY DAY(REFERENCE_DATE) DESC) AS rowNum
                        FROM (  SELECT
                                    P.*, C.BOND_TYPE_THA as BOND_TYPE_THA
                                FROM
                                      TBL_P2_BOND_PORTFOLIO P, TBL_P2_BOND_INDEX I, TBL_P2_BOND_CATEGORY C 
                                WHERE
                                    P.SYMBOL = I.SYMBOL AND I.CATE_ID = C.CATE_ID
                                    AND REFERENCE_DATE IN (
                                         SELECT   MAX(REFERENCE_DATE)
                                         FROM     TBL_P2_BOND_PORTFOLIO
                                         WHERE " . $where2 . "
                                         GROUP BY MONTH(REFERENCE_DATE)
                                    ) ) X 
                     GROUP BY SYMBOL, REFERENCE_DATE, UNREALIZED_PL, SECURITIES_NAME, TYPE, MAT, ISSUER, BOND_TYPE_THA ) z
                     
                  WHERE rowNum >= 1 ) source  
                 
          PIVOT (
                 MAX(UNREALIZED_PL)
                 FOR TMonth
                 IN ( [1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12] )
          ) AS pvtMonth "; 
        Log::info(__FUNCTION__ . ' :: SQL=' . $query);
        $all = DB::select(DB::raw($query));
        

        Log::info(__FUNCTION__  .' :: TOAL=' . $all[0]->total );
        return  $all[0]->total;

    }

    public  function  DataSource($ArrParam, $IsCase, $ispageing = true) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');
        $agefield = ""; 
        
        $where = "";
        if($ispageing){
            $PageSize = $ArrParam['pagesize'];
            $PageNumber = $ArrParam['PageNumber'];
        }

        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
       
        
        // NEED TO SPLIT BU + INDUSTRIAL 
        $name_sht = $ArrParam["name_sht"];  // CATE_ID;
        $industrial = $ArrParam["industrial"];  // CATE_ID;
        $cateid = $ArrParam["industrial"];  // CATE_ID;
        $age = $ArrParam["age"];  
        $year = $ArrParam["year"];
        $bu = '';
        $indus = '';

        if(strlen($cateid) > 0) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
                           ->orderby("BOND_CATE_THA")
                           ->where('CATE_ID','=', $cateid)->get();
            foreach ($categorylist as $row) {
                $indus = $row->BOND_CATE_THA;
                $bu    = $row->BOND_TYPE_THA;
                break;  
            }
        }

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        
        //////////
        $where2 = "" ;
       
        if(strlen(trim($symbol)) > 0) { // && $check_name== "true"){
            $where2 .= " P.SYMBOL = '".$symbol."' ";
             
        }

        if(strlen(trim($name_sht)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " P.SECURITIES_NAME = '".$name_sht."' ";
        }

        if(strlen(trim($cateid)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND C.CATE_ID  = ". $cateid ." ";
            else
               $where2 .= " C.CATE_ID  = ". $cateid ." "; 
        }
        
        Log::info('1) #### AGE ##### =>[' . $age . "] LEN=" . strlen($age));
         if(strlen(trim($age)) > 0) {  
            $prefix = "";
            if(strlen($where2) > 1) {
                $prefix = " AND ";
            }
            $where2 .= $prefix;
            Log::info('2) #### AGE ##### = ' . $age);
            $agefield = " DATEDIFF(MONTH, GETDATE(), P.MAT) as AGE_MONTH ";
            switch(intval(trim($age))) {
                
                case 0;
                    // คงเหลือน้อยกว่าหรือเท่ากับ 3 เดือน (MAT เทียบกับ today >= 1 day <= 3 month)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/4)) ";
                   break;

                case 1:
                    // คงเหลือน้อยกว่า หรือเท่ากับ 6 เดือน
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/2)) ";
                    
                   break;

                case 2:
                    // คงเหลือน้อยกว่าหรือเท่ากับ 1 ปี (MAT เทียบกับ today >= 1 day <= 1 year)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;  

                case 3:
                    //  คงเหลือมากกว่า 6 เดือน แต่น้อยกว่า หรือเท่ากับ 1 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= (365/2)) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;

                case 4:
                    // คงเหลือมากกว่า 1 ปี แต่น้อยกว่า หรือเท่ากับ 3 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 365) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*3)) ";
                    break;

                case 5:
                    // คงเหลือมากกว่า 3 ปี แต่น้อยกว่า หรือเท่ากับ 5 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= (365*3)) " .
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*5)) ";
                    break;

                case 6:
                    // คงเหลือมากกว่า 5 ปีขึ้นไป
                    $where2  .= "  (DATEDIFF(DAY, GETDATE(), P.MAT ) >= (365*5)) ";
                    break;

                default:
                   break;
                
            } // switch
        } // if age

        if(strlen(trim($year)) > 0) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND  YEAR(P.REFERENCE_DATE) = ". trim($year) . " ";
            else
               $where2 .= " YEAR(P.REFERENCE_DATE) = ". trim($year) . " ";
        }
/*
        if((strlen($date_start) > 0) && (strlen($date_end) >0)) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND  P.REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " P.REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
 */       
        
       // if(strlen($where2) > 0) {
       //     $where2 = "" . $where2; 
       // }
        $offset = "";
        $agefield .= (strlen($agefield) > 1) ? "," : $agefield ;
        //if($ispageing){
        //    $offset = " OFFSET " . $PageSize . " * (" . $PageNumber . " - 1) ROWS FETCH NEXT " . $PageSize . " ROWS ONLY OPTION (RECOMPILE) ";
       // }
        $query = "
                SELECT
                    SECURITIES_NAME,
                    SYMBOL,
                    ISSUER,
                    MAT, 
                    BOND_TYPE_THA,
                    ISNULL([1],  0) AS Jan,
                    ISNULL([2],  0) AS Feb,
                    ISNULL([3],  0) AS Mar,
                    ISNULL([4],  0) AS Apr,
                    ISNULL([5],  0) AS May,
                    ISNULL([6],  0) AS Jun,
                    ISNULL([7],  0) AS Jul,
                    ISNULL([8],  0) AS Aug,
                    ISNULL([9],  0) AS Sep,
                    ISNULL([10], 0) AS Oct,
                    ISNULL([11], 0) AS Nov,
                    ISNULL([12], 0) AS Dec 
                FROM (
                    SELECT
                        SYMBOL, 
                        UNREALIZED_PL,
                        TMonth,
                        SECURITIES_NAME,
                        TYPE,
                        MAT,               
                        ISSUER,
                        BOND_TYPE_THA
                     FROM ( 
                        SELECT
                            SYMBOL, 
                            BOND_TYPE_THA,
                            UNREALIZED_PL,
                            MONTH(REFERENCE_DATE) as TMonth,
                            SECURITIES_NAME,
                            TYPE,
                            MAT,               
                            ISSUER, 
                            ROW_NUMBER() OVER (PARTITION BY  MONTH(REFERENCE_DATE), DAY(REFERENCE_DATE) ORDER BY DAY(REFERENCE_DATE) DESC) AS rowNum
                        FROM (  SELECT
                                    P.*, C.BOND_TYPE_THA as BOND_TYPE_THA
                                FROM
                                    TBL_P2_BOND_PORTFOLIO P, TBL_P2_BOND_INDEX I, TBL_P2_BOND_CATEGORY C 
                                WHERE
                                    P.SYMBOL = I.SYMBOL AND I.CATE_ID = C.CATE_ID
                                    AND REFERENCE_DATE IN (
                                        SELECT   
                                            MAX(REFERENCE_DATE)
                                        FROM     
                                            TBL_P2_BOND_PORTFOLIO
                                        WHERE " . $where2 . "
                                        GROUP BY MONTH(REFERENCE_DATE)
                                    )) X 
                        GROUP BY SYMBOL, REFERENCE_DATE, UNREALIZED_PL, SECURITIES_NAME, TYPE, MAT, ISSUER, BOND_TYPE_THA) z
                  WHERE rowNum >= 1 ) source  
                 
          PIVOT (
                 MAX(UNREALIZED_PL)
                 FOR TMonth
                 IN ( [1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12] )
          ) AS pvtMonth "; 

        if($ispageing){
            $query .= " ORDER BY MAT OFFSET " . $PageSize . " * (" . $PageNumber . " - 1) ROWS FETCH NEXT " . $PageSize . " ROWS ONLY OPTION (RECOMPILE) ";
        }

        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }

    
     public  function  DataSourceGrandTotal($ArrParam, $IsCase, $ispageing = false) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');
        $agefield = ""; 
        
        $where = "";
        /*if($ispageing){
            $PageSize = $ArrParam['pagesize'];
            $PageNumber = $ArrParam['PageNumber'];
        }
*/
        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
       
        
        // NEED TO SPLIT BU + INDUSTRIAL 
        $name_sht = $ArrParam["name_sht"];  // CATE_ID;
        $industrial = $ArrParam["industrial"];  // CATE_ID;
        $cateid = $ArrParam["industrial"];  // CATE_ID;
        $age = $ArrParam["age"];  
        $year = $ArrParam["year"];
        $bu = '';
        $indus = '';

        if(strlen($cateid) > 0) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
                           ->orderby("BOND_CATE_THA")
                           ->where('CATE_ID','=', $cateid)->get();
            foreach ($categorylist as $row) {
                $indus = $row->BOND_CATE_THA;
                $bu    = $row->BOND_TYPE_THA;
                break;  
            }
        }

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        
        //////////
        $where2 = "" ;
       
        if(strlen(trim($symbol)) > 0) { // && $check_name== "true"){
            $where2 .= " P.SYMBOL = '".$symbol."' ";
             
        }

        if(strlen(trim($name_sht)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " P.SECURITIES_NAME = '".$name_sht."' ";
        }

        if(strlen(trim($cateid)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND C.CATE_ID  = ". $cateid ." ";
            else
               $where2 .= " C.CATE_ID  = ". $cateid ." "; 
        }
        
        Log::info('1) #### AGE ##### =>[' . $age . "] LEN=" . strlen($age));
         if(strlen(trim($age)) > 0) {  
            $prefix = "";
            if(strlen($where2) > 1) {
                $prefix = " AND ";
            }
            $where2 .= $prefix;
            Log::info('2) #### AGE ##### = ' . $age);
            $agefield = " DATEDIFF(MONTH, GETDATE(), P.MAT) as AGE_MONTH ";
            switch(intval(trim($age))) {
                
                case 0;
                    // คงเหลือน้อยกว่าหรือเท่ากับ 3 เดือน (MAT เทียบกับ today >= 1 day <= 3 month)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/4)) ";
                   break;

                case 1:
                    // คงเหลือน้อยกว่า หรือเท่ากับ 6 เดือน
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/2)) ";
                    
                   break;

                case 2:
                    // คงเหลือน้อยกว่าหรือเท่ากับ 1 ปี (MAT เทียบกับ today >= 1 day <= 1 year)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;  

                case 3:
                    //  คงเหลือมากกว่า 6 เดือน แต่น้อยกว่า หรือเท่ากับ 1 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= (365/2)) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;

                case 4:
                    // คงเหลือมากกว่า 1 ปี แต่น้อยกว่า หรือเท่ากับ 3 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 365) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*3)) ";
                    break;

                case 5:
                    // คงเหลือมากกว่า 3 ปี แต่น้อยกว่า หรือเท่ากับ 5 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= (365*3)) " .
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*5)) ";
                    break;

                case 6:
                    // คงเหลือมากกว่า 5 ปีขึ้นไป
                    $where2  .= "  (DATEDIFF(DAY, GETDATE(), P.MAT ) >= (365*5)) ";
                    break;

                default:
                   break;
                
            } // switch
        } // if age

        if(strlen(trim($year)) > 0) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND  YEAR(P.REFERENCE_DATE) = ". trim($year) . " ";
            else
               $where2 .= " YEAR(P.REFERENCE_DATE) = ". trim($year) . " ";
        }
/*
        if((strlen($date_start) > 0) && (strlen($date_end) >0)) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND  P.REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " P.REFERENCE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
 */       
        
       // if(strlen($where2) > 0) {
       //     $where2 = "" . $where2; 
       // }
        $offset = "";
        $agefield .= (strlen($agefield) > 1) ? "," : $agefield ;
       
        $query = "
                SELECT
                    SUM([1]) AS Jan,
                    SUM([2]) AS Feb,
                    SUM([3]) AS Mar,
                    SUM([4]) AS Apr,
                    SUM([5]) AS May,
                    SUM([6]) AS Jun,
                    SUM([7]) AS Jul,
                    SUM([8]) AS Aug,
                    SUM([9]) AS Sep,
                    SUM([10]) AS Oct,
                    SUM([11]) AS Nov,
                    SUM([12]) AS Dec 
                FROM (
                    SELECT
                        SYMBOL, 
                        UNREALIZED_PL,
                        TMonth,
                        SECURITIES_NAME,
                        TYPE,
                        MAT,               
                        ISSUER,
                        BOND_TYPE_THA
                     FROM ( 
                        SELECT
                            SYMBOL, 
                            BOND_TYPE_THA,
                            UNREALIZED_PL,
                            MONTH(REFERENCE_DATE) as TMonth,
                            SECURITIES_NAME,
                            TYPE,
                            MAT,               
                            ISSUER, 
                            ROW_NUMBER() OVER (PARTITION BY  MONTH(REFERENCE_DATE), DAY(REFERENCE_DATE) ORDER BY DAY(REFERENCE_DATE) DESC) AS rowNum
                        FROM (  SELECT
                                    P.*, C.BOND_TYPE_THA as BOND_TYPE_THA
                                FROM
                                    TBL_P2_BOND_PORTFOLIO P, TBL_P2_BOND_INDEX I, TBL_P2_BOND_CATEGORY C 
                                WHERE
                                    P.SYMBOL = I.SYMBOL AND I.CATE_ID = C.CATE_ID
                                    AND REFERENCE_DATE IN (
                                        SELECT   
                                            MAX(REFERENCE_DATE)
                                        FROM     
                                            TBL_P2_BOND_PORTFOLIO
                                        WHERE " . $where2 . "
                                        GROUP BY MONTH(REFERENCE_DATE)
                                    )) X 
                        GROUP BY SYMBOL, REFERENCE_DATE, UNREALIZED_PL, SECURITIES_NAME, TYPE, MAT, ISSUER, BOND_TYPE_THA) z
                  WHERE rowNum >= 1 ) source  
                 
          PIVOT (
                 MAX(UNREALIZED_PL)
                 FOR TMonth
                 IN ( [1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12] )
          ) AS pvtMonth "; 

        if($ispageing){
            $query .= " ORDER BY MAT OFFSET " . $PageSize . " * (" . $PageNumber . " - 1) ROWS FETCH NEXT " . $PageSize . " ROWS ONLY OPTION (RECOMPILE) ";
        }

        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }

    public function ajax_report_search(Request $request)
    {
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $data = getmemulist();
        $title = getMenuName($data, 63, 3);

        $view_name = 'backend.pages.ajax.ajax_p2_bond_benefits_report';

        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $name_sht   = $request->input('name_sht');
        $industrial = $request->input('industrial');
        $cateid     = $request->input('industrial');
        $symbol     = $request->input('symbol');
        $age        = $request->input('age');
        $year       = $request->input('year');

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $ArrParam = array();
        $ArrParam["pagesize"] = $PageSize;
        $ArrParam["PageNumber"] = $PageNumber;
        $ArrParam["name_sht"] = $name_sht;
        $ArrParam["industrial"] = $industrial;
        $ArrParam["cateid"] = $cateid;
        $ArrParam["symbol"] = $symbol;
        $ArrParam["age"]  = $age;
       
        $ArrParam["date_start"] = date('Y-m-d');//$date_start;
        $ArrParam["date_end"] = date('Y-m-d');//$date_end;

        $data =null;
        $totals= 0;
        $strYear = $year;
         if(strlen(trim($strYear)) <1) {
            $strYear = date('Y');
         }
        $ArrParam["year"] = $strYear;
        $totals = $this->DataSourceCount($ArrParam, true);
        $data = $this->DataSource($ArrParam, true);
        $grandTotal = $this->DataSourceGrandTotal($ArrParam, false);

        $htmlPaginate = Paginatre_gen($totals, $PageSize, 'page_click_search', $PageNumber);

        $utils = new MEAUtils();
         
        $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
        $thai_date_end   = (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";
        Log::info(get_class($this) .'::'. __FUNCTION__.
            ' converted date: ' . $date_start .' to ' . $thai_date_start . ', ' .
            $date_end . ' to '. $thai_date_end );
        
        $pretty_date = $utils->prettyPrintThaiDate($thai_date_start, $thai_date_end, "red");
        
        $returnHTML = view($view_name)->with([
            'htmlPaginate'=> $htmlPaginate,
            'data' => $data,
            'totals' => $totals,
            'grandTotal' => $grandTotal,
            'PageSize' =>$PageSize,
            'PageNumber' =>$PageNumber,
            'date_start' => $thai_date_start,
            'date_end' => $thai_date_end,
            'pretty_date' => $pretty_date,
            'TableTitle' => $title,
            'max_columns' => 4,
            'year' => $strYear

        ])->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }


    public function getChartLabels() {
        $ar = array();
        array_push($ar, 'พลังงาน');
        array_push($ar, 'ท่องเที่ยว');
        array_push($ar, 'บริการ');
        array_push($ar, 'ทรัพยากร');
        array_push($ar, 'ธุรกิจการเงิน');
        array_push($ar, 'สินค้าอุตสาหกรรม');
        array_push($ar, 'อสังหาริมทรัพย์');
        array_push($ar, 'เกษตร');
        array_push($ar, 'เทคโนโลยี');
        array_push($ar, 'อุตสาหกรรมอาหาร');
        array_push($ar, 'ก่อสร้าง');
        array_push($ar, 'อุปโภคบริโภค'); 
        return $ar;
    }

    public function getChartDataSource() {
        /// FAKE: data
        $data1 = array();
        array_push($data1, 1420);
        array_push($data1, 1030);
        array_push($data1, 1060);
        array_push($data1, 1340);
        array_push($data1, 1480);
        array_push($data1, 2440);
        array_push($data1, -2353);
        array_push($data1, 4623);
        array_push($data1, -1215);
        array_push($data1, 3617);
        array_push($data1, -1297);
        array_push($data1, 5586);

        $data2 = array();
        array_push($data2, 3440);
        array_push($data2, 6760);
        array_push($data2, -3540);
        array_push($data2, 5580);
        array_push($data2, 5570);
        array_push($data2, -5520);
        array_push($data2, -4433);
        array_push($data2, 1013);
        array_push($data2, 1555);
        array_push($data2, 1087);
        array_push($data2, -1676);
        array_push($data2, -6476);

        $data = array();
        array_push($data, $data1, $data2);

        return $data;
    }

    public function ajax_chart1_search(Request $request) {
        
        $name_sht   = $request->input('name_sht');
        $industrial = $request->input('industrial');
        $cateid     = $request->input('industrial');
        $symbol     = $request->input('symbol');
        $age        = $request->input('age');
        $year       = $request->input('year');

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');


        $ArrParam = array();
        $ArrParam["year"] = $year;
        $ArrParam["name_sht"] = $name_sht;
        $ArrParam["industrial"] = $industrial;
        $ArrParam["cateid"] = $cateid;
        $ArrParam["symbol"] = $symbol;
        $ArrParam["age"]  = $age;
        $ArrParam["year"] = $year;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;

        $data = null;
        $totals = 1;         

        $utils = new MEAUtils();
        $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
        $thai_date_end =  (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";
        
        Log::info(get_class($this) . '::' . __FUNCTION__ .
            ' Converted Date: ' . $date_start .' TO ' . $thai_date_start . ', ' . $date_end . ' TO '. $thai_date_end );

        $data  = array();
       
        $labels = $this->getChartLabels();
        $data = $this->getChartDataSource();
        $pretty_date = $utils->prettyPrintThaiDate($thai_date_start, $thai_date_end, "");
        $result = [
                'date_start' => $thai_date_start,
                'date_end' => $thai_date_end,
                'pretty_date' => $pretty_date,
                'labels' => $labels,
                'data' => $data      
        ];
                      
        return response()->json(array('success' => true, 'result'=>$result));
    }


    public function ajax_report1_search_export() {
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $ArrParam = array();
        $ArrParam["pagesize"] ="";
        $ArrParam["PageNumber"] ="";
        
        $symbol     = $ArrParam["symbol"] ;     // APCS, CPALL
        $name_sht   = $ArrParam["name_sht"];    // eg. UOBAM, KTAM 
        $industrial = $ArrParam["industrial"];  // CATE_ID;
        $cateid     = $ArrParam["industrial"];  // CATE_ID;
        $age        = $ArrParam["age"];         // age
        $year       = $ArrParam["year"];        // year

        /*
        $ArrParam["emp_id"] =Input::get("EmpID");
        $ArrParam["depart"] =Input::get("depart");
        $ArrParam["plan"] =Input::get("plan");
        $ArrParam["date_start"] =Input::get("date_start");
        $ArrParam["date_end"] =Input::get("date_end");
        
        $ArrParam["check_name"] =Input::get("check_name");
        $ArrParam["check_depart"] =Input::get("check_depart");
        $ArrParam["check_plan"] =Input::get("check_plan");
        $ArrParam["check_date"] =Input::get("check_date");
        */
        // $sql2 = "SELECT TOP  5 * FROM  TBL_MEMBER_BENEFITS WHERe EMP_ID = '".get_userID()."' ORDER BY RECORD_DATE ASC";

        $data = $this->DataSource($ArrParam, true, false);
        $results = array();
        foreach ($data as $item) {
//            $item->filed1 = 'some modification';
//            $item->filed2 = 'some modification2';
            $results[] = (array)$item;

           // $results[5] = get_date_notime($item->modify_new);

            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }



        Excel::create('ExcelExport', function ($excel) use ($results,$ArrParam){

            $excel->sheet('Sheetname', function ($sheet) use ($results,$ArrParam){

                //format
//                $sheet->setColumnFormat(array(
//                    'D' => '0.00',
//
//                ));
                //
                // first row styling and writing content
                $sheet->mergeCells('A1:J1');
                $sheet->mergeCells('A2:J2');
                $sheet->mergeCells('A3:J3');
                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Comic Sans MS');
                    $row->setFontSize(20);
                    $row->setAlignment('center');
                });


                $sheet->row(2, function ($row) {$row->setAlignment('center');});
                $sheet->row(3, function ($row) {$row->setAlignment('center');});
//                $header = array('รายชื่อสมาชิกเปลี่ยนอัตราสะสม');

                $sheet->row(1, array('รายชื่อสมาชิกเปลี่ยนอัตราสะสม'));

                if($ArrParam["check_date"] == "true" && $ArrParam["date_start"] != "" && $ArrParam["date_end"] != ""){
                    $sheet->row(2, array('ในช่วงวันที่ ' . get_date_notime($ArrParam["date_start"]) . ' ถึง ' . get_date_notime($ArrParam["date_end"])   ));
                }


                $sheet->row(3, array('รายงานข้อมูล ณ วันที่ ' . get_date_notime(date("Y-m-d H:i:s"))));

                $header[] = null;
                $header[0] = 'รหัสพนักงาน';
                $header[1] = "ชื่อ-นามสกุล";
                $header[2] = "หน่วยงาน";
                $header[3] = "อัตราสะสม(เดิม)";
                $header[4] = "อัตราสะสม(ใหม่)";
                $header[5] = "วันที่ทำรายการ";
                $header[6] = "วันที่มีผล";
                $header[7] = "ผู้ทำรายการ";
                $header[8] = "สถานะ";
                $header[9] = "วันที่พ้นสภาพ";


                $sheet->row(4, $header);




                foreach ($results as $user) {


                    $sheet->appendRow($user);
                }



            });

        })->download('xls');
    }
}
/*

SELECT
    SYMBOL,
  --  TDay, 
    ISNULL([1],  0) AS Jan,
    ISNULL([2],  0) AS Feb,
    ISNULL([3],  0) AS Mar,
    ISNULL([4],  0) AS Apr,
    ISNULL([5],  0) AS May,
    ISNULL([6],  0) AS Jun,
    ISNULL([7],  0) AS Jul,
    ISNULL([8],  0) AS Aug,
    ISNULL([9],  0) AS Sep,
    ISNULL([10], 0) AS Oct,
    ISNULL([11], 0) AS Nov,
    ISNULL([12], 0) AS Dec
FROM (
  SELECT
     SYMBOL, 
     UNREALIZED_PL,
     TMonth
     
     FROM ( 
       SELECT
               SYMBOL, 
               
               UNREALIZED_PL,
               MONTH(REFERENCE_DATE) as TMonth,
                
               ROW_NUMBER() OVER (PARTITION BY  MONTH(REFERENCE_DATE), DAY(REFERENCE_DATE) ORDER BY DAY(REFERENCE_DATE) DESC) AS rowNum
            FROM ( SELECT
                      *
                    FROM
                      TBL_P2_BOND_PORTFOLIO
                    WHERE
                      REFERENCE_DATE IN (
                         SELECT   MAX(REFERENCE_DATE)
                         FROM     TBL_P2_BOND_PORTFOLIO
                         WHERE YEAR(REFERENCE_DATE) = 2017
                         GROUP BY MONTH(REFERENCE_DATE)
                    ) ) X 
     GROUP BY SYMBOL, REFERENCE_DATE, UNREALIZED_PL ) z
     
  WHERE rowNum >= 1 ) source  
 
  PIVOT (
         MIN(UNREALIZED_PL)
         FOR TMonth
         IN ( [1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12] )
  ) AS pvtMonth
*/
?> 




