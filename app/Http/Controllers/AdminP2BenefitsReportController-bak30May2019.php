<?php
/**
 * FILE: AdminP2BenefitsReportController.php
 * รายงาน ผลประโยชน์ และการลงทุน (Benefits Report) *
*/
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Jenssegers\Date\Date;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

use App\Libraries\MEAUtils;
use Illuminate\Support\Facades\Log;

class AdminP2BenefitsReportController extends Controller
{

    /** 
     * รายงาน ผลประโยชน์ และการลงทุน (Benefits Report)
     */
    public function getreport1()
    {
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $view_name = 'backend.pages.p2_benefits_report';
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 62,
            'menu_id' => 6,
            'title' => getMenuName($data,62,6) . '|  MEA FUND'
        ] );
       

        $allquery = "SELECT * FROM TBL_P2_EQUITY_SECURITIES";
        $equitylist = DB::select(DB::raw($allquery));


        $allquery = "SELECT DISTINCT(INDUSTRIAL) FROM TBL_P2_EQUITY_CATEGORY" ; //"SELECT DISTINT(INDUSTRIAL) FROM TBL_P2_EQUITY_CATEGORY";
        $equitycategory = DB::select(DB::raw($allquery));


        $allquery = "SELECT * FROM TBL_P2_EQUITY_BROKER ORDER BY NAME_SHT ASC;";
        $equitybroker = DB::select(DB::raw($allquery));

        $allquery  = " SELECT MIN(YEAR(TRANS_DATE)) as YEAR_MIN, MAX(YEAR(TRANS_DATE)) as YEAR_MAX FROM TBL_P2_EQUITY_TRANS ";
        $eqTRANS   = DB::select(DB::raw($allquery));
        $allquery  = " SELECT MIN(YEAR(REFERENCE_DATE)) as YEAR_MIN, MAX(YEAR(REFERENCE_DATE)) as YEAR_MAX FROM TBL_P2_EQUITY_NAV ";
        $eqNAV     = DB::select(DB::raw($allquery));

         
        $years = array();
        $cyear = date('Y'); 
        /*for($i=0; $i<15; $i++)
        {
           $years[$i] =  $cyear - $i;    
        } */
        
        $minYearTRANS = $cyear;
        $maxYearTRANS = $cyear;
        $minYearNAV   = $cyear;
        $maxYearNAV   = $cyear;
        
        if(count($eqTRANS) > 0) {
            $minYearTRANS = ($eqTRANS[0]->YEAR_MIN == null) ? $cyear : $eqTRANS[0]->YEAR_MIN;
            $maxYearTRANS = ($eqTRANS[0]->YEAR_MAX == null) ? $cyear : $eqTRANS[0]->YEAR_MAX;
        }

        if(count($eqNAV) > 0) {
            $minYearNAV = ($eqNAV[0]->YEAR_MIN == null) ? $cyear : $eqNAV[0]->YEAR_MIN;
            $maxYearNAV = ($eqNAV[0]->YEAR_MAX == null) ? $cyear : $eqNAV[0]->YEAR_MAX;;
        }

        $arrayYYYY = array($minYearTRANS, $maxYearTRANS, $minYearNAV, $maxYearNAV);
       //  Log::Info(' >>>>> ' . print_r($arrayYYYY, true));
        $years = range(min($arrayYYYY),  max($arrayYYYY));

        //range($eqTRANS[0]->YEAR_MIN, $eqTRANS[0]->YEAR_MAX);

        return view($view_name)->with([
            'equitylist'     => $equitylist,
            'equitycategory' => $equitycategory,
            'equitybroker'   => $equitybroker,
            'years' => $years
            ]);

    }

   public  function  DataSourceCount($ArrParam,$IsCase){

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];

        $reporttype = $ArrParam["reporttype"];
        $category = $ArrParam["category"];
        $equity_name = $ArrParam["equity_name"];
        $choose_year = $ArrParam["choose_year"];

        $where = "";

        if(!empty($category)&& strlen($category)> 0) {
            $where .= " AND C.INDUSTRIAL = '".$category."'";
        }
        if(!empty($equity_name) && strlen($equity_name)>0){
            $where .= " AND G.SECURITIES_NAME  = '".$equity_name."'";
        }
        
        if(!empty($choose_year) && strlen($choose_year)>0){
            if($reporttype =="3") {
                $where .= " AND YEAR(G.REFERENCE_DATE) = " . $choose_year;
            }
            else  if($reporttype =="1") { 
                $where .= " AND YEAR(G.TRANS_DATE) = " . $choose_year;
            } 
            else  if($reporttype =="2") {
                $where .= " AND YEAR(G.TRANS_DATE) = " . $choose_year;
            } 
            else {

            }
        }
        //TRANS_DATE
        /*
        G.SYMBOL = I.SYMBOL 
     AND C.INDUSTRIAL  = I.INDUSTRIAL  AND
    C.BU  = I.BU 
    AND YEAR(G.REFERENCE_DATE) = 2017
        */
        if($reporttype =="3") {
            $allquery = " SELECT count(SYMBOL) as total " .
                        " FROM ( SELECT  C.INDUSTRIAL, I.BU, I.SYMBOL as SYMBOL ".
                        "        FROM TBL_P2_EQUITY_INDEX I, " .
                        "             TBL_P2_EQUITY_CATEGORY C,  " .
                        "             TBL_P2_EQUITY_GAIN_LOSS G " .
                        "        WHERE G.SYMBOL = I.SYMBOL " .
                        "             AND C.INDUSTRIAL  = I.INDUSTRIAL  AND " .
                        "             C.BU  = I.BU " . 
                        $where . " group by C.INDUSTRIAL, I.BU, I.SYMBOL) T ";
        } else if($reporttype =="1") {
            $allquery = " SELECT count(SYMBOL) as total " .
                        " FROM ( SELECT  C.INDUSTRIAL, I.BU, I.SYMBOL as SYMBOL ".
                        "        FROM TBL_P2_EQUITY_INDEX I, " .
                        "             TBL_P2_EQUITY_CATEGORY C,  " .
                        "             TBL_P2_EQUITY_TRANS G " .
                        "        WHERE G.SYMBOL = I.SYMBOL " .
                        "             AND C.INDUSTRIAL  = I.INDUSTRIAL  AND " .
                        "             C.BU  = I.BU " . 
                        $where . " group by C.INDUSTRIAL, I.BU, I.SYMBOL) T ";
        } else if($reporttype =="2") {
            $allquery = " SELECT count(SYMBOL) as total " .
                        " FROM ( SELECT  C.INDUSTRIAL, I.BU, I.SYMBOL as SYMBOL ".
                        "        FROM TBL_P2_EQUITY_INDEX I, " .
                        "             TBL_P2_EQUITY_CATEGORY C,  " .
                        "             TBL_P2_EQUITY_TRANS G " .
                        "        WHERE G.SYMBOL = I.SYMBOL " .
                        "             AND C.INDUSTRIAL  = I.INDUSTRIAL  AND " .
                        "             C.BU  = I.BU " . 
                        $where . " group by C.INDUSTRIAL, I.BU, I.SYMBOL) T ";
        } else {

        }
                   // "  AND YEAR(G.REFERENCE_DATE) = 2016 " .
                   // "   AND C.INDUSTRIAL ='ทรัพยากร'  " .
                   // "   AND G.SECURITIES_NAME='UOBAM' " ;
        
        Log::info(get_class($this) .'::'. __FUNCTION__ . ' - SQL=' . $allquery);

        $all = DB::select(DB::raw($allquery));
        return  $all[0]->total;
    }

    public  function  DataSource($ArrParam, $IsCase, $ispageing= true){

        $where = "";
        if($ispageing){
            $PageSize = $ArrParam['pagesize'];
            $PageNumber = $ArrParam['PageNumber'];
        }

        //if($IsCase) 
        //{

        $reporttype  = $ArrParam["reporttype"];
        $category    = $ArrParam["category"];
        $equity_name = $ArrParam["equity_name"];
        $choose_year = $ArrParam["choose_year"];

         /*   $where = " WHERE focus.EMP_ID IS NOT  NULL";

            if(!empty($category)){
                $where .= " AND category = '".$emp_id."'";
            }
            if(!empty($depart)&& $check_depart== "true"){
                $where .= " AND em.DEP_SHT  = '".$depart."'";
            }
            if(!empty($plan)&& $check_plan== "true"){
                $where .= " AND new.USER_SAVING_RATE = '".$plan."'";
            }
            if(!empty($date_start) && !empty($date_end)&& $check_date== "true"){
                $where .= " AND new.CHANGE_SAVING_RATE_DATE  BETWEEN '".$date_start."' AND '".$date_end."'";
            }
        */
        $where = "";

        if(!empty($category)&& strlen($category)> 0) {
            //if(strlen($where) > 0)
               $where .= " AND C.INDUSTRIAL = '".$category."'";
            //else
            //    $where .= " C.INDUSTRIAL = '".$category."'";

        }
        if(!empty($equity_name) && strlen($equity_name)>0){
            //if(strlen($where) > 0)
                $where .= " AND G.SECURITIES_NAME  = '".$equity_name."'";
            //else
            //    $where .= " G.SECURITIES_NAME  = '".$equity_name."'";

        }
        
        if(!empty($choose_year) && strlen($choose_year)>0){
            if($reporttype =="3") {
                $where .= " AND YEAR(G.REFERENCE_DATE) = " . $choose_year;
            }
            else  if($reporttype =="1") { 
                $where .= " AND YEAR(G.TRANS_DATE) = " . $choose_year;
            } 
            else  if($reporttype =="2") {
                $where .= " AND YEAR(G.TRANS_DATE) = " . $choose_year;
            } 
            else {

            }
        }

        $query = "";
        if($reporttype =="3") {
            $query= "" .
            " select C.INDUSTRIAL, I.BU, I.SYMBOL as SYMBOL , " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  1 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U1,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  2 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U2,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  3 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U3,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  4 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U4,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  5 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U5,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  6 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U6,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  7 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U7,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  8 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U8,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  9 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U9,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) = 10 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U10, " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) = 11 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U11, " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) = 12 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U12  " .
            ",   " .
            
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  1 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K1,   " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  2 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K2,   " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  3 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K3,   " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  4 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K4,   " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  5 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K5,   " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  6 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K6,   " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  7 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K7,   " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  8 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K8,   " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  9 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K9,   " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) = 10 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K10,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) = 11 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K11,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) = 12 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K12   ".
            " FROM TBL_P2_EQUITY_INDEX I, " .
            "    TBL_P2_EQUITY_CATEGORY C, " .
            "    TBL_P2_EQUITY_GAIN_LOSS G " .
            " WHERE G.SYMBOL = I.SYMBOL " .
            " AND C.INDUSTRIAL  = I.INDUSTRIAL ".
            " AND C.BU  = I.BU 
            " .$where;
              
            $query .= " group by C.INDUSTRIAL, I.BU, I.SYMBOL " .
                      " ORDER BY INDUSTRIAL, SYMBOL ";
        }
        
        else if($reporttype =="1") {
            $query= "" .
            " select C.INDUSTRIAL, I.BU, I.SYMBOL as SYMBOL , " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  1 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U1,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  2 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U2,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  3 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U3,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  4 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U4,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  5 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U5,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  6 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U6,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  7 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U7,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  8 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U8,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  9 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U9,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 10 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U10, " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 11 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U11, " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 12 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U12  " .
            ",   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  1 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K1,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  2 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K2,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  3 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K3,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  4 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K4,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  5 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K5,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  6 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K6,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  7 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K7,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  8 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K8,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  9 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K9,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 10 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K10,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 11 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K11,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 12 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K12   ".
            " FROM TBL_P2_EQUITY_INDEX I, " .
            "    TBL_P2_EQUITY_CATEGORY C, " .
            "    TBL_P2_EQUITY_TRANS G " .
            " WHERE G.SYMBOL = I.SYMBOL " .
            " AND C.INDUSTRIAL  = I.INDUSTRIAL ".
            " AND C.BU  = I.BU 
            " .$where;
              
            $query .= " group by C.INDUSTRIAL, I.BU, I.SYMBOL " .
                      " ORDER BY INDUSTRIAL, SYMBOL ";
        }
        else if($reporttype =="2") {
            $query= "" .
            " select C.INDUSTRIAL, I.BU, I.SYMBOL as SYMBOL , " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  1 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U1,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  2 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U2,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  3 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U3,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  4 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U4,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  5 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U5,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  6 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U6,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  7 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U7,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  8 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U8,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  9 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U9,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 10 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U10, " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 11 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U11, " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 12 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U12  " .
            ",   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  1 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K1,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  2 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K2,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  3 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K3,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  4 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K4,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  5 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K5,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  6 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K6,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  7 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K7,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  8 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K8,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  9 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K9,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 10 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K10,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 11 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K11,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 12 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K12   ".
            " FROM TBL_P2_EQUITY_INDEX I, " .
            "    TBL_P2_EQUITY_CATEGORY C, " .
            "    TBL_P2_EQUITY_TRANS G " .
            " WHERE G.SYMBOL = I.SYMBOL " .
            " AND C.INDUSTRIAL  = I.INDUSTRIAL ".
            " AND C.BU  = I.BU 
            " .$where;
              
            $query .= " group by C.INDUSTRIAL, I.BU, I.SYMBOL " .
                      " ORDER BY INDUSTRIAL, SYMBOL ";
        } else {

        }
        //// 

        Log::debug('DataSource Query => '.$query);
        if($ispageing){
            $query .=  " OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
        }

        Log::info(get_class($this) .'::'. __FUNCTION__ . ' - SQL=' . $query);  
        return DB::select(DB::raw($query));
    }

/**
 * DataSourceIndustrialChart
 * query statement to generate Chart group by INDUS_SHT (Industrial Name)
 */
public  function  DataSourceIndustrialChart($ArrParam, $IsCase, $ispageing= true, $groupType="INDUS_SHT"){

        $where = "";
        if($ispageing){
            $PageSize = $ArrParam['pagesize'];
            $PageNumber = $ArrParam['PageNumber'];
        }

        $reporttype  = $ArrParam["reporttype"];
        $category    = $ArrParam["category"];
        $equity_name = $ArrParam["equity_name"];
        $choose_year = $ArrParam["choose_year"];

        $where = "";

        if(!empty($category)&& strlen($category)> 0) {
            $where .= " AND C.INDUSTRIAL = '".$category."'";
        }

        if(!empty($equity_name) && strlen($equity_name)>0){
            $where .= " AND G.SECURITIES_NAME  = '".$equity_name."'";
        }
        
        if(!empty($choose_year) && strlen($choose_year)>0){
            if($reporttype =="3") {
                $where .= " AND YEAR(G.REFERENCE_DATE) = " . $choose_year;
            }
            else  if($reporttype =="1") { 
                $where .= " AND YEAR(G.TRANS_DATE) = " . $choose_year;
            } 
            else  if($reporttype =="2") {
                $where .= " AND YEAR(G.TRANS_DATE) = " . $choose_year;
            } 
            else {

            }
        }

        $query = "";
        if($reporttype =="3") {
            // MARK TO MARKET
            $query= "" .
            " SELECT INDUSTRIAL, " .
            $groupType . "," .
           // "        INDUS_SHT,  " .
            "        SUM(UOB_SUM) as UOB_SUM,  " . 
            "        SUM(KTAM_SUM) as KTAM_SUM " . 
            " FROM ( " .
            "   SELECT INDUSTRIAL, " .
            //"INDUS_SHT,  " .
             $groupType . "," .
            "   " . 
            "   ( " .
            "       U1 + U2 + U3 + U4 + U5 + U6 + U7 + U8 + U9 + U10 + U11 + U12 " .
            "   ) as UOB_SUM, " .
            "   ( " .
            "       K1 + K2 + K3 + K4 + K5 + K6 + K7 + K8 + K9 + K10 + K11 + K12 " .
            "   ) as KTAM_SUM  " .
            "   FROM  ( ".
            "       SELECT C.INDUSTRIAL, ". 
            "              C.".  $groupType . "," ." I.BU, I.SYMBOL as SYMBOL , " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) =  1 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U1,  " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) =  2 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U2,  " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) =  3 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U3,  " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) =  4 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U4,  " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) =  5 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U5,  " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) =  6 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U6,  " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) =  7 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U7,  " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) =  8 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U8,  " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) =  9 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U9,  " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) = 10 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U10, " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) = 11 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U11, " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) = 12 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U12  " .
            ",   " .
            
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) =  1 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K1,   " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) =  2 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K2,   " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) =  3 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K3,   " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) =  4 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K4,   " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) =  5 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K5,   " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) =  6 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K6,   " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) =  7 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K7,   " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) =  8 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K8,   " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) =  9 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K9,   " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) = 10 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K10,  " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) = 11 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K11,  " .
            "           SUM(case WHEN (MONTH(REFERENCE_DATE) = 12 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K12   ".
            "       FROM TBL_P2_EQUITY_INDEX I, " .
            "           TBL_P2_EQUITY_CATEGORY C, " .
            "           TBL_P2_EQUITY_GAIN_LOSS G " .
            "       WHERE G.SYMBOL = I.SYMBOL " .
            "           AND C.INDUSTRIAL  = I.INDUSTRIAL ".
            "           AND C.BU  = I.BU 
            " .$where;
              
            $query .= "    GROUP BY C.INDUSTRIAL, " .
                      "    C.".  $groupType . ", " ." I.BU, I.SYMBOL " .
                      "    ) ABC " .
                      " ) XXX " . 
                      " GROUP BY " . $groupType  .", INDUSTRIAL " . 
                      " ORDER BY " . $groupType . "";
        }
        
        else if($reporttype =="1") {
            // GAIN LOSS
            $query = "" .
            " SELECT INDUSTRIAL, " .
           // "        INDUS_SHT,  " .
            $groupType . "," .
            "        SUM(UOB_SUM) as UOB_SUM,  " . 
            "        SUM(KTAM_SUM) as KTAM_SUM " . 
            " FROM ( " .
            "   SELECT INDUSTRIAL, ".
            //"  INDUS_SHT,  " .
            $groupType . "," .
            "   " . 
            "   ( " .
            "       U1 + U2 + U3 + U4 + U5 + U6 + U7 + U8 + U9 + U10 + U11 + U12 " .
            "   ) as UOB_SUM, " .
            "   ( " .
            "       K1 + K2 + K3 + K4 + K5 + K6 + K7 + K8 + K9 + K10 + K11 + K12 " .
            "   ) as KTAM_SUM  " .
            "   FROM  ( ".
            "       SELECT C.INDUSTRIAL, ".
            "              C.".  $groupType . "," ." I.BU, I.SYMBOL as SYMBOL , " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) =  1 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U1,  " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) =  2 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U2,  " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) =  3 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U3,  " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) =  4 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U4,  " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) =  5 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U5,  " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) =  6 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U6,  " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) =  7 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U7,  " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) =  8 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U8,  " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) =  9 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U9,  " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) = 10 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U10, " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) = 11 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U11, " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) = 12 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U12  " .
            ",   " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) =  1 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K1,   " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) =  2 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K2,   " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) =  3 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K3,   " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) =  4 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K4,   " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) =  5 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K5,   " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) =  6 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K6,   " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) =  7 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K7,   " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) =  8 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K8,   " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) =  9 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K9,   " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) = 10 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K10,  " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) = 11 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K11,  " .
            "           SUM(case WHEN (MONTH(TRANS_DATE) = 12 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K12   " .
            "       FROM TBL_P2_EQUITY_INDEX I, " .
            "           TBL_P2_EQUITY_CATEGORY C, " .
            "           TBL_P2_EQUITY_TRANS G " .
            "       WHERE G.SYMBOL = I.SYMBOL " .
            "           AND C.INDUSTRIAL  = I.INDUSTRIAL ".
            "           AND C.BU  = I.BU 
            " . $where;
              
           $query .= "    GROUP BY C.INDUSTRIAL, " .
                      "    C.".  $groupType . ", " ." I.BU, I.SYMBOL " .
                      "    ) ABC " .
                      " ) XXX " . 
                      " GROUP BY " . $groupType  .", INDUSTRIAL " . 
                      " ORDER BY " . $groupType . "";
        }
        else if($reporttype =="2") {
            // DIVIDEND OF SECURITY
            $query = "" .
            " SELECT INDUSTRIAL, " .
            //"        INDUS_SHT,  " .
             $groupType . "," .
            "        SUM(UOB_SUM) as UOB_SUM,  " . 
            "        SUM(KTAM_SUM) as KTAM_SUM " . 
            " FROM ( " .
            "   SELECT INDUSTRIAL, ".
           // "   INDUS_SHT,  " .
               $groupType . "," .
            "   " . 
            "   ( " .
            "       U1 + U2 + U3 + U4 + U5 + U6 + U7 + U8 + U9 + U10 + U11 + U12 " .
            "   ) as UOB_SUM, " .
            "   ( " .
            "       K1 + K2 + K3 + K4 + K5 + K6 + K7 + K8 + K9 + K10 + K11 + K12 " .
            "   ) as KTAM_SUM  " .
            "   FROM  ( ".
            "       SELECT C.INDUSTRIAL, ".
            "              C.".  $groupType . "," ." I.BU, I.SYMBOL as SYMBOL , " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) =  1 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U1,  " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) =  2 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U2,  " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) =  3 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U3,  " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) =  4 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U4,  " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) =  5 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U5,  " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) =  6 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U6,  " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) =  7 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U7,  " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) =  8 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U8,  " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) =  9 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U9,  " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) = 10 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U10, " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) = 11 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U11, " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) = 12 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U12  " .
            ",   " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) =  1 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K1,   " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) =  2 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K2,   " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) =  3 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K3,   " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) =  4 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K4,   " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) =  5 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K5,   " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) =  6 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K6,   " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) =  7 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K7,   " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) =  8 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K8,   " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) =  9 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K9,   " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) = 10 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K10,  " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) = 11 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K11,  " .
            "          SUM(case WHEN (MONTH(TRANS_DATE) = 12 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K12   ".
            "       FROM TBL_P2_EQUITY_INDEX I, " .
            "           TBL_P2_EQUITY_CATEGORY C, " .
            "           TBL_P2_EQUITY_TRANS G " .
            "       WHERE G.SYMBOL = I.SYMBOL " .
            "           AND C.INDUSTRIAL  = I.INDUSTRIAL ".
            "           AND C.BU  = I.BU 
            " . $where;
              
           $query .= "    GROUP BY C.INDUSTRIAL, " .
                      "    C.".  $groupType . "," ." I.BU, I.SYMBOL " .
                      "    ) ABC " .
                      " ) XXX " . 
                      " GROUP BY " . $groupType  .", INDUSTRIAL " . 
                      " ORDER BY " . $groupType . "";
        } else {

        }
        //// 
        //if($ispageing){
        //    $query .=  " OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
        //}

        Log::info(get_class($this) .'::'. __FUNCTION__ . ' - SQL=' . $query);  
        return DB::select(DB::raw($query));
    }

    public function ajax_report1_profit_loss_search(Request $request)
    {
        /*
          3. Profit LOSS 
         */
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $data = getmemulist();
      //  $title =   getMenuName($data, 62, 6);

         $title =  "กองทุนสำรองเลี้ยงชีพพนักงานการไฟฟ้านครหลวง  ซึ่งจดทะเบียนแล้ว
สรุปรายละเอียดกำไร-ขาดทุน จากการซื้อขายหลักทรัพย์";

        $view_name = 'backend.pages.ajax.ajax_p2_benefits_profit_loss_report';

        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');


        $reporttype  = $request->input('reporttype');
        $category    = $request->input('category');
        $equity_name = $request->input('equity_name');
        $choose_year = $request->input('choose_year');

        $yyyy = (intval($choose_year) == 0) ?  date("Y") : intval($choose_year);
        $choose_year = $yyyy;

        $ArrParam = array();
        $ArrParam["pagesize"]    = $PageSize;
        $ArrParam["PageNumber"]  = $PageNumber;
        $ArrParam["reporttype"]  = $reporttype;
        $ArrParam["category"]    = $category;
        $ArrParam["equity_name"] = $equity_name;
        $ArrParam["choose_year"] = $choose_year;
       
        Log::debug('ArrParam => '.print_r($ArrParam));
        $data =null;
        $totals= 0;

        $data = $this->DataSource($ArrParam, true);

        $totals = $this->DataSourceCount($ArrParam, true);

        $htmlPaginate =Paginatre_gen($totals, $PageSize,'page_click_search',$PageNumber);

        $utils = new MEAUtils();
         
       // $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
       // $thai_date_end =  (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";
      //  Log::info(get_class($this) .'::'. __FUNCTION__.
       //     ' converted date: ' . $date_start .' to ' . $thai_date_start . ', ' .
        //    $date_end . ' to '. $thai_date_end );
        
        //$pretty_date = "ปี " .  (intval($choose_year) +  543); //$utils->prettyPrintThaiDate($thai_date_start, $thai_date_end, "red");
        // $yyyy = (intval($choose_year) == 0) ?  date("Y") : intval($choose_year);
        $pretty_date = "ปี " .  ($yyyy +  543);  

        // $choose_year = ($yyyy - 543);

        $returnHTML = view($view_name)->with([
            'htmlPaginate'=> $htmlPaginate,
            'data' => $data,
            'totals' => $totals,
            'PageSize' =>$PageSize,
            'PageNumber' =>$PageNumber,
            'date_start' => '', //$thai_date_start,
            'date_end' => '', //$thai_date_end,
            'pretty_date' => $pretty_date,
            'TableTitle' => $title

        ])->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }

     public function ajax_report2_dividen_search(Request $request)
    {
        /*
          2. Dividend LOSS 
         */
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $data = getmemulist();
        //$title = getMenuName($data, 62, 6);
        $title =  "กองทุนสำรองเลี้ยงชีพพนักงานการไฟฟ้านครหลวง  ซึ่งจดทะเบียนแล้ว
สรุปรายละเอียดกำไร-ขาดทุน จากการซื้อขายหลักทรัพย์";

        $view_name = 'backend.pages.ajax.ajax_p2_benefits_dividend_report';

        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

       
        $reporttype  = $request->input('reporttype');
        $category    = $request->input('category');
        $equity_name = $request->input('equity_name');
        $choose_year = $request->input('choose_year');

        $yyyy = (intval($choose_year) == 0) ?  date("Y") : intval($choose_year);
        $choose_year = $yyyy;

        $ArrParam = array();
        $ArrParam["pagesize"]    = $PageSize;
        $ArrParam["PageNumber"]  = $PageNumber;
        $ArrParam["reporttype"]  = $reporttype;
        $ArrParam["category"]    = $category;
        $ArrParam["equity_name"] = $equity_name;
        $ArrParam["choose_year"] = $choose_year;
       

        $data =null;
        $totals= 0;

        $data = $this->DataSource($ArrParam, true);

        $totals = $this->DataSourceCount($ArrParam, true);

        $htmlPaginate =Paginatre_gen($totals, $PageSize,'page_click_search', $PageNumber);

        $utils = new MEAUtils();
        // $yyyy = (intval($choose_year) == 0) ?  date("Y") : intval($choose_year);
        $pretty_date = "ปี " .  ($yyyy +  543);  
        
        $returnHTML = view($view_name)->with([
            'htmlPaginate'=> $htmlPaginate,
            'data' => $data,
            'totals' => $totals,
            'PageSize' =>$PageSize,
            'PageNumber' =>$PageNumber,
            'date_start' => '', //$thai_date_start,
            'date_end' => '', //$thai_date_end,
            'pretty_date' => $pretty_date,
            'TableTitle' => $title

        ])->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML));

    }

    public function ajax_report3_gain_loss_search(Request $request)
    {
        /*
          1. Gain LOSS 
         */
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $data = getmemulist();
        //$title = getMenuName($data, 62, 6);
        $title =  "กองทุนสำรองเลี้ยงชีพพนักงานการไฟฟ้านครหลวง  ซึ่งจดทะเบียนแล้ว
สรุปรายละเอียดกำไร-ขาดทุน จากการซื้อขายหลักทรัพย์";

        $view_name = 'backend.pages.ajax.ajax_p2_benefits_gain_loss_report';

        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');


        $reporttype  = $request->input('reporttype');
        $category    = $request->input('category');
        $equity_name = $request->input('equity_name');
        $choose_year = $request->input('choose_year');

       
        $yyyy = (intval($choose_year) == 0) ?  date("Y") : intval($choose_year);
        $choose_year = $yyyy;

 
        $ArrParam = array();
        $ArrParam["pagesize"]    = $PageSize;
        $ArrParam["PageNumber"]  = $PageNumber;
        $ArrParam["reporttype"]  = $reporttype;
        $ArrParam["category"]    = $category;
        $ArrParam["equity_name"] = $equity_name;
        $ArrParam["choose_year"] = $choose_year;
       

        $data =null;
        $totals= 0;

        $data = $this->DataSource($ArrParam, true);

        $totals = $this->DataSourceCount($ArrParam, true);

        $htmlPaginate =Paginatre_gen($totals, $PageSize,'page_click_search', $PageNumber);

        $utils = new MEAUtils();
         
       // $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
       // $thai_date_end =  (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";
      //  Log::info(get_class($this) .'::'. __FUNCTION__.
       //     ' converted date: ' . $date_start .' to ' . $thai_date_start . ', ' .
        //    $date_end . ' to '. $thai_date_end );
        
        //$pretty_date = "ปี " .  (intval($choose_year) +  543); //$utils->prettyPrintThaiDate($thai_date_start, $thai_date_end, "red");
        // $yyyy = (intval($choose_year) == 0) ?  date("Y") : intval($choose_year);
        $pretty_date = "ปี " .  ($yyyy +  543);  

        $returnHTML = view($view_name)->with([
            'htmlPaginate'=> $htmlPaginate,
            'data' => $data,
            'totals' => $totals,
            'PageSize' =>$PageSize,
            'PageNumber' =>$PageNumber,
            'date_start' => '', //$thai_date_start,
            'date_end' => '', //$thai_date_end,
            'pretty_date' => $pretty_date,
            'TableTitle' => $title

        ])->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }


    /**
     * 1. GAIN LOSSS - chart Industrial/BU  (2017/10/18)
     */
    public function ajax_chart1_search(Request $request)
    {

        /*
          1. Gain LOSS 
         */
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $data = getmemulist();
        //$title = getMenuName($data, 62, 6);
        $title =  "กองทุนสำรองเลี้ยงชีพพนักงานการไฟฟ้านครหลวง  ซึ่งจดทะเบียนแล้ว".
                  "สรุปรายละเอียดกำไร-ขาดทุน จากการซื้อขายหลักทรัพย์";

       // $view_name = 'backend.pages.ajax.ajax_p2_benefits_gain_loss_report';

        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');



        $reporttype  = $request->input('reporttype');
        $category    = $request->input('category');
        $equity_name = $request->input('equity_name');
        $choose_year = $request->input('choose_year');

       
        $yyyy = (intval($choose_year) == 0) ?  date("Y") : intval($choose_year);
        $choose_year = $yyyy;

 
        $ArrParam = array();
        $ArrParam["pagesize"]    = $PageSize;
        $ArrParam["PageNumber"]  = $PageNumber;
        $ArrParam["reporttype"]  = $reporttype;
        $ArrParam["category"]    = $category;
        $ArrParam["equity_name"] = $equity_name;
        $ArrParam["choose_year"] = $choose_year;
       

        $data =null;
        $totals= 0;

        $data = $this->DataSourceIndustrialChart($ArrParam, true, false, "INDUS_SHT");
        $bu_data = $this->DataSourceIndustrialChart($ArrParam, true, false, "BU_SHT");

        ///$totals = $this->DataSourceCount($ArrParam, true);

        //$htmlPaginate =Paginatre_gen($totals, $PageSize,'page_click_search', $PageNumber);

        $utils = new MEAUtils();
         
       // $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
       // $thai_date_end =  (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";
      //  Log::info(get_class($this) .'::'. __FUNCTION__.
       //     ' converted date: ' . $date_start .' to ' . $thai_date_start . ', ' .
        //    $date_end . ' to '. $thai_date_end );
        
       // $pretty_date = "ปี " .  (intval($choose_year) +  543); //$utils->prettyPrintThaiDate($thai_date_start, $thai_date_end, "red");
        $yyyy = (intval($choose_year) == 0) ?  date("Y") : intval($choose_year);
        $pretty_date = "ปี " .  ($yyyy +  543);  

        $returnHTML = [
            'data'        => $data,
            'bu_data'     => $bu_data,
            'pretty_date' => $pretty_date,
            'TableTitle'  => $title,
            'equity_name' => $equity_name
        ];
        return response()->json(array('success' => true, 'html'=>$returnHTML, 'result'=>$returnHTML));
    }


    public function getChartLabels() {
        $ar = array();
        array_push($ar, 'พลังงาน');
        array_push($ar, 'ท่องเที่ยว');
        array_push($ar, 'บริการ');
        array_push($ar, 'ทรัพยากร');
        array_push($ar, 'ธุรกิจการเงิน');
        array_push($ar, 'สินค้าอุตสาหกรรม');
        array_push($ar, 'อสังหาริมทรัพย์');
        array_push($ar, 'เกษตร');
        array_push($ar, 'เทคโนโลยี');
        array_push($ar, 'อุตสาหกรรมอาหาร');
        array_push($ar, 'ก่อสร้าง');
        array_push($ar, 'อุปโภคบริโภค'); 
        return $ar;
    }

    public function getChartDataSource() {
        /// FAKE: data
        $data1 = array();
        array_push($data1, 1420);
        array_push($data1, 1030);
        array_push($data1, 1060);
        array_push($data1, 1340);
        array_push($data1, 1480);
        array_push($data1, 2440);
        array_push($data1, -2353);
        array_push($data1, 4623);
        array_push($data1, -1215);
        array_push($data1, 3617);
        array_push($data1, -1297);
        array_push($data1, 5586);

        $data2 = array();
        array_push($data2, 3440);
        array_push($data2, 6760);
        array_push($data2, -3540);
        array_push($data2, 5580);
        array_push($data2, 5570);
        array_push($data2, -5520);
        array_push($data2, -4433);
        array_push($data2, 1013);
        array_push($data2, 1555);
        array_push($data2, 1087);
        array_push($data2, -1676);
        array_push($data2, -6476);

        $data = array();
        array_push($data, $data1, $data2);

        return $data;
    }

/*
    public function ajax_chart1_search(Request $request) {
        // Log::info(get_class($this) .'::'. __FUNCTION__);
        
        $emp_id = $request->input('emp_id');
        $depart = $request->input('depart');
        $plan = $request->input('plan');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $ArrParam = array();
        
        $ArrParam["emp_id"] =$emp_id;
        $ArrParam["depart"] =$depart;
        $ArrParam["plan"] =$plan;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;

        $data =null;
        

        $totals = 1;         

        $utils = new MEAUtils();
         
        $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
        $thai_date_end =  (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";
        
        Log::info(get_class($this) .'::'. __FUNCTION__.
            ' converted date: ' . $date_start .' to ' . $thai_date_start . ', ' . $date_end . ' to '. $thai_date_end );
        $data  = array();
       
        $labels = $this->getChartLabels();

        $data = $this->getChartDataSource();

        $pretty_date = $utils->prettyPrintThaiDate($thai_date_start, $thai_date_end, "");

        $result = [
                'date_start' => $thai_date_start,
                'date_end' => $thai_date_end,
                'pretty_date' => $pretty_date,
                'labels' => $labels,
                'data' => $data      
        ];
                      
        return response()->json(array('success' => true, 'result'=>$result));
    }
*/

    public function ajax_report1_search_export() {
// var url="BenefitsReport/exportsearch?reporttype=" + reporttype +"&category=" + category +
//         "&equity_name=" + equity_name + "&choose_year=" +choose_year;        
//Input::get("param");

        Log::info(get_class($this) .'::'. __FUNCTION__);
        $ArrParam = array();
        $ArrParam["pagesize"] ="";
        $ArrParam["PageNumber"] ="";       
        
        $ArrParam["reporttype"] =Input::get("reporttype");
        $ArrParam["category"] =Input::get("category");
        $ArrParam["equity_name"] =Input::get("equity_name");
        $ArrParam["choose_year"] =Input::get("choose_year");

        $ArrParam["emp_id"] =Input::get("EmpID");
        $ArrParam["depart"] =Input::get("depart");
        $ArrParam["plan"] =Input::get("plan");
        $ArrParam["date_start"] =Input::get("date_start");
        $ArrParam["date_end"] =Input::get("date_end");

        $ArrParam["check_name"] =Input::get("check_name");
        $ArrParam["check_depart"] =Input::get("check_depart");
        $ArrParam["check_plan"] =Input::get("check_plan");
        $ArrParam["check_date"] =Input::get("check_date");
        // $sql2 = "SELECT TOP  5 * FROM  TBL_MEMBER_BENEFITS WHERe EMP_ID = '".get_userID()."' ORDER BY RECORD_DATE ASC";

        $data = $this->DataSource($ArrParam,true,false);
        $results = array();
        foreach ($data as $item) {
//            $item->filed1 = 'some modification';
//            $item->filed2 = 'some modification2';
            $results[] = (array)$item;

           // $results[5] = get_date_notime($item->modify_new);

            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }

        Excel::create('ExcelExport', function ($excel) use ($results,$ArrParam){

            $excel->sheet('Sheetname', function ($sheet) use ($results,$ArrParam){

                //format
//                $sheet->setColumnFormat(array(
//                    'D' => '0.00',
//
//                ));
                //
                // first row styling and writing content
                $sheet->mergeCells('A1:J1');
                $sheet->mergeCells('A2:J2');
                $sheet->mergeCells('A3:J3');
                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Comic Sans MS');
                    $row->setFontSize(20);
                    $row->setAlignment('center');
                });


                $sheet->row(2, function ($row) {$row->setAlignment('center');});
                $sheet->row(3, function ($row) {$row->setAlignment('center');});
//                $header = array('รายชื่อสมาชิกเปลี่ยนอัตราสะสม');

                $sheet->row(1, array('รายชื่อสมาชิกเปลี่ยนอัตราสะสม'));

                if($ArrParam["check_date"] == "true" && $ArrParam["date_start"] != "" && $ArrParam["date_end"] != ""){
                    $sheet->row(2, array('ในช่วงวันที่ ' . get_date_notime($ArrParam["date_start"]) . ' ถึง ' . get_date_notime($ArrParam["date_end"])   ));
                }


                $sheet->row(3, array('รายงานข้อมูล ณ วันที่ ' . get_date_notime(date("Y-m-d H:i:s"))));

                $header[] = null;
                $header[0] = 'รหัสพนักงาน';
                $header[1] = "ชื่อ-นามสกุล";
                $header[2] = "หน่วยงาน";
                $header[3] = "อัตราสะสม(เดิม)";
                $header[4] = "อัตราสะสม(ใหม่)";
                $header[5] = "วันที่ทำรายการ";
                $header[6] = "วันที่มีผล";
                $header[7] = "ผู้ทำรายการ";
                $header[8] = "สถานะ";
                $header[9] = "วันที่พ้นสภาพ";


                $sheet->row(4, $header);




                foreach ($results as $user) {


                    $sheet->appendRow($user);
                }



            });

        })->download('xls');
    }
}

?>



