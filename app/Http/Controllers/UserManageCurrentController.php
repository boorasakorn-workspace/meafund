<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package\Curl;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Input;
use Illuminate\Http\UploadedFile;

class UserManageCurrentController extends Controller
{

    public function getsimple()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 51,
            'menu_id' => 7,
            'title' => getMenuName($data,51,7) ." | MEA"
        ]);

        //$user_group = DB::table('TBL_PRIVILEGE')->select('USER_PRIVILEGE_ID','USER_PRIVILEGE_DESC')->orderBy('USER_PRIVILEGE_ID', 'asc')->get();

        return view('backend.pages.usercurrent');
    }

    public  function  dowloadsample(){
        $file = 'contents/sample/saving_rate.xls';
        $headers = array(
            'Content-Type: application/pdf',
        );
        return \Response::download($file, 'saving_rate.xls', $headers);
    }

    public  function  Checkdate(Request $request){

        /* limit execution timeout 30 sec.*/
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $results = null;

        $result=   Excel::load($request->file('exelimport'))->get();
        
        $count = $result->count();

        return response()->json(array('success' => true, 'html'=>$count));
    }


    public function importdata(Request $request){

        /* limit execution timeout 60 sec.*/
        ini_set('max_execution_time', 60000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        /* statistics */ 
        $count       = 0;
        $skipedCount = 0;
        $passedCount = 0;
        $total_records = 0;
        $progress_count = 0;
        $htmlResult = '';
       
        Session::put('progress_count',0);
        Session::put('total_records', 0);
       

        $results = null;

        $file = $request->file('exelimport');

        $request->file('exelimport')->move(storage_path().'/public/import/' , 'import-7.xlsx');

        $inputfile = storage_path('/public/import/import-7.xlsx');
        /*   
            $ret = Excel::filter('chunk')->load(storage_path('/public/import/import.xlsx'))->chunk(250, function($results){
            $data = array();
            foreach($results as $index => $value) {
        */
        $retdate = Excel::load($inputfile, function($reader) use(&$count, 
                                                                 &$skipedCount, 
                                                                 &$passedCount, 
                                                                 &$total_records, 
                                                                 &$progress_count,
                                                                 &$htmlResult) {
            $results = $reader->get(); 
            $ret = $results->toArray();
            $htmlResult = '';

            $progress_count = 0;
            $total_records = $results->count();

            Session::put('progress_count',$progress_count);
            Session::put('total_records', $total_records);

            $percent = 0;
            
            $progress_count = 0;
           
            foreach($ret as $index => $value) {

                $data = array();
                
                $count++;

                if (($value == null) || ($value["emp_id"] == null) || (strlen($value["emp_id"]) < 2) || (strlen($value["effective_date"]) < 4 )) {
                    $skipedCount++;
                    continue;
                }
                
                /* update progress */
                $progress_count++;
                if($progress_count % 2 == 0)
                    Session::put('progress_count', $progress_count);

                $im_date_start = $value["change_saving_rate_date"];
                $ret_data_start = str_replace("'","",$im_date_start);

                $im_date_modify = $value["effective_date"];
                $ret_data_modify = str_replace("'","",$im_date_modify);

//                var_dump($ret_data_start);

                $date_start = new Date($ret_data_start);
                $date_modify = new Date($ret_data_modify);

                $EMP_ID = $value["emp_id"];

                $allquery = "SELECT COUNT(EMP_ID) AS total FROM TBL_USER_SAVING_RATE  WHERE EMP_ID= '".$EMP_ID."' AND CHANGE_SAVING_RATE_DATE='".$date_start."'";

                $all = DB::select(DB::raw($allquery));
                
                $total =  $all[0]->total;

                if ($total == 0) {
                    array_push($data, array(
                        'EMP_ID' => $value["emp_id"],
                        'USER_SAVING_RATE' => $value["user_saving_rate"],
                        'CHANGE_SAVING_RATE_DATE' => $date_start,
                        'EFFECTIVE_DATE' => $date_modify,
                        'MODIFY_COUNT' => $value["modify_count"],
                        'MODIFY_BY' => $value["modify_by"]
                    ));

                    $affected = DB::table('TBL_USER_SAVING_RATE')->insert($data);
                    if($affected < 1) {
                        if(strlen($htmlResult) < 500)  {
                            $htmlResult .= '<tr><td><span style=\"color: red\">' . $value["emp_id"] .     '</span></td>';
                            $htmlResult .= '    <td><span style=\"color: red\">' . $date_modify .         '</span></td></tr>';
                        }
                    } else {
                        $passedCount++; 
                    } // if 

                } else {
                    // just update counter
                    $passedCount++; 
                }

            }

            /// DB::table('TBL_USER_SAVING_RATE')->insert($data);


        });

        //===>return response()->json(array('success' => true, 'html'=>$ret));

        $header = 'รายละเอียดข้อมูลที่ ไม่สำเร็จ <BR> '.
                  '<table style=\"width: 100px;\" class=\"table table-bordered\">' .
                  '<tr><td> EMP_ID | </td><td> MODIFIED_DATE </td></tr>';
        $footer = '</table>  ';

        if(strlen($htmlResult) < 1) {
            $header  = '';
            $footer = '';  
            $htmlResult = '';
        }

        /* put 100% */ 
        Session::put('progress_count', $total_records);

        return response()->json(array('success' => true, 'html'=>'<br>' . 
                ' Total : ' . $count . ' Record(s), '. 
                ' Passed: ' . $passedCount . ' Record(s), '. 
                ' Failed: ' . ($count - $passedCount) . ' Record(s), '. 
                // ' progress_count:' . Session::get('progress_count') . 
                //  ' Skiped: ' . $skipedCount . ' Record(s)  ' .
                ' <BR> '. $header . $htmlResult . $footer));//$retdate));


    }

}
