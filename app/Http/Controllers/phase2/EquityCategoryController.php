<?php
/**
 * FILE: EquityCategoryController.php
 * Phase#2 Equity Category Management Controller
 * Created: 2016/11/27 00:06
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\UserGroup;
use Illuminate\Support\Facades\Validator;
use Jenssegers\Date\Date;

use App\User;
use Illuminate\Support\Facades\Log;


class EquityCategoryController extends Controller
{
    //

    public function getindex()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data, 60, 1) . ' | MEA'
        ] );


        return view('backend.pages.phase2.p2_tab2_equity_category_page');
    }


    public  function Ajax_Index(Request $request){

        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;

        $Datacount = $this->getCountAll();
        $Data = $this->getData($ArrParam);

        $totals = count($Datacount);

        $htmlPaginate =Paginatre_gen($totals,$PageSize,'page_click_search',$PageNumber);

        $returnHTML = view('backend.pages.ajax.phase2.ajax_p2_equity_category.blade')->with([
            'htmlPaginate'=> $htmlPaginate,
            'data'=>$Data

        ])->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }

    public  function  getCountAll(){
        
        // Log::info('EquitycompanyManagement::getCountAll()');

        return DB::table('TBL_P2_EQUITY_SECURITIES')->orderby("Name_Sht")->get();
        
    }

    public  function  getData($ArrParam){

        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];

        $query =  "SELECT * FROM TBL_P2_EQUITY_SECURITIES ORDER BY NAME_SHT OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
        return DB::select(DB::raw($query));
    }

    /**
     * Handle request delete single/multiple records.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function delete(Request $request)
    {
        $deleted = false;
        $arrId = explode(',',$request->input('group_id'));

        foreach($arrId as $index => $item){

            if($item != ""){
                $deleted =  DB::table('TBL_P2_EQUITY_SECURITIES')->where('NAME_SHT',"=", $item)->delete();
            }
        }

        if($deleted)  {
            return response()->json(["ret" => "1"]);
        } else {
            return response()->json(["ret" => "0"]);
        }
    }


    /**
     * Query equity company data by specifiled code as parameter id.
     * 
     * @param  None
     * @return view\backend\pages
     */
    public function getAdd()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data,60, 1) . ' | MEA'
        ] );

        return view('backend.pages.phase2.p2_tab2_add_equity_category_page');
    }

    /**
     * Query equity company data by specifiled code as parameter id.
     * 
     * @param  Equity company code  $id
     * @return view backend.pages.p2_edit_equity_company_page
     */
    public function getEdit($id)
    {
        $data = getmemulist();
        $this->pageSetting([
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data, 60, 1) . ' | MEA'
        ]);


        $editdata = DB::table('TBL_P2_EQUITY_SECURITIES')->where("NAME_SHT" ,"=",  $id)->first();
        // $editdata = DB::table('TBL_P2_EQUITY_SECURITIES')->where("NAME_SHT" ,"=",  $id)->get()[0];
     
        //if(isset($id)) abort(404);
        if($editdata) {

            return view('backend.pages.p2_tab_edit_equity_category_page')->with(['editdata'=>$editdata]);
        }
        
        abort(404);
    }


    /**
     * Receive POST command to add new equity company data
     * @param $request parameters list with fields 
     *        plan_start, plan_end, company_code, company_name,
     *        company_addr, company_phone, company_fax 
     */
    public function postAdd(Request $request)
    {
         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');
        

        //Log::info('EquityCompanyManagementController::PoseAdd::=>' . $request);
        //Log::info(' postAdd: equity_start=>' . toEnglishDate($request["equity_start"])); 

        $ret = false;
        $datestart  = new Date(toEnglishDate($request["equity_start"]));
        $datereq = $request["equity_end"];
         
        
        if ($request["company_addr"] == '') {

        }

        //print($request);
        //exit();
        if ($request["equity_start"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันเริ่มต้น ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        if ($request["equity_end"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันสิ้นสุด ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }
        

        // Log::info(' postAdd: equity_start=>' . toEnglishDate($datereq));   

        $dateEnd = new Date(toEnglishDate($datereq));

        $today   = new Date();
        $data    = array();

        array_push($data,array(
            'NAME_SHT' => $request["name_sht"],
            //Securities_Name
            'SECURITIES_NAME' =>$request["company_name"],
            'ADDRESS' => $request["company_addr"],

            'START_DATE' => $datestart,
            'END_DATE' => $dateEnd,

            'PHONE' => $request["company_phone"],
            'FAX' => $request["company_fax"],
            'CREATE_DATE' => $today,
            'CREATE_BY' => "Admin"
        ));


        $chk = "SELECT COUNT(NAME_SHT) As total FROM TBL_P2_EQUITY_SECURITIES WHERE NAME_SHT = '". $request["name_sht"]. "'";
        $all = DB::select(DB::raw($chk));
        $total =  $all[0]->total;

        $rethtml = "";


        if ($total > 0) {
           $rethtml = "Short code ที่ท่านใส่มีอยู่ในระบบแล้ว";

        } else {
           $insert = DB::table('TBL_P2_EQUITY_SECURITIES')->insert($data);
           $ret = $insert;
        }


        return response()->json(array('success' => $ret, 'html'=>$rethtml));

//        if($insert) {
//            return redirect()->action('UserGroupController@userGroups');
//        } else {
//            return redirect()->action('UserGroupController@getAddUserGroup')->with('submit_errors', 'ไม่สามารถเพิ่มข้อมูลกลุ่มผู้ใช้ได้');
//        }

    }


    /**
     * Receive POST command to update equity company data
     * @param $request parameters list with fields 
     *        plan_start, plan_end, company_code, company_name,
     *        company_addr, company_phone, company_fax 
     */
    public function postEdit(Request $request)
    {
        $ret = false;
        $datestart  = new Date(toEnglishDate($request["equity_start"]));
        $datereq = $request["equity_end"];

        if ($request["equity_end"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันสิ้นสุด ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }
        if ($request["equity_start"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันเริ่มต้น ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        Log::info(' postEdit: request =>' . $request);   

        $dateEnd = new Date(toEnglishDate($datereq));
        $today = new Date();

        $data = array(

            //'NAME_SHT' => $request["name_sht"],
            'SECURITIES_NAME' =>$request["company_name"],
            'ADDRESS' => $request["company_addr"],

            'START_DATE' => $datestart,
            'END_DATE' => $dateEnd,

            'PHONE' => $request["company_phone"],
            'FAX' => $request["company_fax"],

            'CREATE_BY' => "Admin"
        );

        /**
         * update equity company to database
         */

        $update = DB::table('TBL_P2_EQUITY_SECURITIES')->where('NAME_SHT', "=", $request["name_sht"])->update($data);

        $ret = $update;

        return response()->json(array('success' => $ret, 'html'=>'OK'));

    }

}
