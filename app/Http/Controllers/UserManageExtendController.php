<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package\Curl;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Input;
use Illuminate\Http\UploadedFile;

class UserManageExtendController extends Controller
{

    public function getsimple()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 51,
            'menu_id' => 6,
            'title' => getMenuName($data,51,6) ." | MEA"
        ]);

        //$user_group = DB::table('TBL_PRIVILEGE')->select('USER_PRIVILEGE_ID','USER_PRIVILEGE_DESC')->orderBy('USER_PRIVILEGE_ID', 'asc')->get();

        return view('backend.pages.userextend');
    }

    public  function  dowloadsample(){
        $file = 'contents/sample/member_contribution.xls';
        $headers = array(
            'Content-Type: application/pdf',
        );
        return \Response::download($file, 'member_contribution.xls', $headers);
    }

    public  function  Checkdate(Request $request){

        /* limit execution timeout 30 sec.*/
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $results = null;

        $result=   Excel::load($request->file('exelimport'))->get();
        $count = $result->count();

        return response()->json(array('success' => true, 'html'=>$count));
    }


    public function importdata(Request $request){

        /* limit execution timeout 30 sec.*/
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        /* statistics */ 
        $count       = 0;
        $skipedCount = 0;
        $passedCount = 0;
        $total_records = 0;
        $progress_count = 0;
        $htmlResult = '';
       
        Session::put('progress_count',0);
        Session::put('total_records', 0);
       
        $results = null;

        $file = $request->file('exelimport');
       
        
        $request->file('exelimport')->move(storage_path().'/public/import/' , 'import.xlsx');

        $inputfile = storage_path('/public/import/import.xlsx');

      
      //  $ret = Excel::filter('chunk')->load(storage_path('/public/import/import.xlsx'))->chunk(250, function($results){
//
  //          foreach($results as $index => $value) {

         $retdate = Excel::load($inputfile, function($reader) use(&$count, 
                                                                 &$skipedCount, 
                                                                 &$passedCount, 
                                                                 &$total_records, 
                                                                 &$progress_count,
                                                                 &$htmlResult) {
            $results = $reader->get(); 
            $ret = $results->toArray();
            $htmlResult = '';

            $progress_count = 0;
            $total_records = $results->count();

            Session::put('progress_count',$progress_count);
            Session::put('total_records', $total_records);

            $percent = 0;
            
            $progress_count = 0;
           
            foreach($ret as $index => $value) {

                $count++;

                if (($value == null) || ($value["emp_id"] == null) || (strlen($value["emp_id"]) < 2) || (strlen($value["contribution_start_date"]) < 4 )) {
                    $skipedCount++;
                    continue;
                }

                $im_date_start = $value["contribution_start_date"];
                $ret_data_start = str_replace("'","",$im_date_start);



                $im_date_end = $value["contribution_end_date"];
                $ret_data_end = str_replace("'","",$im_date_end);

                $im_date_modify = $value["contribution_modify_date"];
                $ret_data_modify = str_replace("'","",$im_date_modify);

//                var_dump($ret_data_start);

//                var_dump($ret_data_start);

                $date_start = new Date($ret_data_start);
                $date_end = new Date($ret_data_end);
                $date_modify = new Date($ret_data_modify);

                

                /* update progress */
                $progress_count++;
                if($progress_count % 2 == 0)
                    Session::put('progress_count', $progress_count);
                 

                $update = array(
                    'EMP_ID' => $value["emp_id"],
                    'CONTRIBUTION_START_DATE' =>$date_start,
                    'CONTRIBUTION_END_DATE' => $date_end,
                    'CONTRIBUTION_MODIFY_DATE' => $date_modify,
                    'CONTRIBUTION_RATE_OLD' => $value["contribution_rate_old"],
                    'CONTRIBUTION_RATE_NEW' => $value["contribution_rate_new"]


                );

                $affected = DB::table('TBL_EMPLOYEE_INFO')->where('EMP_ID',"=",$value["emp_id"])->update($update);
                if($affected < 1) {
                    if(strlen($htmlResult) < 500) 
                    {
                            $htmlResult .= '<tr><td><span style=\"color: red\">' . $value["emp_id"] .     '</span></td>';
                            $htmlResult .= '    <td><span style=\"color: red\">' . $date_modify .         '</span></td></tr>';
                    }
                } else {
                    $passedCount++; 
                }

            } // foreach

        });

        $header = 'รายละเอียดข้อมูลที่ ไม่สำเร็จ <BR> '.
                  '<table style=\"width: 100px;\" class=\"table table-bordered\">' .
                  '<tr><td> EMP_ID | </td><td> MODIFIED_DATE </td></tr>';
        $footer = '</table>  ';

        if(strlen($htmlResult) < 1) {
            $header  = '';
            $footer = '';  
            $htmlResult = '';
        }

        /* put 100% */ 
        Session::put('progress_count', $total_records);

        return response()->json(array('success' => true, 'html'=>'<br>' . 
                ' Total : ' . $count . ' Record(s), '. 
                ' Passed: ' . $passedCount . ' Record(s), '. 
                ' Failed: ' . ($count - $passedCount) . ' Record(s), '. 
                // ' progress_count:' . Session::get('progress_count') . 
                //  ' Skiped: ' . $skipedCount . ' Record(s)  ' .
                ' <BR> '. $header . $htmlResult . $footer));//$retdate));



        //return response()->json(array('success' => true, 'html'=>$ret));
    }


}
