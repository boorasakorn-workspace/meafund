<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function pageSetting($data)
    {
        if(isset($data['MENU_ID']) && isset($data['MENU_GROUP_ID']))
        {
            if($data['MENU_ID'] && $data['MENU_GROUP_ID']) {
                if(!menu_access($data['MENU_ID'], $data['MENU_GROUP_ID'])) {
                    //abort(404);
                    if($data['menu_id'] && $data['menu_group_id'])
                        if(!menu_access($data['menu_id'], $data['menu_group_id']))
                           abort(404);
                }
            }
        }
        view()->share('page_setting', $data);
    }



}
