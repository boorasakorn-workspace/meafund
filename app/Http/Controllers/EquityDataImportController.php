<?php
/**
 * FILE: EquityDataImportController.php
 * Phase#2 Equity Data Import Controller 
 * Created: 2016/12/23 00:06
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\UserGroup;
use Illuminate\Support\Facades\Validator;
use Jenssegers\Date\Date;

use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Input;
use Illuminate\Http\UploadedFile;

use App\User;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

use App\Libraries\MEAUtils;

class EquityDataImportController extends Controller
{
    
    public function getindex()
    {
        $viewname = 'backend.pages.p2_60_2_equity_tab2_data_import_page';
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 60,
            'menu_id' => 2,
            'title' => getMenuName($data, 60, 2) . ' | MEA'
        ] );

        return view($viewname);
    }
    
	
    public  function Ajax_Index(Request $request){

        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;

        $Datacount = $this->getCountAll();
        $Data = $this->getData($ArrParam);

        $totals = count($Datacount);

        $htmlPaginate =Paginatre_gen($totals,$PageSize,'page_click_search',$PageNumber);

        $returnHTML = view('backend.pages.ajax.ajax_p2_60_2_equity_data_import')->with([
            'htmlPaginate'=> $htmlPaginate,
            'data'=>$Data

        ])->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }

    public  function  getCountAll(){
    
        return DB::table('TBL_P2_EQUITY_SECURITIES')->orderby("Name_Sht")->get();    
    }

    public  function  getData($ArrParam){

        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];

        $query =  "SELECT * FROM TBL_P2_EQUITY_SECURITIES ORDER BY NAME_SHT OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
        return DB::select(DB::raw($query));
    }

    /**
     * Handle request delete single/multiple records.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function delete(Request $request)
    {
        $deleted = false;
        $arrId = explode(',',$request->input('group_id'));

        foreach($arrId as $index => $item){

            if($item != ""){
                $deleted =  DB::table('TBL_P2_EQUITY_SECURITIES')->where('NAME_SHT',"=", $item)->delete();
            }
        }

        if($deleted)  {
            return response()->json(["ret" => "1"]);
        } else {
            return response()->json(["ret" => "0"]);
        }
    }


    /**
     * Get view equity company .
     * 
     * @param  None
     * @return view\backend\pages "p2_tab1_add_bond_company_page"
     */
    public function getAdd()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data,60, 1) . ' | MEA'
        ] );


        return view('backend.pages.p2_bond_tab1_add_company_page');
    }

    /**
     * Query equity company data by specifiled code as parameter id.
     * 
     * @param  Equity company code  $id
     * @return view backend.pages.p2_edit_equity_company_page
     */
    public function getEdit($id)
    {
        $data = getmemulist();
        $this->pageSetting([
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data, 60, 1) . ' | MEA'
        ]);


        $editdata = DB::table('TBL_P2_EQUITY_SECURITIES')->where("NAME_SHT" ,"=",  $id)->first();
        // $editdata = DB::table('TBL_P2_EQUITY_SECURITIES')->where("NAME_SHT" ,"=",  $id)->get()[0];
     
        //if(isset($id)) abort(404);
        if($editdata) {

            return view('backend.pages.p2_bond_tab1_edit_company_page')->with(['editdata'=>$editdata]);
        }
        
        abort(404);
    }


    /**
     * Receive POST command to add new equity company data
     * @param $request parameters list with fields 
     *        plan_start, plan_end, company_code, company_name,
     *        company_addr, company_phone, company_fax 
     */
    public function postAdd(Request $request)
    {
         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');
        

        Log::info('BondCompanyManagementController::PoseAdd::=>' . $request);
        Log::info(' postAdd: equity_start=>' . toEnglishDate($request["equity_start"])); 

        $ret = false;
        $datestart  = new Date(toEnglishDate($request["equity_start"]));
        $datereq = $request["equity_end"];
         
        
        if ($request["company_addr"] == '') {

        }

        
        if ($request["equity_start"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันเริ่มต้น ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        if ($request["equity_end"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันสิ้นสุด ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }
        

        Log::info(' postAdd: equity_start=>' . toEnglishDate($datereq));   
        $dateEnd = new Date(toEnglishDate($datereq));

        $today   = new Date();
        $data    = array();

        array_push($data,array(
            'NAME_SHT' => $request["name_sht"],
            //Securities_Name
            'SECURITIES_NAME' =>$request["company_name"],
            'ADDRESS' => $request["company_addr"],

            'START_DATE' => $datestart,
            'END_DATE' => $dateEnd,

            'PHONE' => $request["company_phone"],
            'FAX' => $request["company_fax"],
            'CREATE_DATE' => $today,
            'CREATE_BY' => "Admin"
        ));


        $chk = "SELECT COUNT(NAME_SHT) As total FROM TBL_P2_EQUITY_SECURITIES WHERE NAME_SHT = '". $request["name_sht"]. "'";
        $all = DB::select(DB::raw($chk));
        $total =  $all[0]->total;

        $rethtml = "";


        if ($total > 0) {
           $rethtml = "Short code ที่ท่านใส่มีอยู่ในระบบแล้ว";

        } else {
           $insert = DB::table('TBL_P2_EQUITY_SECURITIES')->insert($data);
           $ret = $insert;
        }


        return response()->json(array('success' => $ret, 'html'=>$rethtml));

    }


    /**
     * Receive POST command to update equity company data
     * @param $request parameters list with fields 
     *        plan_start, plan_end, company_code, company_name,
     *        company_addr, company_phone, company_fax 
     */
    public function postEdit(Request $request)
    {
        $ret = false;
        $datestart  = new Date(toEnglishDate($request["equity_start"]));
        $datereq = $request["equity_end"];

        if ($request["equity_end"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันสิ้นสุด ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }
        if ($request["equity_start"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันเริ่มต้น ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        Log::info(' postEdit: request =>' . $request);   

        $dateEnd = new Date(toEnglishDate($datereq));
        $today = new Date();

        $data = array(

            //'NAME_SHT' => $request["name_sht"],
            'SECURITIES_NAME' =>$request["company_name"],
            'ADDRESS' => $request["company_addr"],

            'START_DATE' => $datestart,
            'END_DATE' => $dateEnd,

            'PHONE' => $request["company_phone"],
            'FAX' => $request["company_fax"],

            'CREATE_BY' => "Admin"
        );

        /**
         * update equity company to database
         */

        $update = DB::table('TBL_P2_EQUITY_SECURITIES')->where('NAME_SHT', "=", $request["name_sht"])->update($data);

        $ret = $update;

        return response()->json(array('success' => $ret, 'html'=>'OK'));

    }

           
    /**
     * TAB#2 : Equity Data Import  
     */
    public function T2_getimport()
    {
        Log::info(get_class($this) .'::'. __FUNCTION__);
       

        $viewname = 'backend.pages.p2_60_2_equity_tab2_data_import_page';
        $data = getmemulist();
        $this->pageSetting([
            'menu_group_id' => 60,
            'menu_id' => 2,
            'title' => getMenuName($data, 60, 2) . ' | MEA'
        ]);

        $dataTypelist = $this->getDataTypeList();
        $policy =  $this->getInvestmentPolicy();

        $securities = $this->getEquitySecurities();

        Log::info(get_class($this) .'::'. __FUNCTION__ .' >> '. getMenuName($data, 60, 2));

      
        return  view($viewname)->with([
                     'typelist' => $dataTypelist,
                     'policyList' =>$policy,
                     'securities' => $securities

                 ]);
    }


    public  function T2_Ajax_Index(Request $request) 
    {
         Log::info(get_class($this) .'::'. __FUNCTION__. ' \n request = ' . $request);
       
         $dataTypelist = $this->getDataTypeList();
         $policy =  $this->getInvestmentPolicy();

         $returnHTML = view('backend.pages.ajax.ajax_p2_60_2_equity_data_import')->with([
            'typelist' => $dataTypelist,
            'policyList' =>$policy

        ])->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }


    public  function  getDataTypeList() 
    {
        /* limit execution timeout */
        //ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        //ini_set('memory_limit', '-1');
        // Log::info(get_class($this) .'::'. __FUNCTION__. ' fetch from TBL_P2_EQUITY_IMPORT_TYPE' );
    
        // return DB::table('TBL_P2_EQUITY_IMPORT_TYPE')->orderby("DESCRIPTION")->get(); 
        return DB::table('TBL_P2_EQUITY_IMPORT_TYPE')->orderby("ID")->get();     
    }

    public  function  getInvestmentPolicy() 
    {

        /* limit execution timeout */
        //ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        //ini_set('memory_limit', '-1');
      
        $query =  "SELECT * FROM TBL_INVESTMENT_POLICY  ORDER BY POLICY_NAME ";
        Log::info(get_class($this) .'::'. __FUNCTION__. ' SQLSTR=' . $query );
    
        return DB::select(DB::raw($query));
    }


    public function T2_ImportInsertEquityNAV($inputfile, $checkFileOnly) {
        // fields

        $validate_key = 'ตราสารทุน';
        $results = null;
        $error_msg = '';
        $htmlResult = '';
        $filedate = '';
        $filedatethai ='';

        $passedCount = 1;
        $count = 0;
        $passedCount = 1;
        $status = false;


        $retdate = Excel::load($inputfile, function($reader) use(&$validate_key,
                                                                 &$checkFileOnly,
                                                                 &$status,
                                                                 &$count, 
                                                                 &$error_msg,
                                                                 &$filedate ,
                                                                 &$filedatethai,
                                                                 &$htmlResult) {
            // first row not a header
            $reader->noHeading();

            $results = $reader->get(); 
            $ret = $results->toArray();
            $htmlResult = '';
            Log::info('1) -> foreach($ret as $rs => $value)');

            // Retrive logged ID
            $user_data = Session::get('user_data');
            Log::info('SESSION: [user_data] ='. $user_data->emp_id);

            // database fields 
            $JOB_ID  = '';          //  0 (Auto field) 
            $DEPOSIT_RATIO = 0.0;    //  1
            $DEPOSIT_MB = 0.0;       //  2
            $DEPOSIT_B = 0.0;        //  3
            $STOCK_RATIO = 0.0;      //  4  
            $STOCK_MB = 0.0;         //  5
            $STOCK_B = 0.0;          //  6 
            $OTHER_RATIO = 0.0;      //  7
            
            $OTHER_MB = 0.0;         //  8
            $OTHER_B = 0.0;          //  9
            $NAV_MB = 0.0;           // 10
            $NAV_B = 0.0;            // 11
            $NAV_UNIT = 0.0;         // 12
            $YIELD_MONTH = 0.0;      // 13
            $YIELD_CUMULATIVE = 0.0; // 14

            $REFERENCE_DATE = '';   // 15
            $CREATE_DATE = date("Y-m-d H:i:s");      // 16
            $CREATE_BY = $user_data->emp_id;   // 17

            // loop rows
            foreach($ret as $rs => $value) {
                $count++;
                $data = array();

                
                $index = 0;

                $rowindex = ($count - 1);
                Log::info('[' . $rowindex . ']----- START -----');
                
                $caption = '';

                // loop each column 
                foreach($value as $cols => $col) {
                    // ประเภทตราสาร (A2) การลงทุน ณ 31 พฤษภาคม
                     Log::info('row['. $rowindex . '][' .$index .'] ' . $col);

                    if($count == 1){
                        // this is first row!, we will extract date from this row[0][0]
                        $filedate = $col;
                        $filedatethai = $col;
                        Log::info('3). Before str_replace ' . $filedate ); 
                        $filedate = trim(str_replace('การลงทุน ณ ', ' ', $filedate)) ;
                        Log::info( $filedate );  
                        // convert firstrow to date english
                        $utils = new MEAUtils();

                        $filedate = $utils->toEnglishDateEx($filedate);// . ' ' . '00:00:00';
                        Log::info('File Date :' . $filedate ); 
                        //$filedate = $utils->toEnglishDateEx($filedate);// . ' ' . '00:00:59';
                        break;

                    } else {

                        switch($rowindex) {
                            case 0:
                                Log::info('row['. $rowindex . '][' .$index .'] ' . $col);
                                break;

                            case 1:
                                if (($index == 1) && (strpos($col, $validate_key) === false)) {
                                    // invalid format!
                                    return array('status' =>$status, 'filedate' => $filedatethai);
                                }
                                break;
                                
                            case 2:
                                Log::info('row['. $rowindex . '][' .$index .'] ' . $col);
                                break;

                            case 3:
                                switch($index) {
                                    case 0:
                                        $caption  = $col;
                                        break;
                                    case 1:
                                        $DEPOSIT_RATIO = round((float)$col, 4);
                                        break;
                                    case 2:
                                        $DEPOSIT_MB = round((float)$col, 4);
                                        break;
                                    case 3: 
                                        $DEPOSIT_B = round((float)$col, 4);
                                        break;
                                    default:
                                        break;   
                                }
                                break;

                           case 4:
                                switch($index) {
                                    case 0:
                                        $caption  = $col;
                                        break;
                                    case 1:
                                        $STOCK_RATIO = round((float)$col, 4);
                                        break;
                                    case 2:
                                        $STOCK_MB = round((float)$col, 4);
                                        break;
                                    case 3: 
                                        $STOCK_B = round((float)$col, 4);
                                        break;
                                    default:
                                        break;   
                               }
                               break;

                            case 5:
                               switch($index) {
                                    case 0:
                                        $caption  = $col; 
                                        break;
                                    case 1:
                                        $OTHER_RATIO = round((float)$col, 4);
                                        break;
                                    case 2:
                                        $OTHER_MB = round((float)$col, 4);
                                        break;
                                    case 3: 
                                        $OTHER_B = round((float)$col, 4);
                                        break;
                                    default:
                                        break;   
                               }
                               break;
                           
                             
                            case 6:
                                switch($index) {
                                    case 0:
                                        $caption  = $col;
                                        break;
                                    case 1:
                                        break;
                                    case 2:
                                        break;
                                    case 3: 
                                        break;
                                    default:
                                        break;   
                               }
                               break;
                                

                            case 7:
                               switch($index) {
                                    case 0:
                                        $caption  = $col;
                                        break;

                                    case 1:
                                        break;
                                    case 2:
                                        break;
                                    case 3: 
                                        break;
                                    default:
                                        break;   
                               }
                               break;

                            case 8:
                               switch($index) {
                                    case 0:
                                        $caption  = $col;
                                        break;
                                    case 1:
                                        break;
                                    case 2:
                                        $NAV_MB = round((float)$col, 4);
                                        break;
                                    case 3: 
                                        $NAV_B = round((float)$col, 4);
                                        break;
                                    default:
                                        break;   
                               }
                               break;

                            case 9:
                                switch($index) {
                                    case 0:
                                        $caption  = $col;
                                        break;
                                    case 1:
                                        break;
                                    case 2:
                                        break;
                                    case 3: 
                                        $NAV_UNIT = round((float)$col, 4);
                                        break;
                                    default:
                                        break;   
                                }
                                break;

                            case 10:
                                switch($index) {
                                    case 0:
                                        $caption  = $col;
                                        break;
                                    case 1:
                                        break;
                                    case 2:
                                        break;
                                    case 3: 
                                        $YIELD_MONTH = round((float)$col, 4);
                                        break;
                                    default:
                                        break;   
                                }
                                break;

                            case 11:
                                switch($index) {
                                    case 0:
                                        $caption  = $col;
                                        break;
                                    case 1:
                                        break;
                                    case 2:
                                        break;
                                    case 3: 
                                        $YIELD_CUMULATIVE = round((float)$col, 2);
                                        break;
                                    default:
                                        break;   
                                }
                                break;

                            default:
                                break;    

                       } // END: switch $rowindex

                    } // if else 

                    $index++;

                } // for each column
               
                if ($rowindex < 3) {
                    Log::info('Skipped rows [ ' . $rowindex . ']');
                    continue;
                }
                Log::info('[' . $rowindex . ']----- END -----');

            } // for each rows

            // preparing statment to insert into database
            $CREATE_DATE = date("Y-m-d H:i:s");
            $REFERENCE_DATE = $filedate;
            array_push($data, array(
                'JOB_ID' => 0,       //  1
                'DEPOSIT_RATIO' => $DEPOSIT_RATIO,       //  1
                'DEPOSIT_MB' => $DEPOSIT_MB,             //  2
                'DEPOSIT_B' => $DEPOSIT_B,               //  3
                'STOCK_RATIO' => $STOCK_RATIO,           //  4  
                'STOCK_MB' => $STOCK_MB,                 //  5
                'STOCK_B' => $STOCK_B,                   //  6 
                'OTHER_RATIO' => $OTHER_RATIO,           //  7
                'OTHER_MB' => $OTHER_MB,                 //  8
                'OTHER_B' => $OTHER_B,                   //  9
                'NAV_MB' => $NAV_MB,                     // 10
                'NAV_B' => $NAV_B,                       // 11
                'NAV_UNIT' => $NAV_UNIT,                 // 12
                'YIELD_MONTH' => $YIELD_MONTH,           // 13
                'YIELD_CUMULATIVE' => $YIELD_CUMULATIVE, // 14
                'REFERENCE_DATE' => $REFERENCE_DATE,     // 15
                'CREATE_DATE' => $CREATE_DATE,           // 16
                'CREATE_BY' => $CREATE_BY                // 17
            ));
            Log::info($data);

            // check file only ?
            if(!$checkFileOnly) {
                $found = DB::table('TBL_P2_EQUITY_NAV')->where('REFERENCE_DATE', $REFERENCE_DATE)->first();
                if ($found) {
                    // delete previous first, then insert new record to database
                    DB::table('TBL_P2_EQUITY_NAV')->where('REFERENCE_DATE', $REFERENCE_DATE)->delete();
                }

                $affected = DB::table('TBL_P2_EQUITY_NAV')->insert($data);
                $status = ($affected > 0);
            } else {
                $status = true;
            }
        });

       // $user_data = Session::get('user_data');
       
      // $filedate = $user_data->emp_id;
       return array('status' =>$status, 'filedate' => $filedatethai, 'error_msg' => $error_msg);
    }

    public function T2_ImportInsertBondNAV($inputfile, $checkFileOnly) {
        
        $validate_key = 'ตราสารหน';

        $results = null;
        $error_msg = '';
        $htmlResult = '';
        $filedate = '';
        $filedatethai = '';

        $passedCount = 1;
        $count = 0;
        $passedCount = 1;
        $status = false;

        $retdate = Excel::load($inputfile, function($reader) use(&$validate_key,
                                                                 &$checkFileOnly,
                                                                 &$status,
                                                                 &$count, 
                                                                 &$error_msg,
                                                                 &$filedate ,
                                                                 &$filedatethai,
                                                                 &$htmlResult) {
            $reader->noHeading();
            $results = $reader->get(); 
            $ret = $results->toArray();
            $htmlResult = '';
            
             
            // Retrive logged ID
            $user_data = Session::get('user_data');
            Log::info('SESSION: [user_data] ='. $user_data->emp_id);

            $JOB_ID  = 0;          //  0 (Auto field)
            $GOV_BOND_RATIO = 0.0;   //  1   
            $GOV_BOND_MB = 0.0;      //  2
            $GOV_BOND_B = 0.0;       //  3
            $DEPOSIT_RATIO = 0.0;    //  4
            $DEPOSIT_MB = 0.0;       //  5
            $DEPOSIT_B = 0.0;        //  6
            $DEBENTURE_RATIO = 0.0;  //  7
            $DEBENTURE_MB = 0.0;     //  8
            $DEBENTURE_B = 0.0;      //  9 
            $OTHER_RATIO = 0.0;      // 10 
            $OTHER_MB = 0.0;         // 11
            $OTHER_B = 0.0;          // 12
            $NAV_MB = 0.0;           // 13 
            $NAV_B = 0.0;            // 14 
            $NAV_UNIT = 0.0;         // 15
            $YIELD_MONTH = 0.0;      // 16 
            $YIELD_CUMULATIVE = 0.0; // 17
            $REFERENCE_DATE = '';   // 18
            $CREATE_DATE = date("Y-m-d H:i:s");      // 19
            $CREATE_BY = $user_data->emp_id;   // 20

            // loop rows
            foreach($ret as $rs => $value) {
                $count++;
                $data = array();

                $index = 0;

                $rowindex = ($count - 1);
                Log::info('[' . $rowindex . ']----- START -----');
                
                $caption = '';

                // loop each column 
                foreach($value as $cols => $col) {
                    // ประเภทตราสาร (A2) การลงหนี้ ณ 31 พฤษภาคม
                    Log::info('row['. $rowindex . '][' .$index .'] ' . $col);
                    if($count == 1) {
                        // this is first row!, we will extract date from this row[0][0]
                        $filedate = $col;
                        $filedatethai = $col;
                        Log::info('3). Before str_replace ' . $filedate ); 
                        $filedate = trim(str_replace('การลงทุน ณ ', ' ', $filedate)) ;
                        Log::info( $filedate );  
                        // convert firstrow to date english
                        $utils = new MEAUtils();

                        $filedate = $utils->toEnglishDateEx($filedate); 
                        Log::info('File Date :' . $filedate ); 
                         
                        break;

                    } else {
                        switch($rowindex) {
                            case 0:
                                Log::info('row['. $rowindex . '][' .$index .'] ' . $col);  
                                break;

                            case 1:
                                if (($index == 1) && (strpos($col, $validate_key) === false)) {
                                    // invalid format!
                                     Log::info('row['. $rowindex . '][' .$index .'] ' . $col .' ERROR: Invalid file format.');  
                                    return array('status' =>$status, 'filedate' => $filedatethai);
                                }
                                 
                                break;
                            case 2:
                                Log::info('row['. $rowindex . '][' .$index .'] ' . $col);
                                break;

                            case 3:
                                switch($index) {
                                    case 0:
                                        $caption  = $col;
                                        break;
                                    case 1:
                                        $GOV_BOND_RATIO = round((float)$col, 4);
                                        break;
                                    case 2:
                                        $GOV_BOND_MB = round((float)$col, 4);
                                        break;
                                    case 3: 
                                        $GOV_BOND_B = round((float)$col, 4);
                                        break;
                                    default:
                                        break;   
                                }
                                break;

                           case 4:
                                switch($index) {
                                    case 0:
                                        $caption  = $col;
                                        break;
                                    case 1:
                                        $DEPOSIT_RATIO = round((float)$col, 4);
                                        break;
                                    case 2:
                                        $DEPOSIT_MB = round((float)$col, 4);
                                        break;
                                    case 3: 
                                        $DEPOSIT_B = round((float)$col, 4);
                                        break;
                                    default:
                                        break;   
                               }
                               break;

                            case 5:
                               switch($index) {
                                    case 0:
                                        $caption  = $col; 
                                        break;
                                    case 1:
                                        $DEBENTURE_RATIO = round((float)$col, 4);
                                        break;
                                    case 2:
                                        $DEBENTURE_MB = round((float)$col, 4);
                                        break;
                                    case 3: 
                                        $DEBENTURE_B = round((float)$col, 4);
                                        break;
                                    default:
                                        break;   
                               }
                               break;
                           
                            
                            case 6:
                                switch($index) {
                                    case 0:
                                        $caption  = $col;
                                        break;
                                    case 1:
                                        $OTHER_RATIO = round((float)$col, 4);
                                        break;
                                    case 2:
                                        $OTHER_MB = round((float)$col, 4);
                                        break;
                                    case 3: 
                                        $OTHER_B = round((float)$col, 4);
                                        break;
                                    default:
                                        break;   
                               }
                               break;
                                
                            // skip this rowindex 
                            case 7:
                               switch($index) {
                                    case 0:
                                        $caption  = $col;
                                        break;

                                    case 1:
                                        
                                        break;
                                    case 2:
                                       
                                        break;
                                    case 3: 
                                        
                                        break;
                                    default:
                                        break;   
                               }
                               break;

                            // skip this rowindex 
                            case 8:
                               switch($index) {
                                    case 0:
                                        $caption  = $col;
                                        break;
                                    case 1:
                                         
                                        break;
                                    case 2:
                                        
                                        break;
                                    case 3: 
                                        
                                        break;
                                    default:
                                        break;   
                               }
                               break;

                            case 9:
                                switch($index) {
                                    case 0:
                                        $caption  = $col;
                                        break;
                                    case 1:
                                        break;
                                    case 2:
                                        $NAV_MB = round((float)$col, 4);
                                        break;
                                    case 3: 
                                        $NAV_B = round((float)$col, 4);
                                        break;
                                    default:
                                        break;   
                                }
                                break;

                            case 10:
                                switch($index) {
                                    case 0:
                                        $caption  = $col;
                                        break;
                                    case 1:
                                        break;
                                    case 2:
                                        break;
                                    case 3: 
                                        $NAV_UNIT = round((float)$col, 4);
                                        break;
                                    default:
                                        break;   
                                }
                                break;

                            case 11:
                                switch($index) {
                                    case 0:
                                        $caption  = $col;
                                        break;
                                    case 1:
                                        break;
                                    case 2:
                                        break;
                                    case 3: 
                                        $YIELD_MONTH = round((float)$col, 2);
                                        break;
                                    default:
                                        break;   
                                }
                                break;

                            case 12:
                                switch($index) {
                                    case 0:
                                        $caption  = $col;
                                        break;
                                    case 1:
                                        break;
                                    case 2:
                                        break;
                                    case 3: 
                                        $YIELD_CUMULATIVE = round((float)$col, 2);
                                        break;
                                    default:
                                        break;   
                                }
                                break;
    

                            default:
                                break;    

                       }// END: switch $rowindex

                    } // if else (count)
                    
                    // increment column index
                    $index++;

                } // foreach column 

                
                if ($rowindex < 3) {
                    Log::info('Skipped rowindex [ ' . $rowindex . ']');
                    continue;
                } 

                Log::info('[' . $rowindex . ']----- END -----');

                // preparing statment to insert into database
                $CREATE_DATE = date("Y-m-d H:i:s");
                $REFERENCE_DATE = $filedate;
                array_push($data, array(
                   'JOB_ID' => 0,                          //  0 (Auto field)
                   'GOV_BOND_RATIO' =>  $GOV_BOND_RATIO,   //  1  
                   'GOV_BOND_MB' => $GOV_BOND_MB,          //  2
                   'GOV_BOND_B' => $GOV_BOND_B,            //  3
                   'DEPOSIT_RATIO' => $DEPOSIT_RATIO,      //  4
                   'DEPOSIT_MB' => $DEPOSIT_MB,            //  5
                   'DEPOSIT_B' => $DEPOSIT_B,              //  6
                   'DEBENTURE_RATIO' => $DEBENTURE_RATIO,  //  7
                   'DEBENTURE_MB' => $DEBENTURE_MB,        //  8
                   'DEBENTURE_B' => $DEBENTURE_B,          //  9 
                   'OTHER_RATIO' => $OTHER_RATIO,          // 10 
                   'OTHER_MB' => $OTHER_MB,                // 11
                   'OTHER_B' => $OTHER_B,                  // 12
                   'NAV_MB' => $NAV_MB,                    // 13 
                   'NAV_B' => $NAV_B,                      // 14 
                   'NAV_UNIT' => $NAV_UNIT,                // 15
                   'YIELD_MONTH' => $YIELD_MONTH,          // 16 
                   'YIELD_CUMULATIVE' =>$YIELD_CUMULATIVE, // 17
                   'REFERENCE_DATE' => $REFERENCE_DATE,    // 18
                   'CREATE_DATE' => $CREATE_DATE,          // 19
                   'CREATE_BY' => $CREATE_BY,              // 20
                ));

                Log::info($data);

                if(!$checkFileOnly) {
                    $found = DB::table('TBL_P2_BOND_NAV')->where('REFERENCE_DATE', $REFERENCE_DATE)->first();
                    if ($found) {
                        // delete previous first, then insert new record to database
                        DB::table('TBL_P2_BOND_NAV')->where('REFERENCE_DATE', $REFERENCE_DATE)->delete();
                    }

                    $affected = DB::table('TBL_P2_BOND_NAV')->insert($data);
                    $status = ($affected > 0);
                } else {
                    $status = true;
                }
            }// for loop
        });

        return array('status' =>$status, 'filedate' => $filedatethai);
    }

    /**
     *  Check uploaded file format
     *  @param  request $request
     *  @return json with status, details
     */
    public function T2_checkfile(Request $request) {
        /* limit execution timeout */
        ini_set('max_execution_time', 50000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $EQUITY_TBL = "1"; // นโยบายตราสารทุน
        $BOND_TBL = "2"; // นโยบายตราสารทุน

        Log::info(get_class($this) .'::'. __FUNCTION__. ' datatype: ' . $request->input('datatype') . ',  policy' .  $request->input('policy'));
        $results = null;
        $error_msg = '';
        $htmlResult = '';


        $passedCount = 1;
        $count = 0;
        $passedCount = 1;


        $datatype = $request->input('datatype');//$request->get('datatype');
        $policy = $request->input('policy'); 
        $file = $request->file('exelimport');

        $process_file = '60_2_checkimport.xlsx';
        $request->file('exelimport')->move(storage_path() . '/public/import/', $process_file);
       
        $inputfile = storage_path('/public/import/' . $process_file);

        
        
        Log::info(get_class($this) .'::'. __FUNCTION__. ' Success ' ); 
        $status = true;
        ///////////////////////////////////
        $firstrow = '';
        $filedate = '';
        $error_msg ='';
        $arr = array();  
        if($policy == $EQUITY_TBL) {
           $arr = $this->T2_ImportInsertEquityNAV($inputfile, true);
           $status = $arr['status'];
           $filedate = $arr['filedate']; 
           // $error_msg = $arr['error_msg']; 
        } else if($policy == $BOND_TBL){
           $arr    = $this->T2_ImportInsertBondNAV($inputfile, true); 
           $status = $arr['status'];
           $filedate = $arr['filedate']; 
        } else {
           $filedate = '### ERROR: Invalid file format. ###';
           $status = false; 
        }
        //////////////////////////////////
        return response()->json(array('success' =>  $status, 'html'=>'' . 

                //' policy   : ' . $policy .
                //'<br/> datatype : ' . $datatype . 
                '<br/> status : ' . ($status ? 'ข้อมูลถูกต้อง กดปุ่ม นำเข้าข้อมูล เพื่อนำข้อมูลเข้าระบบ' : 'มีข้อผิดพลาด') . 
                '<br/> date   : ' . $filedate . ' <br/>'));
        
    } // T2_checkfile


    /**
     * Handle request delete category single/multiple records.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function T2_delete(Request $request)
    {
        $deleted = false;
        $arrId = explode(',', $request->input('group_id'));

        foreach($arrId as $index => $item) {

            if($item != "") {
                $deleted =  DB::table('TBL_P2_EQUITY_CATEGORY')->where('CATE_ID',"=", $item)->delete();
            }
        }

        if($deleted)  {
            return response()->json(["ret" => "1"]);
        } else {
            return response()->json(["ret" => "0"]);
        }
    }




    /**
     * Receive POST command to update bond company data
     * @param $request parameters list with fields 
     *        plan_start, plan_end, company_code, company_name,
     *        company_addr, company_phone, company_fax 
     */
    public function T2_postEdit(Request $request)
    {
        $ret = false;
        Log::info('T2_postEdit()');  

       
        return response()->json(array('success' => $ret, 'html'=>'OK'));

    }

    /**
     * Import data from .xlsx into database
     * @param $request parameters list with fields 
     *        datatype, policy and exelimport
     * @return array with status, filedate         
     */
    public function T2_ImportData(Request $request) {
         /* limit execution timeout */
        ini_set('max_execution_time', 50000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $EQUITY_TBL = "1"; // นโยบายตราสารทุน
        $BOND_TBL = "2"; // นโยบายตราสารทุน

        Log::info(get_class($this) .'::'. __FUNCTION__. ' datatype: ' . $request->input('datatype') . ',  policy' .  $request->input('policy'));
        $results = null;
        $error_msg = '';
        $htmlResult = '';


        $passedCount = 1;
        $count = 0;
        $passedCount = 1;


        $datatype = $request->input('datatype');//$request->get('datatype');
        $policy = $request->input('policy'); 
        $file = $request->file('exelimport');

        $process_file = '60_2_checkimport.xlsx';
        $request->file('exelimport')->move(storage_path() . '/public/import/', $process_file);
       
        $inputfile = storage_path('/public/import/' . $process_file);

        
        
        Log::info(get_class($this) .'::'. __FUNCTION__. ' Success ' ); 
        $status = true;
        ///////////////////////////////////
        $firstrow = '';
        $filedate = '';
      
        $arr = array();  
        if($policy == $EQUITY_TBL) {
           $arr = $this->T2_ImportInsertEquityNAV($inputfile, false);
           $status = $arr['status'];
           $filedate = $arr['filedate']; 
        } else if($policy == $BOND_TBL){
           $arr    = $this->T2_ImportInsertBondNAV($inputfile, false); 
           $status = $arr['status'];
           $filedate = $arr['filedate']; 
        } else {
           $filedate = '### ERROR: Invalid file format. ###';
           $status = false; 
        }

        //////////////////////////////////
        return response()->json(array('success' =>  $status, 'html'=>' ' . 
                //'policy   : ' . $policy .
                //'<br/> datatype : ' . $datatype . 
                '<br/> status : ' .   ($status ? "นำเข้าข้อมูลเรียบร้อย" : "มีข้อผิดพลาด") . 
                '<br/> date : ' . $filedate  . '<br/>'));
       
    }

   /*
    public function equityDataImportRRRRRRR(Request $request) 
    {

        // limit execution timeout 
        ini_set('max_execution_time', 30000);
        // unlimit memory size 
        ini_set('memory_limit', '-1');

        
        $count       = 0;
        $skipedCount = 0;
        $passedCount = 0;
        $total_records = 0;
        $htmlResult = '';
       
        $results = null;

        $file = $request->file('exelimport');

        $request->file('exelimport')->move(storage_path().'/public/import/' , 'import.xlsx');
       
        $inputfile = storage_path('/public/import/import.xlsx');


        /// FAKE: START
        return response()->json(array('success' => true, 'html'=>'<br>' . 
                ' Total : ' . $count . ' Record(s), '. 
                ' Passed: ' . $passedCount . ' Record(s), '. 
                ' Failed: ' . ($count - $passedCount) . ' Record(s), '));
        /// FAKE: END

        $retdate = Excel::load($inputfile, function($reader) use(&$count, 
                                                                 &$skipedCount, 
                                                                 &$passedCount, 
                                                                 &$total_records, 
                                                                 &$htmlResult) {
            $results = $reader->get(); 
            $ret = $results->toArray();
            $htmlResult = '';

            // prepare for progress bar 
            /////////////////////////////////////
            $progress_count = 0;
            $total_records = $results->count();
            Session::put('progress_count',$progress_count);
            Session::put('total_records', $total_records);
            
            foreach($ret as $index => $value) {
                $count++;
                $data = array();
                
                if (($value == null) || ($value["industrial"] == null) || (strlen($value["industrial"]) < 2)) {
                    $skipedCount++;
                    continue;
                }

                // update progress 
                $progress_count++;
                if($progress_count % 2 == 0)
                    Session::put('progress_count', $progress_count);
                $CATE_ID    = 0;
                $MARKET     = $value["market"]; 
                $INDUSTRIAL = $value["industrial"];
                $BU         = $value["bu"];
                
                $allquery = "SELECT COUNT(CATE_ID) AS total FROM TBL_P2_EQUITY_CATEGORY  WHERE INDUSTRIAL= '". $INDUSTRIAL . "'" . 
                            "       AND MARKET='". $MARKET . "'" .
                            "       AND BU='". $BU . "')";
                
                $all = DB::select(DB::raw($allquery));
                $total =  $all[0]->total;


                if ($total == 0) {
                    // creste new record 
                    array_push($data,array(
                        'MARKET' => $MARKET,
                        'INDUSTRIAL' => $INDUSTRIAL,
                        'BU' => $BU
                    ));

                    $affected = DB::table('TBL_P2_EQUITY_CATEGORY')->insert($data);
                    if($affected < 1) {
                         if(strlen($htmlResult) < 500) 
                         {
                            $htmlResult .= '<tr>';
                            $htmlResult .= '    <td>' . $CATE_ID    . '</td>';
                            $htmlResult .= '    <td>' . $MARKET     . '</td>';
                            $htmlResult .= '    <td>' . $INDUSTRIAL . '</td>';
                            $htmlResult .= '    <td>' . $BU         . '</td></tr>';
                         }
                    } else {
                        $passedCount++; 
                    }

                } else {

                    // update, if already exist 
                    $data = array('MARKET' => $MARKET,
                                  'INDUSTRIAL' => $INDUSTRIAL,
                                  'BU' => $BU);
                    $affected = DB::table('TBL_P2_EQUITY_CATEGORY')
                                    ->where('INDUSTRIAL', "=", $INDUSTRIAL)
                                    ->where('MARKET', "=", $MARKET)
                                    ->update($data);
                    
                    if($affected < 1) {
                         if(strlen($htmlResult) < 500) 
                         {
                            $htmlResult .= '<tr>';
                            $htmlResult .= '    <td>' . $CATE_ID    . '</td>';
                            $htmlResult .= '    <td>' . $MARKET     . '</td>';
                            $htmlResult .= '    <td>' . $INDUSTRIAL . '</td>';
                            $htmlResult .= '    <td>' . $BU         . '</td></tr>';
                         }
                    } else {
                        $passedCount++; 
                    }
                    // assumed that always success
                    //$passedCount++; 
                }  

            } // foreach

        });  // Excel::load

        $header = 'รายละเอียดข้อมูลที่ ไม่สำเร็จ <BR> '.
                  '<table style=\"width: 100px;\" class=\"table table-bordered\">' .
                  '<tr><td>รหัส หมวดหมู่หลักทรัพย์</td>' . 
                  '    <td>ตลาด</td>'. 
                  '    <td>กลุ่มอุตสาหกรรม</td>' . 
                  '    <td>หมวดธุรกิจ</td></tr>';

        $footer = '</table>  ';

        if(strlen($htmlResult) < 1) {
            $header  = '';
            $footer = '';  
            $htmlResult = ' ###### ';
        }

        // put 100%  
        Session::put('progress_count', $total_records);

        return response()->json(array('success' => true, 'html'=>'<br>' . 
                ' Total : ' . $count . ' Record(s), '. 
                ' Passed: ' . $passedCount . ' Record(s), '. 
                ' Failed: ' . ($count - $passedCount) . ' Record(s), '. 
                // ' progress_count:' . Session::get('progress_count') . 
                //  ' Skiped: ' . $skipedCount . ' Record(s)  ' .
                ' <BR>'. $header . $htmlResult . $footer));
        
    } // importdata
    */
   
    public function dowloadsampleBondCategory() {
        $file = 'contents/sample/p2_bond__category.xls';
        $headers = array(
            'Content-Type: application/pdf',
        );
        return \Response::download($file, 'p2_bond__category.xls', $headers);
    }
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    // >>>>
    // TAB#3 : Bond Index  
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////// 
    public function getindexBondIndex()
    {
        $data = getmemulist();
        $this->pageSetting([
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data, 60, 1) . ' | MEA'
        ]);

        Log::info('BondCompanyManagement::getindexBondIndex() => ' . getMenuName($data, 60, 1));
       
     
        return view('backend.pages.p2_bond_tab3_index_page');
    }


    public  function Ajax_Index_BondIndex(Request $request) 
    {
        Log::info('BondCompanyManagement::Ajax_Index_BondIndex' . $request);
 
        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;

        $Datacount = $this->getCountAllBondIndex();
        $Data = $this->getDataBondIndex($ArrParam);

        //if($Datacount)
        $totals = count($Datacount);
       // else
       //    $totals = 0;

        $htmlPaginate =Paginatre_gen($totals,$PageSize, 'page_click_search',$PageNumber);

        Log::info('view(backend.pages.ajax.ajax_p2_bond_tab3_index)');
        
 
        $returnHTML = view('backend.pages.ajax.ajax_p2_bond_tab3_index')->with([
            'htmlPaginate'=> $htmlPaginate,
            'data'=>$Data 

        ])->render();


        Log::info('BondCompanyManagement::Ajax_Index_BondIndex()'.$returnHTML );
        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }

    public  function Ajax_BondIndexSearchForm(Request $request) 
    {
         Log::info('BondCompanyManagement::Ajax_BondIndexSearchForm' . $request);
         $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')->orderby("MARKET")->get();  
         $securitieslist =  DB::table('TBL_P2_EQUITY_SECURITIES')->orderby("NAME_SHT")->get(); 

         $returnHTML = view('backend.pages.ajax.ajax_p2_bond_tab3_index_form')->with([
            
            'categorylist'   => $categorylist,
            'securitieslist' => $securitieslist

        ])->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML)); 
    }

/*
     public function Ajax_Index_Search(Request $request)
    {
        Log::info('BondCompanyManagement::Ajax_Index_Search : ' . $request);

        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $emp_id = $request->input('emp_id');
        $depart = $request->input('depart');
        $plan = $request->input('plan');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $check_name = $request->input('check_name');
        $check_depart = $request->input('check_depart');
        $check_plan = $request->input('check_plan');
        $check_date = $request->input('check_date');

        $ArrParam = array();
        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;
        $ArrParam["emp_id"] =$emp_id;
        $ArrParam["depart"] =$depart;
        $ArrParam["plan"] =$plan;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;

        $ArrParam["check_name"] =$check_name;
        $ArrParam["check_depart"] =$check_depart;
        $ArrParam["check_plan"] =$check_plan;
        $ArrParam["check_date"] =$check_date;


        $data = $this->DataSource($ArrParam,true);

        $totals =  $this->DataSourceCount($ArrParam,true);

        $htmlPaginate =Paginatre_gen($totals,$PageSize,'page_click_search',$PageNumber);


        $returnHTML = view('backend.pages.ajax.ajax_report1')->with([
            'htmlPaginate'=> $htmlPaginate,
            'data' => $data,
            'totals' => $totals,
            'PageSize' =>$PageSize,
            'PageNumber' =>$PageNumber

        ])->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML));

    }

*/
    public  function  getCountAllBondIndex() 
    {
        /* limit execution timeout */
        //ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        //ini_set('memory_limit', '-1');

        Log::info('BondCompanyManagement::getCountAllBondIndex() all records for TBL_P2_EQUITY_INDEX');

        return DB::table('TBL_P2_EQUITY_INDEX')->orderby("MARKET")->get();
        
    }

    public  function  getDataBondIndex($ArrParam) 
    {

        /* limit execution timeout */
        //ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        //ini_set('memory_limit', '-1');

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];

        $query =  "SELECT * FROM TBL_P2_EQUITY_INDEX ORDER BY MARKET DESC OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
        
        Log::info('BondCompanyManagement::getDataBondIndex()- SQLSTR=' .  $query ); 
        return DB::select(DB::raw($query));
    }

    public function getimportBondIndex() {
         $data = getmemulist();
        $this->pageSetting([
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data, 60, 1) . ' | MEA'
        ]);

        Log::info('BondCompanyManagement::getimportBondIndex() => ' . getMenuName($data, 60, 1));
         
        return view('backend.pages.p2_bond_tab3_import_index_page');
   

    }

    public function importBondIndex(Request $request) 
    {

        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        /* statistics */ 
        $count       = 0;
        $skipedCount = 0;
        $passedCount = 0;
        $total_records = 0;
        $htmlResult = '';
       
        $results = null;

        $file = $request->file('exelimport');

        $request->file('exelimport')->move(storage_path().'/public/import/' , 'import.xlsx');
       
        $inputfile = storage_path('/public/import/import.xlsx');


        /// FAKE: START
        return response()->json(array('success' => true, 'html'=>'<br>' . 
                ' Total : ' . $count . ' Record(s), '. 
                ' Passed: ' . $passedCount . ' Record(s), '. 
                ' Failed: ' . ($count - $passedCount) . ' Record(s), '));
        /// FAKE: END

        $retdate = Excel::load($inputfile, function($reader) use(&$count, 
                                                                 &$skipedCount, 
                                                                 &$passedCount, 
                                                                 &$total_records, 
                                                                 &$htmlResult) {
            $results = $reader->get(); 
            $ret = $results->toArray();
            $htmlResult = '';

            /* prepare for progress bar */
            /////////////////////////////////////
            $progress_count = 0;
            $total_records = $results->count();
            Session::put('progress_count',$progress_count);
            Session::put('total_records', $total_records);
            
            foreach($ret as $index => $value) {
                $count++;
                $data = array();
                
                if (($value == null) || ($value["industrial"] == null) || (strlen($value["industrial"]) < 2)) {
                    $skipedCount++;
                    continue;
                }

                /* update progress */
                $progress_count++;
                if($progress_count % 2 == 0)
                    Session::put('progress_count', $progress_count);
                $CATE_ID    = 0;
                $MARKET     = $value["market"]; 
                $INDUSTRIAL = $value["industrial"];
                $BU         = $value["bu"];
                
                $allquery = "SELECT COUNT(CATE_ID) AS total FROM TBL_P2_EQUITY_INDEX  WHERE INDUSTRIAL= '". $INDUSTRIAL . "'" . 
                            "       AND MARKET='". $MARKET . "'" .
                            "       AND BU='". $BU . "')";
                
                $all = DB::select(DB::raw($allquery));
                $total =  $all[0]->total;


                if ($total == 0) {
                    /* creste new record */
                    array_push($data,array(
                        'MARKET' => $MARKET,
                        'INDUSTRIAL' => $INDUSTRIAL,
                        'BU' => $BU
                    ));

                    $affected = DB::table('TBL_P2_EQUITY_INDEX')->insert($data);
                    if($affected < 1) {
                         if(strlen($htmlResult) < 500) 
                         {
                            $htmlResult .= '<tr>';
                            $htmlResult .= '    <td>' . $CATE_ID    . '</td>';
                            $htmlResult .= '    <td>' . $MARKET     . '</td>';
                            $htmlResult .= '    <td>' . $INDUSTRIAL . '</td>';
                            $htmlResult .= '    <td>' . $BU         . '</td></tr>';
                         }
                    } else {
                        $passedCount++; 
                    }

                } else {

                    /* update, if already exist */
                    $data = array('MARKET' => $MARKET,
                                  'INDUSTRIAL' => $INDUSTRIAL,
                                  'BU' => $BU);
                    $affected = DB::table('TBL_P2_EQUITY_INDEX')
                                    ->where('INDUSTRIAL', "=", $INDUSTRIAL)
                                    ->where('MARKET', "=", $MARKET)
                                    ->update($data);
                    
                    if($affected < 1) {
                         if(strlen($htmlResult) < 500) 
                         {
                            $htmlResult .= '<tr>';
                            $htmlResult .= '    <td>' . $CATE_ID    . '</td>';
                            $htmlResult .= '    <td>' . $MARKET     . '</td>';
                            $htmlResult .= '    <td>' . $INDUSTRIAL . '</td>';
                            $htmlResult .= '    <td>' . $BU         . '</td></tr>';
                         }
                    } else {
                        $passedCount++; 
                    }
                    // assumed that always success
                    //$passedCount++; 
                }  

            } // foreach

        });  // Excel::load

        $header = 'รายละเอียดข้อมูลที่ ไม่สำเร็จ <BR> '.
                  '<table style=\"width: 100px;\" class=\"table table-bordered\">' .
                  '<tr><td>รหัส หมวดหมู่หลักทรัพย์</td>' . 
                  '    <td>ตลาด</td>'. 
                  '    <td>กลุ่มอุตสาหกรรม</td>' . 
                  '    <td>หมวดธุรกิจ</td></tr>';

        $footer = '</table>  ';

        if(strlen($htmlResult) < 1) {
            $header  = '';
            $footer = '';  
            $htmlResult = ' ###### ';
        }

        /* put 100% */ 
        Session::put('progress_count', $total_records);

        return response()->json(array('success' => true, 'html'=>'<br>' . 
                ' Total : ' . $count . ' Record(s), '. 
                ' Passed: ' . $passedCount . ' Record(s), '. 
                ' Failed: ' . ($count - $passedCount) . ' Record(s), '. 
                // ' progress_count:' . Session::get('progress_count') . 
                //  ' Skiped: ' . $skipedCount . ' Record(s)  ' .
                ' <BR>'. $header . $htmlResult . $footer));
        
    } // importdata



    /**
     * Handle request delete category single/multiple records.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function deleteBondIndex(Request $request)
    {

        Log::info('BondCompanyManagement::deleteBondIndex()' . $request); 

        $deleted = false;
        $arrId = explode(',', $request->input('group_id'));

        foreach($arrId as $index => $item) {

            if($item != "") {
                $deleted =  DB::table('TBL_P2_EQUITY_INDEX')->where('INDEX_ID',"=", $item)->delete();
            }
        }

        if($deleted)  {
            return response()->json(["ret" => "1"]);
        } else {
            return response()->json(["ret" => "0"]);
        }
    }


    /**
     * Get view bond securities 
     * 
     * @param  None
     * @return view\backend\pages
     */
    public function getAddBondIndex()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data,60, 1) . ' | MEA'
        ] );


        return view('backend.pages.p2_bond_tab3_add_index_page');
    }

    /**
     * Get view bonf category with data by matched with $id
     * 
     * @param  Bond company code  $id
     * @return view backend.pages.p2_edit_bond_company_page
     */
    public function getEditBondIndex($id)
    {
        $data = getmemulist();
        $this->pageSetting([
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data, 60, 1) . ' | MEA'
        ]);

        $editdata = DB::table('TBL_P2_EQUITY_INDEX')->where("INDEX_ID" ,"=",  $id)->first();
       
        if($editdata) {

            return view('backend.pages.p2_bond_tab3_edit_index_page')->with(['editdata'=>$editdata]);
        }
        
        // any error occurred redirect to page 404
        abort(404);
    }

    public function T2_dowloadsampleEquity() {
        $file = 'contents/sample/t2_sample_equity.xlsx';
        $headers = array(
            'Content-Type: application/pdf',
        );
        return \Response::download($file, 't2_sample_equity.xlsx', $headers);
    }

    public function T2_dowloadsampleBond() {
        $file = 'contents/sample/t2_sample_bond.xlsx';
        $headers = array(
            'Content-Type: application/pdf',
        );
        return \Response::download($file, 't2_sample_bond.xlsx', $headers);
    }

    

    //////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * TAB#4 
     */

    public function getindexBroker()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data, 60, 1) . ' | MEA'
        ] );

        return view('backend.pages.p2_bond_tab4_broker_page');
    }

    public  function Ajax_Index_Broker(Request $request){

        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;

        $Datacount = $this->getCountAllBroker();
        $Data = $this->getDataBroker($ArrParam);

        $totals = count($Datacount);

        $htmlPaginate =Paginatre_gen($totals,$PageSize,'page_click_search',$PageNumber);

        $returnHTML = view('backend.pages.ajax.ajax_p2_bond_tab4_broker')->with([
            'htmlPaginate'=> $htmlPaginate,
            'data'=>$Data

        ])->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }


    public  function  getCountAllBroker(){
        
        Log::info('BondCompanyManagement::getCountAllBroker()');
        return DB::table('TBL_P2_EQUITY_BROKER')->orderby("NAME_SHT")->get();
    }

    public  function  getDataBroker($ArrParam){

        // limit execution timeout 
        ini_set('max_execution_time', 30000);
        // unlimit memory size 
        ini_set('memory_limit', '-1');

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];

        $query =  "SELECT * FROM TBL_P2_EQUITY_BROKER ORDER BY NAME_SHT OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
        return DB::select(DB::raw($query));
    }

    
    public function deleteBroker(Request $request)
    {
        $deleted = false;
        $arrId = explode(',',$request->input('group_id'));

        foreach($arrId as $index => $item){

            if($item != "") {
                $deleted =  DB::table('TBL_P2_EQUITY_BROKER')->where('NAME_SHT',"=", $item)->delete();
            }
        }

        if($deleted)  {
            return response()->json(["ret" => "1"]);
        } else {
            return response()->json(["ret" => "0"]);
        }
    }


   
    public function getAddBroker()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data,60, 1) . ' | MEA'
        ] );


        return view('backend.pages.p2_tab4_add_bond_broker_page');
    }

    
    public function getEditBroker($id)
    {
        $data = getmemulist();
        $this->pageSetting([
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data, 60, 1) . ' | MEA'
        ]);


        $editdata = DB::table('TBL_P2_EQUITY_BROKER')->where("NAME_SHT" ,"=",  $id)->first();
        // $editdata = DB::table('TBL_P2_EQUITY_SECURITIES')->where("NAME_SHT" ,"=",  $id)->get()[0];
     
        //if(isset($id)) abort(404);
        if($editdata) {

            return view('backend.pages.p2_bond_tab4_edit_broker_page')->with(['editdata'=>$editdata]);
        }
        
        abort(404);
    }


   
    public function postAddBroker(Request $request)
    {
        // limit execution timeout 
        ini_set('max_execution_time', 30000);
        // unlimit memory size 
        ini_set('memory_limit', '-1');
        

        Log::info('BondCompanyManagementController::postAddBroker::=>' . $request);
        Log::info(' postAdd: equity_start=>' . toEnglishDate($request["equity_start"])); 

        $ret = false;
        $datestart  = new Date(toEnglishDate($request["equity_start"]));
        $datereq = $request["equity_end"];
         
        
        if ($request["company_addr"] == '') {

        }

        
        if ($request["equity_start"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันเริ่มต้น ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        if ($request["equity_end"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันสิ้นสุด ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }
        
        // Log::info(' postAdd: equity_start=>' . toEnglishDate($datereq));   
        $dateEnd = new Date(toEnglishDate($datereq));

        $today   = new Date();
        $data    = array();

        array_push($data,array(
            'NAME_SHT' => $request["name_sht"],
            //Securities_Name
            'BROKER_NAME' =>$request["company_name"],
            'ADDRESS' => $request["company_addr"],

            'START_DATE' => $datestart,
            'END_DATE' => $dateEnd,

            'PHONE' => $request["company_phone"],
            'FAX' => $request["company_fax"],
            'CREATE_DATE' => $today,
            'CREATE_BY' => "Admin"
        ));


        $chk = "SELECT COUNT(NAME_SHT) As total FROM TBL_P2_EQUITY_BROKER WHERE NAME_SHT = '". $request["name_sht"]. "'";
        $all = DB::select(DB::raw($chk));
        $total =  $all[0]->total;

        $rethtml = "";


        if ($total > 0) {
           $rethtml = "Broker code name ที่ท่านใส่มีอยู่ในระบบแล้ว";

        } else {
           $insert = DB::table('TBL_P2_EQUITY_BROKER')->insert($data);
           $ret = $insert;
        }
        return response()->json(array('success' => $ret, 'html'=>$rethtml));
    }


    
    public function postEditBroker(Request $request)
    {
        $ret = false;
        $datestart  = new Date(toEnglishDate($request["equity_start"]));
        $datereq = $request["equity_end"];

        if ($request["equity_end"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันสิ้นสุด ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }
        if ($request["equity_start"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันเริ่มต้น ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        Log::info(' postEdit: request =>' . $request);   

        $dateEnd = new Date(toEnglishDate($datereq));
        $today = new Date();

        $data = array(

            'BROKER_NAME' =>$request["company_name"],
            'ADDRESS' => $request["company_addr"],

            'START_DATE' => $datestart,
            'END_DATE' => $dateEnd,

            'PHONE' => $request["company_phone"],
            'FAX' => $request["company_fax"],

            'CREATE_BY' => "Admin"
        );

        $update = DB::table('TBL_P2_EQUITY_BROKER')->where('NAME_SHT', "=", $request["name_sht"])->update($data);

        $ret = $update;

        return response()->json(array('success' => $ret, 'html'=>'OK'));

    }

   

    

    /**
     * MENU 1. การ Import ซื้อขายหลักทรัพย์ (Purchase && Sale) 
     *  
     */
    public  function  getEquitySecurities(){
        // Log::info(get_class($this) .'::'. __FUNCTION__);
        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $query =  "SELECT * FROM TBL_P2_EQUITY_SECURITIES ORDER BY NAME_SHT";
        return DB::select(DB::raw($query));
    }

    /**
     * Download sample xls (Purchase & Sale)  
     */
    public function T2_downloadSamplePurchase() {
         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $file = 'contents/sample/UOBAM - Transaction Listing From 01-May-2016 To 31-May-2016.xls';
        $newfile =  'UOBAM - Transaction Listing From 01-May-2016 To 31-May-2016.xls';
       //    $file = 'contents/sample/t2_sample_bond.xlsx';
        $headers = array(
            'Content-Type: application/pdf',
        );
        return \Response::download($file, $newfile, $headers);
    }

    function checkDatetime($x) {
       return (date('Y-m-d', strtotime($x)) == $x);
    }
    /**
     *  Check uploaded file format
     *  @param  request $request
     *  @return json with status, details
     */
    public function T2_checkPurchaseAndSaleFile(Request $request) {
        /* limit execution timeout */
        ini_set('max_execution_time', 50000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        Log::info(get_class($this) .'::'. __FUNCTION__. 
                  ' datatype: ' . $request->input('datatype') .  " ," .
                  ' broker' .  $request->input('broker'));
        
        $results = null;
        $error_msg = '';
        $htmlResult = '';

        $passedCount = 1;
        $count = 0;
        $passedCount = 1;

        $datatype = $request->input('datatype'); // $request->get('datatype');
        $broker = $request->input('broker'); 
        $nameSHT = $broker;
        $file = $request->file('exelimport');

        $clientOriginalFileName = $request->file('exelimport')->getClientOriginalName();

        $process_file = '60_2_check_purchase_import.xls';
        $request->file('exelimport')->move(storage_path() . '/public/import/', $process_file);
       
        $inputfile = storage_path('/public/import/' . $process_file);

        Log::info('clientOriginalFileName:' . $clientOriginalFileName);
        
        $status = true;
        ///////////////////////////////////
        $firstrow = '';
        $filedate = '';
      
        $arr = array();  
        $checkOnly  = true;
        Log::info('T2_checkPurchaseAndSaleFile:: datatype = ' . $datatype);

        /// Is user selected UOBAM ?
        if(($broker == 'brokerUOBAM') || ($broker == 'UOBAM')) {
            $arr = $this->importUOBAMPurchaseAndSale($inputfile, $clientOriginalFileName, $checkOnly, $nameSHT);
            $status = $arr['status'];
            $filedate = $arr['filedate'];
            $error_msg = $arr['error_msg'];  
        } else if(($broker == 'brokerKTAM') || ($broker == 'KTAM')) {
            $arr = $this->importKTAMPurchaseAndSale($inputfile, $clientOriginalFileName, $checkOnly, $nameSHT);
            $status = $arr['status'];
            $filedate = $arr['filedate']; 
            $error_msg = $arr['error_msg']; 
        } else {
        	Log::info('Unexpected Error!');
        }

        Log::info(get_class($this) .'::'. __FUNCTION__. ($status ? "ข้อมูลถูกต้อง กดปุ่ม นำเข้าข้อมูล เพื่อนำข้อมูลเข้าระบบ" : "มีข้อผิดพลาด") ); 
        

        //////////////////////////////////
        return response()->json(array('success' =>  $status, 'html'=>'' . 
                '<br/> file :' . $clientOriginalFileName .
                '<br/> status : '  . ($status ? "ข้อมูลถูกต้อง กดปุ่ม นำเข้าข้อมูล เพื่อนำข้อมูลเข้าระบบ" : "มีข้อผิดพลาด") . 
                '<br/> date : '    . $filedate . 
                ($status ? '' : '<br/> error : ' . $error_msg . " <br/>") ));
        
    } // T2_checkPurchaseAndSaleFile

    /**
     *  Import purchase and salse file
     *  @param  request $request
     *  @return json with status, details
     */
    public function T2_importPurchaseAndSaleFile(Request $request) {
        /* limit execution timeout */
        ini_set('max_execution_time', 50000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        Log::info(get_class($this) .'::'. __FUNCTION__. 
                  ' datatype: ' . $request->input('datatype') .  " ," .
                  ' broker' .  $request->input('broker'));
        
        $results = null;
        $error_msg = '';
        $htmlResult = '';

        $passedCount = 1;
        $count = 0;
        $passedCount = 1;

        $datatype = $request->input('datatype'); // $request->get('datatype');
        $broker = $request->input('broker'); 
        $nameSHT = $broker;
        $file = $request->file('exelimport');

        $clientOriginalFileName = $request->file('exelimport')->getClientOriginalName();

        $process_file = '60_2_check_purchase_import.xls';
        $request->file('exelimport')->move(storage_path() . '/public/import/', $process_file);
       
        $inputfile = storage_path('/public/import/' . $process_file);

        
        
        $status = true;
        
        ///////////////////////////////////////////////////////////////////////////////////////////
        $firstrow = '';
        $filedate = '';      
        $arr = array();  
        Log::info('T2_importPurchaseAndSaleFile:: datatype = ' . $datatype . ', broker=' . $broker );

        /// Is user selected UOBAM ?
        if(($broker == 'brokerUOBAM') || ($broker == 'UOBAM')) {
            $arr = $this->importUOBAMPurchaseAndSale($inputfile, $clientOriginalFileName, false, $nameSHT);
 
            $status = $arr['status'];
            $filedate = $arr['filedate'];
            $error_msg = $arr['error_msg']; 
        } else if(($broker == 'brokerKTAM') || ($broker == 'KTAM')) {
            $arr = $this->importKTAMPurchaseAndSale($inputfile, $clientOriginalFileName, false, $nameSHT);
            $status = $arr['status'];
            $filedate = $arr['filedate']; 
            $error_msg = $arr['error_msg'];
        } else {
        	Log::info('Unexpected Error!');
        }
        
        Log::info(get_class($this) .'::'. __FUNCTION__. ($status ? "นำเข้าข้อมูลเรียบร้อย" : "มีข้อผิดพลาด") ); 
          
        //////////////////////////////////
        return response()->json(array('success' =>  $status, 'html'=>'' . 
                '<br/> status : '     . ($status ? "นำเข้าข้อมูลเรียบร้อย" : "มีข้อผิดพลาด! " . $error_msg) . 
                '<br/> date   : '       . $filedate . ($status ? '' : 
                '<br/> error  : '  . $error_msg . " <br/>" )));
        
    } // T2_checkPurchaseAndSaleFile
    /**
     * MENU.1 
     * เป็นเมนูสําหรับนําเข้าข้อมูล ซื้อ - ขายหลักทรัพย์ (รูปแบบไฟล์นามสกลุ .xls) 
     * see. "KTAM_Transaction 16012017.xls"
     */
    function  importKTAMPurchaseAndSale($inputfile,  $clientOriginalFileName, $checkFileOnly, $nameSHT) {
        $results = null;
        $error_msg = '';
        $htmlResult = '';
        $filedate = date('Y-m-d');
        $filedatethai = date('Y-m-d');

        $headers = array();  
        $rootHeader = array();
        $childHeader = array();
        $firstColIndex = 0;
        $header_row = -1;

        $status = false;



        Log::info('importKTAMPurchaseAndSale');
        $retdate = Excel::load($inputfile, function($reader) use( &$clientOriginalFileName,
                                                                  &$rootHeader,
                                                                  &$childHeader,
                                                                  &$firstColIndex,
                                                                  &$header_row,
                                                                  &$nameSHT,
                                                                  &$checkFileOnly,
                                                                  &$status,
                                                                  &$count, 
                                                                  &$error_msg,
                                                                  &$filedate ,
                                                                  &$filedatethai,
                                                                  &$htmlResult) {
            // first row not a header
            $reader->noHeading();
            // $reader->ignoreEmpty();
            // $reader->setSeparator('+');
            // $reader->formatDates(true, 'Y/');
            $reader->formatDates(true, 'Y-m-d');

            //$reader->formatDates(false);

            $results = $reader->get(); 
            $ret = $results->toArray();


            // signature column header1
            $TAG_HD_SECURITY        = "security";
            $TAG_HD_SECURITIES      = "securities";
            $TAG_HD_TRADE_DATE      = "trade"; // trade date
            $TAG_HD_SETTLEMENT_DATE = "settlement";  //  Settlement date
            $TAG_HD_NO_OF           = "no.of";       // no of Shares

            $TAG_HD_UNIT_PRICE      = "unit"; // unit price 
            $TAG_HD_GROSS           = "gross"; // gross amount

            $TAG_HD_COMMISSION_VAT  = "commission"; // commission VAT
            $TAG_HD_WITHHOLDING     = "withholding";
            $TAG_HD_NET_AMOUNT      = "net amount";
                           
            $TAG_HD_TOTAL_COST      = "total cost";
            $TAG_HD_GAIN_LOSS       = "gain(loss)";
            $TAG_HD_BROKER          = "broker";
             
            // Purchase TAG 
            $TAG_HD_PURCHASE        = "share purchase"; 

            // Sale TAG
            $TAG_HD_SALE            = "share sale";

            // dividend   
            $TAG_HD_DIV             = "share dividend";

            // dividend   
            $TAG_HD_DIV_ENT         = "dividend entitlement";
             
            $TAG_HD_BONUS_ISSUE     = "bonus issue";  
                
            $current_tag        = "";       
            $state = -1;
            $state_keys = array(0=>'PERCHASE', 1=>'SALE',  2=>'DIVIDEND', 3=>'BONUS ISSUE');
            $prev_key = '';
             
            $count = 0;
            $status = false;
            /*
            $rootHeader = array();

            $childHeader = array();
            $firstColIndex = 0;
            $header_row = -1;
            */
            // loop rows

            /**
             * column index for each subheader 
             */
            $brokerage_index = 4;
            $sale_hd_index = 7; // 5
            $EXPECTED_COLUMNS = 16;


            $colindex = 0; 
            $rowindex = 0;
            $firstRowIndex = 0;

            $error_msg = "ไม่พบ column [" . $TAG_HD_DIV_ENT . "] หรือ header ของ excel ไม่ถูก";

            // Loop throught rows
            foreach($ret as $r => $rows) {
                $colindex = 0;
                // loop throught column
                foreach($rows as $cols => $vm) {
                    if (is_array($vm)) { 
                	        Log::info('INVALID FILE FORMAT ==> ' . print_r($vm, true));
                	    	continue;
                	} 
                    $value = $vm;
                    $tag = trim(strtolower(($value == NULL) ? '' : $value));
                    switch($tag) {
                    	/* checking for header */
                    	case $TAG_HD_SECURITIES:
                    	case $TAG_HD_SECURITY:
                    	    $hd = array($tag => $tag,                 
                                       'COL' => $colindex,
                                       'ROW' => $rowindex);  
                            array_push($rootHeader, $hd);       // [0]
                            $firstColIndex = $colindex;
                            // Log::info('checking for header' . $tag . ' :: ' . print_r($hd, true));
                    	    break;

                    	case $TAG_HD_TRADE_DATE:      // trade date [1]
                    	    //break;
                        case $TAG_HD_SETTLEMENT_DATE: // Settlement date [2]
                            //break;
                        case $TAG_HD_NO_OF:           // no of Shares [3]
                            //break;
                        case $TAG_HD_UNIT_PRICE:      // unit price  [4]
                            //break; 
                        case $TAG_HD_GROSS:           // gross amount [5] -- >NOT USE
                            //break;
                        case $TAG_HD_COMMISSION_VAT:  // commission VAT [6]
                            //break;
                        case $TAG_HD_WITHHOLDING:     // [7]
                            //break; 
                        case $TAG_HD_NET_AMOUNT:      // [8]
                            //break;
                        case $TAG_HD_TOTAL_COST:      // [9]
                            //break; 
                        case $TAG_HD_GAIN_LOSS:       // [10]
                            //break;
                        case $TAG_HD_BROKER:          // [11]
                            $header_row = $rowindex;  
                            $firstRowIndex = $rowindex;
                            $hd = array($tag => $tag,                 
                                       'COL' => $colindex,
                                       'ROW' => $rowindex);  
                            array_push($rootHeader, $hd);       // 0
                            //Log::info('CHECK HEADER :: ' . print_r($hd, true));
                            
                            break;
                        /* end check header */
                        
                        /* check sections */
                        case $TAG_HD_BONUS_ISSUE:
                        case $TAG_HD_PURCHASE: 
                        case $TAG_HD_SALE:   
                        case $TAG_HD_DIV:
                        case $TAG_HD_DIV_ENT:
                            /*$hd = array($tag => $tag,
                                            'COL' => $colindex,
                                            'ROW' => $rowindex); 
                            array_push($rootHeader, $hd);  */
                            $status = true; 
                            break;   

                        default:
                            break;       

                        /* end check sections */    
                    } // switch
                    $colindex++;
   
                } // foreach column
                $rowindex++;
                // assume header row less than 26
                if($rowindex >  25) {
                    break;  
                }
            } // foreach rows
            
            if(!$checkFileOnly) {
                $ar = $this->saveDB_KTAM_PurchaseAndSale(
                	                $firstRowIndex, 
                	                $checkFileOnly, 
                                    $ret, $rootHeader, 
                                    $childHeader, $header_row, 
                                    $firstColIndex, $nameSHT, $clientOriginalFileName);
                $status = $ar['status'];

            } else {

            }
        });

        if($status) {
            $error_msg = '';
        }
        return array('status' =>$status, 'filedate' => $filedatethai, 'error_msg' => $error_msg);
    } 

     function saveDB_KTAM_PurchaseAndSale(
     	     $firstRowIndex, 
     	     $checkFileOnly, 
             $rows, 
             $rootHeader, 
             $childHeader, 
             $header_row,
             $firstColIndex, 
             $nameSHT, 
             $clientOriginalFileName) {
        $errorMessae = '';
        // Retrive logged ID
        $user_data = Session::get('user_data');
        /* PURCHASE */
        $JOB_ID          = '';   //  0 (Auto field) 
        $TRANS_DATE      = '';   //  1
        $SETTLE_DATE     = '';   //  2
        $SYMBOL          = '';   //  3
        $TYPE            = '';   //  4
        $SECURITIES_NAME = '';   //  5
        $BROKER_NAME     = 0.0;  //  6  
        $UNIT_PURCHASE   = 0.0;  //  7
        $PRICE_PURCHASE  = 0.0;  //  8 
        $TOTAL_PURCHASE  = 0.0;  //  9
        $BROKERAGE       = 0;    // 10
        $VAT             = 0.0;  // 11
        $WIHOUT_VAT      = 0.0;  // 12
        /* SALES */
        $UNIT_SALE       = 0.0;  // 13
        $PRICE_SALE      = 0.0;  // 14
        $TOTAL_SALE      = 0.0;  // 15
        $UNIT_COST       = 0.0;  // 16
        $TOTAL_COST      = 0.0;  // 17
        $PROFIT_LOSS     = 0.0;  // 18

        $UNIT_DIV        = 0.0;  // 19
        $PRICE_DIV       = 0.0;  // 20 
        $TOTAL_DIV       = 0.0;  // 21

        $UNIT_BONUS       = 0.0;  // 21
        $PRICE_BONUS      = 0.0;  // 22 
        $TOTAL_BONUS      = 0.0;  // 23

        $REMAIN_UNIT     = 0.0;  // 22
        $PRICE_PER_UNIT  = 0.0;  // 23
        $TOTAL_AMOUNT    = 0.0;  // 24
        $STATUS          = 0;    // 25
        $CREATE_DATE = date("Y-m-d H:i:s");  // 24
        $CREATE_BY = $user_data->emp_id;     // 25
        $REFERENCE_DATE  = '';               // 26

        $TODAY_DATE = date("Y-m-d");  

        // signature tag
        
        // Purchase TAG 
        $TAG_HD_PURCHASE        = "share purchase"; 

        // Sale TAG 
        $TAG_HD_SALE            = "share sale";

        // dividend   
        $TAG_HD_DIV             = "share dividend";

        $TAG_HD_DIV_ENT         = "dividend entitlement";

        $TAG_HD_BONUS_ISSUE     = "bonus issue";

        $TAG_SUB_DIV_ENT        = "sub total dividend entitlement";   


        $TAG_SUB_BONUS_ISSUE    = "sub total bonus issue";
        // equity listed
        $TAG_SUB_TOTAL_PURCHASE = "sub total share purchase";
            
        $TAG_SUB_TOTAL_SALE     = "sub total share sale";
        $TAG_BUY_SELL           = "total (buy-sell) equities";
        $TAG_HD_EQUITIES        = "equities";
        $TAG_EMPTY              = "";                                               
            
        $current_tag            = "";    
        
        $state = -1;
        $TYPE  = '';   //  4
        $state_keys = array(0=>'PERCHASE', 1=>'SALE', 2=>'DIVIDEND', 3=>'BONUS ISSUE');
        $prev_key = '';
        $rowindex = 0;
        $status = false;
        $section_changed = false;
        $section = "";
        $skip_row = false;

        // Retrive logged ID
        Log::info('SESSION: [user_data] =' . $CREATE_BY);

        Log::info('ENTER: saveDB_KTAM_PurchaseAndSale');
        $num = 0;
        foreach($rows as $r => $row) {
            $num++;
            $data = array();
            // RESET VAR
            //
            // Log::info(print_r($row, true));
            // continue;    
            //////////////////////////////////
            $JOB_ID          = date('YmdHis') .'-' .uniqid() ;   //  0 (Auto field) 
            $TRANS_DATE      = '';   //  1
            $SETTLE_DATE     = '';   //  2
            $SYMBOL          = '';   //  3
            
            $SECURITIES_NAME = '';   //  5
            $BROKER_NAME     = '';   //  6  
            $UNIT_PURCHASE   = 0.0;  //  7
            $PRICE_PURCHASE  = 0.0;  //  8 
            $TOTAL_PURCHASE  = 0.0;  //  9
            $BROKERAGE       = 0;    //  10
            $VAT             = 0.0;  // 11
            $WIHOUT_VAT      = 0.0;  // 12

            $UNIT_SALE       = 0.0;  //  12
            $PRICE_SALE      = 0.0;  // 13
            $TOTAL_SALE      = 0.0;  // 14
            $UNIT_COST       = 0.0;  // 15
            $TOTAL_COST      = 0.0;  // 16
            $PROFIT_LOSS     = 0.0;  // 17
            $UNIT_DIV        = 0.0;  // 18
            $PRICE_DIV       = 0.0;  // 19 
            $TOTAL_DIV       = 0.0;  // 20

            $UNIT_BONUS       = 0.0;  // 21
            $PRICE_BONUS      = 0.0;  // 22 
            $TOTAL_BONUS      = 0.0;  // 23

            $REMAIN_UNIT     = 0.0;  // 24
            $PRICE_PER_UNIT  = 0.0;  // 25
            $TOTAL_AMOUNT    = 0.0;  // 26

            //$STATUS          = 0;    // 24
            $CREATE_DATE     = date("Y-m-d H:i:s");       // 27
            $CREATE_BY       = $user_data->emp_id;        // 28
            $REFERENCE_DATE  = $clientOriginalFileName;   // 29

            $colindex = 0;

           /* if($rowindex <= ($firstRowIndex)) {
                $rowindex++;
                continue;
            }*/ 

            /*if($section_changed) {
            	$section_changed = false;
                continue; 
            }*/
            
            // loop each column
            
           // 
            $empty = "";
            foreach($row as $cols => $value) {
                if($section_changed) {
                	Log::info('>>>> section_changed:' . ($section_changed ? "TRUE" : "FALSE"));
                	$section_changed = false;
                	$skip_row = true;
                	break;
                }

                if($skip_row) {
                    $skip_row = false;
                	break;
                }
                
                $tag = trim(strtolower(($value == NULL) ? '' : $value));
                 
                //Log::info("section[" . $section . "] = " . $tag); 
                switch (trim($tag)) {
                    case $TAG_HD_BONUS_ISSUE:
                    case $TAG_HD_PURCHASE: 
                    case $TAG_HD_SALE: 
                    case $TAG_HD_DIV:
                    case $TAG_HD_DIV_ENT:
                        $section = trim($tag);
                        $section_changed = true; 
                        //Log::info("ROW[" . $rowindex . "] = " . print_r($row, true));
                        break;

                    // equity listed
                    case $TAG_HD_EQUITIES:    
                    case $TAG_SUB_TOTAL_PURCHASE:
                    case $TAG_SUB_TOTAL_SALE:
                    case $TAG_SUB_DIV_ENT:
                    case $TAG_BUY_SELL:
                    case $TAG_SUB_BONUS_ISSUE:
                        // if($colindex == $rootHeader[0]['COL'])
                        $skip_row = true;

                        break;

                    case $TAG_EMPTY:    
                        if($colindex == $rootHeader[0]['COL'])
                            $skip_row = true;
                        break;    
                     
                    default:
                        // begin extract data for each section
                        // Log::info("section[" . $section . "] = " . $tag);
                        switch($section) {
                             
                        	case $TAG_HD_PURCHASE: 
                        	case $TAG_HD_SALE:
                        	case $TAG_HD_DIV:
                            case $TAG_HD_DIV_ENT:
                            case $TAG_HD_BONUS_ISSUE: 

                        	    if(($tag == $TAG_SUB_TOTAL_PURCHASE) ||
                                   ($tag == $TAG_SUB_TOTAL_SALE) ||
                                   ($tag == $TAG_SUB_DIV_ENT) ||
                                   ($tag == $TAG_SUB_BONUS_ISSUE) ||
                                   ($tag == $TAG_BUY_SELL)){
                                   	// Ignore sub_total_purchase, sub_total_sale, buy_sell
                        	        $section = "";
                        	    } else {
	                                switch($colindex) {
                                        case $rootHeader[0]['COL']:     // trade date (YYYY-MM-DD)
	                                	    $SYMBOL = trim($value);    //  $SECURITIES_NAME as Symbol
	                                	    Log::info("FOUND SYMBOL = " . trim($value));
	                                	    break;

	                                    case $rootHeader[1]['COL']:     // trade date (YYYY-MM-DD)
	                                        Log::info("ROW[" . $rowindex. " ] = " . $tag);
	                                        
	                                        $valid = preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", trim($tag));
	                                        if(!$valid) { 
	                                        	Log::info("ERROR: [".  trim($tag) . "] Invalid TradeDate format!!! =>" . print_r($row, true));
	                                        	$skip_row = true;
	                                        	return array('status' => false);
	                                        }

	                                        $TRANS_DATE = trim($tag);  // 
	                                        Log::info('TRANS_DATE:' . $TRANS_DATE);
	                                        	
	                                       
	                                        break;

	                                    case $rootHeader[2]['COL']:     // trade date (YYYY-MM-DD)
	                                        $valid = preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", trim($tag));
	                                        if(!$valid) { 
	                                        	Log::info("ERROR: [".  trim($tag) . "] Invalid Settlement date format!!! =>" . print_r($row, true));
	                                        	$skip_row = true;
	                                        	return array('status' => false);
	                                        }
	                                        $SETTLE_DATE = trim($tag);  // 
	                                        Log::info('SETTLE_DATE:' . $SETTLE_DATE);
	                                        break;

	                                    case $rootHeader[3]['COL']:   // UNITS_PURCHASE (no.of shared)
									        switch($section) {
								            	case $TAG_HD_PURCHASE: 
								            	    $UNIT_PURCHASE = (double)$tag;  // A
								            	    break;

								                case $TAG_HD_SALE: 
								                    $UNIT_SALE = (double)$tag; // F
								            	    break;

								                case $TAG_HD_DIV:
								                    $UNIT_DIV = (double)$tag;
								            	    break;

                                                case $TAG_HD_DIV_ENT:
                                                    $UNIT_DIV = (double)$tag;
                                                    break;   

                                                case $TAG_HD_BONUS_ISSUE:
                                                    $UNIT_PURCHASE = (double)$tag;  // NEW
                                                    $UNIT_BONUS = (double)$tag;
                                                    break;    
								            	default:
								            	    break;    
								            }
	                                        //$UNIT_PURCHASE = (double)$tag;  // A
	                                        //$UNIT_SALE = (double)$tag; // F

	                                        
	                                        $status = is_numeric($tag);
	                                        if(!$status) { 
	                                        	Log::info("ERROR: invalid UNIT_PURCHASE/UNIT_SALE format!!! =>" . print_r($row, true));
	                                        	$skip_row = true;
	                                        	return array('status' => false);
	                                        }
	                                        break;   
	                                    case $rootHeader[4]['COL']:   // PRICE_PURCHASE
	                                        //$PRICE_PURCHASE = (double)$tag;   // B
	                                        //$PRICE_SALE = (double)$tag;   // G
	                                        switch($section) {
								            	case $TAG_HD_PURCHASE: 
								            	    $PRICE_PURCHASE = (double)$tag;  // B
								            	    break;

								                case $TAG_HD_SALE: 
								                    $PRICE_SALE = (double)$tag; // G
								            	    break;

								                case $TAG_HD_DIV:
								                    $PRICE_DIV = (double)$tag;
								            	    break;

                                                case $TAG_HD_DIV_ENT: 
                                                    $PRICE_DIV = (double)$tag;
                                                    break;   

                                                 case $TAG_HD_BONUS_ISSUE:
								            	    $PRICE_PURCHASE = (double)$tag;  // NEW
                                                    $PRICE_BONUS = (double)$tag;
                                                    break; 
								            	default:
								            	    break;    
								            }

	                                        $status = is_numeric($tag);
	                                        if(!$status) { 
	                                        	Log::info("ERROR: invalid PRICE_PURCHASE format!!! =>" . print_r($row, true));
	                                        	$skip_row = true;
	                                        	return array('status' => false);
	                                        }
	                                        break;

	                                    case $rootHeader[5]['COL']:   // Gross amount 
	                                        Log::info("IGNORED: Gross Amount: " . trim($tag));
	                                        break;

	                                    case $rootHeader[6]['COL']:   // BROKERAGE
	                                        $BROKERAGE = (double)$tag;          // C
	                                        $TOTAL_BROKERAGE = (double)$tag;    // H
	                                        $status = is_numeric($tag);
	                                        if(!$status) { 
	                                        	Log::info("ERROR: invalid BROKERAGE/TOTAL_BROKERAGE format!!! =>" . print_r($row, true));
	                                        	$skip_row = true;
	                                        	return array('status' => false);
	                                        }
	                                        break;

	                                    case $rootHeader[7]['COL']:      // WT D
	                                        $WIHOUT_VAT = (double)$tag;  // D + I
	                                        $status = is_numeric($tag);
	                                        if(!$status) { 
	                                        	Log::info("ERROR: invalid WIHOUT_VAT format!!! =>" . print_r($row, true));
	                                        	$skip_row = true;
	                                        	return array('status' => false);
	                                        }
	                                        break;        

	                                    case $rootHeader[8]['COL']:   // TOTAL_PURCHASE, TOAL_SALE    
	                                        // $TOTAL_PURCHASE = abs((double)$tag);     // E
	                                        // $TOTAL_SALE = abs((double)$tag);         // J
	                                        switch($section) {
								            	case $TAG_HD_PURCHASE: 
								            	    $TOTAL_PURCHASE  = (double)$tag;  // B
								            	    break;

								                case $TAG_HD_SALE: 
								                    $TOTAL_SALE = (double)$tag; // G
								            	    break;

								                case $TAG_HD_DIV:
								                    $TOTAL_DIV = (double)$tag;
								            	    break;

                                                case $TAG_HD_DIV_ENT:
                                                    $TOTAL_DIV = (double)$tag;
                                                    break;

                                                case $TAG_HD_BONUS_ISSUE:
								            	    $TOTAL_PURCHASE  = (double)$tag;  // NEW
                                                    $TOTAL_BONUS = (double)$tag;
                                                    break; 

								            	default:
								            	    break;    
								            }
								            
	                                        $status = is_numeric($tag);
	                                        if(!$status) { 
	                                        	Log::info("ERROR: invalid TOTAL_PURCHASE/TOAL_SALE format!!! =>" . print_r($row, true));
	                                        	$skip_row = true;
	                                        	return array('status' => false);
	                                        }
	                                        break; 

	                                    case $rootHeader[9]['COL']:     // TOTAL_COST    
                                            $TOTAL_COST = (double)trim($tag);
                                            break;

                                         case $rootHeader[10]['COL']:   // PROFIT_LOSS 
                                            $PROFIT_LOSS = (double)trim($tag);
                                            break;        

                                        case $rootHeader[11]['COL']:    // BROKER_NAME 
                                            $BROKER_NAME = trim($value); 
                                            break;
	                                }
                                }
                        	    break;

                        	default:
                        	    break;    

                        } // switch $section
                        // end extract data for each section
                        break;      

                } // switch $tag
                $colindex++;

            } // foreach column
            
            /* continue next row */
            if($skip_row) {
            	$skip_row = false;
            	        
                continue;
            }
            
            if($section == "") {
            	$skip_row = false;	        
                continue;
            }
            // Total (Buy-Sell) EQUITIES
            Log::info("STEP#0: section[" . $section . "] = " . print_r($row, true));

            switch($section) {
            	case $TAG_HD_PURCHASE: 
            	    $TYPE = "P";
            	    break;
                case $TAG_HD_SALE: 
                    $TYPE = "S";
            	    break;
                case $TAG_HD_BONUS_ISSUE:
                    /* Ignore all in seciton "BONUS_ISSUE" */
                    //$TYPE = "IGNORED";
                    $TYPE = "P";//$TYPE = "B";
                    break; 

                case $TAG_HD_DIV_ENT: 
                     
                case $TAG_HD_DIV:
                    $TYPE = "D";
            	    break;
            	default:
            	    break;    
            }

            if($TYPE=="IGNORED")
                continue;

            /////////////////////////////
            // BEGINE INSERT DB
            /////////////////////////////
             $SECURITIES_NAME = $nameSHT;
        
            /// STEP#1
            $query = "SELECT * FROM TBL_P2_EQUITY_INDEX WHERE SYMBOL='". trim($SYMBOL) ."' ";
            Log::info('STEP#1 SQL:' . $query);          
            $rowsets = DB::select(DB::raw($query));
            $SYMBOL_RENAME_FLAG = 0; 
            $OLD_SYMBOL='';
            foreach ($rowsets as $dbrecord) {
                $SYMBOL_RENAME_FLAG  = ($dbrecord->SYMBOL_RENAME_FLAG == null) ? 0: $dbrecord->SYMBOL_RENAME_FLAG;
                $OLD_SYMBOL = $dbrecord->OLD_SYMBOL;
            }
            ///
            /// STEP#2
            ///  :: Check symbol & SECURITIES_NAME, if not exist will create one
            ///
            Log::info('STEP#2 $this->addReference ==>$OLD_SYMBOL=' . $OLD_SYMBOL . ',$SYMBOL='. trim($SYMBOL));
            $VERIFY_OLD_SYMBOL = $this->addReference(trim($SYMBOL), trim($SECURITIES_NAME));
            //Log::info('STEP#3 $this->addReference ==>$OLD_SYMBOL=' . $OLD_SYMBOL . ',$SYMBOL='. trim($SYMBOL));
            
            
            Log::info('BEGIN TYPE:' . $TYPE . ', SECTIONS:' . $section);
            if ($TYPE == '') { 
                Log::info('ERROR###### $TYPE:' . $TYPE . ', STATE:' . $state);
            	continue;
            }
            
            ///   check last remain unit value
            if(($SYMBOL_RENAME_FLAG==1) && ($VERIFY_OLD_SYMBOL == 0)) {
                $query = " SELECT TOP 1 * FROM TBL_P2_EQUITY_TRANS " .
                         " WHERE " .   
                         "       SYMBOL='". trim($OLD_SYMBOL) ."' " .
                         "       AND TYPE='"  . $TYPE . "'" .
                         "       AND SECURITIES_NAME = '" . trim($SECURITIES_NAME) ."' " .                                                              
                         " ORDER BY TRANS_DATE DESC ";

                Log::info('VERIFY_OLD_SYMBOL==0 => SQL:' . $query);          
                $rowsets = DB::select(DB::raw($query));
                
                //$REMAIN_UNIT = $UNIT_PURCHASE;
                foreach ($rowsets as $dbrecord) {
                    
                    switch($state) {
                         case 0: // Purchase
                            $REMAIN_UNIT = $UNIT_PURCHASE + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT= $TOTAL_AMOUNT + $TOTAL_PURCHASE;
                            if($REMAIN_UNIT > 0)
                               $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                               $PRICE_PER_UNIT = $TOTAL_AMOUNT;	
                            break;

                         case 1: // Sales
                            $REMAIN_UNIT = abs($UNIT_SALE) + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT = $TOTAL_AMOUNT - $TOTAL_SALE;
                            if($REMAIN_UNIT > 0)
                               $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                               $PRICE_PER_UNIT = $TOTAL_AMOUNT;	
                            break;
                         
                         case 2: // Dividend
                            // Wait for P'Nutt
                            $REMAIN_UNIT = abs($UNIT_SALE) + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT = $TOTAL_AMOUNT - $TOTAL_SALE;
                            if($REMAIN_UNIT > 0)
                               $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                               $PRICE_PER_UNIT = $TOTAL_AMOUNT;	
                            break;
                        case 3: // Dividend
                            // Wait for P'Nutt
                            $REMAIN_UNIT = abs($UNIT_BONUS) + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT = $TOTAL_AMOUNT - $TOTAL_BONUS;
                            if($REMAIN_UNIT > 0)
                               $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                               $PRICE_PER_UNIT = $TOTAL_AMOUNT; 
                            break;    
                           
                    }

                    Log::info('row['. $rowindex . '] RECORD:  ' . print_r($rowsets, true));
                    break;
                }

            } else if(($SYMBOL_RENAME_FLAG==1) && ($VERIFY_OLD_SYMBOL==1)) {
                $query = " SELECT TOP 1 * FROM TBL_P2_EQUITY_TRANS " .
                         " WHERE " .   
                         "       SYMBOL='". trim($SYMBOL) ."' " .
                         "       AND TYPE='"  . $TYPE . "'" .
                         "       AND SECURITIES_NAME = '" . trim($SECURITIES_NAME) ."' " .                                                              
                         " ORDER BY TRANS_DATE DESC ";
                Log::info('VERIFY_OLD_SYMBOL, VERIFY_OLD_SYMBOL ==1 => SQL:' . $query); 
                $rowsets = DB::select(DB::raw($query));
                 

                foreach ($rowsets as $dbrecord) {
                   
                    Log::info('row['. $rowindex . '] RECORD:  ' . print_r($rowsets, true));
                    switch($state) {
                         case 0: // Purchase
                            $REMAIN_UNIT = $UNIT_PURCHASE + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT= $TOTAL_AMOUNT + $TOTAL_PURCHASE;
                            //$PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            if($REMAIN_UNIT > 0)
                                $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                            	$PRICE_PER_UNIT = $TOTAL_AMOUNT; 
                            break;

                         case 1: // Sales
                            $REMAIN_UNIT = abs($UNIT_SALE) + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT = $TOTAL_AMOUNT - $TOTAL_SALE;
                            // $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            if($REMAIN_UNIT > 0)
                                $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                            	$PRICE_PER_UNIT = $TOTAL_AMOUNT; 
                            break;
                         
                         case 2: // Dividend
                            // Wait for P'Nutt
                            $REMAIN_UNIT = abs($UNIT_SALE) + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT = $TOTAL_AMOUNT - $TOTAL_SALE;
                            if($REMAIN_UNIT > 0)
                                $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                            	$PRICE_PER_UNIT = $TOTAL_AMOUNT;
                            
                            break;

                        case 3:
                            $REMAIN_UNIT = abs($UNIT_BONUS) + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT = $TOTAL_AMOUNT - $TOTAL_BONUS;
                            if($REMAIN_UNIT > 0)
                                $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                                $PRICE_PER_UNIT = $TOTAL_AMOUNT;
                            break;    
                    }
                    break;
                } // foreach
            ///   check last remain unit value
            } else if($SYMBOL_RENAME_FLAG==0) {
                $query = " SELECT TOP 1 * FROM TBL_P2_EQUITY_TRANS " .
                         " WHERE " .   
                         "       SYMBOL='". trim($SYMBOL) ."' " .
                           "     AND TYPE='"  . $TYPE . "'" .
                         "       AND SECURITIES_NAME = '" . trim($SECURITIES_NAME) ."' " .                                                              
                         " ORDER BY TRANS_DATE DESC ";
                Log::info('SYMBOL_RENAME_FLAG==0 => SQL:' . $query);          
                $rowsets = DB::select(DB::raw($query));
                
                //Log::info('STEP#1 UNIT_PURCHASE:' . $UNITS_PURCHASE);
	            //Log::info('STEP#1 UNIT_SALE:' . $UNITS_SALE);

                //$REMAIN_UNIT = $UNIT_PURCHASE;
                foreach ($rowsets as $dbrecord) {
                    
                    switch($state) {
                         case 0: // Purchase
                            $REMAIN_UNIT = $UNIT_PURCHASE + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT= $TOTAL_AMOUNT + $TOTAL_PURCHASE;
                            if($REMAIN_UNIT > 0)
                                $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                            	$PRICE_PER_UNIT = $TOTAL_AMOUNT; 
                            break;

                         case 1: // Sales
                            $REMAIN_UNIT = abs($UNIT_SALE) + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT = $TOTAL_AMOUNT - $TOTAL_SALE;
                            if($REMAIN_UNIT > 0)
                                $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                            	$PRICE_PER_UNIT = $TOTAL_AMOUNT;
                            break;
                         
                         case 2: // Dividend
                            $REMAIN_UNIT = abs($UNIT_SALE) + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT = $TOTAL_AMOUNT - $TOTAL_SALE;
                            if($REMAIN_UNIT > 0)
                                $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                            	$PRICE_PER_UNIT = $TOTAL_AMOUNT;
                            break;

                        case 3:  // Bonus
                            $REMAIN_UNIT = abs($UNIT_BONUS) + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT = $TOTAL_AMOUNT - $TOTAL_BONUS;
                            if($REMAIN_UNIT > 0)
                                $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                                $PRICE_PER_UNIT = $TOTAL_AMOUNT;
                            break;
                    }
                    break;
                }       
            } else {
                Log::info('Unknown Error');
            }

            Log::info('END###### $TYPE:' . $TYPE . ', SECTION:' . $section);
            if(($TRANS_DATE==null) || ($TRANS_DATE =='')) { 
            	Log::info('ERROR TRANS_DATE:' . $TRANS_DATE . ', SECTION:' . $section);
            	$status = false;
            	continue; 
            }

            ////////////////////////////////////////////
            $record_insert =  array (
                'JOB_ID'          => $JOB_ID,                  //  0 (Auto field) 
                'TRANS_DATE'      => $TRANS_DATE,              //  1
                'SETTLE_DATE'     => $SETTLE_DATE,             //  2
                'SYMBOL'          => $SYMBOL,                  //  3
                'TYPE'            => $TYPE,                    //  4
                'SECURITIES_NAME' => $SECURITIES_NAME,         //  5 from drop down
                'BROKER_NAME'     => $BROKER_NAME,             //  6  
                'UNIT_PURCHASE'   => $UNIT_PURCHASE,           //  7
                'PRICE_PURCHASE'  => $PRICE_PURCHASE,          //  8 
                'TOTAL_PURCHASE'  => abs($TOTAL_PURCHASE),          //  9
                'VAT'             => $VAT,                     // 10  
                'WT'              => $WIHOUT_VAT,              // 11
                'BROKERAGE'       => $BROKERAGE,               // 12
                'UNIT_SALE'       => $UNIT_SALE,               // 13
                'PRICE_SALE'      => $PRICE_SALE,              // 14
                'TOTAL_SALE'      => abs($TOTAL_SALE),              // 15
                'UNIT_COST'       => $UNIT_COST,               // 16
                'TOTAL_COST'      => $TOTAL_COST,              // 17
                'PROFIT_LOSS'     => $PROFIT_LOSS,             // 18

                'UNIT_DIV'        => $UNIT_DIV,                // 19
                'PRICE_DIV'       => $PRICE_DIV,               // 20 
                'TOTAL_DIV'       => $TOTAL_DIV,               // 21


                'UNIT_BONUS'      => $UNIT_BONUS,              // 22
                'PRICE_BONUS'     => $PRICE_BONUS,             // 23
                'TOTAL_BONUS'     => $TOTAL_BONUS,             // 24
                
                // == TBD: start remove ==
                'REMAIN_UNIT'     => $REMAIN_UNIT,             // 25  
                'PRICE_PER_UNIT'  => $PRICE_PER_UNIT,          // 26 SUM(TOTAL_AMOUNT) / SUM(REMAIN_UNIT)
                'TOTAL_AMOUNT'    => $TOTAL_AMOUNT,            // 27 
                // == TBD: end ==



                'STATUS'          => 1,                        // 28 0=Wait for confirm, 1=Comfirmed
                'CREATE_DATE'     => $CREATE_DATE,             // 29
                'CREATE_BY'       => $CREATE_BY,               // 30
                'REFERENCE'       => $REFERENCE_DATE,          // 31
                'TOTAL_BROKERAGE' => $BROKERAGE                // 32
            );

            $record_update =  array (
                 
                //'TRANS_DATE'      => $TRANS_DATE,              //  1
                'SETTLE_DATE'     => $SETTLE_DATE,             //  2
                //'SYMBOL'          => $SYMBOL,                  //  3
                'TYPE'            => $TYPE,                    //  4
                // 'SECURITIES_NAME' => $SECURITIES_NAME,         //  5 from drop down
                'BROKER_NAME'     => $BROKER_NAME,             //  6  
                'UNIT_PURCHASE'   => $UNIT_PURCHASE,           //  7
                'PRICE_PURCHASE'  => $PRICE_PURCHASE,          //  8 
                'TOTAL_PURCHASE'  => abs($TOTAL_PURCHASE),          //  9
                'VAT'             => $VAT,                     // 10  
                'WT'              => $WIHOUT_VAT,              // 11
                'BROKERAGE'       => $BROKERAGE,               // 12
                'UNIT_SALE'       => $UNIT_SALE,               // 13
                'PRICE_SALE'      => $PRICE_SALE,              // 14
                'TOTAL_SALE'      => abs($TOTAL_SALE),              // 15
                'UNIT_COST'       => $UNIT_COST,               // 16
                'TOTAL_COST'      => $TOTAL_COST,              // 17
                'PROFIT_LOSS'     => $PROFIT_LOSS,             // 18

                'UNIT_DIV'        => $UNIT_DIV,                // 19
                'PRICE_DIV'       => $PRICE_DIV,               // 20 
                'TOTAL_DIV'       => $TOTAL_DIV,               // 21
                
                'UNIT_BONUS'      => $UNIT_BONUS,              // 22
                'PRICE_BONUS'     => $PRICE_BONUS,             // 23
                'TOTAL_BONUS'     => $TOTAL_BONUS,             // 24

                'REMAIN_UNIT'     => $REMAIN_UNIT,             // 25 
                'PRICE_PER_UNIT'  => $PRICE_PER_UNIT,          // 26 SUM(TOTAL_AMOUNT) / SUM(REMAIN_UNIT)
                'TOTAL_AMOUNT'    => $TOTAL_AMOUNT,            // 27 

                'STATUS'          => 1,                        // 28 0=Wait for confirm, 1=Comfirmed
                // 'CREATE_DATE'     => $CREATE_DATE,             // 29
                // 'CREATE_BY'       => $CREATE_BY,               // 30
                'REFERENCE'       => $REFERENCE_DATE,          // 31
                'TOTAL_BROKERAGE' => $BROKERAGE                // 32
            );
            try
            {  
                if(!$checkFileOnly) {
                   // INSERT NEW RECORD
                    array_push($data, $record_insert);
                    Log::info('row['. $rowindex . '] INSERT RECORD:  ' . print_r($record_insert, true));
                    $affected = DB::table('TBL_P2_EQUITY_TRANS')->insert($data);
                    $status = ($affected > 0);
                    Log::info('row['. $rowindex . '] INSERT RECORD STATUS:  ' . $status);
                }
                // /* Check previous record */ 
                // $query = " SELECT " . 
                //          "     COUNT(SYMBOL) As total ,(SELECT JOB_ID FROM TBL_P2_EQUITY_TRANS " .
                //          " WHERE TRANS_DATE = '". $TRANS_DATE . "'" .
                //          "     AND TOTAL_PURCHASE = '" . str_replace('-','',$TOTAL_PURCHASE) . "'" .
				// 		 "     AND TOTAL_SALE = '" . str_replace('-','',$TOTAL_SALE) . "'" .
                //          "     AND SYMBOL = '" . $SYMBOL . "'" .
                //          "     AND TYPE='"  . $TYPE . "'" .
                //          "     AND SECURITIES_NAME ='" . $SECURITIES_NAME  . "') AS JOB_ID".
                //          " FROM ".
                //          "     TBL_P2_EQUITY_TRANS " . 
                //          " WHERE TRANS_DATE = '". $TRANS_DATE . "'" .
                //          "     AND TOTAL_PURCHASE = '" . str_replace('-','',$TOTAL_PURCHASE) . "'" .
				// 		 "     AND TOTAL_SALE = '" . str_replace('-','',$TOTAL_SALE) . "'" .
                //          "     AND SYMBOL = '" . $SYMBOL . "'" .
                //          "     AND TYPE='"  . $TYPE . "'" .
                //          "     AND SECURITIES_NAME ='" . $SECURITIES_NAME  . "'";
                         
                // $all = DB::select(DB::raw($query));
                // $total =  $all[0]->total;
                // $JOBID = $all[0]->JOB_ID;
                // if($total <= 0) {
                //     if(!$checkFileOnly) {
                //        // INSERT NEW RECORD
                       
                //         array_push($data, $record_insert);
                //         Log::info('row['. $rowindex . '] INSERT RECORD:  ' . print_r($record_insert, true));
                //         $affected = DB::table('TBL_P2_EQUITY_TRANS')->insert($data);
                //         $status = ($affected > 0);
                //         Log::info('row['. $rowindex . '] INSERT RECORD STATUS:  ' . $status);
                //     }

                // } else {
                //     if(!$checkFileOnly) {
                // 	   // UPDATE EXISTING RECORD
                // 	   Log::info('row['. $rowindex . '] UPDATE EXISTING RECORD:  ' . print_r($record_update, true));
                // 	   // TRANS_DATE, SYMBOL, SECURITIES_NAME
                // 	   array_push($data, $record_update);
                // 	   $affected = DB::table('TBL_P2_EQUITY_TRANS')
                // 	                                ->where('JOB_ID' ,'=', $JOBID) 
                //                                     ->update($record_update);
                                                    
                //         $status = ($affected > 0);
                //         Log::info('row['. $rowindex . '] UPDATE RECORD STATUS:  ' . $status);
                //     }
                // }

                // // $id = DB::table('TBL_P2_EQUITY_TRANS')->insertGetId($data);
                // // $status = ($affected > 0);
                if(!$status) {
                    return array('status' => $status);
                }

            }
            catch (Illuminate\Database\QueryException $e) {
                $status = false;
                $errorCode = $e->errorInfo[1];
                Log::info('QueryException:' . $e->getMessage());
                $errorMessae = $e->getMessage();
                if($errorCode == 1062) {
                    Log::info('QueryException: We have a duplicate entry problem.');
                    $errorMessae = 'We have a duplicate entry problem.';
                }
                break;

            } 
            // catch(\Exception $e) {

            //     $status = false;
            //     Log::info('Exception:' . $e->getMessage());
            //     $errorMessae = $e->getMessage();
            //     break;
            // }
           
            /////////////////////////////
            // END INSERT/UPDATE DB
            /////////////////////////////

            if($num == count($rows)-2){
                return array('status' => $status);
            }
            $rowindex++;
            

        } // for 
        //return array('status' => $status);
        return array('status' => $status);
    }
    /**
     * MENU.1 
     * เป็นเมนูสําหรับนําเข้าข้อมูล ซื้อ - ขายหลักทรัพย์ (รูปแบบไฟล์นามสกลุ .xls) 
     * see. UOBAM-Transaction Listing 5-01-2017.cleaned.xls
     */
    function importUOBAMPurchaseAndSale($inputfile,  $clientOriginalFileName, $checkFileOnly, $nameSHT) {
        
        $validate_key = 'purchase';
        $results = null;
        $error_msg = '';
        $htmlResult = '';
        $filedate = date('Y-m-d');
        $filedatethai = date('Y-m-d');

        $headers = array();  
        $rootHeader = array();
        $childHeader = array();
        $firstColIndex = 0;
        $header_row = -1;
        //&$validate_key,

        $retdate = Excel::load($inputfile, function($reader) use( &$clientOriginalFileName,
                                                                  &$rootHeader,
                                                                  &$childHeader,
                                                                  &$firstColIndex,
                                                                  &$header_row,
                                                                  &$nameSHT,
                                                                  &$checkFileOnly,
                                                                  &$status,
                                                                  &$count, 
                                                                  &$error_msg,
                                                                  &$filedate ,
                                                                  &$filedatethai,
                                                                  &$htmlResult) {
            // first row not a header
            $reader->noHeading();
            // $reader->ignoreEmpty();
            // $reader->setSeparator('+');
            // $reader->formatDates(true, 'Y/');
            //$reader->formatDates(true, 'Y-m-d');

            $reader->formatDates(false);

            
            $results = $reader->get(); 
            $ret = $results->toArray();

            // signature column header1
            $TAG_HD_DATE            = "date";
            $TAG_HD_SECURITIES      = "securities";
            $TAG_HD_BROKER          = "broker";
            $TAG_HD_PURCHASE        = "purchase";
            $TAG_HD_BROKERAGE       = "brokerage"; //Brokerage
            $TAG_HD_VAT             = "vat";
            $TAG_HD_WIHOUT_VAT      = "w/t";

            $TAG_HD_SALE            = "sale";

            // signature column header2
            $TAG_HD_TRANS           = "trans.";
            $TAG_HD_SETTLE          = "settle";
            $TAG_HD_SECURITIES_NAME = "name";   
            $TAG_HD_BROKER_NAME     = "name";   
            $TAG_HD_PURCHASE_UNITS  = "units";
            $TAG_HD_PURCHASE_PRICE  = "price";
            $TAG_HD_PURCHASE_TOTAL  = "total";

            $TAG_HD_SALE_UNIT   = "unit";
            $TAG_HD_SALE_PRICE  = "price";
            $TAG_HD_SALE_TOTAL  = "total";

            $TAG_HD_BONUS_ISSUE     = "bonus issue";

            $TAG_HD_UNIT_COST   = "unit cost";
            $TAG_HD_TOTAL_COST   = "total cost";
            $TAG_HD_PROFIT_LOSS  = "profit(loss)";


            // signature section tag
            $TAG_PURCHASE       = "purchase";
            $TAG_SALE           = "sale";
            $TAG_DIVIDEND       = "dividend";
            $TAG_SUBTOTAL       = "sub-total";
            $TAG_TOTAL_PURCHASE = "total purchase";
                    
            //         
            $current_tag        = "";    
                
            $state = -1;
            $state_keys = array(0=>'PERCHASE',1=>'SALE',  2=>'DIVIDEND', 3=>'BONUS ISSUE');
            $prev_key = '';
            $rowindex = 0;

            $count = 0;
            $status = false;
            /*
            $rootHeader = array();

            $childHeader = array();
            $firstColIndex = 0;
            $header_row = -1;
            */
            // loop rows

            /**
             * column index for each subheader 
             */
            $brokerage_index = 4;
            $sale_hd_index = 7; 
            $EXPECTED_COLUMNS = 16;

            $error_msg = "ไม่พบ column [" . $TAG_HD_PROFIT_LOSS . "] หรือ header ของ excel ไม่ถูก";

            foreach($ret as $r => $rows) {
                $colindex = 0;
                // loop each column

                foreach($rows as $cols => $vm) {
                	    if (is_array($vm)) { 
                	        Log::info('INVALID FILE FORMAT ==> ' . print_r($vm, true));
                	        
                	    	continue;
                	    } else {
                           $value = $vm;
                	    }

                        $tag = strtolower(($value == NULL) ? '' : $value);
                        switch($tag) {
                        	
        
                            /// LEVEL : 0
                            case $TAG_HD_DATE:
                                $hd = array( $TAG_HD_DATE => $TAG_HD_DATE ,                // Col:0
                                             'COL' => $colindex ,
                                             'ROW' => $rowindex);  
                                array_push($rootHeader, $hd);       // 0
                                $firstColIndex = $colindex;
                                break;

                            case $TAG_HD_SECURITIES:
                                $hd = array($TAG_HD_SECURITIES => $TAG_HD_SECURITIES,
                                            'COL' => $colindex,
                                            'ROW' => $rowindex, 
                                            ); 
                                array_push($rootHeader, $hd);  // 1
                                break;

                            case $TAG_HD_BROKER:
                                $hd = array($TAG_HD_BROKER => $TAG_HD_BROKER,
                                            'COL' => $colindex,
                                            'ROW' => $rowindex);  
                                array_push($rootHeader,  $hd);  // 2
                                break;


                            case $TAG_HD_PURCHASE:
                                $hd = array($TAG_HD_PURCHASE => $TAG_HD_PURCHASE, 
                                            'COL' => $colindex,
                                            'ROW' => $rowindex);  
                                array_push($rootHeader, $hd); // 3
                                break;

                            case $TAG_HD_BROKERAGE:
                                //Log::info('row['. $rowindex . '][' .$colindex .'] :'. 
                                //          ' ADD:' . $TAG_HD_BROKERAGE );
                                $hd = array($TAG_HD_BROKERAGE => $TAG_HD_BROKERAGE, 
                                            'COL' => $colindex,
                                            'ROW' => $rowindex);
                                array_push($rootHeader, $hd); // 4
                                break;
                           
                            case $TAG_HD_VAT:
                                //Log::info('row['. $rowindex . '][' .$colindex .'] :'. 
                                //          ' ADD:' . $TAG_HD_VAT );
                                $hd = array($TAG_HD_VAT => $TAG_HD_VAT, 
                                            'COL' => $colindex,
                                            'ROW' => $rowindex);
                                array_push($rootHeader, $hd); // 5
                                break;

                            case $TAG_HD_WIHOUT_VAT:
                                //Log::info('row['. $rowindex . '][' .$colindex .'] :'. 
                                //          ' ADD:' . $TAG_HD_WIHOUT_VAT );
                                $hd = array($TAG_HD_WIHOUT_VAT => $TAG_HD_WIHOUT_VAT, 
                                            'COL' => $colindex,
                                            'ROW' => $rowindex);
                                array_push($rootHeader, $hd); // 6
                                break;    
                            //////////////    

                            case $TAG_HD_SALE: 
                                $hd = array($TAG_HD_SALE => $TAG_HD_SALE,
                                            'COL' => $colindex,
                                            'ROW' => $rowindex); 
                                array_push($rootHeader, $hd); // 7
                                break;   

                            //// LEVEL : 1
                            case $TAG_HD_TRANS:   // child: 0
                                if($rootHeader[0]) {
                                    $hdDate = $rootHeader[0];
                                    if($hdDate) {
                                        $COL = $hdDate['COL'];
                                        $ROW = $hdDate['ROW'];
                                        if($COL == $colindex) {
                                            $header_row = $ROW;
                                            Log::info('row['. $rowindex . '][' .$colindex .'] :'. 
                                              ' RESULT:' .$hdDate[$TAG_HD_DATE] . ',' .
                                              ' PARENT ROW:'. $ROW .  ',' .
                                              ' PARENT COLUMN:' . $COL);

                                            $hd = array( $TAG_HD_TRANS => $TAG_HD_TRANS ,                
                                                         'COL' => $colindex ,
                                                         'ROW' => $rowindex);  
                                            array_push($childHeader, $hd);   // 0
                                       }
                                    }
                                }
                                break;

                            case $TAG_HD_SETTLE:   // child 1
                                if(($header_row == $rowindex-1) && $rootHeader[0]) {
                                    $hdDate = $rootHeader[0];
                                    if($hdDate) {
                                        $COL = $hdDate['COL'];
                                        $ROW = $hdDate['ROW'];
                                        if($COL <= $colindex) {
                                            $header_row = $ROW;
                                           // Log::info('row['. $rowindex . '][' .$colindex .'] :'. 
                                           //   ' RESULT:' .$hdDate[$TAG_HD_DATE] . ',' .
                                           //   ' PARENT ROW:'. $ROW .  ',' .
                                           //   ' PARENT COLUMN:' . $COL);

                                            $hd = array( $TAG_HD_SETTLE => $TAG_HD_SETTLE ,                
                                                         'COL' => $colindex ,
                                                         'ROW' => $rowindex);  
                                            array_push($childHeader, $hd);   // 1
                                       }
                                    }
                                }
                                break;  

                            // name 
                            case $TAG_HD_SECURITIES_NAME:     // [2]
                            case $TAG_HD_BROKER_NAME:         // [3]  
                                if(($header_row == $rowindex-1) && $rootHeader[2]) {
                                    $hdDate = $rootHeader[2];
                                    if($hdDate) {
                                        $COL = $hdDate['COL'];
                                        $ROW = $hdDate['ROW'];
                                        if($COL == $colindex) {
                                           
                                           // Log::info('row['. $rowindex . '][' .$colindex .'] :' . 
                                           //   ' PARENT NAME:' . $hdDate[$TAG_HD_BROKER] . '+' . $TAG_HD_BROKER_NAME . ' ,' .
                                           //   ' PARENT ROW:'. $ROW .  ',' .
                                           //   ' PARENT COLUMN:' . $COL);

                                            $hd = array( $TAG_HD_BROKER_NAME => $TAG_HD_BROKER_NAME ,                
                                                         'COL' => $colindex ,
                                                         'ROW' => $rowindex);  
                                            array_push($childHeader, $hd);  // 2
                                        } else {
                                            if($rootHeader[1]) {
                                                $hdDate = $rootHeader[1];
                                                if($hdDate) {
                                                    $COL = $hdDate['COL'];
                                                    $ROW = $hdDate['ROW'];
                                                    if($COL == $colindex) {
                                                        $header_row = $ROW;
                                                       // Log::info('row['. $rowindex . '][' .$colindex .'] :' . 
                                                       //   ' PARENT NAME:' .$hdDate[$TAG_HD_SECURITIES] . '+' . $TAG_HD_SECURITIES_NAME . ' ,' .
                                                       //   ' PARENT ROW:'. $ROW .  ',' .
                                                       //   ' PARENT COLUMN:' . $COL);

                                                        $hd = array( $TAG_HD_SECURITIES_NAME => $TAG_HD_SECURITIES_NAME ,                
                                                                     'COL' => $colindex ,
                                                                     'ROW' => $rowindex);  
                                                        array_push($childHeader, $hd);  // 3
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }  else {
                                    $status = false;
                                }
                                break;

                            case $TAG_HD_PURCHASE_UNITS:   // [4]
                            case $TAG_HD_PURCHASE_PRICE:   // [5]
                            case $TAG_HD_PURCHASE_TOTAL:   // [6]
                                 // 
                                 // !!! คำเตือน !!! 
                                 //
                                 // เนื่องจาก Column Price Key จะเหมือนกัน จึงจำเป็นต้องนำ 
                                 // Field "Price" และ "Total" ของ Salse มาเช็คใน ส่วน header ของ Purchase  ด้วย
                                 //
                                if($rootHeader[3]) {
                                    $hdPurchase = $rootHeader[3];
                                    if($hdDate) {
                                        $COL = $hdPurchase['COL'];
                                        $ROW = $hdPurchase['ROW'];
                                        if((($COL+0) == $colindex) || (($COL+1) == $colindex) || (($COL+2) == $colindex)) {
                                             
                                            //Log::info('row['. $rowindex . '][' .$colindex .'] :' . 
                                            //              ' PARENT NAME:' .$hdPurchase[$TAG_HD_PURCHASE] . '+' . $tag . ' ,' .
                                            //              ' PARENT ROW:'. $ROW .  ',' .
                                            //              ' PARENT COLUMN:' . $COL);
                                            $header_row = $ROW; 
                                            $hd = array( $tag => $tag ,                
                                                         'COL' => $colindex ,
                                                         'ROW' => $rowindex);  
                                            array_push($childHeader, $hd);   // 0
                                       } else {

                                         if($rootHeader[$sale_hd_index]) {
                                                $hdPurchase = $rootHeader[$sale_hd_index];
                                                if($hdDate) {
                                                    $COL = $hdPurchase['COL'];
                                                    $ROW = $hdPurchase['ROW'];
                                                    if((($COL+0) == $colindex) || 
                                                       (($COL+1) == $colindex) || 
                                                       (($COL+2) == $colindex) || 
                                                       (($COL+3) == $colindex) ||
                                                       (($COL+4) == $colindex) ||
                                                       (($COL+5) == $colindex)) {
                                                         
                                                        Log::info('row['. $rowindex . '][' .$colindex .'] :' . 
                                                          ' SALE NAME:' . $tag . ' ,' .
                                                          ' SALE ROW:'. $ROW .  ',' .
                                                          ' SALE COLUMN:' . $COL);

                                                        $hd = array( $tag => $tag ,                
                                                                     'COL' => $colindex ,
                                                                     'ROW' => $rowindex);  
                                                        array_push($childHeader, $hd);   // 0
                                                   }
                                                }
                                            }
                                       }
                                    }
                                }
                                break;
                           

                            case null:
                
                            case "": // Only for child of "Brokerage" / "VAT" / "W/T"  
                                 // 
                                 // !!! คำเตือน !!! 
                                 //
                                 // เนื่องจาก Column "Brokerage" "VAT" "W/T" จะถูก merge อยู่ใน ส่วน header
                                 //  จึงไม่เห็น ค่า (empty) ใน $tag นี้  จำเป็นต้อง ใช้ $brokerage_index เป็น ตัวอ้างอิง
                                 //  โดยปรกติ $brokerage_index จะเริ่มต้นที่ ค่า "7"  
                                 //  
                                 //
                                if(($header_row == $rowindex-1) && ($header_row>1)) {
                                    if($brokerage_index < 9) {
                                        $hdDate = $rootHeader[$brokerage_index]; // Broker rage
                                        if($hdDate) {
                                            $COL = $hdDate['COL'];
                                            $ROW = $hdDate['ROW'];
                                            if($COL == $colindex) {
                                                switch($colindex) {
                                                    case 7: 
                                                        $hd = array( $TAG_HD_BROKERAGE => $TAG_HD_BROKERAGE ,                
                                                                             'COL' => $colindex ,
                                                                             'ROW' => $rowindex);  
                                                            array_push($childHeader, $hd); // 7
                                                            $brokerage_index++;
                                                            break;
                                                    case 8: 
                                                            $hd = array( $TAG_HD_VAT => $TAG_HD_VAT ,                
                                                                             'COL' => $colindex ,
                                                                             'ROW' => $rowindex);  
                                                            array_push($childHeader, $hd); // 8
                                                            $brokerage_index++;
                                                            break;
                                                    case 9: 
                                                            $hd = array( $TAG_HD_WIHOUT_VAT => $TAG_HD_WIHOUT_VAT ,                
                                                                             'COL' => $colindex ,
                                                                             'ROW' => $rowindex);  
                                                            array_push($childHeader, $hd); // 9
                                                            $brokerage_index++;
                                                            break;
                                                }  
                                            }
                                        }
                                    }
                                }
                                break;   
                                
                            case $TAG_HD_SALE_UNIT:      // [10]

                            //case $TAG_HD_SALE_PRICE:   // [11]
                            //case $TAG_HD_SALE_TOTAL:   // [12]

                            case $TAG_HD_UNIT_COST:    // [13]
                            case $TAG_HD_TOTAL_COST:   // [14]
                            case $TAG_HD_PROFIT_LOSS:  // [15]
                                // $sale_hd_index = 7; // 5
                                if($rootHeader[$sale_hd_index]) {
                                    $hdPurchase = $rootHeader[$sale_hd_index];
                                    if($hdDate) {
                                        $COL = $hdPurchase['COL'];
                                        $ROW = $hdPurchase['ROW'];
                                        if((($COL+0) == $colindex) || 
                                           (($COL+1) == $colindex) || 
                                           (($COL+2) == $colindex) || 
                                           (($COL+3) == $colindex) ||
                                           (($COL+4) == $colindex) ||
                                           (($COL+5) == $colindex)) {
                                             
                                           Log::info('row['. $rowindex . '][' .$colindex .'] :' . 
                                                          ' SALE NAME:' . $tag . ' ,' .
                                                          ' SALE ROW:'. $ROW .  ',' .
                                                          ' SALE COLUMN:' . $COL);

                                            $hd = array( $tag => $tag ,                
                                                         'COL' => $colindex ,
                                                         'ROW' => $rowindex);  
                                            array_push($childHeader, $hd);   // 0
                                            $status = true;

                                            $firstRowIndex= $rowindex ;
                                        }
                                    }
                                }
                                break;

                            default:
                                break;
                        }
                        $colindex++;

                    } //check header

                    $rowindex++;
                    if($rowindex >  10) 
                        break;
            } // for loop rows
            
            // Log::info(print_r($childHeader,  true));
            
            if(!$checkFileOnly) {
                $ar = $this->saveDB_UOBAM_PurchaseAndSale($firstRowIndex, $checkFileOnly, 
                                    $ret, $rootHeader, 
                                    $childHeader, $header_row, 
                                    $firstColIndex, $nameSHT, $clientOriginalFileName);
                $status = $ar['status'];
                
            } else {
                //
                // sanity checking:
                //  By expect number of array count MUST equal to $EXPECTED_COLUMNS
                //
                if($status) {

                    $status = (count($childHeader) == $EXPECTED_COLUMNS);
                    if($status) {
                        $error_msg = '';
                        $ar = $this->saveDB_UOBAM_PurchaseAndSale($firstRowIndex, $checkFileOnly, 
                                    $ret, $rootHeader, 
                                    $childHeader, $header_row, 
                                    $firstColIndex, $nameSHT, $clientOriginalFileName);
                        $status = $ar['status'];
                        Log::info('$status == ' . $status);
                    } else {
                        $error_msg = 'จำนวณ แถวที่หัว xls ไม่เท่าเท่ากับ ' . $EXPECTED_COLUMNS ;
                    }

                }
            }
        });
        
        //if($checkFileOnly) {
             
        return array('status' =>$status, 'filedate' => $filedatethai, 'error_msg'=>$error_msg);
        //} 
    }

    function updateReference ($SYMBOL, $SECURITIES_NAME, $VERIFY_OLD_SYMBOL) {
        try 
        {
            DB::table('TBL_P2_EQUITY_RENAME_REF')
                ->where('SYMBOL', trim($SYMBOL))
                ->where('SECURITIES_NAME', trim($SECURITIES_NAME))
                ->update(array('VERIFY_OLD_SYMBOL' => $VERIFY_OLD_SYMBOL));

            $query = " UPDATE TBL_P2_EQUITY_RENAME_REF SET VERIFY_OLD_SYMBOL = " . $VERIFY_OLD_SYMBOL . " " . 
                     " WHERE " .   
                     "       SYMBOL='". trim($SYMBOL) ."' " .
                     "       AND SECURITIES_NAME = '" . trim($SECURITIES_NAME) ."' ";
        } catch (Illuminate\Database\QueryException $e) {
            $errorCode = $e->errorInfo[1];
            Log::error('QueryException:' . $e->getMessage());
            if($errorCode == 1062) {
                Log::error('QueryException: We have a duplicate entry problem.');
            }
        } catch(\Exception $e) {

            Log::error('Exception:' . $e->getMessage());
        }
    }

    function addReference($SYMBOL, $SECURITIES_NAME) {
        $queryCheckSYMBOL = "
        SELECT COUNT(*) as total FROM TBL_P2_EQUITY_INDEX WHERE SYMBOL = 'TEST'
        ";
        $check = DB::select(DB::raw($queryCheckSYMBOL));
        $isCheckExist =  ($check[0]->total > 0);
        if(!$isCheckExist) {
            try{
                $Insert = "
                INSERT INTO meafund_db.dbo.TBL_P2_EQUITY_INDEX
                (MARKET, SET_INDEX50, SET_INDEX100, SET_HD, INDUSTRIAL, BU, SYMBOL, SYMBOL_RENAME_FLAG, EFFECTIVE_DATE, START_DATE, CREATE_DATE)
                VALUES('SET', 'N', 'N', 'N', 'OTHER', 'OTHER', '". $SYMBOL ."', 0, getdate(), getdate(), getdate())
                ";
                $InsertSymbol = DB::select(DB::raw($Insert));
            }
            catch (Illuminate\Database\QueryException $e) {
                $errorCode = $e->errorInfo[1];
                Log::error('QueryException:' . $e->getMessage());
                if($errorCode == 1062) {
                    Log::error('QueryException: We have a duplicate entry problem.');
                }
                $refflag = false;
            } catch(\Exception $e) {
                 
                Log::error('Exception:' . $e->getMessage());
                $refflag = false;
            } 
        }



        $refflag = false;
        $VERIFY_OLD_SYMBOL=1;
        $query = "SELECT COUNT(*) as total FROM TBL_P2_EQUITY_RENAME_REF " .
                 " WHERE " .   
                 "       SYMBOL='". trim($SYMBOL) ."' " .
                 "       AND SECURITIES_NAME = '" . trim($SECURITIES_NAME) ."' ";
        $all = DB::select(DB::raw($query));

        Log::info('addReference($SYMBOL, $SECURITIES_NAME): '. $query);

        $isDataExist =  ($all[0]->total > 0);
        if(!$isDataExist) {
           
            $VERIFY_OLD_SYMBOL = 0;
            $refrows = array();
            $refrecord =  array(
                'SYMBOL'            => trim($SYMBOL),            //  0 (SYMBOL from xls file.) 
                'SECURITIES_NAME'   => trim($SECURITIES_NAME),   //  1 (from DROPDOWN list
                'VERIFY_OLD_SYMBOL' => $VERIFY_OLD_SYMBOL        //  2 (initial VERIFY_OLD_SYMBOL = 0)
                //'CREATE_DATE'       => $CREATE_DATE,             //  3
                //'CREATE_BY'         => $CREATE_BY,               //  4
            );
            
            try{
                $addrefsql = "
                INSERT INTO meafund_db.dbo.TBL_P2_EQUITY_INDEX
                (MARKET, SET_INDEX50, SET_INDEX100, SET_HD, INDUSTRIAL, BU, SYMBOL, SYMBOL_RENAME_FLAG, OLD_SYMBOL, EFFECTIVE_DATE, COMP_NAME, ADDRESS, ZIP_CODE, PHONE, FAX, URL, START_DATE, END_DATE, CREATE_DATE, CREATE_BY, MODIFY_DATE, MODIFY_BY)
                VALUES('SET', 'N', 'N', 'N', 'OTHER', 'OTHER', '".trim($SYMBOL)."', 0, '', '', '', '', '', '', '', '', '', '', getdate(), '', getdate(), '''');
                ";
                DB::select(DB::raw($addrefsql));
            }
            catch (Illuminate\Database\QueryException $e) {
                $errorCode = $e->errorInfo[1];
                Log::error('QueryException:' . $e->getMessage());
                if($errorCode == 1062) {
                    Log::error('QueryException: We have a duplicate entry problem.');
                }
                $refflag = false;
            } catch(\Exception $e) {
                 
                Log::error('Exception:' . $e->getMessage());
                $refflag = false;
            }

            try
             {  // TEST ONLY PERCHASE
                array_push($refrows, $refrecord);
                // Log::info('row['. $rowindex . '] INSERT RECORD:  ' . print_r($record, true));
                $affected = DB::table('TBL_P2_EQUITY_RENAME_REF')->insert($refrows);
                // $id = DB::table('TBL_P2_EQUITY_TRANS')->insertGetId($data);
                if($affected > 0) {
                    $refflag = true;
                    Log::info('Created REF on TBL_P2_EQUITY_RENAME_REF :: SYMBOL:' . 
                          trim($SYMBOL). ', SECURITIES_NAME:' . trim($SECURITIES_NAME));
                } else {
                    Log::info('Sumpbol &  SECURITIES_NAME alreay exist in table TBL_P2_EQUITY_RENAME_REF (' . 
                          trim($SYMBOL). ', SECURITIES_NAME:' . trim($SECURITIES_NAME) . ')');
                    $refflag = false;
                }
            }
            catch (Illuminate\Database\QueryException $e) {
                $errorCode = $e->errorInfo[1];
                Log::error('QueryException:' . $e->getMessage());
                if($errorCode == 1062) {
                    Log::error('QueryException: We have a duplicate entry problem.');
                }
                $refflag = false;
            } catch(\Exception $e) {
                 
                Log::error('Exception:' . $e->getMessage());
                $refflag = false;
            }
                 
        } 
        return $VERIFY_OLD_SYMBOL;
    }
    /*
    $ar = $this->saveDB_UOBAM_PurchaseAndSale($firstRowIndex, $checkFileOnly, $ret, $rootHeader, 
                                    $childHeader, $header_row, 
                                    $firstColIndex, $nameSHT, $clientOriginalFileName);
    */
    function saveDB_UOBAM_PurchaseAndSale($firstRowIndex, $checkFileOnly, 
             $rows, $rootHeader, $childHeader, $header_row,
             $firstColIndex, 
             $nameSHT, $clientOriginalFileName) {
                

        $errorMessae = '';
        // Retrive logged ID
        $user_data = Session::get('user_data');
        /* PURCHASE */
        $JOB_ID          = '';   //  0 (Auto field) 
        $TRANS_DATE      = '';   //  1
        $SETTLE_DATE     = '';   //  2
        $SYMBOL          = '';   //  3
        $TYPE            = '';   //  4
        $SECURITIES_NAME = '';   //  5
        $BROKER_NAME     = 0.0;  //  6  
        $UNIT_PURCHASE   = 0.0;  //  7
        $PRICE_PURCHASE  = 0.0;  //  8 
        $TOTAL_PURCHASE  = 0.0;  //  9
        $BROKERAGE       = 0;    // 10
        $VAT             = 0.0;  // 11
        $WIHOUT_VAT      = 0.0;  // 12
        /* SALES */
        $UNIT_SALE       = 0.0;  // 13
        $PRICE_SALE      = 0.0;  // 14
        $TOTAL_SALE      = 0.0;  // 15
        $UNIT_COST       = 0.0;  // 16
        $TOTAL_COST      = 0.0;  // 17
        $PROFIT_LOSS     = 0.0;  // 18

        $UNIT_DIV        = 0.0;  // 19
        $PRICE_DIV       = 0.0;  // 20 
        $TOTAL_DIV       = 0.0;  // 21

        $UNIT_BONUS        = 0.0;  // 19
        $PRICE_BONUS       = 0.0;  // 20 
        $TOTAL_BONUS       = 0.0;  // 21

        $REMAIN_UNIT     = 0.0;  // 22
        $PRICE_PER_UNIT  = 0.0;  // 23
        $TOTAL_AMOUNT    = 0.0;  // 24
        $STATUS          = 0;    // 25
        $CREATE_DATE = date("Y-m-d H:i:s");  // 24
        $CREATE_BY = $user_data->emp_id;     // 25
        $REFERENCE_DATE  = '';               // 26

        $TODAY_DATE = date("Y-m-d");  

        // signature tag
        $TAG_PURCHASE       = "purchase";
        $TAG_SALE           = "sale";
        $TAG_DIVIDEND       = "dividend";
        $TAG_SUBTOTAL_SECTION   = "sub-total";
        $TAG_TOTAL_PURCHASE = "total purchase";
        $TAG_TOTAL_SALE     = "total sale";
        $TAG_TOTAL_TRADE    = "total trade";
        $TAG_TOTAL_DIVIDEND    = "total dividend";
        $TAG_EQUITY_LISTED  = "equity listed";

        $TAG_EQUITY_UNLISTED  = "equity unlisted";

        $TAG_BONUS_ISSUE    = "bonus issue";
        $TAG_TOTAL_BONUS_ISSUE    = "total bonus issue";

        // equity listed
            
        $current_tag        = "";    
        
        $state = -1;
        $TYPE  = '';   //  4
        $state_keys = array(0=>'PERCHASE', 1=>'SALE', 2=>'DIVIDEND', 3=>'BONUS ISSUE');
        $prev_key = '';
        $rowindex = 0;
        $status = false;
        $section_changed = false;
        
        
        // Retrive logged ID
        Log::info('SESSION: [user_data] =' . $CREATE_BY);

        //Log::info('ENTER: saveDB_UOBAM_PurchaseAndSale');
        //Log::debug('saveDB_UOBAM_PurchaseAndSale-->Start');
        foreach($rows as $r => $row) {
            //Log::info('DEBUG: saveDB_UOBAM_PurchaseAndSale.........$row['.print_r($row,true).']');
            $data = array();
            // RESET VAR
            //
            // Log::info(print_r($row, true));
            // continue;    
            //////////////////////////////////
            $JOB_ID          = date('YmdHis') .'-' .uniqid() ;   //  0 (Auto field) 
            $TRANS_DATE      = '';   //  1
            $SETTLE_DATE     = '';   //  2
            $SYMBOL          = '';   //  3
            
            $SECURITIES_NAME = '';   //  5
            $BROKER_NAME     = 0.0;  //  6  
            $UNIT_PURCHASE   = 0.0;  //  7
            $PRICE_PURCHASE  = 0.0;  //  8 
            $TOTAL_PURCHASE  = 0.0;  //  9
            $BROKERAGE       = 0;    //  10
            $VAT             = 0.0;  // 11
            $WIHOUT_VAT      = 0.0;  // 12

            $UNIT_SALE       = 0.0;  //  12
            $PRICE_SALE      = 0.0;  // 13
            $TOTAL_SALE      = 0.0;  // 14


            $UNIT_COST       = 0.0;  // 15
            $TOTAL_COST      = 0.0;  // 16
            $PROFIT_LOSS     = 0.0;  // 17
            $UNIT_DIV        = 0.0;  // 18
            $PRICE_DIV       = 0.0;  // 19 
            $TOTAL_DIV       = 0.0;  // 20

            $UNIT_BONUS      = 0.0;  // 19
            $PRICE_BONUS     = 0.0;  // 20 
            $TOTAL_BONUS     = 0.0;  // 21

            $REMAIN_UNIT     = 0.0;  // 21
            $PRICE_PER_UNIT  = 0.0;  // 22
            $TOTAL_AMOUNT    = 0.0;  // 23
            //$STATUS          = 0;    // 24
            $CREATE_DATE     = date("Y-m-d H:i:s");  // 24
            $CREATE_BY       = $user_data->emp_id;   // 25
            $REFERENCE_DATE  = $clientOriginalFileName;   // 26

            
            $colindex = 0;
            

            if($rowindex <= ($firstRowIndex)) {
                $rowindex++;
                continue;
            } 

           // Log::info('RAWDATA: ' .  print_r($row,  true));  
          
           // Log::info('2) RAWDATA: ' .  print_r($row,  true));
            // loop each column
            $status = true;
            foreach($row as $cols => $value) {
                if($section_changed) 
                    //$section_changed = false;
                	break;

                $tag = trim(strtolower(($value == NULL) ? '' : $value));
                //Log::info('$tag -> ' .  $tag);
                switch ($tag) {
                    case $TAG_EQUITY_UNLISTED:
                    case $TAG_SUBTOTAL_SECTION:
                        Log::info('FOUND @@@@@@ :) : ' . $value . ', STATE: '.  $state);
                        $section_changed = true;
                        continue;

                    case $TAG_TOTAL_BONUS_ISSUE:   
                    case $TAG_EQUITY_LISTED:
                    case $TAG_TOTAL_PURCHASE:
                  //  case $TAG_SUBTOTAL_SECTION:
                    case $TAG_TOTAL_SALE:
                   // case $TAG_TOTAL_TRADE:
                        $TRANS_DATE = "SKIPED";
                        //$TYPE = '';
                        Log::info('FOUND ****** :) : ' . $value . ', STATE: '.  $state);
                        $section_changed = true;
                        break;    
                    case $TAG_TOTAL_DIVIDEND:
                        $TRANS_DATE = "SKIPED";
                        Log::info('FOUND ****** :) : ' . $value . ', STATE: '.  $state);
                        $section_changed = true;                    
                        break;
                    case $TAG_BONUS_ISSUE:
                        $TRANS_DATE = "SKIPED";
                        $TOTAL_AMOUNT = 0.0;
                        $REMAIN_UNIT = 0.0;
                        $state = 3;
                        $TYPE = 'B';
                        Log::info('FOUND BONUS ISSUE: ' . $TAG_BONUS_ISSUE. ', STATE: '.  $state);
                        $section_changed = true;
                        break;     

                    case $TAG_PURCHASE:
                        $TRANS_DATE = "SKIPED";
                        $TOTAL_AMOUNT = 0.0;
                        $REMAIN_UNIT = 0.0;
                        $state = 0;
                        $TYPE = 'P';
                        Log::info('FOUND PURCHASE: ' . $TAG_PURCHASE . ', STATE: '.  $state);
                        $section_changed = true; 
                        continue; 

                    case $TAG_SALE:
                        $TRANS_DATE = "SKIPED";
                        $TOTAL_AMOUNT = 0.0;
                        $REMAIN_UNIT = 0.0;
                        $state = 1;
                        $TYPE = 'S';
                        Log::info('FOUND TAG_SALE: ' . $TAG_SALE . ', STATE: '.  $state);
                        $section_changed = true;
                        continue; 

                    case $TAG_DIVIDEND:
                        $TRANS_DATE = "SKIPED";
                        $TOTAL_AMOUNT = 0.0;
                        $REMAIN_UNIT = 0.0;
                        $state = 2;
                        $TYPE = 'D';
                        Log::info('FOUND DIVIDEND: ' . $TAG_DIVIDEND . ', STATE: '.  $state);
                        $section_changed = true;
                        continue;  

                  

                    case null:
                    case '':
                        continue;

                    default:
                        switch($colindex) {
                            case  $childHeader[0]['COL']:     // 0
                                try {
                                    // check first column is DATE or LABEL TAG
                                    $TRANS_DATE = $tag;
                                    if($TODAY_DATE == $TRANS_DATE) {
                                        $TRANS_DATE = "SKIPED";
                                        $prev_key = $TRANS_DATE;
                                        
                                    } else {
                                        if($prev_key == "SKIPED") { 
                                            $prev_key = $value;

                                            //if($state < 3)
                                            //   $state++;
                                        }
                                    } 

                                    if($TRANS_DATE == $TAG_PURCHASE) {
                                    	//$TOTAL_AMOUNT = 0.0;
                                    	//$REMAIN_UNIT = 0.0;
                                    	//$PRICE_PER_UNIT = 0.0;
                                        $state = 0;
                                    }else if($TRANS_DATE == $TAG_SALE) {
                                        //$TOTAL_AMOUNT = 0.0;
                                        //$REMAIN_UNIT = 0.0;
                                        //$PRICE_PER_UNIT = 0.0;
                                        $state = 1;
                                    }else if($TRANS_DATE == $TAG_DIVIDEND) {
                                    	//$TOTAL_AMOUNT = 0.0;
                                    	//$REMAIN_UNIT = 0.0;
                                    	//$PRICE_PER_UNIT = 0.0;
                                        $state = 2;
                                    }else if($TRANS_DATE == $TAG_BONUS_ISSUE) {
                                        //$TOTAL_AMOUNT = 0.0;
                                        //$REMAIN_UNIT = 0.0;
                                        //$PRICE_PER_UNIT = 0.0;
                                        $state = 3;
                                    }

                                 } catch(InvalidArgumentException $x) { 
                                    $TRANS_DATE = 'NOT A DATE FIELD';
                                    Log::info('row['. $rowindex . '][' .$colindex .'] STRING: ' . $TRANS_DATE);
                                }
                                break;

                            case $childHeader[1]['COL']; //($firstColIndex + 1) :   // 1
                                $SETTLE_DATE = $value;
                                break;  
                            
                            case $childHeader[2]['COL']:   // 2
                                $SYMBOL = trim($value);    //  $SECURITIES_NAME as Symbol
                                Log::info('row['. $rowindex . '][' .$childHeader[2]['COL'] .'] >>> SECURITIES_NAME:' . $SYMBOL);
                                break;
 
                            case $childHeader[3]['COL']:   // 3
                                $BROKER_NAME = trim($value); // BROKER NANE
                                if($BROKER_NAME =="0") {
                                    $BROKER_NAME = "";
                                }
                                Log::info('row['. $rowindex . '][' .$childHeader[3]['COL'] .'] >>> BROKER_NAME:' . $BROKER_NAME);
                                  
                                break;

                            case $childHeader[4]['COL']:   // 4
                                $UNIT_PURCHASE = (double)$tag; //$tag;
                                $status = is_numeric($tag);

                                $UNIT_BONUS = (double)$tag; //$tag;
                                break;

                            case $childHeader[5]['COL']:   // 5
                                $PRICE_PURCHASE = (double)$tag; //$tag;
                                $status = is_numeric($tag);

                                $PRICE_BONUS = (double)$tag; //$tag;
                                break;

                            case $childHeader[6]['COL']:   // 6
                                $TOTAL_PURCHASE = (double)$tag; //$tag;
                                $status = is_numeric($tag);

                                $TOTAL_BONUS = (double)$tag;
                                break;

                            case $childHeader[7]['COL']:  // 7
                                $BROKERAGE = (double)$tag; // $tag;
                                $status = is_numeric($tag);
                                break;

                            case $childHeader[8]['COL']:  // 8
                                $VAT  = (double)$tag; //$tag;
                                $status = is_numeric($tag);
                                break;

                            case $childHeader[9]['COL']:  // 9
                                $WIHOUT_VAT  = (double)$tag; //$tag;
                                $status = is_numeric($tag);
                                break;
                     
                            case $childHeader[10]['COL']:   // 10
                                if($TYPE == 'S') {
                                   $UNIT_SALE = abs((double)$tag);
                                   $status = is_numeric($tag);
                                }
                                if($TYPE =='D') {
                                   $UNIT_DIV = abs((double)$tag);
                                   $status = is_numeric($tag);
                                }
                                /* 
                                if($TYPE =='B') {
                                   $UNIT_BONUS = abs((double)$tag);
                                   $status = is_numeric($tag);
                                }
                                */
                                break;

                            case $childHeader[11]['COL']:  // 11
                                if($TYPE == 'S') {
                                   $PRICE_SALE = (double)$tag; // (trim($tag) == '') ? 0.0 : trim($tag); //$tag;
                                   $status = is_numeric($tag);
                                }
                                if($TYPE =='D') {
                                   $PRICE_DIV = (double)$tag;
                                   $status = is_numeric($tag);
                                }
                                /*
                                if($TYPE =='B') {
                                   $PRICE_BONUS = (double)$tag;
                                   $status = is_numeric($tag);
                                }
                                */
                                break;

                            case $childHeader[12]['COL']:  // 12
                                if($TYPE == 'S') {
                                   $TOTAL_SALE = (double)$tag;//(trim($tag) == '') ? 0.0 : trim($tag); //$tag;
                                   $status = is_numeric($tag);
                                }
                                if($TYPE == 'D') {
                                   $TOTAL_DIV = (double)$tag;
                                   $status = is_numeric($tag);
                                }
                                /*
                                if($TYPE == 'B') {
                                   $TOTAL_BONUS = (double)$tag;
                                   $status = is_numeric($tag);
                                }
                                */
                                break;

                            case $childHeader[13]['COL']:  // 13
                                $UNIT_COST =  (double)$tag;//(trim($tag) == '') ? 0.0 : trim($tag); //$tag;
                                $status = is_numeric($tag);
                                break;

                            case $childHeader[14]['COL']:  // 14
                                $TOTAL_COST= (double)$tag; //(trim($tag) == '') ? 0.0 :  trim($tag); //$tag;
                                $status = is_numeric($tag);
                                break;

                            case $childHeader[15]['COL']:  // 15
                                $PROFIT_LOSS = (double)$tag; // (trim($tag) == '') ? 0.0 : trim($tag);
                                $status = is_numeric($tag);
                                break; 
                           
                            default:
                                break;    
                        }
                        break;    
                }
       
                //Log::info('row['. $rowindex . '][' .$colindex .'] SETTLE_DATE:  ' . $SETTLE_DATE);
                if(!$status) {
                    Log::info('Invalid File format!');
                    Log::info('row['. $rowindex . '][' .$colindex .'] VALUE:  ' . $tag);
                    return array('status' => $status);
                }

                $colindex++;
                               
            } // foreach column

            if($section_changed) {
                $section_changed = false;
                continue; 
            }

            /* */
            if((strtolower($TRANS_DATE) == $TAG_EQUITY_LISTED) ||
                    (strtolower($TRANS_DATE) == $TAG_EQUITY_UNLISTED) || 
                    (strtolower($TRANS_DATE) == $TAG_TOTAL_PURCHASE) ||
                    (strtolower($TRANS_DATE) == $TAG_SUBTOTAL_SECTION) ||
                    (strtolower($TRANS_DATE) == $TAG_TOTAL_SALE) ||
                    (strtolower($TRANS_DATE) == $TAG_TOTAL_BONUS_ISSUE) ||
                    (strtolower($TRANS_DATE) == $TAG_TOTAL_TRADE))
            {
            	continue;
            }	

            /* SECTIONS */
            if((strtolower($TRANS_DATE) == $TAG_PURCHASE) || 
                (strtolower($TRANS_DATE) == $TAG_SALE) || 
                (strtolower($TRANS_DATE) == $TAG_DIVIDEND)||
                (strtolower($TRANS_DATE) == $TAG_BONUS_ISSUE)) {
                continue;                
            }

            //if($TRANS_DATE == 'equity listed') continue;   
            //if($TRANS_DATE == 'equity unlisted') continue;
            
            if(($TRANS_DATE =='SKIPED') 
                || ($TRANS_DATE=='NOT A DATE FIELD') 
                || ($TRANS_DATE=='') 
                || (strtolower($TRANS_DATE) =='total trade') 
                || (strtolower($TRANS_DATE) =='total transaction')) 
                continue;
            if($state == -1) continue; 


            $SECURITIES_NAME = $nameSHT;
        
            /// STEP#1
            $query = "SELECT * FROM TBL_P2_EQUITY_INDEX WHERE SYMBOL='". trim($SYMBOL) ."' ";
            Log::debug('Check exist symbol query: ' . $query);          
            $rowsets = DB::select(DB::raw($query));
            $SYMBOL_RENAME_FLAG = 0; 
            $OLD_SYMBOL='';
            foreach ($rowsets as $dbrecord) {
                $SYMBOL_RENAME_FLAG  = ($dbrecord->SYMBOL_RENAME_FLAG == null) ? 0: $dbrecord->SYMBOL_RENAME_FLAG;
                $OLD_SYMBOL = $dbrecord->OLD_SYMBOL;
            }

            ///
            /// STEP#2
            ///  :: Check symbol & SECURITIES_NAME, if not exist will create one
            ///
            $VERIFY_OLD_SYMBOL = $this->addReference(trim($SYMBOL), trim($SECURITIES_NAME));
          

            Log::info('BEGIN###### $TYPE:' . $TYPE . ', STATE:' . $state);
            if ($TYPE == '') { 
                Log::error('ERROR###### $TYPE:' . $TYPE . ', STATE:' . $state);
            	continue;
            }
             
            ///   check last remain unit value
            if(($SYMBOL_RENAME_FLAG==1) && ($VERIFY_OLD_SYMBOL == 0)) {
                $query = " SELECT TOP 1 * FROM TBL_P2_EQUITY_TRANS " .
                         " WHERE " .   
                         "       SYMBOL='". trim($OLD_SYMBOL) ."' " .
                         "       AND TYPE='"  . $TYPE . "'" .
                         "       AND SECURITIES_NAME = '" . trim($SECURITIES_NAME) ."' " .                                                              
                         " ORDER BY TRANS_DATE DESC ";
                Log::debug('VERIFY_OLD_SYMBOL==0 => SQL:' . $query);          
                $rowsets = DB::select(DB::raw($query));
                
                //$REMAIN_UNIT = $UNIT_PURCHASE;
                foreach ($rowsets as $dbrecord) {
                    
                    switch($state) {
                         case 0: // Purchase
                            $REMAIN_UNIT = $UNIT_PURCHASE + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT= $TOTAL_AMOUNT + $TOTAL_PURCHASE;
                            if($REMAIN_UNIT > 0)
                               $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                               $PRICE_PER_UNIT = $TOTAL_AMOUNT;	
                            break;

                         case 1: // Sales
                            $REMAIN_UNIT = abs($UNIT_SALE) + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT = $TOTAL_AMOUNT - $TOTAL_SALE;
                            if($REMAIN_UNIT > 0)
                               $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                               $PRICE_PER_UNIT = $TOTAL_AMOUNT;	
                            break;
                         
                         case 2: // Dividend
                            // Wait for P'Nutt
                            $REMAIN_UNIT = abs($UNIT_SALE) + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT = $TOTAL_AMOUNT - $TOTAL_SALE;
                            if($REMAIN_UNIT > 0)
                               $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                               $PRICE_PER_UNIT = $TOTAL_AMOUNT;	
                            break;

                         case 3: // Bonus issue
                            // Wait for P'Nutt
                            $REMAIN_UNIT = abs($UNIT_BONUS) + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT = $TOTAL_AMOUNT - $TOTAL_BONUS;
                            if($REMAIN_UNIT > 0)
                               $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                               $PRICE_PER_UNIT = $TOTAL_AMOUNT; 
                            break;    
                           
                    }

                    Log::info('row['. $rowindex . '] RECORD:  ' . print_r($rowsets, true));
                    break;
                }

            } else if(($SYMBOL_RENAME_FLAG==1) && ($VERIFY_OLD_SYMBOL==1)) {
                $query = " SELECT TOP 1 * FROM TBL_P2_EQUITY_TRANS " .
                         " WHERE " .   
                         "       SYMBOL='". trim($SYMBOL) ."' " .
                         "       AND TYPE='"  . $TYPE . "'" .
                         "       AND SECURITIES_NAME = '" . trim($SECURITIES_NAME) ."' " .                                                              
                         " ORDER BY TRANS_DATE DESC ";
                // Log::info('VERIFY_OLD_SYMBOL, VERIFY_OLD_SYMBOL ==1 => SQL:' . $query); 
                $rowsets = DB::select(DB::raw($query));
                 

                foreach ($rowsets as $dbrecord) {
                   
                    Log::info('row['. $rowindex . '] RECORD:  ' . print_r($rowsets, true));
                    switch($state) {
                         case 0: // Purchase
                            $REMAIN_UNIT = $UNIT_PURCHASE + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT= $TOTAL_AMOUNT + $TOTAL_PURCHASE;
                            //$PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            if($REMAIN_UNIT > 0)
                                $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                            	$PRICE_PER_UNIT = $TOTAL_AMOUNT; 
                            break;

                         case 1: // Sales
                            $REMAIN_UNIT = abs($UNIT_SALE) + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT = $TOTAL_AMOUNT - $TOTAL_SALE;
                            // $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            if($REMAIN_UNIT > 0)
                                $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                            	$PRICE_PER_UNIT = $TOTAL_AMOUNT; 
                            break;
                         
                         case 2: // Dividend
                            // Wait for P'Nutt
                            $REMAIN_UNIT = abs($UNIT_SALE) + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT = $TOTAL_AMOUNT - $TOTAL_SALE;
                            if($REMAIN_UNIT > 0)
                                $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                            	$PRICE_PER_UNIT = $TOTAL_AMOUNT;
                            
                            break;

                        case 3: // Bonus
                            $REMAIN_UNIT = abs($UNIT_BONUS) + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT = $TOTAL_AMOUNT - $TOTAL_BONUS;
                            if($REMAIN_UNIT > 0)
                                $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                                $PRICE_PER_UNIT = $TOTAL_AMOUNT;
                            
                            break;    
                    }
                    break;
                } // foreach
            ///   check last remain unit value
            } else if($SYMBOL_RENAME_FLAG==0) {
                $query = " SELECT TOP 1 * FROM TBL_P2_EQUITY_TRANS " .
                         " WHERE " .   
                         "       SYMBOL='". trim($SYMBOL) ."' " .
                           "     AND TYPE='"  . $TYPE . "'" .
                         "       AND SECURITIES_NAME = '" . trim($SECURITIES_NAME) ."' " .                                                              
                         " ORDER BY TRANS_DATE DESC ";
                Log::info('SYMBOL_RENAME_FLAG==0 => SQL:' . $query);          
                $rowsets = DB::select(DB::raw($query));
                
                //$REMAIN_UNIT = $UNIT_PURCHASE;
                foreach ($rowsets as $dbrecord) {
                    
                    switch($state) {
                         case 0: // Purchase
                            $REMAIN_UNIT = $UNIT_PURCHASE + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT= $TOTAL_AMOUNT + $TOTAL_PURCHASE;
                            if($REMAIN_UNIT > 0)
                                $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                            	$PRICE_PER_UNIT = $TOTAL_AMOUNT; 
                            break;

                         case 1: // Sales
                            $REMAIN_UNIT = abs($UNIT_SALE) + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT = $TOTAL_AMOUNT - $TOTAL_SALE;
                            if($REMAIN_UNIT > 0)
                                $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                            	$PRICE_PER_UNIT = $TOTAL_AMOUNT;
                            break;
                         
                         case 2: // Dividend
                            $REMAIN_UNIT = abs($UNIT_SALE) + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT = $TOTAL_AMOUNT - $TOTAL_SALE;
                            if($REMAIN_UNIT > 0)
                                $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                            	$PRICE_PER_UNIT = $TOTAL_AMOUNT;
                            break;

                         case 3: // Bonus issue
                            $REMAIN_UNIT = abs($UNIT_BONUS) + $dbrecord->REMAIN_UNIT;
                            $TOTAL_AMOUNT = $TOTAL_AMOUNT - $TOTAL_BONUS;
                            if($REMAIN_UNIT > 0)
                                $PRICE_PER_UNIT = ($TOTAL_AMOUNT / $REMAIN_UNIT);
                            else
                                $PRICE_PER_UNIT = $TOTAL_AMOUNT;
                            break;  
                    }

                    Log::info('row['. $rowindex . '] RECORD:  ' . print_r($rowsets, true));
                    break;
                }       
            } else {
                $errorMessae = "Unknown error";
                Log::info('Unknown Error');
            }

      
            Log::info('END###### $TYPE:' . $TYPE . ', STATE:' . $state);
            if(($TRANS_DATE==null) || ($TRANS_DATE =='')) continue; 

            /* check reserved field name */
            if((strtolower($TRANS_DATE) == trim($TAG_EQUITY_LISTED)) ||
               (strtolower($TRANS_DATE) == trim($TAG_EQUITY_UNLISTED)) ||
               (strtolower($TRANS_DATE) == trim($TAG_TOTAL_PURCHASE)) ||
               (strtolower($TRANS_DATE) == trim($TAG_SUBTOTAL_SECTION)) ||
               (strtolower($TRANS_DATE) == trim($TAG_TOTAL_SALE)) ||
               (strtolower($TRANS_DATE) == trim($TAG_TOTAL_BONUS_ISSUE)) ||
               (strtolower($TRANS_DATE) == trim($TAG_TOTAL_TRADE)))
            {
            	continue;
            }	

            /* check SECTIONS */
            if((strtolower($TRANS_DATE) == $TAG_PURCHASE) || 
                (strtolower($TRANS_DATE) == $TAG_SALE) || 
                (strtolower($TRANS_DATE) == $TAG_BONUS_ISSUE) ||
                (strtolower($TRANS_DATE) == $TAG_DIVIDEND)) {
                continue;                
            }
 

            ////////////////////////////////////////////
            $record_insert =  array (
                'JOB_ID'          => $JOB_ID,                  //  0 (Auto field) 
                'TRANS_DATE'      => $TRANS_DATE,              //  1
                'SETTLE_DATE'     => $SETTLE_DATE,             //  2
                'SYMBOL'          => $SYMBOL,                  //  3
                'TYPE'            => $TYPE,                    //  4
                'SECURITIES_NAME' => $SECURITIES_NAME,         //  5 from drop down
                'BROKER_NAME'     => $BROKER_NAME,             //  6  
                'UNIT_PURCHASE'   => $UNIT_PURCHASE,           //  7
                'PRICE_PURCHASE'  => $PRICE_PURCHASE,          //  8 
                'TOTAL_PURCHASE'  => $TOTAL_PURCHASE,          //  9
                'VAT'             => $VAT,                     // 10  
                'WT'              => $WIHOUT_VAT,              // 11
                'BROKERAGE'       => $BROKERAGE,               // 12
                'UNIT_SALE'       => $UNIT_SALE,               // 13
                'PRICE_SALE'      => $PRICE_SALE,              // 14
                'TOTAL_SALE'      => $TOTAL_SALE,              // 15
                'UNIT_COST'       => $UNIT_COST,               // 16
                'TOTAL_COST'      => $TOTAL_COST,              // 17
                'PROFIT_LOSS'     => $PROFIT_LOSS,             // 18

                'UNIT_DIV'        => $UNIT_DIV,                // 19
                'PRICE_DIV'       => $PRICE_DIV,               // 20 
                'TOTAL_DIV'       => $TOTAL_DIV,               // 21

                'UNIT_BONUS'      => $UNIT_BONUS,              // 22
                'PRICE_BONUS'     => $PRICE_BONUS,             // 23 
                'TOTAL_BONUS'     => $TOTAL_BONUS,             // 24
                
                // == TBD: start remove ==
                'REMAIN_UNIT'     => $REMAIN_UNIT,             // 25  
                'PRICE_PER_UNIT'  => $PRICE_PER_UNIT,          // 26 SUM(TOTAL_AMOUNT) / SUM(REMAIN_UNIT)
                'TOTAL_AMOUNT'    => $TOTAL_AMOUNT,            // 27 
                // == TBD: end ==

                'STATUS'          => 1,                        // 28 0=Wait for confirm, 1=Comfirmed
                'CREATE_DATE'     => $CREATE_DATE,             // 29
                'CREATE_BY'       => $CREATE_BY,               // 30
                'REFERENCE'       => $REFERENCE_DATE,          // 31
                'TOTAL_BROKERAGE' => ($VAT +  $BROKERAGE)      // 32
            );

            $record_update =  array (
                 
                //'TRANS_DATE'      => $TRANS_DATE,              //  1
                'SETTLE_DATE'     => $SETTLE_DATE,             //  2
                //'SYMBOL'          => $SYMBOL,                  //  3
                'TYPE'            => $TYPE,                    //  4
                // 'SECURITIES_NAME' => $SECURITIES_NAME,         //  5 from drop down
                'BROKER_NAME'     => $BROKER_NAME,             //  6  
                'UNIT_PURCHASE'   => $UNIT_PURCHASE,           //  7
                'PRICE_PURCHASE'  => $PRICE_PURCHASE,          //  8 
                'TOTAL_PURCHASE'  => $TOTAL_PURCHASE,          //  9
                'VAT'             => $VAT,                     // 10  
                'WT'              => $WIHOUT_VAT,              // 11
                'BROKERAGE'       => $BROKERAGE,               // 12
                'UNIT_SALE'       => $UNIT_SALE,               // 13
                'PRICE_SALE'      => $PRICE_SALE,              // 14
                'TOTAL_SALE'      => $TOTAL_SALE,              // 15
                'UNIT_COST'       => $UNIT_COST,               // 16
                'TOTAL_COST'      => $TOTAL_COST,              // 17
                'PROFIT_LOSS'     => $PROFIT_LOSS,             // 18

                'UNIT_DIV'        => $UNIT_DIV,                // 19
                'PRICE_DIV'       => $PRICE_DIV,               // 20 
                'TOTAL_DIV'       => $TOTAL_DIV,               // 21

                'UNIT_BONUS'      => $UNIT_BONUS,              // 22
                'PRICE_BONUS'     => $PRICE_BONUS,             // 23 
                'TOTAL_BONUS'     => $TOTAL_BONUS,             // 24
                
                'REMAIN_UNIT'     => $REMAIN_UNIT,             // 25  
                'PRICE_PER_UNIT'  => $PRICE_PER_UNIT,          // 26 SUM(TOTAL_AMOUNT) / SUM(REMAIN_UNIT)
                'TOTAL_AMOUNT'    => $TOTAL_AMOUNT,            // 27 

                'STATUS'          => 1,                        // 28 0=Wait for confirm, 1=Comfirmed
                'CREATE_DATE'     => $CREATE_DATE,             // 29
                'CREATE_BY'       => $CREATE_BY,               // 30
                'REFERENCE'       => $REFERENCE_DATE,          // 31
                'TOTAL_BROKERAGE' => ($VAT +  $BROKERAGE)      // 32
            );
            
            try
             {  
                if(!$checkFileOnly) {
                   // INSERT NEW RECORD
                    array_push($data, $record_insert);
                    // Log::info('row['. $rowindex . '] INSERT RECORD:  ' . print_r($record_insert, true));
                    $affected = DB::table('TBL_P2_EQUITY_TRANS')->insert($data);
                    $status = ($affected > 0);
                    // Log::info('row['. $rowindex . '] INSERT RECORD STATUS:  ' . $status);
                }
                // /* Check previous record */ 
                // $query = " SELECT " . 
                //          "     COUNT(SYMBOL) As total ,( SELECT JOB_ID FROM TBL_P2_EQUITY_TRANS" .
                //          " WHERE TRANS_DATE = '". $TRANS_DATE . "'" .
                //          "     AND TOTAL_PURCHASE = '" . $TOTAL_PURCHASE . "'" .
                //          "     AND SYMBOL = '" . $SYMBOL . "'" .
                //          "     AND TYPE='"  . $TYPE . "'" .
                //          "     AND SECURITIES_NAME ='" . $SECURITIES_NAME  . "') AS JOB_ID".
                //          " FROM ".
                //          "     TBL_P2_EQUITY_TRANS " . 
                //          " WHERE TRANS_DATE = '". $TRANS_DATE . "'" .
                //          "     AND SYMBOL = '" . $SYMBOL . "'" .
                //          "     AND TOTAL_PURCHASE = '" . $TOTAL_PURCHASE . "'" .
                //          "     AND TYPE='"  . $TYPE . "'" .
                //          "     AND SECURITIES_NAME ='" . $SECURITIES_NAME  . "'";
                // $all = DB::select(DB::raw($query));
                // $total =  $all[0]->total;
                // $JOBID =  $all[0]->JOB_ID;
                // if($total <= 0) {
                //     if(!$checkFileOnly) {
                // 	   // INSERT NEW RECORD
                //         array_push($data, $record_insert);
                //         // Log::info('row['. $rowindex . '] INSERT RECORD:  ' . print_r($record_insert, true));
                //         $affected = DB::table('TBL_P2_EQUITY_TRANS')->insert($data);
                //         $status = ($affected > 0);
                //         // Log::info('row['. $rowindex . '] INSERT RECORD STATUS:  ' . $status);
                //     }

                // } else {
                //     if(!$checkFileOnly) {
                // 	   // UPDATE EXISTING RECORD
                // 	   //Log::info('row['. $rowindex . '] UPDATE EXISTING RECORD:  ' . print_r($record_update, true));
                // 	   // TRANS_DATE, SYMBOL, SECURITIES_NAME
                // 	   array_push($data, $record_update);
                // 	   $affected = DB::table('TBL_P2_EQUITY_TRANS')
                // 	                                ->where('JOB_ID' ,'=', $JOBID) 
                // 	                                ->update($record_update);
                //         $status = ($affected > 0);
                //        // Log::info('row['. $rowindex . '] UPDATE RECORD STATUS:  ' . $status);
                //     }
                // }
                // // $id = DB::table('TBL_P2_EQUITY_TRANS')->insertGetId($data);
                // // $status = ($affected > 0);
                if(!$status) {
                    return array('status' => $status);
                }

            }
            catch (Illuminate\Database\QueryException $e) {
                $status = false;
                $errorCode = $e->errorInfo[1];
                Log::info('QueryException:' . $e->getMessage());
                $errorMessae = $e->getMessage();
                if($errorCode == 1062) {
                    Log::info('QueryException: We have a duplicate entry problem.');
                    $errorMessae = "We have a duplicate entry problem.";
                }
                // terminate loop

                break;
            } catch(\Exception $e) {
                $status = false; 
                $errorMessae = $e->getMessage();
                Log::info('Exception:' . $e->getMessage());
                // terminate loop
                break;
            }
            
            $rowindex++;

        } // foreach

        Log::info( "saveDB_UOBAM_PurchaseAndSale: status:" . $status);
        return array('status' => $status, 'error_msg' => $errorMessae);
    }

    /**
     * MENU 2. - การ Import ข้อมูลค่านายหน้า ซื้อ-ขายหลักทรัพย์( BROKERAGE ) 
     *  
     */

    /**
     * Download sample xls (Brokerage)  
     */
    public function T2_downloadSampleBrokerage() {
         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $file = 'contents/sample/UOBAM_COM0816.xls';
        $newfile =  'UOBAM_COM0816.xls';
        $headers = array(
            'Content-Type: application/pdf',
        );
        return \Response::download($file, $newfile, $headers);
    }

    /**
     *  Check uploaded file format
     *  @param  request $request
     *  @return json with status, details
     */
    public function T2_checkBrokerageFile(Request $request) {
        /* limit execution timeout */
        ini_set('max_execution_time', 50000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        Log::info(get_class($this) .'::'. __FUNCTION__. 
                  ' datatype: ' . $request->input('datatype') .  " ," .
                  ' broker:' .  $request->input('broker'));
        
        $results = null;
        $error_msg = '';
        $htmlResult = '';

        $passedCount = 1;
        $count = 0;
        $passedCount = 1;

        $datatype = $request->input('datatype'); // $request->get('datatype');
        $broker = $request->input('broker'); 
        $nameSHT = $broker;
        $file = $request->file('exelimport');

        $clientOriginalFileName = $request->file('exelimport')->getClientOriginalName();

        $process_file = '60_2_check_brokerage_import.xls';
        $request->file('exelimport')->move(storage_path() . '/public/import/', $process_file);
       
        $inputfile = storage_path('/public/import/' . $process_file);

        Log::info('clientOriginalFileName:' . $clientOriginalFileName);
        
        $status = true;
        
        $checkOnly  = true;

        //$arr = $this->T2_importBrokerageFile($request);
        //$status = $arr['status'];
        //$filedate = $arr['filedate']; 

        ///////////////////////////////////////////////////////////////////////////////////////////
        $firstrow = '';
        $filedate = '';      
        $arr = array();  
        $tmp = strtoupper($nameSHT);
        if(strpos($tmp, 'UOB') >=0) {   
          $arr = $this->importBrokerageUOBAMFile($inputfile, $clientOriginalFileName, false, $nameSHT);
          $status = $arr['status'];
          $filedate = $arr['filedate']; 
        } elseif (strpos($tmp, 'SCB') >=0) { 
          $arr = $this->importBrokerageUOBAMFile($inputfile, $clientOriginalFileName, false, $nameSHT);
          $status = $arr['status'];
          $filedate = $arr['filedate']; 
        } else {
          $status = false;
        }

        Log::info(get_class($this) .'::'. __FUNCTION__. ($status ? "ข้อมูลถูกต้อง กดปุ่ม นำเข้าข้อมูล เพื่อนำข้อมูลเข้าระบบ" : "มีข้อผิดพลาด") ); 
        

        //////////////////////////////////
        return response()->json(array('success' =>  $status, 'html'=>'' . 
                '<br/> file :'   . $clientOriginalFileName .
                '<br/> status :' . ($status ? "ข้อมูลถูกต้อง กดปุ่ม นำเข้าข้อมูล เพื่อนำข้อมูลเข้าระบบ" : "มีข้อผิดพลาด") . 
                '<br/> date :'   . $filedate . " <br/>" ));
        
    } // T2_checkBrokerageFile

   
    /**
     * Import Brokerage - Menu 2.
     */
     public function T2_importBrokerageFile($request) {
        /* limit execution timeout */
        ini_set('max_execution_time', 50000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        Log::info(get_class($this) .'::'. __FUNCTION__. 
                  ' datatype: ' . $request->input('datatype') .  " ," .
                  ' broker' .  $request->input('broker'));
        
        $results = null;
        $error_msg = '';
        $htmlResult = '';

        $passedCount = 1;
        $count = 0;
        $passedCount = 1;

        $datatype = $request->input('datatype'); // $request->get('datatype');
        $broker = $request->input('broker'); 
        $nameSHT = $broker;
        $file = $request->file('exelimport');

        $clientOriginalFileName = $request->file('exelimport')->getClientOriginalName();

        $process_file = '60_2_brokerage_import.xls';
        $request->file('exelimport')->move(storage_path() . '/public/import/', $process_file);
       
        $inputfile = storage_path('/public/import/' . $process_file);

        $status = true;
        ///////////////////////////////////////////////////////////////////////////////////////////
        $firstrow = '';
        $filedate = '';      
        $arr = array();  
        $tmp = strtoupper($nameSHT);
        if(strpos($tmp, 'UOB') >=0) {   
          $arr = $this->importBrokerageUOBAMFile($inputfile, $clientOriginalFileName, false, $nameSHT);
          $status = $arr['status'];
          $filedate = $arr['filedate']; 
        } elseif (strpos($tmp, 'SCB') >=0) { 
          $arr = $this->importBrokerageUOBAMFile($inputfile, $clientOriginalFileName, false, $nameSHT);
          $status = $arr['status'];
          $filedate = $arr['filedate']; 
        } else {
          $status = false;
        }
        //$status = $arr['status'];
        //$filedate = $arr['filedate']; 
        Log::info(get_class($this) .'::'. __FUNCTION__. ($status ? "นำเข้าข้อมูลเรียบร้อย" : "มีข้อผิดพลาด") ); 

        //////////////////////////////////
        return response()->json(array('success' =>  $status, 'html'=>'' . 
                '<br/> status : ' .   ($status ? "นำเข้าข้อมูลเรียบร้อย" : "มีข้อผิดพลาด") . 
                '<br/> date : '     . $filedate . " <br/>" ));
        
    } // T2_importBrokerageFile
    
    /**
     * UOBAM Only
     */
    function importBrokerageUOBAMFile($inputfile,  $clientOriginalFileName, $checkFileOnly, $nameSHT) {
        $validate_key = 'purchase';
        $results = null;
        $error_msg = '';
        $htmlResult = '';
        $filedate = date('Y-m-d');
        $filedatethai = date('Y-m-d');

        $headers = array();  
        $rootHeader = array();
        $childHeader = array();
        $firstColIndex = 0;
        $header_row = -1;
        //&$validate_key,

        $ret = Excel::load($inputfile, function($reader) use( &$clientOriginalFileName,
                                                                  &$rootHeader,
                                                                  &$childHeader,
                                                                  &$firstColIndex,
                                                                  &$header_row,
                                                                  &$nameSHT,
                                                                  &$checkFileOnly,
                                                                  &$status,
                                                                  &$count, 
                                                                  &$error_msg,
                                                                  &$filedate ,
                                                                  &$filedatethai,
                                                                  &$htmlResult) {
            // first row not a header
            $reader->noHeading();
            // $reader->ignoreEmpty();
            // $reader->setSeparator('+');
            // $reader->formatDates(true, 'Y/');
            $reader->formatDates(true, 'Y-m-d');
            $results = $reader->get(); 
            $ret = $results->toArray();

            // signature column header1 
            $TAG_REPORT_ID      = "REPORT ID:";
            $TAG_REPORT_HD_PORT = "PORT:"; 
            $TAG_REPORT_HD_TYPE = "REPORT TYPE:";
            $TAG_REPROT_PERIOD_FROM  = "PERIOD FROM:";

            $TAG_REPORT_HD_TITLE ="TRADING TRANSACTION REPORT";
            $TAG_REPORT_HD_TIME = "TIME:";

            // signature column header2
            // PURCHASE
            $TAG_HD_TRANS_DATE      = "TRANS.DATE";
            $TAG_HD_SETT_DATE       = "SETT.DATE";
            $TAG_HD_PURCSEC_CODE    = "SEC.CODE";   
            $TAG_HD_BROKER_NAME     = "BROKER";   
            $TAG_HD_PURCHASE_UNITS  = "UNITS";
            $TAG_HD_PURCHASE_PRICE  = "PRICE";
            $TAG_HD_PURCHASE_TOTAL  = "TOTAL PURCHASE";
            $TAG_HD_BROKERAGE       = "BROKERAGE";
            $TAG_HD_VAT             = "VAT";
          
            // SALE
            $TAG_HD_SALE_UNITS      = "UNITS";
            $TAG_HD_SALE_PRICE      = "PRICE";
            $TAG_HD_SALE_TOTAL      = "TOTAL SALE";
            $TAG_HD_SALE_UNIT_COST  = "UNIT COST";
            $TAG_HD_SALE_TOAL_COST  = "TOTAL_COST";
            $TAG_HD_SALE_PROFIT     = "PROFIT (LOSS)";

            //         
            $current_tag        = "";    
                
            $state = -1;
            $state_keys = array(0=>'TRANS.DATE',1=>'VAT', 2 => "*** Total ***", 3=>'TOTAL MEA-EQ');
            $prev_key = '';
            $rowindex = 0;

            $count = 0;
            $status = false;
            /*
            $rootHeader = array();

            $childHeader = array();
            $firstColIndex = 0;
            $header_row = -1;
            */
            // loop rows
            $rowindex = 0;
            $firstColIndex = -1;
            $firstRowIndex = -1;
            $foundHeader = false;
            $foundTitle =false;
            $tag = ''; 

            $period = '';
            foreach($ret as $r => $row) {
                $colindex = 0;
                $SECURITIES_NAME = $nameSHT;
                $SEQ = 0;
                $COMPANY_CODE = '';
                $COMPANY_NAME = '';
                $SECTOR = '';
               
                 // Retrive logged ID
                $user_data = Session::get('user_data');
                
                $CREATE_DATE = date("Y-m-d H:i:s");   
                $CREATE_BY = $user_data->emp_id;      
                $REFERENCE_DATE  = '';                

                // loop each column
                foreach($row as $cols => $value) {
                    if(is_array($value)) {
                        $tag = '';
                        continue;
                    } else {
                       $tag = strtoupper(($value == NULL) ? '' : $value);
                    }

                    // Title Checking
                    //  Report ID: ....
                    //  Report Type: ...
                    //  PORT: MEA-Q....
                    if (strpos($tag, $TAG_REPORT_ID) !== false) {
                        $tag =  $TAG_REPORT_ID;
                    }elseif(strpos($tag, $TAG_REPORT_HD_TYPE) !== false) {
                        $tag =  $TAG_REPORT_HD_TYPE;
                    }elseif (strpos($tag, $TAG_REPORT_HD_PORT) !== false) {
                        $tag =  $TAG_REPORT_HD_PORT;

                    } else {

                    }


                    switch($tag) {
                        case $TAG_REPORT_ID:
                            break;

                        case $TAG_REPORT_HD_TYPE:
                            break;

                        case $TAG_REPORT_HD_PORT:
                            //$var = export()
                            $foundTitle = true;
                            $pos1 = strpos($value, 'PERIOD FROM:');
                            $pos2 = strpos($value, 'TO :');
                            // echo 'POS:' . $pos1 . ' : ' . $pos2 ;
                            if(($pos1 >0) && ($pos2 > $pos1)) {
                                $period_from = substr($value, $pos1+12, 12);
                                $period_to = substr($value, $pos2+4, 15);
                                Log::info(__FUNCTION__. ' :: FROM:' . $period_from . ' TO: ' . $period_to ); 
                                //echo trim($period_from) . ' -> ' .  trim($period_to);
                                $status=true;
                                $period  = $period_from .' - '. $period_to;
                                // $foundHeader = true;
                            }
                            break;

                        case $TAG_HD_TRANS_DATE:
                            $hd = array( $tag => $tag ,                 
                                         'COL' => $colindex ,
                                         'ROW' => $rowindex);  
                            array_push($rootHeader, $hd);             // 0
                            break;

                        case $TAG_HD_SETT_DATE:
                            $hd = array( $tag => $tag ,                 
                                         'COL' => $colindex ,
                                         'ROW' => $rowindex);  
                            array_push($rootHeader, $hd);             // 1
                            break;

                        case $TAG_HD_PURCSEC_CODE:
                            $hd = array( $tag => $tag ,                 
                                         'COL' => $colindex ,
                                         'ROW' => $rowindex);  
                            array_push($rootHeader, $hd);             // 2
                            break;   

                        case $TAG_HD_BROKER_NAME:
                            $hd = array( $tag => $tag ,                 
                                         'COL' => $colindex ,
                                         'ROW' => $rowindex);  
                            array_push($rootHeader, $hd);             // 3
                            break;   

                        case $TAG_HD_PURCHASE_UNITS:
                            if(!$rootHeader['UNITS-PURCHASE']) {
                                $hd = array( 'UNITS-PURCHASE' => 'UNITS-PURCHASE' ,                 
                                             'COL' => $colindex ,
                                             'ROW' => $rowindex); 
                            } else {
                                $hd = array('UNITS-SALE' => 'UNITS-SALE' ,                 
                                             'COL' => $colindex ,
                                             'ROW' => $rowindex); 
                            } 
                            array_push($rootHeader, $hd);             // 4
                            break;

                        case $TAG_HD_PURCHASE_PRICE:
                            if(!$rootHeader['PRICE-PURCHASE']) {
                                $hd = array('PRICE-PURCHASE' => 'PRICE-PURCHASE' ,                 
                                         'COL' => $colindex ,
                                         'ROW' => $rowindex);  
                            } else {
                                $hd = array('PRICE-SALE' => 'PRICE-SALE' ,                 
                                         'COL' => $colindex ,
                                         'ROW' => $rowindex); 
                            }
                            array_push($rootHeader, $hd);             // 5
                            break;

                        case $TAG_HD_PURCHASE_TOTAL:
                            
                        case $TAG_HD_BROKERAGE:

                        case $TAG_HD_VAT:
                            $hd = array( $tag => $tag ,                 
                                         'COL' => $colindex ,
                                         'ROW' => $rowindex);  
                            array_push($rootHeader, $hd);             // 8
                            break;
                      
                        //////////////////////////////
                        //    SALE
                        //////////////////////////////
                        case $TAG_HD_SALE_UNITS:
                            
                        case $TAG_HD_SALE_PRICE:
                            
                        case $TAG_HD_SALE_TOTAL:
                            
                        case $TAG_HD_SALE_UNIT_COST:
                            
                        case $TAG_HD_SALE_TOAL_COST:
                            
                        case $TAG_HD_SALE_PROFIT:
                             $hd = array( $tag => $tag ,                 
                                         'COL' => $colindex ,
                                         'ROW' => $rowindex);  
                             array_push($rootHeader, $hd);             // 8
                             break;
                        default:
                            break;         
                    }// switch

                    $colindex++;     
                } // foreach column
                $rowindex++;
                if($foundHeader) 
                    break;
            } // foreach

        }); //  Excel::load
        $filedate = date("Y-m-d H:i:s");
        return array('status' => $status, 'filedate' => $filedate); 
    }

    //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    /**
     * MENU 3. - การ Import  ข้อมูลราคาปิดการซื้อ-ขายหลักทรัพย์รายวัน 
     *  
     */
    /**
     * Download sample xls ( ข้อมูลราคาปิดการซื้อ-ขายหลักทรัพย์รายวัน)  
     */
    public function T2_downloadSampleClosed() {
         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $file = 'contents/sample/closed_08092559.xlsx';
        $newfile =  'closed_08092559.xlsx';
        $headers = array(
            'Content-Type: application/pdf',
        );
        return \Response::download($file, $newfile, $headers);
    }
     /**
     *  Check uploaded file format
     *  @param  request $request
     *  @return json with status, details
     */
    public function T2_checkClosedFile(Request $request) {
        /* limit execution timeout */
        ini_set('max_execution_time', 50000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        Log::info(get_class($this) .'::'. __FUNCTION__. 
                  ' datatype: ' . $request->input('datatype') .  " ," .
                  ' broker:' .  $request->input('broker'));
        
        $results = null;
        $error_msg = '';
        $htmlResult = '';

        $passedCount = 1;
        $count = 0;
        $passedCount = 1;

        $datatype = $request->input('datatype'); // $request->get('datatype');
        $broker = $request->input('broker'); 
        $nameSHT = $broker;
        $file = $request->file('exelimport');

        $clientOriginalFileName = $request->file('exelimport')->getClientOriginalName();

        $process_file = '60_2_check_closed_import.xls';
        $request->file('exelimport')->move(storage_path() . '/public/import/', $process_file);
       
        $inputfile = storage_path('/public/import/' . $process_file);

        Log::info('clientOriginalFileName:' . $clientOriginalFileName);
        
        $status = true;
        
        $checkOnly  = true;

        //$arr = $this->T2_importBrokerageFile($request);
        //$status = $arr['status'];
        //$filedate = $arr['filedate']; 

        ///////////////////////////////////////////////////////////////////////////////////////////
        $firstrow = '';
        $filedate = '';      
        $arr = array();  
        $tmp = strtoupper($nameSHT);
        $arr = $this->importClosedFile($inputfile, $clientOriginalFileName, true, $nameSHT);
        $status = $arr['status'];
        $filedate = $arr['filedate']; 

       /* if(strpos($tmp, 'UOB') >=0) {   
          $arr = $this->importBrokerageUOBAMFile($inputfile, $clientOriginalFileName, false, $nameSHT);
          $status = $arr['status'];
          $filedate = $arr['filedate']; 
        } elseif (strpos($tmp, 'SCB') >=0) { 
          $arr = $this->importBrokerageUOBAMFile($inputfile, $clientOriginalFileName, false, $nameSHT);
          $status = $arr['status'];
          $filedate = $arr['filedate']; 
        } else {
          $status = false;
        }
      */
        Log::info(get_class($this) .'::'. __FUNCTION__. ($status ? "ข้อมูลถูกต้อง กดปุ่ม นำเข้าข้อมูล เพื่อนำข้อมูลเข้าระบบ" : "มีข้อผิดพลาด") ); 
        
        //////////////////////////////////
        return response()->json(array('success' =>  $status, 'html'=>'' . 
                '<br/> file :' . $clientOriginalFileName .
                '<br/> status : ' .   ($status ? "ข้อมูลถูกต้อง กดปุ่ม นำเข้าข้อมูล เพื่อนำข้อมูลเข้าระบบ" : "มีข้อผิดพลาด") . 
                '<br/> date : '     . $filedate . " <br/>" ));
        
    } // T2_checkClosedFile

   
    /**
     * Import Closed - Menu 3.
     */
     public function T2_importClosedFile(Request $request) {
        /* limit execution timeout */
        ini_set('max_execution_time', 50000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        Log::info(get_class($this) .'::'. __FUNCTION__. 
                  ' datatype: ' . $request->input('datatype') .  " ," .
                  ' broker' .  $request->input('broker'));
        
        $results = null;
        $error_msg = '';
        $htmlResult = '';

        $passedCount = 1;
        $count = 0;
        $passedCount = 1;

        $datatype = $request->input('datatype'); // $request->get('datatype');
        $broker = $request->input('broker'); 
        $nameSHT = $broker;
        $file = $request->file('exelimport');

        $clientOriginalFileName = $request->file('exelimport')->getClientOriginalName();

        $process_file = '60_2_closed_import.xls';
        $request->file('exelimport')->move(storage_path() . '/public/import/', $process_file);
       
        $inputfile = storage_path('/public/import/' . $process_file);

        
        
        $status = true;
        ///////////////////////////////////////////////////////////////////////////////////////////
        $firstrow = '';
        $filedate = date("Y-m-d");      
        $arr = array();  
        $tmp = strtoupper($nameSHT);

        $arr = $this->importClosedFile($inputfile, $clientOriginalFileName, false, $nameSHT);
        $status = $arr['status'];
        $filedate = $arr['filedate']; 
        
        Log::info(get_class($this) .'::'. __FUNCTION__. ($status ? "นำเข้าข้อมูลเรียบร้อย" : "มีข้อผิดพลาด") ); 

        //////////////////////////////////
        return response()->json(array('success' =>  $status, 'html'=>'' . 
                '<br/> status : ' .   ($status ? "นำเข้าข้อมูลเรียบร้อย" : "มีข้อผิดพลาด") . 
                '<br/> date : '     . $filedate . " <br/>" ));
        
    } // T2_importClosedFile

    function importClosedFile($inputfile,  $clientOriginalFileName, $checkFileOnly, $nameSHT) {
        $validate_key = 'purchase';
        $results = null;
        $error_msg = '';
        $htmlResult = '';
        $filedate = date('Y-m-d');
        $filedatethai = date('Y-m-d');

        $headers = array();  
        $rootHeader = array();
        $childHeader = array();
        $firstColIndex = 0;
        $header_row = -1;
        
        $ret = Excel::load($inputfile, function($reader) use( &$clientOriginalFileName,
                                                                  &$rootHeader,
                                                                  &$childHeader,
                                                                  &$firstColIndex,
                                                                  &$header_row,
                                                                  &$nameSHT,
                                                                  &$checkFileOnly,
                                                                  &$status,
                                                                  &$count, 
                                                                  &$error_msg,
                                                                  &$filedate ,
                                                                  &$filedatethai,
                                                                  &$htmlResult) {
            // first row not a header
            $reader->noHeading();
            // $reader->ignoreEmpty();
            // $reader->setSeparator('+');
            // $reader->formatDates(true, 'Y/');
            $reader->formatDates(true, 'Y-m-d');
            $results = $reader->get(); 
            $ret = $results->toArray();

            // signature column 
            
            $TAG_HD_SYMBOL          = "symbol";
            $TAG_HD_PCLOSE          = "pclose";
            $TAG_HD_PE              = "p/e";
            $TAG_HD_P_BV            = "p/bv";
            $TAG_HD_DIV_YIELD       = "div_yield";
            $TAG_HD_EPS             = "eps";

            //         
            $current_tag            = "";    
                
            $state = -1;
            $state_keys = array(0=>'TRANS.DATE',1=>'VAT', 2 => "*** Total ***", 3=>'TOTAL MEA-EQ');
            $prev_key = '';
            $rowindex = 0;

            $count = 0;
            $status = false;

            /*
            $rootHeader = array();
            $childHeader = array();
            $firstColIndex = 0;
            $header_row = -1;
            */

            // loop rows
            $rowindex = 0;
            $firstColIndex = -1;
            $firstRowIndex = -1;
            $foundHeader = false;
            $foundTitle = false;
            $tag = ''; 
            $period = '';

            foreach($ret as $r => $row) {
                $colindex = 0;
                
                // Retrive logged ID
                $user_data = Session::get('user_data');
                
                $CREATE_DATE = date("Y-m-d H:i:s");   
                $CREATE_BY = $user_data->emp_id;      
                $REFERENCE_DATE  = '';     

                          

                // loop each column
                foreach($row as $cols => $value) {
                	if(is_array($value)) {
                        $tag = '';
                        continue;
                    } else {
                        $tag = strtolower(($value == NULL) ? '' : $value);
                    }
                    switch($tag) {
                    	case $TAG_HD_SYMBOL:     // "symbol"
                        case $TAG_HD_PCLOSE:     // "pclose"
                        case $TAG_HD_PE:         // "p/e"
                        case $TAG_HD_P_BV:       // "p/bv"
                        case $TAG_HD_DIV_YIELD:  // "div_yield"
                        case $TAG_HD_EPS:        // "eps"
                            $hd = array( $tag => $tag,                 
                                         'COL' => $colindex ,
                                         'ROW' => $rowindex);  
                            array_push($rootHeader, $hd);              // 0
                            $firstColIndex = $colindex;
                            $status = true;
                            $foundHeader = true;
                            Log::info('HEADER:[' . $colindex . ']' . print_r($hd, true)); 
                            break;
                    }
                    $colindex++;
                } // foreach

                $rowindex++;
                if($foundHeader) {
                	$firstRowIndex = $rowindex;
                    break;
                }
            } // row


            // Log::info(print_r($childHeader,  true));

            if(!$checkFileOnly) {
                $ar = $this->saveDB_Closed($firstRowIndex,
                                    $checkFileOnly, 
                                    $ret, 
                                    $rootHeader, 
                                    $childHeader, 
                                    $header_row, 
                                    $firstColIndex, $nameSHT, $clientOriginalFileName);
                $status = $ar['status'];
                
            } else {
               
            }
        });

        $filedate = date("Y-m-d H:i:s");
        return array('status' => $status, 'filedate' => $filedate); 

    }

    
    function saveDB_Closed($firstRowIndex, 
    	    $checkFileOnly, 
            $rows, $rootHeader, 
            $childHeader, $header_row,
            $firstColIndex, 
            $nameSHT, 
            $clientOriginalFileName) {
        
        // Retrive logged ID
        $user_data = Session::get('user_data');
        /* PURCHASE */
        $JOB_ID          = '';   //  0 (Auto field) 
        $SYMBOL          = '';
        $PCLOSE          = 0.0;
        $PE              = 0.0;
        $PBV             = 0.0;
        $DIV_YIELD       = 0.0;
        $EPS             = 0.0; 

        $STATUS          = 0;    // 25
        
        $CREATE_DATE = date("Y-m-d H:i:s");  // 24
        $CREATE_BY = $user_data->emp_id;     // 25
        $REFERENCE_DATE  = '';               // 26

        $TODAY_DATE = date("Y-m-d");  

            
        $current_tag        = "";    
        
        $state = -1;
        $TYPE  = '';   //  4
        $state_keys = array(0=>'PERCHASE', 1=>'SALE', 2=>'DIVIDEND');
        $prev_key = '';
        $rowindex = 0;
        $status = false;
        $section_changed = false;
        
        
        // Retrive logged ID
        Log::info('SESSION: [user_data] =' . $CREATE_BY);

        Log::info('ENTER: saveDB_Closed');
         
        /* extract datetime from file name */ 
        $vf = explode(".", $clientOriginalFileName);
        $pieces = explode("_", $vf[0]);
        if(count($pieces) < 2) {
        	Log::info('ERROR: Invalid file name format, please check file name (eg. closed_08092559.xls)');
        	return array('status' => $status);
        }

        $yyyy = substr($pieces[1], 4);
        $numYear = intval($yyyy); 
        if($numYear > 2500) {
           $numYear -= 543;
        }
        $dd = substr($pieces[1], 0, 2);
        $mm = substr($pieces[1], 2, 2);
        $period = strval($numYear) . '-' . $mm .'-' . $dd;
        
        foreach($rows as $r => $row) {
            $data = array();
            
            $JOB_ID          = date('YmdHis') .'-' .uniqid() ;   //  0 (Auto field) 
            $TRANS_DATE      = '';   //  1
            $REFERENCE       = $clientOriginalFileName;

            $SYMBOL          = '';
	        $PCLOSE          = 0.0;
	        $PE              = 0.0;
	        $PBV             = 0.0;
	        $DIV_YIELD       = 0.0;
	        $EPS             = 0.0; 

            //$STATUS          = 0;    // 24
            $CREATE_DATE     = date("Y-m-d H:i:s");  // 24
            $CREATE_BY       = $user_data->emp_id;   // 25



            $REFERENCE_DATE  = $period . " 00:00:00";   // 26

            
            $colindex = 0;

            if($rowindex < ($firstRowIndex)) {
                $rowindex++;
                continue;
            } 

            // loop each column
            $status = true;
            foreach($row as $cols => $value) {
              
                $tag = trim(strtolower(($value == NULL) ? '' : $value));
                 
            	switch($colindex) {
                    case $rootHeader[0]['COL']:     // 0
        	            $SYMBOL = $value;
        	            break;
                    case $rootHeader[1]['COL']:     // 1
                        $PCLOSE = (double)$tag;
                        break;
                    case $rootHeader[2]['COL']:     // 2
                        $PE = (double)$tag;
                        break;
                    case $rootHeader[3]['COL']:     // 3
                        $PBV = (double)$tag;
                        break;
                    case $rootHeader[4]['COL']:     // 4
                        $DIV_YIELD = (double)$tag;
                        break;
                    case $rootHeader[5]['COL']:     // 5
                        $EPS = (double)$tag;
                        break;

                    default:
                        break;    
                } // switch
                
                $colindex++;

            }// foreach column

            
            $record_insert = array (
                'JOB_ID'          => $JOB_ID,                  //  0 (Auto field) 
                'REFERENCE_DATE'  => $REFERENCE_DATE,          // 
                'SYMBOL'          => $SYMBOL,                  //  1
	            'PCLOSE'          => $PCLOSE,                  //  2
	            'PE'              => $PE,                      //  3
	            'PBV'             => $PBV,                    //  4
	            'DIV_YIELD'       => $DIV_YIELD,               //  5
	            'EPS'             => $EPS,                     //  6
	            'STATUS'          => 1,                        //  7
                'CREATE_DATE'     => $CREATE_DATE,             //  8
                'CREATE_BY'       => $CREATE_BY,               //  9
                'REFERENCE'       => $clientOriginalFileName   // 10
            );

            /******************************************************************************************************************************** */
            /*
            Author        : Chalermpol Chueayen (chalermpols@msn.com)
            Date Modified : 2019-03-19
            Purpose       : Add column MODIFY_DATE, MODIFY_BY into table TBL_P2_EQUITY_CLOSED_INDEX
                            to used for keep modified existing symbol and perion 
            */
            $record_update = array (
            	'REFERENCE_DATE'  => $REFERENCE_DATE,
                'SYMBOL'          => $SYMBOL,                  //  1
	            'PCLOSE'          => $PCLOSE,                  //  2
	            'PE'              => $PE,                      //  3
	            'PBV'             => $PBV,                    //  4
	            'DIV_YIELD'       => $DIV_YIELD,               //  5
	            'EPS'             => $EPS,                     //  6
	            'STATUS'          => 1,                        //  7
                'MODIFY_DATE'     => $CREATE_DATE,             //  8
                'MODIFY_BY'       => $CREATE_BY,               //  9
                'REFERENCE'       => $clientOriginalFileName   // 10
            );
            /******************************************************************************************************************************** */
            
            try
             {  
                /* Check previous record */ 
                $query = " SELECT " . 
                         "     COUNT(SYMBOL) As total " .
                         " FROM ".
                         "     TBL_P2_EQUITY_CLOSED_INDEX  " . 
                         " WHERE " .
                         "     SYMBOL = '" . $SYMBOL . "' " .
                         "     AND REFERENCE_DATE = '" . $period . "'";

                Log::debug('Check previous record -> ' . $query);

                $all = DB::select(DB::raw($query));
                $total =  $all[0]->total;

                if($total <= 0) {
                    if(!$checkFileOnly) {
                	    // INSERT NEW RECORD
                        array_push($data, $record_insert);
                        Log::info('row['. $rowindex . '] INSERT RECORD:  ' . print_r($record_insert, true));
                        $affected = DB::table('TBL_P2_EQUITY_CLOSED_INDEX')->insert($data);
                        $status = ($affected > 0);
                        Log::info('row['. $rowindex . '] INSERT RECORD STATUS:  ' . $status);
                    }

                } else {
                    if(!$checkFileOnly) {
                	    // UPDATE EXISTING RECORD
                	    Log::info('row['. $rowindex . '] UPDATE EXISTING RECORD:  ' . print_r($record_update, true));
                	    // TRANS_DATE, SYMBOL, SECURITIES_NAME
                	    array_push($data, $record_update);
                	    $affected = DB::table('TBL_P2_EQUITY_CLOSED_INDEX')
                	                                ->where('SYMBOL' ,'=', $SYMBOL)
                	                                ->where('REFERENCE_DATE' ,'=', $period)
                	                                ->update($record_update);
                        $status = ($affected > 0);
                        Log::info('row['. $rowindex . '] UPDATE RECORD STATUS:  ' . $status);
                    }
                }

                if(!$status) {
                    return array('status' => $status);
                }
            }
            catch (Illuminate\Database\QueryException $e) {
                $status = false;
                $errorCode = $e->errorInfo[1];
                Log::error('QueryException:' . $e->getMessage());
                if($errorCode == 1062) {
                    Log::error('QueryException: We have a duplicate entry problem.');
                }
            } catch(\Exception $e) {
                $status = false;
                Log::error('Exception:' . $e->getMessage());
            }

            $rowindex++;
        } // row
        
        return array('status' => $status); 
    }

    // END: T2_importClosedFile Menu 3. ข้อมูลราคาปิดการซื้อ-ขายหลักทรัพย์รายวัน 
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>




    //////////////////////////////////////////////////
    /**
     * MENU 7. ข้อมูลรายละเอียดผลประโยชน์การลงทุน
     */   
    public function T2_downloadSampleGainLoss() {
         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $file = 'contents/sample/SecuritiesName_Gain_Loss_DDMMYYYY.xls';
        $newfile =  'SecuritiesName_Gain_Loss_DDMMYYYY.xls';
       //    $file = 'contents/sample/t2_sample_bond.xlsx';
        $headers = array(
            'Content-Type: application/pdf',
        );
        return \Response::download($file, $newfile, $headers);
    }

     /**
      *  T2_checkGainLoss 
      *  Check uploaded file format
      *  @param  request $request
      *  @return json with status, details
      */
    public function T2_checkGainLoss(Request $request) {
        /* limit execution timeout */
        ini_set('max_execution_time', 50000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        Log::info(get_class($this) .'::'. __FUNCTION__. 
                  ' datatype: ' . $request->input('datatype') .  " ," .
                  ' broker' .  $request->input('stockuniversebroker'));
        
        $results = null;
        $error_msg = '';
        $htmlResult = '';

        $passedCount = 1;
        $count = 0;
        $passedCount = 1;

        $datatype = $request->input('datatype'); // $request->get('datatype');
        $broker = $request->input('broker');     // บริษัทจัดการ (Securities Name $NAME_SHT)
        $period = $request->input('period');     // 2017-01-31
       
        $nameSHT = $broker;
        
        $file = $request->file('exelimport');

        $clientOriginalFileName = $request->file('exelimport')->getClientOriginalName();

        $process_file = '60_2_check_gain_loss_import.xls';
        $request->file('exelimport')->move(storage_path() . '/public/import/', $process_file);
       
        $inputfile = storage_path('/public/import/' . $process_file);

        Log::info('name' . $nameSHT .', period:' . $period. ',clientOriginalFileName:' . $clientOriginalFileName);
        
        
        ///////////////////////////////////
        $firstrow = '';
        $filedate = '';
      
        $arr = array();  
        $checkOnly  = true;
        $status = (strlen($nameSHT) > 0); 
        $filedate = date('Y-m-d');
        $arr = $this->importGainLoss($inputfile, $clientOriginalFileName, $checkOnly, $nameSHT, $period);
        $status = $arr['status'];
        $filedate = $period; 


        //$filedate = date('Y-m-d');

        Log::info(get_class($this) .'::'. __FUNCTION__. ($status ? "ข้อมูลถูกต้อง กดปุ่ม นำเข้าข้อมูล เพื่อนำข้อมูลเข้าระบบ" : "มีข้อผิดพลาด") ); 
        

        //////////////////////////////////
        return response()->json(array('success' =>  $status, 'html'=>'' . 
                '<br/> ไฟล์ : ' . $clientOriginalFileName .
                '<br/> เวลาตามชื่อไฟล์ : ' . $period .
                '<br/> บริษัท จัดการ : ' . $nameSHT .
                '<br/> status : ' .   ($status ? "ข้อมูลถูกต้อง กดปุ่ม นำเข้าข้อมูล เพื่อนำข้อมูลเข้าระบบ" : "มีข้อผิดพลาด") . 
                '<br/>' ));
        
    } // T2_checkGainLoss

    public function T2_importGainLoss(Request $request) {
        /* limit execution timeout */
        ini_set('max_execution_time', 50000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        Log::info(get_class($this) .'::'. __FUNCTION__. 
                  ' datatype: ' . $request->input('datatype') .  " ," .
                  ' broker' .  $request->input('stockuniversebroker'));
        
        $results = null;
        $error_msg = '';
        $htmlResult = '';

        $passedCount = 1;
        $count = 0;
        $passedCount = 1;

        $datatype = $request->input('datatype'); // datatype 
        $broker = $request->input('broker');     // broker 
        $period = $request->input('period');     // 2017-01-31
        $nameSHT = $broker;
        $file = $request->file('exelimport');

        $clientOriginalFileName = $request->file('exelimport')->getClientOriginalName();

        // $process_file = $clientOriginalFileName;  
        $process_file = '60_2_check_gain_loss_import.xls';
        $request->file('exelimport')->move(storage_path() . '/public/import/', $process_file);
       
        $inputfile = storage_path('/public/import/' . $process_file);

        Log::info('clientOriginalFileName:' . $clientOriginalFileName);
        
        $status = true;
        ///////////////////////////////////
        $firstrow = '';
        $filedate = '';
      
        $arr = array();  
        $checkOnly  = false;

        $arr = $this->importGainLoss($inputfile, $clientOriginalFileName, $checkOnly, $nameSHT, $period);

        $status = $arr['status'];
        $filedate = $arr['filedate']; 

       // $status = true;
       // $filedate = date('Y-m-d');

        Log::info(get_class($this) .'::'. __FUNCTION__. ($status ? "นำเข้าข้อมูลเรียบร้อย" : "มีข้อผิดพลาด") ); 
        

        //////////////////////////////////
        return response()->json(array('success' =>  $status, 'html'=>'' . 
                '<br/> file :' . $clientOriginalFileName .
                '<br/> status : ' .   ($status ? "นำเข้าข้อมูลเรียบร้อย" : "มีข้อผิดพลาด") . 
                '<br/> date : '     . $filedate . " <br/>" ));
        
    } // T2_importGainLoss

    public function importGainLoss($inputfile, $clientOriginalFileName, $checkOnly, $nameSHT, $period) {
        $validate_key = 'symbol';
        $validate_key = 'value';
        $results = null;
        $error_msg = '';
        $htmlResult = '';
        $filedate = date('Y-m-d');
        $filedatethai = date('Y-m-d');

        $headers = array();  
        $rootHeader = array();
        $childHeader = array();
        $firstColIndex = 0;
        $header_row = -1;
        $status = false;

        $retdata = Excel::load($inputfile, function($reader) use( &$clientOriginalFileName,
                                                                  &$rootHeader,
                                                                  &$childHeader,
                                                                  &$firstColIndex,
                                                                  &$header_row,
                                                                  &$nameSHT,
                                                                  &$checkOnly,
                                                                  &$status,
                                                                  &$count, 
                                                                  &$error_msg,
                                                                  &$filedate ,
                                                                  &$filedatethai,
                                                                  &$period,
                                                                  &$nameSHT,
                                                                  &$htmlResult) {
            // first row not a header
            $reader->noHeading();
            // $reader->ignoreEmpty();
            // $reader->setSeparator('+');
            // $reader->formatDates(true, 'Y/');
            $reader->formatDates(true, 'Y-m-d');
            $results = $reader->get(); 
            $ret = $results->toArray();
            

            // signature column header2
            $TAG_HD_SYMBOL       = "symbol";
            $TAG_HD_VALUE        = "value";
            $rowindex = 0;
            $firstColIndex = -1;
            $firstRowIndex = -1;
            $foundHeader = false;
            

            // retrive logged ID
            $user_data = Session::get('user_data');
            $CREATE_DATE = date("Y-m-d H:i:s");   
            $CREATE_BY = $user_data->emp_id;      
            $REFERENCE_DATE  = $period;  

            foreach($ret as $r => $row) {
                $colindex = 0;
                $SECURITIES_NAME = $nameSHT;
                 
                $FSYMBOL = '';
                $FVALUE = '';
               

                
                $JOB_ID          = date('YmdHis') .'-' .uniqid() ;   //  0 (Auto field)               

                $bExitLoop = false;
                // loop each column
                foreach($row as $cols => $value) {
                    if(is_array($value)) {
                        $tag = '';
                        $colindex++;
                        continue;
                    }
                    $tag = trim(strtolower(($value == NULL) ? '' : $value));
                 
                    switch($tag) {
                        case $TAG_HD_SYMBOL:
                            $firstColIndex = $colindex;
                            break;

                        case $TAG_HD_VALUE:
                            $foundHeader = true;
                            $firstRowIndex = $rowindex + 1;
                            $bExitLoop = true;
                            break;

                        default:
                            break;    
                    }
                    if($bExitLoop) { 
                        $colindex++; 
                        break;
                    }
                    
                    if(($foundHeader) && ($firstRowIndex > -1)) {
                        switch($colindex){
                            case $firstColIndex:
                               $FSYMBOL = $value;
                               break;

                            case ($firstColIndex+1):
                               $FVALUE = $value;
                               Log::info('SYMBOL: ['. $FSYMBOL . '] , VALUE: [' . $FVALUE . ']'); 
                               $status = true;
                               break;

                            default:
                               break;      
                        }
                        
                        if(!$checkOnly && (strlen($FSYMBOL) > 0) && (strlen($FVALUE) > 0)) {

                            try {  

                                // save to DB
                                $record_insert = array(
                                    'JOB_ID'          => $JOB_ID,                  //  0  
                                    'REFERENCE_DATE'  => $period,                  //  1
                                    'SYMBOL'          => $FSYMBOL,                 //  2
                                    'VALUE'           => $FVALUE,                  //  3
                                    'SECURITIES_NAME' => $nameSHT,                 //  4
                                    'CREATE_DATE'     => $CREATE_DATE,             //  5 
                                    'CREATE_BY'       => $CREATE_BY,               //  6
                                    'REFERENCE'       => $clientOriginalFileName   //  7
                                );

                                $record_update = array(
                                    'REFERENCE_DATE'  => $period,                  //  1
                                    'VALUE'           => $FVALUE,                  //  3
                                    'SECURITIES_NAME' => $nameSHT,                 //  4
                                    'CREATE_DATE'     => $CREATE_DATE,             //  5 
                                    'CREATE_BY'       => $CREATE_BY,               //  6
                                    'REFERENCE'       => $clientOriginalFileName   //  7
                                );
                                /* Check previous record */ 
                                $query = " SELECT " . 
                                         "     COUNT(SYMBOL) As total " .
                                         " FROM ".
                                         "     TBL_P2_EQUITY_GAIN_LOSS  " . 
                                         " WHERE " .
                                         "     SYMBOL = '" . $FSYMBOL . "' AND ".
                                         "     SECURITIES_NAME = '" . $nameSHT . "' AND ".
                                         "     REFERENCE_DATE='" . $period . "' " ;

                                $all = DB::select(DB::raw($query));
                                $total =  $all[0]->total;

                                if($total <= 0) { 
                                    $affected = DB::table('TBL_P2_EQUITY_GAIN_LOSS')->insert($record_insert);
                                    Log::info('row['. $rowindex . '] INSERT RECORD:  ' . print_r($record_insert, true));
                                } else {
                                    $affected = DB::table('TBL_P2_EQUITY_GAIN_LOSS')
                                                    ->where('SYMBOL' ,'=', $FSYMBOL)
                                                    ->where('SECURITIES_NAME','=', $nameSHT)
                                                    ->where('REFERENCE_DATE' ,'=', $period)
                                                    ->update($record_update);
                                    Log::info('row['. $rowindex . '] UPDATE RECORD:  ' . print_r($record_update, true));
                                }
                                $status = ($affected > 0);

                            }
                            catch (Illuminate\Database\QueryException $e) {
                                $errorCode = $e->errorInfo[1];
                                Log::info('QueryException:' . $e->getMessage());
                                if($errorCode == 1062) {
                                    Log::info('QueryException: We have a duplicate entry problem.');
                                }
                            }
                            catch(\Exception $e) {
                                Log::info('Exception:' . $e->getMessage());
                            }

                        } // checkonly
                    }
                    $colindex++;
                }
                $rowindex++;
            }
        }); // Excel

        $filedate = date("Y-m-d H:i:s");
        return array('status' => $status, 'filedate' => $filedate); 
    }

    // END:  Menu 7. ข้อมูลรายละเอียดผลประโยชน์การลงทุน 
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


 
    //////////////////////////////////////////////////
    /**
     * MENU 8. Import Stock Universe
     */

    public function T2_downloadSampleStockUniverse() {
         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $file = 'contents/sample/UOBAM_Stock universe_1H2017.xls';
        $newfile =  'UOBAM_Stock universe_1H2017.xls';
       //    $file = 'contents/sample/t2_sample_bond.xlsx';
        $headers = array(
            'Content-Type: application/pdf',
        );
        return \Response::download($file, $newfile, $headers);
    }


     /**
     *  Check uploaded file format
     *  @param  request $request
     *  @return json with status, details
     */
    public function T2_checkStockUniverseFile(Request $request) {
        /* limit execution timeout */
        ini_set('max_execution_time', 50000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        Log::info(get_class($this) .'::'. __FUNCTION__. 
                  ' datatype: ' . $request->input('datatype') .  " ," .
                  ' broker' .  $request->input('stockuniversebroker'));
        
        $results = null;
        $error_msg = '';
        $htmlResult = '';

        $passedCount = 1;
        $count = 0;
        $passedCount = 1;

        $datatype = $request->input('datatype'); // $request->get('datatype');
        $broker = $request->input('stockuniversebroker');  // // บริษัทจัดการ (Securities Name $NAME_SHT)
        $file_security_name = $request->input('file_security_name');
        $period_half_year = $request->input('period_half_year');
        $period_year = $request->input('period_year');

        //dataimport.append('period_half_year', period_half_year); // 1st or 2rd half of the year ?
        //        dataimport.append('period_year', period_year);

        $nameSHT = $broker;
        $validate = ($file_security_name == $nameSHT);
        $file = $request->file('exelimport');

        $clientOriginalFileName = $request->file('exelimport')->getClientOriginalName();

        $process_file = '60_2_check_stock_universe_import.xls';
        $request->file('exelimport')->move(storage_path() . '/public/import/', $process_file);
       
        $inputfile = storage_path('/public/import/' . $process_file);

        Log::info('period_half_year:' . $period_half_year .', year:' . $period_year. ',clientOriginalFileName:' . $clientOriginalFileName);
        
        $status = $validate;
        ///////////////////////////////////
        $firstrow = '';
        $filedate = '';
      
        $arr = array();  
        $checkOnly  = true;

       /* $arr = $this->importPurchaseAndSale($inputfile, $clientOriginalFileName, $checkOnly, $nameSHT);
        $status = $arr['status'];
        $filedate = $arr['filedate']; 
*/

        $filedate = date('Y-m-d');

        Log::info(get_class($this) .'::'. __FUNCTION__. ($status ? "ข้อมูลถูกต้อง กดปุ่ม นำเข้าข้อมูล เพื่อนำข้อมูลเข้าระบบ" : "มีข้อผิดพลาด") ); 
        

        //////////////////////////////////
        return response()->json(array('success' =>  $status, 'html'=>'' . 
                '<br/> file : ' . $clientOriginalFileName .
                '<br/> period_half_year : ' . $period_half_year .
                '<br/> year : ' . $period_year .
                '<br/> securities name : ' . $nameSHT .
                '<br/> status : ' .   ($status ? "ข้อมูลถูกต้อง กดปุ่ม นำเข้าข้อมูล เพื่อนำข้อมูลเข้าระบบ" : "มีข้อผิดพลาด") . 
                '<br/> date : '     . $filedate . " <br/>" ));
        
    } // T2_checkStockUniverseFile

    public function T2_ImportStockUniverseFile(Request $request) {
        /* limit execution timeout */
        ini_set('max_execution_time', 50000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        Log::info(get_class($this) .'::'. __FUNCTION__. 
                  ' datatype: ' . $request->input('datatype') .  " ," .
                  ' broker' .  $request->input('stockuniversebroker'));
        
        $results = null;
        $error_msg = '';
        $htmlResult = '';

        $passedCount = 1;
        $count = 0;
        $passedCount = 1;

        $datatype = $request->input('datatype'); // $request->get('datatype');
        $broker = $request->input('stockuniversebroker'); 
        $file_security_name = $request->input('file_security_name');
        $period_half_year = $request->input('period_half_year');  // 1H or 2H
        $period_year = $request->input('period_year');   // 2017

        $nameSHT = $broker;
        $file = $request->file('exelimport');

        $clientOriginalFileName = $request->file('exelimport')->getClientOriginalName();

        $process_file = $clientOriginalFileName; //'60_2_check_stock_universe_import.xls';
        $request->file('exelimport')->move(storage_path() . '/public/import/', $process_file);
       
        $inputfile = storage_path('/public/import/' . $process_file);

        Log::info('clientOriginalFileName:' . $clientOriginalFileName);
        
        $status = true;
        ///////////////////////////////////
        $firstrow = '';
        $filedate = '';
      
        $arr = array();  
        $checkOnly  = false;

        $arr = $this->importStockUniverse($inputfile, $clientOriginalFileName, 
                      $checkOnly, $nameSHT, 
                      $file_security_name,
                      $period_half_year,
                      $period_year);
        /*
        $inputfile, $clientOriginalFileName, 
                      $checkOnly, $nameSHT, 
                      $file_security_name,
                      $period_half_year,
                      $period_year
        */
        $status = $arr['status'];
        $filedate = $arr['filedate']; 

         
        $filedate = date('Y-m-d');

        Log::info(get_class($this) .'::'. __FUNCTION__. ($status ? "นำเข้าข้อมูลเรียบร้อย" : "มีข้อผิดพลาด") ); 
        

        //////////////////////////////////
        return response()->json(array('success' =>  $status, 'html'=>'' . 
                '<br/> file :' . $clientOriginalFileName .
                '<br/> status : ' .   ($status ? "นำเข้าข้อมูลเรียบร้อย" : "มีข้อผิดพลาด") . 
                '<br/> date : '     . $filedate . " <br/>" ));
        
    } // T2_ImportStockUniverseFile

    public function importStockUniverse($inputfile, $clientOriginalFileName, 
                      $checkOnly, $nameSHT, 
                      $file_security_name,
                      $period_half_year,
                      $period_year) {
        $validate_key = 'purchase';
        $results = null;
        $error_msg = '';
        $htmlResult = '';
        $filedate = date('Y-m-d');
        $filedatethai = date('Y-m-d');

        $headers = array();  
        $rootHeader = array();
        $childHeader = array();
        $firstColIndex = 0;
        $header_row = -1;
        //&$validate_key,

        $retdata = Excel::load($inputfile, function($reader) use( &$clientOriginalFileName,
                                                                  &$rootHeader,
                                                                  &$childHeader,
                                                                  &$firstColIndex,
                                                                  &$header_row,
                                                                  &$nameSHT,
                                                                  &$checkFileOnly,
                                                                  &$status,
                                                                  &$count, 
                                                                  &$error_msg,
                                                                  &$filedate ,
                                                                  &$filedatethai,

                                                                  &$period_half_year,
                                                                  &$period_year,
                                                                  &$file_security_name,


                                                                  &$htmlResult) {
            // first row not a header
            $reader->noHeading();
            // $reader->ignoreEmpty();
            // $reader->setSeparator('+');
            // $reader->formatDates(true, 'Y/');
            $reader->formatDates(true, 'Y-m-d');
            $results = $reader->get(); 
            $ret = $results->toArray();
            

              // signature column header2
            $TAG_HD_SEQ          = "seq";
            $TAG_HD_COMPANY_CODE = "company code";
            $TAG_HD_COMPANY_NAME = "company name";   
            $TAG_HD_SECTOR       = "sector";
            

            $rowindex = 0;
            $firstColIndex = -1;
            $firstRowIndex = -1;
            $foundHeader = false;

            foreach($ret as $r => $row) {
                $colindex = 0;
                $SECURITIES_NAME = $nameSHT;
                $SEQ = 0;
                $COMPANY_CODE = '';
                $COMPANY_NAME = '';
                $SECTOR = '';
               
                 // Retrive logged ID
                $user_data = Session::get('user_data');
                
                $CREATE_DATE = date("Y-m-d H:i:s");   
                $CREATE_BY = $user_data->emp_id;      
                $REFERENCE_DATE  = '';                

                // loop each column
                foreach($row as $cols => $value) {
                    if(is_array($value)) {
                        $tag = '';
                        continue;
                    } else {
                       $tag = strtolower(($value == NULL) ? '' : $value);
                    }
                    switch($tag) {
                        /// LEVEL : 0
                        case $TAG_HD_SEQ:              
                            $hd = array( $TAG_HD_SEQ => $TAG_HD_SEQ ,                 
                                         'COL' => $colindex ,
                                         'ROW' => $rowindex);  
                            array_push($rootHeader, $hd);              // 0
                            $firstColIndex = $colindex;
                            break;

                        case $TAG_HD_COMPANY_CODE:
                            $hd = array( $COMPANY_CODE => $COMPANY_CODE ,                 
                                         'COL' => $colindex ,
                                         'ROW' => $rowindex);  
                            array_push($rootHeader, $hd);             // 1
                            $firstRowIndex  = $rowindex;
                            $foundHeader = true;
                            break;

                        case $TAG_HD_COMPANY_NAME:
                            $hd = array( $COMPANY_NAME => $COMPANY_NAME ,                 
                                         'COL' => $colindex ,
                                         'ROW' => $rowindex);  
                            array_push($rootHeader, $hd);             // 2
                            break;    

                        case $TAG_HD_SECTOR:
                            $hd = array( $SECTOR => $SECTOR ,                 
                                         'COL' => $colindex ,
                                         'ROW' => $rowindex);  
                            array_push($rootHeader, $hd);             // 3
                            
                            break;

                        default:
                            break;    
                    }

                    $colindex++; 
                }
                $rowindex++;
                if($foundHeader) 
                    break;
            }

            

            // Save DB
            $rowindex = 0;
            foreach($ret as $r => $row) {
                $colindex = 0;
                $readyFlag = false;
                
                if($rowindex <= $firstRowIndex) {
                    $rowindex++;
                    continue;
                }


                $JOB_ID          = date('YmdHis') .'-' .uniqid() ;   //  0 (Auto field) 
                $SECURITIES_NAME = $nameSHT;
                $SEQ = 0;
                $COMPANY_CODE = '';
                $COMPANY_NAME = '';
                $SECTOR = '';
               
                 // Retrive logged ID
                $user_data = Session::get('user_data');
            
                $CREATE_DATE = date("Y-m-d H:i:s");   
                $CREATE_BY = $user_data->emp_id;      
                $REFERENCE  = $clientOriginalFileName;

                $data = array();

                // loop each column
                foreach($row as $cols => $value) {
                    if(is_array($value))  {
                       Log::info('row['. $rowindex . '] ['. $colindex .'] FOUND ARRAY ->'  . print_r($value, true));
                       $tag = '';
                       continue;
                    } else {
                       $tag = strtolower(($value == NULL) ? '' : $value);
                    }
                    switch($colindex) {
                        case $rootHeader[0]['COL']:      // 0  -> SEQ
                           $SEQ = $value;
                           break;

                        case $rootHeader[1]['COL']:     // 1 ->  COMPANY CODE
                           $COMPANY_CODE = $value;
                            $readyFlag = true;
                           break;

                        case $rootHeader[2]['COL']:     // 2 ->  COMPANY NAME
                           $COMPANY_NAME = $value;
                           break;

                        case $rootHeader[3]['COL']:     // 3 ->   SECTOR
                           $SECTOR = $value;
                           $readyFlag = true;
                           break;
                         
                        default:
                           break;       
                    }

                    $colindex++; 
                }

                // Sanity checking    

                if($readyFlag && (!$checkFileOnly)) {
                    
                    try
                    {  
                        // Save to DB
                        $record = array(
                            'JOB_ID'          => $JOB_ID,                  //  0 (Auto field) 
                            'SYMBOL'          => $COMPANY_CODE,            //  2
                            'PERIOD'          => $period_half_year,        //
                            'YEAR'            => $period_year,             // 
                            'NAME_SHT'        => $nameSHT,                 //  5 from drop down (NAME_SHT)
                            'CREATE_DATE'     => $CREATE_DATE,             //  6 
                            'CREATE_BY'       => $CREATE_BY,               //  7
                            'REFERENCE'       => $REFERENCE                //  8
                        );
                        
                        // Log::info('row['. $rowindex . '] ' . print_r($record, true));  
                        array_push($data, $record);

                        // Log::info('row['. $rowindex . '] >>> ' . print_r($record, true));
                        DB::table('TBL_P2_STOCK_UNIVERSE')->where('REFERENCE', '=', $REFERENCE)
                                                          ->where('SYMBOL',    '=', $COMPANY_CODE)->delete();
                                                          //->where('NAME_SHT',  '=', $nameSHT)
                                                          //->where('YEAR',      '=', $period_year)
                                                          //->where('PERIOD',    '=', $period_half_year)->delete();

                        $affected = DB::table('TBL_P2_STOCK_UNIVERSE')->insert($data);

                        $status = ($affected > 0);
                        
                        // Log::info('row['. $rowindex . '] INSERT RECORD STATUS:  ' . $status);
                    }
                    catch (Illuminate\Database\QueryException $e) {
                        $errorCode = $e->errorInfo[1];
                        Log::info('QueryException:' . $e->getMessage());
                        if($errorCode == 1062) {
                            Log::info('QueryException: We have a duplicate entry problem.');
                        }
                    } catch(\Exception $e) {
                        Log::info('Exception:' . $e->getMessage());
                    }
                }
                $rowindex++;
            }

        });  //Excel::load
        
        $filedate = date("Y-m-d H:i:s");
        return array('status' => $status, 'filedate' => $filedate); 

        /*
        return response()->json(array('success' =>  $status, 'html'=>'' . 
                '<br/> file : ' . $clientOriginalFileName .
                '<br/> period_half_year : ' . $period_half_year .
                '<br/> year : ' . $period_year .
                '<br/> securities name : ' . $nameSHT .
                '<br/> status : ' .   ($status ? "นำเข้าข้อมูลเรียบร้อย" : "มีข้อผิดพลาด") . 
                '<br/> date : '     . $filedate . " <br/>" ));
        */
    }
    
    function saveDBStockUniverse($rows, $rootHeader, $childHeader, $firstRowIndex, $firstColIndex, $nameSHT) {

    }



    /**
    *  FROM [4] เป็นเมนูสำหรับนําเข้าข้อมูล มูลค่า หน่วยลงทุน และอัตราผลตอบแทนเข้าสู่ระบบกองทุน ฯ 
    */
    public function T2_getindexEquityUnitValIndex()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 60,
            'menu_id' => 1,
            'title' => getMenuName($data, 60, 1) . ' | MEA'
        ] );

        Log::info('EquitycompanyManagement::getindexEquityIndex() => ' . getMenuName($data, 60, 1));

        $symbols =  DB::table('TBL_P2_EQUITY_INDEX')->orderby("SYMBOL")->get(); 
    
        return view('backend.pages.p2_equity_unit_val_page')->with(['symbols' => $symbols]);
    }

    public function getCountAllEquityUnitVal($ArrParam) 
    {
        /*Log::info('EquityDataImportController::getCountAllEquityUnitVal()');
        return DB::table('TBL_P2_EQUITY_UNIT_VAL')->orderby("REFERENCE_DATE")->get();*/
        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];

        $policy_id       = $ArrParam["investment"];
        $securities_name = $ArrParam["securities"];
        $date_start      = $ArrParam["date_start"];
        $date_end        = $ArrParam["date_end"];

        // default current year
        if($date_start == "")
            $date_state = date('Y-m-d') . " " . "00:00:00";
        if($date_end == "")
            $date_end = date('Y-m-d') . " " . "23:59:59";


        $where = " WHERE (1=1) ";
        if(trim($policy_id) !="") {
            if(strlen($where) > 1) {
                $where .= " AND b.POLICY_ID =" . $policy_id . " ";
            } else {
                $where .= " b.POLICY_ID =" . $policy_id . " ";
            }
        }

        if(trim($securities_name) !="") {
            if(strlen($where) > 1) {
                $where .= " AND e.SECURITIES_NAME ='" . $securities_name . "' ";
            } else {
                $where .= " e.SECURITIES_NAME ='" . $securities_name . "' ";
            }
        }

       /* if((trim($date_start) !="") && (trim($date_end) !=""))  {
            if(strlen($where) > 1) {
                $where .= " AND REFERENCE_DATE BETWEEN '" . $date_start . " 00:00:00' " .
                          " AND '" . $date_end . " 23:59:59' ";
            } else {
                $where .= "  REFERENCE_DATE BETWEEN '" . $date_start . " 00:00:00' " .
                          " AND '" . $date_end . " 23:59:59' ";
            }
        }*/
        if((trim($date_start) !="") && (trim($date_end) !=""))  {
            if(strlen($where) > 1) {
                $where .= " AND b.REFERENCE_DATE BETWEEN '" . $date_start . " 00:00:00' " .
                          " AND '" . $date_end . " 23:59:59' ";
                $where .= " OR e.REFERENCE_DATE BETWEEN '" . $date_start . " 00:00:00' " .
                          " AND '" . $date_end . " 23:59:59' ";          
            } else {
                $where .= " b.REFERENCE_DATE BETWEEN '" . $date_start . " 00:00:00' " .
                          " AND '" . $date_end . " 23:59:59' ";
                $where .= " OR e.REFERENCE_DATE BETWEEN '" . $date_start . " 00:00:00' " .
                          " AND '" . $date_end . " 23:59:59' ";          
            }
        }

        $query =  "SELECT COUNT(SECURITIES_NAME) as total FROM TBL_P2_EQUITY_UNIT_VAL " . $where ;

          $query = " SELECT count(*) as total from (" . 
                 " SELECT e.JOB_ID as JOB1, e.REFERENCE_DATE as REF1, e.POLICY_ID  as E_ID, " .
                 "   e.NAV_B as E_NAV_B, " . 
                 "   e.NAV_UNIT as E_NAV_UNIT, " .
                 "   e.UNIT as E_UNIT, " .
                
                 "   b.REFERENCE_DATE as REF2, " .
                 "   b.POLICY_ID as BOND_ID, " . 
                 "   b.NAV_B As BOND_NAV_B, " . 
                 "   b.NAV_UNIT As BOND_NAV_UNIT, " .
                 "   b.UNIT As BOND_UNIT " . 
                 " FROM TBL_P2_EQUITY_UNIT_VAL e " .
                 " FULL OUTER JOIN TBL_P2_BOND_UNIT_VAL b " .
                 "      ON b.SECURITIES_NAME = e.SECURITIES_NAME " .
                 "         AND b.REFERENCE_DATE = e.REFERENCE_DATE " . $where .
                 " ) CNT ";

        $all = DB::select(DB::raw($query));
        $total =  $all[0]->total;
        return $total;
    }

    public function getDataEquityUnitVal($ArrParam) {
        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];

        $policy_id       = $ArrParam["investment"];
        $securities_name = $ArrParam["securities"];
        $date_start      = $ArrParam["date_start"];
        $date_end        = $ArrParam["date_end"];


        $where = " WHERE (1=1) ";
        if(trim($policy_id) !="") {
            if(strlen($where) > 1) {
                $where .= " AND b.POLICY_ID =" . $policy_id . " ";
            } else {
                $where .= " b.POLICY_ID =" . $policy_id . " ";
            }
        }

        if(trim($securities_name) !="") {
            if(strlen($where) > 1) {
                $where .= " AND e.SECURITIES_NAME ='" . $securities_name . "' ";
            } else {
                $where .= " e.SECURITIES_NAME ='" . $securities_name . "' ";
            }
        }

        // default current year
        if($date_start == "")
            $date_state = date('Y-m-d') . " " . "00:00:00";
        if($date_end == "")
            $date_end = date('Y-m-d') . " " . "23:59:59";

        if((trim($date_start) !="") && (trim($date_end) !=""))  {
            if(strlen($where) > 1) {
                $where .= " AND b.REFERENCE_DATE BETWEEN '" . $date_start . " 00:00:00' " .
                          " AND '" . $date_end . " 23:59:59' ";
                $where .= " OR e.REFERENCE_DATE BETWEEN '" . $date_start . " 00:00:00' " .
                          " AND '" . $date_end . " 23:59:59' ";          
            } else {
                $where .= " b.REFERENCE_DATE BETWEEN '" . $date_start . " 00:00:00' " .
                          " AND '" . $date_end . " 23:59:59' ";
                $where .= " OR e.REFERENCE_DATE BETWEEN '" . $date_start . " 00:00:00' " .
                          " AND '" . $date_end . " 23:59:59' ";          
            }
        }



        $query =  "SELECT * FROM TBL_P2_EQUITY_UNIT_VAL  " . $where . " ORDER BY REFERENCE_DATE OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
        


/*SELECT * FROM Table1
ORDER BY
CASE 
    WHEN Table1.CREATION_DATE = Table1.UPDATE_DATE THEN Table1.CREATION_DATE
    ELSE Table1.UPDATE_DATE
END ASC
*/


        $query = " SELECT e.SECURITIES_NAME as SEC1, e.JOB_ID as JOB1, e.REFERENCE_DATE as REF1, e.POLICY_ID  as E_ID, " .
                 "   e.NAV_B as E_NAV_B, " . 
                 "   e.NAV_UNIT as E_NAV_UNIT, " .
                 "   e.UNIT as E_UNIT, " .
                 "   e.YIELD_MONTH as E_YIELD_MONTH, " .
                 "   e.YIELD_CUMULATIVE as E_YIELD_CUMULATIVE, " .
                 "   b.SECURITIES_NAME as SEC2, " . 
                 "   b.JOB_ID as JOB2, " .
                 "   b.REFERENCE_DATE as REF2, " .
                 "   b.POLICY_ID as B_ID, " . 
                 "   b.NAV_B As B_NAV_B, " . 
                 "   b.NAV_UNIT As B_NAV_UNIT, " .
                 "   b.UNIT As B_UNIT, " . 
                 "   b.YIELD_MONTH as B_YIELD_MONTH, " .
                 "   b.YIELD_CUMULATIVE as B_YIELD_CUMULATIVE " .
                 " FROM TBL_P2_EQUITY_UNIT_VAL e " .
                 " FULL OUTER JOIN TBL_P2_BOND_UNIT_VAL b " .
                 "      ON b.SECURITIES_NAME = e.SECURITIES_NAME " .
                 "         AND b.REFERENCE_DATE = e.REFERENCE_DATE " . $where .

                 " ORDER BY " .
                 "   CASE WHEN convert(datetime, e.REFERENCE_DATE, 103) >= convert(datetime, b.REFERENCE_DATE, 103) " . 
                 "        THEN convert(datetime, e.REFERENCE_DATE, 103) " . 
                 "        ELSE convert(datetime, b.REFERENCE_DATE, 103) " .
                 "   END ASC OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
                  
                // ") TBL ORDER BY REF2, REF1 OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
         Log::info($query);
        return DB::select(DB::raw($query));
    }

    public function Ajax_Index_EquityUnitValIndex(Request $request) 
    {
        Log::info('EquityDataImportController::Ajax_Index_EquityUnitValIndex' . $request);
        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $investment = $request->input('investment');
        $securities = $request->input('securities');

        $date_start = $request->input('date_start');
        $date_end   = $request->input('date_end');

        $ArrParam["pagesize"]   = $PageSize;
        $ArrParam["PageNumber"] = $PageNumber;

        $ArrParam["investment"] = $investment;
        $ArrParam["securities"] = $securities;
        $ArrParam["date_start"] = $date_start;
        $ArrParam["date_end"]   = $date_end;
        
        $totals = $this->getCountAllEquityUnitVal($ArrParam);
        $dataset = $this->getDataEquityUnitVal($ArrParam);

        /*if($Datacount)
           $totals = $totals;
        else
           $totals = 0;
         */

        $htmlPaginate = Paginatre_gen($totals, $PageSize, 'page_click_search', $PageNumber);
    
        Log::info('view(backend.pages.ajax.ajax_p2_equity_unit_val_detail)');
        
        $returnHTML = view('backend.pages.ajax.ajax_p2_equity_unit_val_detail')->with([
            'htmlPaginate'=> $htmlPaginate,
            'data'=>$dataset, 
            'totals' => $totals,
            'PageSize' =>$PageSize,
            'PageNumber' =>$PageNumber
        ])->render();

        //Log::info('EquitycompanyManagement::Ajax_Index_EquityIndex()' . $returnHTML);
        return response()->json(array('success' => true, 'html'=>$returnHTML));

    }

    public function Ajax_EquityUnitValIndexSearchForm(Request $request) 
    {
         Log::info('Ajax_EquityUnitValIndexSearchForm::' . $request);
         $securitiesList = DB::table('TBL_P2_EQUITY_SECURITIES')->orderby("NAME_SHT")->get(); 
         $investmentlist = DB::table('TBL_P2_EQUITY_INVESTMENT_POLICY')->orderby("POLICY_ID")->get();  

         Log::info('Ajax_EquityUnitValIndexSearchForm:: returnHTML');
         $returnHTML = view('backend.pages.ajax.ajax_p2_equity_unit_val_form')->with([
            'investmentlist' => $investmentlist,
            'securitiesList' => $securitiesList
        ])->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML)); 
    }

    /**
     * Handle request delete single/multiple records.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function deleteUnitVal(Request $request)
    {
        $deleted = false;
        $table_id = $request->input('policy_id');
        $arrId = explode(',', $request->input('record_id'));

        Log::info('#1) deleteUnitVal::' . $request);
        $tableName = "TBL_P2_EQUITY_UNIT_VAL";
        if(($table_id == 2) || ($table_id=="2")) {
            $tableName = "TBL_P2_BOND_UNIT_VAL";
        } else { 
            $tableName = "TBL_P2_EQUITY_UNIT_VAL";
        }

       
        $id = $request->input('record_id');
        $id = str_replace("B.", "", $id);
        $id = str_replace("E.", "", $id);
        
        Log::info('#2) table_id =>' . $table_id . ' ::deleteUnitVal:: ' . $tableName . ' -> '. $id);
        $deleted =  DB::table($tableName)->where('JOB_ID',"=", trim($id))->delete(); 
        /*foreach($arrId as $index => $item){
            if($item != "") {
                $id = str_replace("B.", "", $item);
                $id = str_replace("E.", "", $id);
                
                Log::info('#2) deleteUnitVal::' . trim($id));
                $deleted =  DB::table($tableName)->where('JOB_ID',"=", trim($id))->delete();
            }
        }*/

        if($deleted)  {
            return response()->json(["ret" => "1"]);
        } else {
            return response()->json(["ret" => "0"]);
        }
    }

    public function deleteUnitValRecords(Request $request)
    {
        $deleted = false;
        $arrId = explode(',', $request->input('record_id'));

        Log::info('#1) deleteUnitVal::' . $request);
        $tableName = "TBL_P2_EQUITY_UNIT_VAL";
       
        Log::info('#2) table_id =>' . $table_id . ' ::deleteUnitVal:: ' . $tableName . ' -> '. $id);
        $deleted =  DB::table($tableName)->where('JOB_ID',"=", trim($id))->delete(); 
        foreach($arrId as $index => $item){
            if($item != "") {
                $itmarrs = explode(".", $item);
                if($itmarrs[0] =='B') {
                    $tableName = "TBL_P2_BOND_UNIT_VAL";
                } else {
                    $tableName = "TBL_P2_EQUITY_UNIT_VAL";
                }
                $id = str_replace("B.", "", $item);
                $id = str_replace("E.", "", $id);
                
                Log::info('#2) deleteUnitVal::' . trim($id));
                $deleted =  DB::table($tableName)->where('JOB_ID',"=", trim($id))->delete();
            }
        }

        if($deleted)  {
            return response()->json(["ret" => "1"]);
        } else {
            return response()->json(["ret" => "0"]);
        }
    }

    /**
     * Receive POST command to add new TBL_P2_EQUILTY_UNIT_VAL / TBL_P2_BOND_UNIT_VAL data
     * @param $request parameters: 
     *        SECURITIES_NAME, POLICY_ID, NAV_B, NAV_UNIT,
     *        UNIT, YIELD_MONTH, YIELD_CUMULATIVE 
     */
    public function postAddUnitVal(Request $request)
    {
        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');
        
        $ret = false;
        Log::info('#1) ## postAddUnitVal: request =>' . $request);  
        $rdate = $request["reference_date"]; 
      
        $arr = explode("-", $rdate);
        $iyear = intval($arr[0]) - 543;
        if($iyear<1900)
            $datestart = $rdate;
        else
            $datestart = strval($iyear) . "-" . $arr[1] . "-" . $arr[2];

        $dateref  = new Date($datestart);

        Log::info('#2) ## postAddUnitVal: request =>' . $dateref); 
       
        if ($request["reference_date"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันที่อ้างอิง ";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

    
        $table_id = $request->input('policy_id');
        $tableName = "TBL_P2_EQUITY_UNIT_VAL";
        if(($table_id == 2) || ($table_id=="2")) {
            $tableName = "TBL_P2_BOND_UNIT_VAL";
        } else  {
            $tableName = "TBL_P2_EQUITY_UNIT_VAL";
        }

        
        $user_data = Session::get('user_data');
        $CREATE_BY = $user_data->emp_id;     // 25    
        $today   = new Date();
        $data    = array();

        array_push($data, array(
            'JOB_ID'           => uniqid() .'-'. date('Y-m-d'),
            'SECURITIES_NAME'  => $request["name_sht"],
            'REFERENCE_DATE'   => $dateref,
            'POLICY_ID'        => $request["policy_id"],
            'NAV_B'            => doubleval(str_replace(',', '', strval($request["nav_b"]))),
            'NAV_UNIT'         => doubleval(str_replace(',', '', strval($request["nav_unit12"]))),
            'UNIT'             => doubleval(str_replace(',', '', strval($request["unit"]))),
            'YIELD_MONTH'      => doubleval(str_replace(',', '', strval($request["yield_month"]))),
            'YIELD_CUMULATIVE' => doubleval(str_replace(',', '', strval($request["yield_cumulative"]))),
            'BM_MONTH'         => doubleval(str_replace(',', '', strval($request["bm_month"]))),
            'BM_CUMULATIVE'    => doubleval(str_replace(',', '', strval($request["bm_cumulative"]))),
            'CREATE_DATE'      => $today,
            'CREATE_BY'        => $CREATE_BY,
            'MODIFY_DATE'      => $today,
            'MODIFY_BY'        => $CREATE_BY 
        ));
       /* 
        $chk = "SELECT COUNT(JOB_ID) As total FROM ". $tableName . " WHERE JOB_ID = '". $request["name_sht"]. "'";
        $all = DB::select(DB::raw($chk));
        $total =  $all[0]->total;
       */
        $rethtml = "OK";

        try {  
            
            $insert = DB::table($tableName)->insert($data);
            $ret = $insert;
            
            // TODO: update lastdate 
            $dataupdate    = array(
                'BM_MONTH'         => doubleval(str_replace(',', '', strval($request["bm_month"]))),
                'BM_CUMULATIVE'    => doubleval(str_replace(',', '', strval($request["bm_cumulative"])))
            );

            $insert = DB::table($tableName)
                ->where('REFERENCE_DATE', '=', $dateref)
                ->where('POLICY_ID',      '=', $request["policy_id"])
                ->update($dataupdate);

        } catch (\Exception $e) {
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Exception:' . $e->getMessage());
            $rethtml = "มีข้อผิดพลาด วันที่อ้างอิง หรือ บริษัทจัดการ ที่ท่านเลือกมีอยู่ในระบบแล้ว ";
            $ret = false;
        } 
        return response()->json(array('success' => $ret, 'html'=>$rethtml));
    }


    /**
     * Receive POST command to update existing record in TBL_P2_EQUILTY_UNIT_VAL / TBL_P2_BOND_UNIT_VAL 
     * @param $request parameters: 
     *        JOB_ID,
     *        SECURITIES_NAME, POLICY_ID, NAV_B, NAV_UNIT,
     *        UNIT, YIELD_MONTH, YIELD_CUMULATIVE 
     */

    public function postEditsUnitVal(Request $request)
    {
        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $ret = false;
        Log::info('#1) ## postEditsUnitVal: request =>' . $request);  
        $rdate = toEnglishDate($request["reference_date"]);
        // 09 มี.ค. 2560 
        // 2560-03-02
        $arr = explode("-", $rdate);
        $iyear = intval($arr[2]) - 543;
        $datestart = strval($iyear) . "-" . $arr[1] . "-" . $arr[0];

        $dateref  = new Date($datestart);

        Log::info('#2) ## postEditsUnitVal: request =>' . $dateref);  
        $reference_date = $request["reference_date"];

       
        if ($request["reference_date"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันที่อ้างอิง ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

         

        $table_id = $request->input('policy_id');

        $tableName = "TBL_P2_EQUITY_UNIT_VAL";
        if(($table_id == 2) || ($table_id=="2")) {
            $tableName = "TBL_P2_BOND_UNIT_VAL";
        } else  {
            $tableName = "TBL_P2_EQUITY_UNIT_VAL";
        }

        $today = new Date();
        $user_data = Session::get('user_data');
        $CREATE_BY = $user_data->emp_id;     // 25 
        
        $data = array(
            'SECURITIES_NAME'  => $request["name_sht"],
            'REFERENCE_DATE'   => $dateref,
            'POLICY_ID'        => $request["policy_id"],
            'NAV_B'            => doubleval(str_replace(',', '', strval($request["nav_b"]))),
            'NAV_UNIT'         => doubleval(str_replace(',', '', strval($request["nav_unit12"]))),
            'UNIT'             => doubleval(str_replace(',', '', strval($request["unit"]))),
            'YIELD_MONTH'      => doubleval(str_replace(',', '', strval($request["yield_month"]))),
            'YIELD_CUMULATIVE' => doubleval(str_replace(',', '', strval($request["yield_cumulative"]))),
            'BM_MONTH'         => doubleval(str_replace(',', '', strval($request["bm_month"]))),
            'BM_CUMULATIVE'    => doubleval(str_replace(',', '', strval($request["bm_cumulative"]))),
            // 'CREATE_DATE'      => $today,
            // 'CREATE_BY'        => $CREATE_BY,
            'MODIFY_DATE'      => $today,
            'MODIFY_BY'        => $CREATE_BY 
        );

        Log::info('postEditsUnitVal=>TABLE: ' . $tableName . ' , SQL: ' .  print_r($data, true));  
        try {

            $update = DB::table($tableName)->where('JOB_ID', "=", $request["record_id"])->update($data);
            $ret = $update;

             // TODO: update lastdate 
            $dataupdate    = array(
              'BM_MONTH'         => doubleval(str_replace(',', '', strval($request["bm_month"]))),
              'BM_CUMULATIVE'    => doubleval(str_replace(',', '', strval($request["bm_cumulative"])))
            );
    
            $update = DB::table($tableName)
                ->where('REFERENCE_DATE', '=', $dateref)
                ->where('POLICY_ID',      '=', $request["policy_id"])
                ->update($dataupdate);


            $msg = "OK";
            if($ret)  {
                $msg = "บันทึกเรียบร้อย";
            } else {
                $msg = 'มีข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้!';
            }
        } catch (\Exception $e) {
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Exception:' . $e->getMessage());
            $msg = "มีข้อผิดพลาด วันที่อ้างอิง หรือ บริษัทจัดการ ที่ท่านเลือกมีอยู่ในระบบแล้ว ";
            $ret = false;
        } 
        return response()->json(array('success' => $ret, 'html'=> $msg));
    }

    /**
     * Receive GET command to update existing record in TBL_P2_EQUILTY_UNIT_VAL / TBL_P2_BOND_UNIT_VAL 
     * @param $request parameters: 
     *        JOB_ID,
     *        SECURITIES_NAME, POLICY_ID, NAV_B, NAV_UNIT,
     *        UNIT, YIELD_MONTH, YIELD_CUMULATIVE 
     */
    public function getAddUnitVal( )
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 60,
            'menu_id' => 2,
            'title' => getMenuName($data, 60, 2) . ' | MEA'
        ]);

        $securitiesList = DB::table('TBL_P2_EQUITY_SECURITIES')->orderby("NAME_SHT")->get(); 
        $policyList     = DB::table('TBL_P2_EQUITY_INVESTMENT_POLICY')->orderby("POLICY_NAME")->get();
        
        $viewName = "backend.pages.p2_equity_add_unit_val_page";
        return view($viewName)->with(
              ['securitiesList'=>$securitiesList,
               'policyList'=>$policyList]);
    }


    /**
     * Receive GET command to update existing record in TBL_P2_EQUILTY_UNIT_VAL / TBL_P2_BOND_UNIT_VAL 
     * @param $request parameters: 
     *        JOB_ID,
     *        SECURITIES_NAME, POLICY_ID, NAV_B, NAV_UNIT,
     *        UNIT, YIELD_MONTH, YIELD_CUMULATIVE 
     */
    public function getEditUnitVal($id)
    {
        $data = getmemulist();
        $this->pageSetting([
            'menu_group_id' => 60,
            'menu_id' => 2,
            'title' => getMenuName($data, 60, 2) . ' | MEA'
        ]);

        $arrId = explode('.', $id);
        $tableName = ($arrId[0] == 'E') ? "TBL_P2_EQUITY_UNIT_VAL" : "TBL_P2_BOND_UNIT_VAL";

        $viewName = "backend.pages.p2_equity_edit_unit_val_page";


        $securitiesList = DB::table('TBL_P2_EQUITY_SECURITIES')->orderby("NAME_SHT")->get(); 
        $policyList     = DB::table('TBL_P2_EQUITY_INVESTMENT_POLICY')->orderby("POLICY_NAME")->get();
        
        //$viewName = "backend.pages.p2_equity_add_unit_val_page";
        //return view($viewName)->with(
        //      ['securitiesList'=>$securitiesList,
        //       'policyList'=>$policyList]);

        $editdata = DB::table($tableName)->where("JOB_ID" ,"=",  $arrId[1])->first();
        
        if($editdata) {
            return view($viewName)->with(['editdata'=>$editdata, 
                                          'securitiesList'=> $securitiesList,
                                          'policyList'=> $policyList]);
        }
        
        abort(404);
    }

 
    // Route::post('addUnitVal',    'EquityDataImportController@postAddUnitVal');
    //        Route::post('editsUnitVal',  'EquityDataImportController@postEditUnitVal');
    //        Route::post('deleteUnitVal', 'EquityDataImportController@deleteUnitVal');        
           
} // class EquityDataImportController (60-2)
/*
SAMPLE TRANSACTIONS
=====================
DB::transaction(function()
{
    $newAcct = Account::create([
        'accountname' => Input::get('accountname')
    ]);

    $newUser = User::create([
        'username' => Input::get('username'),
        'account_id' => $newAcct->id,
    ]);

    if( !$newUser )
    {
        throw new \Exception('User not created for account');
    }
});
*/
?>