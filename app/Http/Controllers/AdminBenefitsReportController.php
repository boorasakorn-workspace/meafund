<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Jenssegers\Date\Date;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

use App\Libraries\MEAUtils;
use Illuminate\Support\Facades\Log;

/*********************************************************************************************************************************/
//
// Author        : Chalermpol Chueayen (chalermpols@msn.com)
// Date Modified : 2019-05-21
// Purpose       : Implement export excel for 3 reports
/*********************************************************************************************************************************/

class AdminBenefitsReportController extends Controller
{
    public  function  DataSource($ArrParam, $IsCase, $ispageing= true){

        $where = "";
        if($ispageing){
            $PageSize = $ArrParam['pagesize'];
            $PageNumber = $ArrParam['PageNumber'];
        }

        //if($IsCase) 
        //{

        $reporttype  = $ArrParam["reporttype"];
        $category    = $ArrParam["category"];
        $equity_name = $ArrParam["equity_name"];
        $choose_year = $ArrParam["choose_year"];

         /*   $where = " WHERE focus.EMP_ID IS NOT  NULL";

            if(!empty($category)){
                $where .= " AND category = '".$emp_id."'";
            }
            if(!empty($depart)&& $check_depart== "true"){
                $where .= " AND em.DEP_SHT  = '".$depart."'";
            }
            if(!empty($plan)&& $check_plan== "true"){
                $where .= " AND new.USER_SAVING_RATE = '".$plan."'";
            }
            if(!empty($date_start) && !empty($date_end)&& $check_date== "true"){
                $where .= " AND new.CHANGE_SAVING_RATE_DATE  BETWEEN '".$date_start."' AND '".$date_end."'";
            }
        */
        $where = "";

        if(!empty($category)&& strlen($category)> 0) {
            //if(strlen($where) > 0)
               $where .= " AND C.INDUSTRIAL = '".$category."'";
            //else
            //    $where .= " C.INDUSTRIAL = '".$category."'";

        }
        if(!empty($equity_name) && strlen($equity_name)>0){
            //if(strlen($where) > 0)
                $where .= " AND G.SECURITIES_NAME  = '".$equity_name."'";
            //else
            //    $where .= " G.SECURITIES_NAME  = '".$equity_name."'";

        }
        
        if(!empty($choose_year) && strlen($choose_year)>0){
            if($reporttype =="3") {
                $where .= " AND YEAR(G.REFERENCE_DATE) = " . $choose_year;
            }
            else  if($reporttype =="1") { 
                $where .= " AND YEAR(G.TRANS_DATE) = " . $choose_year;
            } 
            else  if($reporttype =="2") {
                $where .= " AND YEAR(G.TRANS_DATE) = " . $choose_year;
            } 
            else {

            }
        }

        $query = "";
        if($reporttype =="3") {
            $query= "" .
            " select C.INDUSTRIAL, I.BU, I.SYMBOL as SYMBOL , " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  1 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U1,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  2 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U2,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  3 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U3,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  4 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U4,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  5 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U5,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  6 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U6,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  7 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U7,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  8 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U8,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  9 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U9,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) = 10 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U10, " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) = 11 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U11, " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) = 12 AND SECURITIES_NAME = 'UOBAM') then value  else 0 end) U12  " .
            ",   " .
            
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  1 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K1,   " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  2 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K2,   " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  3 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K3,   " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  4 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K4,   " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  5 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K5,   " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  6 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K6,   " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  7 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K7,   " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  8 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K8,   " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) =  9 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K9,   " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) = 10 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K10,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) = 11 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K11,  " .
            "    SUM(case WHEN (MONTH(REFERENCE_DATE) = 12 AND SECURITIES_NAME = 'KTAM') then value  else 0 end) K12   ".
            " FROM TBL_P2_EQUITY_INDEX I, " .
            "    TBL_P2_EQUITY_CATEGORY C, " .
            "    TBL_P2_EQUITY_GAIN_LOSS G " .
            " WHERE G.SYMBOL = I.SYMBOL " .
            " AND C.INDUSTRIAL  = I.INDUSTRIAL ".
            " AND C.BU  = I.BU 
            " .$where;
              
            $query .= " group by C.INDUSTRIAL, I.BU, I.SYMBOL " .
                      " ORDER BY INDUSTRIAL, SYMBOL ";
        }
        
        else if($reporttype =="1") {
            $query= "" .
            " select C.INDUSTRIAL, I.BU, I.SYMBOL as SYMBOL , " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  1 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U1,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  2 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U2,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  3 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U3,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  4 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U4,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  5 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U5,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  6 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U6,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  7 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U7,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  8 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U8,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  9 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U9,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 10 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U10, " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 11 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U11, " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 12 AND SECURITIES_NAME = 'UOBAM') then PROFIT_LOSS  else 0 end) U12  " .
            ",   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  1 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K1,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  2 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K2,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  3 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K3,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  4 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K4,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  5 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K5,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  6 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K6,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  7 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K7,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  8 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K8,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  9 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K9,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 10 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K10,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 11 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K11,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 12 AND SECURITIES_NAME = 'KTAM') then PROFIT_LOSS  else 0 end) K12   ".
            " FROM TBL_P2_EQUITY_INDEX I, " .
            "    TBL_P2_EQUITY_CATEGORY C, " .
            "    TBL_P2_EQUITY_TRANS G " .
            " WHERE G.SYMBOL = I.SYMBOL " .
            " AND C.INDUSTRIAL  = I.INDUSTRIAL ".
            " AND C.BU  = I.BU 
            " .$where;
              
            $query .= " group by C.INDUSTRIAL, I.BU, I.SYMBOL " .
                      " ORDER BY INDUSTRIAL, SYMBOL ";
        }
        else if($reporttype =="2") {
            $query= "" .
            " select C.INDUSTRIAL, I.BU, I.SYMBOL as SYMBOL , " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  1 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U1,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  2 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U2,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  3 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U3,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  4 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U4,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  5 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U5,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  6 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U6,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  7 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U7,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  8 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U8,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  9 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U9,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 10 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U10, " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 11 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U11, " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 12 AND SECURITIES_NAME = 'UOBAM') then TOTAL_DIV  else 0 end) U12  " .
            ",   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  1 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K1,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  2 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K2,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  3 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K3,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  4 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K4,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  5 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K5,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  6 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K6,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  7 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K7,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  8 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K8,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) =  9 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K9,   " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 10 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K10,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 11 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K11,  " .
            "    SUM(case WHEN (MONTH(TRANS_DATE) = 12 AND SECURITIES_NAME = 'KTAM') then TOTAL_DIV  else 0 end) K12   ".
            " FROM TBL_P2_EQUITY_INDEX I, " .
            "    TBL_P2_EQUITY_CATEGORY C, " .
            "    TBL_P2_EQUITY_TRANS G " .
            " WHERE G.SYMBOL = I.SYMBOL " .
            " AND C.INDUSTRIAL  = I.INDUSTRIAL ".
            " AND C.BU  = I.BU 
            " .$where;
              
            $query .= " group by C.INDUSTRIAL, I.BU, I.SYMBOL " .
                      " ORDER BY INDUSTRIAL, SYMBOL ";
        } else {

        }
        //// 
        if($ispageing){
            $query .=  " OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
        }

        Log::info(get_class($this) .'::'. __FUNCTION__ . ' - SQL=' . $query);  
        return DB::select(DB::raw($query));
    }

    function ajax_report_search_export() {
        Log::info(get_class($this) .'::'. __FUNCTION__);

        $ArrParam = array();
        $ArrParam["pagesize"] ="";
        $ArrParam["PageNumber"] ="";
        $ArrParam["emp_id"] =Input::get("EmpID");
        $ArrParam["depart"] =Input::get("depart");
        $ArrParam["plan"] =Input::get("plan");
        $ArrParam["date_start"] =Input::get("date_start");
        $ArrParam["date_end"] =Input::get("date_end");
        $ArrParam["check_name"] =Input::get("check_name");
        $ArrParam["check_depart"] =Input::get("check_depart");
        $ArrParam["check_plan"] =Input::get("check_plan");
        $ArrParam["check_date"] =Input::get("check_date");
        $ArrParam["reporttype"]=Input::get("reporttype");
        $ArrParam["category"]    = Input::get("category");
        $ArrParam["equity_name"] = Input::get("equity_name");
        $ArrParam["choose_year"] = Input::get("choose_year");

        $data = $this->DataSource($ArrParam,true,false);
        $results = array();
        foreach ($data as $item) {
            $results[] = (array)$item;
        }
        //Log::debug(print_r($results));

        $reportName = '';
        switch($ArrParam["reporttype"]) {
            case '1':
                $reportTitle = 'สรุปรายละเอียดเงินปันผล จากการซื้อขายหลักทรัพย์';
                $reportName = 'รายงานผลประโยชน์ กำไร-ขาดทุน';
                break;
            case '2':
                $reportTitle = 'สรุปรายละเอียดเงินปันผล จากการซื้อขายหลักทรัพย์';
                $reportName = 'รายงานผลประโยชน์ เงินปันผล';
                break;
            case '3':
                $reportTitle = 'สรุปรายละเอียดส่วนเพิ่ม(ลด) จากการซื้อขายหลักทรัพย์';
                $reportName = 'รายงานผลประโยชน์ ส่วนเพิ่ม-ลด';
                break;
        }

        $ArrParam["reportName"] = $reportName;

        Excel::create('รายงาน'.$reportName, function ($excel) use ($results, $ArrParam) {

            $excel->sheet('รายงาน', function ($sheet) use ($results, $ArrParam){
                // first row styling and writing content
                $sheet->setStyle([
                    'borders' => [
                        'allborders' => [
                            'color' => [
                                'rgb' => 'b3b6b7'
                            ]
                        ]
                    ]
                ]);
                $sheet->mergeCells('A1:AB1'); //กองทุนสำรองเลี้ยงชีพพนักงานการไฟฟ้านครหลวง ซึ่งจดทะเบียนแล้ว
                $sheet->mergeCells('A2:AB2'); //สรุปรายละเอียดกำไร-ขาดทุน จากการซื้อขายหลักทรัพย์
                $sheet->mergeCells('A3:AB3'); //ปี 2562
           
                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Cordia New');
                    $row->setFontSize(20);
                    $row->setFontWeight('bold');
                    $row->setAlignment('center');
                    // $row->setBackground('#b2babb');
                    // Set all borders (top, right, bottom, left)
                    // $row->setBorder('thin', 'thin', 'none', 'thin');

                });

                $sheet->row(2, function ($row) { 
                    $row->setAlignment('center');
                    // $row->setBackground('#e5e8e8');
                    // $row->setBorder('none', 'thin', 'none', 'thin');
                });

                $sheet->row(3, function ($row) { 
                    $row->setAlignment('center');
                    // $row->setBackground('#e5e8e8');
                    // $row->setBorder('none', 'thin', 'thin', 'thin');
                });

                $sheet->row(1, array('กองทุนสำรองเลี้ยงชีพพนักงานการไฟฟ้านครหลวง ซึ่งจดทะเบียนแล้ว'));

                $reportTitle = '';
                switch($ArrParam["reporttype"]) {
                    case '1':
                        $reportTitle = 'สรุปรายละเอียดเงินปันผล จากการซื้อขายหลักทรัพย์';
                        break;
                    case '2':
                        $reportTitle = 'สรุปรายละเอียดเงินปันผล จากการซื้อขายหลักทรัพย์';
                        break;
                    case '3':
                        $reportTitle = 'สรุปรายละเอียดส่วนเพิ่ม(ลด) จากการซื้อขายหลักทรัพย์';
                        break;
                }
                $sheet->row(2, array($reportTitle));
                $reportYear = intval($ArrParam["choose_year"]) + 543;
                $sheet->row(3, array('ปี '.$reportYear));

                $sheet->mergeCells('A4:A6');
                $sheet->cell('A4', function($cell) {
                    $cell->setValue('กลุ่มอุตสาหกรรม');
                    $cell->setAlignment('center');                   
                    $cell->setValignment('center');
                    // $cell->setBackground('#e5e8e8');
                    // $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
                $sheet->getStyle('A4')->getAlignment()->setWrapText(true);
                
                $sheet->mergeCells('B4:B6');
                $sheet->cell('B4', function($cell) {
                    $cell->setValue('หลักทรัพย์');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                    // $cell->setBackground('#e5e8e8');
                    // $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
                $sheet->getStyle('B4')->getAlignment()->setWrapText(true);
                
                $sheet->mergeCells('C4:N4');
                $sheet->cell('C4', function($cell) {
                    $cell->setValue('UOBAM');
                    $cell->setAlignment('center');
                    // $cell->setBackground('#e5e8e8');
                    // $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $sheet->mergeCells('C5:N5');
                $sheet->cell('C5', function($cell) {
                    $cell->setValue('GAIN (LOSS) OF SECURITY INCOME');
                    $cell->setAlignment('center');
                    // $cell->setBackground('#e5e8e8');
                    // $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $sheet->mergeCells('O4:O6');
                $sheet->cell('O4', function($cell) {
                    $cell->setValue('ACCUMULATE');
                    $cell->setAlignment('center');                   
                    $cell->setValignment('center');
                    // $cell->setBackground('#e5e8e8');
                    // $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
                $sheet->getStyle('O4')->getAlignment()->setWrapText(true);

                $sheet->mergeCells('P4:AA4');
                $sheet->cell('P4', function($cell) {
                    $cell->setValue('KTAM');
                    $cell->setAlignment('center');
                    // $cell->setBackground('#e5e8e8');
                    // $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $sheet->mergeCells('P5:AA5');
                $sheet->cell('P5', function($cell) {
                    $cell->setValue('GAIN (LOSS) OF SECURITY INCOME');
                    $cell->setAlignment('center');
                    // $cell->setBackground('#e5e8e8');
                    // $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $sheet->mergeCells('AB4:AB6');
                $sheet->cell('AB4', function($cell) {
                    $cell->setValue('ACCUMULATE');
                    $cell->setAlignment('center');                   
                    $cell->setValignment('center');
                    // $cell->setBackground('#e5e8e8');
                    // $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
                $sheet->getStyle('AB4')->getAlignment()->setWrapText(true);

                $monthAbv[] = Array('ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.');

                $sheet->setCellValue('C6', 'ม.ค.');$sheet->setCellValue('D6', 'ก.พ.');$sheet->setCellValue('E6', 'มี.ค.');
                $sheet->setCellValue('F6', 'เม.ย.');$sheet->setCellValue('G6', 'พ.ค.');$sheet->setCellValue('H6', 'มิ.ย.');
                $sheet->setCellValue('I6', 'ก.ค.');$sheet->setCellValue('J6', 'ส.ค.');$sheet->setCellValue('K6', 'ก.ย.');
                $sheet->setCellValue('L6', 'ต.ค.');$sheet->setCellValue('M6', 'พ.ย.');$sheet->setCellValue('N6', 'ธ.ค.');
                $sheet->setCellValue('P6', 'ม.ค.');$sheet->setCellValue('Q6', 'ก.พ.');$sheet->setCellValue('R6', 'มี.ค.');
                $sheet->setCellValue('S6', 'เม.ย.');$sheet->setCellValue('T6', 'พ.ค.');$sheet->setCellValue('U6', 'มิ.ย.');
                $sheet->setCellValue('V6', 'ก.ค.');$sheet->setCellValue('W6', 'ส.ค.');$sheet->setCellValue('X6', 'ก.ย.');
                $sheet->setCellValue('Y6', 'ต.ค.');$sheet->setCellValue('Z6', 'พ.ย.');$sheet->setCellValue('AA6', 'ธ.ค.');

                $sheet->row(5, function ($row) { 
                    $row->setAlignment('center');
                    // $row->setBackground('#e5e8e8');
                    // $row->setBorder('thin', 'thin', 'thin', 'thin');
                });
                $sheet->row(6, function ($row) { 
                    $row->setAlignment('center');
                    // $row->setBackground('#e5e8e8');
                    // $row->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $sheet->setColumnFormat(array(
                    'C' => '#,##0.00',
                    'D' => '#,##0.00',
                    'E' => '#,##0.00',
                    'F' => '#,##0.00',
                    'G' => '#,##0.00',
                    'H' => '#,##0.00', 
                    'I' => '#,##0.00',  
                    'J' => '#,##0.00',
                    'K' => '#,##0.00',
                    'L' => '#,##0.00',
                    'M' => '#,##0.00',
                    'N' => '#,##0.00',
                    'O' => '#,##0.00',
                    'P' => '#,##0.00',
                    'Q' => '#,##0.00',
                    'R' => '#,##0.00',
                    'S' => '#,##0.00',
                    'T' => '#,##0.00',
                    'U' => '#,##0.00',
                    'V' => '#,##0.00',
                    'W' => '#,##0.00',
                    'X' => '#,##0.00',
                    'Y' => '#,##0.00',
                    'Z' => '#,##0.00',
                    'AA' => '#,##0.00',
                    'AB' => '#,##0.00'  
              ));

                //[INDUSTRIAL] => เกษตรและอุตสาหกรรมอาหาร [BU] => อาหารและเครื่องดื่ม [SYMBOL] => TKN [U1] => 437121.19 [U2] => 0.0 [U3] => 0.0 [U4] => 0.0 [U5] => 0.0 [U6] => 0.0 [U7] => 0.0 [U8] => 0.0 [U9] => 0.0 [U10] => 0.0 [U11] => 0.0 [U12] => 0.0 [K1] => 0.0 [K2] => 0.0 [K3] => 0.0 [K4] => 0.0 [K5] => 0.0 [K6] => 0.0 [K7] => 0.0 [K8] => 0.0 [K9] => 0.0 [K10] => 0.0 [K11] => 0.0 [K12] => 0.0 )
                $industrialTemp = '';
                foreach ($results as $item) {
                    Log::debug('$industrialTemp => '.$industrialTemp);

                    $cellValue[] = null; 
                    $cellValue[0] = $industrialTemp != $item['INDUSTRIAL'] ? $item['INDUSTRIAL'] : '';
                    $cellValue[1] = $item['SYMBOL'];
                    $cellValue[2] = $item['U1'];
                    $cellValue[3] = $item['U2'];
                    $cellValue[4] = $item['U3'];
                    $cellValue[5] = $item['U4'];
                    $cellValue[6] = $item['U5'];
                    $cellValue[7] = $item['U6'];
                    $cellValue[8] = $item['U7'];
                    $cellValue[9] = $item['U8'];
                    $cellValue[10] = $item['U9'];
                    $cellValue[11] = $item['U10'];
                    $cellValue[12] = $item['U11'];
                    $cellValue[13] = $item['U12'];
                    $cellValue[14] = $item['U1']+$item['U2']+$item['U3']+$item['U4']+$item['U5']+$item['U6']+$item['U7']+$item['U8']+$item['U9']+$item['U10']+$item['U11']+$item['U12'];
                    $cellValue[15] = $item['K1'];
                    $cellValue[16] = $item['K2'];
                    $cellValue[17] = $item['K3'];
                    $cellValue[18] = $item['K4'];
                    $cellValue[19] = $item['K5'];
                    $cellValue[20] = $item['K6'];
                    $cellValue[21] = $item['K7'];
                    $cellValue[22] = $item['K8'];
                    $cellValue[23] = $item['K9'];
                    $cellValue[24] = $item['K10'];
                    $cellValue[25] = $item['K11'];
                    $cellValue[26] = $item['K12'];
                    $cellValue[27] = $item['K1']+$item['K2']+$item['K3']+$item['K4']+$item['K5']+$item['K6']+$item['K7']+$item['K8']+$item['K9']+$item['K10']+$item['K11']+$item['K12'];

                    $industrialTemp = $item['INDUSTRIAL'];

                    $sheet->appendRow($cellValue);
                }
            })->download('xls');
        });
    }
}
?>