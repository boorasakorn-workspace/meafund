<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Jenssegers\Date\Date;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;


use App\Libraries\MEAUtils;
use Illuminate\Support\Facades\Log;

class AdminP2EquityTradingReportController extends Controller
{
 
     
    /** 
     * รายงานซื้อ-ขายหลักทรัพย์ (Trading Report)
     */
    public function getreport()
    {
        $view_name = 'backend.pages.p2_equity_trading_report';
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 62,
            'menu_id' => 1,
            'title' => getMenuName($data,62,1) . '|  MEA FUND'
        ] );

      $allquery = "SELECT * FROM TBL_P2_EQUITY_SECURITIES";
        $equitylist = DB::select(DB::raw($allquery));

      

        $allquery = "SELECT * FROM TBL_P2_EQUITY_CATEGORY";
        $equitycategory = DB::select(DB::raw($allquery));

        // return view($view_name)->with([
        //     'equitylist'     => $equitylist,
        //     'equitycategory' => $equitycategory
        //     ]);
        $allquery = "SELECT * FROM TBL_P2_EQUITY_SECURITIES";
        $equitylist = DB::select(DB::raw($allquery));

        $allquery = "SELECT * FROM TBL_P2_EQUITY_CATEGORY ORDER BY INDUSTRIAL";
        $equitycategory = DB::select(DB::raw($allquery));

        $allquery = "SELECT * FROM TBL_P2_EQUITY_BROKER ORDER BY NAME_SHT ASC;";
        $equitybroker = DB::select(DB::raw($allquery));

        $allquery = "SELECT SYMBOL, COMP_NAME FROM TBL_P2_EQUITY_INDEX ORDER BY SYMBOL ASC;";
        $equityindex = DB::select(DB::raw($allquery));
  
        $years = array();
        for($i= date('Y'); $i>date('Y')-25; $i--) {
           array_push($years, $i);
        }

       /* 
        $symbols = array();
        //$equitylist as $item
        foreach ($equityindex as $item) {
            $symbols[$item->SYMBOL] = $item->SYMBOL . ' | ' . $item->COMP_NAME;
        }
        */
       
        
        /////////////
        $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')->orderby("INDUSTRIAL")->get();  
        $securitieslist =  DB::table('TBL_P2_EQUITY_SECURITIES')->orderby("NAME_SHT")->get(); 
        $sectionList = array( );
        $tmp = array();
        $key = "";

        $child = array();

        foreach ($categorylist as $row) {
            $key = $row->INDUSTRIAL;
            if (array_key_exists($key, $sectionList)) {
                $child = $sectionList[$key];
            } else {
                $child = array();
            }
            array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'BU'=>$row->BU,
                                     'INDUSTRIAL'=>$row->INDUSTRIAL
                                     ));
            $sectionList[$key] = $child;
            
            /*
            if($key != $row->INDUSTRIAL) {
               if($key == "") {
                   $child = array();
               } else {
                  $sectionList[$key] = $child;
                  $child = array();
               }
            } 
            $key = $row->INDUSTRIAL;
            array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'BU'=>$row->BU,
                                     'INDUSTRIAL'=>$row->INDUSTRIAL));
            */                         
        }
        /*return view('backend.pages.p2_tab3_add_equity_index_page')->with([
            
            'categorylist'   => $categorylist,
            'securitieslist' => $securitieslist,
            'sectionList'    => $sectionList

        ]); //->render();
*/
        return view($view_name)->with([
            'equitylist'     => $equitylist,
            'equitycategory' => $equitycategory,
            'equitybroker'   => $equitybroker,
            'years'          => $years,
            'symbols'        => $equityindex,
            'securitieslist' => $securitieslist,
            'sectionList'    => $sectionList
            ]);

    }

    public  function  DataSourceTradingReportCount($ArrParam, $IsCase){
        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];



        $symbol   = $ArrParam["symbol"] ;
        $name_sht = $ArrParam["name_sht"];
        $industrial = $ArrParam["industrial"];
        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

    }

   public  function  DataSourceCount($ArrParam, $IsCase){

        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];


        // Log::info(get_class($this) .'::'. __FUNCTION__);

        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM 
        $industrial = $ArrParam["industrial"];  // CATE_ID;
        
        $bu = '';
        $indus = '';

        if((!empty($industrial)) && (strlen($industrial) > 0)) {
            $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')
                           ->orderby("INDUSTRIAL")
                           ->where('CATE_ID','=', $industrial)->get();
            foreach ($categorylist as $row) {
                $indus = $row->INDUSTRIAL;
                $bu    = $row->BU;
                break;  
            }
        }
        
        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        
        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 5) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 5) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        $where = "" ;//
        //$where_inner = " WHERE ";
               
        if((!empty($symbol)) && (strlen($symbol) > 0)) { // && $check_name== "true"){
            $where .= " x.SYMBOL = '".$symbol."' ";
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where) > 1)
                $where .= " AND x.SECURITIES_NAME = '".$name_sht."' ";
            else
                $where .= " x.SECURITIES_NAME = '".$name_sht."' ";  
        }

        if((!empty($indus)) && (strlen($indus) > 0)) {  
            if(strlen($where) > 1)
               $where .= " AND k.INDUSTRIAL  = '".$indus."' ";
            else
               $where .= " k.INDUSTRIAL  = '".$indus."' "; 
        }

        if((!empty($bu)) && (strlen($bu) > 0)) {  
            if(strlen($where) > 1)
                $where .= " AND k.BU  = '".$bu."' ";
            else
                $where .= " k.BU  = '".$bu."' ";  
        }
        
        if(!$IsCase) {
            $where = "";
        }
        if(strlen($where) > 1) {
            $where = " AND " . $where  ; 
        }

        // Log::info(__FUNCTION__ . ' ::#2 WHERE#1 ====>' . $where );


        // WHERE2
        $where2 = "" ;//
        if((!empty($symbol)) && (strlen($symbol) > 0)) { // && $check_name== "true"){
            $where2 .= " SYMBOL = '".$symbol."' ";
            //$where_inner .= " x.SYMBOL = '" . $symbol .  "' "; 
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " SECURITIES_NAME = '".$name_sht."' ";
        }

        if((!empty($indus)) && (strlen($indus) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND INDUSTRIAL  = '".$indus."' ";
            else
               $where2 .= " INDUSTRIAL  = '".$indus."' "; 
        }

        if((!empty($bu)) && (strlen($bu) > 0)) {  
            if(strlen($where2) > 1) 
                $where2 .= " AND BU  = '".$bu."' ";
            else
                $where2 .= " BU  = '".$bu."' ";  
        }
        
        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
        // Log::info(__FUNCTION__ . ' ::#3 WHERE#2 ====>' . $where2 . ' date_start=' . $date_start);
        
       
        if(!$IsCase) {
            $where2 = "";

        }

        
        if(strlen($where2) > 1) {
            $where2 = " WHERE " . $where2  ; 
        }
         
         // Log::info(__FUNCTION__ . ' :: WHERE#2 ====>' . $where2 );

        // COUNT(SYMBOL) As total 
      
         $query = " ;WITH cte AS ( " .
                  "     SELECT " . 
                  "         row_number() OVER (ORDER BY TRANS_DATE) AS rownum, x.* , " . 
                  "         k.INDUSTRIAL, k.BU " . 
                  "     FROM " . 
                  "         TBL_P2_EQUITY_TRANS x, TBL_P2_EQUITY_INDEX  k " .
                  "     WHERE x.SYMBOL = k.SYMBOL " . 
                  $where .
                  " ) " .
                  " SELECT COUNT(*) as total FROM ( " .
                  "     SELECT  ".
                  "         a.TRANS_DATE, a.SYMBOL, a.SECURITIES_NAME, a.INDUSTRIAL, a.BU, ".
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE,a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, ". 
                  "         SUM(b.UNIT_PURCHASE) AS P_SUM, " .
                  "         SUM(b.UNIT_SALE) AS S_SUM, " .
                  "         SUM(b.UNIT_DIV) AS D_SUM, " .
                  "         SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as REMAIN_UNIT " .    
                  "     FROM cte a " . 
                  "     LEFT JOIN cte b " . 
                  "     ON a.SYMBOL = b.SYMBOL AND b.rownum <= a.rownum " .
                  "     GROUP BY " . 
                  "         a.SYMBOL, a.TRANS_DATE, a.rownum, " .
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE, a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, a.SECURITIES_NAME, " .
                  "         a.INDUSTRIAL, a.BU " .
                  " ) T  ". 
                  $where2 ;
                  /// " ORDER BY a.SYMBOL, a.TRANS_DATE " ; 

                  
        $all = DB::select(DB::raw($query));
        Log::info(__FUNCTION__ . ' :: SQL=' . $query);

        Log::info(__FUNCTION__  .' :: TOAL=' . $all[0]->total );
        return  $all[0]->total;
    }

    public  function  DataSource($ArrParam, $IsCase, $ispageing= true) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $where = "";
        if($ispageing){
            $PageSize = $ArrParam['pagesize'];
            $PageNumber = $ArrParam['PageNumber'];
        }

        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        
        // NEED TO SPLIT BU + INDUSTRIAL 
        $industrial = $ArrParam["industrial"];  
        $bu = '';
        $indus = '';

        if((!empty($industrial)) && (strlen($industrial) > 0)) {
            $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')
                           ->orderby("INDUSTRIAL")
                           ->where('CATE_ID','=', $industrial)->get();
            foreach ($categorylist as $row) {
                $indus = $row->INDUSTRIAL;
                $bu    = $row->BU;
                break;  
            }
        }

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        
        $where = "" ;//
        //$where_inner = " WHERE ";
               
        if((!empty($symbol)) && (strlen($symbol) > 0)) { // && $check_name== "true"){
            $where .= " x.SYMBOL = '".$symbol."' ";
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where) > 1)
                $where .= " AND x.SECURITIES_NAME = '".$name_sht."' ";
            else
                $where .= " x.SECURITIES_NAME = '".$name_sht."' ";  
        }

        if((!empty($indus)) && (strlen($indus) > 0)) {  
            if(strlen($where) > 1)
               $where .= " AND k.INDUSTRIAL  = '".$indus."' ";
            else
               $where .= " k.INDUSTRIAL  = '".$indus."' "; 
        }

        if((!empty($bu)) && (strlen($bu) > 0)) {  
            if(strlen($where) > 1)
                $where .= " AND k.BU  = '".$bu."' ";
            else
                $where .= " k.BU  = '".$bu."' ";  
        }
        
        //if((!empty($date_start)) && (!empty($date_end))) { 
        //    $where .= " AND x.TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
        //}
       
        if(!$IsCase) {
            $where = "";
        }
        if(strlen($where) > 1) {
            $where = " AND " . $where  ; 
        }


       // WHERE2
        $where2 = "" ;//
        if((!empty($symbol)) && (strlen($symbol) > 0)) { // && $check_name== "true"){
            $where2 .= " SYMBOL = '".$symbol."' ";
            //$where_inner .= " x.SYMBOL = '" . $symbol .  "' "; 
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " SECURITIES_NAME = '".$name_sht."' ";
        }

        if((!empty($indus)) && (strlen($indus) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND INDUSTRIAL  = '".$indus."' ";
            else
               $where2 .= " INDUSTRIAL  = '".$indus."' "; 
        }

        if((!empty($bu)) && (strlen($bu) > 0)) {  
            if(strlen($where2) > 1) 
                $where2 .= " AND BU  = '".$bu."' ";
            else
                $where2 .= " BU  = '".$bu."' ";  
        }
        
        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       
        if(!$IsCase) {
            $where2 = "";

        }

        if(strlen($where2) > 1) {
            $where2 = " WHERE " . $where2  ; 
        }

        Log::info('indus='.$indus . ', bu=' .$bu . ', date_start=' . $date_start);

        $query =  " ;WITH cte AS ( " .
                  "     SELECT " . 
                  "         row_number() OVER (ORDER BY TRANS_DATE) AS rownum, x.*, " . 
                  "         c.INDUSTRIAL as INDUSTRIAL, k.BU " . 
                  "     FROM " . 
                  "         TBL_P2_EQUITY_TRANS x, TBL_P2_EQUITY_INDEX  k , TBL_P2_EQUITY_CATEGORY c" . 
                  "     WHERE x.SYMBOL = k.SYMBOL  AND c.BU = k.BU " . 
                    $where .
                  " ) " .
                  " SELECT * FROM ( " .
                  "     SELECT ".
                  "         a.TRANS_DATE, a.SYMBOL, a.TYPE, a.SECURITIES_NAME, a.INDUSTRIAL, a.BU, ".
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE,a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, ". 
                  "         SUM(b.UNIT_PURCHASE) AS P_SUM, " .
                  "         SUM(b.UNIT_SALE) AS S_SUM, " .
                  "         SUM(b.UNIT_DIV) AS D_SUM, " .
                  "         SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as REMAIN_UNIT " .    
                  "     FROM cte a " . 
                  "     LEFT JOIN cte b " . 
                  "     ON a.SYMBOL = b.SYMBOL AND b.rownum <= a.rownum " .
                  "     GROUP BY " . 
                  "         a.SYMBOL, a.TYPE, a.TRANS_DATE, a.rownum, " .
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE, a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, a.SECURITIES_NAME, " .
                  "         a.INDUSTRIAL, a.BU " .
                  " ) T " .
                  $where2 . 
                  "  ORDER BY TRANS_DATE, SYMBOL " ;
                 // " ORDER BY a.SYMBOL, a.TRANS_DATE " ; 

        //$all = DB::select(DB::raw($allquery));
        // $query .= " ORDER BY focus.datenew DESC";

        if($ispageing){
            $query .= " OFFSET " . $PageSize . " * (" . $PageNumber . " - 1) ROWS FETCH NEXT " . $PageSize . " ROWS ONLY OPTION (RECOMPILE) ";
        }
        // var_dump($query);
        //           exit();
        // var_dump($query);
 
        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }

    public  function  DataSourceExport($ArrParam, $IsCase, $ispageing= true) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        
        $where = "";
        if($ispageing){
            $PageSize = $ArrParam['pagesize'];
            $PageNumber = $ArrParam['PageNumber'];
        }

        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        
        // NEED TO SPLIT BU + INDUSTRIAL 
        $industrial = $ArrParam["industrial"];  
        $bu = '';
        $indus = '';

        if((!empty($industrial)) && (strlen($industrial) > 0)) {
            $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')
                           ->orderby("INDUSTRIAL")
                           ->where('CATE_ID','=', $industrial)->get();
            foreach ($categorylist as $row) {
                $indus = $row->INDUSTRIAL;
                $bu    = $row->BU;
                break;  
            }
        }

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        
        $where = "" ;//
        //$where_inner = " WHERE ";
               
        if((!empty($symbol)) && (strlen($symbol) > 0)) { // && $check_name== "true"){
            $where .= " x.SYMBOL = '".$symbol."' ";
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where) > 1)
                $where .= " AND x.SECURITIES_NAME = '".$name_sht."' ";
            else
                $where .= " x.SECURITIES_NAME = '".$name_sht."' ";  
        }

        if((!empty($indus)) && (strlen($indus) > 0)) {  
            if(strlen($where) > 1)
               $where .= " AND k.INDUSTRIAL  = '".$indus."' ";
            else
               $where .= " k.INDUSTRIAL  = '".$indus."' "; 
        }

        if((!empty($bu)) && (strlen($bu) > 0)) {  
            if(strlen($where) > 1)
                $where .= " AND k.BU  = '".$bu."' ";
            else
                $where .= " k.BU  = '".$bu."' ";  
        }
        
        //if((!empty($date_start)) && (!empty($date_end))) { 
        //    $where .= " AND x.TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
        //}
       
        if(!$IsCase) {
            $where = "";
        }
        if(strlen($where) > 1) {
            $where = " AND " . $where  ; 
        }


       // WHERE2
        $where2 = "" ;//
        if((!empty($symbol)) && (strlen($symbol) > 0)) { // && $check_name== "true"){
            $where2 .= " SYMBOL = '".$symbol."' ";
            //$where_inner .= " x.SYMBOL = '" . $symbol .  "' "; 
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " SECURITIES_NAME = '".$name_sht."' ";
        }

        if((!empty($indus)) && (strlen($indus) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND INDUSTRIAL  = '".$indus."' ";
            else
               $where2 .= " INDUSTRIAL  = '".$indus."' "; 
        }

        if((!empty($bu)) && (strlen($bu) > 0)) {  
            if(strlen($where2) > 1) 
                $where2 .= " AND BU  = '".$bu."' ";
            else
                $where2 .= " BU  = '".$bu."' ";  
        }
        
        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       
        if(!$IsCase) {
            $where2 = "";

        }

        if(strlen($where2) > 1) {
            $where2 = " WHERE " . $where2  ; 
        }

        Log::info('indus='.$indus . ', bu=' .$bu . ', date_start=' . $date_start);

        $query =  " ;WITH cte AS ( " .
                  "     SELECT " . 
                  "         row_number() OVER (ORDER BY TRANS_DATE) AS rownum, x.*, " . 
                  "         k.INDUSTRIAL, k.BU " . 
                  "     FROM " . 
                  "         TBL_P2_EQUITY_TRANS x, TBL_P2_EQUITY_INDEX  k " . 
                  "     WHERE x.SYMBOL = k.SYMBOL " . 
                    $where .
                  " ) " .
                  " SELECT TRANS_DATE, SYMBOL, SECURITIES_NAME, INDUSTRIAL, BU, ". 
                  "        UNIT_DIV, TOTAL_DIV, UNIT_PURCHASE, PRICE_PURCHASE, TOTAL_PURCHASE, ". 
                  "        P_SUM, UNIT_SALE, PRICE_SALE, TOTAL_SALE, PROFIT_LOSS, REMAIN_UNIT  ".
                 /* "        ,SUM(TOTAL_PURCHASE) AS J,  " .
                  "         SUM(TOTAL_SALE) AS N,  " .
                  "         SUM(PROFIT_LOSS) AS O  " .*/
                  " FROM ( " .
                  "     SELECT ".
                  "         a.TRANS_DATE, a.SYMBOL, a.TYPE, a.SECURITIES_NAME, a.INDUSTRIAL, a.BU, ".
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE,a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, ". 
                  "         SUM(b.UNIT_PURCHASE) AS P_SUM, " .
                  "         SUM(b.UNIT_SALE) AS S_SUM, " .
                  "         SUM(b.UNIT_DIV) AS D_SUM, " .
                  "         SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as REMAIN_UNIT " .    

                /*  "        ,SUM(a.TOTAL_PURCHASE) as SUM_J,  " .
                  "         SUM(a.TOTAL_SALE) as SUM_N,  " .
                  "         SUM(a.PROFIT_LOSS) as SUM_O  " .
*/
                  "     FROM cte a " . 
                  "     LEFT JOIN cte b " . 
                  "     ON a.SYMBOL = b.SYMBOL AND b.rownum <= a.rownum " .
                  "     GROUP BY " . 
                  "         a.SYMBOL, a.TYPE, a.TRANS_DATE, a.rownum, " .
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE, a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, a.SECURITIES_NAME, " .
                  "         a.INDUSTRIAL, a.BU " .
                  " ) T " .
                  $where2 . 
                  
                 /* "  GROUP BY  " . 
                  "    TRANS_DATE, SYMBOL, SECURITIES_NAME, INDUSTRIAL, BU, ". 
                  "        UNIT_DIV, TOTAL_DIV, UNIT_PURCHASE, PRICE_PURCHASE, TOTAL_PURCHASE, ". 
                  "        P_SUM, UNIT_SALE, PRICE_SALE, TOTAL_SALE, PROFIT_LOSS, REMAIN_UNIT  " .*/
                  "  ORDER BY TRANS_DATE, SYMBOL " ;
                  ////" ORDER BY a.SYMBOL, a.TRANS_DATE " ; 

        //$all = DB::select(DB::raw($allquery));
        //$query .= " ORDER BY focus.datenew DESC";

        if($ispageing){
            $query .= " OFFSET " . $PageSize . " * (" . $PageNumber . " - 1) ROWS FETCH NEXT " . $PageSize . " ROWS ONLY OPTION (RECOMPILE) ";
        }

        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }


public  function  DataSourceGrandTotal($ArrParam) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        
        $where = "";
        
        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        
        // NEED TO SPLIT BU + INDUSTRIAL 
        $industrial = $ArrParam["industrial"];  
        $bu = '';
        $indus = '';

        if((!empty($industrial)) && (strlen($industrial) > 0)) {
            $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')
                           ->orderby("INDUSTRIAL")
                           ->where('CATE_ID','=', $industrial)->get();
            foreach ($categorylist as $row) {
                $indus = $row->INDUSTRIAL;
                $bu    = $row->BU;
                break;  
            }
        }

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        
        $where = "" ;//
        
               
        if((!empty($symbol)) && (strlen($symbol) > 0)) { // && $check_name== "true"){
            $where .= " x.SYMBOL = '".$symbol."' ";
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where) > 1)
                $where .= " AND x.SECURITIES_NAME = '".$name_sht."' ";
            else
                $where .= " x.SECURITIES_NAME = '".$name_sht."' ";  
        }

        if((!empty($indus)) && (strlen($indus) > 0)) {  
            if(strlen($where) > 1)
               $where .= " AND k.INDUSTRIAL  = '".$indus."' ";
            else
               $where .= " k.INDUSTRIAL  = '".$indus."' "; 
        }

        if((!empty($bu)) && (strlen($bu) > 0)) {  
            if(strlen($where) > 1)
                $where .= " AND k.BU  = '".$bu."' ";
            else
                $where .= " k.BU  = '".$bu."' ";  
        }
        
  
        if(strlen($where) > 1) {
            $where = " AND " . $where  ; 
        }


       // WHERE2
        $where2 = "" ;//
        if((!empty($symbol)) && (strlen($symbol) > 0)) { 
            $where2 .= " SYMBOL = '".$symbol."' ";
      
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " SECURITIES_NAME = '".$name_sht."' ";
        }

        if((!empty($indus)) && (strlen($indus) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND INDUSTRIAL  = '".$indus."' ";
            else
               $where2 .= " INDUSTRIAL  = '".$indus."' "; 
        }

        if((!empty($bu)) && (strlen($bu) > 0)) {  
            if(strlen($where2) > 1) 
                $where2 .= " AND BU  = '".$bu."' ";
            else
                $where2 .= " BU  = '".$bu."' ";  
        }
        
        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       

        if(strlen($where2) > 1) {
            $where2 = " WHERE " . $where2  ; 
        }

        Log::info('indus='.$indus . ', bu=' .$bu . ', date_start=' . $date_start);

        $query =  " ;WITH cte AS ( " .
                  "     SELECT " . 
                  "         row_number() OVER (ORDER BY TRANS_DATE) AS rownum, x.*, " . 
                  "         k.INDUSTRIAL, k.BU " . 
                  "     FROM " . 
                  "         TBL_P2_EQUITY_TRANS x, TBL_P2_EQUITY_INDEX  k " . 
                  "     WHERE x.SYMBOL = k.SYMBOL " . 
                    $where .
                  " ) " .
                  " SELECT " .
                  "         SUM(TOTAL_PURCHASE) AS J,  " .
                  "         SUM(TOTAL_SALE) AS N,      " .
                  "         SUM(PROFIT_LOSS) AS O      " .
                  " FROM ( " .
                  "     SELECT ".
                  "         a.TRANS_DATE, a.SYMBOL, a.TYPE, a.SECURITIES_NAME, a.INDUSTRIAL, a.BU, ".
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE,a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, ". 
                  "         SUM(b.UNIT_PURCHASE) AS P_SUM, " .
                  "         SUM(b.UNIT_SALE) AS S_SUM, " .
                  "         SUM(b.UNIT_DIV) AS D_SUM, " .
                  "         SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as REMAIN_UNIT " .    
                  "     FROM cte a " . 
                  "     LEFT JOIN cte b " . 
                  "     ON a.SYMBOL = b.SYMBOL AND b.rownum <= a.rownum " .
                  "     GROUP BY " . 
                  "         a.SYMBOL, a.TYPE, a.TRANS_DATE, a.rownum, " .
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE, a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, a.SECURITIES_NAME, " .
                  "         a.INDUSTRIAL, a.BU " .
                  " ) T " .
                  $where2 ;
                  
                //   var_dump($query);
                //   exit();
        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }


public  function DataSourceBarChart($ArrParam) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        
        $where = "";
        
        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        
        // NEED TO SPLIT BU + INDUSTRIAL 
        $industrial = $ArrParam["industrial"];  
        $bu = '';
        $indus = '';

        if((!empty($industrial)) && (strlen($industrial) > 0)) {
            $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')
                           ->orderby("INDUSTRIAL")
                           ->where('CATE_ID','=', $industrial)->get();
            foreach ($categorylist as $row) {
                $indus = $row->INDUSTRIAL;
                $bu    = $row->BU;
                break;  
            }
        }

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        
        $where = "" ;//
        
               
        if((!empty($symbol)) && (strlen($symbol) > 0)) { // && $check_name== "true"){
            $where .= " x.SYMBOL = '".$symbol."' ";
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where) > 1)
                $where .= " AND x.SECURITIES_NAME = '".$name_sht."' ";
            else
                $where .= " x.SECURITIES_NAME = '".$name_sht."' ";  
        }

        if((!empty($indus)) && (strlen($indus) > 0)) {  
            if(strlen($where) > 1)
               $where .= " AND k.INDUSTRIAL  = '".$indus."' ";
            else
               $where .= " k.INDUSTRIAL  = '".$indus."' "; 
        }

        if((!empty($bu)) && (strlen($bu) > 0)) {  
            if(strlen($where) > 1)
                $where .= " AND k.BU  = '".$bu."' ";
            else
                $where .= " k.BU  = '".$bu."' ";  
        }
        
  
        if(strlen($where) > 1) {
            $where = " AND " . $where  ; 
        }


       // WHERE2
        $where2 = "" ;//
        if((!empty($symbol)) && (strlen($symbol) > 0)) { 
            $where2 .= " SYMBOL = '".$symbol."' ";
      
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " SECURITIES_NAME = '".$name_sht."' ";
        }

        if((!empty($indus)) && (strlen($indus) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND INDUSTRIAL  = '".$indus."' ";
            else
               $where2 .= " INDUSTRIAL  = '".$indus."' "; 
        }

        if((!empty($bu)) && (strlen($bu) > 0)) {  
            if(strlen($where2) > 1) 
                $where2 .= " AND BU  = '".$bu."' ";
            else
                $where2 .= " BU  = '".$bu."' ";  
        }
        
        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       

        if(strlen($where2) > 1) {
            $where2 = " WHERE " . $where2  ; 
        }

        Log::info('indus='.$indus . ', bu=' .$bu . ', date_start=' . $date_start);

        $query =  " ;WITH cte AS ( " .
                  "     SELECT " . 
                  "         row_number() OVER (ORDER BY TRANS_DATE) AS rownum, x.*, " . 
                  "         k.INDUSTRIAL, k.BU " . 
                  "     FROM " . 
                  "         TBL_P2_EQUITY_TRANS x, TBL_P2_EQUITY_INDEX  k " . 
                  "     WHERE x.SYMBOL = k.SYMBOL " . 
                    $where .
                  " ) " .
                  " SELECT  INDUSTRIAL," .
                  "         SUM(TOTAL_PURCHASE) AS J,  " .
                  "         SUM(TOTAL_SALE) AS N,      " .
                  "         SUM(PROFIT_LOSS) AS O      " .
                  " FROM ( " .
                  "     SELECT ".
                  "         a.TRANS_DATE, a.SYMBOL, a.TYPE, a.SECURITIES_NAME, a.INDUSTRIAL, a.BU, ".
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE,a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, ". 
                  "         SUM(b.UNIT_PURCHASE) AS P_SUM, " .
                  "         SUM(b.UNIT_SALE) AS S_SUM, " .
                  "         SUM(b.UNIT_DIV) AS D_SUM, " .
                  "         SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as REMAIN_UNIT " .    
                  "     FROM cte a " . 
                  "     LEFT JOIN cte b " . 
                  "     ON a.SYMBOL = b.SYMBOL AND b.rownum <= a.rownum " .
                  "     GROUP BY " . 
                  "         a.SYMBOL, a.TYPE, a.TRANS_DATE, a.rownum, " .
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE, a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, a.SECURITIES_NAME, " .
                  "         a.INDUSTRIAL, a.BU " .
                  " ) T " .
                  $where2 .
                  ' GROUP BY INDUSTRIAL ';
        /**
         * NEW VERSION 
         */          
         $query =  " ;WITH cte AS ( " .
                  "     SELECT " . 
                  "         row_number() OVER (ORDER BY TRANS_DATE) AS rownum, x.*, " . 
                  "         c.INDUSTRIAL as INDUSTRIAL, k.BU , c.INDUS_SHT " . 
                  "     FROM " . 
                  "         TBL_P2_EQUITY_TRANS x, TBL_P2_EQUITY_INDEX  k, TBL_P2_EQUITY_CATEGORY c" . 
                  "     WHERE x.SYMBOL = k.SYMBOL AND  c.BU = k.BU " . 
                    $where .
                  " ) " .
                  " SELECT  INDUS_SHT as INDUSTRIAL,   " .
                  "         SUM(TOTAL_PURCHASE) AS J,  " .
                  "         SUM(TOTAL_SALE) AS N,      " .
                  "         SUM(PROFIT_LOSS) AS O      " .
                  " FROM ( " .
                  "     SELECT ".
                  "         a.TRANS_DATE, a.SYMBOL, a.TYPE, a.SECURITIES_NAME, a.INDUSTRIAL, a.INDUS_SHT, ".
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE,a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, ". 
                  "         SUM(b.UNIT_PURCHASE) AS P_SUM, " .
                  "         SUM(b.UNIT_SALE) AS S_SUM, " .
                  "         SUM(b.UNIT_DIV) AS D_SUM, " .
                  "         SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as REMAIN_UNIT " .    
                  "     FROM cte a " . 
                  "     LEFT JOIN cte b " . 
                  "     ON a.SYMBOL = b.SYMBOL AND b.rownum <= a.rownum " .
                  "     GROUP BY " . 
                  "         a.SYMBOL, a.TYPE, a.TRANS_DATE, a.rownum, " .
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE, a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, a.SECURITIES_NAME, " .
                  "         a.INDUSTRIAL, a.INDUS_SHT " .
                  " ) T " .
                  $where2 .
                  ' GROUP BY INDUS_SHT ';
                  
        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }


    // Business Chart "AGRO"
    public  function DataSourceBusinessTypeBarChart($ArrParam) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        
        $where = "";
        
        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        
        // NEED TO SPLIT BU + INDUSTRIAL 
        $industrial = $ArrParam["industrial"];  
        $indus_name  = $ArrParam["indus_name"]; 
        $bu = '';
        $indus = '';

        if((!empty($industrial)) && (strlen($industrial) > 0)) {
            $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')
                           ->orderby("INDUSTRIAL")
                           ->where('CATE_ID','=', $industrial)->get();
            foreach ($categorylist as $row) {
                $indus = $row->INDUSTRIAL;
                $bu    = $row->BU;
                break;  
            }
        }

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        
        $where = "" ;//
        
               
        if((!empty($symbol)) && (strlen($symbol) > 0)) { // && $check_name== "true"){
            $where .= " x.SYMBOL = '".$symbol."' ";
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where) > 1)
                $where .= " AND x.SECURITIES_NAME = '".$name_sht."' ";
            else
                $where .= " x.SECURITIES_NAME = '".$name_sht."' ";  
        }

        if((!empty($indus)) && (strlen($indus) > 0)) {  
            if(strlen($where) > 1)
               $where .= " AND k.INDUSTRIAL  = '".$indus."' ";
            else
               $where .= " k.INDUSTRIAL  = '".$indus."' "; 
        }

        if((!empty($bu)) && (strlen($bu) > 0)) {  
            if(strlen($where) > 1)
                $where .= " AND k.BU  = '".$bu."' ";
            else
                $where .= " k.BU  = '".$bu."' ";  
        }
        
  
        if(strlen($where) > 1) {
            $where = " AND " . $where  ; 
        }


       // WHERE2
        $where2 = " INDUS_SHT='". $indus_name . "' " ;//
        if((!empty($symbol)) && (strlen($symbol) > 0)) { 
            $where2 .= " SYMBOL = '".$symbol."' ";
      
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " SECURITIES_NAME = '".$name_sht."' ";
        }

        if((!empty($indus)) && (strlen($indus) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND INDUSTRIAL  = '".$indus."' ";
            else
               $where2 .= " INDUSTRIAL  = '".$indus."' "; 
        }

        if((!empty($bu)) && (strlen($bu) > 0)) {  
            if(strlen($where2) > 1) 
                $where2 .= " AND BU  = '".$bu."' ";
            else
                $where2 .= " BU  = '".$bu."' ";  
        }
        
        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       

        if(strlen($where2) > 1) {
            $where2 = " WHERE " . $where2  ; 
        }

        Log::info('indus='.$indus . ', bu=' .$bu . ', date_start=' . $date_start);
/*
        $query =  " ;WITH cte AS ( " .
                  "     SELECT " . 
                  "         row_number() OVER (ORDER BY TRANS_DATE) AS rownum, x.*, " . 
                  "         c.INDUSTRIAL as INDUSTRIAL, k.BU , c.INDUS_SHT ".
                  "     FROM " . 
                  "         TBL_P2_EQUITY_TRANS x, TBL_P2_EQUITY_INDEX  k, TBL_P2_EQUITY_CATEGORY c  " . 
                  "     WHERE x.SYMBOL = k.SYMBOL AND c.BU = k.BU " . 
                    $where .
                  " ) " .
                  " SELECT  INDUSTRIAL," .
                  "         SUM(TOTAL_PURCHASE) AS J,  " .
                  "         SUM(TOTAL_SALE) AS N,      " .
                  "         SUM(PROFIT_LOSS) AS O      " .
                  " FROM ( " .
                  "     SELECT ".
                  "         a.TRANS_DATE, a.SYMBOL, a.TYPE, a.SECURITIES_NAME, a.INDUSTRIAL, a.BU, a.INDUS_SHT, ".
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE,a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, ". 
                  "         SUM(b.UNIT_PURCHASE) AS P_SUM, " .
                  "         SUM(b.UNIT_SALE) AS S_SUM, " .
                  "         SUM(b.UNIT_DIV) AS D_SUM, " .
                  "         SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as REMAIN_UNIT " .    
                  "     FROM cte a LEFT JOIN cte b " . 
                  "          ON a.SYMBOL = b.SYMBOL AND b.rownum <= a.rownum AND a.BU = b.BU " .
                  "     GROUP BY " . 
                  "         a.SYMBOL, a.TYPE, a.TRANS_DATE, a.rownum, " .
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE, a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, a.SECURITIES_NAME, " .
                  "         a.INDUSTRIAL, a.BU, a.INDUS_SHT " .
                  " ) T " .
                  $where2 .
                  ' GROUP BY INDUSTRIAL ';
                  */
        /**
         * NEW VERSION 
         */          
         $query =  " ;WITH cte AS ( " .
                  "     SELECT " . 
                  "         row_number() OVER (ORDER BY TRANS_DATE) AS rownum, x.*, " . 
                  "         c.INDUSTRIAL as INDUSTRIAL, k.BU , c.INDUS_SHT, c.BU_SHT " . 
                  "     FROM " . 
                  "         TBL_P2_EQUITY_TRANS x, TBL_P2_EQUITY_INDEX  k, TBL_P2_EQUITY_CATEGORY c" . 
                  "     WHERE x.SYMBOL = k.SYMBOL AND c.BU = k.BU " . 
                    $where .
                  " ) " .
                  " SELECT  INDUS_SHT, " . 
                  "         INDUSTRIAL as INDUSTRIAL,". 
                  "         BU,   " .
                  "         BU_SHT," .
                  "         SUM(TOTAL_PURCHASE) AS J,  " .
                  "         SUM(TOTAL_SALE) AS N,      " .
                  "         SUM(PROFIT_LOSS) AS O      " .
                  " FROM ( " .
                  "     SELECT ".
                  "         a.TRANS_DATE, a.SYMBOL, a.TYPE, a.SECURITIES_NAME, a.INDUSTRIAL, a.BU, a.INDUS_SHT, a.BU_SHT, ".
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE,a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, ". 
                  "         SUM(b.UNIT_PURCHASE) AS P_SUM, " .
                  "         SUM(b.UNIT_SALE) AS S_SUM, " .
                  "         SUM(b.UNIT_DIV) AS D_SUM, " .
                  "         SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as REMAIN_UNIT " .    
                  "     FROM cte a " . 
                  "     LEFT JOIN cte b " . 
                  "     ON a.SYMBOL = b.SYMBOL AND b.rownum <= a.rownum AND a.BU = b.BU " .
                  "     GROUP BY " . 
                  "         a.SYMBOL, a.TYPE, a.TRANS_DATE, a.rownum, " .
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE, a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, a.SECURITIES_NAME, " .
                  "         a.INDUSTRIAL, a.BU, a.INDUS_SHT, a.BU_SHT " .
                  " ) T " .
                  $where2 .
                  " GROUP BY INDUSTRIAL, INDUS_SHT, BU, BU_SHT ";
                  
        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }

    public function ajax_report_search(Request $request)
    {
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $data = getmemulist();
        $title = getMenuName($data,62,1);

        $ar = $this->retrive_customize_field(62, 1);

        $view_name = 'backend.pages.ajax.ajax_p2_equity_trading_report';
        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $symbol = $request->input('symbol');
        $name_sht = $request->input('name_sht');
        $industrial = $request->input('industrial');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $ArrParam = array();
        $column = array();
        $prop_hdr = array();
        //{ pagesize: "10", PageNumber: 1, symbol: "", name_sht: "", industrial: "", date_start: "", date_end: "" }
        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;

        $ArrParam["symbol"] =$symbol;
        $ArrParam["name_sht"] =$name_sht;
        $ArrParam["industrial"] =$industrial;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;


        $data =null;
        $totals= 0;

        $totals = $this->DataSourceCount($ArrParam, true);
        $data = $this->DataSource($ArrParam, true);
        $grandtoal = $this->DataSourceGrandTotal($ArrParam);
        
        //$chartds = $this->DataSourceBarChart($ArrParam);
        $htmlPaginate =Paginatre_gen($totals, $PageSize,'page_click_search', $PageNumber);
        //if((!empty($date_start)) && (!empty($date_end))) 
        //{
        /*  
        $thai_date_start = (!empty($date_start)) ? $this->toThaiDateTime($date_start, false) : "";
        $thai_date_end =  (!empty($date_end)) ? $this->toThaiDateTime($date_end, false) : "";
        */
        $utils = new MEAUtils();
        Log::info(get_class($this) .'::'. __FUNCTION__. ' $utils =new MEAUtils()' );
        $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
        $thai_date_end =  (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";
        Log::info(get_class($this) .'::'. __FUNCTION__. ' ' .  
            ' converted date: ' . $date_start .' to ' . $thai_date_start . ', ' . $date_end . ' to '. $thai_date_end );
        
        $pretty_date = $utils->prettyPrintThaiDate($thai_date_start, $thai_date_end, "red");
      
        
        // option
        $opt_array = $ar['result'];
        
        $sub_array =  $opt_array[0];
        // crate column output array 
        $column = array();
        for($i =0; $i< 50; $i++) {
             if (count($opt_array) == 0) {
                $column[$i] = 'x'; 
             } else {
                $column[$i] = 'o'; 
            }
        }

        if (count($sub_array) > 0) {
            $sub = $sub_array['options'];
           
            Log::info(print_r($sub_array, true));
            $idx  = 0;
            if (is_array($sub))
            {
              foreach($sub as $x => $item) {
                  
                  if(strrpos($item, 'option') >= 0) {
                      Log::info($item);
                      $idx = intval(substr($item, 6, 2));
                      $column[$idx] = 'x';
                  }
                  $idx++;
              }
            } else {
                for($i =0; $i< 50; $i++) {
                   $column[$i] = 'x'; 
               } 
            }
        }
        
        $prop_hdr = array();
        $ln = 0;
        for($i = 0; $i< 5; $i++ ) {
            if($column[$i] == 'x') {
               $ln++;
            }
        }
        array_push($prop_hdr, array('length' =>$ln));

        $ln = 0;
        for($i = 5; $i< 7; $i++ ) {
            if($column[$i] == 'x') {
               $ln++;
            }
        }
        array_push($prop_hdr, array('length' =>$ln));

        $ln = 0;
        for($i = 7; $i< 11; $i++ ) {
            if($column[$i] == 'x') {
               $ln++;
            }
        }
        array_push($prop_hdr, array('length' =>$ln));

        $ln = 0;
        for($i = 11; $i< 16; $i++ ) {
            if($column[$i] == 'x') {
               $ln++;
            }
        }
        array_push($prop_hdr, array('length' =>$ln));

        // array_push($prop_hdr, array('length' =>'5'));
        // array_push($prop_hdr, array('length' =>'2'));
        // array_push($prop_hdr, array('length' =>'4'));
        // array_push($prop_hdr, array('length' =>'5'));

        // for($i = 0; $i< 50; $i++ ) {
        //     array_push($column,'x');
        // }
        // print_r($grandtoal);
        //     exit();
        //$opt_array = $results->options;
        $returnHTML = view($view_name)->with([
                'htmlPaginate'=> $htmlPaginate,
                'data' => $data,
                'totals' => $totals,

                'grandtoal' => $grandtoal,  // J, N, O
                'PageSize' =>$PageSize,
                'PageNumber' =>$PageNumber,
                'date_start' => $thai_date_start,
                'date_end' => $thai_date_end,
                'pretty_date' => $pretty_date,
                'TableTitle' => $title,
                'fields' => $column,
                'fields_poperties' => $prop_hdr

            ])->render();
            // var_dump(response()->json(array('success' => true, 'html'=>$returnHTML)));
            // exit();
        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }



    public function ajax_chart_search(Request $request) {
        Log::info(get_class($this) .'::'. __FUNCTION__);
         
        $data = getmemulist();
        $title = getMenuName($data,62,1);

        $ar = $this->retrive_customize_field(62, 1);

        // $view_name = 'backend.pages.ajax.ajax_p2_equity_trading_report';

        $symbol = $request->input('symbol');
        $name_sht = $request->input('name_sht');
        $industrial = $request->input('industrial');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $ArrParam = array();

        //{ pagesize: "10", PageNumber: 1, symbol: "", name_sht: "", industrial: "", date_start: "", date_end: "" }
        $ArrParam["pagesize"] ="";
        $ArrParam["PageNumber"] ="";

        $ArrParam["symbol"] =$symbol;
        $ArrParam["name_sht"] =$name_sht;
        $ArrParam["industrial"] =$industrial;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;


        $data =null;
        $totals= 0;

        $chartds = $this->DataSourceBarChart($ArrParam);
      
        $utils = new MEAUtils();
        $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
        $thai_date_end =  (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";
        
        Log::info(get_class($this) .'::'. __FUNCTION__.
            ' converted date: ' . $date_start .' to ' . $thai_date_start . ', ' . $date_end . ' to '. $thai_date_end );
        $data  = array();
        $data1 = array();
        $data2 = array();

        //$buarray = array(); 
        $bu_list = DB::table('TBL_P2_EQUITY_CATEGORY')
                           ->orderby("INDUSTRIAL")
                           ->where('CATE_ID','=', $industrial)->get();
          
     
        $pretty_date = $utils->prettyPrintThaiDate($thai_date_start, $thai_date_end, "");
        $returnHTML = [
                'data' => $chartds,
                'date_start' => $thai_date_start,
                'date_end' => $thai_date_end,
                'pretty_date' => $pretty_date,
                'bu_list' => $bu_list

        ];
                      

        return response()->json(array('success' => true, 'result'=>$returnHTML));
    }

    /**
     * Handle request Business Chart
     *
     * @param symbol name eg "CPG"
     * @param name_sht Equity security short name eg. "UOBAM"
     * @param industrial A category id of division of industrial
     * @param indus_name division of industial name eg. "AGRO" or "AGRI"
     * @param date_start start period format "dd-MM-YYYY" 
     * @param date_end   end period in format "dd-MM-YYYY" 
     */
    public function ajax_business_chart_search(Request $request) {
        Log::info(get_class($this) .'::'. __FUNCTION__);
         
        $data = getmemulist();
        $title = getMenuName($data,62,1);

        $ar = $this->retrive_customize_field(62, 1);

        $symbol     = $request->input('symbol');      // eg. "CPF"
        $name_sht   = $request->input('name_sht');    // eg. "UOBAM "or "KTAM" 
        $industrial = $request->input('industrial');  // eg. CATE_ID: 21
        $indus_name = $request->input('indus_name');  // eg "AGRO" from user clicked on chart index
        $date_start = $request->input('date_start');  // eg.
        $date_end   = $request->input('date_end');

        $ArrParam = array();

        //{ pagesize: "10", PageNumber: 1, symbol: "", name_sht: "", industrial: "", date_start: "", date_end: "" }
        $ArrParam["pagesize"]   = "";
        $ArrParam["PageNumber"] = "";

        $ArrParam["symbol"]     = $symbol;
        $ArrParam["name_sht"]   = $name_sht;
        $ArrParam["industrial"] = $industrial;
        $ArrParam["indus_name"] = $indus_name;

        $ArrParam["date_start"] = $date_start;
        $ArrParam["date_end"]   = $date_end;


        $data = null;
        $totals = 0;

        $chartds = $this->DataSourceBusinessTypeBarChart($ArrParam);
        //$chartds = $this->DataSourceBarChart($ArrParam);
      
        $utils = new MEAUtils();
        $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
        $thai_date_end =  (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";
        
        Log::info(get_class($this) .'::'. __FUNCTION__.
            ' converted date: ' . $date_start .' to ' . $thai_date_start . ', ' . $date_end . ' to '. $thai_date_end );
        $data  = array();
        $data1 = array();
        $data2 = array();

        //$buarray = array(); 
        $bu_list = DB::table('TBL_P2_EQUITY_CATEGORY')
                           ->orderby("INDUSTRIAL")
                           ->where('INDUS_SHT','=', $indus_name)->get();
          
     
        $pretty_date = $utils->prettyPrintThaiDate($thai_date_start, $thai_date_end, "");
        $returnHTML = [
                'data' => $chartds,
                'date_start' => $thai_date_start,
                'date_end' => $thai_date_end,
                'pretty_date' => $pretty_date,
                'bu_list' => $bu_list

        ];
                      

        return response()->json(array('success' => true, 'result'=>$returnHTML));
    }

    public function ajax_report_search_export() {
       // Log::info(get_class($this) .'::'. __FUNCTION__);
        
      /*  $ArrParam["emp_id"] =Input::get("EmpID");
        $ArrParam["depart"] =Input::get("depart");
        $ArrParam["plan"] =Input::get("plan");
        $ArrParam["date_start"] =Input::get("date_start");
        $ArrParam["date_end"] =Input::get("date_end");

        $ArrParam["check_name"] =Input::get("check_name");
        $ArrParam["check_depart"] =Input::get("check_depart");
        $ArrParam["check_plan"] =Input::get("check_plan");
        $ArrParam["check_date"] =Input::get("check_date");
        // $sql2 = "SELECT TOP  5 * FROM  TBL_MEMBER_BENEFITS WHERe EMP_ID = '".get_userID()."' ORDER BY RECORD_DATE ASC";
*/
        $symbol = Input::get('symbol');
        $name_sht = Input::get('name_sht');
        $industrial = Input::get('industrial');
        $date_start = Input::get('date_start');
        $date_end = Input::get('date_end');

        $ArrParam = array();
        $ArrParam["pagesize"] ="";
        $ArrParam["PageNumber"] ="";
        $ArrParam["symbol"] =$symbol;
        $ArrParam["name_sht"] =$name_sht;
        $ArrParam["industrial"] =$industrial;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;

        $data = $this->DataSourceExport($ArrParam, false, false);
        $results = array();
        foreach ($data as $item) {
//            $item->filed1 = 'some modification';
//            $item->filed2 = 'some modification2';
            $results[] = (array)$item;

           // $results[5] = get_date_notime($item->modify_new);

            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }



        Excel::create('รายงานซื้อ-ขายหลักทรัพย์', function ($excel) use ($results, $ArrParam){

            $excel->sheet('รายงานซื้อ-ขายหลักทรัพย์', function ($sheet) use ($results, $ArrParam){
                // first row styling and writing content
                $sheet->setStyle([
                    'borders' => [
                        'allborders' => [
                            'color' => [
                                'rgb' => 'b3b6b7'
                            ]
                        ]
                    ]
                ]);
                $sheet->mergeCells('A1:S1');
                $sheet->mergeCells('A2:S2');
                $sheet->mergeCells('A3:S3');

                $sheet->mergeCells('F4:G4');
                $sheet->mergeCells('H4:K4');
                $sheet->mergeCells('L4:P4');
                $sheet->mergeCells('Q4:S4');

                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Cordia New');
                    $row->setFontSize(20);
                    $row->setFontWeight('bold');
                    $row->setAlignment('center');
                    $row->setBackground('#b2babb');
                    // Set all borders (top, right, bottom, left)
                    $row->setBorder('thin', 'thin', 'none', 'thin');

                });

                $sheet->row(2, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'none', 'thin');
                });

                $sheet->row(3, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'thin', 'thin');

                });



                $sheet->row(1, array('รายงานซื้อ-ขายหลักทรัพย์'));

                if($ArrParam["date_start"] != "" && $ArrParam["date_end"] != ""){
                    $sheet->row(2, array('ในช่วงวันที่ ' . get_date_notime($ArrParam["date_start"]) . ' ถึง ' . get_date_notime($ArrParam["date_end"])   ));
                }


                $sheet->row(3, array('รายงานข้อมูล ณ วันที่ ' . get_date_notime(date("Y-m-d H:i:s"))));

               


                $sheet->cell('F4', function($cell) {
                    $cell->setValue('เงินปันผล');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                });

                $sheet->cell('H4', function($cell) {
                    $cell->setValue('ข้อมูลการซื้อ');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                });

                $sheet->cell('L4', function($cell) {
                    $cell->setValue('ข้อมูลการขาย');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                });

                $sheet->cell('Q4', function($cell) {
                    $cell->setValue('คงเหลือ (ราคาทุน)');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                });

                 

                 
                $header[]   = null;
                $header[0]  = "วันเดือนปี";
                $header[1]  = "ชื่อ";
                $header[2]  = "บลจ";
                $header[3]  = "กลุ่มอุตสาหกรรม";
                $header[4]  = "หมวดธุรกิจ";
                $header[5]  = "ผลปันผล";
                $header[6]  = "จำนวนเงิน";
                $header[7]  = "หน่วยซื้อ";
                $header[8]  = "ราคาซื้อต่อหน่วย";
                $header[9]  = "ยอดซื้อ (บาท)";
                $header[10] = "คงเหลือ (หน่วย)";
                $header[11] = "หน่วยขาย";
                $header[12] = "ราคาขายต่อหน่วย";
                $header[13] = "ยอดขาย (บาท) ";
                $header[14] = "กำไร-ขาดทุน";
                $header[15] = "คงเหลือ (หน่วย)";
                $header[16] = "จำนวนหน่วย ";
                $header[17] = "ราคาต่อหน่วย";
                $header[18] = "จำนวนเงิน";
                $sheet->row(5, $header);

                $LAST_ROW = 5 + 1;
                // Set multiple column formats
                $sheet->setColumnFormat(array(
                        'A' => '@',
                        'F' => '#,##0.00',
                        'G' => '#,##0.00',
                        'H' => '#,##0.00',
                        'I' => '#,##0.00',
                        'J' => '#,##0.00', // SUM(TOTAL_PURCHASE)
                        'K' => '#,##0.00',  
                        'L' => '#,##0.00',
                        'M' => '#,##0.00', // SUM(TOTAL_SALE)
                        'N' => '#,##0.00',
                        'O' => '#,##0.00', // SUM(PROFIT_LOSS)
                        'P' => '#,##0.00', 
                        'Q' => '#,##0',
                        'R' => '#,##0.00', // SUM(PROFIT_LOSS)
                        'S' => '#,##0.00',
                  ));
/*
  " SELECT TRANS_DATE, SYMBOL, SECURITIES_NAME, INDUSTRIAL, BU, ". 
                  "        UNIT_DIV, TOTAL_DIV, UNIT_PURCHASE, PRICE_PURCHASE, TOTAL_PURCHASE, ". 
                  "        P_SUM, UNIT_SALE, PRICE_SALE, TOTAL_SALE, PROFIT_LOSS, REMAIN_UNIT, ".
                  "        SUM(TOTAL_PURCHASE) as J,  " .
                  "        SUM(TOTAL_SALE) as N,  " .
                  "        SUM(PROFIT_LOSS) as O,  " .
*/         
                $rscount = count($results);   
                $idx = 0; 
                $N = 0.0;
                $J = 0.0;
                $O = 0.0;
                $utotal = 0.0;
                $ptotal = 0.0;
                $stotal = 0.0;
                foreach ($results as $item) {
                    
                     $rs[] = null; 
                     $rs[0] = $item['TRANS_DATE']; // A
                     $rs[1] = $item['SYMBOL']; 
                     $rs[2] = $item['SECURITIES_NAME'];
                     $rs[3] = $item['INDUSTRIAL'];
                     $rs[4] = $item['BU'];
                     $rs[5] = $item['UNIT_DIV'];
                     $rs[6] = $item['TOTAL_DIV'];
                     $rs[7] = $item['UNIT_PURCHASE'];
                     $rs[8] = $item['PRICE_PURCHASE'];
                     $rs[9] = $item['TOTAL_PURCHASE'];
                     $rs[10] = $item['P_SUM'];
                     $rs[11] = $item['UNIT_SALE'];
                     $rs[12] = $item['PRICE_SALE'];
                     $rs[13] = $item['TOTAL_SALE'];
                     $rs[14] = $item['PROFIT_LOSS'];
                     $rs[15] = $item['REMAIN_UNIT'];
                     
                     $J += $item['TOTAL_PURCHASE'];
                     $N += $item['TOTAL_SALE'];
                     $O += $item['PROFIT_LOSS'];
                     $utotal = $utotal + $item['UNIT_PURCHASE'] - $item['UNIT_SALE'];
                     $ptotal = $ptotal + $item['TOTAL_PURCHASE'] - $item['TOTAL_SALE'];
                     if($item['UNIT_PURCHASE'] == 0){
                        $stotal = $item['PRICE_SALE'];
                     }
                     else{
                        $stotal = $item['PRICE_PURCHASE'];
                     }
                     
                     $rs[16] = $utotal;
                     $rs[17] = $stotal;
                     $rs[18] = $ptotal;
                     $sheet->appendRow($rs);
                     $idx++;
                }

                $sheet->cell('I'. strval($idx+$LAST_ROW), function($cell) {
                        $cell->setValue("ยอดรวม");
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                });


                if($rscount > 0) {
                    
                    $sheet->cell('J'. strval($idx+$LAST_ROW), function($cell) use ($J) {
                        $cell->setValue($J);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                    });
                    $sheet->cell('N'. strval($idx+$LAST_ROW), function($cell) use ($N) {
                        $cell->setValue($N);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                    });
                    $sheet->cell('O'. strval($idx+$LAST_ROW), function($cell) use ($O) {
                        $cell->setValue($O);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                    });
                } 

            });

        })->download('xls');
    }

    public function ajax_store_customize_field(Request $request) {
        $status = false;
        $result = array();
        $store_path = storage_path() .'/public/import';
        try {
            $menu_id = $request->input('menu_id');  //eg. 60
            $submenu_id = $request->input('submenu_id');
            $utils = new MEAUtils();
            $user = $utils->getLoggedInUser(); 
            $json = $request->input('options');
            $options = array('options'=>$json);
            $file = $store_path . '/' . $user. '-' . $menu_id . '-' . $submenu_id. '.json';
            file_put_contents($file, json_encode($options, TRUE));
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Saved:' . $file );
            array_push($results, $file);

        } catch (\Exception $e) {
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Exception:' .$e->getMessage());
            $status = false;
        } 
        return response()->json(array('success' => $status, 'result'=>$result));
    }

    
    public function retrive_customize_field($menu_id, $submenu_id) {
        $status = false;
        $result = array();
        $store_path = storage_path() .'/public/import';
        try {
            //$menu_id = $request->input('menu_id');  // eg. 60
            //$submenu_id = $request->input('submenu_id');
            $utils = new MEAUtils(); 
            
            $user = $utils->getLoggedInUser(); 
            $file = $store_path . '/' . $user . '-' . $menu_id . '-' . $submenu_id. '.json';

            $json = json_decode(file_get_contents($file), true);
            
            array_push($result, $json);

            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Retrived:' . $file );
            $status = true;
        } catch (\Exception $e) {
            
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Exception:' .$e->getMessage());
            $status = false;
        } 
        return array('success' => $status, 'result'=>$result);
    } 

    public function ajax_retrive_customize_field(Request $request) {
        /*$status = false;
        $result = array();
        $store_path = storage_path() .'/public/import';
        try {
            $menu_id = $request->input('menu_id');  // eg. 60
            $submenu_id = $request->input('submenu_id');
            $utils = new MEAUtils();// Session::get('user_data');
            $user = $utils->getLoggedInUser(); 
            $file = $store_path . '/' . $user . '-' . $menu_id . '-' . $submenu_id. '.json';
            $json = json_decode(file_get_contents($file), true);
            array_push($result, $json);

            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Retrived:' . $file );
            $status = true;
        } catch (\Exception $e) {
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Exception:' .$e->getMessage());
            $status = false;
        } 

        return response()->json(array('success' => $status, 'result'=>$result));*/
        $menu_id = $request->input('menu_id');  // eg. 60
        $submenu_id = $request->input('submenu_id');
          
        $r = $this->retrive_customize_field($menu_id, $submenu_id);
        return response()->json($r);
    }

    /* Utils */
   /* public function toThaiDateTime($strDate, $withTime)
    {
        $strYear = date("Y",strtotime($strDate)) + 543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        if ($withTime) {
            $strHour= date("H",strtotime($strDate));
            $strMinute= date("i",strtotime($strDate));
            $strSeconds= date("s",strtotime($strDate));
            
            return $strDay . " " . $strMonthThai. " ". $strYear .",  " . $strHour . ":" . $strMinute;
        } 
        return $strDay . " " . $strMonthThai. " ". $strYear ;
        
    }
    */

    /////////////////////////
    ///
    //
 /*   $opt_array = array('result'=>['option2', 'option1', 'option3']);
$sub_array = $opt_array['result']; 
$o = array();
foreach($sub_array as $x => $i ) {
    $str = substr($i, 6); // option0
    if(strrpos($str,'option') >= 0) {
        $idx = substr($i, 6, 1);
        $nr = array($idx => 'checked');
        array_push($o, $nr);
    }
   
}

print_r($o['0']);
 */

}


/**
 * 1) STATEMENT SUM TOTAL:
 * =====================
 * V1:
 * SELECT  COALESCE(SUM(TOTAL_PURCHASE) / NULLIF(SUM(UNIT_PURCHASE), 0), 0) AS TMP_PRICE_PERCHASE,
 *       COALESCE (SUM(TOTAL_SALE) / NULLIF(SUM(UNIT_SALE), 0), 0) AS TMP_PRICE_SALE,
 *       COALESCE (SUM(TOTAL_DIV) / NULLIF(SUM(UNIT_DIV), 0), 0) AS TMP_PRICE_DIV
 *
 *     FROM TBL_P2_EQUITY_TRANS WHERE SYMBOL='CPALL'
 *     AND SECURITIES_NAME ='UOBAM' AND (TRANS_DATE BETWEEN '2017-05-01 00:00:01' AND  '2017-05-31 23:59:59');
 *
 */

 /* V2:
 SELECT  COALESCE(SUM(TOTAL_PURCHASE) / NULLIF(SUM(UNIT_PURCHASE), 0), 0) AS TMP_PRICE_PERCHASE, SUM(UNIT_PURCHASE) AS SUM_UNIT_PERCHASE, SUM(TOTAL_PURCHASE) AS SUM_TOAL_PURCHASE,
        COALESCE(SUM(TOTAL_SALE) / NULLIF(SUM(UNIT_SALE), 0), 0) AS TMP_PRICE_SALE, SUM(UNIT_SALE) AS SUM_UNIT_SALE, SUM(TOTAL_SALE) AS SUM_TOAL_SALE, SUM(TOTAL_COST) AS SUM_TOTAL_COST, SUM(PROFIT_LOSS) AS SUM_PROFIT_LOSS,

        COALESCE(SUM(TOTAL_DIV) / NULLIF(SUM(UNIT_DIV), 0), 0) AS TMP_PRICE_DIV,  SUM(UNIT_DIV) AS SUM_UNIT_DIV, SUM(TOTAL_DIV) AS SUM_TOAL_DIV

      FROM TBL_P2_EQUITY_TRANS WHERE SYMBOL='CPALL'
       AND SECURITIES_NAME ='UOBAM' AND (TRANS_DATE BETWEEN '2017-01-01 00:00:01' AND  '2017-01-31 23:59:59');


 */
       /*
       SELECT  COALESCE(SUM(TOTAL_PURCHASE) / NULLIF(SUM(UNIT_PURCHASE), 0), 0) AS TMP_PRICE_PERCHASE, 
        SUM(UNIT_PURCHASE) AS SUM_UNIT_PERCHASE, SUM(TOTAL_PURCHASE) AS SUM_TOAL_PURCHASE,  
        

        COALESCE(SUM(TOTAL_SALE) / NULLIF(SUM(UNIT_SALE), 0), 0) AS TMP_PRICE_SALE, 
        SUM(UNIT_SALE) AS SUM_UNIT_SALE, SUM(TOTAL_SALE) AS SUM_TOTAL_SALE, SUM(TOTAL_COST) AS SUM_TOTAL_COST, SUM(PROFIT_LOSS) AS SUM_PROFIT_LOSS,
        
        COALESCE(SUM(TOTAL_DIV) / NULLIF(SUM(UNIT_DIV), 0), 0) AS TMP_PRICE_DIV,  SUM(UNIT_DIV) AS SUM_UNIT_DIV, SUM(TOTAL_DIV) AS SUM_TOAL_DIV,

        SUM(UNIT_PURCHASE) - SUM(UNIT_SALE) as TOTAL_UNIT_DIFF

         

      FROM TBL_P2_EQUITY_TRANS WHERE SYMBOL='CPALL'
       AND SECURITIES_NAME ='UOBAM' AND (TRANS_DATE BETWEEN '2017-01-01 00:00:01' AND  '2017-01-31 23:59:59');


       */

       /* V4:
      SELECT   a.LAST_SYMBOL, 
        (CASE WHEN a.LAST_UNIT_PURCHASE >= 0 THEN 'IN PERCHASE' ELSE 'IN SALE' END) as T,
        --a.LAST_UNIT_PURCHASE as WOW, 
        COALESCE(SUM(b.TOTAL_PURCHASE) / NULLIF(SUM(b.UNIT_PURCHASE), 0), 0) AS TMP_PRICE_PERCHASE, 
        SUM(b.UNIT_PURCHASE) AS SUM_UNIT_PERCHASE, SUM(b.TOTAL_PURCHASE) AS SUM_TOAL_PURCHASE,  
        

        COALESCE(SUM(b.TOTAL_SALE) / NULLIF(SUM(b.UNIT_SALE), 0), 0) AS TMP_PRICE_SALE, 
        SUM(b.UNIT_SALE) AS SUM_UNIT_SALE, SUM(b.TOTAL_SALE) AS SUM_TOTAL_SALE,
       SUM(b.TOTAL_COST) AS SUM_TOTAL_COST, SUM(b.PROFIT_LOSS) AS SUM_PROFIT_LOSS,
        
        COALESCE(SUM(b.TOTAL_DIV) / NULLIF(SUM(b.UNIT_DIV), 0), 0) AS TMP_PRICE_DIV, 
                SUM(b.UNIT_DIV) AS SUM_UNIT_DIV, SUM(b.TOTAL_DIV) AS SUM_TOAL_DIV,

        SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as TOTAL_UNIT_DIFF


      FROM TBL_P2_EQUITY_TRANS b INNER JOIN  
         
         (select 
            TOP 1 TRANS_DATE as LAST_TRANS_DATE, 
            TYPE as LAST_TYPE, 
            UNIT_PURCHASE as LAST_UNIT_PURCHASE,
            SYMBOL as LAST_SYMBOL ,
            SECURITIES_NAME
        from 
            TBL_P2_EQUITY_TRANS
        WHERE 
            TRANS_DATE < '2017-01-31 23:59:59' AND SYMBOL='CPALL' AND SECURITIES_NAME='UOBAM') a 

         ON a.LAST_SYMBOL = b.SYMBOL 
           AND a.SECURITIES_NAME = b.SECURITIES_NAME
           AND b.SYMBOL='CPALL' AND b.SECURITIES_NAME ='UOBAM'
           AND (b.TRANS_DATE BETWEEN '2017-01-07 00:00:01' AND  '2017-01-31 23:59:59')  
         
          GROUP BY a.LAST_SYMBOL, a.LAST_UNIT_PURCHASE;
      


       */

          /*
          V5:
          ====
          select * FROM TBL_P2_EQUITY_TRANS,
 (SELECT   a.LAST_SYMBOL, 
        (CASE WHEN a.LAST_UNIT_PURCHASE > 0 THEN 'IN PURCHASE' ELSE 'IN SALE' END) as LAST_TR,
        --a.LAST_UNIT_PURCHASE as WOW, 
        COALESCE(SUM(b.TOTAL_PURCHASE) / NULLIF(SUM(b.UNIT_PURCHASE), 0), 0) AS TMP_PRICE_PURCHASE, 
        SUM(b.UNIT_PURCHASE) AS SUM_UNIT_PURCHASE, SUM(b.TOTAL_PURCHASE) AS SUM_TOAL_PURCHASE,  
        

        COALESCE(SUM(b.TOTAL_SALE) / NULLIF(SUM(b.UNIT_SALE), 0), 0) AS TMP_PRICE_SALE, 
        SUM(b.UNIT_SALE) AS SUM_UNIT_SALE, SUM(b.TOTAL_SALE) AS SUM_TOTAL_SALE,
       SUM(b.TOTAL_COST) AS SUM_TOTAL_COST, SUM(b.PROFIT_LOSS) AS SUM_PROFIT_LOSS,
        
        COALESCE(SUM(b.TOTAL_DIV) / NULLIF(SUM(b.UNIT_DIV), 0), 0) AS TMP_PRICE_DIV, 
                SUM(b.UNIT_DIV) AS SUM_UNIT_DIV, SUM(b.TOTAL_DIV) AS SUM_TOAL_DIV,

        ABS(SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE)) as TOTAL_UNIT_DIFF


      FROM TBL_P2_EQUITY_TRANS b INNER JOIN  
         
         (select 
            TOP 1 TRANS_DATE as LAST_TRANS_DATE, 
            TYPE as LAST_TYPE, 
            UNIT_PURCHASE as LAST_UNIT_PURCHASE,
            SYMBOL as LAST_SYMBOL ,
            SECURITIES_NAME
        from 
            TBL_P2_EQUITY_TRANS
        WHERE 
            TRANS_DATE < '2017-01-18 23:59:59' AND SYMBOL='AP' AND SECURITIES_NAME='UOBAM' ORDER BY TRANS_DATE DESC) a 

         ON a.LAST_SYMBOL = b.SYMBOL 
           AND a.SECURITIES_NAME = b.SECURITIES_NAME
           AND b.SYMBOL='AP' AND b.SECURITIES_NAME ='UOBAM'
           AND (b.TRANS_DATE BETWEEN '2017-01-11 00:00:01' AND  '2017-01-18 23:59:59')             
         
           GROUP BY a.LAST_SYMBOL, a.LAST_UNIT_PURCHASE ) C 

      WHERE 
           SYMBOL='AP' AND SECURITIES_NAME ='UOBAM' 
           AND (TRANS_DATE BETWEEN '2017-01-01 00:00:01' AND  '2017-01-18 23:59:59')
 

          */
/*
 V6:
 ===

SELECT x.TRANS_DATE, x.SYMBOL,  x.BU, x.INDUS, x.UNIT_PURCHASE, x.PRICE_PURCHASE, x.TOTAL_PURCHASE, x.P_SUM + COALESCE(SUM(y.P_SUM),0) SUM_UNIT_PURCHASE
      , x.UNIT_SALE, x.PRICE_SALE, x.TOTAL_SALE, x.PROFIT_LOSS
FROM
(
    SELECT 
        SUM(UNIT_PURCHASE) P_SUM, 
        SYMBOL, 
        D.BU as BU, 
        D.INDUSTRIAL as INDUS, 
        TRANS_DATE, 
        UNIT_PURCHASE, 
        PRICE_PURCHASE, 
        TOTAL_PURCHASE, 
        UNIT_SALE,
        PRICE_SALE, TOTAL_SALE, PROFIT_LOSS
    FROM 
        TBL_P2_EQUITY_TRANS, 
        (SELECT BU, INDUSTRIAL FROM TBL_P2_EQUITY_INDEX WHERE SYMBOL='APCS'  ) D
    
    
    GROUP BY TRANS_DATE, UNIT_PURCHASE, SYMBOL, PRICE_PURCHASE, TOTAL_PURCHASE, D.BU, D.INDUSTRIAL, UNIT_SALE,
          PRICE_SALE, TOTAL_SALE, PROFIT_LOSS
) x

LEFT OUTER JOIN
(
    SELECT SUM(UNIT_PURCHASE) P_SUM, TRANS_DATE, UNIT_PURCHASE
    FROM TBL_P2_EQUITY_TRANS
    GROUP BY TRANS_DATE, UNIT_PURCHASE
) y ON y.TRANS_DATE < x.TRANS_DATE

WHERE SYMBOL='APCS'

GROUP BY x.TRANS_DATE, x.P_SUM, x.SYMBOL, x.INDUS, x.BU, x.UNIT_PURCHASE, x.PRICE_PURCHASE, x.TOTAL_PURCHASE, x.UNIT_SALE,
         x.PRICE_SALE, x.TOTAL_SALE, x.PROFIT_LOSS
 ORDER BY x.TRANS_DATE



 */

/*
 V7:
 ...
 ;WITH cte AS (
    SELECT 
        row_number() OVER (ORDER BY TRANS_DATE, SYMBOL) AS rownum,*
    FROM 
        TBL_P2_EQUITY_TRANS
)
SELECT
    a.TRANS_DATE, a.SYMBOL,  a.UNIT_PURCHASE, a.UNIT_SALE, 
    SUM(b.UNIT_PURCHASE) AS P_SUM,
    SUM(b.UNIT_SALE) AS S_SUM,
    SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as REMAIN_UNIT
FROM  cte a
LEFT JOIN cte b 
ON a.SYMBOL = b.SYMBOL AND b.rownum <= a.rownum
GROUP BY a.SYMBOL, a.TRANS_DATE, a.rownum, a.UNIT_PURCHASE, a.UNIT_SALE
ORDER BY a.SYMBOL, a.TRANS_DATE

*/ 
/*
V8:
==


;WITH cte AS (
    SELECT row_number() OVER (ORDER BY TRANS_DATE) AS rownum, x.*, k.INDUSTRIAL, k.BU 
    FROM TBL_P2_EQUITY_TRANS x, TBL_P2_EQUITY_INDEX  k
    WHERE x.SYMBOL = k.SYMBOL
)
SELECT
    a.TRANS_DATE, a.SYMBOL, a.SECURITIES_NAME, a.INDUSTRIAL, a.BU,
    a.UNIT_PURCHASE, a.PRICE_PURCHASE,a.TOTAL_PURCHASE,
    a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS,
    a.UNIT_DIV, a.TOTAL_DIV, 
    SUM(b.UNIT_PURCHASE) AS P_SUM,
    SUM(b.UNIT_SALE) AS S_SUM,
    SUM(b.UNIT_DIV) AS D_SUM,
    SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as REMAIN_UNIT
     
FROM  cte a 
            
    
LEFT JOIN cte b 

ON a.SYMBOL = b.SYMBOL AND b.rownum <= a.rownum
GROUP BY a.SYMBOL, a.TRANS_DATE, a.rownum, 
    a.UNIT_PURCHASE, a.PRICE_PURCHASE, a.TOTAL_PURCHASE,
    a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS,
    a.UNIT_DIV, a.TOTAL_DIV, a.SECURITIES_NAME,
    a.INDUSTRIAL, a.BU
ORDER BY a.SYMBOL, a.TRANS_DATE


*/
?>
