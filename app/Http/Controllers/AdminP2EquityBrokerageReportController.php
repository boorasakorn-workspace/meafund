<?php
/**
 * FILE: AdminP2EquityBrokerageReportController.php
 * Phase#2 Brokerag Report Controller 
 * Created: 2017/03/23 00:06
 */
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Jenssegers\Date\Date;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;

use App\User;
use App\Libraries\MEAUtils;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class AdminP2EquityBrokerageReportController extends Controller
{
 
    /** 
     * รายงานซื้อ-ขายหลักทรัพย์ (Brokerage Report)
     */
    public function getreport()
    {
        $view_name = 'backend.pages.p2_equity_brokerage_report';
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 62,
            'menu_id' => 4,
            'title' => getMenuName($data,62, 4) . '|  MEA FUND'
        ] );

        
        $allquery = "SELECT * FROM TBL_P2_EQUITY_SECURITIES";
        $equitylist = DB::select(DB::raw($allquery));

 
        $allquery = "SELECT * FROM TBL_P2_EQUITY_CATEGORY ORDER BY INDUSTRIAL";
        $equitycategory = DB::select(DB::raw($allquery));

        /* บริษิท Broker*/
        $allquery = "SELECT * FROM TBL_P2_EQUITY_BROKER ORDER BY NAME_SHT ASC;";
        $equitybroker = DB::select(DB::raw($allquery));

        $allquery = "SELECT SYMBOL, COMP_NAME FROM TBL_P2_EQUITY_INDEX ORDER BY SYMBOL ASC;";
        $equityindex = DB::select(DB::raw($allquery));
  
        $years = array();
        for($i= date('Y'); $i>date('Y')-25; $i--) {
           array_push($years, $i);
        }
        /* บริษัทิจัดการ */
        $securitieslist =  DB::table('TBL_P2_EQUITY_SECURITIES')->orderby("NAME_SHT")->get(); 
      

       
        /////////////
        $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')->orderby("INDUSTRIAL")->get();  
        $sectionList = array( );
        $tmp = array();
        $key = "";

        $child = array();

        foreach ($categorylist as $row) {
            $key = $row->INDUSTRIAL;
            if (array_key_exists($key, $sectionList)) {
                $child = $sectionList[$key];
            } else {
                $child = array();
            }
            array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'BU'=>$row->BU,
                                     'INDUSTRIAL'=>$row->INDUSTRIAL
                                     ));
            $sectionList[$key] = $child;
        }
        
        return view($view_name)->with([
            'equitylist'     => $equitylist,
            'equitycategory' => $equitycategory,
            'equitybroker'   => $equitybroker,
            'years'          => $years,
            'symbols'        => $equityindex,
            'securitieslist' => $securitieslist,
            'sectionList'    => $sectionList
            ]);

    }

    
    public  function  DataSourceCount($ArrParam, $IsCase) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        
        $where = "";

        $broker   = $ArrParam["broker"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        $where = "" ;
               
        if((!empty($broker)) && (strlen($broker) > 0)) { 
            $where .= " A.BROKER_NAME = '".$broker."' ";
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where) > 1)
                $where .= " AND A.SECURITIES_NAME = '".$name_sht."' ";
            else
                $where .= " A.SECURITIES_NAME = '".$name_sht."' ";  
        }

        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where) > 1) 
               $where .= " AND A.TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where .= " A.TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       
   
        
        if(strlen($where) > 1) {
            $where = " WHERE " . $where  . " AND " . "B.NAME_SHT = A.BROKER_NAME "; 
        } else {
            $where = " WHERE B.NAME_SHT = A.BROKER_NAME ";
        }


       // WHERE2
         $where2 = "" ;//
        if((!empty($broker)) && (strlen($broker) > 0)) {  
           // $where2 .= " BROKER_NAME = '".$broker."' ";
            $where2 .= " NAME_SHT = '".$broker."' ";
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND A.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " A.SECURITIES_NAME = '".$name_sht."' ";
        }

        
        
        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND A.TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " A.TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       
       

        if(strlen($where2) > 1) {
            $where2 = " WHERE " . $where2  ; 
        }
     
        $query = ";WITH CTE AS ".
                 " ( " .
                 " SELECT  TRANS_DATE, SECURITIES_NAME, NAME_SHT, BROKER_NAME, [P], [S], [PX], [SX], [S_BRAGE], [P_BRAGE], [P_VAT], [S_VAT], TOTAL_PURCHASE, TOTAL_SALE " .
                 " FROM " . 
                 " ( SELECT TRANS_DATE, SECURITIES_NAME, " . 
                 "      B.NAME_SHT as NAME_SHT, ".
                 "      B.BROKER_NAME as BROKER_NAME, ". 
                 "      TOTAL_BROKERAGE AS TMP, " .
                 "      TOTAL_BROKERAGE, ".
                 "      TOTAL_PURCHASE, ".
                 "      TOTAL_SALE, ".
                 "      BROKERAGE, " .
                 "      VAT, ".
                 "      TYPE as PTYPE, ". 
                 "      CONCAT(TYPE, 'X') AS PPTYPE, " .
                 "      CONCAT(TYPE, '_BRAGE') AS BRAGE_TYPE , ".
                 "      CONCAT(TYPE, '_VAT') AS VAT_TYPE " .
                 "   FROM TBL_P2_EQUITY_TRANS A, TBL_P2_EQUITY_BROKER B ".
                $where .
                 " ) AS PS " . 
                 "   PIVOT (SUM(TMP) for [PPTYPE]  in (PX , SX)) AS PTT " .
                 "   PIVOT (SUM(TOTAL_BROKERAGE) for [PTYPE]  in ([P],[S])) AS PT ".
                 "   PIVOT (SUM(BROKERAGE) for [BRAGE_TYPE]  in ([P_BRAGE], [S_BRAGE])) AS PSBR  " .
                 "   PIVOT (SUM(VAT) for [VAT_TYPE]  in ([P_VAT], [S_VAT])) AS PSVAT " .
                 " )  " .
                 "    " .
                 " SELECT COUNT(*) as TOTAL, " .
                 "        SUM(PURCHASE_BROKERAGE) as SUM_GRAND_TOTAL_PURCHASE_BROKERAGE, ". 
                 "        SUM(SALE_BROKERAGE)     as SUM_GRAND_TOTAL_SALE_BROKERAGE, " .
                 "        SUM(SUM_BROKERAGE)      as SUM_GRAND_TOTAL_BROKERAGE " .
                 " FROM ( ".
                 "    SELECT  SECURITIES_NAME, NAME_SHT, BROKER_NAME, " .
                 "            PURCHASE_BROKERAGE = (  " .
                 "                CASE    " .
                 "                   WHEN SECURITIES_NAME = 'UOBAM' THEN " . 
                 "                      SUM(ISNULL([P_BRAGE], 0))     " . 
                 "                   WHEN SECURITIES_NAME = 'KTAM' THEN  " .
                 "                      (SUM(ISNULL([P_BRAGE], 0)) * (100.00 / 107.00)) " .
                 "                END), " .
                 "                               " .
                 "            SALE_BROKERAGE = ( " .
                 "                CASE  " .
                 "                   WHEN SECURITIES_NAME='UOBAM' THEN  " .
                 "                      SUM(ISNULL([S_BRAGE], 0))  " .
                 "                   WHEN SECURITIES_NAME='KTAM' THEN  " .
                 "                      (SUM(ISNULL([S_BRAGE], 0)) * (100.00 / 107.00)) " .
                 "                END), " .
                 "            SUM_BROKERAGE = ( " . 
                 "                CASE          " . 
                 "                   WHEN SECURITIES_NAME = 'UOBAM' THEN  " .
                 "                      SUM(ISNULL([P_BRAGE], 0.001)) + SUM(ISNULL([S_BRAGE], 0.001)) " .
                 "                   WHEN SECURITIES_NAME = 'KTAM' THEN   " . 
                 "                      (SUM(ISNULL([P_BRAGE], 0)) * (100.00 / 107.00)) + (SUM(ISNULL([S_BRAGE], 0)) * (100.00 / 107.00)) " .
                 "                END), " .   
                 "            SUM(ISNULL([P], 0.001)) AS PURCHASE,  ".
                 "            SUM(ISNULL([S], 0.001)) AS SALE, ". 
                 "            (SUM(ISNULL([P], 0.001)) + SUM(ISNULL([S], 0))) AS SUM_C, " . 
                 "                                 " . 
                 "            GROSS_PURCHASE = (   " .                 
                 "               CASE    " .  
                 "                  WHEN SECURITIES_NAME = 'UOBAM' THEN   " .                     
                 "                     SUM(TOTAL_PURCHASE) -  SUM(ISNULL([P_BRAGE], 0.001)) - SUM(ISNULL([P_VAT], 0))  " .               
                 "                      " .  
                 "                  WHEN SECURITIES_NAME = 'KTAM' THEN " .                       
                 "                     SUM(TOTAL_PURCHASE)  -  (SUM(ISNULL([P_BRAGE], 0.001)) * (100.00 / 107.00))   " .                    
                 "                      " .
                 "                  ELSE  " .                          
                 "                     0  " .                 
                 "               END ),   " .
                 "                        " .
                 "            GROSS_SALE = (       " .          
                 "               CASE     " .  
                 "                  WHEN SECURITIES_NAME='UOBAM' THEN   " .                                                 
                 "                     SUM(TOTAL_SALE) +  SUM(ISNULL([S_BRAGE], 0)) + SUM(ISNULL([S_VAT], 0))    " .
                 "                        " .
                 "                  WHEN SECURITIES_NAME='KTAM' THEN    " .                   
                 "                     SUM(TOTAL_SALE) + (SUM(ISNULL([S_BRAGE], 0.001)) * (100.00 / 107.00))    " .               
                 "                  ELSE   " .                         
                 "                     0   " .               
                 "               END ),    " .
                 "                         " .
                 "            GROSS = (    " .
                 "               CASE      " .
                 "                  WHEN SECURITIES_NAME='UOBAM' THEN ". 
                 "                       SUM(TOTAL_PURCHASE) -  SUM(ISNULL([P_BRAGE], 0)) - SUM(ISNULL([P_VAT], 0))  " .
                 "                       +  " .
                 "                       SUM(TOTAL_SALE) +  SUM(ISNULL([S_BRAGE], 0)) + SUM(ISNULL([S_VAT], 0)) " .
                 "                                         " .         
                 "                  WHEN SECURITIES_NAME='KTAM' THEN " .
                 "                      SUM(TOTAL_PURCHASE)  -  (SUM(ISNULL([P_BRAGE], 0.001)) * (100.00 / 107.00)) " .
                 "                      +  " .
                 "                      SUM(TOTAL_SALE) + (SUM(ISNULL([S_BRAGE], 0.001)) * (100.00 / 107.00)) ".
                 "                  ELSE    " .
                 "                     0    " .
                 "               END ),     " .
                 "                          " .     
                 "            COMM_PURCHASE = (   ".                 
                 "               CASE   " .                          
                 "                  WHEN SECURITIES_NAME='UOBAM' THEN  " .                 
                 "                  (                       " .
                 "                      SUM(ISNULL([P_BRAGE], 0))       ".                   
                 "                       /                              " .
                 "                       (SUM(TOTAL_PURCHASE) -  SUM(ISNULL([P_BRAGE], 0.001)) - SUM(ISNULL([P_VAT], 0.001))) " .
                 "                  ) * 100        ".                                      
                 "                  " .  
                 "                  WHEN SECURITIES_NAME='KTAM' THEN        " .            
                 "                  (                                       " .                     
                 "                      (SUM(ISNULL([P_BRAGE], 0)) * (100.00 / 107.00)) " .                                          
                 "                       /                         " .
                 "                      (                          " .   
                 "                          SUM(TOTAL_PURCHASE)  -  (SUM(ISNULL([P_BRAGE], 0.001)) * (100.00 / 107.00))   " .                                                      
                 "                      )     " .
                 "                  ) * 100   " .   
                 "                            " .   
                 "                  ELSE      " .                      
                 "                      0     " .                
                 "               END ),       " .
                 "                            " .
                 "            COMM_SALE = (   " .                    
                 "                CASE        " .                  
                 "                   WHEN SECURITIES_NAME='UOBAM' THEN   " .                
                 "                   (                                   " .
                 "                       SUM(ISNULL([S_BRAGE], 0))   " .                       
                 "                       /                               " .       
                 "                       (SUM(TOTAL_SALE) -  SUM(ISNULL([S_BRAGE], 0.001)) - SUM(ISNULL([S_VAT], 0.001)))  " .
                 "                                  " .
                 "                   ) * 100        " .                               
                 "                                  " .
                 "                   WHEN SECURITIES_NAME='KTAM' THEN     " .               
                 "                   (                                    " .
                 "                                                        " .
                 "                       (SUM(ISNULL([S_BRAGE], 0)) * (100.00 / 107.00))  " .                                          
                 "                       /                                " .
                 "                       (                                " .
                 "                           SUM(TOTAL_SALE)  -  (SUM(ISNULL([S_BRAGE], 0.001)) * (100.00 / 107.00))  " .                                                       
                 "                       )                                " .
                 "                   ) * 100                              " .
                 "                                                        " .
                 "                   ELSE                                 " .
                 "                     0                                  " .
                 "               END ),                                   " .
                 "                                " .
                 "            COMM_TOTAL = (      " .
                 "               CASE             " .
                 "                  WHEN SECURITIES_NAME='UOBAM' THEN " .
                 "                  (             " .
                 "                      (         " .
                 "                          SUM(ISNULL([P_BRAGE], 0)) + SUM(ISNULL([S_BRAGE], 0))) " .
                 "                          /     " . 
                 "                          ( SUM(TOTAL_PURCHASE) -  SUM(ISNULL([P_BRAGE], 0.001)) - SUM(ISNULL([P_VAT], 0)) " .
                 "                          +     " .
                 "                          SUM(TOTAL_SALE) +  SUM(ISNULL([S_BRAGE], 0.001)) + SUM(ISNULL([S_VAT], 0)) " .
                 "                      )         " .
                 "                  )  * 100      " .
                 "                                " .         
                 "                  WHEN SECURITIES_NAME='KTAM' THEN  " .
                 "                  (             " .
                 "                      (         " .
                 "                          (SUM(ISNULL([P_BRAGE], 0)) * (100.00 / 107.00)) +  (SUM(ISNULL([S_BRAGE], 0)) * (100.00 / 107.00)) " .
                 "                      )         " .
                 "                      /         " .
                 "                      (         " .
                 "                          (     " . 
                 "                             SUM(TOTAL_PURCHASE)  -  (SUM(ISNULL([P_BRAGE], 0.001)) * (100.00 / 107.00))  " .
                 "                             +  " .
                 "                             SUM(TOTAL_SALE) + (SUM(ISNULL([S_BRAGE], 0.001)) * (100.00 / 107.00)) " .
                 "                          )     " .
                 "                      )         " .
                 "                  ) * 100       " .
                 "                                " .
                 "                  ELSE          " .
                 "                      0         " .
                 "               END ),           " .
                 "            COUNT([PX]) AS P_COUNT, " .
                 "            COUNT([SX]) AS S_COUNT, " .
                 "            (COUNT([PX]) +  COUNT([SX])) AS COUNT_C  " .
                 "    FROM CTE A " .
                 " " . $where2 . "  " .
                 "    GROUP BY SECURITIES_NAME, NAME_SHT, BROKER_NAME " . 
                 " ) T  ";
                 //"    ORDER BY SECURITIES_NAME, BROKER_NAME " ;
                
        
        // $r = DB::select(DB::raw($query));

        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }

   
    public  function  DataSource($ArrParam, $IsCase, $ispageing= true) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        
        $where = "";
        if($ispageing){
            $PageSize = $ArrParam['pagesize'];
            $PageNumber = $ArrParam['PageNumber'];
        }

        $broker   = $ArrParam["broker"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        $where = "" ;
               
        if((!empty($broker)) && (strlen($broker) > 0)) { 
            $where .= " A.BROKER_NAME = '".$broker."' ";
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where) > 1)
                $where .= " AND A.SECURITIES_NAME = '".$name_sht."' ";
            else
                $where .= " A.SECURITIES_NAME = '".$name_sht."' ";  
        }

        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where) > 1) 
               $where .= " AND A.TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where .= " A.TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       
   
        
        if(strlen($where) > 1) {
            $where = " WHERE " . $where  . " AND " . "B.NAME_SHT = A.BROKER_NAME "; 
        } else {
            $where = " WHERE B.NAME_SHT = A.BROKER_NAME ";
        }


       // WHERE2
         $where2 = "" ;//
        if((!empty($broker)) && (strlen($broker) > 0)) {  
           // $where2 .= " BROKER_NAME = '".$broker."' ";
            $where2 .= " NAME_SHT = '".$broker."' ";
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND A.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " A.SECURITIES_NAME = '".$name_sht."' ";
        }

        
        
        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND A.TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " A.TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       
       

        if(strlen($where2) > 1) {
            $where2 = " WHERE " . $where2  ; 
        }
     
        $query = ";WITH CTE AS ".
                 " ( " .
                 " SELECT  TRANS_DATE, SECURITIES_NAME, NAME_SHT, BROKER_NAME, [P], [S], [PX], [SX], [S_BRAGE], [P_BRAGE], [P_VAT], [S_VAT], TOTAL_PURCHASE, TOTAL_SALE " .
                 " FROM " . 
                 " ( SELECT TRANS_DATE, SECURITIES_NAME, " . 
                 "      B.NAME_SHT as NAME_SHT, ".
                 "      B.BROKER_NAME as BROKER_NAME, ". 
                 "      TOTAL_BROKERAGE AS TMP, " .
                 "      TOTAL_BROKERAGE, ".
                 "      TOTAL_PURCHASE, ".
                 "      TOTAL_SALE, ".
                 "      BROKERAGE, " .
                 "      VAT, ".
                 "      TYPE as PTYPE, ". 
                 "      CONCAT(TYPE, 'X') AS PPTYPE, " .
                 "      CONCAT(TYPE, '_BRAGE') AS BRAGE_TYPE , ".
                 "      CONCAT(TYPE, '_VAT') AS VAT_TYPE " .
                 "   FROM TBL_P2_EQUITY_TRANS A, TBL_P2_EQUITY_BROKER B ".
                $where .
                 " ) AS PS " . 
                 "   PIVOT (SUM(TMP) for [PPTYPE]  in (PX , SX)) AS PTT " .
                 "   PIVOT (SUM(TOTAL_BROKERAGE) for [PTYPE]  in ([P],[S])) AS PT ".
                 "   PIVOT (SUM(BROKERAGE) for [BRAGE_TYPE]  in ([P_BRAGE], [S_BRAGE])) AS PSBR  " .
                 "   PIVOT (SUM(VAT) for [VAT_TYPE]  in ([P_VAT], [S_VAT])) AS PSVAT " .
                 " )  " .
                 "    " .
                 "    SELECT  SECURITIES_NAME, NAME_SHT, BROKER_NAME, " .
                 //"            SUM(ISNULL([P_BRAGE], 0.001)) AS PURCHASE_BROKERAGE, " .
                 //"            SUM(ISNULL([S_BRAGE], 0.001)) AS SALE_BROKERAGE,     " .
                 //"            (SUM(ISNULL([P_BRAGE], 0.001)) + SUM(ISNULL([S_BRAGE], 0.001))) AS SUM_BROKERAGE, " .
                 "            PURCHASE_BROKERAGE = (  " .
                 "                CASE    " .
                 "                   WHEN SECURITIES_NAME = 'UOBAM' THEN " . 
                 "                      SUM(ISNULL([P_BRAGE], 0))     " . 
                 "                   WHEN SECURITIES_NAME = 'KTAM' THEN  " .
                 "                      (SUM(ISNULL([P_BRAGE], 0)) * (100.00 / 107.00)) " .
                 "                END), " .
                 "                               " .
                 "            SALE_BROKERAGE = ( " .
                 "                CASE  " .
                 "                   WHEN SECURITIES_NAME='UOBAM' THEN  " .
                 "                      SUM(ISNULL([S_BRAGE], 0))  " .
                 "                   WHEN SECURITIES_NAME='KTAM' THEN  " .
                 "                      (SUM(ISNULL([S_BRAGE], 0)) * (100.00 / 107.00)) " .
                 "                END), " .
                 "            SUM_BROKERAGE = ( " . 
                 "                CASE          " . 
                 "                   WHEN SECURITIES_NAME = 'UOBAM' THEN  " .
                 "                      SUM(ISNULL([P_BRAGE], 0.001)) + SUM(ISNULL([S_BRAGE], 0.001)) " .
                 "                   WHEN SECURITIES_NAME = 'KTAM' THEN   " . 
                 "                      (SUM(ISNULL([P_BRAGE], 0)) * (100.00 / 107.00)) + (SUM(ISNULL([S_BRAGE], 0)) * (100.00 / 107.00)) " .
                 "                END), " .   
                 "            SUM(ISNULL([P], 0.001)) AS PURCHASE,  ".
                 "            SUM(ISNULL([S], 0.001)) AS SALE, ". 
                 "            (SUM(ISNULL([P], 0.001)) + SUM(ISNULL([S], 0))) AS SUM_C, " . 
                 "                                 " . 
                 "            GROSS_PURCHASE = (   " .                 
                 "               CASE    " .  
                 "                  WHEN SECURITIES_NAME = 'UOBAM' THEN   " .                     
                 "                     SUM(TOTAL_PURCHASE) -  SUM(ISNULL([P_BRAGE], 0.001)) - SUM(ISNULL([P_VAT], 0))  " .               
                 "                      " .  
                 "                  WHEN SECURITIES_NAME = 'KTAM' THEN " .                       
                 "                     SUM(TOTAL_PURCHASE)  -  (SUM(ISNULL([P_BRAGE], 0.001)) * (100.00 / 107.00))   " .                    
                 "                      " .
                 "                  ELSE  " .                          
                 "                     0  " .                 
                 "               END ),   " .
                 "                        " .
                 "            GROSS_SALE = (       " .          
                 "               CASE     " .  
                 "                  WHEN SECURITIES_NAME='UOBAM' THEN   " .                                                 
                 "                     SUM(TOTAL_SALE) +  SUM(ISNULL([S_BRAGE], 0)) + SUM(ISNULL([S_VAT], 0))    " .
                 "                        " .
                 "                  WHEN SECURITIES_NAME='KTAM' THEN    " .                   
                 "                     SUM(TOTAL_SALE) + (SUM(ISNULL([S_BRAGE], 0.001)) * (100.00 / 107.00))    " .               
                 "                  ELSE   " .                         
                 "                     0   " .               
                 "               END ),    " .
                 "                         " .
                 "            GROSS = (    " .
                 "               CASE      " .
                 "                  WHEN SECURITIES_NAME='UOBAM' THEN ". 
                 "                       SUM(TOTAL_PURCHASE) -  SUM(ISNULL([P_BRAGE], 0)) - SUM(ISNULL([P_VAT], 0))  " .
                 "                       +  " .
                 "                       SUM(TOTAL_SALE) +  SUM(ISNULL([S_BRAGE], 0)) + SUM(ISNULL([S_VAT], 0)) " .
                 "                                         " .         
                 "                  WHEN SECURITIES_NAME='KTAM' THEN " .
                 "                      SUM(TOTAL_PURCHASE)  -  (SUM(ISNULL([P_BRAGE], 0.001)) * (100.00 / 107.00)) " .
                 "                      +  " .
                 "                      SUM(TOTAL_SALE) + (SUM(ISNULL([S_BRAGE], 0.001)) * (100.00 / 107.00)) ".
                 "                  ELSE    " .
                 "                     0    " .
                 "               END ),     " .
                 "                          " .     
                 "            COMM_PURCHASE = (   ".                 
                 "               CASE   " .                          
                 "                  WHEN SECURITIES_NAME='UOBAM' THEN  " .                 
                 "                  (                       " .
                 "                      SUM(ISNULL([P_BRAGE], 0))       ".                   
                 "                       /                              " .
                 "                       (SUM(TOTAL_PURCHASE) -  SUM(ISNULL([P_BRAGE], 0.001)) - SUM(ISNULL([P_VAT], 0.001))) " .
                 "                  ) * 100        ".                                      
                 "                  " .  
                 "                  WHEN SECURITIES_NAME='KTAM' THEN        " .            
                 "                  (                                       " .                     
                 "                      (SUM(ISNULL([P_BRAGE], 0)) * (100.00 / 107.00)) " .                                          
                 "                       /                         " .
                 "                      (                          " .   
                 "                          SUM(TOTAL_PURCHASE)  -  (SUM(ISNULL([P_BRAGE], 0.001)) * (100.00 / 107.00))   " .                                                      
                 "                      )     " .
                 "                  ) * 100   " .   
                 "                            " .   
                 "                  ELSE      " .                      
                 "                      0     " .                
                 "               END ),       " .
                 "                            " .
                 "            COMM_SALE = (   " .                    
                 "                CASE        " .                  
                 "                   WHEN SECURITIES_NAME='UOBAM' THEN   " .                
                 "                   (                                   " .
                 "                       SUM(ISNULL([S_BRAGE], 0))   " .                       
                 "                       /                               " .       
                 "                       (SUM(TOTAL_SALE) -  SUM(ISNULL([S_BRAGE], 0.001)) - SUM(ISNULL([S_VAT], 0.001)))  " .
                 "                                  " .
                 "                   ) * 100        " .                               
                 "                                  " .
                 "                   WHEN SECURITIES_NAME='KTAM' THEN     " .               
                 "                   (                                    " .
                 "                                                        " .
                 "                       (SUM(ISNULL([S_BRAGE], 0)) * (100.00 / 107.00))  " .                                          
                 "                       /                                " .
                 "                       (                                " .
                 "                           SUM(TOTAL_SALE)  -  (SUM(ISNULL([S_BRAGE], 0.001)) * (100.00 / 107.00))  " .                                                       
                 "                       )                                " .
                 "                   ) * 100                              " .
                 "                                                        " .
                 "                   ELSE                                 " .
                 "                     0                                  " .
                 "               END ),                                   " .
                 "                                " .
                 "            COMM_TOTAL = (      " .
                 "               CASE             " .
                 "                  WHEN SECURITIES_NAME='UOBAM' THEN " .
                 "                  (             " .
                 "                      (         " .
                 "                          SUM(ISNULL([P_BRAGE], 0)) + SUM(ISNULL([S_BRAGE], 0))) " .
                 "                          /     " . 
                 "                          ( SUM(TOTAL_PURCHASE) -  SUM(ISNULL([P_BRAGE], 0.001)) - SUM(ISNULL([P_VAT], 0)) " .
                 "                          +     " .
                 "                          SUM(TOTAL_SALE) +  SUM(ISNULL([S_BRAGE], 0.001)) + SUM(ISNULL([S_VAT], 0)) " .
                 "                      )         " .
                 "                  )  * 100      " .
                 "                                " .         
                 "                  WHEN SECURITIES_NAME='KTAM' THEN  " .
                 "                  (             " .
                 "                      (         " .
                 "                          (SUM(ISNULL([P_BRAGE], 0)) * (100.00 / 107.00)) +  (SUM(ISNULL([S_BRAGE], 0)) * (100.00 / 107.00)) " .
                 "                      )         " .
                 "                      /         " .
                 "                      (         " .
                 "                          (     " . 
                 "                             SUM(TOTAL_PURCHASE)  -  (SUM(ISNULL([P_BRAGE], 0.001)) * (100.00 / 107.00))  " .
                 "                             +  " .
                 "                             SUM(TOTAL_SALE) + (SUM(ISNULL([S_BRAGE], 0.001)) * (100.00 / 107.00)) " .
                 "                          )     " .
                 "                      )         " .
                 "                  ) * 100       " .
                 "                                " .
                 "                  ELSE          " .
                 "                      0         " .
                 "               END ),           " .
                 "            COUNT([PX]) AS P_COUNT, " .
                 "            COUNT([SX]) AS S_COUNT, " .
                 "            (COUNT([PX]) +  COUNT([SX])) AS COUNT_C  " .
                 "    FROM CTE A " .
                 " " . $where2 . "  " .
                 "    GROUP BY SECURITIES_NAME, NAME_SHT, BROKER_NAME " . 
                 "    ORDER BY SECURITIES_NAME, BROKER_NAME " ;
                
        
        if($ispageing){
           $query .= " OFFSET " . $PageSize . " * (" . $PageNumber . " - 1) ROWS FETCH NEXT " . $PageSize . " ROWS ONLY OPTION (RECOMPILE) ";
        }

        // $r = DB::select(DB::raw($query));

        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }
/*
    public  function  DataSourceExport($ArrParam, $IsCase, $ispageing= true) {
        Log::info(print_r($ArrParam, true));
         
        ini_set('max_execution_time', 30000);
      
        ini_set('memory_limit', '-1');

        
      
        if($ispageing){
            $PageSize = $ArrParam['pagesize'];
            $PageNumber = $ArrParam['PageNumber'];
        }

        $broker   = $ArrParam["broker"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

    

       // WHERE2
         $where2 = "" ;//
        if((!empty($broker)) && (strlen($broker) > 0)) {  
            $where2 .= " BROKER_NAME = '".$broker."' ";
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " SECURITIES_NAME = '".$name_sht."' ";
        }

        
        
        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       

        if(strlen($where2) > 1) {
            $where2 = " WHERE " . $where2 ; 
        }
     
        $query = ";WITH CTE AS ".
                 " ( " .
                 " SELECT  TRANS_DATE, SECURITIES_NAME, BROKER_NAME, [P], [S], [PX], [SX] " .
                 " FROM " . 
                 " ( SELECT TRANS_DATE, SECURITIES_NAME, BROKER_NAME, TOTAL_BROKERAGE AS TMP, " .
                 "      TOTAL_BROKERAGE , TYPE as PTYPE, CONCAT(TYPE, 'X') AS PPTYPE " .
                 "   FROM TBL_P2_EQUITY_TRANS ".
                 " ) AS PS " . 
                 "   PIVOT (SUM(TMP) for [PPTYPE]  in (PX , SX)) AS PTT " .
                 "   PIVOT (SUM(TOTAL_BROKERAGE) for [PTYPE]  in ([P],[S])) AS PT ".
                 " ) " .
                 " " .
                 "    SELECT  SECURITIES_NAME, BROKER_NAME, SUM(ISNULL([P], 0)) AS PURCHASE,  ".
                 "            SUM(ISNULL([S], 0)) AS SALE , (SUM(ISNULL([P], 0)) + SUM(ISNULL([S], 0))) AS SUM_C, " . 
                // "            (SUM(ISNULL([P], 0)) + SUM(ISNULL([S], 0))) AS SUM_C, ".
                 "            COUNT([PX]) AS P_COUNT, COUNT([SX]) AS S_COUNT, ".
                 "            (COUNT([PX]) +  COUNT([SX])) AS COUNT_C  ".
                 "    FROM  CTE A " .
                 " " . $where2 . "  " .
                 "    GROUP BY SECURITIES_NAME, BROKER_NAME " . 
                 "    ORDER BY SECURITIES_NAME, BROKER_NAME " ;
                
        
       // if($ispageing){
       //     $query .= " OFFSET " . $PageSize . " * (" . $PageNumber . " - 1) ROWS FETCH NEXT " . $PageSize . " ROWS ONLY OPTION (RECOMPILE) ";
       // }
        $r = DB::select(DB::raw($query));

        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }
*/

public  function  DataSourceGrandTotal($ArrParam) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        
        $where = "";
        
        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        
        // NEED TO SPLIT BU + INDUSTRIAL 
        $industrial = $ArrParam["industrial"];  
        $bu = '';
        $indus = '';

        if((!empty($industrial)) && (strlen($industrial) > 0)) {
            $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')
                           ->orderby("INDUSTRIAL")
                           ->where('CATE_ID','=', $industrial)->get();
            foreach ($categorylist as $row) {
                $indus = $row->INDUSTRIAL;
                $bu    = $row->BU;
                break;  
            }
        }

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        
        $where = "" ;//
        
               
        if((!empty($symbol)) && (strlen($symbol) > 0)) { // && $check_name== "true"){
            $where .= " x.SYMBOL = '".$symbol."' ";
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where) > 1)
                $where .= " AND x.SECURITIES_NAME = '".$name_sht."' ";
            else
                $where .= " x.SECURITIES_NAME = '".$name_sht."' ";  
        }

        if((!empty($indus)) && (strlen($indus) > 0)) {  
            if(strlen($where) > 1)
               $where .= " AND k.INDUSTRIAL  = '".$indus."' ";
            else
               $where .= " k.INDUSTRIAL  = '".$indus."' "; 
        }

        if((!empty($bu)) && (strlen($bu) > 0)) {  
            if(strlen($where) > 1)
                $where .= " AND k.BU  = '".$bu."' ";
            else
                $where .= " k.BU  = '".$bu."' ";  
        }
        
  
        if(strlen($where) > 1) {
            $where = " AND " . $where  ; 
        }


       // WHERE2
        $where2 = "" ;//
        if((!empty($symbol)) && (strlen($symbol) > 0)) { 
            $where2 .= " SYMBOL = '".$symbol."' ";
      
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " SECURITIES_NAME = '".$name_sht."' ";
        }

        if((!empty($indus)) && (strlen($indus) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND INDUSTRIAL  = '".$indus."' ";
            else
               $where2 .= " INDUSTRIAL  = '".$indus."' "; 
        }

        if((!empty($bu)) && (strlen($bu) > 0)) {  
            if(strlen($where2) > 1) 
                $where2 .= " AND BU  = '".$bu."' ";
            else
                $where2 .= " BU  = '".$bu."' ";  
        }
        
        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       

        if(strlen($where2) > 1) {
            $where2 = " WHERE " . $where2  ; 
        }

        Log::info('indus='.$indus . ', bu=' .$bu . ', date_start=' . $date_start);

        $query =  " ;WITH cte AS ( " .
                  "     SELECT " . 
                  "         row_number() OVER (ORDER BY TRANS_DATE) AS rownum, x.*, " . 
                  "         k.INDUSTRIAL, k.BU " . 
                  "     FROM " . 
                  "         TBL_P2_EQUITY_TRANS x, TBL_P2_EQUITY_INDEX  k " . 
                  "     WHERE x.SYMBOL = k.SYMBOL " . 
                    $where .
                  " ) " .
                  " SELECT " .
                  "         SUM(TOTAL_PURCHASE) AS J,  " .
                  "         SUM(TOTAL_SALE) AS N,      " .
                  "         SUM(PROFIT_LOSS) AS O      " .
                  " FROM ( " .
                  "     SELECT ".
                  "         a.TRANS_DATE, a.SYMBOL, a.TYPE, a.SECURITIES_NAME, a.INDUSTRIAL, a.BU, ".
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE,a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, ". 
                  "         SUM(b.UNIT_PURCHASE) AS P_SUM, " .
                  "         SUM(b.UNIT_SALE) AS S_SUM, " .
                  "         SUM(b.UNIT_DIV) AS D_SUM, " .
                  "         SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as REMAIN_UNIT " .    
                  "     FROM cte a " . 
                  "     LEFT JOIN cte b " . 
                  "     ON a.SYMBOL = b.SYMBOL AND b.rownum <= a.rownum " .
                  "     GROUP BY " . 
                  "         a.SYMBOL, a.TYPE, a.TRANS_DATE, a.rownum, " .
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE, a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, a.SECURITIES_NAME, " .
                  "         a.INDUSTRIAL, a.BU " .
                  " ) T " .
                  $where2 ;
                  
        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }

public  function DataSourceBarChart($ArrParam) {
    Log::info(print_r($ArrParam, true));

     /* limit execution timeout */
    ini_set('max_execution_time', 30000);
    /* unlimit memory size */
    ini_set('memory_limit', '-1');

 
        $broker   = $ArrParam["broker"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        $where = "" ;
               
        if((!empty($broker)) && (strlen($broker) > 0)) { 
            $where .= " A.BROKER_NAME = '".$broker."' ";
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where) > 1)
                $where .= " AND A.SECURITIES_NAME = '".$name_sht."' ";
            else
                $where .= " A.SECURITIES_NAME = '".$name_sht."' ";  
        }

        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where) > 1) 
               $where .= " AND A.TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where .= " A.TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       
   
        
        if(strlen($where) > 1) {
            $where = " WHERE " . $where  . " AND " . "B.NAME_SHT = A.BROKER_NAME "; 
        } else {
            $where = " WHERE B.NAME_SHT = A.BROKER_NAME ";
        }


       // WHERE2
         $where2 = "" ;//
        if((!empty($broker)) && (strlen($broker) > 0)) {  
           // $where2 .= " BROKER_NAME = '".$broker."' ";
            $where2 .= " NAME_SHT = '".$broker."' ";
        }

        if((!empty($name_sht)) && (strlen($name_sht) > 0)) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND A.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " A.SECURITIES_NAME = '".$name_sht."' ";
        }

        
        
        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND A.TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " A.TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       
       

        if(strlen($where2) > 1) {
            $where2 = " WHERE " . $where2  ; 
        }
     
        $query = ";WITH CTE AS ".
                 " ( " .
                 " SELECT  TRANS_DATE, SECURITIES_NAME, NAME_SHT, BROKER_NAME, [P], [S], [PX], [SX], [S_BRAGE], [P_BRAGE], [P_VAT], [S_VAT], TOTAL_PURCHASE, TOTAL_SALE " .
                 " FROM " . 
                 " ( SELECT TRANS_DATE, SECURITIES_NAME, " . 
                 "      B.NAME_SHT as NAME_SHT, ".
                 "      B.BROKER_NAME as BROKER_NAME, ". 
                 "      TOTAL_BROKERAGE AS TMP, " .
                 "      TOTAL_BROKERAGE, ".
                 "      TOTAL_PURCHASE, ".
                 "      TOTAL_SALE, ".
                 "      BROKERAGE, " .
                 "      VAT, ".
                 "      TYPE as PTYPE, ". 
                 "      CONCAT(TYPE, 'X') AS PPTYPE, " .
                 "      CONCAT(TYPE, '_BRAGE') AS BRAGE_TYPE , ".
                 "      CONCAT(TYPE, '_VAT') AS VAT_TYPE " .
                 "   FROM TBL_P2_EQUITY_TRANS A, TBL_P2_EQUITY_BROKER B ".
                $where .
                 " ) AS PS " . 
                 "   PIVOT (SUM(TMP) for [PPTYPE]  in (PX , SX)) AS PTT " .
                 "   PIVOT (SUM(TOTAL_BROKERAGE) for [PTYPE]  in ([P],[S])) AS PT ".
                 "   PIVOT (SUM(BROKERAGE) for [BRAGE_TYPE]  in ([P_BRAGE], [S_BRAGE])) AS PSBR  " .
                 "   PIVOT (SUM(VAT) for [VAT_TYPE]  in ([P_VAT], [S_VAT])) AS PSVAT " .
                 " )  " .
                 "    " .
                  "    " .
                // " SELECT COUNT(*) as TOTAL, " .
                // "        SUM(PURCHASE_BROKERAGE) as SUM_GRAND_TOTAL_PURCHASE_BROKERAGE, ". 
                // "        SUM(SALE_BROKERAGE)     as SUM_GRAND_TOTAL_SALE_BROKERAGE, " .
                // "        SUM(SUM_BROKERAGE)      as SUM_GRAND_TOTAL_BROKERAGE " .
                 " SELECT SECURITIES_NAME, ".
                 "        BROKER_NAME,  " . 
                 "        NAME_SHT," .
                 "        SUM(SUM_BROKERAGE) as SUM_C  " .
                 " FROM ( ".
                 "    SELECT  SECURITIES_NAME, NAME_SHT, BROKER_NAME, " .
                 "            PURCHASE_BROKERAGE = (  " .
                 "                CASE    " .
                 "                   WHEN SECURITIES_NAME = 'UOBAM' THEN " . 
                 "                      SUM(ISNULL([P_BRAGE], 0))     " . 
                 "                   WHEN SECURITIES_NAME = 'KTAM' THEN  " .
                 "                      (SUM(ISNULL([P_BRAGE], 0)) * (100.00 / 107.00)) " .
                 "                END), " .
                 "                               " .
                 "            SALE_BROKERAGE = ( " .
                 "                CASE  " .
                 "                   WHEN SECURITIES_NAME='UOBAM' THEN  " .
                 "                      SUM(ISNULL([S_BRAGE], 0))  " .
                 "                   WHEN SECURITIES_NAME='KTAM' THEN  " .
                 "                      (SUM(ISNULL([S_BRAGE], 0)) * (100.00 / 107.00)) " .
                 "                END), " .
                 "            SUM_BROKERAGE = ( " . 
                 "                CASE          " . 
                 "                   WHEN SECURITIES_NAME = 'UOBAM' THEN  " .
                 "                      SUM(ISNULL([P_BRAGE], 0.001)) + SUM(ISNULL([S_BRAGE], 0.001)) " .
                 "                   WHEN SECURITIES_NAME = 'KTAM' THEN   " . 
                 "                      (SUM(ISNULL([P_BRAGE], 0)) * (100.00 / 107.00)) + (SUM(ISNULL([S_BRAGE], 0)) * (100.00 / 107.00)) " .
                 "                END), " .   
                 "            SUM(ISNULL([P], 0.001)) AS PURCHASE,  ".
                 "            SUM(ISNULL([S], 0.001)) AS SALE, ". 
                 "            (SUM(ISNULL([P], 0.001)) + SUM(ISNULL([S], 0))) AS SUM_C, " . 
                 "                                 " . 
                 "            GROSS_PURCHASE = (   " .                 
                 "               CASE    " .  
                 "                  WHEN SECURITIES_NAME = 'UOBAM' THEN   " .                     
                 "                     SUM(TOTAL_PURCHASE) -  SUM(ISNULL([P_BRAGE], 0.001)) - SUM(ISNULL([P_VAT], 0))  " .               
                 "                      " .  
                 "                  WHEN SECURITIES_NAME = 'KTAM' THEN " .                       
                 "                     SUM(TOTAL_PURCHASE)  -  (SUM(ISNULL([P_BRAGE], 0.001)) * (100.00 / 107.00))   " .                    
                 "                      " .
                 "                  ELSE  " .                          
                 "                     0  " .                 
                 "               END ),   " .
                 "                        " .
                 "            GROSS_SALE = (       " .          
                 "               CASE     " .  
                 "                  WHEN SECURITIES_NAME='UOBAM' THEN   " .                                                 
                 "                     SUM(TOTAL_SALE) +  SUM(ISNULL([S_BRAGE], 0)) + SUM(ISNULL([S_VAT], 0))    " .
                 "                        " .
                 "                  WHEN SECURITIES_NAME='KTAM' THEN    " .                   
                 "                     SUM(TOTAL_SALE) + (SUM(ISNULL([S_BRAGE], 0.001)) * (100.00 / 107.00))    " .               
                 "                  ELSE   " .                         
                 "                     0   " .               
                 "               END ),    " .
                 "                         " .
                 "            GROSS = (    " .
                 "               CASE      " .
                 "                  WHEN SECURITIES_NAME='UOBAM' THEN ". 
                 "                       SUM(TOTAL_PURCHASE) -  SUM(ISNULL([P_BRAGE], 0)) - SUM(ISNULL([P_VAT], 0))  " .
                 "                       +  " .
                 "                       SUM(TOTAL_SALE) +  SUM(ISNULL([S_BRAGE], 0)) + SUM(ISNULL([S_VAT], 0)) " .
                 "                                         " .         
                 "                  WHEN SECURITIES_NAME='KTAM' THEN " .
                 "                      SUM(TOTAL_PURCHASE)  -  (SUM(ISNULL([P_BRAGE], 0.001)) * (100.00 / 107.00)) " .
                 "                      +  " .
                 "                      SUM(TOTAL_SALE) + (SUM(ISNULL([S_BRAGE], 0.001)) * (100.00 / 107.00)) ".
                 "                  ELSE    " .
                 "                     0    " .
                 "               END ),     " .
                 "                          " .     
                 "            COMM_PURCHASE = (   ".                 
                 "               CASE   " .                          
                 "                  WHEN SECURITIES_NAME='UOBAM' THEN  " .                 
                 "                  (                       " .
                 "                      SUM(ISNULL([P_BRAGE], 0))       ".                   
                 "                       /                              " .
                 "                       (SUM(TOTAL_PURCHASE) -  SUM(ISNULL([P_BRAGE], 0.001)) - SUM(ISNULL([P_VAT], 0.001))) " .
                 "                  ) * 100        ".                                      
                 "                  " .  
                 "                  WHEN SECURITIES_NAME='KTAM' THEN        " .            
                 "                  (                                       " .                     
                 "                      (SUM(ISNULL([P_BRAGE], 0)) * (100.00 / 107.00)) " .                                          
                 "                       /                         " .
                 "                      (                          " .   
                 "                          SUM(TOTAL_PURCHASE)  -  (SUM(ISNULL([P_BRAGE], 0.001)) * (100.00 / 107.00))   " .                                                      
                 "                      )     " .
                 "                  ) * 100   " .   
                 "                            " .   
                 "                  ELSE      " .                      
                 "                      0     " .                
                 "               END ),       " .
                 "                            " .
                 "            COMM_SALE = (   " .                    
                 "                CASE        " .                  
                 "                   WHEN SECURITIES_NAME='UOBAM' THEN   " .                
                 "                   (                                   " .
                 "                       SUM(ISNULL([S_BRAGE], 0))   " .                       
                 "                       /                               " .       
                 "                       (SUM(TOTAL_SALE) -  SUM(ISNULL([S_BRAGE], 0.001)) - SUM(ISNULL([S_VAT], 0.001)))  " .
                 "                                  " .
                 "                   ) * 100        " .                               
                 "                                  " .
                 "                   WHEN SECURITIES_NAME='KTAM' THEN     " .               
                 "                   (                                    " .
                 "                                                        " .
                 "                       (SUM(ISNULL([S_BRAGE], 0)) * (100.00 / 107.00))  " .                                          
                 "                       /                                " .
                 "                       (                                " .
                 "                           SUM(TOTAL_SALE)  -  (SUM(ISNULL([S_BRAGE], 0.001)) * (100.00 / 107.00))  " .                                                       
                 "                       )                                " .
                 "                   ) * 100                              " .
                 "                                                        " .
                 "                   ELSE                                 " .
                 "                     0                                  " .
                 "               END ),                                   " .
                 "                                " .
                 "            COMM_TOTAL = (      " .
                 "               CASE             " .
                 "                  WHEN SECURITIES_NAME='UOBAM' THEN " .
                 "                  (             " .
                 "                      (         " .
                 "                          SUM(ISNULL([P_BRAGE], 0)) + SUM(ISNULL([S_BRAGE], 0))) " .
                 "                          /     " . 
                 "                          ( SUM(TOTAL_PURCHASE) -  SUM(ISNULL([P_BRAGE], 0.001)) - SUM(ISNULL([P_VAT], 0)) " .
                 "                          +     " .
                 "                          SUM(TOTAL_SALE) +  SUM(ISNULL([S_BRAGE], 0.001)) + SUM(ISNULL([S_VAT], 0)) " .
                 "                      )         " .
                 "                  )  * 100      " .
                 "                                " .         
                 "                  WHEN SECURITIES_NAME='KTAM' THEN  " .
                 "                  (             " .
                 "                      (         " .
                 "                          (SUM(ISNULL([P_BRAGE], 0)) * (100.00 / 107.00)) +  (SUM(ISNULL([S_BRAGE], 0)) * (100.00 / 107.00)) " .
                 "                      )         " .
                 "                      /         " .
                 "                      (         " .
                 "                          (     " . 
                 "                             SUM(TOTAL_PURCHASE)  -  (SUM(ISNULL([P_BRAGE], 0.001)) * (100.00 / 107.00))  " .
                 "                             +  " .
                 "                             SUM(TOTAL_SALE) + (SUM(ISNULL([S_BRAGE], 0.001)) * (100.00 / 107.00)) " .
                 "                          )     " .
                 "                      )         " .
                 "                  ) * 100       " .
                 "                                " .
                 "                  ELSE          " .
                 "                      0         " .
                 "               END ),           " .
                 "            COUNT([PX]) AS P_COUNT, " .
                 "            COUNT([SX]) AS S_COUNT, " .
                 "            (COUNT([PX]) +  COUNT([SX])) AS COUNT_C  " .
                 "    FROM CTE A " .
                 " " . $where2 . "  " .
                 "    GROUP BY SECURITIES_NAME, NAME_SHT, BROKER_NAME " . 
                 " ) T  " .
                 " GROUP BY SECURITIES_NAME, NAME_SHT, BROKER_NAME " . 
              //   " ORDER BY SECURITIES_NAME, NAME_SHT, BROKER_NAME " ;
                "    ORDER BY SECURITIES_NAME, BROKER_NAME " ;
                
        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));

    }

    public function ajax_report_search(Request $request)
    {
        Log::info(get_class($this) .'::'. __FUNCTION__);

        $data = getmemulist();
        $title = getMenuName($data,62, 4);

        //==> $ar = $this->retrive_customize_field(62, 4);

        $view_name = 'backend.pages.ajax.ajax_p2_equity_brokerage_report';
        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $broker = $request->input('broker');
        $name_sht = $request->input('name_sht');
       // $industrial = $request->input('industrial');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $ArrParam = array();

        //{ pagesize: "10", PageNumber: 1, symbol: "", name_sht: "", industrial: "", date_start: "", date_end: "" }
        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;

        $ArrParam["broker"]   = $broker;
        $ArrParam["name_sht"] = $name_sht;
        //$ArrParam["industrial"] =$industrial;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;

        $data = null;
        $totals= 0;
        $grandtoal = 0;
        
        $utils = new MEAUtils();
      
        $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
        $thai_date_end =  (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";

        Log::info(get_class($this) .'::'. __FUNCTION__. ' ' .  
            ' converted date: ' . $date_start .' to ' . $thai_date_start . ', ' . $date_end . ' to '. $thai_date_end );
        
        $pretty_date = $utils->prettyPrintThaiDate($thai_date_start, $thai_date_end, "red");

        $securitieslist =  DB::table('TBL_P2_EQUITY_SECURITIES')
                          ->where("NAME_SHT", "=", $name_sht )
                          ->orderby("NAME_SHT")->get(); 

        
        $all = $this->DataSourceCount($ArrParam, true);
        
        $totals = $all[0]->TOTAL; 
        $count_all = 0; //$all[0]->COUNT_ALL;
        $sum_all  =  $all[0]->SUM_GRAND_TOTAL_BROKERAGE;
        $sum_grand_total_purchase_brokerage  =  $all[0]->SUM_GRAND_TOTAL_PURCHASE_BROKERAGE;
        $sum_grand_total_sale_brokerage  =  $all[0]->SUM_GRAND_TOTAL_SALE_BROKERAGE;

        $data = $this->DataSource($ArrParam, true);

        $htmlPaginate =Paginatre_gen($totals, $PageSize,'page_click_search', $PageNumber);

        $SecuritiesName = "";
        foreach ($securitieslist as $row) {
           $SecuritiesName = $row->SECURITIES_NAME;
           break;
        }

        $returnHTML = view($view_name)->with([
                'htmlPaginate'=> $htmlPaginate,
                'data' => $data,
                'totals' => $totals,
    
                'count_all' => $count_all,
                'sum_all' => $sum_all,

                'sum_grand_total_brokerage' => $sum_all,

                'sum_grand_total_purchase_brokerage' => $sum_grand_total_purchase_brokerage,
                'sum_grand_total_sale_brokerage' => $sum_grand_total_sale_brokerage,

                'grandtoal' => $grandtoal,  
                'PageSize' =>$PageSize,
                'PageNumber' =>$PageNumber,
                'date_start' => $thai_date_start,
                'date_end'   => $thai_date_end,
                'pretty_date' => $pretty_date,
                'TableTitle' => $title,
                'SecuritiesName' => $SecuritiesName

                //'fields' => $column,
                //'fields_poperties' => $prop_hdr

            ])->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }



    public function ajax_chart_search(Request $request) {
        Log::info(get_class($this) .'::'. __FUNCTION__);
         
        $data = getmemulist();
        $title = getMenuName($data,62, 4);

        $ar = $this->retrive_customize_field(62, 4);

        // $view_name = 'backend.pages.ajax.ajax_p2_equity_trading_report';

        $broker = $request->input('broker');
        $name_sht = $request->input('name_sht');
        
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');



        $ArrParam = array();

        //{ pagesize: "10", PageNumber: 1, symbol: "", name_sht: "", industrial: "", date_start: "", date_end: "" }
        $ArrParam["pagesize"] ="";
        $ArrParam["PageNumber"] ="";

        $ArrParam["broker"] =$broker;
        $ArrParam["name_sht"] =$name_sht;
       // $ArrParam["industrial"] =$industrial;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;


        $data =null;
        $totals= 0;

        $chartds = $this->DataSourceBarChart($ArrParam);
       // $chartds = $this->DataSourceCount($ArrParam, true);
      
        $utils = new MEAUtils();
        $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
        $thai_date_end =  (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";
        
        Log::info(get_class($this) .'::'. __FUNCTION__.
            ' converted date: ' . $date_start .' to ' . $thai_date_start . ', ' . $date_end . ' to '. $thai_date_end );
        $data  = array();
        $data1 = array();
        $data2 = array();
     
        $pretty_date = $utils->prettyPrintThaiDate($thai_date_start, $thai_date_end, "");
        $returnHTML = [
                'data' => $chartds,
                'date_start' => $thai_date_start,
                'date_end' => $thai_date_end,
                'pretty_date' => $pretty_date
        ];
                      

        return response()->json(array('success' => true, 'result'=>$returnHTML));
    }


    public function ajax_report_search_export() {
        
        $menu = getmemulist();
        $title = getMenuName($menu, 62, 4);

        $broker = Input::get('broker');
        $name_sht = Input::get('name_sht');
        $date_start = Input::get('date_start');
        $date_end = Input::get('date_end');

        $ArrParam = array();
        $ArrParam["pagesize"] = "50000";
        $ArrParam["PageNumber"] ="1";
        $ArrParam["broker"] =$broker;
        $ArrParam["name_sht"] =$name_sht;

        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;

     
        $utils = new MEAUtils();
        $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
        $thai_date_end =  (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";

        Log::info(' converted date: ' . $date_start .' to ' . $thai_date_start . ', ' . $date_end . ' to '. $thai_date_end );
        

        $securitieslist =  DB::table('TBL_P2_EQUITY_SECURITIES')
                          ->where("NAME_SHT", "=", $name_sht )
                          ->orderby("NAME_SHT")->get(); 
        $SecuritiesName = "";
        foreach ($securitieslist as $row) {
           $SecuritiesName = $row->SECURITIES_NAME;
           break;
        } 
        

        Log::info('Export Parameters:' .  print_r($ArrParam, true));
        $all = $this->DataSourceCount($ArrParam, true);
    

        $totals    = $all[0]->TOTAL; 
        $count_all = 0;  
        $sum_all   = $all[0]->SUM_GRAND_TOTAL_BROKERAGE;
        $sum_grand_total_purchase_brokerage  =  $all[0]->SUM_GRAND_TOTAL_PURCHASE_BROKERAGE;
        $sum_grand_total_sale_brokerage      =  $all[0]->SUM_GRAND_TOTAL_SALE_BROKERAGE;
        $data = $this->DataSource($ArrParam, false);

        $results = array();
        foreach ($data as $item) {
            $results[] = (array)$item;
        }

// $sum_grand_total_purchase_brokerage, $sum_grand_total_sale_brokerage) {
        //$sum_grand_total_purchase_brokerage, $sum_grand_total_sale_brokerage) {
        Excel::create($title, function ($excel) use ($results, $ArrParam, $title, $SecuritiesName, $count_all, $sum_all, $totals) {
            

            $excel->sheet("ค่านายหน้า", function ($sheet) use ($results, $ArrParam, $title, $SecuritiesName, $count_all, $sum_all, $totals) {
               
                
                // first row styling and writing content
                $sheet->setStyle([
                    'borders' => [
                        'allborders' => [
                            'color' => [
                                'rgb' => 'b3b6b7'
                            ]
                        ]
                    ]
                ]);
                $sheet->mergeCells('A1:M1');
                $sheet->mergeCells('A2:M2');
                $sheet->mergeCells('A3:M3');
                $sheet->mergeCells('A4:M4');

                //$sheet->mergeCells('A5:A7');
                //$sheet->mergeCells('B5:B7');

                $sheet->mergeCells('C5:D6');
                $sheet->mergeCells('I5:J5');
                
                
                $sheet->mergeCells('C6:D6');  // รายซื้อบริษัทิ Broker
                $sheet->mergeCells('E5:J5');  //  ค่านายหน้า
                $sheet->mergeCells('E6:G6');  //  ซื้อ
 

                $sheet->mergeCells('H6:J6');  // ขาย
                $sheet->mergeCells('K6:M6');  // รวม


                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Cordia New');
                    $row->setFontSize(20);
                    $row->setFontWeight('bold');
                    $row->setAlignment('center');
                    $row->setBackground('#b2babb');
                    // Set all borders (top, right, bottom, left)
                    $row->setBorder('thin', 'thin', 'none', 'thin');

                });

                $sheet->row(2, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'none', 'thin');
                });

                $sheet->row(3, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'thin', 'thin');

                });
                $sheet->row(4, function ($row) { 
                    $row->setAlignment('center');
                    //$row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'thin', 'thin');

                });



                $sheet->row(1, array($title));

                if($ArrParam["date_start"] != "" && $ArrParam["date_end"] != ""){
                    $sheet->row(2, array('ในช่วงวันที่ ' . get_date_notime($ArrParam["date_start"]) . ' ถึง ' . get_date_notime($ArrParam["date_end"])   ));
                }

                
                $sheet->row(3, array('รายงานข้อมูล ณ วันที่ ' . get_date_notime(date("Y-m-d H:i:s"))));
                $sheet->row(4, array($SecuritiesName));


                $sheet->cell('A5', function($cell) {
                    $cell->setValue('');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $sheet->cell('B5', function($cell) {
                    $cell->setValue('');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $sheet->cell('C5', function($cell) {
                    $cell->setValue('รายชื่อบริษัท Broker');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $sheet->cell('E5', function($cell) {
                    $cell->setValue('ค่านายหน้า');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $sheet->cell('K5', function($cell) {
                    $cell->setValue('รวม');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $sheet->cell('J5', function($cell) {
                    $cell->setValue('อัตราค่านายหน้า %');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
              
                $sheet->cell('A6', function($cell) {
                    $cell->setValue('ลำดับ');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $sheet->cell('B6', function($cell) {
                    $cell->setValue('บลจ.');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $sheet->cell('E6', function($cell) {
                    $cell->setValue('ชื้อ');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $sheet->cell('H6', function($cell) {
                    $cell->setValue('ขาย');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $header2[]   = null;
                $header2[0]  = "";  // ลำดับ
                $header2[1]  = "";
                $header2[2]  = "ชื่อย่อ";
                $header2[3]  = "ชื่อ";
                

                $header2[4]  = "จำนวนครั้ง";
                $header2[5]  = "จำนวนเงิน";
                $header2[6]  = "% Commission";

                $header2[7]  = "จำนวนครั้ง";
                $header2[8]  = "จำนวนเงิน";
                $header2[9]  = "% Commission";

                $header2[10]  = "จำนวนครั้ง";
                $header2[11]  = "จำนวนเงิน";
                $header2[12]  = "% Commission";

                $header2[13]  = "% Broker";
                $sheet->row(7, $header2);

                $LAST_ROW = 5 + 1;

                // Set multiple column formats
                $sheet->setColumnFormat(array(
                        //'A' => '@',
                        'B' => '@',
                        'C' => '@',

                        
                        //'D' => '@',
                        'E' => '@',
                        //'F' => '@',
                        'G' => '#,##0.00', // SUM(TOTAL_PURCHASE)
                        //'H' => '@',  
                        'I' => '@',
                        'J' => '#,##0.00', // SUM(TOTAL_SALE)
                   
                        //'K' => '#,##0.00',
                        'L' => '#,##0.00', // SUM(PROFIT_LOSS)
                        'M' => '#,##0.00'  
                ));

                $rscount = count($results);   

                $idx = 0; 
                $N = 0.0;
                $J = 0.0;
                $O = 0.0;
                $P_COUNT = 0;
                $S_COUNT = 0;
                $PURCHASE = 0.0;
                $SALE = 0.0;
                $CSUM = 0.0;
                $C = 0;
                $PERCENT = 0.0;
                  
                $item = 0;
                $HEADER_ROWS = 7 + 1;

                $PUR_COMM = 0.0;
                $SALE_COMM = 0.0;
                $SUM_BROKERAGE = 0.0;

                $PURCHASE_BROKERAGE = 0.0;
                $SALE_BROKERAGE = 0.0;


             

                foreach ($results as $item) {

                    $item++;
                    $P_COUNT       += $item['P_COUNT'];
                    $PURCHASE_BROKERAGE      += $item['PURCHASE_BROKERAGE'];
                    $PUR_COMM      += $item['COMM_PURCHASE'];

                    $S_COUNT       += $item['S_COUNT'];
                    $SALE_BROKERAGE          += $item['SALE_BROKERAGE'];
                    $SALE_COMM     += $item['COMM_SALE'];

                    $C             += ($item['P_COUNT']  + $item['S_COUNT']);
                    $SUM_BROKERAGE += $item['SUM_BROKERAGE']; //
                    $CSUM          += $item['COMM_TOTAL'];     
                  
                    $PERCENT       = ($sum_all / $sum_all) * 100;

                    $rs[]   = null; 
                    $rs[0]  = (int)($idx + 1); // A
                    $rs[1]  = $item['SECURITIES_NAME'];    // บลจ. security name (UOBAM/KTAM)
                    $rs[2]  = $item['NAME_SHT'];           // ชื่อย่อ (BLS / CPF)
                    $rs[3]  = $item['BROKER_NAME'];        // broker name 
 
                    $rs[4]  = $item['P_COUNT'];            // purchase transaction count
                    $rs[5]  = $item['PURCHASE_BROKERAGE']; // purchase brokerage 
                    $rs[6]  = $item['COMM_PURCHASE'];      // % commission (purchase)

                    $rs[7]  = $item['S_COUNT'];            // sale transaction count
                    $rs[8]  = $item['SALE_BROKERAGE'];     // sale brokerage
                    $rs[9]  = $item['COMM_SALE'];          // % commission (sale)

                    $rs[10] = ($item['P_COUNT'] + $item['S_COUNT']);
                    $rs[11] = $item['SUM_BROKERAGE'];      
                    $rs[12] = $item['COMM_TOTAL'] ;       // % commission (purchase / sale)
                    $rs[13] = ((($item['PURCHASE_BROKERAGE'] + $item['SALE_BROKERAGE']) / $sum_all) * 100); // number_format(((($item['PURCHASE'] + $item['SALE']) / $sum_all) * 100), 2, '.', ',');
                   

                    $sheet->appendRow($rs);
                    $idx++;
                }

                $sheet->cell('D'. strval($idx + $HEADER_ROWS), function($cell) {
                        $cell->setValue("รวม");
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                if($rscount > 0) {
                    
                    $sheet->cell('E'. strval($idx+$HEADER_ROWS), function($cell) use ($P_COUNT ) {
                        $cell->setValue($P_COUNT );
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell('F'. strval($idx+$HEADER_ROWS), function($cell) use ($PURCHASE_BROKERAGE ) {
                        $cell->setValue($PURCHASE_BROKERAGE );
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell('G'. strval($idx+$HEADER_ROWS), function($cell) use ($PURCHASE_BROKERAGE ) {
                       // $cell->setValue('');
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });


                    $sheet->cell('H'. strval($idx+$HEADER_ROWS), function($cell) use ($S_COUNT) {
                        $cell->setValue($S_COUNT);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell('I'. strval($idx+$HEADER_ROWS), function($cell) use ($SALE_BROKERAGE) {
                        $cell->setValue($SALE_BROKERAGE);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');                     
                    });

                    $sheet->cell('J'. strval($idx+$HEADER_ROWS), function($cell) use ($SALE_BROKERAGE) {
                        //$cell->setValue('');
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');                     
                    });

                    $sheet->cell('K'. strval($idx+$HEADER_ROWS), function($cell) use ($C) {
                        $cell->setValue($C);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell('L'. strval($idx+$HEADER_ROWS), function($cell) use ($SUM_BROKERAGE ) {
                        $cell->setValue($SUM_BROKERAGE );
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell('M'. strval($idx+$HEADER_ROWS), function($cell) use ($CSUM) {
                       // $cell->setValue($CSUM);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell('N'. strval($idx+$HEADER_ROWS), function($cell) use ($PERCENT) {
                        $cell->setValue($PERCENT);
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                } 
                
            });

        })->download('xls');
    }

    public function ajax_store_customize_field(Request $request) {
        $status = false;
        $result = array();
        $store_path = storage_path() .'/public/import';
        try {
            $menu_id = $request->input('menu_id');  //eg. 60
            $submenu_id = $request->input('submenu_id');
            $utils = new MEAUtils();
            $user = $utils->getLoggedInUser(); 
            $json = $request->input('options');
            $options = array('options'=>$json);
            $file = $store_path . '/' . $user. '-' . $menu_id . '-' . $submenu_id. '.json';
            file_put_contents($file, json_encode($options, TRUE));
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Saved:' . $file );
            array_push($results, $file);

        } catch (\Exception $e) {
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Exception:' .$e->getMessage());
            $status = false;
        } 
        return response()->json(array('success' => $status, 'result'=>$result));
    }
    
    public function retrive_customize_field($menu_id, $submenu_id) {
        $status = false;
        $result = array();
        $store_path = storage_path() .'/public/import';
        try {
            $utils = new MEAUtils(); 
            $user = $utils->getLoggedInUser(); 
            $file = $store_path . '/' . $user . '-' . $menu_id . '-' . $submenu_id. '.json';
            $json = json_decode(file_get_contents($file), true);
            array_push($result, $json);

            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Retrived:' . $file );
            $status = true;
        } catch (\Exception $e) {
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Exception:' .$e->getMessage());
            $status = false;
        } 
        return array('success' => $status, 'result'=>$result);
    } 

    public function ajax_retrive_customize_field(Request $request) {
        
        $menu_id = $request->input('menu_id');  // eg. 60
        $submenu_id = $request->input('submenu_id');
          
        $r = $this->retrive_customize_field($menu_id, $submenu_id);
        return response()->json($r);
    }
}?>
