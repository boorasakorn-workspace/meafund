<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Jenssegers\Date\Date;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Support\Facades\Log;
use App\Libraries\MEAUtils;

class AdminP2EquityClosedReportController extends Controller
{

    /**
     * Helper functions
     */
    public function toThaiDateTime($strDate, $withTime) {
        $utils = new MEAUtils();
        return $utils->toThaiDateTime($strDate, $withTime) ;
    }
    /** 
     * รายงานซื้อ-ขายหลักทรัพย์ (Trading Report)
     */
    public function getreport()
    {
        $view_name = 'backend.pages.p2_equity_closed_report';
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 62,
            'menu_id' => 5,
            'title' => getMenuName($data,62, 5) . '|  MEA FUND'
        ] );

      /*  $allquery = "SELECT * FROM TBL_P2_EQUITY_SECURITIES";
        $equitylist = DB::select(DB::raw($allquery));

        $allquery = "SELECT * FROM TBL_P2_EQUITY_CATEGORY";
        $equitycategory = DB::select(DB::raw($allquery));

        
        $allquery = "SELECT * FROM TBL_P2_EQUITY_BROKER";
        $equitybroker = DB::select(DB::raw($allquery));

        return view($view_name)->with([
            'equitylist'     => $equitylist,
            'equitycategory' => $equitycategory,
            'equitybroker' => $equitybroker
            ]);
*/
        $allquery = "SELECT * FROM TBL_P2_EQUITY_SECURITIES";
        $equitylist = DB::select(DB::raw($allquery));

        $allquery = "SELECT * FROM TBL_P2_EQUITY_CATEGORY ORDER BY INDUSTRIAL";
        $equitycategory = DB::select(DB::raw($allquery));

        /* บริษิท Broker*/
        $allquery = "SELECT * FROM TBL_P2_EQUITY_BROKER ORDER BY NAME_SHT ASC;";
        $equitybroker = DB::select(DB::raw($allquery));

        $allquery = "SELECT SYMBOL, COMP_NAME FROM TBL_P2_EQUITY_INDEX ORDER BY SYMBOL ASC;";
        $equityindex = DB::select(DB::raw($allquery));
  
        /*$years = array();
        for($i= date('Y'); $i>date('Y')-25; $i--) {
           array_push($years, $i);
        }*/

        $allquery = "SELECT MAX(YEAR(REFERENCE_DATE)+1) AS YMAX, " .
                    "       MIN(YEAR(REFERENCE_DATE)-1) AS YMIN ".
                    "FROM TBL_P2_EQUITY_CLOSED_INDEX ";
        $rs = DB::select(DB::raw($allquery));
        
        
        $years = array();
        foreach ($rs as $k => $v) {
            for($i=$v->YMIN; $i <=$v->YMAX; $i++) {
                array_push($years, $i);
            }
            break;
        }

        /* บริษัทิจัดการ */
        $securitieslist =  DB::table('TBL_P2_EQUITY_SECURITIES')->orderby("NAME_SHT")->get(); 
      
        /////////////
        $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')->orderby("INDUSTRIAL")->get();  
        $sectionList = array( );
        $tmp = array();
        $key = "";

        $child = array();

        foreach ($categorylist as $row) {
            $key = $row->INDUSTRIAL;

            if (array_key_exists($key, $sectionList)) {
                $child = $sectionList[$key];
            } else {
                $child = array();
            }

            array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'INDUSTRIAL'=>$row->INDUSTRIAL,
                                     'BU'=>$row->BU
                                     ));
            $sectionList[$key] = $child;
        }
        $sectionList[$key] = $child;

        /*
        foreach ($categorylist as $row) {
            if($key != $row->INDUSTRIAL) {
               if($key == "") {
                   $child = array();
               } else {
                  $sectionList[$key] = $child;
                  $child = array();
               }
            } 
            $key = $row->INDUSTRIAL;
            array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'BU'=>$row->BU,
                                     'INDUSTRIAL'=>$row->INDUSTRIAL
                                     ));
        }
        $sectionList[$key] = $child;
        */

        return view($view_name)->with([
            'equitylist'     => $equitylist,
            'equitycategory' => $equitycategory,
            'equitybroker'   => $equitybroker,
            'years'          => $years,
            'symbols'        => $equityindex,
            'securitieslist' => $securitieslist,
            'sectionList'    => $sectionList
            ]);

    }

   public  function  DataSourceCount($ArrParam,$IsCase){

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];
        /*
        industrial : industrial,
                name_sht :name_sht,
                symbol : symbol,
                date_start:date_start,
                date_end:date_end,
        */


        $industrial = $ArrParam['industrial'];
        $cateid = $ArrParam['cateid'];
        $name_sht = $ArrParam['name_sht'];
        $symbol = $ArrParam['symbol'];
        $date_start = $ArrParam['date_start'];
        $date_end = $ArrParam['date_end'];

        $sel_month = $ArrParam['sel_month'];
        $sel_year = $ArrParam['sel_year'];

    
        $allquery= "";

    
        // => EXEC dbo.SP_ClosedIndex @PageSize = 20, @PageIndex=1, @TYear=2017, @TMonth=6 , @CateID='', @Symbol='CPF', @NameSht='UOBAM' 

        $allquery ='EXEC SP_ClosedIndexRecordCount ' .
                '  @TYear=' . $sel_year . 
                ', @TMonth=' . $sel_month ;

        if(!empty($cateid) && (strlen($cateid) > 0))      
                $allquery .= ', @CateId=' . $cateid;

        if(!empty($symbol) && (strlen($symbol) > 0))        
                $allquery .= ', @Symbol=' . $symbol;  

        if(!empty($name_sht) && (strlen($name_sht) > 0))      
                $allquery .= ', @NameSht=' . $name_sht ;          
         
        Log::info($allquery);

        $all = DB::select(DB::raw($allquery));
        //return  $all[0]->total;
        return  $all[0];
    }

    public  function  DataSource($ArrParam, $IsCase, $ispageing= true){

        $where = "";
        if($ispageing){
            $PageSize = $ArrParam['pagesize'];
            $PageNumber = $ArrParam['PageNumber'];
        }

        $industrial = $ArrParam['industrial'];
        $cateid = $ArrParam['cateid'];
        $name_sht = $ArrParam['name_sht'];
        $symbol = $ArrParam['symbol'];
        $date_start = $ArrParam['date_start'];
        $date_end = $ArrParam['date_end'];

        $sel_month = $ArrParam['sel_month'];
        $sel_year = $ArrParam['sel_year'];

        $query= "";
        
        // => EXEC dbo.SP_ClosedIndex @PageSize = 20, @PageIndex=1, @TYear=2017, @TMonth=6 , @CateID='', @Symbol='CPF', @NameSht='UOBAM' 
        $query ='EXEC SP_ClosedIndex ' .
                '  @PageSize='. $PageSize.
                ', @PageIndex='. $PageNumber .
                ', @TYear=' . $sel_year . 
                ', @TMonth=' . $sel_month ;
        
        if(!empty($cateid) && (strlen($cateid) > 0))      
                $query .= ', @CateId=' . $cateid;

        if(!empty($symbol) && (strlen($symbol) > 0))        
                $query .= ', @Symbol=' . $symbol;  

        if(!empty($name_sht) && (strlen($name_sht) > 0))      
                $query .= ', @NameSht=' . $name_sht ;          
        Log::info($query);
        // echo $query;die();
        return DB::select($query);
    }

     public  function  DataSourceExport($ArrParam, $IsCase, $ispageing= false){
        $where = "";
       

        $PageSize = 20000;
        $PageNumber = 1;

        $industrial = $ArrParam['industrial'];
        $cateid = $ArrParam['cateid'];
        $name_sht = $ArrParam['name_sht'];
        $symbol = $ArrParam['symbol'];
        $date_start = $ArrParam['date_start'];
        $date_end = $ArrParam['date_end'];

        $sel_month = $ArrParam['sel_month'];
        $sel_year = $ArrParam['sel_year'];

        $query = "";

        // => EXEC dbo.SP_ClosedIndex @PageSize = 20, @PageIndex=1, @TYear=2017, @TMonth=6 , @CateID='', @Symbol='CPF', @NameSht='UOBAM' 
        $query ='EXEC SP_ClosedIndex ' .
                '  @PageSize='. $PageSize.
                ', @PageIndex='. $PageNumber .
                ', @TYear=' . $sel_year . 
                ', @TMonth=' . $sel_month ;

        if(!empty($cateid) && (strlen($cateid) > 0))      
                $query .= ', @CateId=' . $cateid;

        if(!empty($symbol) && (strlen($symbol) > 0))        
                $query .= ', @Symbol=' . $symbol;  

        if(!empty($name_sht) && (strlen($name_sht) > 0))      
                $query .= ', @NameSht=' . $name_sht ;          
         
        Log::info($query);

        return DB::select($query);
    }

    public function ajax_report_search(Request $request)
    {
        
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $view_name = 'backend.pages.ajax.ajax_p2_equity_closed_report';
        
        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $industrial = $request->input('industrial');
        $cateid = $request->input('industrial');
        
        $symbol = $request->input('symbol');
        $name_sht = $request->input('name_sht');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $sel_month = $request->input('sel_month');
        $sel_year = $request->input('sel_year');

        $TableTitle = $request->input('table_title');
        
        $ArrParam = array();
        $ArrParam["pagesize"]   = $PageSize;
        $ArrParam["PageNumber"] = $PageNumber;
        $ArrParam["industrial"] = $industrial;
        $ArrParam["cateid"]     = $cateid;
        $ArrParam["name_sht"]   = $name_sht;
        $ArrParam["symbol"]     = $symbol;
        $ArrParam["date_start"] = $date_start;
        $ArrParam["date_end"]   = $date_end;

        $ArrParam["sel_month"]  = $sel_month;
        $ArrParam["sel_year"]   = $sel_year; //

        $data =null;
        $rss = null;
        $totals= 0;
        $SumTotalValueEOM = 0.0;
        
        

        $allquery = "SELECT DAY(EOMONTH('" . $sel_year . "-" . $sel_month . "-01')) AS LAST_DAY; " ;
        Log::debug("ajax_report_search.allquery -> ".$allquery);
        
        $config = DB::select(DB::raw($allquery));
        

        $data = $this->DataSource($ArrParam,true);
        
        $rss = $this->DataSourceCount($ArrParam,true);
        $totals = $rss->total;
        $SumTotalValueEOM = $rss->SumTotalValueEOM;

        $htmlPaginate =Paginatre_gen($totals, $PageSize,'page_click_search',$PageNumber);

        $strMonth = $sel_month ;
        $strYear = $sel_year + 543 ;

        $thaiMonthArray = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $period = "เดือน " . $thaiMonthArray[$strMonth] . " " . $strYear;

        $subtitle = "";
        if($name_sht=='UOBAM')
            $subtitle = "บริษัทหลักทรัพย์จัดการกองทุน ยูโอบี (ประเทศไทย) จํากัด ";
        else if($name_sht=='KTAM')
            $subtitle = "บริษัทหลักทรัพย์จัดการกองทุน กรุงไทย จำกัด (มหาชน)";
        
        Log::info(get_class($this) .'::'. __FUNCTION__. ' ->Return HTML ($PageSize, $PageNumber)' );
        $returnHTML = view($view_name)->with([
            'htmlPaginate'=> $htmlPaginate,
            'data' => $data,
            'totals' => $totals,
            'SumTotalValueEOM' => $SumTotalValueEOM,
            'PageSize' =>$PageSize,
            'PageNumber' =>$PageNumber,
            'period' => $period,
            'TableTitle' => $TableTitle,
            'Subtitle' => $subtitle,
            'config' => $config

        ])->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }

    public function ajax_report_search_export() {

        Log::info(get_class($this) .'::'. __FUNCTION__);
        $ArrParam = array();
        $ArrParam["pagesize"]   = "";
        $ArrParam["PageNumber"] = "";
        $ArrParam["industrial"] = Input::get("industrial");
        $ArrParam["cateid"]     = Input::get("industrial");
        $ArrParam["name_sht"]   = Input::get("name_sht");
        $ArrParam["symbol"]     = Input::get("symbol");
        $ArrParam["date_start"] = Input::get("date_start");
        $ArrParam["date_end"]   = Input::get("date_end");
        $ArrParam["sel_month"]  = Input::get('sel_month');
        $ArrParam["sel_year"]   = Input::get('sel_year');

        $data = $this->DataSourceExport($ArrParam, true, false);
        $results = array();
        foreach ($data as $item) {
            $results[] = (array)$item;
        }

        $rss = $this->DataSourceCount($ArrParam,true);
        $totals = $rss->total;
        $SumTotalValueEOM = $rss->SumTotalValueEOM;

        //$htmlPaginate =Paginatre_gen($totals, $PageSize,'page_click_search',$PageNumber);

        $strMonth = $ArrParam["sel_month"];
        $strYear = $ArrParam["sel_year"] + 543;

        $thaiMonthArray = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $period = "เดือน " . $thaiMonthArray[$strMonth] . " " .  $strYear ;

        $subtitle = "";
        if($ArrParam["name_sht"]=='UOBAM')
            $subtitle = "บริษัทหลักทรัพย์จัดการกองทุน ยูโอบี (ประเทศไทย) จํากัด ";
        else if($ArrParam["name_sht"]=='KTAM')
            $subtitle = "บริษัทหลักทรัพย์จัดการกองทุน กรุงไทย จำกัด (มหาชน)";
        
        Log::info(get_class($this) .'::'. __FUNCTION__. ' ' .  $subtitle);
        
        Excel::create('ExcelExport', function ($excel) use ($results, $data, $ArrParam, $strMonth, $strYear, $subtitle, $period , $SumTotalValueEOM){

            $excel->sheet('Sheetname', function ($sheet) use ($results, $data, $ArrParam, $strMonth, $strYear, $subtitle, $period , $SumTotalValueEOM){

                $LASTDAY = 1;
                foreach($data as $index =>$field) {
                    $LASTDAY = $field->LAST_DAY;
                    break;
                }
                //$LASTDAY = 29;


                if($LASTDAY == 28) {
                    $sheet->mergeCells('A1:AQ1');
                    $sheet->mergeCells('A2:AQ2');
                    $sheet->mergeCells('A3:AQ3');
                    $sheet->mergeCells('A4:AQ4');
                    $sheet->mergeCells('A5:AQ5');
                } else if($LASTDAY == 29) {
                    $sheet->mergeCells('A1:AR1');
                    $sheet->mergeCells('A2:AR2');
                    $sheet->mergeCells('A3:AR3');
                    $sheet->mergeCells('A4:AR4');
                    $sheet->mergeCells('A5:AR5');
                } else if($LASTDAY == 30) {
                    $sheet->mergeCells('A1:AS1');
                    $sheet->mergeCells('A2:AS2');
                    $sheet->mergeCells('A3:AS3');
                    $sheet->mergeCells('A4:AS4');
                    $sheet->mergeCells('A5:AS5');
                } else if($LASTDAY == 31) {
                    $sheet->mergeCells('A1:AT1');
                    $sheet->mergeCells('A2:AT2');
                    $sheet->mergeCells('A3:AT3');
                    $sheet->mergeCells('A4:AT4');
                    $sheet->mergeCells('A5:AT5');
                    
                } 

                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Comic Sans MS');
                    $row->setFontSize(20);
                    $row->setAlignment('center');
                    $row->setBackground('#F2F2F2');
                    // Set all borders (top, right, bottom, left)
                    $row->setBorder('none', 'none', 'none', 'none');
                });


                $sheet->row(2, function ($row) {
                    $row->setAlignment('center');
                    $row->setBackground('#F2F2F2');
                    $row->setBorder('none', 'none', 'none', 'none');
                });
                $sheet->row(3, function ($row) {
                    $row->setAlignment('center');
                    $row->setBackground('#F2F2F2');
                    $row->setBorder('none', 'none', 'none', 'none');
                });

                $sheet->row(4, function ($row) {
                    $row->setAlignment('center');
                    $row->setBackground('#F2F2F2');

                    $row->setBorder('none', 'none', 'none', 'none');
                });
                $sheet->row(5, function ($row) {
                    $row->setAlignment('center');
                    $row->setBackground('#F2F2F2');
                   //$row->setBorder('none', 'none', 'none', 'none');
                });
                /*
                $sheet->row(6, function ($row) {
                    $row->setAlignment('center');
                    $row->setBackground('#F2F2F2');
                   // $row->setBorder('thin', 'thin', 'none', 'thin','#B6B6B6');
                     
                });
                $sheet->row(7, function ($row) {
                    $row->setAlignment('center');
                    $row->setBackground('#F2F2F2');
                   // $row->setBorder('none', 'thin', 'thin', 'thin', '#B6B6B6');
                     
                });
*/
                


                $sheet->row(1, array('รายงานความเคลื่อนไหวราคาตราสารทุน'));
                $sheet->row(2, array('กองทุนสํารองเลี้ยงชีพพนักงานการไฟฟ้านครหลวง ซึ่งจดทะเบียนแล้ว'));
                $sheet->row(3, array("สรุปรายละเอียดหุ้น " .  $period));
                $sheet->row(4, array($subtitle));
                
                $sheet->row(5, array('รายงานข้อมูล ณ วันที่ ' . get_date_notime(date("Y-m-d H:i:s"))));


               // $LASTDAY = 28;
                $header[] = null;
                $sheet->mergeCells('D6:E6');
                $sheet->mergeCells('F6:G6');
                
                if($LASTDAY == 28) {
                    $sheet->mergeCells('I6:AK6');
                    $sheet->setWidth(array(
                        'AL' => 10,
                        'AM' => 12,
                        'AN' => 15,
                        'AO' => 20,
                        'AP' => 20,
                        'AQ' => 15    
                    ));


                    $sheet->mergeCells('AL6:AL7');
                    $sheet->mergeCells('AM6:AM7');
                    $sheet->mergeCells('AN6:AN7');
                    $sheet->mergeCells('AO6:AO7');
                    $sheet->mergeCells('AP6:AP7');
                    $sheet->mergeCells('AQ6:AQ7');


                    
                }
                else if($LASTDAY == 29) {
                    $sheet->mergeCells('I6:AL6');
                     $sheet->setWidth(array(
                        'AM' => 10,
                        'AN' => 12,
                        'AO' => 15,
                        'AP' => 20,
                        'AQ' => 20,
                        'AR' => 15
                        ));

                    $sheet->mergeCells('AM6:AM7');
                    $sheet->mergeCells('AN6:AN7');
                    $sheet->mergeCells('AO6:AO7');
                    $sheet->mergeCells('AP6:AP7');
                    $sheet->mergeCells('AQ6:AQ7');
                    $sheet->mergeCells('AR6:AR7');
                     
                }
                else if($LASTDAY == 30) {
                    $sheet->mergeCells('I6:AM6');

                    $sheet->setWidth(array(
                        'AN' => 10,
                        'AO' => 12,
                        'AP' => 15,
                        'AQ' => 20,
                        'AR' => 20,
                        'AS' => 15
                        ));
                     

                    $sheet->mergeCells('AN6:AN7');
                    $sheet->mergeCells('AO6:AO7');
                    $sheet->mergeCells('AP6:AP7');
                    $sheet->mergeCells('AQ6:AQ7');
                    $sheet->mergeCells('AR6:AR7');
                    $sheet->mergeCells('AS6:AS7');

                    
                    
                }
                else if($LASTDAY == 31) {
                    $sheet->mergeCells('I6:AN6');

                    $sheet->setWidth(array(
                        'AO' => 10,
                        'AP' => 12,
                        'AQ' => 15,
                        'AR' => 15,
                        'AS' => 15,
                        'AT' => 15
                        ));
 
                    $sheet->mergeCells('AO6:AO7');
                    $sheet->mergeCells('AP6:AP7');
                    $sheet->mergeCells('AQ6:AQ7');
                    $sheet->mergeCells('AR6:AR7');
                    $sheet->mergeCells('AS6:AS7');
                    $sheet->mergeCells('AT6:AT7');
 
                }

                $sheet->mergeCells('A6:A7');
                $sheet->mergeCells('B6:B7');
                $sheet->mergeCells('C6:C7');
                $sheet->mergeCells('H6:H7');
                $sheet->mergeCells('F6:G6');

                $header[] = null;
                $header[0] = "กลุ่มอุตสาหกรรม";        // A
                $header[1] = "หลักทรัพย์";             // B
                $header[2] = "จํานวนหน่วย";            // C
                $header[3] = "วันที่ลงทุน";             // D,E
                $header[4] = "";      // F,G
                $header[5] = "ราคาเฉลี่ยต่อเดือน";      // F,G
                $header[6] = "";

                $header[7] = "จํานวนเงินลงทุน(ล้านบาท)";   // H
                /******************************************************************************************************************************** */
                /*
                Author        : Chalermpol Chueayen (chalermpols@msn.com)
                Date Modified : 2019-03-19
                Purpose       : Modify wording 
                */
                $header[8] = " ราคาปิดของหุ้น " . $period; // I
                //$header[9] = "มูลค่ารวม";
                /******************************************************************************************************************************** */
                
                for($i =1; $i <= $LASTDAY; $i++) {
                    $header[8+$i] = "";
                }
                $idx = 8+$LASTDAY ;

                $header[$idx] = ""; //"มูลค่ารวม";
                
                $header[$idx + 1] = "ร้อยล่ะของหุ้น";   // H
                $header[$idx + 2] = "เงินลงทุน(เพิ่ม/ลด)"; // I
                $header[$idx + 3] = "EARNING PURCHASE";  
                $header[$idx + 4] = "PRICE EARNING RATIO";
                $header[$idx + 5] = "PRICE PER BLOCK VALUE";
                $header[$idx + 6] = "DIVIDEND YIELD %";
              
                $sheet->row(6, $header);
                $sheet->row(6, function ($row) {$row->setAlignment('center'); });
                
                $sheet->cells('A7:B7', function($cells) {
                        $cells->setBackground('#b2babb');
                });
                //////

                $headerx[] = null;
                $headerx[0] = "กลุ่มอุตสาหกรรม";     // A
                $headerx[1] = "หลักทรัพย์";          // B
                $headerx[2] = "จํานวนหน่วย";         // C

                $headerx[3] = "เริ่มลงทุน";          // D
                $headerx[4] = "วันสุดท้าย";          // 
                $headerx[5] = "วันแรก";             // F
                $headerx[6] = "วันล่าสุด";           // 
                $headerx[7] = "จํานวนเงินลงทุน(ล้านบาท)"; // G
               
                for($i =1; $i <= $LASTDAY; $i++) {
                    $headerx[7+$i] = $i;
                    
                    
                }

                $idx = 7+$LASTDAY+1;
                $headerx[$idx ] = "มูลค่ารวม";
         /*       $headerx[$idx + 1] = "ร้อยล่ะของหุ้น";   // H
                $headerx[$idx + 2] = "เงินลงทุน(เพิ่ม/ลด)"; // I
                $headerx[$idx + 3] = "EARNING PURCHASE";  
                $headerx[$idx + 4] = "PRICE EARNING RATIO";
                $headerx[$idx + 5] = "PRICE PER BLOCK VALUE";
                $headerx[$idx + 6] = "DIVIDEND YIELD %";*/

                $sheet->row(7, $headerx);
                $sheet->row(7, function ($row) {$row->setAlignment('center');});

               
                foreach($data as $index =>$field) {
                
                    $ZZZ = $field->GIG / 1000000; 
                    $INVEST_NET = abs($ZZZ) -  $field->TotalValueEOM;
              
                    $LAST_PRICE = ($field->TOTAL_SALE - $field->TOTAL_PURCHASE ) / $field->FINAL_REMAIN_UNIT;

                    ///////
                    $rec[] = null;
 
                    $rec[0] = $field->INDUSTRIAL; // <!-- INDUSTRIAL -->
                    $rec[1] = $field->SYMBOL;   // <!-- SYMBOL -->
                    $rec[2] = number_format((float)$field->FINAL_REMAIN_UNIT, 2, '.', ','); //  <!-- P/E -->
                    $rec[3] = $this->toThaiDateTime($field->FIRST_TRANS_DATE, false); // <!--P/BV-->
                    $rec[4] = $this->toThaiDateTime($field->LAST_TRANS_DATE, false);  // <!-- Div Yield -->
                    $rec[5] = number_format((float)$field->FIRST_PRICE_PURCHASE, 2, '.', ',');  
                    $rec[6] = number_format((float)$LAST_PRICE,  2, '.', ','); // <!-- PClosed -->
               
                    if($ZZZ > 0)
                       $rec[7] = number_format((float)$ZZZ,  2, '.', ','); //  <!-- จำนวนเงินลงทุน -->
                    else
                       $rec[7] =number_format((float)$ZZZ,  2, '.', ','); //   <!-- จำนวนเงินลงทุน -->
                
                    $rec[ 8]  = number_format((float)$field->A1, 2, '.', ','); // <!--P/BV-->
                    $rec[ 9]  = number_format((float)$field->A2, 2, '.', ','); // </td> <!-- Div Yield -->
                    $rec[10] = number_format((float)$field->A3, 2, '.', ','); // </td> <!-- EPS-->
                    $rec[11] = number_format((float)$field->A4, 2, '.', ','); // </td>       <!-- PClosed -->
                    $rec[12] = number_format((float)$field->A5, 2, '.', ','); // </td>   <!-- P/E -->
                    $rec[13] = number_format((float)$field->A3, 2, '.', ',');  // <!--P/BV-->
                    $rec[14] = number_format((float)$field->A7, 2, '.', ',');  // <!-- Div Yield -->
                    $rec[15] = number_format((float)$field->A8, 2, '.', ',');  // <!-- EPS-->
                    $rec[16] = number_format((float)$field->A9, 2, '.', ',');  // <!-- EPS-->
                    $rec[17] = number_format((float)$field->A10, 2, '.', ','); // <!-- EPS-->
                    $rec[18] = number_format((float)$field->A11, 2, '.', ','); // <!--P/BV-->
                    $rec[19] = number_format((float)$field->A12, 2, '.', ','); // <!-- Div Yield -->
                    $rec[20] = number_format((float)$field->A13, 2, '.', ','); // <!-- EPS-->
                    $rec[21] = number_format((float)$field->A14, 2, '.', ','); // <!-- PClosed -->
                    $rec[22] = number_format((float)$field->A15, 2, '.', ','); // <!-- P/E -->
                    $rec[23] = number_format((float)$field->A16, 2, '.', ','); // <!--P/BV-->
                    $rec[24] = number_format((float)$field->A17, 2, '.', ','); // <!-- Div Yield -->
                    $rec[25] = number_format((float)$field->A18, 2, '.', ','); // <!-- EPS-->
                    $rec[26] = number_format((float)$field->A19, 2, '.', ','); // <!-- EPS-->
                    $rec[27] = number_format((float)$field->A20, 2, '.', ','); // <!-- EPS-->
                    $rec[28] = number_format((float)$field->A21, 2, '.', ','); // <!-- EPS-->
                    $rec[29] = number_format((float)$field->A22, 2, '.', ','); // <!-- EPS-->
                    $rec[30] = number_format((float)$field->A23, 2, '.', ','); // <!-- EPS-->
                    $rec[31] = number_format((float)$field->A24, 2, '.', ','); // <!-- EPS-->
                    $rec[32] = number_format((float)$field->A25, 2, '.', ','); // <!-- EPS-->
                    $rec[33] = number_format((float)$field->A26, 2, '.', ','); // <!-- EPS-->
                    $rec[34] = number_format((float)$field->A27, 2, '.', ','); // <!-- EPS-->
                    $rec[35] = number_format((float)$field->A28, 2, '.', ','); // <!-- EPS-->
                    $pos = 36;

                    if($field->LAST_DAY==29) {
                        $rec[36] = number_format((float)$field->A29, 2, '.', ','); // <!-- EPS-->
                        $pos = 37;
                    }
                    else if($field->LAST_DAY==30) {
                        $rec[36] = number_format((float)$field->A29, 2, '.', ','); // <!-- EPS-->
                        $rec[37] = number_format((float)$field->A30, 2, '.', ','); //<!-- EPS-->
                        $pos = 38;
                    }
                    else if($field->LAST_DAY==31) {
                        $rec[36] = number_format((float)$field->A29, 2, '.', ','); // <!-- EPS-->
                        $rec[37] = number_format((float)$field->A30, 2, '.', ','); //<!-- EPS-->
                        $rec[38] = number_format((float)$field->A31, 2, '.', ','); // <!-- EPS-->
                        $pos = 39;
                    }
                    
                    $rec[$pos + 1] = number_format((float)$field->TotalValueEOM, 2, '.', ','); // <!--มูลค่ารวม -->
                    $rec[$pos + 2] = number_format((float) ($field->TotalValueEOM / $SumTotalValueEOM) * 100, 2, '.', ','); // <!-- ร้อนล่ะของหุ้น -->
                    
                    if($INVEST_NET > 0)
                        $rec[$pos + 3] = number_format((float) $INVEST_NET, 2, '.', ','); // <!-- เงินลงทุนเพิ่มลด-->
                    else
                        $rec[$pos + 3] = number_format((float) $INVEST_NET, 2, '.', ','); // <!-- เงินลงทุนเพิ่มลด-->
                     
                    $rec[$pos + 4] = number_format((float)$field->EPS, 2, '.', ','); // <!-- EPS-->
                    $rec[$pos + 5] = number_format((float)$field->PE, 2, '.', ',');  // <!-- PE-->
                    $rec[$pos + 6] = number_format((float)$field->PBV, 2, '.', ','); // <!-- PBV-->
                    $rec[$pos + 7] = number_format((float)$field->LAST_DIV_YIELD, 2, '.', ','); // <!-- LAST_DIV_YIELD-->
                    ////
                     $sheet->appendRow($rec);
                }

                // Footer
                $idx = 7+$LASTDAY;
                $summary[] = null;
                
                for($x = 0; $x < $idx; $x++) {
                    $summary[$x] = "";
                }
                $summary[$idx] = "รวม";
                $summary[$idx+1] = number_format((float)$SumTotalValueEOM , 2, '.', ',');
                if($SumTotalValueEOM > 0) 
                   $summary[$idx+2] = "100";
                else
                   $summary[$idx+2] = "";
                $sheet->appendRow($summary);
/*
                foreach ($results as $rec) {

                    $sheet->appendRow($rec);
                }
*/
            });

        })->download('xls');
    }
} // class AdminP2EquityClosedReportController

/*
STORE_PROEDURE :
================
SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Sunsystems 
-- Create date: Jul 4, 2017
-- Description: Closed Index Report
-- =============================================
ALTER PROCEDURE [dbo].[SP_ClosedIndex]
	@PageSize integer,
	@PageIndex integer,
	@TYear integer,
	@TMonth integer,
	@CateID varchar(50)=NULL,
	@Symbol varchar(50)=NULL,
	@NameSht varchar(50)=NULL
AS
BEGIN

    DECLARE @TempTransTable TABLE
    (
       SYMBOL varchar(MAX),
       FIRST varchar(max),
       SECOND varchar(max)
    );
     
    DECLARE @TempTransSumTable TABLE
    (
       NSYMBOL varchar(MAX),
       LAST_DIV_YIELD float,
       PE float,
       PBV float,
       EPS float,
	   PCLOSE float,
       MAX_DATE date,
       MIN_DATE date,
       UNIT_PURCHASE float
    );
    
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE
	      @cols AS NVARCHAR(MAX),
          @query AS NVARCHAR(MAX),
		   @querySum AS NVARCHAR(MAX),
		   @query_trans AS NVARCHAR(MAX),
		   @cols_trans AS NVARCHAR(MAX),
		  @y as  integer,
		  @m as integer,
		  @CTYear as NVARCHAR(MAX),
		  @CTMonth as NVARCHAR(MAX),
		  
          @day1 AS NVARCHAR(3),
          @day2 as NVARCHAR(3),
          @day3 as NVARCHAR(3),
          @day4 as NVARCHAR(3),             
          @day5 as NVARCHAR(3),            
          @day6 as NVARCHAR(3),
          @day7 as NVARCHAR(3),
          @day8 as NVARCHAR(3),
          @day9 as NVARCHAR(3),
          @day10 as NVARCHAR(3),
          @day11 as NVARCHAR(3),
          @day12 as NVARCHAR(3),
          @day13 as NVARCHAR(3),
          @day14 as NVARCHAR(3),             
          @day15 as NVARCHAR(3),            
          @day16 as NVARCHAR(3),
          @day17 as NVARCHAR(3),
          @day18 as NVARCHAR(3),
          @day19 as NVARCHAR(3),
          @day20 as NVARCHAR(3),
          @day21 as NVARCHAR(3),
          @day22 as NVARCHAR(3),
          @day23 as NVARCHAR(3),
          @day24 as NVARCHAR(3),             
          @day25 as NVARCHAR(3),            
          @day26 as NVARCHAR(3),
          @day27 as NVARCHAR(3),
          @day28 as NVARCHAR(3),
          @day29 as NVARCHAR(3),
          @day30 as NVARCHAR(3),
          @day31 as NVARCHAR(3)

   -- set @day1 = QUOTENAME('1')
    select @y = @TYear;
	select @m = @TMonth;
    select @cols = STUFF(
                (SELECT  distinct ',' + QUOTENAME(MIN(TRANS_DATE))  + ',' + QUOTENAME(MAX(TRANS_DATE)) 
                    FROM TBL_P2_EQUITY_TRANS
					WHERE  YEAR(TRANS_DATE) = @TYear AND MONTH(TRANS_DATE) = @TMonth
                    FOR XML PATH(''), TYPE
                ).value('.', 'NVARCHAR(MAX)') 
                , 1, 1, '')

 
   select @CTYear = STR(@TYear) 
   select @CTMonth = STR(@TMonth)
  
   
    set @query = 'SELECT SYMBOL, ' + @cols + ' 
             FROM 
             (
                SELECT SYMBOL, PCLOSE,  REFERENCE_DATE
                from TBL_P2_EQUITY_CLOSED_INDEX 
				WHERE  YEAR(REFERENCE_DATE) ='+ @CTYear +' AND MONTH(REFERENCE_DATE) = ' + @CTMonth +'
				
            ) PX 
            pivot 
            (
                sum(PCLOSE)
            
                for REFERENCE_DATE in (' + @cols + ')
            ) p '

 set @day1  =  '1' 
 set @day2  =  '2'
 set @day3  =  '3'
 set @day4  =  '4'
 set @day5  =  '5'
 set @day6  =  '6' 
 set @day7  =  '7'
 set @day8  =  '8'
 set @day9  =  '9'
 set @day10 =  '10'
 set @day11 =  '11'
 set @day12 =  '12'
 set @day13 =  '13'
 set @day14 =  '14'
 set @day15 =  '15'
 set @day16 =  '16'
 set @day17 =  '17'
 set @day18 =  '18'
 set @day19 =  '19'
 set @day20 =  '20'
 set @day21 =  '21'
 set @day22 =  '22'
 set @day23 =  '23'
 set @day24 =  '24'
 set @day25 =  '25'
 set @day26 =  '26'
 set @day27 =  '27' 
 set @day28 =  '28'   
 set @day29 =  '29'   
 set @day30 =  '30'   
 set @day31 =  '31'          
           
INSERT INTO @TempTransTable execute(@query )

select @querySum =' SELECT  X.NSYMBOL, Z.DIV_YIELD as LAST_DIV_YIELD, 
                            Z.PE as PE, 
                            Z.PBV as PBV,
                            Z.EPS as EPS,
							z.PCLOSE,
                            MAX_DATE, MIN_DATE, SUM(UNIT_PURCHASE) as UNIT_PURCHASE
                    FROM (SELECT DISTINCT(T.SYMBOL) as NSYMBOL, 
	                          T.UNIT_PURCHASE as UNIT_PURCHASE,
					            MAX(C.REFERENCE_DATE) as MAX_DATE,
					            MIN(C.REFERENCE_DATE) as MIN_DATE
					        FROM TBL_P2_EQUITY_TRANS T, 
					            TBL_P2_EQUITY_CLOSED_INDEX C          
					        WHERE T.SYMBOL = C.SYMBOL
					            AND YEAR(C.REFERENCE_DATE) = '+ @CTYear + ' 
					            AND MONTH(C.REFERENCE_DATE) = ' + @CTMonth + '
					        GROUP BY 
					            T.SYMBOL, T.UNIT_PURCHASE 
					   ) X, TBL_P2_EQUITY_CLOSED_INDEX Z
                     WHERE X.NSYMBOL = Z.SYMBOL
                           AND Z.REFERENCE_DATE = X.MAX_DATE 
                     GROUP BY X.NSYMBOL,MAX_DATE, MIN_DATE, Z.DIV_YIELD, Z.PE, Z.PBV, Z.EPS, Z.PCLOSE '
                     
INSERT INTO @TempTransSumTable execute(@querySum)
                      

SELECT DISTINCT(TSM.NSYMBOL) as SYMBOL, 
                DAY(EOMONTH( @CTYear + '-' +@CTMonth + '-1')) AS DAY_EOMONTH,
                TSM.UNIT_PURCHASE,
                TSM.MAX_DATE as REF_MAX_DATE, TSM.MIN_DATE as REF_MIN_DATE,
                TSM.LAST_DIV_YIELD as LAST_DIV_YIELD,
                TSM.PE as PE, TSM.PBV as PBV,
                TSM.EPS as EPS, TSM.PCLOSE as PCLOSE,
				((TSM.UNIT_PURCHASE * TSM.PCLOSE) / 1000000) as TotalValueEOM,
				SUM((TSM.UNIT_PURCHASE * TSM.PCLOSE) / 1000000) as SumTotalValueEOM,
                MMM.* 
FROM ( SELECT  
        MT1.PRICE_PURCHASE AS FIRST_PRICE_PURCHASE, 
        MT2.PRICE_PURCHASE AS LAST_PRICE_PURCHASE, 
		MT2.TOTAL_AMOUNT as LAST_TOTAL_AMOUNT,
        MT1.PRICE_SALE AS FIRST_SALE, 
        MT1.PRICE_SALE AS LAST_SALE,
		 MT1.SECURITIES_NAME,
		 I.INDUSTRIAL,
		 MA.* 
       FROM ( SELECT Temp.SYMBOL,  X.* 
              FROM ( SELECT  SYMBOL AS  NEW_SYMBOL, FIRST_TRANS_DATE, LAST_TRANS_DATE, LAST_DAY, 
			  			ISNULL(MAX(CASE WHEN DAY(RF) =  1 THEN PCLOSE END), 0)   'A1' ,
			  			ISNULL(MAX(CASE WHEN DAY(RF) =  2 THEN PCLOSE END), 0)   'A2' ,
			  			ISNULL(MAX(CASE WHEN DAY(RF) =  3 THEN PCLOSE END), 0)   'A3' ,
			  			ISNULL(MAX(CASE WHEN DAY(RF) =  4 THEN PCLOSE END), 0)   'A4' ,
			  			ISNULL(MAX(CASE WHEN DAY(RF) =  5 THEN PCLOSE END), 0)   'A5' ,
			  			ISNULL(MAX(CASE WHEN DAY(RF) =  6 THEN PCLOSE END), 0)   'A6' ,
			  			ISNULL(MAX(CASE WHEN DAY(RF) =  7 THEN PCLOSE END), 0)   'A7' , 
			  			ISNULL(MAX(CASE WHEN DAY(RF) =  8 THEN PCLOSE END), 0)   'A8' ,
			  			ISNULL(MAX(CASE WHEN DAY(RF) =  9 THEN PCLOSE END), 0)   'A9' ,
			  	    	ISNULL(MAX(CASE WHEN DAY(RF) = 10 THEN PCLOSE END), 0)   'A10',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 11 THEN PCLOSE END), 0)   'A11',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 12 THEN PCLOSE END), 0)   'A12',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 13 THEN PCLOSE END), 0)   'A13',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 14 THEN PCLOSE END), 0)   'A14',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 15 THEN PCLOSE END), 0)   'A15',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 16 THEN PCLOSE END), 0)   'A16',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 17 THEN PCLOSE END), 0)   'A17',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 18 THEN PCLOSE END), 0)   'A18',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 19 THEN PCLOSE END), 0)   'A19',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 20 THEN PCLOSE END), 0)   'A20',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 21 THEN PCLOSE END), 0)   'A21',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 22 THEN PCLOSE END), 0)   'A22',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 23 THEN PCLOSE END), 0)   'A23',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 24 THEN PCLOSE END), 0)   'A24',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 25 THEN PCLOSE END), 0)   'A25',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 26 THEN PCLOSE END), 0)   'A26',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 27 THEN PCLOSE END), 0)   'A27',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 28 THEN PCLOSE END), 0)   'A28',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 29 THEN PCLOSE END), 0)   'A29',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 30 THEN PCLOSE END), 0)   'A30',
			  			ISNULL(MAX(CASE WHEN DAY(RF) = 31 THEN PCLOSE END), 0)   'A31'
				      FROM ( SELECT  
		              		     C.SYMBOL AS SYMBOL,
		              		     C.PCLOSE AS PCLOSE,
		              		 
		              		     C.REFERENCE_DATE AS  RF,
		                       
			          		     DAY(EOMONTH(C.REFERENCE_DATE)) AS LAST_DAY,
					   		     MIN(T.TRANS_DATE) As FIRST_TRANS_DATE,
				       		     MAX(T.TRANS_DATE) AS LAST_TRANS_DATE
								
          		             FROM 
          		                  TBL_P2_EQUITY_CLOSED_INDEX C, TBL_P2_EQUITY_TRANS T  
          		             WHERE  C.SYMBOL = T.SYMBOL    
		       		              AND YEAR(C.REFERENCE_DATE) =  @TYear 
		       		              AND MONTH(C.REFERENCE_DATE)=  @TMonth 
    	          	              AND T.TYPE = 'P'
					
	                      GROUP BY C.SYMBOL, REFERENCE_DATE, PCLOSE   
                        ) SRC 
                    GROUP BY SYMBOL,  FIRST_TRANS_DATE, LAST_TRANS_DATE, LAST_DAY   
              ) X , @TempTransTable Temp
           WHERE  X.NEW_SYMBOL = Temp.SYMBOL 
       ) MA, TBL_P2_EQUITY_TRANS MT1, 
                    TBL_P2_EQUITY_TRANS MT2, 
                    TBL_P2_EQUITY_INDEX I, 
                    TBL_P2_EQUITY_CLOSED_INDEX C
                    
   WHERE  MA.SYMBOL = MT1.SYMBOL AND MT2.SYMBOL = MA.SYMBOL 
          AND  I.SYMBOL = MA.SYMBOL
          AND MA.FIRST_TRANS_DATE = MT1.TRANS_DATE 
          AND MT2.TRANS_DATE = MA.LAST_TRANS_DATE
          AND C.SYMBOL = MA.SYMBOL 
          -- optional parameters
          AND  (@Symbol IS NULL OR MA.SYMBOL = @Symbol)
          AND  (@NameSht IS NULL OR MT1.SECURITIES_NAME = @NameSht)
          AND  (@NameSht IS NULL OR MT2.SECURITIES_NAME = @NameSht)

       --ORDER BY MA.SYMBOL, MA.FIRST_TRANS_DATE

       --OFFSET @PageSize  * (@PageIndex - 1) ROWS FETCH NEXT @PageSize  ROWS ONLY OPTION (RECOMPILE) 

) MMM,  @TempTransSumTable TSM

WHERE  TSM.NSYMBOL= MMM.SYMBOL
      

GROUP BY 
    TSM.NSYMBOL, 
    TSM.MIN_DATE,
    TSM.MAX_DATE,
    TSM.LAST_DIV_YIELD,
    TSM.PE,
    TSM.PBV,
    TSM.EPS,
	TSM.PCLOSE,
	MMM.FIRST_PRICE_PURCHASE,
	MMM.LAST_PRICE_PURCHASE,
	MMM.LAST_TOTAL_AMOUNT,
	MMM.FIRST_SALE,
	MMM.LAST_SALE, 
	MMM.SECURITIES_NAME,
	MMM.INDUSTRIAL,
	MMM.SYMBOL,
	MMM.NEW_SYMBOL,
	MMM.FIRST_TRANS_DATE,
	MMM.LAST_TRANS_DATE,
	MMM.LAST_DAY,
	MMM.A1,
	MMM.A2,
	MMM.A3,
	MMM.A4,
	MMM.A5,
	MMM.A6,
	MMM.A7,MMM.A8,
	MMM.A9,MMM.A10,
	MMM.A11,
	MMM.A12,MMM.A13,
	MMM.A14,MMM.A15,
	MMM.A16,
	MMM.A17,MMM.A18,
	MMM.A19,MMM.A20,
	MMM.A21,MMM.A22,
	MMM.A23,MMM.A24,
	MMM.A25,
	MMM.A26,MMM.A27,
	MMM.A28,MMM.A29,
	MMM.A30,MMM.A31,
   TSM.UNIT_PURCHASE
   --TotalValueEOM
   ORDER BY MMM.SYMBOL, MMM.FIRST_TRANS_DATE
   OFFSET @PageSize  * (@PageIndex - 1) ROWS FETCH NEXT @PageSize  ROWS ONLY OPTION (RECOMPILE) 
END
GO
*/
?>