<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package\Curl;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Input;
use Illuminate\Http\UploadedFile;

use Illuminate\Support\Facades\Log;

class UserManageBenefitcontroller extends Controller
{

    public function getsimple()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 51,
            'menu_id' => 4,
            'title' => getMenuName($data,51,4) ." | MEA"
        ]);

        //$user_group = DB::table('TBL_PRIVILEGE')->select('USER_PRIVILEGE_ID','USER_PRIVILEGE_DESC')->orderBy('USER_PRIVILEGE_ID', 'asc')->get();

        return view('backend.pages.userbenefit');
    }

    public  function  Checkdate(Request $request){

        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');


        $results = null;

        $result=   Excel::load($request->file('exelimport'))->get();
        $count = $result->count();
//
//      Excel::load($request->file('exelimport'), function ($reader) use($count) {
//
//            $results = $reader->get();
//
//            $ret = $results->toArray();
//
//
//            $count = count($ret);
//
//
//        });

        return response()->json(array('success' => true, 'html'=>$count));



    }

    public  function  dowloadsample() {
        $file = 'contents/sample/member_beneficiary.xls';
        $headers = array(
            'Content-Type: application/pdf',
        );
        return \Response::download($file, 'member_beneficiary.xls', $headers);
    }

    public function importdata(Request $request) {

        // limit execution timeout 
        ini_set('max_execution_time', 120000);
        // unlimit memory size 
        ini_set('memory_limit', '-1');

        $results = null;

        // statistics 
        $count       = 0;
        $skipedCount = 0;
        $passedCount = 0;
        $total_records = 0;
        $progress_count = 0;
        $htmlResult = '';


        $file = $request->file('exelimport');
        $request->file('exelimport')->move(storage_path().'/public/import/' , 'import.xlsx');

        $inputfile = storage_path('/public/import/import.xlsx');
        $retdate = Excel::load($inputfile, function($reader) use(&$count, 
                                                             &$skipedCount, 
                                                             &$passedCount, 
                                                             &$total_records, 
                                                             &$progress_count,
                                                             &$htmlResult) {
        // $ret = Excel::filter('chunk')->load(storage_path('/public/import/import.xlsx'))->chunk(250, function($results){

            $results = $reader->get(); 
            $ret = $results->toArray();
            $htmlResult = '';

            $progress_count = 0;
            $total_records = $results->count();

           

            foreach($ret as $index => $value) {

                // foreach($results as $index => $value) {
                $data = array();
                if (($value == null) || ($value["emp_id"] == null) || (strlen($value["emp_id"]) < 2)) {
                    $skipedCount++;
                    continue;
                }

                $count++;
                $EMP_ID = $value["emp_id"];
                $allquery = "SELECT COUNT(EMP_ID) AS total FROM TBL_USER_BENEFICIARY  WHERE EMP_ID= '" . $EMP_ID . "'";

                $all = DB::select(DB::raw($allquery));
                $total =  $all[0]->total;
                $date = new Date();

                if ($total == 0) {
                    array_push($data,array(
                        'EMP_ID' => $value["emp_id"],
                        'FULL_NAME' =>$value["full_name"],
                        'FILE_NO' => $value["file_no"],
                        'FILE_PATH' => $value["file_path"],
                        'CREATE_DATE' => $value["create_date"],
                        'CREATE_BY' => $value["create_by"],
                        'FILE_NAME' =>$value["file_name"]
                    ));

                    $affected = DB::table('TBL_USER_BENEFICIARY')->insert($data);
                    if($affected < 1) {
                        if(strlen($htmlResult) < 500) {
                            $htmlResult .= '<tr><td>' . $value["emp_id"]     . '</td>';
                            $htmlResult .= '    <td>' . $value["full_name"] . '</td></tr>';
                        }
                    } else {
                        $passedCount++; 
                    }

                } else {
                    // assumed that always success
                    $passedCount++; 
                }
               
            } // foreach

          
        });

        $header = 'รายละเอียดข้อมูลที่ ไม่สำเร็จ <BR> '.
                  '<table style=\"width: 100px;\" class=\"table table-bordered\">' .
                  '<tr><td>EMP_ID</td><td>FULL_NAME</td></tr>';
        $footer = '</table>  ';

        if(strlen($htmlResult) < 1) {
            $header  = '';
            $footer = '';  
            $htmlResult = '';
        }

        /* put 100% */ 
        // Session::put('progress_count', $total_records);

        return response()->json(array('success' => true, 'html'=>'<br>' . 
                ' Total : ' . $count . ' Record(s), '. 
                ' Passed: ' . $passedCount . ' Record(s), '. 
                ' Failed: ' . ($count - $passedCount) . ' Record(s), '. 
                // ' progress_count:' . Session::get('progress_count') . 
                //  ' Skiped: ' . $skipedCount . ' Record(s)  ' .
                ' <BR> '. $header . $htmlResult . $footer));
    }

/*  // ORIGINAL 
    public function importdata(Request $request) {

        // limit execution timeout 
        ini_set('max_execution_time', 30000);
        // unlimit memory size 
        ini_set('memory_limit', '-1');

        $results = null;

        // statistics  
        $count       = 0;
        $skipedCount = 0;
        $passedCount = 0;
        $total_records = 0;
        $progress_count = 0;
        $htmlResult = '';


        $file = $request->file('exelimport');
        $request->file('exelimport')->move(storage_path().'/public/import/' , 'import.xlsx');

        $ret = Excel::filter('chunk')->load(storage_path('/public/import/import.xlsx'))->chunk(250, function($results){
            $data = array();
            foreach($results as $index => $value) {
                $EMP_ID = $value["emp_id"];
                $allquery = "SELECT COUNT(EMP_ID) AS total FROM TBL_USER_BENEFICIARY  WHERE EMP_ID= '".$EMP_ID."'";

                $all = DB::select(DB::raw($allquery));
                $total =  $all[0]->total;
                $date = new Date();

                if ($total == 0) {
                        array_push($data,array(
                            'EMP_ID' => $value["emp_id"],
                            'FULL_NAME' =>$value["full_name"],
                            'FILE_NO' => $value["file_no"],
                            'FILE_PATH' => $value["file_path"],
                            'CREATE_DATE' => $value["create_date"],
                            'CREATE_BY' => $value["create_by"],
                            'FILE_NAME' =>$value["file_name"]

                        ));
                }
            }

            DB::table('TBL_USER_BENEFICIARY')->insert($data);
        });

        return response()->json(array('success' => true, 'html'=>$ret));
    }
    */


    public function importdata_pdf(Request $request){



//        var_dump(getenv('BENEFICIARY_PDF_PATH'));
        $data = array();

        foreach($request->file() as $value){
//            $file = $request->file('exelimport_' . $index);
            $file = $value->getClientOriginalName();
            $emp_id = explode('.',$file)[0];
            $extension = explode('.',$file)[1];

            $full_name = "";
            $file_name = $emp_id . "." . $extension;
            $filePath = "";

            $qfullname = "SELECT FULL_NAME FROM TBL_EMPLOYEE_INFO WHERE EMP_ID= '".$emp_id."'";
             $datafull_name = DB::select(DB::raw($qfullname));
            if($datafull_name)
            {
                $full_name = $datafull_name[0]->FULL_NAME;
            }


            $qfileName = "SELECT WEB_BENEFICIARY_ROOT_PATH FROM TBL_CONTROL_CFG";
            $datafile_name= DB::select(DB::raw($qfileName));
            if($datafile_name)
            {
                $filePath = $datafile_name[0]->WEB_BENEFICIARY_ROOT_PATH . $emp_id . ".pdf";
            }

            $allquery = "SELECT COUNT(EMP_ID) AS total FROM TBL_USER_BENEFICIARY  WHERE EMP_ID= '".$emp_id."'";
            $all = DB::select(DB::raw($allquery));
            $total =  $all[0]->total;
            $date = new Date();

            if ($total == 0) {
                array_push($data,array(
                    'EMP_ID' => $emp_id,
                    'FULL_NAME' =>$full_name,
                    'FILE_NO' => 1,
                    'FILE_PATH' =>$filePath,
                    'CREATE_DATE' => $date,
                    'CREATE_BY' => '"Admin"',
                    'FILE_NAME' =>$file_name,

                ));

                //storage_path
                //public_path
                //Log::info('EquitycompanyManagement::BENEFICIARY_PDF_PATH() => ' . public_path().getenv('BENEFICIARY_PDF_PATH')); 

                //Log::info('EquitycompanyManagement::move to $file_name() => ' . $file_name); 
               //======> $value->move(public_path().getenv('BENEFICIARY_PDF_PATH') , $file_name);
            }
            
            Log::info('EquitycompanyManagement::BENEFICIARY_PDF_PATH() => ' . public_path().getenv('BENEFICIARY_PDF_PATH')); 
            Log::info('EquitycompanyManagement::move to $file_name() => ' . $file_name); 
            $value->move(public_path().getenv('BENEFICIARY_PDF_PATH') , $file_name);
        }



        DB::table('TBL_USER_BENEFICIARY')->insert($data);



        return response()->json(array('success' => true, 'html'=>"ok"));
    }
}
