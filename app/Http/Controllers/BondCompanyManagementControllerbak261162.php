<?php
/**
 * Phase#2 Bond Company Management 
 * Created: 2017/05/09 00:06
 */
 /** HTML:
  * 
  *  TAB#1:
  *  ======
  
  *  pages/p2_bond_tab1_company_management.blade.php
  *  pages/p2_bond_tab1_add_company_page.blade.php
  *  pages/p2_bond_tab1_edit_company_page.blade.php
  *  pages/ajax/ajax_p2_bond_tab1_company_management.blade.php
  * 
  *  TAB#2:
  *  ======
  *  
  *  pages/p2_bond_tab2_category_page.blade.php
  *  pages/p2_bond_tab2_add_category_page.blade.php
  *  pages/p2_bond_tab2_edit_category_page.blade.php
  *  pages/p2_bond_tab2_import_category_page.blade.php
  *  pages/ajax/ajax_p2_bond_tab2_category.blade.php
  *  
  *  TAB#3:
  *  ======
  *  pages/p2_bond_tab3_index_page.blade.php
  *  pages/p2_bond_tab3_add_index_page.blade.php
  *  pages/p2_bond_tab3_edit_index_page.blade.php
  *  pages/p2_bond_tab3_import_index_page.blade.php
  *  pages/ajax/ajax_p2_tab3_bond_index.blade.php
  *  pages/ajax/ajax_p2_tab3_bond_index_form.blade.php
  *
  *  TAB#4:
  *  ======
  *  pages/p2_bond_tab4_broker_page.blade.php
  *  pages/ajax/ajax_p2_bond_tab4_broker.blade.php
  *  pages/p2_bond_tab4_add_broker_page.blade.php
  *  pages/p2_bond_tab4_edit_broker_page.blade.php
  * 
  */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\UserGroup;
use Illuminate\Support\Facades\Validator;
use Jenssegers\Date\Date;

use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Input;
use Illuminate\Http\UploadedFile;

use Illuminate\Support\Arr;

use App\User;
use Illuminate\Support\Facades\Log;
use App\Libraries\MEAUtils;


class BondCompanyManagementController extends Controller
{
    public function getindex()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 61,
            'menu_id' => 1,
            'title' => getMenuName($data, 61, 1) . ' | MEA'
        ] );

        return view('backend.pages.p2_bond_tab1_company_management');
    }
    

    public  function Ajax_Index(Request $request){

        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;

        $Datacount = $this->getCountAll();
        $Data = $this->getData($ArrParam);

        $totals = count($Datacount);

        $htmlPaginate =Paginatre_gen($totals,$PageSize,'page_click_search',$PageNumber);

        $returnHTML = view('backend.pages.ajax.ajax_p2_bond_tab1_company_management')->with([
            'htmlPaginate'=> $htmlPaginate,
            'data'=>$Data

        ])->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }


    public  function  getCountAll(){
        return DB::table('TBL_P2_BOND_SECURITIES')->orderby("Name_Sht")->get();
    }

    public  function  getData($ArrParam){

        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];

        $query =  "SELECT * FROM TBL_P2_BOND_SECURITIES ORDER BY NAME_SHT OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
        return DB::select(DB::raw($query));
    }

    /**
     * Handle request delete single/multiple records.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function delete(Request $request)
    {
        $deleted = false;
        $arrId = explode(',',$request->input('group_id'));

        foreach($arrId as $index => $item){

            if($item != ""){
                $deleted =  DB::table('TBL_P2_BOND_SECURITIES')->where('NAME_SHT',"=", $item)->delete();
            }
        }

        if($deleted)  {
            return response()->json(["ret" => "1"]);
        } else {
            return response()->json(["ret" => "0"]);
        }
    }


    /**
     * Get view Bond company .
     * 
     * @param  None
     * @return view\backend\pages "p2_bond_tab1_add_company_page"
     */
    public function getAdd()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 61,
            'menu_id' => 1,
            'title' => getMenuName($data,61, 1) . ' | MEA'
        ] );


        return view('backend.pages.p2_bond_tab1_add_company_page');
    }

    /**
     * Query bond company data by specifiled code as parameter id.
     * 
     * @param  Bond company code  $id
     * @return view backend.pages.p2_bond_tab1_edit_company_page
     */
    public function getEdit($id)
    {
        $data = getmemulist();
        $this->pageSetting([
            'menu_group_id' => 61,
            'menu_id' => 1,
            'title' => getMenuName($data, 61, 1) . ' | MEA'
        ]);


        $editdata = DB::table('TBL_P2_BOND_SECURITIES')->where("NAME_SHT" ,"=",  $id)->first();
        // $editdata = DB::table('TBL_P2_EQUITY_SECURITIES')->where("NAME_SHT" ,"=",  $id)->get()[0];
     
        //if(isset($id)) abort(404);
        if($editdata) {

            return view('backend.pages.p2_bond_tab1_edit_company_page')->with(['editdata'=>$editdata]);
        }
        
        abort(404);
    }


    /**
     * Receive POST command to add new equity company data
     * @param $request parameters list with fields 
     *        plan_start, plan_end, company_code, company_name,
     *        company_addr, company_phone, company_fax 
     */
    public function postAdd(Request $request)
    {
         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');
        

        Log::info('BondCompanyManagementController::PoseAdd::=>' . $request);
        Log::info(' postAdd: equity_start=>' . toEnglishDate($request["equity_start"])); 

        $ret = false;
        $datestart  = new Date(toEnglishDate($request["equity_start"]));
        $datereq = $request["equity_end"];
         
        
        if ($request["company_addr"] == '') {

        }

        
        if ($request["equity_start"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันเริ่มต้น ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        if ($request["equity_end"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันสิ้นสุด ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }
        

        Log::info(' postAdd: equity_start=>' . toEnglishDate($datereq));   
        $dateEnd = new Date(toEnglishDate($datereq));

        $today   = new Date();
        $data    = array();

        array_push($data,array(
            'NAME_SHT' => $request["name_sht"],
            //Securities_Name
            'SECURITIES_NAME' =>$request["company_name"],
            'ADDRESS' => $request["company_addr"],

            'START_DATE' => $datestart,
            'END_DATE' => $dateEnd,

            'PHONE' => $request["company_phone"],
            'FAX' => $request["company_fax"],
            'CREATE_DATE' => $today,
            'CREATE_BY' => "Admin"
        ));


        $chk = "SELECT COUNT(NAME_SHT) As total FROM TBL_P2_BOND_SECURITIES WHERE NAME_SHT = '". $request["name_sht"]. "'";
        $all = DB::select(DB::raw($chk));
        $total =  $all[0]->total;

        $rethtml = "";

        if ($total > 0) {
           $rethtml = "Short code ที่ท่านใส่มีอยู่ในระบบแล้ว";

        } else {
           $insert = DB::table('TBL_P2_BOND_SECURITIES')->insert($data);
           $ret = $insert;
        }

        return response()->json(array('success' => $ret, 'html'=>$rethtml));
    }


    /**
     * Receive POST command to update equity company data
     * @param $request parameters list with fields 
     *        plan_start, plan_end, company_code, company_name,
     *        company_addr, company_phone, company_fax 
     */
    public function postEdit(Request $request)
    {
        $ret = false;
        $datestart  = new Date(toEnglishDate($request["equity_start"]));
        $datereq = $request["equity_end"];

        if ($request["equity_end"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันสิ้นสุด ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }
        if ($request["equity_start"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันเริ่มต้น ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        Log::info(' postEdit: request =>' . $request);   

        $dateEnd = new Date(toEnglishDate($datereq));
        $today = new Date();

        // retrive logged ID
        $user_data = Session::get('user_data');
        $CREATE_DATE = date("Y-m-d H:i:s");   
        $CREATE_BY = $user_data->emp_id; 

        $data = array(

            'NAME_SHT' => $request["name_sht"],

            'SECURITIES_NAME' =>$request["company_name"],
            'ADDRESS' => $request["company_addr"],

            'START_DATE' => $datestart,
            'END_DATE' => $dateEnd,

            'PHONE' => $request["company_phone"],
            'FAX' => $request["company_fax"],

            'CREATE_BY' => $CREATE_BY
        );

        /**
         * update bond company to database
         */

        // $update = DB::table('TBL_P2_BOND_SECURITIES')->where('NAME_SHT', "=", $request["name_sht"])->update($data);

        $update = DB::table('TBL_P2_BOND_SECURITIES')->where('SEC_ID', "=", $request["sec_id"])->update($data);

        $ret = $update;

        return response()->json(array('success' => $ret, 'html'=>'OK'));
 
    }

    

    /*  BOND CATEGORY:
        ================
        Route::get('category',          'BondCompanyManagementController@getindexCategory');  // -> VIEW: p2_tab2_equity_category_page.blade.php
        Route::get('addCategory',       'BondCompanyManagementController@getAddCategory');    // -> VIEW: p2_tab2_add_equity_category_page.blade.php 
        Route::get('editCategory/{id}', 'BondCompanyManagementController@getEditCategory');   // -> VIEW: p2_tab2_edit_equity_category_page.blade.php 
            
        Route::post('addCategory',      'BondCompanyManagementController@postAddCategory');
        Route::post('editsCategory',    'BondCompanyManagementController@postEditCategory');
        Route::post('deleteCategory',   'BondCompanyManagementController@deleteCategory');
        Route::post('getallCategory',   'BondCompanyManagementController@Ajax_Index_Category'); // -> VIEW: ajax_p2_equity_category_page.blade.php 
    */
       
    /**
     * TAB#2 : Bond Category  
     */
    public function getindexCategory()
    {
        $data = getmemulist();
        $this->pageSetting([
            'menu_group_id' => 61,
            'menu_id' => 1,
            'title' => getMenuName($data, 61, 1) . ' | MEA'
        ]);

        Log::info('BondCompanyManagement::getindexCategory() => ' . getMenuName($data, 61, 1)); 
        return view('backend.pages.p2_bond_tab2_category_page');
    }


    public  function Ajax_Index_Category(Request $request) 
    {
        Log::info('BondCompanyManagement::Ajax_Index_Category' . $request);
 
        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;

        $Datacount = $this->getCountAllCategory();
        $Data = $this->getDataCategory($ArrParam);

        $totals = count($Datacount);

        $htmlPaginate =Paginatre_gen($totals,$PageSize,'page_click_search',$PageNumber);

        $returnHTML = view('backend.pages.ajax.ajax_p2_bond_tab2_category')->with([
            'htmlPaginate'=> $htmlPaginate,
            'data'=>$Data

        ])->render();

        Log::info('BondCompanyManagement::Ajax_Index_Category()' );
        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }


    public function getCountAllCategory() 
    {
        /* limit execution timeout */
        //ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        //ini_set('memory_limit', '-1');

        Log::info('BondCompanyManagement::getCountAllCategory() all records for TBL_P2_BOND_CATEGORY');

        return DB::table('TBL_P2_BOND_CATEGORY')->orderby("CATE_ID")->get();
        
    }

    public function getDataCategory($ArrParam) 
    {
        Log::info('BondCompanyManagement::getDataCategory()');
        /* limit execution timeout */
        // ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        // ini_set('memory_limit', '-1');

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];

        $query =  "SELECT * FROM TBL_P2_BOND_CATEGORY ORDER BY CATE_ID DESC OFFSET ". $PageSize . " * (" . $PageNumber . " - 1) ROWS FETCH NEXT " . $PageSize . " ROWS ONLY OPTION (RECOMPILE)";
        
        Log::info('BondCompanyManagement::getDataCategory()- SQLSTR=' .  $query ); 
        return DB::select(DB::raw($query));
    }


    /**
     * Handle request delete category single/multiple records.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function deleteCategory(Request $request)
    {
        $deleted = false;
        $arrId = explode(',', $request->input('group_id'));

        foreach($arrId as $index => $item) {
            if($item != "") {
                $deleted =  DB::table('TBL_P2_BOND_CATEGORY')->where('CATE_ID',"=", $item)->delete();
            }
        }

        if($deleted)  {
            return response()->json(["ret" => "1"]);
        } else {
            return response()->json(["ret" => "0"]);
        }
    }

    /**
     * Get view bond category 
     * 
     * @param  None
     * @return view\backend\pages
     */
    public function getAddCategory()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 61,
            'menu_id' => 1,
            'title' => getMenuName($data,61, 1) . ' | MEA'
        ] );

        return view('backend.pages.p2_bond_tab2_add_category_page');
    }

    /**
     * Get view bond category with data by matched with $id
     * 
     * @param  Bond company code  $id
     * @return view backend.pages.p2_edit_bond_company_page
     */
    public function getEditCategory($id)
    {
        $data = getmemulist();
        $this->pageSetting([
            'menu_group_id' => 61,
            'menu_id' => 1,
            'title' => getMenuName($data, 61, 1) . ' | MEA'
        ]);

        $editdata = DB::table('TBL_P2_BOND_CATEGORY')->where("CATE_ID" ,"=",  $id)->first();
       
        if($editdata) {
            return view('backend.pages.p2_bond_tab2_edit_category_page')->with(['editdata'=>$editdata]);
        }
        // any error occurred redirect to page 404
        abort(404);
    }


    /**
     * Receive POST command to add new equity category
     * @param $request parameters list with fields 
     *        plan_start, plan_end, company_code, company_name,
     *        company_addr, company_phone, company_fax 
     */
    public function postAddCategory(Request $request)
    {
         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');
        
        $ret = false;
       
        if ($request["industrial"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล กลุ่มอุตสาหกรรม ";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        if ($request["bu"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล หมวดธุรกิจ";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        
        $data  = array();
        array_push($data, array(
            'BOND_CATE_THA' => trim($request["industrial"]),
            'BOND_TYPE_THA' => trim($request["bu"]),
            'BOND_CATE_ENG' => trim($request["industrial_eng"]),
            'BOND_TYPE_ENG' => trim($request["bu_eng"])
        ));

        $chk = " SELECT " . 
               "    COUNT(BOND_CATE_THA) As total " . 
               " FROM " . 
               "    TBL_P2_BOND_CATEGORY " .
               " WHERE BOND_CATE_THA = '" . trim($request["industrial"]) . "' " .
               "    AND BOND_TYPE_THA = '" . trim($request["bu"]) . "' ";
        $all = DB::select(DB::raw($chk));
        $total =  $all[0]->total;

        $rethtml = "";

        if ($total > 0) {
           $rethtml = "หมวดหมู่หลักทรัพย์ ที่ท่านใส่มีอยู่ในระบบแล้ว";

        } else {
           Log::info('BOND_CATE_THA: ' . $request["industrial"]);  
           $insert = DB::table('TBL_P2_BOND_CATEGORY')->insert($data);
           $ret = $insert;
        }

        return response()->json(array('success' => $ret, 'html'=>$rethtml));
    }


    /**
     * Receive POST command to update equity company data
     * @param $request parameters list with fields 
     *        plan_start, plan_end, company_code, company_name,
     *        company_addr, company_phone, company_fax 
     */
    public function postEditCategory(Request $request)
    {
        $ret = false;
        Log::info('postEditCategory(): industrial='. $request["industrial"]);  

        if ($request["industrial"] == "") {
            $rethtml = "มีข้อผิดพลาด ท่านไม่ได้ระบุ ประเภทตราสารหนี้";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        if ($request["bu"] == "") {
            $rethtml = "มีข้อผิดพลาด ท่านไม่ได้ระบุ หมวดหมวดหมู่ธุระกิจ";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        $data = array(
            'BOND_CATE_THA' => trim($request["industrial"]),
            'BOND_TYPE_THA' => trim($request["bu"]),
            'BOND_CATE_ENG' => trim($request["industrial_eng"]),
            'BOND_TYPE_ENG' => trim($request["bu_eng"])
        );

        /**
         * update equity category to database
         */
        $update = DB::table('TBL_P2_BOND_CATEGORY')->where('CATE_ID', "=", $request["cate_id"])->update($data);

        $ret = $update;
        return response()->json(array('success' => $ret, 'html'=>'OK'));
    }

    public function getimportCategory()
    {
        $this->pageSetting( [
            'menu_group_id' => 61,
            'menu_id' => 1,
            'title' => 'นำเข้าข้อมูล | MEA'
        ]);

        return view('backend.pages.p2_bond_tab2_import_category_page');
    }

    public function importCategory(Request $request) 
    {

        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        /* statistics */ 
        $count       = 0;
        $skipedCount = 0;
        $passedCount = 0;
        $total_records = 0;
        $htmlResult = '';
       
        $results = null;

        $file = $request->file('exelimport');

        $request->file('exelimport')->move(storage_path().'/public/import/' , 'import.xlsx');
       
        $inputfile = storage_path('/public/import/import.xlsx');


        /// FAKE: START
        return response()->json(array('success' => true, 'html'=>'<br>' . 
                ' Total : ' . $count . ' Record(s), '. 
                ' Passed: ' . $passedCount . ' Record(s), '. 
                ' Failed: ' . ($count - $passedCount) . ' Record(s), '));
        /// FAKE: END

        $retdate = Excel::load($inputfile, function($reader) use(&$count, 
                                                                 &$skipedCount, 
                                                                 &$passedCount, 
                                                                 &$total_records, 
                                                                 &$htmlResult) {
            $results = $reader->get(); 
            $ret = $results->toArray();
            $htmlResult = '';

            /* LAM prepare for progress bar */
            /////////////////////////////////////
            $progress_count = 0;
            $total_records = $results->count();
            Session::put('progress_count',$progress_count);
            Session::put('total_records', $total_records);
            
            foreach($ret as $index => $value) {
                $count++;
                $data = array();
                
                if (($value == null) || ($value["industrial"] == null) || (strlen($value["industrial"]) < 2)) {
                    $skipedCount++;
                    continue;
                }

                /* update progress */
                $progress_count++;
                if($progress_count % 2 == 0)
                    Session::put('progress_count', $progress_count);
                $CATE_ID    = 0;
                $MARKET     = $value["market"]; 
                $INDUSTRIAL = $value["industrial"];
                $BU         = $value["bu"];
                
                $allquery = "SELECT COUNT(CATE_ID) AS total FROM TBL_P2_BOND_CATEGORY  WHERE INDUSTRIAL= '". $INDUSTRIAL . "'" . 
                            "       AND MARKET='". $MARKET . "'" .
                            "       AND BU='". $BU . "')";
                
                $all = DB::select(DB::raw($allquery));
                $total =  $all[0]->total;


                if ($total == 0) {
                    /* creste new record */
                    array_push($data,array(
                        'MARKET' => $MARKET,
                        'INDUSTRIAL' => $INDUSTRIAL,
                        'BU' => $BU
                    ));

                    $affected = DB::table('TBL_P2_BOND_CATEGORY')->insert($data);
                    if($affected < 1) {
                         if(strlen($htmlResult) < 500) 
                         {
                            $htmlResult .= '<tr>';
                            $htmlResult .= '    <td>' . $CATE_ID    . '</td>';
                            $htmlResult .= '    <td>' . $MARKET     . '</td>';
                            $htmlResult .= '    <td>' . $INDUSTRIAL . '</td>';
                            $htmlResult .= '    <td>' . $BU         . '</td></tr>';
                         }
                    } else {
                        $passedCount++; 
                    }

                } else {

                    /* update, if already exist */
                    $data = array('MARKET' => $MARKET,
                                  'INDUSTRIAL' => $INDUSTRIAL,
                                  'BU' => $BU);
                    $affected = DB::table('TBL_P2_BOND_CATEGORY')
                                    ->where('INDUSTRIAL', "=", $INDUSTRIAL)
                                    ->where('MARKET', "=", $MARKET)
                                    ->update($data);
                    
                    if($affected < 1) {
                         if(strlen($htmlResult) < 500) 
                         {
                            $htmlResult .= '<tr>';
                            $htmlResult .= '    <td>' . $CATE_ID    . '</td>';
                            $htmlResult .= '    <td>' . $MARKET     . '</td>';
                            $htmlResult .= '    <td>' . $INDUSTRIAL . '</td>';
                            $htmlResult .= '    <td>' . $BU         . '</td></tr>';
                         }
                    } else {
                        $passedCount++; 
                    }
                    // assumed that always success
                    //$passedCount++; 
                }  

            } // foreach

        });  // Excel::load

        $header = 'รายละเอียดข้อมูลที่ ไม่สำเร็จ <BR> '.
                  '<table style=\"width: 100px;\" class=\"table table-bordered\">' .
                  '<tr><td>รหัส หมวดหมู่หลักทรัพย์</td>' . 
                  '    <td>ตลาด</td>'. 
                  '    <td>กลุ่มอุตสาหกรรม</td>' . 
                  '    <td>หมวดธุรกิจ</td></tr>';

        $footer = '</table>  ';

        if(strlen($htmlResult) < 1) {
            $header  = '';
            $footer = '';  
            $htmlResult = ' ###### ';
        }

        /* put 100% */ 
        Session::put('progress_count', $total_records);

        return response()->json(array('success' => true, 'html'=>'<br>' . 
                ' Total : ' . $count . ' Record(s), '. 
                ' Passed: ' . $passedCount . ' Record(s), '. 
                ' Failed: ' . ($count - $passedCount) . ' Record(s), '. 
                // ' progress_count:' . Session::get('progress_count') . 
                //  ' Skiped: ' . $skipedCount . ' Record(s)  ' .
                ' <BR>'. $header . $htmlResult . $footer));
        
    } // importdata


    public function dowloadsampleBondCategory() {
        $file = 'contents/sample/p2_equity_category.xls';
        $headers = array(
            'Content-Type: application/pdf',
        );
        return \Response::download($file, 'p2_equity_category.xls', $headers);
    }
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    // 
    // TAB#3 : Bond Index  
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////// 
    public function getindexBondIndex()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 61,
            'menu_id' => 1,
            'title' => getMenuName($data, 61, 1) . ' | MEA'
        ] );

        Log::info('BondCompanyManagement::getindexBondIndex() => ' . getMenuName($data, 61, 1));

        $symbols =  DB::table('TBL_P2_BOND_INDEX')->orderby("SYMBOL")->get();       
        return view('backend.pages.p2_bond_tab3_index_page')->with(['symbols' => $symbols]);
    }


    public  function Ajax_Index_BondIndex(Request $request) 
    {
        Log::info('BondCompanyManagement::Ajax_Index_BondIndex' . $request);
        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $symbol = $request->input('symbol');
        //$cate_id = $request->input('cate_id');

        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;

        $ArrParam["symbol"] = $symbol;
        //$ArrParam["cate_id"] = $cate_id;


         
        $Datacount = $this->getCountAllBondIndex( $ArrParam );
        $Data = $this->getDataBondIndex($ArrParam);

        if($Datacount)
           $totals = count($Datacount);
        else
           $totals = 0;


        $htmlPaginate =Paginatre_gen($totals,$PageSize, 'page_click_search', $PageNumber);
    
        Log::info('view(backend.pages.ajax.ajax_p2_bond_tab3_index)');
        
 
        $returnHTML = view('backend.pages.ajax.ajax_p2_bond_tab3_index')->with([
            'htmlPaginate'=> $htmlPaginate,
            'data'=>$Data 

        ])->render();


         
        return response()->json(array('success' => true, 'html'=>$returnHTML));

    }

    public  function Ajax_BondIndexSearchForm(Request $request) 
    {
        $categorylist = DB::table('TBL_P2_BOND_CATEGORY')->orderby("CATE_ID")->get();  
        $symbols = DB::table('TBL_P2_BOND_INDEX')->orderby("SYMBOL")->get(); 
        $sectionList = array( );
        $tmp = array();
        $key = "";
        $child = array();

        foreach ($categorylist as $row) {
            $key = $row->BOND_CATE_THA;
            if (array_key_exists($key, $sectionList)) {
                $child = $sectionList[$key];
            } else {
                $child = array();
            }
            array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'INDUSTRIAL'=>$row->BOND_CATE_THA,
                                     'BU'=>$row->BOND_TYPE_THA
                                     ));
            $sectionList[$key] = $child;
        }

        $returnHTML = view('backend.pages.ajax.ajax_p2_bond_tab3_index_form')->with([
            'categorylist'   => $categorylist,
            'symbols'        => $symbols,
            'sectionList'    => $sectionList
        ])->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML)); 
    }

    public  function  getCountAllBondIndex($ArrParam) 
    {
        $where = "";

        if(trim($ArrParam["symbol"]) !="") {
 
              $where .= " SYMBOL LIKE '" . $ArrParam["symbol"] . "' ";
        } 
/*
        if(trim($ArrParam["market"]) !="") {
              $where .= " AND MARKET ='" . trim($ArrParam["market"]) . "' ";
        }

        if(trim($ArrParam["cate_id"]) !="") {
              $categorylist = DB::table('TBL_P2_BOND_CATEGORY')->where('CATE_ID', "=", trim($ArrParam["cate_id"]))->first();  
              
              $where .= " AND INDUSTRIAL ='" . $categorylist->INDUSTRIAL . "' " .
                       "  AND BU ='" . $categorylist->BU . "' ";
        }
       
       */

        if(strlen($where) > 1) {
           $where = " WHERE " . $where; 
        }

        $query =  "SELECT * FROM TBL_P2_BOND_INDEX " . $where . " ORDER BY SYMBOL DESC; ";// OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
        
        Log::info('BondCompanyManagement::getCountAllBondIndex()- SQLSTR=' .  $query ); 
        return DB::select(DB::raw($query));        
    }

    public  function  getDataBondIndex($ArrParam) 
    {

        /* limit execution timeout */
        //ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        //ini_set('memory_limit', '-1');

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];
        $where = "";


        if(trim($ArrParam["symbol"]) !="") {
              $where .= " SYMBOL LIKE '" . $ArrParam["symbol"] . "' ";
        }
        /*
        if(trim($ArrParam["market"]) !="") {
              $where .= " AND MARKET ='" . trim($ArrParam["market"]) . "' ";
        }

        if(trim($ArrParam["cate_id"]) !="") {
              $categorylist = DB::table('TBL_P2_BOND_CATEGORY')->where('CATE_ID', "=", trim($ArrParam["cate_id"]))->first();  
              
              $where .= " AND INDUSTRIAL ='" . $categorylist->INDUSTRIAL . "' " .
                       "  AND BU ='" . $categorylist->BU . "' ";
        }
        */
        
        if(strlen($where) > 1) {
           $where = " WHERE " . $where; 
        }

        if($ArrParam["PageNumber"]!="")
           $query =  "SELECT * FROM TBL_P2_BOND_INDEX " . $where . " ORDER BY SYMBOL DESC OFFSET ". $PageSize . " * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
        else
           $query =  "SELECT * FROM TBL_P2_BOND_INDEX " . $where . " ORDER BY SYMBOL DESC OFFSET ". $PageSize . " * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";  

        Log::info('BondCompanyManagement::getDataBondIndex()- SQLSTR=' .  $query ); 
        return DB::select(DB::raw($query));
    }

 
    public function getimportBondIndex() {
        //Log::debug('BondCompanyManagementController::getindexBondIndex() => start');

        $data = getmemulist();

        //Log::debug('BondCompanyManagementController::getindexBondIndex().$data =>'.print_r($data));


        $this->pageSetting([
            'menu_group_id' => 61,
            'menu_id' => 1,
            'title' => getMenuName($data, 61, 1) . ' | MEA'
        ]);

        //Log::info('BondcompanyManagement::getimportBondIndex() => ' . getMenuName($data, 61, 1));
         
        return view('backend.pages.p2_bond_tab3_import_index_page');
   
 
    }

    /**
     *  Import  นำเข้าข้อมูล หลักทรัพย์ TAB3 
     *  2017-03-29
     */
    public function is_set($val) {
        if(is_array($val)) return !empty($val);
      
        return strlen(trim($val)) ? true : false;
    }

    public function importBondIndex(Request $request) 
    {
        
        //Log::debug('Entered.............importBondIndex');

        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        /* statistics */ 
        $count       = 0;
        $skipedCount = 0;
        $passedCount = 0;
        $total_records = 0;
        $htmlResult = '';
        $status = false;
       
        $results = null;

        $file = $request->file('exelimport');

        $clientOriginalFileName = $request->file('exelimport')->getClientOriginalName();

        $request->file('exelimport')->move(storage_path().'/public/import/' , 'import_tab3.xlsx');
       
        $inputfile = storage_path('/public/import/import_tab3.xlsx');


        $user_data = Session::get('user_data');
        $emp_id = $user_data->emp_id;     // 25 

        $retdate = Excel::load($inputfile, function($reader) use(&$emp_id,
                                                                 &$status,
                                                                 &$clientOriginalFileName,
                                                                 &$count, 
                                                                 &$skipedCount, 
                                                                 &$passedCount, 
                                                                 &$total_records, 
                                                                 &$htmlResult) {
            $results = $reader->get(); 
            $ret = $results->toArray();
            $htmlResult = '';

            /* LAM prepare for progress bar */
            /////////////////////////////////////
            $progress_count = 0;

            $total_records = $results->count();
            $count = $total_records;

            $bondTypeList = [];
            Arr::set($bondTypeList,'GB','3');
            Arr::set($bondTypeList,'FB','20');
            Arr::set($bondTypeList,'SA','2');
            Arr::set($bondTypeList,'SOE','4');
            Arr::set($bondTypeList,'TB','1');

            //Log::debug(Arr::get($bondTypeList,'GB'));
            $deptTypeList = [];
            Arr::set($deptTypeList,'BE','21');
            Arr::set($deptTypeList,'DB','20');
  
            foreach($ret as $index => $value) {                                                    
                $data = array();

                //if (($value == null) || ($value["COMP_NAME"] == null) || (strlen($value["COMP_NAME"]) < 2)) {
                //    $skipedCount++;
                //    $status = false; 
                //    break;
                //}
                            
                //Log::info('REC: ' .  print_r($value, true));

                $bondType = strlen(str_replace(' ', '', trim($value["bond_type_by_issuer"]))) < 2 ? null : str_replace(' ', '', trim($value["bond_type_by_issuer"]));
                $debtType = strlen(str_replace(' ', '', trim($value["debt_instrument_type"]))) < 2 ? null : str_replace(' ', '', trim($value["debt_instrument_type"]));

                //Log::debug('bondType -> [' . $bondType.']');
                //Log::debug('debtType -> [' . $debtType.']');


                // TABLE [dbo].[TBL_P2_BOND_INDEX]                                      Excel Fields                        Array['fieldname']              Database Fields [DataType]
                //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                $INDEX_ID				   = 0;                                         //									                                [INDEX_ID] [int] NOT NULL,
                //$CATE_ID				   = ($value["debt_instrument_type"] === null) ? ($value["bond_type_by_issuer"] === null) ? "999": Arr::get($bondTypeList, $value["bond_type_by_issuer"]) : Arr::get($deptTypeList, $value["debt_instrument_type"]); // [CATE_ID] [int] NOT NULL,
                
                $cateId = ($debtType == '') ? ($bondType == '') ? "999": Arr::get($bondTypeList, $bondType) : Arr::get($deptTypeList, $debtType);
                $CATE_ID				   =  $cateId == '' ? "999" : $cateId;// [CATE_ID] [int] NOT NULL,
                
                $SYMBOL					   = $value["thaibma_symbol"];                  // ThaiBMA Symbol	                [thaibma_symbol]				[SYMBOL] [varchar](100) NOT NULL,
                $ISIN					   = $value["isin"];                            // ISIN			                    [isin]					        [ISIN] [varchar](255) NULL,
                $FULL_NAME				   = $value["issue_name_thai"];                 // Issue Name Thai	                [issue_name_thai]				[FULL_NAME] [text] NULL,
                $ISSUER					   = $value["issuer"];                          // Issuer			                [issuer]				        [ISSUER] [varchar](100) NULL,
                $COMP_RATING			   = $value["company_rating_rating_agency"];    // Company Rating / Rating Agency   [company_rating_rating_agency]	[COMP_RATING] [varchar](255) NULL,
                $BUSINESS_SECTOR		   = $value["business_sector"];                 // Business Sector		            [business_sector]			    [BUSINESS_SECTOR] [varchar](100) NULL,
                $BOND_TYPE				   = $value["bond_type_by_issuer"];             // Bond Type by Issuer	            [bond_type_by_issuer]			[BOND_TYPE] [varchar](100) NULL,
                $PRINCIPAL_PAYMENT		   = $value["principal_payment"];               // Principal Payment	            [principal_payment]				[PRINCIPAL_PAYMENT] [varchar](255) NULL,
                $BOND_STRUCTURE			   = $value["bond_structure"];                  // Bond Structure		            [bond_structure]			    [BOND_STRUCTURE] [varchar](255) NULL,
                $DEBT_TYPE				   = $value["debt_instrument_type"];            // Debt Instrument Type             [debt_instrument_type]			[DEBT_TYPE] [varchar](100) NULL,
                $SECURED_TYPE			   = $value["secured_type"];                    // Secured Type		                [secured_type]	 			    [SECURED_TYPE] [varchar](255) NULL,
                $SECURED_BY				   = $value["secured_by"];                      // Secured by			            [secured_by]			        [SECURED_BY] [varchar](255) NULL,
                $LIR1					   = $value["tris"];                            // TRIS				                [tris]				            [LIR1] [varchar](20) NULL,
                $LIR2					   = $value["fitch_thailand"];                  // Fitch (Thailand)	                [fitch_thailand]				[LIR2] [varchar](20) NULL,
                $IIR1					   = $value["moodys"];                          // Moody's				            [moodys]			            [IIR1] [varchar](20) NULL,
                $IIR2					   = $value["sp"];                              // S&P					            [sp]			                [IIR2] [varchar](20) NULL,
                $IIR3					   = $value["fitch_rating"];                    // Fitch Rating		                [fitch_rating]				    [IIR3] [varchar](20) NULL,
                $IIR4					   = $value["ri"];                              // R&I					            [ri]			                [IIR4] [varchar](20) NULL,
                $CURRENT_PAR			   = $value["current_par_thb"];                 // Current Par (THB)	            [current_par_thb]				[CURRENT_PAR] [float] NULL,
                $ISSUE_SIZE				   = $value["issue_size_thb_mln"];              // Issue Size (THB mln)             [issue_size_thb_mln]			[ISSUE_SIZE] [float] NULL,
                $OUTSTANDING_VALUE		   = (double)$value["outstanding_value_thb_mln"];       // Outstanding Value (THB mln)	    [outstanding_value_thb_mln]	    [OUTSTANDING_VALUE] [float] NOT NULL,
                $ISSUE_TERM				   = (double)$value["issue_term_yrs"];                  // Issue Term (Yrs.)	            [issue_term_yrs]				[ISSUE_TERM] [float] NULL,
                $TTM					   = $value["ttm_yrs"];                         // TTM (Yrs.)			            [ttm_yrs]			            [TTM] [float] NOT NULL,
                $ISSUE_DATE				   = $value["issue_date"];                      // Issue Date			            [issue_date]			        [ISSUE_DATE] [date] NOT NULL,
                $MATURITY_DATE			   = $value["maturity_date"];                   // Maturity Date		            [maturity_date]				    [MATURITY_DATE] [date] NOT NULL,                
                $REGISTERED_DATE		   = $value["registered_date"];                 // Registered Date		            [registered_date]			    [REGISTERED_DATE] [date] NULL,
                $COUPON_TYPE			   = $value["coupon_payment"];                  // Coupon Payment		            [coupon_payment]			    [COUPON_TYPE] [varchar](100) NOT NULL,
                
                $paymentDate = (strlen(trim($value["payment_date"]))) < 8 ? null : trim($value["payment_date"]);
                $PAYMENT_DATE			   = $paymentDate;                              // Payment Date		                [payment_date]				    [PAYMENT_DATE] [date] NULL,
                
                $xiDate = (strlen(trim($value["xi_date"]))) < 8 ? null : trim($value["xi_date"]);
                $XI_DATE				   = $xiDate;                                   // XI Date				            [xi_date]			            [XI_DATE] [date] NULL,
                
                $resetDate = (strlen(trim($value["reset_date"]))) < 8 ? null : trim($value["reset_date"]);
                $RESET_DATE				   = $resetDate;                                // Reset Date			            [reset_date]			        [RESET_DATE] [date] NULL,
                
                $COUPON_FREQUENCY		   = $value["coupon_frequency"];                // Coupon Frequency	                [coupon_frequency]				[COUPON_FREQUENCY] [varchar](100) NULL,
                $COUPON_RATE			   = (double)$value["coupon"];                          // Coupon (%)			            [coupon]			            [COUPON_RATE] [float] NOT NULL,
                $EMBEDDED_OPTION		   = $value["embedded_option"];                 // Embedded Option		            [embedded_option]			    [EMBEDDED_OPTION] [varchar](100) NULL,
                $CLAIM_TYPE				   = $value["claim_type"];                      // Claim Type			            [claim_type]			        [CLAIM_TYPE] [varchar](100) NULL,
                $DISTRIBUTION_TYPE		   = $value["distribution_type"];               // Distribution Type	            [distribution_type]				[DISTRIBUTION_TYPE] [varchar](100) NULL,
                $DURATION				   = (double)$value["duration"];                        // Duration			                [duration]				        [DURATION] [float] NULL,
                $REGISTRAR				   = $value["registrar"];                       // Registrar			            [registrar]				        [REGISTRAR] [varchar](100) NULL,
                $UNDERWRITER			   = $value["underwriter"];                     // Underwriter			            [Underwriter]			        [UNDERWRITER] [varchar](255) NULL,
                $BONDHOLDER_REPRESENTATIVE = $value["bondholder_representative"];       // Bondholder Representative        [bondholder_representative]		[BONDHOLDER_REPRESENTATIVE] [varchar](100) NULL,
                $ISSUER_NAME			   = $value["issuer_name"];                     // Issuer Name			            [issuer_name]			        [ISSUER_NAME] [varchar](255) NULL,
                $REFERENCE				   = $clientOriginalFileName;                   // 									                                [REFERENCE] [varchar](500) NULL,
                //$FLAG					   = 'NO';                                      // 									                                [FLAG] [varchar](20) NULL,
                //$CREATE_DATE             = date('Y-m-d');                             //									                                [CREATE_DATE] [datetime] NOT NULL,
                $CREATE_BY				   = $emp_id;                                   //									                                [CREATE_BY] [varchar](100) NOT NULL,
                //$MODIFY_DATE			   = date('Y-m-d');                             //									                                [MODIFY_DATE] [datetime] NULL,
                $MODIFY_BY				   = $emp_id;                                   //									                                [MODIFY_BY] [varchar](100) NULL,
                
                //Log::debug('$CATE_ID=>' . $CATE_ID);
                
                $allquery = "SELECT COUNT(SYMBOL) AS total FROM TBL_P2_BOND_INDEX  WHERE SYMBOL= '". $SYMBOL . "';";
                
                $all = DB::select(DB::raw($allquery));
                $total =  $all[0]->total;
                // Log::info('SQLSTR: ' .  $allquery);
                //$total = 0;
                if ($total == 0) {
                    /* creste new record */
                    $data = array(
                        'SYMBOL'                    => $SYMBOL,              
                        'CATE_ID'                   => $CATE_ID,				  				  
                        'SYMBOL'                    => $SYMBOL,					  				  
                        'ISIN'                      => $ISIN,					  					  
                        'FULL_NAME'                 => $FULL_NAME,				  				  
                        'ISSUER'                    => $ISSUER,					  					  
                        'COMP_RATING'               => $COMP_RATING,			  			  
                        'BUSINESS_SECTOR'           => $BUSINESS_SECTOR,		  		  
                        'BOND_TYPE'                 => $BOND_TYPE,				  				  
                        'PRINCIPAL_PAYMENT'         => $PRINCIPAL_PAYMENT,		  		  
                        'BOND_STRUCTURE'            => $BOND_STRUCTURE,			  			  
                        'DEBT_TYPE'                 => $DEBT_TYPE,				  				  
                        'SECURED_TYPE'              => $SECURED_TYPE,			  			  
                        'SECURED_BY'                => $SECURED_BY,				  				  
                        'LIR1'                      => $LIR1,					  					  
                        'LIR2'                      => $LIR2,					  					  
                        'IIR1'                      => $IIR1,					  					  
                        'IIR2'                      => $IIR2,					  					  
                        'IIR3'                      => $IIR3,					  					  
                        'IIR4'                      => $IIR4,					  					  
                        'CURRENT_PAR'               => $CURRENT_PAR,			  			  
                        'ISSUE_SIZE'                => $ISSUE_SIZE,				  				  
                        'OUTSTANDING_VALUE'         => $OUTSTANDING_VALUE,		  		  
                        'ISSUE_TERM'                => $ISSUE_TERM,				  				  
                        'TTM'                       => $TTM,					  					  
                        'ISSUE_DATE'                => $ISSUE_DATE,				  				  
                        'MATURITY_DATE'             => $MATURITY_DATE,			  			  
                        'REGISTERED_DATE'           => $REGISTERED_DATE,		  		  
                        'COUPON_TYPE'               => $COUPON_TYPE,			  			  
                        'PAYMENT_DATE'              => $PAYMENT_DATE,			  			  
                        'XI_DATE'                   => $XI_DATE,				  				  
                        'RESET_DATE'                => $RESET_DATE,				  				  
                        'COUPON_FREQUENCY'          => $COUPON_FREQUENCY,		  		  
                        'COUPON_RATE'               => $COUPON_RATE,			  			  
                        'EMBEDDED_OPTION'           => $EMBEDDED_OPTION,		  		  
                        'CLAIM_TYPE'                => $CLAIM_TYPE,				  				  
                        'DISTRIBUTION_TYPE'         => $DISTRIBUTION_TYPE,		  		  
                        'DURATION'                  => $DURATION,				  			  
                        'REGISTRAR'                 => $REGISTRAR,				  				  
                        'UNDERWRITER'               => $UNDERWRITER,			  			  
                        'BONDHOLDER_REPRESENTATIVE' => $BONDHOLDER_REPRESENTATIVE,
                        'ISSUER_NAME'               => $ISSUER_NAME,			  			  
                        'REFERENCE'                 => $REFERENCE,
                        'CREATE_BY'				    => $CREATE_BY,
                        'MODIFY_BY'		            => $MODIFY_BY		  				  
                    );

                    $affected = DB::table('TBL_P2_BOND_INDEX')->insert($data);
                    if($affected < 1) {
                         if(strlen($htmlResult) < 500) 
                         {
                            $htmlResult .= '<tr>';
                            $htmlResult .= '    <td>' . $SYMBOL     . '</td>';
                            $htmlResult .= '    <td>' . $COMP_NAME  . '</td>';
                            $htmlResult .= '    <td>' . $INDUSTRIAL . '</td>';
                            $htmlResult .= '    <td>' . $BU         . '</td>';
                            $htmlResult .= '    <td>' . $ADDRESS    . '</td></tr>';   
                         }
                    } else {
                        $passedCount++; 
                    }

                } else {
                         /* update, if already exist */
                    $data = array(
                       'SYMBOL'                    => $SYMBOL,              
                       'CATE_ID'                   => $CATE_ID,				  				  
                       'SYMBOL'                    => $SYMBOL,					  				  
                       'ISIN'                      => $ISIN,					  					  
                       'FULL_NAME'                 => $FULL_NAME,				  				  
                       'ISSUER'                    => $ISSUER,					  					  
                       'COMP_RATING'               => $COMP_RATING,			  			  
                       'BUSINESS_SECTOR'           => $BUSINESS_SECTOR,		  		  
                       'BOND_TYPE'                 => $BOND_TYPE,				  				  
                       'PRINCIPAL_PAYMENT'         => $PRINCIPAL_PAYMENT,		  		  
                       'BOND_STRUCTURE'            => $BOND_STRUCTURE,			  			  
                       'DEBT_TYPE'                 => $DEBT_TYPE,				  				  
                       'SECURED_TYPE'              => $SECURED_TYPE,			  			  
                       'SECURED_BY'                => $SECURED_BY,				  				  
                       'LIR1'                      => $LIR1,					  					  
                       'LIR2'                      => $LIR2,					  					  
                       'IIR1'                      => $IIR1,					  					  
                       'IIR2'                      => $IIR2,					  					  
                       'IIR3'                      => $IIR3,					  					  
                       'IIR4'                      => $IIR4,					  					  
                       'CURRENT_PAR'               => $CURRENT_PAR,			  			  
                       'ISSUE_SIZE'                => $ISSUE_SIZE,				  				  
                       'OUTSTANDING_VALUE'         => $OUTSTANDING_VALUE,		  		  
                       'ISSUE_TERM'                => $ISSUE_TERM,				  				  
                       'TTM'                       => $TTM,					  					  
                       'ISSUE_DATE'                => $ISSUE_DATE,				  				  
                       'MATURITY_DATE'             => $MATURITY_DATE,			  			  
                       'REGISTERED_DATE'           => $REGISTERED_DATE,		  		  
                       'COUPON_TYPE'               => $COUPON_TYPE,			  			  
                       'PAYMENT_DATE'              => $PAYMENT_DATE,			  			  
                       'XI_DATE'                   => $XI_DATE,				  				  
                       'RESET_DATE'                => $RESET_DATE,				  				  
                       'COUPON_FREQUENCY'          => $COUPON_FREQUENCY,		  		  
                       'COUPON_RATE'               => $COUPON_RATE,			  			  
                       'EMBEDDED_OPTION'           => $EMBEDDED_OPTION,		  		  
                       'CLAIM_TYPE'                => $CLAIM_TYPE,				  				  
                       'DISTRIBUTION_TYPE'         => $DISTRIBUTION_TYPE,		  		  
                       'DURATION'                  => $DURATION,				  			  
                       'REGISTRAR'                 => $REGISTRAR,				  				  
                       'UNDERWRITER'               => $UNDERWRITER,			  			  
                       'BONDHOLDER_REPRESENTATIVE' => $BONDHOLDER_REPRESENTATIVE,
                       'ISSUER_NAME'               => $ISSUER_NAME,			  			  
                       'REFERENCE'                 => $REFERENCE,
                       'MODIFY_BY'	               => $MODIFY_BY			  				  
                    );

                    $affected = DB::table('TBL_P2_BOND_INDEX')
                                    ->where('SYMBOL', "=", $SYMBOL)
                                    ->update($data);
                    
                    if($affected < 1) {
                         if(strlen($htmlResult) < 500) {
                            $htmlResult .= ' <tr>';
                            $htmlResult .= '    <td>' . $SYMBOL     . '</td>';
                            $htmlResult .= '    <td>' . $COMP_NAME  . '</td>';
                            $htmlResult .= '    <td>' . $INDUSTRIAL . '</td>';
                            $htmlResult .= '    <td>' . $BU         . '</td>';
                            $htmlResult .= '    <td>' . $ADDRESS    . '</td> </tr>';   
                         }
                    } else {
                        $passedCount++; 
                    }
                }  
            } // foreach
        });  // Excel::load

        $header = 'รายละเอียดข้อมูลที่ ไม่สำเร็จ <BR> '.
                  '<table style=\"width: 100px;\" class=\"table table-bordered\">' .
                  '<tr><td>รหัส หมวดหมู่หลักทรัพย์</td>' . 
                  '    <td>ชื่อบริษัทิ</td>'. 
                  '    <td>กลุ่มอุตสาหกรรม</td>' . 
                  '    <td>หมวดธุรกิจ</td>'. 
                  '    <td>ที่อยู่</td></tr>';

        $footer = '</table>  ';

        if(strlen($htmlResult) < 1) {
            $header  = '';
            $footer = '';  
            $htmlResult = ' ###### ';
        }

        $status = (($count - $passedCount) == 0);
        /* put 100% */ 
        // Session::put('progress_count', $total_records);

        return response()->json(array('success' => $status, 'html'=>'<br>' . 
                ' Total : ' . $count . ' Record(s), '. 
                ' Passed: ' . $passedCount . ' Record(s), '. 
                ' Failed: ' . ($count - $passedCount) . ' Record(s), '. 
                ' <BR>'. $header . $htmlResult . $footer));
        
    } // importdata



    /**
     * Handle request delete category single/multiple records.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function deleteBondIndex(Request $request)
    {

        Log::info('BondCompanyManagement::deleteBondIndex()' . $request); 

        $deleted = false;
        $arrId = explode(',', $request->input('group_id'));

        foreach($arrId as $index => $item) {

            if($item != "") {
                $deleted =  DB::table('TBL_P2_BOND_INDEX')->where('INDEX_ID',"=", $item)->delete();
            }
        }

        if($deleted)  {
            return response()->json(["ret" => "1"]);
        } else {
            return response()->json(["ret" => "0"]);
        }
    }


    /**
     *  add view bond securities 
     *  TAB#3
     * @param  None
     * @return view\backend\pages
     */
    public function getAddBondIndex()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 61,
            'menu_id' => 1,
            'title' => getMenuName($data,61, 1) . ' | MEA'
        ] );
        
        /////////////
        $categorylist   = DB::table('TBL_P2_BOND_CATEGORY')->orderby("INDUSTRIAL")->get();  
        $securitieslist = DB::table('TBL_P2_BOND_SECURITIES')->orderby("NAME_SHT")->get(); 
        $sectionList = array( );
        $tmp = array();
        $key = "";

        $child = array();
        foreach ($categorylist as $row) {
            $key = $row->INDUSTRIAL;
            if (array_key_exists($key, $sectionList)) {
                $child = $sectionList[$key];
            } else {
                $child = array();
            }
            array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'BU'=>$row->BU,
                                     'INDUSTRIAL'=>$row->INDUSTRIAL
                                     ));
            $sectionList[$key] = $child;
        }  

       /* foreach ($categorylist as $row) {
            if($key != $row->INDUSTRIAL) {
               if($key == "") {
                   $child = array();
               } else {
                  $sectionList[$key] = $child;
                  $child = array();
               }
            } 
            $key = $row->INDUSTRIAL;
            array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'BU'=>$row->BU,
                                     'INDUSTRIAL'=>$row->INDUSTRIAL));
        }
        */
        return view('backend.pages.p2_bond_tab3_add_index_page')->with([
            
            'categorylist'   => $categorylist,
            'securitieslist' => $securitieslist,
            'sectionList'    => $sectionList

        ]); //->render();
    }

    /**
     * Get view equity category with data by matched with $id
     * 
     * @param  Equity company code  $id
     * @return view backend.pages.p2_edit_equity_company_page
     */
    public function getEditBondIndex($id)
    {
        Log::info('AAA # BondCompanyManagement::getEditBondIndex() -> id=' . $id . '\n'); 
        
        
        $data = getmemulist();
        $this->pageSetting([
            'menu_group_id' => 61,
            'menu_id' => 1,
            'title' => getMenuName($data, 61, 1) . ' | MEA'
        ]);

 
        $categorylist = DB::table('TBL_P2_BOND_CATEGORY')->orderby("BOND_CATE_THA")->get();  
        $securitieslist =  DB::table('TBL_P2_BOND_SECURITIES')->orderby("NAME_SHT")->get(); 
        $sectionList = array( );
        $tmp = array();
        $key = "";

        $child = array();
        foreach ($categorylist as $row) {
            $key = $row->BOND_CATE_THA;
            if (array_key_exists($key, $sectionList)) {
                $child = $sectionList[$key];
            } else {
                $child = array();
            }
            array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'BU'=>$row->BOND_TYPE_THA,
                                     'INDUSTRIAL'=>$row->BOND_CATE_THA
                                     ));
            $sectionList[$key] = $child;
        }
        
        /*
        foreach ($categorylist as $row) {
            if($key != $row->INDUSTRIAL) {
               if($key == "") {
                   $child = array();
               } else {
                  $sectionList[$key] = $child;
                  $child = array();
               }
            } 
            $key = $row->INDUSTRIAL;
            array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'BU'=>$row->BU,
                                     'INDUSTRIAL'=>$row->INDUSTRIAL));
        }
        */

        $editdata = DB::table('TBL_P2_BOND_INDEX')->where("INDEX_ID" ,"=",  $id)->first();
        
        if($editdata) {
            return view('backend.pages.p2_bond_tab3_edit_index_page')->with([
                'editdata'       => $editdata,  //data at edit current record
                'categorylist'   => $categorylist,
                'securitieslist' => $securitieslist,
                'sectionList'    => $sectionList
                ]);
        }

        Log::info('BBBB # BondCompanyManagement::getEditBondIndex() -> id=' . $id . '\n'); 
        
        // any error occurred redirect to page 404
        abort(404);
    }

    public function dowloadsampleBondIndex() {
        $file = 'contents/sample/SampleBondFeature.xlsx';
        $headers = array(
            'Content-Type: application/vnd.ms-excel',
        );
        return \Response::download($file, 'SampleBondFeature.xlsx', $headers);
    }

    public function postAddBondIndex(Request $request) {
        $status = false;
        $html = "Error";
        $industrial_cate_id  = $request->input('industrial_cate_id');
         
        $record_id  = $request->input('record_id');
        $market     = $request->input('market');      
        $setindex   = $request->input('setindex');   
        $industrial_cate_id = $request->input('industrial_cate_id');                  
        $name_sht   = $request->input('name_sht'); // or symbol ?
        $new_symbol = $request->input('new_symbol');
        $effective_date = $request->input('effective_date');// effective_date,


        $zipcode = $request->input('zipcode');
        $equity_url = $request->input('equity_url');
        $company_name  = $request->input('company_name');
        $company_addr = $request->input('company_addr');

        $company_phone = $request->input('company_phone');
        $company_fax = $request->input('company_fax');

        $equity_start = $request->input('equity_start');
        $equity_end = $request->input('equity_end');

        Log::info(get_class() . ' -> ' . $request);

        

        if((!$setindex) || ($setindex == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ SET Index !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        if((!$market) || ($market == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ ตลาด !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        if((!$name_sht) || ($name_sht == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ ชื่อย่อหลักทรัพย์ !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        if((!$company_name) || ($company_name == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ ชื่อบริษัทิ !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        if((!$equity_start) || ($equity_start == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ วันเริ่มต้น !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }
        if((!$equity_end) || ($equity_end == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ วันสิ้นสุด!';
            return response()->json(array('success' => $status, 'html'=>$html));
        }
    

        $categorylist = DB::table('TBL_P2_BOND_CATEGORY')->where('CATE_ID', "=", trim($industrial_cate_id))->first();  
        if(!$categorylist) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ไม่พบ รหัส กลุ่มอุตสาหกรรม และกลุ่มธุรกิจ ที่ท่านป้อน !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        $BU = $categorylist->BU;
        $INDUSTRIAL = $categorylist->INDUSTRIAL;
        $utils = new MEAUtils();

        $equity_start = $utils->toEnglishDate($equity_start);
        $equity_end   = $utils->toEnglishDate($equity_end);

        $ar1 = explode('-',$equity_start);
        $ar2 = explode('-',$equity_end);
        $stdate = $ar1[2] . '-' . $ar1[1] . '-'. $ar1[0];
        $enddate = $ar2[2] . '-' . $ar2[1] . '-'. $ar2[0];


        $setindex100  = '';
        $setindex50   = '';
        switch ($setindex) {
            case 'SET100':
                $setindex100 = 'Y';
                break;

            case 'SET50':
                $setindex50 = 'Y';
                $setindex100 = 'Y';
                break;

            case 'None':
            case 'NONE':
               $setindex100  = '';
               $setindex50   = '';
               break;    
            
            default:
                # code...
                break;
        }
        Log::info(get_class() . ' -> equity_start=' . $equity_start . ', equity_end=' . $equity_end);

        // Retrive logged ID
        $user_data = Session::get('user_data');

        $data = array();

        array_push($data,array(

            'MARKET' => $market,
            
            'INDUSTRIAL' => $INDUSTRIAL,
            'BU' => $BU,
            'SYMBOL' => $name_sht,
            'COMP_NAME' => $company_name,
            'ADDRESS' => $company_addr,
            'ZIP_CODE' => $zipcode, 
            
            'SET_INDEX100' => $setindex100,
            'SET_INDEX50' => $setindex50,

            'PHONE' => $company_phone,
            'FAX' => $company_fax,
            'URL' => $equity_url,

            'START_DATE' => $stdate, //$equity_start,
            'END_DATE' => $enddate, //$equity_end,

            'CREATE_DATE' => date("Y-m-d H:i:s"),
            'CREATE_BY' => $user_data->emp_id

        ));

        Log::info(get_class() . ' data => ' .  print_r($data, true));
        //$status = false;
        $affected = DB::table('TBL_P2_BOND_INDEX')->insert($data);
        $status = ($affected > 0);

        return response()->json(array('success' => $status, 'html'=>$html));
                
    }

     public function postEditBondIndex(Request $request) {
        $status = false;
        $html = "Error!!!";
        //$industrial_cate_id  = $request->input('record_id');

        $industrial_cate_id  = $request->input('industrial_cate_id');


         
        $record_id  = $request->input('record_id');
        $market     = $request->input('market');      
        $setindex   = $request->input('setindex');   
        $industrial_cate_id = $request->input('industrial_cate_id');                  
        $name_sht   = $request->input('name_sht'); // or symbol ?
        $new_symbol = $request->input('new_symbol');
        $effective_date = $request->input('effective_date');// effective_date,


        $zipcode = $request->input('zipcode');
        $equity_url = $request->input('equity_url');
        $company_name  = $request->input('company_name');
        $company_addr = $request->input('company_addr');

        $company_phone = $request->input('company_phone');
        $company_fax = $request->input('company_fax');

        $equity_start = $request->input('equity_start');
        $equity_end = $request->input('equity_end');

        Log::info(get_class() . ' -> ' . $request);

        $old_symbol = $name_sht;

        if((!$setindex) || ($setindex == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ SET Index !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        if((!$market) || ($market == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ ตลาด !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        if((!$name_sht) || ($name_sht == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ ชื่อย่อหลักทรัพย์ !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        if((!$company_name) || ($company_name == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ ชื่อบริษัทิ !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        if((!$equity_start) || ($equity_start == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ วันเริ่มต้น !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }
        if((!$equity_end) || ($equity_end == "")) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ท่านไม่ได้ระบุ วันสิ้นสุด!';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        $newflag = (($new_symbol !="") && ($effective_date !=""));
        if($newflag) {
            $old_symbol = $name_sht;

        } else {
            $new_symbol = $name_sht;
        }
        

        $categorylist = DB::table('TBL_P2_BOND_CATEGORY')->where('CATE_ID', "=", trim($industrial_cate_id))->first();  
        if(!$categorylist) {
            $status = false;
            $html = ' เกิดข้อผิดพลาด: ไม่พบ รหัส กลุ่มอุตสาหกรรม และกลุ่มธุรกิจ ที่ท่านป้อน !';
            return response()->json(array('success' => $status, 'html'=>$html));
        }

        $BU = $categorylist->BU;
        $INDUSTRIAL = $categorylist->INDUSTRIAL;
        $utils = new MEAUtils();

        $equity_start = $utils->toEnglishDate($equity_start);
        $equity_end   = $utils->toEnglishDate($equity_end);

        $ar1 = explode('-',$equity_start);
        $ar2 = explode('-',$equity_end);
        $stdate = $ar1[2] . '-' . $ar1[1] . '-'. $ar1[0];
        $enddate = $ar2[2] . '-' . $ar2[1] . '-'. $ar2[0];


        $setindex100  = '';
        $setindex50   = '';
        switch ($setindex) {
            case 'SET100':
                $setindex100 = 'Y';
                break;

            case 'SET50':
                $setindex50 = 'Y';
                $setindex100 = 'Y';
                break;

            case 'None':
            case 'NONE':
               $setindex100  = '';
               $setindex50   = '';
               break;    
            
            default:
                # code...
                break;
        }
        Log::info(get_class() . ' -> equity_start=' . $equity_start . ', equity_end=' . $equity_end);

        // Retrive logged ID
        $user_data = Session::get('user_data');

        $data = array(
            'SYMBOL_RENAME_FLAG' => ($newflag ? "1" : "0"),
            'OLD_SYMBOL'  => $old_symbol,

            
            'MARKET' => $market,
            
            'INDUSTRIAL' => $INDUSTRIAL,
            'BU' => $BU,
            'SYMBOL' => $new_symbol, //$name_sht,
            'COMP_NAME' => $company_name,
            'ADDRESS' => $company_addr,
            'ZIP_CODE' => $zipcode, 
            
            'SET_INDEX100' => $setindex100,
            'SET_INDEX50' => $setindex50,

            'PHONE' => $company_phone,
            'FAX' => $company_fax,
            'URL' => $equity_url,

            'START_DATE' => $stdate, //$equity_start,
            'END_DATE' => $enddate, //$equity_end,

            'MODIFY_DATE' => date("Y-m-d H:i:s"),
            'MODIFY_BY' => $user_data->emp_id

        );

        Log::info(get_class() . ' data => ' .  print_r($data, true) . '\r\n');
        //$status = false;
        $affected = DB::table('TBL_P2_BOND_INDEX')->where('INDEX_ID', $record_id)->update($data);
        $status = ($affected > 0);

        return response()->json(array('success' => $status, 'html'=>$html));
                
    }



    //////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * TAB#4 
     */

    public function getindexBroker()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 61,
            'menu_id' => 1,
            'title' => getMenuName($data, 61, 1) . ' | MEA'
        ] );

        return view('backend.pages.p2_bond_tab4_broker_page');
    }
    
    


    public  function Ajax_Index_Broker(Request $request){

        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;

        $Datacount = $this->getCountAllBroker();
        $Data = $this->getDataBroker($ArrParam);

        $totals = count($Datacount);

        $htmlPaginate =Paginatre_gen($totals,$PageSize,'page_click_search',$PageNumber);

        $returnHTML = view('backend.pages.ajax.ajax_p2_bond_tab4_broker')->with([
            'htmlPaginate'=> $htmlPaginate,
            'data'=>$Data

        ])->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }


    public  function  getCountAllBroker(){
        Log::info('BondCompanyManagement::getCountAllBroker()');
        return DB::table('TBL_P2_BOND_BROKER')->orderby("NAME_SHT")->get();
    }

    public function getDataBroker($ArrParam){
        // limit execution timeout 
        ini_set('max_execution_time', 30000);
        // unlimit memory size 
        ini_set('memory_limit', '-1');

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];

        $query =  "SELECT * FROM TBL_P2_BOND_BROKER ORDER BY NAME_SHT OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
        return DB::select(DB::raw($query));
    }

    
    public function deleteBroker(Request $request)
    {
        $deleted = false;
        $arrId = explode(',',$request->input('group_id'));

        foreach($arrId as $index => $item){

            if($item != ""){
                $deleted =  DB::table('TBL_P2_BOND_BROKER')->where('NAME_SHT',"=", $item)->delete();
            }
        }

        if($deleted)  {
            return response()->json(["ret" => "1"]);
        } else {
            return response()->json(["ret" => "0"]);
        }
    }


   
    public function getAddBroker()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 61,
            'menu_id' => 1,
            'title' => getMenuName($data,61, 1) . ' | MEA'
        ] );

        return view('backend.pages.p2_bond_tab4_add_broker_page');
    }

    
    public function getEditBroker($id)
    {
        $data = getmemulist();
        $this->pageSetting([
            'menu_group_id' => 61,
            'menu_id' => 1,
            'title' => getMenuName($data, 61, 1) . ' | MEA'
        ]);


        $editdata = DB::table('TBL_P2_BOND_BROKER')->where("NAME_SHT" ,"=",  $id)->first();
        
        if($editdata) {
            return view('backend.pages.p2_bond_tab4_edit_broker_page')->with(['editdata'=>$editdata]);
        }
        
        abort(404);
    }


   
    public function postAddBroker(Request $request)
    {
        // limit execution timeout 
        ini_set('max_execution_time', 30000);
        // unlimit memory size 
        ini_set('memory_limit', '-1');
        

        Log::info('BondCompanyManagementController::PoseAdd::=>' . $request);
        Log::info(' postAdd: equity_start=>' . toEnglishDate($request["equity_start"])); 

        $ret = false;
        $datestart  = new Date(toEnglishDate($request["equity_start"]));
        $datereq = $request["equity_end"];
         
        
        if ($request["company_addr"] == '') {

        }

        
        if ($request["equity_start"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันเริ่มต้น ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        if ($request["equity_end"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันสิ้นสุด ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }
        

        Log::info(' postAdd: equity_start=>' . toEnglishDate($datereq));   
        $dateEnd = new Date(toEnglishDate($datereq));

        $today   = new Date();
        $data    = array();

        array_push($data,array(
            'NAME_SHT' => $request["name_sht"],
            //Securities_Name
            'BROKER_NAME' =>$request["company_name"],
            'ADDRESS' => $request["company_addr"],

            'START_DATE' => $datestart,
            'END_DATE' => $dateEnd,

            'PHONE' => $request["company_phone"],
            'FAX' => $request["company_fax"],
            'CREATE_DATE' => $today,
            'CREATE_BY' => "Admin"
        ));


        $chk = "SELECT COUNT(NAME_SHT) As total FROM TBL_P2_BOND_BROKER WHERE NAME_SHT = '". $request["name_sht"]. "'";
        $all = DB::select(DB::raw($chk));
        $total =  $all[0]->total;

        $rethtml = "";


        if ($total > 0) {
           $rethtml = "Broker code name ที่ท่านใส่มีอยู่ในระบบแล้ว";

        } else {
           $insert = DB::table('TBL_P2_BOND_BROKER')->insert($data);
           $ret = $insert;
        }
        return response()->json(array('success' => $ret, 'html'=>$rethtml));
    }


    
    public function postEditBroker(Request $request)
    {
        $ret = false;
        $datestart  = new Date(toEnglishDate($request["equity_start"]));
        $datereq = $request["equity_end"];

        if ($request["equity_end"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันสิ้นสุด ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }
        if ($request["equity_start"] == "") {
            $rethtml = "มีข้อผิดพลาด ไม่ได้กำหนดข้อมูล วันเริ่มต้น ";
            //$datereq = "9999-12-31 00:00:00.000";
            return response()->json(array('success' => $ret, 'html'=>$rethtml));
        }

        Log::info(' postEdit: request =>' . $request);   

        $dateEnd = new Date(toEnglishDate($datereq));
        $today = new Date();

        $data = array(
            'NAME_SHT' => $request["name_sht"],
            'BROKER_NAME' =>$request["company_name"],
            'ADDRESS' => $request["company_addr"],

            'START_DATE' => $datestart,
            'END_DATE' => $dateEnd,

            'PHONE' => $request["company_phone"],
            'FAX' => $request["company_fax"],

            'CREATE_BY' => "Admin"
        );

        // $update = DB::table('TBL_P2_BOND_BROKER')->where('NAME_SHT', "=", $request["name_sht"])->update($data);
        $update = DB::table('TBL_P2_BOND_BROKER')->where('BROKER_ID', "=", $request["broker_id"])->update($data);

        $ret = $update;

        return response()->json(array('success' => $ret, 'html'=>'OK'));

    }
    

    /**
     * Export to CSV file 
     */
    public function ajax_report_search_export(Request $request) {
       // Log::info(get_class($this) .'::'. __FUNCTION__);
    
         // limit execution timeout 
        ini_set('max_execution_time', 30000);
        // unlimit memory size 
        ini_set('memory_limit', '-1');

        $menu = getmemulist();
        $title = getMenuName($menu, 61, 1);
          

        $symbol = $request->input('symbol');
        $cate_id = $request->input('cate_id');
        $market = $request->input('market');
       
        $ArrParam = array();
        $ArrParam["pagesize"] ="";
        $ArrParam["PageNumber"] ="";

        $ArrParam["symbol"] = $symbol;
        //$ArrParam["cate_id"] = $cate_id;
        //$ArrParam["market"] = $market;

        //$data = $this->DataSource($ArrParam, false, false);
        $utils = new MEAUtils();
      
        Log::info('Export Parameters:' .  print_r($ArrParam, true));
        /*$all = $this->DataSourceCount($ArrParam, false, false);
        $totals = $all[0]->TOTAL; 
        $count_all = $all[0]->COUNT_ALL;
        $sum_all  =  $all[0]->SUM_ALL;
        $data = $this->DataSource($ArrParam, false, false);
*/
        $Datacount = $this->getCountAllBondIndex( $ArrParam );
        $data = $this->getDataBondIndex($ArrParam);

        if($Datacount)
           $totals = count($Datacount);
        else
           $totals = 0;

        $title = "listedcompanies_" . date('Y-m-d');
        $results = array();
        foreach ($data as $item) {
            $results[] = (array)$item;
        }

        Excel::create($title, function ($excel) use ($results, $ArrParam, $title,  $totals) {

            $excel->sheet("listedcompanies", function ($sheet) use ($results, $ArrParam, $title, $totals) {
                // first row styling and writing content
                $sheet->setStyle([
                    'borders' => [
                        'allborders' => [
                            'color' => [
                                'rgb' => 'b3b6b7'
                            ]
                        ]
                    ]
                ]);
                

                $header[]   = null;
                $header[0]  = "INDEX_ID";
                $header[1]  = "SYMBOL";
                $header[2]  = "SYMBOL_RENAME_FLAG";
                $header[3]  = "OLD_SYMBOL";
                $header[4]  = "MODIFY_DATE";
                $header[5]  = "MODIFY_BY";
                $header[6]  = "EFFECTIVE_DATE";
                $header[7]  = "COMP_NAME";
                $header[8]  = "MARKET";
                $header[9]  = "SET_INDEX50";
                $header[10]  = "SET_INDEX100";
                $header[11]  = "SET_HD";
                $header[12]  = "INDUSTRIAL";
                $header[13]  = "BU";
                $header[14]  = "ADDRESS";
                $header[15]  = "ZIP_CODE";
                $header[16]  = "PHONE";
                $header[17]  = "FAX";
                $header[19]  = "URL";
                $header[20]  = "START_DATE";
                $header[21]  = "END_DATE";
                $header[22]  = "CREATE_DATE";
                $header[23]  = "CREATE_BY";
                
                //$sheet->row(1, $header);

                // Set multiple column formats
               /* $sheet->setColumnFormat(array(
                        'A' => '@',
                        'B' => '@',
                        'C' => '@',
                        
                        //'D' => '@',
                        'E' => '#,##0.00',
                        //'F' => '@',
                        'G' => '#,##0.00', // SUM(TOTAL_PURCHASE)
                        //'H' => '@',  
                        'I' => '#,##0.00',
                        'J' => '#,##0.00', // SUM(TOTAL_SALE)
                        //'K' => '#,##0.00',
                        //'L' => '#,##0.00', // SUM(PROFIT_LOSS)
                        //'M' => '#,##0.00'  
                ));
                */
                $row = 2;
                $sheet->fromArray($results);
                /*
                foreach ($results as $item) {
                    $rowitem[]   = null;
                    $rowitem[0]   = $item["INDEX_ID"];
                    $rowitem[1]   = $item["SYMBOL"];
                    $rowitem[2]   = $item["SYMBOL_RENAME_FLAG"];
                    $rowitem[3]   = $item["OLD_SYMBOL"];
                    $rowitem[4]   = $item["MODIFY_DATE"];
                    $rowitem[5]   = $item["MODIFY_BY"];
                    $rowitem[6]   = $item["EFFECTIVE_DATE"];
                    $rowitem[7]   = $item["COMP_NAME"];
                    $rowitem[8]   = $item["MARKET"];
                    $rowitem[9]   = $item["SET_INDEX50"];
                    $rowitem[10]  = $item["SET_INDEX100"];
                    $rowitem[11]  = $item["SET_HD"];
                    $rowitem[12]  = $item["INDUSTRIAL"];
                    $rowitem[13]  = $item["BU"];
                    $rowitem[14]  = $item["ADDRESS"];
                    $rowitem[15]  = $item["ZIP_CODE"];
                    $rowitem[16]  = $item["PHONE"];
                    $rowitem[17]  = $item["FAX"];
                    $rowitem[19]  = $item["URL"];
                    $rowitem[20]  = $item["START_DATE"];
                    $rowitem[21]  = $item["END_DATE"];
                    $rowitem[22]  = $item["CREATE_DATE"];
                    $rowitem[23]  = $item["CREATE_BY"];
                    $sheet->row($row, $rowitem);
                    $row++;
                    if($row < 10)
                       Log::info('ITEM:' .  print_r($item, true));
                    
                }*/
            });
        })->download('xls');
   }

} // class BondCompanyManagementController
 



