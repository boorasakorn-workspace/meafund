<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Jenssegers\Date\Date;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;


use App\Libraries\MEAUtils;
use Illuminate\Support\Facades\Log;

class AdminP2BondTradingReportController extends Controller
{
    /** 
     *  ตราสารหนี้
     * รายงานซื้อ-ขายหลักทรัพย์ (Bond Trading Report)
     */
   /* public function matGetYear($date_1 , $date_2) {
      $datetime1 = date_create($date_1);
      $datetime2 = date_create($date_2);
     
      $interval = date_diff($datetime1, $datetime2);
      $ss = $interval->format('%y,%m,%d');
      return explode(",", $ss);
    }*/

    
    public function matGetYear($date_1 , $date_2 ) {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);
   
        $interval = date_diff($datetime1, $datetime2);
        $ss = $interval->format('%y,%m,%d');

        $days = (int)$interval->format("%r%a");
        if($days < 0) {
            // fixed: minus value
           return explode(",", "0,0,0");
        }
        return explode(",", $ss);
      
    }

    public function getreport()
    {
        $view_name = 'backend.pages.p2_bond_trading_report';
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 63,
            'menu_id' => 2,
            'title' => getMenuName($data,63,2) . '|  MEA FUND'
        ]);

        $allquery = "SELECT * FROM TBL_P2_BOND_SECURITIES";
        $equitylist = DB::select(DB::raw($allquery));

       /* $allquery = "SELECT * FROM TBL_P2_EQUITY_CATEGORY ORDER BY INDUSTRIAL";
        $equitycategory = DB::select(DB::raw($allquery));
*/
        $allquery = "SELECT * FROM TBL_P2_BOND_BROKER ORDER BY NAME_SHT ASC;";
        $equitybroker = DB::select(DB::raw($allquery));

        $allquery = "SELECT SYMBOL FROM TBL_P2_BOND_INDEX ORDER BY SYMBOL ASC;";
        $equityindex = DB::select(DB::raw($allquery));
  
        $years = array();
        for($i= date('Y'); $i>date('Y')-25; $i--) {
           array_push($years, $i);
        }
 
        /////////////
        $categorylist = DB::table('TBL_P2_BOND_CATEGORY')->orderby("BOND_CATE_THA")->get();  

        $securitieslist =  DB::table('TBL_P2_EQUITY_SECURITIES')->orderby("NAME_SHT")->get(); 
        $sectionList = array( );
        $tmp = array();
        $key = "";

        $child = array();

        foreach ($categorylist as $row) {
            if($key != $row->BOND_CATE_THA) {
               if($key == "") {
                   $child = array();
               } else {
                  $sectionList[$key] = $child;
                  $child = array();
               }
            } 
            $key = $row->BOND_CATE_THA;
            array_push($child, array('CATE_ID'=>$row->CATE_ID, 
                                     'BOND_TYPE_THA'=>$row->BOND_TYPE_THA,
                                     'BOND_CATE_THA'=>$row->BOND_CATE_THA));
        }
        $sectionList[$key] = $child;

        
        return view($view_name)->with([
            'equitylist'     => $equitylist,
            //'equitycategory' => $equitycategory,
            'equitybroker'   => $equitybroker,
            'years'          => $years,
            'symbols'        => $equityindex,
            'securitieslist' => $securitieslist,
            'sectionList'    => $sectionList
        ]);
    }

    public  function  DataSourceTradingReportCount($ArrParam, $IsCase){
        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];
        //$symbol   = $ArrParam["symbol"] ;
        //$name_sht = $ArrParam["name_sht"];
        $industrial = $ArrParam["industrial"];
        $age = $ArrParam["age"];
        
        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];
    }

   public  function  DataSourceCount($ArrParam, $IsCase){

        /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];

        // Log::info(get_class($this) .'::'. __FUNCTION__);

        $symbol     = $ArrParam["symbol"] ;       // APCS, CPALL
        $name_sht   = $ArrParam["name_sht"];      // eg. UOBAM, KTAM 
        $age        = $ArrParam["age"];
        $industrial = $ArrParam["industrial"];  // CATE_ID;
        $cateid     = $ArrParam["industrial"];  

        $bu = '';
        $indus = '';

        if((!empty($industrial)) && (strlen($industrial) > 0)) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
                           ->orderby("BOND_CATE_THA")
                           ->where('CATE_ID','=', $industrial)->get();
            foreach ($categorylist as $row) {
                $indus = $row->BOND_CATE_THA;
                $bu    = $row->BOND_TYPE_THA;
                break;  
            }
        }
        
        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 5) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 5) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        $where = "" ;

      
        // WHERE2
        $where2 = "";
        if(strlen(trim($symbol)) > 0) {  
            $where2 .= " P.SYMBOL = '". $symbol ."' ";
        }

        if(strlen(trim($name_sht)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " P.SECURITIES_NAME = '".$name_sht."' ";
        }

        if(strlen(trim($cateid)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND C.CATE_ID = '".$cateid."' ";
            else
               $where2 .= " C.CATE_ID = '".$cateid."' ";
        }

        Log::info('1) #### AGE ##### =>[' . $age . "] LEN=" . strlen($age));
        if(strlen(trim($age)) > 0) {  
            $prefix = "";
            if(strlen($where2) > 1) {
                 $prefix = " AND ";
            }
            $where2 .= $prefix;
            Log::info('2) #### AGE ##### = ' . $age);
            $agefield = " DATEDIFF(MONTH, GETDATE(), P.MAT) as AGE_MONTH ";
            switch(intval(trim($age))) {
                
                case 0;
                    // คงเหลือน้อยกว่าหรือเท่ากับ 3 เดือน (MAT เทียบกับ today >= 1 day <= 3 month)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/4)) ";
                   break;

                case 1:
                    // > 3 เดือน <= 6 เดือน
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= (365/4)) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/2)) ";
                    
                   break;
/*
                case 2:
                    // คงเหลือน้อยกว่าหรือเท่ากับ 1 ปี (MAT เทียบกับ today >= 1 day <= 1 year)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;  
*/
                case 2:
                    //  คงเหลือมากกว่า 6 เดือน แต่น้อยกว่า หรือเท่ากับ 1 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365/2)) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;

                case 3:
                    // คงเหลือมากกว่า 1 ปี แต่น้อยกว่า หรือเท่ากับ 3 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > 365) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*3)) ";
                    break;

                case 4:
                    // คงเหลือมากกว่า 3 ปี แต่น้อยกว่า หรือเท่ากับ 5 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*3)) " .
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*5)) ";
                    break;

                case 5:
                    // คงเหลือมากกว่า 5 ปีขึ้นไป
                    $where2  .= "  (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*5)) ";
                    break;

                default:
                   break;
                
            } // switch

        } // if age


        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SETTLE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= "     P.SETTLE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
        // Log::info(__FUNCTION__ . ' ::#3 WHERE#2 ====>' . $where2 . ' date_start=' . $date_start);
        
       
        if(!$IsCase) {
            $where2 = "";
        }

        if(strlen($where2) > 1) {
            $where2 = " AND " . $where2  ; 
        }
     
        // $query = "SELECT COUNT(*) as total FROM TBL_P2_BOND_TRANS P " . $where2 ;
        $query = " SELECT COUNT(P.SYMBOL) as total " . 
                 "     FROM TBL_P2_BOND_TRANS P, TBL_P2_BOND_INDEX I, TBL_P2_BOND_CATEGORY C " .
                 " WHERE P.SYMBOL = I.SYMBOL AND I.CATE_ID = C.CATE_ID ".
                 "   ".
                 $where2;

        $all = DB::select(DB::raw($query));
        Log::info(__FUNCTION__ . ' :: SQL=' . $query);

        Log::info(__FUNCTION__  .' :: TOAL=' . $all[0]->total );
        return  $all[0]->total;
    }

    public function DataSource($ArrParam, $IsCase, $ispageing= true) {
        Log::info(print_r($ArrParam, true));
         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');
/*
 var name_sht    = $('#securities_name').val();   // ชื่อบริษัท จัดการ "UOBAM"
            var symbol      = $('#symbol').val();            // ชื่อตราสาร symbol eg. "APCS"
            var bondrange   = $('#bondrange').val();         // ช่วงกำไร ขาดทุน
            var broker      = $('#broker').val();            // ชื่อบริษัท Broker
            var age         = $('#age').val();               // อายุตราสาร
            var industrial  = $('#industrial').val();        // ประเภทตราสาร CATE_ID --> 21 

*/
        
        $where = "";
        if($ispageing){
            $PageSize = $ArrParam['pagesize'];
            $PageNumber = $ArrParam['PageNumber'];
        }

        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht   = $ArrParam["name_sht"];      // eg. UOBAM, KTAM 
        $age      = $ArrParam["age"];       // age 1-5 ปี
        
        // NEED TO SPLIT BU + INDUSTRIAL 
        $industrial = $ArrParam["industrial"];  
        $cateid     = $ArrParam["industrial"];  

        $bu = '';
        $indus = '';
        
        if(strlen($industrial) > 0) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
                           ->orderby("BOND_CATE_THA")
                           ->where('CATE_ID','=', $industrial)->get();
                           
            foreach ($categorylist as $row) {
                $indus = $row->BOND_CATE_THA;
                $bu    = $row->BOND_TYPE_THA;
                break;  
            }
        }
        
        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        
        $where2 = "" ;//
        if(strlen(trim($symbol)) > 0) { // && $check_name== "true"){
            $where2 .= " P.SYMBOL = '". $symbol ."' ";
            //$where_inner .= " x.SYMBOL = '" . $symbol .  "' "; 
        }

        if(strlen($name_sht) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " P.SECURITIES_NAME = '".$name_sht."' ";
        }

        if(strlen(trim($cateid)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND C.CATE_ID = '".$cateid."' ";
            else
               $where2 .= " C.CATE_ID = '".$cateid."' ";
        }
     
       Log::info('1) #### AGE ##### =>[' . $age . "] LEN=" . strlen($age));
        if(strlen(trim($age)) > 0) {  
            $prefix = "";
            if(strlen($where2) > 1) {
                 $prefix = " AND ";
            }
            $where2 .= $prefix;
            Log::info('2) #### AGE ##### = ' . $age);
            $agefield = " DATEDIFF(MONTH, GETDATE(), P.MAT) as AGE_MONTH ";
            switch(intval(trim($age))) {
                 case 0;
                    // คงเหลือน้อยกว่าหรือเท่ากับ 3 เดือน (MAT เทียบกับ today >= 1 day <= 3 month)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/4)) ";
                   break;

                case 1:
                    // > 3 เดือน <= 6 เดือน
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= (365/4)) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/2)) ";
                    
                   break;
/*
                case 2:
                    // คงเหลือน้อยกว่าหรือเท่ากับ 1 ปี (MAT เทียบกับ today >= 1 day <= 1 year)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;  
*/
                case 2:
                    //  คงเหลือมากกว่า 6 เดือน แต่น้อยกว่า หรือเท่ากับ 1 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365/2)) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;

                case 3:
                    // คงเหลือมากกว่า 1 ปี แต่น้อยกว่า หรือเท่ากับ 3 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > 365) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*3)) ";
                    break;

                case 4:
                    // คงเหลือมากกว่า 3 ปี แต่น้อยกว่า หรือเท่ากับ 5 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*3)) " .
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*5)) ";
                    break;

                case 5:
                    // คงเหลือมากกว่า 5 ปีขึ้นไป
                    $where2  .= "  (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*5)) ";
                    break;

                default:
                   break;
                
                               
            } // switch

        } // if age
    
        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SETTLE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " P.SETTLE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       
        if(!$IsCase) {
            $where2 = "";
        }

        if(strlen($where2) > 1) {
            $where2 = " AND " . $where2  ; 
        }
       
        $query = " SELECT *, YEAR(P.SETTLE_DATE) as YYYY, " . 
                 "    MONTH(P.SETTLE_DATE) as MM, " . 
                 "    DAY(P.SETTLE_DATE) as DD " . 
                 " FROM TBL_P2_BOND_TRANS P, TBL_P2_BOND_INDEX I, TBL_P2_BOND_CATEGORY C " .
                 " WHERE P.SYMBOL = I.SYMBOL AND I.CATE_ID = C.CATE_ID ".
                 $where2 . " ORDER BY TRANS_DATE ASC ";
           
        if($ispageing){
            $query .= " OFFSET " . $PageSize . " * (" . $PageNumber . " - 1) ROWS FETCH NEXT " . $PageSize . " ROWS ONLY OPTION (RECOMPILE) ";
        }


        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }

    public  function  DataSourceExport($ArrParam, $IsCase, $ispageing= true) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        
        $where = "";
       /* if($ispageing){
            $PageSize = $ArrParam['pagesize'];
            $PageNumber = $ArrParam['PageNumber'];
        }*/

        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        $age      = $ArrParam["age"];

        // NEED TO SPLIT BU + INDUSTRIAL 
        $industrial = $ArrParam["industrial"];  
        $cateid     = $ArrParam["industrial"];  

        $bu = '';
        $indus = '';

        if(strlen($cateid) > 0) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
                           ->orderby("BOND_CATE_THA")
                           ->where('CATE_ID','=', $cateid)->get();
            foreach ($categorylist as $row) {
                $indus = $row->BOND_CATE_THA;
                $bu    = $row->BOND_TYPE_THA;
                break;  
            }
        }


        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }

        
     /*   $where = "" ;//
        //$where_inner = " WHERE ";
               
        if(strlen(trim($symbol)) > 0) { // && $check_name== "true"){
            $where .= " P.SYMBOL = '".$symbol."' ";
        }

        if(strlen(trim($name_sht)) > 0) {  
            if(strlen($where) > 1)
                $where .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
                $where .= " P.SECURITIES_NAME = '".$name_sht."' ";  
        }

        

        if(strlen($where) > 1) {
            $where = " AND " . $where  ; 
        }
*/

       // WHERE2
        $where2 = "";
        if(strlen(trim($symbol)) > 0) {  
            $where2 .= " P.SYMBOL = '" . $symbol . "' ";
        }

        if(strlen(trim($name_sht)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '" . $name_sht . "' ";
            else
               $where2 .= " P.SECURITIES_NAME = '" . $name_sht . "' ";
        }

        if(strlen(trim($cateid)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND C.CATE_ID = '".$cateid."' ";
            else
               $where2 .= " C.CATE_ID = '".$cateid."' ";
        }
      

        Log::info('1) #### AGE ##### =>[' . $age . "] LEN=" . strlen($age));
        if(strlen(trim($age)) > 0) {  
            $prefix = "";
            if(strlen($where2) > 1) {
                 $prefix = " AND ";
            }

            $where2 .= $prefix;
            Log::info('2) #### AGE ##### = ' . $age);
            $agefield = " DATEDIFF(MONTH, GETDATE(), P.MAT) as AGE_MONTH ";
            switch(intval(trim($age))) {
                
               case 0;
                    // คงเหลือน้อยกว่าหรือเท่ากับ 3 เดือน (MAT เทียบกับ today >= 1 day <= 3 month)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/4)) ";
                   break;

                case 1:
                    // > 3 เดือน <= 6 เดือน
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= (365/4)) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/2)) ";
                    
                   break;
/*
                case 2:
                    // คงเหลือน้อยกว่าหรือเท่ากับ 1 ปี (MAT เทียบกับ today >= 1 day <= 1 year)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;  
*/
                case 2:
                    //  คงเหลือมากกว่า 6 เดือน แต่น้อยกว่า หรือเท่ากับ 1 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365/2)) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;

                case 3:
                    // คงเหลือมากกว่า 1 ปี แต่น้อยกว่า หรือเท่ากับ 3 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > 365) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*3)) ";
                    break;

                case 4:
                    // คงเหลือมากกว่า 3 ปี แต่น้อยกว่า หรือเท่ากับ 5 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*3)) " .
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*5)) ";
                    break;

                case 5:
                    // คงเหลือมากกว่า 5 ปีขึ้นไป
                    $where2  .= "  (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*5)) ";
                    break;

                default:
                   break;
                
            } // switch
        } // if age
    

        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND P.TRANS_DATE  BETWEEN '" . $date_start . "' AND '" . $date_end . "' ";
            else
               $where2 .= "     P.TRANS_DATE  BETWEEN '" . $date_start . "' AND '" . $date_end . "' "; 
        }
       
        if(strlen($where2) > 1) {
            $where2 = " AND " . $where2  ; 
        }

        
  
       // $query = " SELECT *, YEAR(SETTLE_DATE) as YYYY, MONTH(SETTLE_DATE) as MM, DAY(SETTLE_DATE) as DD FROM TBL_P2_BOND_TRANS " . $where2 .
       //          " ORDER BY TRANS_DATE ASC ";
        
         $query = " SELECT *, YEAR(P.SETTLE_DATE) as YYYY, " . 
                 "    MONTH(P.SETTLE_DATE) as MM, " . 
                 "    DAY(P.SETTLE_DATE) as DD " . 
                 " FROM TBL_P2_BOND_TRANS P, TBL_P2_BOND_INDEX I, TBL_P2_BOND_CATEGORY C " .
                 " WHERE P.SYMBOL = I.SYMBOL AND I.CATE_ID = C.CATE_ID ".
                 $where2 . " ORDER BY TRANS_DATE ASC ";

        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }


public  function  DataSourceGrandTotal($ArrParam) {
        Log::info(print_r($ArrParam, true));

         /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        
        $where = "";
        
        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        $age      = $ArrParam["age"];

        // NEED TO SPLIT BU + INDUSTRIAL 
        $industrial = $ArrParam["industrial"];  // CATE_ID
        $cateid      = $ArrParam["industrial"]; 

        $bu = '';
        $indus = '';

        if(strlen($cateid) > 0) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
                           ->orderby("BOND_CATE_THA")
                           ->where('CATE_ID','=', $cateid)->get();
            foreach ($categorylist as $row) {
                $indus = $row->BOND_CATE_THA;
                $bu    = $row->BOND_TYPE_THA;
                break;  
            }
        }

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }


        $where2 = "";
        if(strlen(trim($symbol)) > 0) { 
            $where2 .= " P.SYMBOL = '".$symbol."' ";
        }

        if(strlen(trim($name_sht)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " P.SECURITIES_NAME = '".$name_sht."' ";
        }

        if(strlen(trim($cateid)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND C.CATE_ID = '".$cateid."' ";
            else
               $where2 .= " C.CATE_ID = '".$cateid."' ";
        }

        Log::info('1) #### AGE ##### =>[' . $age . "] LEN=" . strlen($age));
        if(strlen(trim($age)) > 0) {  
            $prefix = "";
            if(strlen($where2) > 1) {
                 $prefix = " AND ";
            }

            $where2 .= $prefix;
            Log::info('2) #### AGE ##### = ' . $age);
            $agefield = " DATEDIFF(MONTH, GETDATE(), P.MAT) as AGE_MONTH ";
            switch(intval(trim($age))) {
                
                case 0;
                    // คงเหลือน้อยกว่าหรือเท่ากับ 3 เดือน (MAT เทียบกับ today >= 1 day <= 3 month)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/4)) ";
                   break;

                case 1:
                    // > 3 เดือน <= 6 เดือน
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= (365/4)) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/2)) ";
                    
                   break;
/*
                case 2:
                    // คงเหลือน้อยกว่าหรือเท่ากับ 1 ปี (MAT เทียบกับ today >= 1 day <= 1 year)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;  
*/
                case 2:
                    //  คงเหลือมากกว่า 6 เดือน แต่น้อยกว่า หรือเท่ากับ 1 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365/2)) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;

                case 3:
                    // คงเหลือมากกว่า 1 ปี แต่น้อยกว่า หรือเท่ากับ 3 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > 365) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*3)) ";
                    break;

                case 4:
                    // คงเหลือมากกว่า 3 ปี แต่น้อยกว่า หรือเท่ากับ 5 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*3)) " .
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*5)) ";
                    break;

                case 5:
                    // คงเหลือมากกว่า 5 ปีขึ้นไป
                    $where2  .= "  (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*5)) ";
                    break;

                default:
                   break;
                
            } // switch
        } // if age
    

        
        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SETTLE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " P.SETTLE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       

        if(strlen($where2) > 1) {
            $where2 = " AND " . $where2  ; 
        }

        $query =  " SELECT " .
                  "    SUM(FACE_VALUE) as SUM_FACE_VALUE, " .
                  "    SUM(YIELD_PERCENTAGE) as SUM_YIELD_PERCENTAGE, " . 
                  "    SUM(ACCRUED_PUR)      as SUM_ACCRUED_PUR,      " .
                  "    SUM(CLEAN_VAL_PUR)    as SUM_CLEAN_VAL_PUR,      " .
                  "    SUM(TOTAL_VAL_PUR)    as SUM_TOTAL_VAL_PUR,      " .
                  "    SUM(CLEAN_VAL_SALE)   as SUM_CLEAN_VAL_SALE,   " .
                  "    SUM(ACCRUED_SALE)     as SUM_ACCRUED_SALE,     " .
                  "    SUM(TOTAL_VAL_SALE)   as SUM_TOTAL_VAL_SALE,   " .
                  "    SUM(TOTAL_AMORT_SALE) as SUM_TOTAL_AMORT_SALE, " .
                  "    SUM(PROFIT_LOSS_SALE) as SUM_PROFIT_LOSS_SALE, " .
                  "    SUM(CLEAN_VAL_MAT)    as SUM_CLEAN_VAL_MAT, " .
                  "    SUM(ACCRUED_MAT)      as SUM_ACCRUED_MAT," .
                  "    SUM(TOTAL_VAL_MAT)    as SUM_TOTAL_VAL_MAT, " .
                  "    SUM(TOTAL_AMORT_MAT)  as SUM_TOTAL_AMORT_MAT, " .
                  "    SUM(PROFIT_LOSS_MAT)  as SUM_PROFIT_LOSS_MAT, " .
                  "    SUM(CLEAN_VAL_INT)    as SUM_CLEAN_VAL_INT, " .
                  "    SUM(ACCRUED_INT)      as SUM_ACCRUED_INT, " .
                  "    SUM(TOTAL_VAL_INT)    as SUM_TOTAL_VAL_INT, " .
                  "    SUM(TOTAL_AMORT_INT)  as SUM_TOTAL_AMORT_INT, " .
                  "    SUM(PROFIT_LOSS_INT)  as SUM_PROFIT_LOSS_INT, " .
                  "    SUM(amort_cost)       as SUM_AMORT_COST " .
                  " FROM TBL_P2_BOND_TRANS P, TBL_P2_BOND_INDEX I, TBL_P2_BOND_CATEGORY C " .
                  " WHERE P.SYMBOL = I.SYMBOL AND I.CATE_ID = C.CATE_ID ".
                  $where2 ;
                 
        // $query .= " OFFSET " . $PageSize . " * (" . $PageNumber . " - 1) ROWS FETCH NEXT " . $PageSize . " ROWS ONLY OPTION (RECOMPILE) ";
            
        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }
/////////////////////////////////////
public function DataSourceBarChart($ArrParam)  {
      /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        
        $where = "";
        
        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        $age      = $ArrParam["age"];      // อายุตราสาร

        // NEED TO SPLIT BU + INDUSTRIAL 
        $industrial = $ArrParam["industrial"];  // CATE_ID
        $cateid    = $ArrParam["industrial"]; 

        $bu = '';
        $indus = '';

        if(strlen($cateid) > 0) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
                           ->orderby("BOND_CATE_THA")
                           ->where('CATE_ID','=', $cateid)->get();
            foreach ($categorylist as $row) {
                $indus = $row->BOND_CATE_THA;
                $bu    = $row->BOND_TYPE_THA;
                break;  
            }
        }

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }


        $where2 = "";
        if(strlen(trim($symbol)) > 0) { 
            $where2 .= " P.SYMBOL = '".$symbol."' ";
        }

        if(strlen(trim($name_sht)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " P.SECURITIES_NAME = '".$name_sht."' ";
        }

        if(strlen(trim($cateid)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND C.CATE_ID = '".$cateid."' ";
            else
               $where2 .= " C.CATE_ID = '".$cateid."' ";
        }

        Log::info('1) #### AGE ##### =>[' . $age . "] LEN=" . strlen($age));
        if(strlen(trim($age)) > 0) {  
            $prefix = "";
            if(strlen($where2) > 1) {
                 $prefix = " AND ";
            }

            $where2 .= $prefix;
            Log::info('2) #### AGE ##### = ' . $age);
            $agefield = " DATEDIFF(MONTH, GETDATE(), P.MAT) as AGE_MONTH ";
            switch(intval(trim($age))) {
                case 0;
                    // คงเหลือน้อยกว่าหรือเท่ากับ 3 เดือน (MAT เทียบกับ today >= 1 day <= 3 month)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/4)) ";
                   break;

                case 1:
                    // > 3 เดือน <= 6 เดือน
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= (365/4)) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/2)) ";
                    
                   break;
/*
                case 2:
                    // คงเหลือน้อยกว่าหรือเท่ากับ 1 ปี (MAT เทียบกับ today >= 1 day <= 1 year)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;  
*/
                case 2:
                    //  คงเหลือมากกว่า 6 เดือน แต่น้อยกว่า หรือเท่ากับ 1 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365/2)) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;

                case 3:
                    // คงเหลือมากกว่า 1 ปี แต่น้อยกว่า หรือเท่ากับ 3 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > 365) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*3)) ";
                    break;

                case 4:
                    // คงเหลือมากกว่า 3 ปี แต่น้อยกว่า หรือเท่ากับ 5 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*3)) " .
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*5)) ";
                    break;

                case 5:
                    // คงเหลือมากกว่า 5 ปีขึ้นไป
                    $where2  .= "  (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*5)) ";
                    break;

                default:
                   break;

            } // switch
        } // if age
    
        
        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SETTLE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " P.SETTLE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       

        if(strlen($where2) > 1) {
            $where2 = " AND " . $where2  ; 
        }

        $query =  " SELECT " .
                  "    C.CATE_ID       as CATE_ID, " .
                  "    C.BOND_CATE_THA as BOND_CATE_THA, " .
                  "    C.BOND_CATE_THA as BOND_TYPE_THA, " .
                  "    SUM(FACE_VALUE) as SUM_FACE_VALUE, " .
                  "    SUM(YIELD_PERCENTAGE) as SUM_YIELD_PERCENTAGE, " . 
                  "    SUM(ACCRUED_PUR)      as SUM_ACCRUED_PUR,      " .
                  "    SUM(CLEAN_VAL_PUR)    as SUM_CLEAN_VAL_PUR,      " .
                  "    SUM(TOTAL_VAL_PUR)    as SUM_TOTAL_VAL_PUR,      " . // <-- CHART
                  "    SUM(CLEAN_VAL_SALE)   as SUM_CLEAN_VAL_SALE,   " .
                  "    SUM(ACCRUED_SALE)     as SUM_ACCRUED_SALE,     " .
                  "    SUM(TOTAL_VAL_SALE)   as SUM_TOTAL_VAL_SALE,   " .   //<-- CHART
                  "    SUM(TOTAL_AMORT_SALE) as SUM_TOTAL_AMORT_SALE, " .
                  "    SUM(PROFIT_LOSS_SALE) as SUM_PROFIT_LOSS_SALE, " .   //<-- CHART
                  "    SUM(CLEAN_VAL_MAT)    as SUM_CLEAN_VAL_MAT, " .
                  "    SUM(ACCRUED_MAT)      as SUM_ACCRUED_MAT," .
                  "    SUM(TOTAL_VAL_MAT)    as SUM_TOTAL_VAL_MAT, " .
                  "    SUM(TOTAL_AMORT_MAT)  as SUM_TOTAL_AMORT_MAT, " .
                  "    SUM(PROFIT_LOSS_MAT)  as SUM_PROFIT_LOSS_MAT, " .
                  "    SUM(CLEAN_VAL_INT)    as SUM_CLEAN_VAL_INT, " .
                  "    SUM(ACCRUED_INT)      as SUM_ACCRUED_INT, " .
                  "    SUM(TOTAL_VAL_INT)    as SUM_TOTAL_VAL_INT, " .
                  "    SUM(TOTAL_AMORT_INT)  as SUM_TOTAL_AMORT_INT, " .
                  "    SUM(PROFIT_LOSS_INT)  as SUM_PROFIT_LOSS_INT, " .
                  "    SUM(amort_cost)       as SUM_AMORT_COST " .
                  " FROM TBL_P2_BOND_TRANS P, TBL_P2_BOND_INDEX I, TBL_P2_BOND_CATEGORY C " .
                  " WHERE P.SYMBOL = I.SYMBOL AND I.CATE_ID = C.CATE_ID ".
                  $where2 . 
                   
                  " GROUP BY C.CATE_ID,       " .
                  "          C.BOND_CATE_THA, " .
                  "          C.BOND_TYPE_THA  " ;
                 
        // $query .= " OFFSET " . $PageSize . " * (" . $PageNumber . " - 1) ROWS FETCH NEXT " . $PageSize . " ROWS ONLY OPTION (RECOMPILE) ";
            
        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
}


/////////////////////////////////////
public function DataSourceBarChartByAge($ArrParam)  {
      /* limit execution timeout */
        ini_set('max_execution_time', 30000);
        /* unlimit memory size */
        ini_set('memory_limit', '-1');

        
        $where = "";
        
        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        $age      = $ArrParam["age"];      // อายุตราสาร

        // NEED TO SPLIT BU + INDUSTRIAL 
        $industrial = $ArrParam["industrial"];  // CATE_ID
        $cateid    = $ArrParam["industrial"]; 

        $bu = '';
        $indus = '';

        if(strlen($cateid) > 0) {
            $categorylist = DB::table('TBL_P2_BOND_CATEGORY')
                           ->orderby("BOND_CATE_THA")
                           ->where('CATE_ID','=', $cateid)->get();
            foreach ($categorylist as $row) {
                $indus = $row->BOND_CATE_THA;
                $bu    = $row->BOND_TYPE_THA;
                break;  
            }
        }

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }


        $where2 = "";
        if(strlen(trim($symbol)) > 0) { 
            $where2 .= " P.SYMBOL = '".$symbol."' ";
        }

        if(strlen(trim($name_sht)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " P.SECURITIES_NAME = '".$name_sht."' ";
        }

        if(strlen(trim($cateid)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND C.CATE_ID = '".$cateid."' ";
            else
               $where2 .= " C.CATE_ID = '".$cateid."' ";
        }

        Log::info('1) #### AGE ##### =>[' . $age . "] LEN=" . strlen($age));
        if(strlen(trim($age)) > 0) {  
            $prefix = "";
            if(strlen($where2) > 1) {
                 $prefix = " AND ";
            }

            $where2 .= $prefix;
            Log::info('2) #### AGE ##### = ' . $age);
            $agefield = " DATEDIFF(MONTH, GETDATE(), P.MAT) as AGE_MONTH ";
            switch(intval(trim($age))) {
                case 0;
                    // คงเหลือน้อยกว่าหรือเท่ากับ 3 เดือน (MAT เทียบกับ today >= 1 day <= 3 month)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/4)) ";
                   break;

                case 1:
                    // > 3 เดือน <= 6 เดือน
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= (365/4)) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365/2)) ";
                    
                   break;
/*
                case 2:
                    // คงเหลือน้อยกว่าหรือเท่ากับ 1 ปี (MAT เทียบกับ today >= 1 day <= 1 year)
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) >= 0) " .
                               " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;  
*/
                case 2:
                    //  คงเหลือมากกว่า 6 เดือน แต่น้อยกว่า หรือเท่ากับ 1 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365/2)) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= 365) ";
                    break;

                case 3:
                    // คงเหลือมากกว่า 1 ปี แต่น้อยกว่า หรือเท่ากับ 3 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > 365) " . 
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*3)) ";
                    break;

                case 4:
                    // คงเหลือมากกว่า 3 ปี แต่น้อยกว่า หรือเท่ากับ 5 ปี
                    $where2  .= "     (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*3)) " .
                                " AND (DATEDIFF(DAY, GETDATE(), P.MAT ) <= (365*5)) ";
                    break;

                case 5:
                    // คงเหลือมากกว่า 5 ปีขึ้นไป
                    $where2  .= "  (DATEDIFF(DAY, GETDATE(), P.MAT ) > (365*5)) ";
                    break;

                default:
                   break;
                   
            } // switch
        } // if age
    
        
        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND P.SETTLE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " P.SETTLE_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       

        if(strlen($where2) > 1) {
            $where2 = " AND " . $where2  ; 
        }
        $sqlstr= " SELECT YMD,   " . 
                  "    SUM(TOTAL_VAL_PUR)    as SUM_TOTAL_VAL_PUR,  " .      
                  "    SUM(TOTAL_VAL_SALE)   as SUM_TOTAL_VAL_SALE, " .
                  "    SUM(PROFIT_LOSS_INT)  as SUM_PROFIT_LOSS_INT ".    
                  " FROM ( " . 
                  "    SELECT P.*,  C.*, " .        
                  "       YMD = ( " .
                 "          CASE " . 
                 "             WHEN  ((DATEDIFF(DAY, GETDATE(), P.MAT  ) >= 0)       AND (DATEDIFF(DAY, GETDATE(), P.MAT  ) <= (365/4))) THEN 0 " .  
                 "             WHEN  ((DATEDIFF(DAY, GETDATE(), P.MAT  ) > (365/4))  AND (DATEDIFF(DAY, GETDATE(), P.MAT  ) <= (365/2))) THEN 1 " .
         //        "             WHEN  ((DATEDIFF(DAY, GETDATE(), P.MAT  ) >= 0)       AND (DATEDIFF(DAY, GETDATE(), P.MAT  ) <= (365)))   THEN 2 " .
                  "             WHEN  ((DATEDIFF(DAY, GETDATE(), P.MAT  ) > (365/2))  AND (DATEDIFF(DAY, GETDATE(), P.MAT  ) <= (365)))   THEN 2 " .
                  "             WHEN  ((DATEDIFF(DAY, GETDATE(), P.MAT  ) > (365))    AND (DATEDIFF(DAY, GETDATE(), P.MAT  ) <= (365*3))) THEN 3 " .
                  "             WHEN  ((DATEDIFF(DAY, GETDATE(), P.MAT  ) > (365*3))  AND (DATEDIFF(DAY, GETDATE(), P.MAT  ) <= (365*5))) THEN 4 " .
                  "             WHEN   (DATEDIFF(DAY, GETDATE(), P.MAT  ) > (365*5))                                                      THEN 5 " .
                  "             ELSE 5" .  
                  "          END ".
                  "          )  " .  
                  "    FROM  TBL_P2_BOND_TRANS P,   " .           
                  "          TBL_P2_BOND_INDEX I,   " .               
                  "          TBL_P2_BOND_CATEGORY C " .          
                  "    WHERE I.SYMBOL = P.SYMBOL    " .              
                  "          AND I.CATE_ID = C.CATE_ID  ".  $where2 . 
                  " ) A " .
                  " GROUP BY YMD  ORDER BY YMD ASC   ";  
        Log::info(__FUNCTION__ . ' :: ' . $sqlstr);
        return DB::select(DB::raw($sqlstr));
}

public function GetSecurityName($name_sht) {
       $query = "SELECT * FROM TBL_P2_BOND_SECURITIES WHERE NAME_SHT = '" . $name_sht . "'";
       $caption = '';
       $data = DB::select(DB::raw($query));
       foreach($data as $index =>$field) {
           $caption = $field->SECURITIES_NAME;
           break;
       }
       return $caption;
}

/////////////////////////////////////
public  function DataSourceBarChart_XXXXXX($ArrParam) {
        Log::info(print_r($ArrParam, true));

         
        ini_set('max_execution_time', 30000);
        
        ini_set('memory_limit', '-1');

        
        $where = "";
        
        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        
        // NEED TO SPLIT BU + INDUSTRIAL 
        $industrial = $ArrParam["industrial"];  
        $cateid     = $ArrParam["industrial"]; 
        $bu = '';
        $indus = '';

        if(strlen(trim($industrial)) > 0) {
            $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')
                           ->orderby("INDUSTRIAL")
                           ->where('CATE_ID','=', $industrial)->get();
            foreach ($categorylist as $row) {
                $indus = $row->INDUSTRIAL;
                $bu    = $row->BU;
                break;  
            }
        }

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }
        
        $where = "" ;//
               
        if(strlen(trim($symbol)) > 0) {  
            $where .= " x.SYMBOL = '".$symbol."' ";
        }

        if(strlen(trim($name_sht)) > 0) {  
            if(strlen($where) > 1)
                $where .= " AND x.SECURITIES_NAME = '".$name_sht."' ";
            else
                $where .= " x.SECURITIES_NAME = '".$name_sht."' ";  
        }

        if(strlen(trim($indus)) > 0) {  
            if(strlen($where) > 1)
               $where .= " AND k.INDUSTRIAL  = '".$indus."' ";
            else
               $where .= " k.INDUSTRIAL  = '".$indus."' "; 
        }

        if((!empty($bu)) && (strlen($bu) > 0)) {  
            if(strlen($where) > 1)
                $where .= " AND k.BU  = '".$bu."' ";
            else
                $where .= " k.BU  = '".$bu."' ";  
        }
          
        if(strlen($where) > 1) {
            $where = " AND " . $where  ; 
        }

       // WHERE2
        $where2 = "" ;//
        if(strlen(trim($symbol)) > 0) { 
            $where2 .= " SYMBOL = '".$symbol."' ";
      
        }

        if(strlen(trim($name_sht)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " SECURITIES_NAME = '".$name_sht."' ";
        }

        if(strlen(trim($indus)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND INDUSTRIAL  = '".$indus."' ";
            else
               $where2 .= " INDUSTRIAL  = '".$indus."' "; 
        }

        if(strlen(trim($bu)) > 0) {  
            if(strlen($where2) > 1) 
                $where2 .= " AND BU  = '".$bu."' ";
            else
                $where2 .= " BU  = '".$bu."' ";  
        }
        
        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       

        if(strlen($where2) > 1) {
            $where2 = " WHERE " . $where2  ; 
        }

        
        $query =  " ;WITH cte AS ( " .
                  "     SELECT " . 
                  "         row_number() OVER (ORDER BY TRANS_DATE) AS rownum, x.*, " . 
                  "         k.INDUSTRIAL, k.BU " . 
                  "     FROM " . 
                  "         TBL_P2_EQUITY_TRANS x, TBL_P2_EQUITY_INDEX  k " . 
                  "     WHERE x.SYMBOL = k.SYMBOL " . 
                    $where .
                  " ) " .
                  " SELECT  INDUSTRIAL," .
                  "         SUM(TOTAL_PURCHASE) AS J,  " .
                  "         SUM(TOTAL_SALE) AS N,      " .
                  "         SUM(PROFIT_LOSS) AS O      " .
                  " FROM ( " .
                  "     SELECT ".
                  "         a.TRANS_DATE, a.SYMBOL, a.TYPE, a.SECURITIES_NAME, a.INDUSTRIAL, a.BU, ".
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE,a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, ". 
                  "         SUM(b.UNIT_PURCHASE) AS P_SUM, " .
                  "         SUM(b.UNIT_SALE) AS S_SUM, " .
                  "         SUM(b.UNIT_DIV) AS D_SUM, " .
                  "         SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as REMAIN_UNIT " .    
                  "     FROM cte a " . 
                  "     LEFT JOIN cte b " . 
                  "     ON a.SYMBOL = b.SYMBOL AND b.rownum <= a.rownum " .
                  "     GROUP BY " . 
                  "         a.SYMBOL, a.TYPE, a.TRANS_DATE, a.rownum, " .
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE, a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, a.SECURITIES_NAME, " .
                  "         a.INDUSTRIAL, a.BU " .
                  " ) T " .
                  $where2 .
                  ' GROUP BY INDUSTRIAL ';
                  
        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }

/*
public  function DataSourceBarChart_Original($ArrParam) {
        Log::info(print_r($ArrParam, true));

         
        ini_set('max_execution_time', 30000);
         
        ini_set('memory_limit', '-1');

        
        $where = "";
        
        $symbol   = $ArrParam["symbol"] ;  //  APCS, CPALL
        $name_sht = $ArrParam["name_sht"]; // eg. UOBAM, KTAM
        
        // NEED TO SPLIT BU + INDUSTRIAL 
        $industrial = $ArrParam["industrial"];  
        $cateid     = $ArrParam["industrial"]; 
        $bu = '';
        $indus = '';

        if(strlen(trim($industrial)) > 0) {
            $categorylist = DB::table('TBL_P2_EQUITY_CATEGORY')
                           ->orderby("INDUSTRIAL")
                           ->where('CATE_ID','=', $industrial)->get();
            foreach ($categorylist as $row) {
                $indus = $row->INDUSTRIAL;
                $bu    = $row->BU;
                break;  
            }
        }

        $date_start = $ArrParam["date_start"];
        $date_end = $ArrParam["date_end"];

        // if user not specified datetime, current datetime will use. 
        if(strlen($date_start) < 2) {
           $date_start = date('Y-m-d') . ' 00:00:01';
        }

        if(strlen($date_end) < 2) {
            $date_end = date('Y-m-d') . ' 23:59:59';
        }
        
        $where = "" ;//
               
        if(strlen(trim($symbol)) > 0) {  
            $where .= " x.SYMBOL = '".$symbol."' ";
        }

        if(strlen(trim($name_sht)) > 0) {  
            if(strlen($where) > 1)
                $where .= " AND x.SECURITIES_NAME = '".$name_sht."' ";
            else
                $where .= " x.SECURITIES_NAME = '".$name_sht."' ";  
        }

        if(strlen(trim($indus)) > 0) {  
            if(strlen($where) > 1)
               $where .= " AND k.INDUSTRIAL  = '".$indus."' ";
            else
               $where .= " k.INDUSTRIAL  = '".$indus."' "; 
        }

        if((!empty($bu)) && (strlen($bu) > 0)) {  
            if(strlen($where) > 1)
                $where .= " AND k.BU  = '".$bu."' ";
            else
                $where .= " k.BU  = '".$bu."' ";  
        }
          
        if(strlen($where) > 1) {
            $where = " AND " . $where  ; 
        }

       // WHERE2
        $where2 = "" ;//
        if(strlen(trim($symbol)) > 0) { 
            $where2 .= " SYMBOL = '".$symbol."' ";
      
        }

        if(strlen(trim($name_sht)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND SECURITIES_NAME = '".$name_sht."' ";
            else
               $where2 .= " SECURITIES_NAME = '".$name_sht."' ";
        }

        if(strlen(trim($indus)) > 0) {  
            if(strlen($where2) > 1) 
               $where2 .= " AND INDUSTRIAL  = '".$indus."' ";
            else
               $where2 .= " INDUSTRIAL  = '".$indus."' "; 
        }

        if(strlen(trim($bu)) > 0) {  
            if(strlen($where2) > 1) 
                $where2 .= " AND BU  = '".$bu."' ";
            else
                $where2 .= " BU  = '".$bu."' ";  
        }
        
        if((!empty($date_start)) && (!empty($date_end))) { 
            if(strlen($where2) > 1) 
               $where2 .= " AND TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' ";
            else
               $where2 .= " TRANS_DATE  BETWEEN '". $date_start."' AND '".$date_end."' "; 
        }
       

        if(strlen($where2) > 1) {
            $where2 = " WHERE " . $where2  ; 
        }

        
        $query =  " ;WITH cte AS ( " .
                  "     SELECT " . 
                  "         row_number() OVER (ORDER BY TRANS_DATE) AS rownum, x.*, " . 
                  "         k.INDUSTRIAL, k.BU " . 
                  "     FROM " . 
                  "         TBL_P2_EQUITY_TRANS x, TBL_P2_EQUITY_INDEX  k " . 
                  "     WHERE x.SYMBOL = k.SYMBOL " . 
                    $where .
                  " ) " .
                  " SELECT  INDUSTRIAL," .
                  "         SUM(TOTAL_PURCHASE) AS J,  " .
                  "         SUM(TOTAL_SALE) AS N,      " .
                  "         SUM(PROFIT_LOSS) AS O      " .
                  " FROM ( " .
                  "     SELECT ".
                  "         a.TRANS_DATE, a.SYMBOL, a.TYPE, a.SECURITIES_NAME, a.INDUSTRIAL, a.BU, ".
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE,a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, ". 
                  "         SUM(b.UNIT_PURCHASE) AS P_SUM, " .
                  "         SUM(b.UNIT_SALE) AS S_SUM, " .
                  "         SUM(b.UNIT_DIV) AS D_SUM, " .
                  "         SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as REMAIN_UNIT " .    
                  "     FROM cte a " . 
                  "     LEFT JOIN cte b " . 
                  "     ON a.SYMBOL = b.SYMBOL AND b.rownum <= a.rownum " .
                  "     GROUP BY " . 
                  "         a.SYMBOL, a.TYPE, a.TRANS_DATE, a.rownum, " .
                  "         a.UNIT_PURCHASE, a.PRICE_PURCHASE, a.TOTAL_PURCHASE, " .
                  "         a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS, " .
                  "         a.UNIT_DIV, a.TOTAL_DIV, a.SECURITIES_NAME, " .
                  "         a.INDUSTRIAL, a.BU " .
                  " ) T " .
                  $where2 .
                  ' GROUP BY INDUSTRIAL ';
                  
        Log::info(__FUNCTION__ . ' :: ' . $query);
        return DB::select(DB::raw($query));
    }
    */
    public function ajax_report_search(Request $request)
    {
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $data = getmemulist();
        $title = getMenuName($data,63, 2);

       // $ar = $this->retrive_customize_field(62,1);

        $view_name = 'backend.pages.ajax.ajax_p2_bond_trading_report';
        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $symbol = $request->input('symbol');
        $name_sht = $request->input('name_sht');
       // $broker = $request->input('broker');
        $age = $request->input('age');
        $industrial = $request->input('industrial');
       // $bondrange = $request->input('bondrange');

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $ArrParam = array();

        //{ pagesize: "10", PageNumber: 1, symbol: "", name_sht: "", industrial: "", date_start: "", date_end: "" }
        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;

        $ArrParam["symbol"] =$symbol;
        $ArrParam["name_sht"] =$name_sht;
        //$ArrParam["broker"] =$broker;
        $ArrParam["age"] =$age;
        $ArrParam["industrial"] =$industrial;
       // $ArrParam["bondrange"] =$bondrange;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;


        $data =null;
        $totals= 0;

        $totals = $this->DataSourceCount($ArrParam, true);
        $data = $this->DataSource($ArrParam, true);
        $grandtoal = $this->DataSourceGrandTotal($ArrParam);

        //$chartds = $this->DataSourceBarChart($ArrParam);
        $htmlPaginate =Paginatre_gen($totals, $PageSize,'page_click_search', $PageNumber);

        //if((!empty($date_start)) && (!empty($date_end))) 
        //{
        /*  
        $thai_date_start = (!empty($date_start)) ? $this->toThaiDateTime($date_start, false) : "";
        $thai_date_end =  (!empty($date_end)) ? $this->toThaiDateTime($date_end, false) : "";
        */
        $utils = new MEAUtils();
        Log::info(get_class($this) .'::'. __FUNCTION__. ' $utils =new MEAUtils()' );
        $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
        $thai_date_end =  (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";
        Log::info(get_class($this) .'::'. __FUNCTION__. ' ' .  
            ' converted date: ' . $date_start .' to ' . $thai_date_start . ', ' . $date_end . ' to '. $thai_date_end );
        
        $pretty_date = $utils->prettyPrintThaiDate($thai_date_start, $thai_date_end, "red");
      
/*
        // option
        $opt_array = $ar['result'];
        
        $sub_array =  $opt_array[0];
        // crate column output array 
        $column = array();
        for($i =0; $i< 50; $i++) {
             if (count($opt_array) == 0) {
                $column[$i] = 'x'; 
             } else {
                $column[$i] = 'o'; 
            }
        }

        if (count($sub_array) > 0) {
            $sub = $sub_array['options'];
            $idx  = 0;
            foreach($sub as $x => $item) {
                
                if(strrpos($item, 'option') >= 0) {
                    Log::info($item);
                    $idx = intval(substr($item, 6, 2));
                    $column[$idx] = 'x';
                }
                $idx++;
            }
        }
        
        $prop_hdr = array();
        $ln = 0;
        for($i = 0; $i< 5; $i++ ) {
            if($column[$i] == 'x') {
               $ln++;
            }
        }
        array_push($prop_hdr, array('length' =>$ln));

        $ln = 0;
        for($i = 5; $i< 7; $i++ ) {
            if($column[$i] == 'x') {
               $ln++;
            }
        }
        array_push($prop_hdr, array('length' =>$ln));

        $ln = 0;
        for($i = 7; $i< 11; $i++ ) {
            if($column[$i] == 'x') {
               $ln++;
            }
        }
        array_push($prop_hdr, array('length' =>$ln));

        $ln = 0;
        for($i = 11; $i< 16; $i++ ) {
            if($column[$i] == 'x') {
               $ln++;
            }
        }
        array_push($prop_hdr, array('length' =>$ln));

*/

        //$opt_array = $results->options;
   
        $returnHTML = view($view_name)->with([
                'htmlPaginate'=> $htmlPaginate,
                'data' => $data,
                'totals' => $totals,
                'grandtoal'=>$grandtoal,
  
                'PageSize' =>$PageSize,
                'PageNumber' =>$PageNumber,
                'date_start' => $thai_date_start,
                'date_end' => $thai_date_end,
                'pretty_date' => $pretty_date,
                'TableTitle' => $title

            ])->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }




    public function ajax_chart_search(Request $request) {
        Log::info(get_class($this) .'::'. __FUNCTION__);
         
        $data = getmemulist();
        $title = getMenuName($data,63,2);

        $ar = $this->retrive_customize_field(63, 2);

        $symbol = $request->input('symbol');
        $name_sht = $request->input('name_sht');
        $broker = $request->input('broker');
        $age = $request->input('age');
        $industrial = $request->input('industrial');
        $cateid = $request->input('industrial');
       // $bondrange = $request->input('bondrange');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');



        $ArrParam = array();

        //{ pagesize: "10", PageNumber: 1, symbol: "", name_sht: "", industrial: "", date_start: "", date_end: "" }
        $ArrParam["pagesize"] ="";
        $ArrParam["PageNumber"] ="";

        $ArrParam["symbol"] =$symbol;
        $ArrParam["name_sht"] =$name_sht;
        $ArrParam["broker"] =$broker;
        $ArrParam["age"] =$age;
        $ArrParam["industrial"] =$industrial;
        $ArrParam["cateid"] =$cateid;
       // $ArrParam["bondrange"] =$bondrange;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;

        $data =null;
        $totals= 0;
        $subtitle = '';

        $chartds = $this->DataSourceBarChart($ArrParam);
        $chartdsAge = $this->DataSourceBarChartByAge($ArrParam);

        if(strlen($name_sht) > 1) {
            $subtitle = $this->GetSecurityName($name_sht);
        }

        $utils = new MEAUtils();
        $thai_date_start = (!empty($date_start)) ? $utils->toThaiDateTime($date_start, false) : "";
        $thai_date_end   = (!empty($date_end)) ? $utils->toThaiDateTime($date_end, false) : "";
        
        Log::info('subtitle:'   . $subtitle .
            ' converted date: ' . $date_start .' to ' . $thai_date_start . ', ' . $date_end . ' to '. $thai_date_end );
        $data  = array();
        $data1 = array();
        $data2 = array();

        
     
        $pretty_date = $utils->prettyPrintThaiDate($thai_date_start, $thai_date_end, "");
        $returnHTML = [
                'data' => $chartds,
                'data_age' => $chartdsAge,
                'date_start' => $thai_date_start,
                'date_end' => $thai_date_end,
                'pretty_date' => $pretty_date,
                'subtitle' => $subtitle

        ];
                      
        return response()->json(array('success' => true, 'result'=>$returnHTML));
    }


    public function ajax_report_search_export() {
       // Log::info(get_class($this) .'::'. __FUNCTION__);
        
      /*  $ArrParam["emp_id"] =Input::get("EmpID");
        $ArrParam["depart"] =Input::get("depart");
        $ArrParam["plan"] =Input::get("plan");
        $ArrParam["date_start"] =Input::get("date_start");
        $ArrParam["date_end"] =Input::get("date_end");

        $ArrParam["check_name"] =Input::get("check_name");
        $ArrParam["check_depart"] =Input::get("check_depart");
        $ArrParam["check_plan"] =Input::get("check_plan");
        $ArrParam["check_date"] =Input::get("check_date");
        // $sql2 = "SELECT TOP  5 * FROM  TBL_MEMBER_BENEFITS WHERe EMP_ID = '".get_userID()."' ORDER BY RECORD_DATE ASC";
*/
        $symbol = Input::get('symbol');
        $name_sht = Input::get('name_sht');
        $industrial = Input::get('industrial');
        $cateid = Input::input('industrial');
        $age = Input::get('age');
        $date_start = Input::get('date_start');
        $date_end = Input::get('date_end');

        $ArrParam = array();
        $ArrParam["pagesize"] ="";
        $ArrParam["PageNumber"] ="";
        
        $ArrParam["symbol"] =$symbol;
        $ArrParam["name_sht"] =$name_sht;
        // $ArrParam["broker"] =$broker;
        $ArrParam["age"] =$age;
        $ArrParam["industrial"] =$industrial;
        $ArrParam["cateid"] = $cateid;
        //$ArrParam["bondrange"] =$bondrange;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;


        $data = $this->DataSourceExport($ArrParam, false, false);
        $grandtotal  = $this->DataSourceGrandTotal($ArrParam);
        $results = array();
        foreach ($data as $item) {
//            $item->filed1 = 'some modification';
//            $item->filed2 = 'some modification2';
            $results[] = (array)$item;

           // $results[5] = get_date_notime($item->modify_new);

            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }
       
        $grandtotal_results = array();
         foreach ($grandtotal as $item) {
            $grandtotal_results[] = (array)$item;
        }




        Excel::create('รายงานซื้อ-ขายหลักทรัพย์', function ($excel) use ($results, $grandtotal_results, $ArrParam) {

            $excel->sheet('รายงานซื้อ-ขายหลักทรัพย์', function ($sheet) use ($results, $grandtotal_results, $ArrParam) {
                // first row styling and writing content
                $sheet->setStyle([
                    'borders' => [
                        'allborders' => [
                            'color' => [
                                'rgb' => 'b3b6b7'
                            ]
                        ]
                    ]
                ]);
                $sheet->mergeCells('A1:AC1');
                $sheet->mergeCells('A2:AC2');
                $sheet->mergeCells('A3:AC3');

                $sheet->mergeCells('G4:I4');
                $sheet->mergeCells('J4:N4');
                $sheet->mergeCells('O4:S4');
                $sheet->mergeCells('T4:X4');
                $sheet->mergeCells('AA4:AC4');

                //$sheet->mergeCells('Y4:Y5');
                //$sheet->mergeCells('Z4:Z5');
                 

                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Cordia New');
                    $row->setFontSize(20);
                    $row->setFontWeight('bold');
                    $row->setAlignment('center');
                    $row->setBackground('#b2babb');
                    // Set all borders (top, right, bottom, left)
                    $row->setBorder('thin', 'thin', 'none', 'thin');

                });

                $sheet->row(2, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'none', 'thin');
                });

                $sheet->row(3, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'thin', 'thin');

                });

                $sheet->row(1, array('รายงานซื้อ-ขายหลักทรัพย์'));

                if($ArrParam["date_start"] != "" && $ArrParam["date_end"] != ""){
                    $sheet->row(2, array('ในช่วงวันที่ ' . get_date_notime($ArrParam["date_start"]) . ' ถึง ' . get_date_notime($ArrParam["date_end"])   ));
                }

                $sheet->row(3, array('รายงานข้อมูล ณ วันที่ ' . get_date_notime(date("Y-m-d H:i:s"))));
               
                
                $sheet->cell('A5:AC5', function($cell) {
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#2ecc71');
                    
                });
				
				$sheet->cell('A4:AC4', function($cell) {
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#dedede');
                    
                });

				$sheet->cell('Y4:Z4', function($cell) {
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#dedede');
                    
                });

                 $sheet->cell('Y5:Z5', function($cell) {
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#2ecc71');
                    
                });

                $sheet->cell('G4', function($cell) {
                    $cell->setValue('Purchase');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setFontColor('#FFFFFF');
                    $cell->setBackground('#34495e');
                });

                $sheet->cell('J4', function($cell) {
                    $cell->setValue('Sale');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#34495e');
                    $cell->setFontColor('#FFFFFF');
                });
                $sheet->cell('J4:N4', function($cell) {
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#34495e');
                    $cell->setFontColor('#FFFFFF');
                });


                $sheet->cell('O4', function($cell) {
                    $cell->setValue('Maturity');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                });
                $sheet->cell('O4:S4', function($cell) {
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#34495e');
                    $cell->setFontColor('#FFFFFF');
                });

                $sheet->cell('T4', function($cell) {
                    $cell->setValue('Interest');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    //$cell->setBackground('#f2f3f4');
                });
                 $sheet->cell('T4:X4', function($cell) {
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#34495e');
                    $cell->setFontColor('#FFFFFF');
                });


                $sheet->cell('AA4', function($cell) {
                    $cell->setValue('อายุตราสาร');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });
                $sheet->cell('AA4:AC4', function($cell) {
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#34495e');
                    $cell->setFontColor('#FFFFFF');
                });



                
                $header[]   = null;
                $header[0]  = "Type";
                $header[1]  = "Settle Date";
                $header[2]  = "Symbol";
                $header[3]  = "Maturity";
                $header[4]  = "Face Value";
                $header[5]  = "Yield";
                $header[6]  = "Clean Value";
                $header[7]  = "Accrued Int";
                $header[8]  = "Total Value";
                $header[9]  = "Clean Value";
                $header[10] = "Accrued Int";
                $header[11] = "Total Value";
                $header[12] = "Total Amort Cost";
                $header[13] = "Profit(Loss)";
                $header[14] = "Clean Value";
                $header[15] = "Accrued Int";
                $header[16] = "Total Value";
                $header[17] = "Total Amort Cost";
                $header[18] = "Profit(Loss)";
                $header[19] = "Clean Value";
                $header[20] = "Accrued Int";
                $header[21] = "Total Value";
                $header[22] = "Total Amort Cost";
                $header[23] = "Profit(Loss)";
                $header[24] = "Coupon Rate";
                $header[25] = "Net Amount";
                $header[26] = "Year";
                $header[27] = "Month";
                $header[28] = "Day";
                
                $sheet->row(5, $header);

               


                $LAST_ROW = 5 + 1;
                // Set multiple column formats
                $sheet->setColumnFormat(array(
                         'A' => '@',
                         'C' => '@',
                         'E' => '#,##0.00',
                         'F' => '#,##0.00',
                         'G' => '#,##0.00',
                         'H' => '#,##0.00',
                         'I' => '#,##0.00',
                         'J' => '#,##0.00', // SUM(TOTAL_PURCHASE)
                         'K' => '#,##0.00',  
                         'L' => '#,##0.00',
                         'M' => '#,##0.00', // SUM(TOTAL_SALE)
                         'N' => '#,##0.00',
                         'O' => '#,##0.00', // SUM(PROFIT_LOSS)
                         'P' => '#,##0.00',
                         'Q' => '#,##0.00',
                         'R' => '#,##0.00',
                         'S' => '#,##0.00',
                         'T' => '#,##0.00',
                         'U' => '#,##0.00',
                         'V' => '#,##0.00',
                         'W' => '#,##0.00',
                         'X' => '#,##0.00',
                         'Y' => '#,##0.00',
                         'Z' => '#,##0.00',
                        'AA' => '@',
                        'AB' => '@',
                        'AC' => '@'   
                  ));
/*
  " SELECT TRANS_DATE, SYMBOL, SECURITIES_NAME, INDUSTRIAL, BU, ". 
                  "        UNIT_DIV, TOTAL_DIV, UNIT_PURCHASE, PRICE_PURCHASE, TOTAL_PURCHASE, ". 
                  "        P_SUM, UNIT_SALE, PRICE_SALE, TOTAL_SALE, PROFIT_LOSS, REMAIN_UNIT, ".
                  "        SUM(TOTAL_PURCHASE) as J,  " .
                  "        SUM(TOTAL_SALE) as N,  " .
                  "        SUM(PROFIT_LOSS) as O,  " .
*/         
                $rscount = count($results);   
                $idx = 0; 
                $N = 0.0;
                $J = 0.0;
                $O = 0.0;
                foreach ($results as $item) {
                    $rs[] = null; 
                    $TYPE_STR = '?';
                    switch($item['TYPE']){
                          case 'P':
                            $TYPE_STR  ='P'; //'Purchase';
                            break;
                          case 'S':
                            $TYPE_STR  ='S'; //'Sale';
                            break;
                          case 'I':
                            $TYPE_STR  ='I'; //'Interest';
                            break;
                          default:
                            $TYPE_STR  ='M'; //'Maturity';
                            break;     
                    }
                    $rs[ 0] = $TYPE_STR ; // A
                    $rs[ 1] = $item['SETTLE_DATE']; 
                    $rs[ 2] = $item['SYMBOL'];
                    $rs[ 3] = $item['MAT'];
                    $rs[ 4] = $item['FACE_VALUE'];
                    $rs[ 5] = $item['YIELD_PERCENTAGE'];

                    $rs[ 6] = $item['CLEAN_VAL_PUR'];
                    $rs[ 7] = $item['ACCRUED_PUR'];
                    $rs[ 8] = $item['TOTAL_VAL_PUR'];

                    $rs[ 9] = $item['CLEAN_VAL_SALE'];
                    $rs[10] = $item['ACCRUED_SALE'];
                    $rs[11] = $item['TOTAL_VAL_SALE'];
                    $rs[12] = $item['TOTAL_AMORT_SALE'];
                    $rs[13] = $item['PROFIT_LOSS_SALE'];

                    $rs[14] = $item['CLEAN_VAL_MAT'];
                    $rs[15] = $item['ACCRUED_MAT'];
                    $rs[16] = $item['TOTAL_VAL_MAT'];
                    $rs[17] = $item['TOTAL_AMORT_MAT'];
                    $rs[18] = $item['PROFIT_LOSS_MAT'];

                    $rs[19] = $item['CLEAN_VAL_INT'];
                    $rs[20] = $item['ACCRUED_INT'];
                    $rs[21] = $item['TOTAL_VAL_INT'];
                    $rs[22] = $item['TOTAL_AMORT_INT'];
                    $rs[23] = $item['PROFIT_LOSS_INT'];

                    $rs[24] = $item['COUPON_RATE'];
                    $rs[25] = $item['NET_AMOUNT'];

                    $rs[26] = $this->matGetYear(date('Y-m-d'),  $item['MAT'])[0];
                    $rs[27] = $this->matGetYear(date('Y-m-d'),  $item['MAT'])[1];
                    $rs[28] = $this->matGetYear(date('Y-m-d'),  $item['MAT'])[2];

                    $sheet->appendRow($rs);
                    $idx++;
                }

                $sheet->cell('D'. strval($idx+$LAST_ROW), function($cell) {
                        $cell->setValue("รวม");
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                /*$sheet->cell('D'. strval($idx+$LAST_ROW+1), function($cell) {
                        $cell->setValue("รวมทั้งหมด");
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
*/
                /* $sheet->cell('Y4', function($cell) {
                        $cell->setValue("Coupon Rate");
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
                $sheet->cell('Z4' , function($cell) {
                //        $cell->setValue("Net Amount");
                        $cell->setAlignment('right');
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
*/

                if($rscount > 0) {
                    foreach ($grandtotal_results as $item) {
                        $sheet->cell('G'. strval($idx+$LAST_ROW), function($cell) use ($item) {
                           $cell->setValue($item['SUM_CLEAN_VAL_PUR']);
                           $cell->setAlignment('right');
                           $cell->setBorder('thin', 'thin', 'thin', 'thin');
                         
                           $cell->setFontColor('#0000FF');
                        });

                        $sheet->cell('H'. strval($idx+$LAST_ROW), function($cell) use ($item) {
                           $cell->setValue($item['SUM_ACCRUED_PUR']);
                           $cell->setAlignment('right');
                           $cell->setBorder('thin', 'thin', 'thin', 'thin');
                         
                           $cell->setFontColor('#0000FF');
                        });

                        $sheet->cell('I'. strval($idx+$LAST_ROW), function($cell) use ($item) {
                           $cell->setValue($item['SUM_TOTAL_VAL_PUR']);
                           $cell->setAlignment('right');
                           $cell->setBorder('thin', 'thin', 'thin', 'thin');
                           $cell->setFontColor('#0000FF');
                        });

                        $sheet->cell('J'. strval($idx+$LAST_ROW), function($cell) use ($item) {
                           $cell->setValue($item['SUM_CLEAN_VAL_SALE']);
                           $cell->setAlignment('right');
                           $cell->setBorder('thin', 'thin', 'thin', 'thin');
                           $cell->setFontColor('#0000FF');
                        });

                        $sheet->cell('K'. strval($idx+$LAST_ROW), function($cell) use ($item) {
                           $cell->setValue($item['SUM_ACCRUED_SALE']);
                           $cell->setAlignment('right');
                           $cell->setBorder('thin', 'thin', 'thin', 'thin');
                           $cell->setFontColor('#0000FF');
                        });

                        $sheet->cell('L'. strval($idx+$LAST_ROW), function($cell) use ($item) {
                           $cell->setValue($item['SUM_TOTAL_VAL_SALE']);
                           $cell->setAlignment('right');
                           $cell->setBorder('thin', 'thin', 'thin', 'thin');
                           $cell->setFontColor('#0000FF');
                        });

                        $sheet->cell('M'. strval($idx+$LAST_ROW), function($cell) use ($item) {
                           $cell->setValue($item['SUM_TOTAL_AMORT_SALE']);
                           $cell->setAlignment('right');
                           $cell->setBorder('thin', 'thin', 'thin', 'thin');
                           $cell->setFontColor('#0000FF');
                        });

                        $sheet->cell('N'. strval($idx+$LAST_ROW), function($cell) use ($item) {
                           $cell->setValue($item['SUM_PROFIT_LOSS_SALE']);
                           $cell->setAlignment('right');
                           $cell->setBorder('thin', 'thin', 'thin', 'thin');
                           $cell->setFontColor('#0000FF');
                        });

                        $sheet->cell('O'. strval($idx+$LAST_ROW), function($cell) use ($item) {
                           $cell->setValue($item['SUM_CLEAN_VAL_MAT']);
                           $cell->setAlignment('right');
                           $cell->setBorder('thin', 'thin', 'thin', 'thin');
                           $cell->setFontColor('#0000FF');
                        });

                        $sheet->cell('P'. strval($idx+$LAST_ROW), function($cell) use ($item) {
                           $cell->setValue($item['SUM_ACCRUED_MAT']);
                           $cell->setAlignment('right');
                           $cell->setBorder('thin', 'thin', 'thin', 'thin');
                           $cell->setFontColor('#0000FF');
                        });

                        $sheet->cell('Q'. strval($idx+$LAST_ROW), function($cell) use ($item) {
                           $cell->setValue($item['SUM_TOTAL_VAL_MAT']);
                           $cell->setAlignment('right');
                           $cell->setBorder('thin', 'thin', 'thin', 'thin');
                           $cell->setFontColor('#0000FF');
                        });

                        $sheet->cell('R'. strval($idx+$LAST_ROW), function($cell) use ($item) {
                           $cell->setValue($item['SUM_TOTAL_AMORT_MAT']);
                           $cell->setAlignment('right');
                           $cell->setBorder('thin', 'thin', 'thin', 'thin');
                           $cell->setFontColor('#0000FF');
                        });

                        $sheet->cell('S'. strval($idx+$LAST_ROW), function($cell) use ($item) {
                           $cell->setValue($item['SUM_PROFIT_LOSS_MAT']);
                           $cell->setAlignment('right');
                           $cell->setBorder('thin', 'thin', 'thin', 'thin');
                           $cell->setFontColor('#0000FF');
                        });

                        $sheet->cell('T'. strval($idx+$LAST_ROW), function($cell) use ($item) {
                           $cell->setValue($item['SUM_CLEAN_VAL_INT']);
                           $cell->setAlignment('right');
                           $cell->setBorder('thin', 'thin', 'thin', 'thin');
                           $cell->setFontColor('#0000FF');
                        });

                        $sheet->cell('U'. strval($idx+$LAST_ROW), function($cell) use ($item) {
                           $cell->setValue($item['SUM_ACCRUED_INT']);
                           $cell->setAlignment('right');
                           $cell->setBorder('thin', 'thin', 'thin', 'thin');
                           $cell->setFontColor('#0000FF');
                        });
                        $sheet->cell('V'. strval($idx+$LAST_ROW), function($cell) use ($item) {
                           $cell->setValue($item['SUM_TOTAL_VAL_INT']);
                           $cell->setAlignment('right');
                           $cell->setBorder('thin', 'thin', 'thin', 'thin');
                           $cell->setFontColor('#0000FF');
                        });

                        $sheet->cell('W'. strval($idx+$LAST_ROW), function($cell) use ($item) {
                           $cell->setValue($item['SUM_TOTAL_AMORT_INT']);
                           $cell->setAlignment('right');
                           $cell->setBorder('thin', 'thin', 'thin', 'thin');
                           $cell->setFontColor('#0000FF');
                        });

                        $sheet->cell('X'. strval($idx+$LAST_ROW), function($cell) use ($item) {
                           $cell->setValue($item['SUM_PROFIT_LOSS_INT']);
                           $cell->setAlignment('right');
                           $cell->setBorder('thin', 'thin', 'thin', 'thin');
                           $cell->setFontColor('#0000FF');
                        });

                         
                    }
                } 

            });
        })->download('xls');
    }

    public function ajax_store_customize_field(Request $request) {
        $status = false;
        $result = array();
        $store_path = storage_path() .'/public/import';
        try {
            $menu_id = $request->input('menu_id');  //eg. 60
            $submenu_id = $request->input('submenu_id');
            $utils = new MEAUtils();
            $user = $utils->getLoggedInUser(); 
            $json = $request->input('options');
            $options = array('options'=>$json);
            $file = $store_path . '/' . $user. '-' . $menu_id . '-' . $submenu_id. '.json';
            file_put_contents($file, json_encode($options, TRUE));
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Saved:' . $file );
            array_push($results, $file);

        } catch (\Exception $e) {
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Exception:' .$e->getMessage());
            $status = false;
        } 
        return response()->json(array('success' => $status, 'result'=>$result));
    }

    
    public function retrive_customize_field($menu_id, $submenu_id) {
        $status = false;
        $result = array();
        $store_path = storage_path() .'/public/import';
        try {
            //$menu_id = $request->input('menu_id');  // eg. 60
            //$submenu_id = $request->input('submenu_id');
            $utils = new MEAUtils(); 
            $user = $utils->getLoggedInUser(); 
            $file = $store_path . '/' . $user . '-' . $menu_id . '-' . $submenu_id. '.json';
            $json = json_decode(file_get_contents($file), true);
            array_push($result, $json);

            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Retrived:' . $file );
            $status = true;
        } catch (\Exception $e) {
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Exception:' .$e->getMessage());
            $status = false;
        } 
        return array('success' => $status, 'result'=>$result);
    } 

    public function ajax_retrive_customize_field(Request $request) {
        /*$status = false;
        $result = array();
        $store_path = storage_path() .'/public/import';
        try {
            $menu_id = $request->input('menu_id');  // eg. 60
            $submenu_id = $request->input('submenu_id');
            $utils = new MEAUtils();// Session::get('user_data');
            $user = $utils->getLoggedInUser(); 
            $file = $store_path . '/' . $user . '-' . $menu_id . '-' . $submenu_id. '.json';
            $json = json_decode(file_get_contents($file), true);
            array_push($result, $json);

            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Retrived:' . $file );
            $status = true;
        } catch (\Exception $e) {
            Log::info(get_class($this) .'::'. __FUNCTION__ . ' Exception:' .$e->getMessage());
            $status = false;
        } 

        return response()->json(array('success' => $status, 'result'=>$result));*/
        $menu_id = $request->input('menu_id');  // eg. 60
        $submenu_id = $request->input('submenu_id');
          
        $r = $this->retrive_customize_field($menu_id, $submenu_id);
        return response()->json($r);
    }

    /* Utils */
   /* public function toThaiDateTime($strDate, $withTime)
    {
        $strYear = date("Y",strtotime($strDate)) + 543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        if ($withTime) {
            $strHour= date("H",strtotime($strDate));
            $strMinute= date("i",strtotime($strDate));
            $strSeconds= date("s",strtotime($strDate));
            
            return $strDay . " " . $strMonthThai. " ". $strYear .",  " . $strHour . ":" . $strMinute;
        } 
        return $strDay . " " . $strMonthThai. " ". $strYear ;
        
    }
    */

    /////////////////////////
    ///
    //
 /*   $opt_array = array('result'=>['option2', 'option1', 'option3']);
$sub_array = $opt_array['result']; 
$o = array();
foreach($sub_array as $x => $i ) {
    $str = substr($i, 6); // option0
    if(strrpos($str,'option') >= 0) {
        $idx = substr($i, 6, 1);
        $nr = array($idx => 'checked');
        array_push($o, $nr);
    }
   
}

print_r($o['0']);
 */

}


/**
 * 1) STATEMENT SUM TOTAL:
 * =====================
 * V1:
 * SELECT  COALESCE(SUM(TOTAL_PURCHASE) / NULLIF(SUM(UNIT_PURCHASE), 0), 0) AS TMP_PRICE_PERCHASE,
 *       COALESCE (SUM(TOTAL_SALE) / NULLIF(SUM(UNIT_SALE), 0), 0) AS TMP_PRICE_SALE,
 *       COALESCE (SUM(TOTAL_DIV) / NULLIF(SUM(UNIT_DIV), 0), 0) AS TMP_PRICE_DIV
 *
 *     FROM TBL_P2_EQUITY_TRANS WHERE SYMBOL='CPALL'
 *     AND SECURITIES_NAME ='UOBAM' AND (TRANS_DATE BETWEEN '2017-05-01 00:00:01' AND  '2017-05-31 23:59:59');
 *
 */

 /* V2:
 SELECT  COALESCE(SUM(TOTAL_PURCHASE) / NULLIF(SUM(UNIT_PURCHASE), 0), 0) AS TMP_PRICE_PERCHASE, SUM(UNIT_PURCHASE) AS SUM_UNIT_PERCHASE, SUM(TOTAL_PURCHASE) AS SUM_TOAL_PURCHASE,
        COALESCE(SUM(TOTAL_SALE) / NULLIF(SUM(UNIT_SALE), 0), 0) AS TMP_PRICE_SALE, SUM(UNIT_SALE) AS SUM_UNIT_SALE, SUM(TOTAL_SALE) AS SUM_TOAL_SALE, SUM(TOTAL_COST) AS SUM_TOTAL_COST, SUM(PROFIT_LOSS) AS SUM_PROFIT_LOSS,

        COALESCE(SUM(TOTAL_DIV) / NULLIF(SUM(UNIT_DIV), 0), 0) AS TMP_PRICE_DIV,  SUM(UNIT_DIV) AS SUM_UNIT_DIV, SUM(TOTAL_DIV) AS SUM_TOAL_DIV

      FROM TBL_P2_EQUITY_TRANS WHERE SYMBOL='CPALL'
       AND SECURITIES_NAME ='UOBAM' AND (TRANS_DATE BETWEEN '2017-01-01 00:00:01' AND  '2017-01-31 23:59:59');


 */
       /*
       SELECT  COALESCE(SUM(TOTAL_PURCHASE) / NULLIF(SUM(UNIT_PURCHASE), 0), 0) AS TMP_PRICE_PERCHASE, 
        SUM(UNIT_PURCHASE) AS SUM_UNIT_PERCHASE, SUM(TOTAL_PURCHASE) AS SUM_TOAL_PURCHASE,  
        

        COALESCE(SUM(TOTAL_SALE) / NULLIF(SUM(UNIT_SALE), 0), 0) AS TMP_PRICE_SALE, 
        SUM(UNIT_SALE) AS SUM_UNIT_SALE, SUM(TOTAL_SALE) AS SUM_TOTAL_SALE, SUM(TOTAL_COST) AS SUM_TOTAL_COST, SUM(PROFIT_LOSS) AS SUM_PROFIT_LOSS,
        
        COALESCE(SUM(TOTAL_DIV) / NULLIF(SUM(UNIT_DIV), 0), 0) AS TMP_PRICE_DIV,  SUM(UNIT_DIV) AS SUM_UNIT_DIV, SUM(TOTAL_DIV) AS SUM_TOAL_DIV,

        SUM(UNIT_PURCHASE) - SUM(UNIT_SALE) as TOTAL_UNIT_DIFF

         

      FROM TBL_P2_EQUITY_TRANS WHERE SYMBOL='CPALL'
       AND SECURITIES_NAME ='UOBAM' AND (TRANS_DATE BETWEEN '2017-01-01 00:00:01' AND  '2017-01-31 23:59:59');


       */

       /* V4:
      SELECT   a.LAST_SYMBOL, 
        (CASE WHEN a.LAST_UNIT_PURCHASE >= 0 THEN 'IN PERCHASE' ELSE 'IN SALE' END) as T,
        --a.LAST_UNIT_PURCHASE as WOW, 
        COALESCE(SUM(b.TOTAL_PURCHASE) / NULLIF(SUM(b.UNIT_PURCHASE), 0), 0) AS TMP_PRICE_PERCHASE, 
        SUM(b.UNIT_PURCHASE) AS SUM_UNIT_PERCHASE, SUM(b.TOTAL_PURCHASE) AS SUM_TOAL_PURCHASE,  
        

        COALESCE(SUM(b.TOTAL_SALE) / NULLIF(SUM(b.UNIT_SALE), 0), 0) AS TMP_PRICE_SALE, 
        SUM(b.UNIT_SALE) AS SUM_UNIT_SALE, SUM(b.TOTAL_SALE) AS SUM_TOTAL_SALE,
       SUM(b.TOTAL_COST) AS SUM_TOTAL_COST, SUM(b.PROFIT_LOSS) AS SUM_PROFIT_LOSS,
        
        COALESCE(SUM(b.TOTAL_DIV) / NULLIF(SUM(b.UNIT_DIV), 0), 0) AS TMP_PRICE_DIV, 
                SUM(b.UNIT_DIV) AS SUM_UNIT_DIV, SUM(b.TOTAL_DIV) AS SUM_TOAL_DIV,

        SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as TOTAL_UNIT_DIFF


      FROM TBL_P2_EQUITY_TRANS b INNER JOIN  
         
         (select 
            TOP 1 TRANS_DATE as LAST_TRANS_DATE, 
            TYPE as LAST_TYPE, 
            UNIT_PURCHASE as LAST_UNIT_PURCHASE,
            SYMBOL as LAST_SYMBOL ,
            SECURITIES_NAME
        from 
            TBL_P2_EQUITY_TRANS
        WHERE 
            TRANS_DATE < '2017-01-31 23:59:59' AND SYMBOL='CPALL' AND SECURITIES_NAME='UOBAM') a 

         ON a.LAST_SYMBOL = b.SYMBOL 
           AND a.SECURITIES_NAME = b.SECURITIES_NAME
           AND b.SYMBOL='CPALL' AND b.SECURITIES_NAME ='UOBAM'
           AND (b.TRANS_DATE BETWEEN '2017-01-07 00:00:01' AND  '2017-01-31 23:59:59')  
         
          GROUP BY a.LAST_SYMBOL, a.LAST_UNIT_PURCHASE;
      


       */

          /*
          V5:
          ====
          select * FROM TBL_P2_EQUITY_TRANS,
 (SELECT   a.LAST_SYMBOL, 
        (CASE WHEN a.LAST_UNIT_PURCHASE > 0 THEN 'IN PURCHASE' ELSE 'IN SALE' END) as LAST_TR,
        --a.LAST_UNIT_PURCHASE as WOW, 
        COALESCE(SUM(b.TOTAL_PURCHASE) / NULLIF(SUM(b.UNIT_PURCHASE), 0), 0) AS TMP_PRICE_PURCHASE, 
        SUM(b.UNIT_PURCHASE) AS SUM_UNIT_PURCHASE, SUM(b.TOTAL_PURCHASE) AS SUM_TOAL_PURCHASE,  
        

        COALESCE(SUM(b.TOTAL_SALE) / NULLIF(SUM(b.UNIT_SALE), 0), 0) AS TMP_PRICE_SALE, 
        SUM(b.UNIT_SALE) AS SUM_UNIT_SALE, SUM(b.TOTAL_SALE) AS SUM_TOTAL_SALE,
       SUM(b.TOTAL_COST) AS SUM_TOTAL_COST, SUM(b.PROFIT_LOSS) AS SUM_PROFIT_LOSS,
        
        COALESCE(SUM(b.TOTAL_DIV) / NULLIF(SUM(b.UNIT_DIV), 0), 0) AS TMP_PRICE_DIV, 
                SUM(b.UNIT_DIV) AS SUM_UNIT_DIV, SUM(b.TOTAL_DIV) AS SUM_TOAL_DIV,

        ABS(SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE)) as TOTAL_UNIT_DIFF


      FROM TBL_P2_EQUITY_TRANS b INNER JOIN  
         
         (select 
            TOP 1 TRANS_DATE as LAST_TRANS_DATE, 
            TYPE as LAST_TYPE, 
            UNIT_PURCHASE as LAST_UNIT_PURCHASE,
            SYMBOL as LAST_SYMBOL ,
            SECURITIES_NAME
        from 
            TBL_P2_EQUITY_TRANS
        WHERE 
            TRANS_DATE < '2017-01-18 23:59:59' AND SYMBOL='AP' AND SECURITIES_NAME='UOBAM' ORDER BY TRANS_DATE DESC) a 

         ON a.LAST_SYMBOL = b.SYMBOL 
           AND a.SECURITIES_NAME = b.SECURITIES_NAME
           AND b.SYMBOL='AP' AND b.SECURITIES_NAME ='UOBAM'
           AND (b.TRANS_DATE BETWEEN '2017-01-11 00:00:01' AND  '2017-01-18 23:59:59')             
         
           GROUP BY a.LAST_SYMBOL, a.LAST_UNIT_PURCHASE ) C 

      WHERE 
           SYMBOL='AP' AND SECURITIES_NAME ='UOBAM' 
           AND (TRANS_DATE BETWEEN '2017-01-01 00:00:01' AND  '2017-01-18 23:59:59')
 

          */
/*
 V6:
 ===

SELECT x.TRANS_DATE, x.SYMBOL,  x.BU, x.INDUS, x.UNIT_PURCHASE, x.PRICE_PURCHASE, x.TOTAL_PURCHASE, x.P_SUM + COALESCE(SUM(y.P_SUM),0) SUM_UNIT_PURCHASE
      , x.UNIT_SALE, x.PRICE_SALE, x.TOTAL_SALE, x.PROFIT_LOSS
FROM
(
    SELECT 
        SUM(UNIT_PURCHASE) P_SUM, 
        SYMBOL, 
        D.BU as BU, 
        D.INDUSTRIAL as INDUS, 
        TRANS_DATE, 
        UNIT_PURCHASE, 
        PRICE_PURCHASE, 
        TOTAL_PURCHASE, 
        UNIT_SALE,
        PRICE_SALE, TOTAL_SALE, PROFIT_LOSS
    FROM 
        TBL_P2_EQUITY_TRANS, 
        (SELECT BU, INDUSTRIAL FROM TBL_P2_EQUITY_INDEX WHERE SYMBOL='APCS'  ) D
    
    
    GROUP BY TRANS_DATE, UNIT_PURCHASE, SYMBOL, PRICE_PURCHASE, TOTAL_PURCHASE, D.BU, D.INDUSTRIAL, UNIT_SALE,
          PRICE_SALE, TOTAL_SALE, PROFIT_LOSS
) x

LEFT OUTER JOIN
(
    SELECT SUM(UNIT_PURCHASE) P_SUM, TRANS_DATE, UNIT_PURCHASE
    FROM TBL_P2_EQUITY_TRANS
    GROUP BY TRANS_DATE, UNIT_PURCHASE
) y ON y.TRANS_DATE < x.TRANS_DATE

WHERE SYMBOL='APCS'

GROUP BY x.TRANS_DATE, x.P_SUM, x.SYMBOL, x.INDUS, x.BU, x.UNIT_PURCHASE, x.PRICE_PURCHASE, x.TOTAL_PURCHASE, x.UNIT_SALE,
         x.PRICE_SALE, x.TOTAL_SALE, x.PROFIT_LOSS
 ORDER BY x.TRANS_DATE



 */

/*
 V7:
 ...
 ;WITH cte AS (
    SELECT 
        row_number() OVER (ORDER BY TRANS_DATE, SYMBOL) AS rownum,*
    FROM 
        TBL_P2_EQUITY_TRANS
)
SELECT
    a.TRANS_DATE, a.SYMBOL,  a.UNIT_PURCHASE, a.UNIT_SALE, 
    SUM(b.UNIT_PURCHASE) AS P_SUM,
    SUM(b.UNIT_SALE) AS S_SUM,
    SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as REMAIN_UNIT
FROM  cte a
LEFT JOIN cte b 
ON a.SYMBOL = b.SYMBOL AND b.rownum <= a.rownum
GROUP BY a.SYMBOL, a.TRANS_DATE, a.rownum, a.UNIT_PURCHASE, a.UNIT_SALE
ORDER BY a.SYMBOL, a.TRANS_DATE

*/ 
/*
V8:
==


;WITH cte AS (
    SELECT row_number() OVER (ORDER BY TRANS_DATE) AS rownum, x.*, k.INDUSTRIAL, k.BU 
    FROM TBL_P2_EQUITY_TRANS x, TBL_P2_EQUITY_INDEX  k
    WHERE x.SYMBOL = k.SYMBOL
)
SELECT
    a.TRANS_DATE, a.SYMBOL, a.SECURITIES_NAME, a.INDUSTRIAL, a.BU,
    a.UNIT_PURCHASE, a.PRICE_PURCHASE,a.TOTAL_PURCHASE,
    a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS,
    a.UNIT_DIV, a.TOTAL_DIV, 
    SUM(b.UNIT_PURCHASE) AS P_SUM,
    SUM(b.UNIT_SALE) AS S_SUM,
    SUM(b.UNIT_DIV) AS D_SUM,
    SUM(b.UNIT_PURCHASE) - SUM(b.UNIT_SALE) as REMAIN_UNIT
     
FROM  cte a 
            
    
LEFT JOIN cte b 

ON a.SYMBOL = b.SYMBOL AND b.rownum <= a.rownum
GROUP BY a.SYMBOL, a.TRANS_DATE, a.rownum, 
    a.UNIT_PURCHASE, a.PRICE_PURCHASE, a.TOTAL_PURCHASE,
    a.UNIT_SALE, a.PRICE_SALE, a.TOTAL_SALE, a.TOTAL_COST, a.PROFIT_LOSS,
    a.UNIT_DIV, a.TOTAL_DIV, a.SECURITIES_NAME,
    a.INDUSTRIAL, a.BU
ORDER BY a.SYMBOL, a.TRANS_DATE


*/
?>
