<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Jenssegers\Date\Date;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Support\Facades\Log;

/**
 * FILE: AdminP2EquityNavReportController.php
 */
class AdminP2EquityNavReportController extends Controller
{

    const UNSPECIFIED                      = 0; // 0 - display TABLE current year and PIE last month 
    const SPECIFIED_YEAR_AND_LAST_MONTH    = 1; // 1 - display TABLE selected year and PIE last month
    const CURRENT_YEAR_AND_SPECIFIED_MONTH = 2; // 2 - display TABLE /PIE with selected month 
    const SPECIFIED_YEAR_AND_MONTH         = 3; // 3 - display PIE chart upon selected year and month


    /** 
     * (Equit/Bound Report)
     */
    public function getreport_main()
    {
        $view_name = 'backend.pages.p2_equity_nav_report_main';
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 62,
            'menu_id' => 1,
            'title' => getMenuName($data,62,1) . '|  MEA FUND'
        ] );

        $allquery = "SELECT * FROM TBL_P2_EQUITY_SECURITIES";
        $equitylist = DB::select(DB::raw($allquery));

        $allquery = "SELECT * FROM TBL_P2_EQUITY_CATEGORY";
        $equitycategory = DB::select(DB::raw($allquery));

        ////////////////////////////////////////////////

        $allquery      = "SELECT MAX(YEAR(REFERENCE_DATE)) as YYYY FROM TBL_P2_BOND_NAV";
        $bondMaxYear   = DB::select(DB::raw($allquery));
        $allquery      = "SELECT MIN(YEAR(REFERENCE_DATE)) as YYYY FROM TBL_P2_BOND_NAV";
        $bondMinYear   = DB::select(DB::raw($allquery));

        $allquery      = "SELECT MAX(YEAR(REFERENCE_DATE)) as YYYY FROM TBL_P2_EQUITY_NAV";
        $equityMaxYear = DB::select(DB::raw($allquery));
        $allquery      = "SELECT MIN(YEAR(REFERENCE_DATE)) as YYYY FROM TBL_P2_EQUITY_NAV";
        $equityMinYear = DB::select(DB::raw($allquery));

        ////////////////////////////////////////////////
        
        $currentYear = date("Y");
        $years = range($currentYear, $currentYear - 20);

        $BminY =  $currentYear;
        $BmaxY =  $currentYear;
        $EminY =  $currentYear;
        $EmaxY =  $currentYear;
        $tmp    = array();

        if($bondMinYear && $bondMaxYear) {
            $BminY = $bondMinYear[0]->YYYY;
            $BmaxY = $bondMaxYear[0]->YYYY;
           //$bondyears = range($bondMinYear[0]->YYYY, $bondMaxYear[0]->YYYY);

        }

        if($equityMinYear && $equityMaxYear) {
            $EminY = $bondMinYear[0]->YYYY;
            $EmaxY = $bondMaxYear[0]->YYYY;
        }

        $tmp = array($EminY, $EmaxY, $BminY, $BmaxY);
        $minYear = min($tmp);
        $maxYear = max($tmp);
        $years = range($minYear, $maxYear);
        
        return view($view_name)->with([
            'equitylist'     => $equitylist,
            'equitycategory' => $equitycategory,
            'years'          => $years
            //'bondyears'      => $bondyears,
            //'equityyears'    => $equityyears
            ]);
    }

   /* public function getDetils($id)
    {
        $view_name = 'backend.pages.p2_equity_nav_report_details';
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 62,
            'menu_id' => 1,
            'title' => getMenuName($data,62,1) . '|  MEA FUND'
        ] );

        $allquery = "SELECT * FROM TBL_P2_EQUITY_SECURITIES";
        $equitylist = DB::select(DB::raw($allquery));

        $allquery = "SELECT * FROM TBL_P2_EQUITY_CATEGORY";
        $equitycategory = DB::select(DB::raw($allquery));

        $currentYear = date("Y");
        $years = range($currentYear, $currentYear - 20);
        
        return view($view_name)->with([
            'equitylist'     => $equitylist,
            'equitycategory' => $equitycategory,
            'years'  => $years
            ]);
    }*/

/*
    public function getreport1()
    {
        $view_name = 'backend.pages.p2_equity_nav_report_1';
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 62,
            'menu_id' => 1,
            'title' => getMenuName($data,62,1) . '|  MEA FUND'
        ] );

        $allquery = "SELECT * FROM TBL_P2_EQUITY_SECURITIES";
        $equitylist = DB::select(DB::raw($allquery));

        $allquery = "SELECT * FROM TBL_P2_EQUITY_CATEGORY";
        $equitycategory = DB::select(DB::raw($allquery));

        return view($view_name)->with([
            'equitylist'     => $equitylist,
            'equitycategory' => $equitycategory
            ]);

    }

    public function getreport2()
    {
        $view_name = 'backend.pages.p2_equity_nav_report_2';
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 62,
            'menu_id' => 1,
            'title' => getMenuName($data,62, 1) . '|  MEA FUND'
        ] );

        $allquery = "SELECT * FROM TBL_P2_EQUITY_SECURITIES";
        $equitylist = DB::select(DB::raw($allquery));

        $allquery = "SELECT * FROM TBL_P2_EQUITY_CATEGORY";
        $equitycategory = DB::select(DB::raw($allquery));

        return view($view_name)->with([
            'equitylist'     => $equitylist,
            'equitycategory' => $equitycategory
            ]);
    }
*/
/*
    public  function  DataSourceCount($ArrParam,$IsCase){

        $PageSize = $ArrParam['pagesize'];
        $PageNumber = $ArrParam['PageNumber'];

        $emp_id =$ArrParam['emp_id'];
        $depart = $ArrParam['depart'];
        $plan = $ArrParam['plan'];
        $date_start = $ArrParam['date_start'];
        $date_end = $ArrParam['date_end'];

        $check_name =$ArrParam["check_name"] ;
        $check_depart =$ArrParam["check_depart"] ;
        $check_plan=$ArrParam["check_plan"] ;
        $check_date=$ArrParam["check_date"] ;

        $where = " WHERE focus.EMP_ID IS NOT  NULL";

        if(!empty($emp_id)&& $check_name== "true"){
            $where .= " AND focus.EMP_ID = '".$emp_id."'";
        }
        if(!empty($depart)&& $check_depart== "true"){
            $where .= " AND em.DEP_SHT  = '".$depart."'";
        }
        if(!empty($plan)&& $check_plan== "true"){
            $where .= " AND new.USER_SAVING_RATE = '".$plan."'";
        }
        if(!empty($date_start) && !empty($date_end)&& $check_date== "true"){
            $where .= " AND new.CHANGE_SAVING_RATE_DATE  BETWEEN '".$date_start."' AND '".$date_end."'";
        }

        $allquery = "SELECT COUNT(focus.EMP_ID ) AS total FROM (SELECT f.EMP_ID, MAX(f.CHANGE_SAVING_RATE_DATE) as datenew FROM TBL_USER_SAVING_RATE f GROUP BY f.EMP_ID) AS focus INNER JOIN TBL_EMPLOYEE_INFO em ON em.EMP_ID = focus.EMP_ID CROSS APPLY (SELECT TOP 1 fn.USER_SAVING_RATE,fn.CHANGE_SAVING_RATE_DATE , fn.EFFECTIVE_DATE, fn.MODIFY_BY FROM TBL_USER_SAVING_RATE fn WHERE fn.EMP_ID = focus.EMP_ID ORDER BY fn.CHANGE_SAVING_RATE_DATE DESC) AS new OUTER APPLY (SELECT TOP 1 ol.USER_SAVING_RATE FROM TBL_USER_SAVING_RATE ol WHERE ol.EMP_ID = focus.EMP_ID AND ol.CHANGE_SAVING_RATE_DATE <new.CHANGE_SAVING_RATE_DATE) AS old";

        if($IsCase){
            $allquery .= $where;
        }

        $all = DB::select(DB::raw($allquery));
        return  $all[0]->total;
    }

    public  function  DataSource($ArrParam, $IsCase, $ispageing = true) {

        $where = "";
        if($ispageing){
            $PageSize = $ArrParam['pagesize'];
            $PageNumber = $ArrParam['PageNumber'];
        }

        if($IsCase) {

            $emp_id = $ArrParam['emp_id'];
            $depart = $ArrParam['depart'];
            $plan = $ArrParam['plan'];
            $date_start = $ArrParam['date_start'];
            $date_end = $ArrParam['date_end'];

            $check_name =$ArrParam["check_name"] ;
            $check_depart =$ArrParam["check_depart"] ;
            $check_plan=$ArrParam["check_plan"] ;
            $check_date=$ArrParam["check_date"] ;

            $where = " WHERE focus.EMP_ID IS NOT  NULL";

            if(!empty($emp_id)&& $check_name== "true"){
                $where .= " AND focus.EMP_ID = '".$emp_id."'";
            }
            if(!empty($depart)&& $check_depart== "true"){
                $where .= " AND em.DEP_SHT  = '".$depart."'";
            }
            if(!empty($plan)&& $check_plan== "true"){
                $where .= " AND new.USER_SAVING_RATE = '".$plan."'";
            }
            if(!empty($date_start) && !empty($date_end)&& $check_date== "true"){
                $where .= " AND new.CHANGE_SAVING_RATE_DATE  BETWEEN '".$date_start."' AND '".$date_end."'";
            }
        }


        $query="SELECT focus.EMP_ID As EMP_ID,em.FULL_NAME AS FULL_NAME,em.DEP_SHT AS DEP_SHT ,old.USER_SAVING_RATE as rate_old, new.USER_SAVING_RATE AS rate_new,new.CHANGE_SAVING_RATE_DATE AS modify_new , new.EFFECTIVE_DATE AS effec_new,new.MODIFY_BY AS new_modify_by,new.STATUS_DESC,new.LEAVE_FUND_GROUP_DATE
                FROM (SELECT f.EMP_ID, MAX(f.CHANGE_SAVING_RATE_DATE) as datenew FROM TBL_USER_SAVING_RATE f GROUP BY f.EMP_ID) AS focus
                INNER JOIN TBL_EMPLOYEE_INFO em ON em.EMP_ID = focus.EMP_ID CROSS APPLY (SELECT TOP 1 fn.USER_SAVING_RATE,fn.CHANGE_SAVING_RATE_DATE , fn.EFFECTIVE_DATE, fn.MODIFY_BY,uss.STATUS_DESC,fn.USER_STATUS_ID,fn.LEAVE_FUND_GROUP_DATE FROM TBL_USER_SAVING_RATE fn LEFT OUTER JOIN TBL_USER_STATUS uss ON uss.USER_STATUS_ID = fn.USER_STATUS_ID WHERE fn.EMP_ID = focus.EMP_ID ORDER BY fn.CHANGE_SAVING_RATE_DATE DESC) AS new
                OUTER APPLY (SELECT TOP 1 ol.USER_SAVING_RATE FROM TBL_USER_SAVING_RATE ol WHERE ol.EMP_ID = focus.EMP_ID AND ol.CHANGE_SAVING_RATE_DATE <new.CHANGE_SAVING_RATE_DATE ORDER BY ol.CHANGE_SAVING_RATE_DATE DESC) AS old";


        if($IsCase){
            $query .= $where;
        }

        $query .= " ORDER BY focus.datenew DESC";

        if($ispageing){
            $query .=  " OFFSET ".$PageSize." * (".$PageNumber." - 1) ROWS FETCH NEXT ".$PageSize." ROWS ONLY OPTION (RECOMPILE)";
        }

        return DB::select(DB::raw($query));
    }

    public function ajax_report_search1(Request $request)
    {

        Log::info(get_class($this) .'::'. __FUNCTION__);
        $view_name = 'backend.pages.ajax.ajax_p2_equity_nav_report_1';
        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $emp_id = $request->input('emp_id');
        $depart = $request->input('depart');
        $plan = $request->input('plan');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $check_name = $request->input('check_name');
        $check_depart = $request->input('check_depart');
        $check_plan = $request->input('check_plan');
        $check_date = $request->input('check_date');

        $ArrParam = array();
        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;
        $ArrParam["emp_id"] =$emp_id;
        $ArrParam["depart"] =$depart;
        $ArrParam["plan"] =$plan;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;

        $ArrParam["check_name"] =$check_name;
        $ArrParam["check_depart"] =$check_depart;
        $ArrParam["check_plan"] =$check_plan;
        $ArrParam["check_date"] =$check_date;


        $data =null;
        $totals= 0;

        $data = $this->DataSource($ArrParam,true);

        $totals = 1;// $this->DataSourceCount($ArrParam,true);

        $htmlPaginate =Paginatre_gen($totals, $PageSize,'page_click_search',$PageNumber);


        Log::info(get_class($this) .'::'. __FUNCTION__. ' ->Return HTML ($PageSize, $PageNumber)' );
        $returnHTML = view($view_name)->with([
            'htmlPaginate'=> $htmlPaginate,
            'data' => $data,
            'totals' => $totals,
            'PageSize' =>$PageSize,
            'PageNumber' =>$PageNumber

        ])->render();

         
      
        return response()->json(array('success' => true, 'html'=>$returnHTML));

    }

    public function ajax_report_search_export1() 
    {
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $ArrParam = array();
        $ArrParam["pagesize"] ="";
        $ArrParam["PageNumber"] ="";
        $ArrParam["emp_id"] =Input::get("EmpID");
        $ArrParam["depart"] =Input::get("depart");
        $ArrParam["plan"] =Input::get("plan");
        $ArrParam["date_start"] =Input::get("date_start");
        $ArrParam["date_end"] =Input::get("date_end");

        $ArrParam["check_name"] =Input::get("check_name");
        $ArrParam["check_depart"] =Input::get("check_depart");
        $ArrParam["check_plan"] =Input::get("check_plan");
        $ArrParam["check_date"] =Input::get("check_date");
       
        $data = $this->DataSource($ArrParam,true,false);
        $results = array();
        foreach ($data as $item) {
            $results[] = (array)$item;
            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }

        Excel::create('ExcelExport', function ($excel) use ($results,$ArrParam) {
            $excel->sheet('Sheetname', function ($sheet) use ($results,$ArrParam) {
                // first row styling and writing content
                $sheet->mergeCells('A1:J1');
                $sheet->mergeCells('A2:J2');
                $sheet->mergeCells('A3:J3');
                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Comic Sans MS');
                    $row->setFontSize(20);
                    $row->setAlignment('center');
                });

                $sheet->row(2, function ($row) {$row->setAlignment('center');});
                $sheet->row(3, function ($row) {$row->setAlignment('center');});

                $sheet->row(1, array('รายชื่อสมาชิกเปลี่ยนอัตราสะสม'));

                if($ArrParam["check_date"] == "true" && $ArrParam["date_start"] != "" && $ArrParam["date_end"] != ""){
                    $sheet->row(2, array('ในช่วงวันที่ ' . get_date_notime($ArrParam["date_start"]) . ' ถึง ' . get_date_notime($ArrParam["date_end"])   ));
                }


                $sheet->row(3, array('รายงานข้อมูล ณ วันที่ ' . get_date_notime(date("Y-m-d H:i:s"))));

                $header[] = null;
                $header[0] = 'รหัสพนักงาน';
                $header[1] = "ชื่อ-นามสกุล";
                $header[2] = "หน่วยงาน";
                $header[3] = "อัตราสะสม(เดิม)";
                $header[4] = "อัตราสะสม(ใหม่)";
                $header[5] = "วันที่ทำรายการ";
                $header[6] = "วันที่มีผล";
                $header[7] = "ผู้ทำรายการ";
                $header[8] = "สถานะ";
                $header[9] = "วันที่พ้นสภาพ";

                $sheet->row(4, $header);
                foreach ($results as $user) {
                    $sheet->appendRow($user);
                }
            });
        })->download('xls');
    }
*/
    ///////////////////////////
    /// NAV2
    public function ajax_report_search2(Request $request)
    {
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $view_name = 'backend.pages.ajax.ajax_p2_equity_nav_report_2';
        $PageSize = $request->input('pagesize');
        $PageNumber = $request->input('PageNumber');

        $emp_id = $request->input('emp_id');
        $depart = $request->input('depart');
        $plan = $request->input('plan');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $check_name = $request->input('check_name');
        $check_depart = $request->input('check_depart');
        $check_plan = $request->input('check_plan');
        $check_date = $request->input('check_date');

        $ArrParam = array();
        $ArrParam["pagesize"] =$PageSize;
        $ArrParam["PageNumber"] =$PageNumber;
        $ArrParam["emp_id"] =$emp_id;
        $ArrParam["depart"] =$depart;
        $ArrParam["plan"] =$plan;
        $ArrParam["date_start"] =$date_start;
        $ArrParam["date_end"] =$date_end;

        $ArrParam["check_name"] =$check_name;
        $ArrParam["check_depart"] =$check_depart;
        $ArrParam["check_plan"] =$check_plan;
        $ArrParam["check_date"] =$check_date;

        $data =null;
        $totals= 0;

        $data = $this->DataSource($ArrParam,true);

        $totals = 1;

        $htmlPaginate =Paginatre_gen($totals, $PageSize,'page_click_search',$PageNumber);

        Log::info(get_class($this) .'::'. __FUNCTION__. ' ->Return HTML ($PageSize, $PageNumber)' );
        $returnHTML = view($view_name)->with([
            'htmlPaginate'=> $htmlPaginate,
            'data' => $data,
            'totals' => $totals,
            'PageSize' =>$PageSize,
            'PageNumber' =>$PageNumber

        ])->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }

    public function ajax_report_search_export2() {
        Log::info(get_class($this) .'::'. __FUNCTION__);
        $ArrParam = array();
        $ArrParam["pagesize"] ="";
        $ArrParam["PageNumber"] ="";
        $ArrParam["emp_id"] =Input::get("EmpID");
        $ArrParam["depart"] =Input::get("depart");
        $ArrParam["plan"] =Input::get("plan");
        $ArrParam["date_start"] =Input::get("date_start");
        $ArrParam["date_end"] =Input::get("date_end");

        $ArrParam["check_name"] =Input::get("check_name");
        $ArrParam["check_depart"] =Input::get("check_depart");
        $ArrParam["check_plan"] =Input::get("check_plan");
        $ArrParam["check_date"] =Input::get("check_date");
      
        $data = $this->DataSource($ArrParam,true,false);
        $results = array();
        foreach ($data as $item) {
            $results[] = (array)$item;

            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }



        Excel::create('ExcelExport', function ($excel) use ($results,$ArrParam){
            $excel->sheet('Sheetname', function ($sheet) use ($results,$ArrParam){
                $sheet->mergeCells('A1:J1');
                $sheet->mergeCells('A2:J2');
                $sheet->mergeCells('A3:J3');
                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Comic Sans MS');
                    $row->setFontSize(20);
                    $row->setAlignment('center');
                });

                $sheet->row(2, function ($row) {$row->setAlignment('center');});
                $sheet->row(3, function ($row) {$row->setAlignment('center');});
                $sheet->row(1, array('รายชื่อสมาชิกเปลี่ยนอัตราสะสม'));

                if($ArrParam["check_date"] == "true" && $ArrParam["date_start"] != "" && $ArrParam["date_end"] != ""){
                    $sheet->row(2, array('ในช่วงวันที่ ' . get_date_notime($ArrParam["date_start"]) . ' ถึง ' . get_date_notime($ArrParam["date_end"])   ));
                }

                $sheet->row(3, array('รายงานข้อมูล ณ วันที่ ' . get_date_notime(date("Y-m-d H:i:s"))));

                $header[] = null;
                $header[0] = 'รหัสพนักงาน';
                $header[1] = "ชื่อ-นามสกุล";
                $header[2] = "หน่วยงาน";
                $header[3] = "อัตราสะสม(เดิม)";
                $header[4] = "อัตราสะสม(ใหม่)";
                $header[5] = "วันที่ทำรายการ";
                $header[6] = "วันที่มีผล";
                $header[7] = "ผู้ทำรายการ";
                $header[8] = "สถานะ";
                $header[9] = "วันที่พ้นสภาพ";

                $sheet->row(4, $header);
                foreach ($results as $user) {
                    $sheet->appendRow($user);
                }
            });
        })->download('xls');
    }
 

    public function mainReportDatasource($tbl, $policy, $selected_month, $selected_year) {
        $current_year = date("Y");
        $current_month = date("n");
        Log::info(get_class($this) .'::'. __FUNCTION__);
        
          
        $where = '';
        $query = " SELECT " .    
                 "    month(REFERENCE_DATE) as item,  " .
                 "    datename(month, REFERENCE_DATE) [month], " .
                 "    sum(NAV_MB) AS MB,  " . 
                 "    sum(NAV_UNIT) AS NU, " . 
                 "    sum(YIELD_MONTH) AS YM,  " . 
                 "    sum(YIELD_CUMULATIVE) AS YC " . 
                 " FROM ". 
                 "    " . $tbl . " " .
                 " WHERE  ";

                  
        if($selected_month && $selected_year && (strlen($selected_month) > 0) && (strlen($selected_year) >= 2))  {    
            // CASE:1 specified Year and Month            (OK)
            $query = " WITH months(d) as " .
                     " (                  " .
                     "   SELECT dateadd(m, x, -1) " .
                     "   FROM (values(" . $selected_month . ")) x(x) " .
                     " )   ".             
                     " SELECT " . 
                     "   month(m.d) as item,  " .
                     "   datename(month, m.d) [month], " .
                     "   '" . (intval($selected_year) + 543) . "' as yyyy, " .
                     "   isnull(sum(case when year(REFERENCE_DATE) = ". $selected_year . "  then NAV_MB end), 0) as MB, " .
                     "   isnull(sum(case when year(REFERENCE_DATE) = ". $selected_year . "  then NAV_UNIT end), 0) as NU, " .
                     "   isnull(sum(case when year(REFERENCE_DATE) = ". $selected_year . "  then YIELD_MONTH end), 0) as YM, " .
                     "   isnull(sum(case when year(REFERENCE_DATE) = ". $selected_year . "  then YIELD_CUMULATIVE end), 0) as YC " .
                     " FROM " .
                     "   months m " .
                     " LEFT JOIN " .
                     "   " . $tbl . " ts " .
                     " ON    " .
                     "   month(REFERENCE_DATE) = month(m.d) " .
                     " GROUP BY " .
                     "   m.d, datename(month, m.d) " .
                     " ORDER BY " .
                     "   month(m.d) ";
            $where =  " " ;       
            

        } else if((strlen($selected_month) > 0) && (strlen($selected_year) < 2))  {   
            // CASE: 2 : current year and specified month     
            $query = " WITH months(d) as " .
                     " (                  " .
                     "   SELECT dateadd(m, x, -1) " .
                     "   FROM (values(" . $selected_month . ")) x(x) " .
                     " )   ".             
                     " SELECT " . 
                     "   month(m.d) as item,  " .
                     "   datename(month, m.d) [month], " .
                     "   '" . (intval($current_year) + 543)  . "' as yyyy, " .
                     "   isnull(sum(case when year(REFERENCE_DATE) = ". $current_year . "  then NAV_MB end), 0) as MB, " .
                     "   isnull(sum(case when year(REFERENCE_DATE) = ". $current_year . "  then NAV_UNIT end), 0) as NU, " .
                     "   isnull(sum(case when year(REFERENCE_DATE) = ". $current_year . "  then YIELD_MONTH end), 0) as YM, " .
                     "   isnull(sum(case when year(REFERENCE_DATE) = ". $current_year . "  then YIELD_CUMULATIVE end), 0) as YC " .
                     " FROM " .
                     "   months m " .
                     " LEFT JOIN " .
                     "   " . $tbl . " ts " .
                     " ON    " .
                     "   month(REFERENCE_DATE) = month(m.d) " .
                     " GROUP BY " .
                     "   m.d, datename(month, m.d) " .
                     " ORDER BY " .
                     "   month(m.d) ";
            $where =  " " ; 

        } else if((strlen($selected_month) < 1) && (strlen($selected_year) >= 2))  {  
            // CASE: 3 : specified year only     
            $query = " WITH months(d) as " .
                     " (                  " .
                     "   SELECT dateadd(m, x, 0) " .
                     "   FROM (values(1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12)) x(x) " .
                     " )   ".             
                     " SELECT " . 
                     "   month(m.d) as item,  " .
                     "   datename(month, m.d) [month], " .
                     "   '" . (intval($selected_year) + 543) . "' as yyyy, " .
                     "   isnull(sum(case when year(REFERENCE_DATE) = ". $selected_year . "  then NAV_MB end), 0) as MB, " .
                     "   isnull(sum(case when year(REFERENCE_DATE) = ". $selected_year . "  then NAV_UNIT end), 0) as NU, " .
                     "   isnull(sum(case when year(REFERENCE_DATE) = ". $selected_year . "  then YIELD_MONTH end), 0) as YM, " .
                     "   isnull(sum(case when year(REFERENCE_DATE) = ". $selected_year . "  then YIELD_CUMULATIVE end), 0) as YC " .
                     " FROM " .
                     "   months m " .
                     " LEFT JOIN " .
                     "   " . $tbl . " ts " .
                     " ON    " .
                     "   month(REFERENCE_DATE) = month(m.d) " .
                     " GROUP BY " .
                     "   m.d, datename(month, m.d) " .
                     " ORDER BY " .
                     "   month(m.d) ";
            $where =  " " ;                 
        } else { 
            // ELSE:  not specified year/month, we will use current year      

            $query = " WITH months(d) as " .
                     " (                  " .
                     "   SELECT dateadd(m, x, 0) " .
                     "   FROM (values(1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12)) x(x) " .
                     " )   ".             
                     " SELECT " . 
                     "   month(m.d) as item,  " .
                     "   datename(month, m.d) [month], " .
                     "   " . (intval($current_year) + 543) . " as yyyy, " .
                     "   isnull(sum(case when year(REFERENCE_DATE) = ". $current_year . "  then NAV_MB end), 0) as MB, " .
                     "   isnull(sum(case when year(REFERENCE_DATE) = ". $current_year . "  then NAV_UNIT end), 0) as NU, " .
                     "   isnull(sum(case when year(REFERENCE_DATE) = ". $current_year . "  then YIELD_MONTH end), 0) as YM, " .
                     "   isnull(sum(case when year(REFERENCE_DATE) = ". $current_year . "  then YIELD_CUMULATIVE end), 0) as YC " .
                     " FROM " .
                     "   months m " .
                     " LEFT JOIN " .
                     "   " . $tbl ." ts " .
                     " ON    " .
                     "   month(REFERENCE_DATE) = month(m.d) " .
                     " GROUP BY " .
                     "   m.d, datename(month, m.d) " .
                     " ORDER BY " .
                     "   month(m.d) ";
            $where =  " " ;       
                  
        }
        
        $query .= $where; 
        Log::info(get_class($this) .'::'. __FUNCTION__  . ' SQLSTR=' . $query);

        $resultset = DB::select(DB::raw($query));
        return $resultset;  
    }

    public function bondChartDatasource($policy, $selected_month, $selected_year) {
        $current_year = date("Y");
        $current_month = date("n");

        $s_month =  $current_month;
        $s_year=  $current_year;
        
        Log::info(get_class($this) .'::'. __FUNCTION__);
        
        $tbl = 'TBL_P2_BOND_NAV';
        //REFERENCE_DATE    GOV_BOND_RATIO  DEPOSIT_RATIO   DEBENTURE_RATIO OTHER_RATIO
        $where = '';
       /* $query = " SELECT " .    
                 "    sum(GOV_BOND_RATIO)  AS GR,  " . 
                 "    sum(DEPOSIT_RATIO)   AS DR, " . 
                 "    sum(DEBENTURE_RATIO) AS DE,  " . 
                 "    sum(OTHER_RATIO)     AS OT " . 
                 " FROM ". 
                 "    " . $tbl . " " .
                 " WHERE ";
*/
          $query = " SELECT TOP 1 REF_DATE, GR, DR, DE, OT, RowRank " .
                 " FROM ( " .
                 "     SELECT lp.GOV_BOND_RATIO as GR, " . 
                 "        lp.DEPOSIT_RATIO as DR, " . 
                 "        lp.DEBENTURE_RATIO AS DE, " . 
                 "        lp.OTHER_RATIO AS OT, " . 
                 "        lp.REFERENCE_DATE AS REF_DATE, " .
                 "        ROW_NUMBER() OVER (PARTITION BY YEAR(REFERENCE_DATE)  ".
                 "               ORDER BY lp.REFERENCE_DATE DESC) 'RowRank' " .
                 "     FROM " . $tbl . "  lp " .
                 " WHERE ";      

                  
        if($selected_month && $selected_year && (strlen($selected_month) > 0) && (strlen($selected_year) >= 2))  {   
            $where = "    year(REFERENCE_DATE) = " . $selected_year . " AND " .
                     "    month(REFERENCE_DATE) = " . $selected_month . " "  .
                     " ) sub " . 
                     " WHERE RowRank = 1 " ; 
            
        } else if((strlen($selected_month) > 0) && (strlen($selected_year) < 2))  {   
            // CASE: 2 : current year and specified month     
            $where =  "    year(REFERENCE_DATE) = " . $current_year . " AND " .
                      "    month(REFERENCE_DATE) = " . $selected_month . " "  .
                      " ) sub " . 
                      " WHERE RowRank = 1 " ; 

        } else if((strlen($selected_month) < 1) && (strlen($selected_year) >= 2))  {  
            $where = "    year(REFERENCE_DATE) = " . $selected_year . "  " .
                     " ) sub " . 
                     " WHERE RowRank = 1 " ; 
     
        } else { 
            // ELSE:  not specified year/month, we will use current year      
            $where = "    year(REFERENCE_DATE) = " . $current_year . "  " . 
                     " ) sub " . 
                     " WHERE RowRank = 1 " ;   
        }
        
        $query .= $where; 
        Log::info(get_class($this) .'::'. __FUNCTION__  . ' SQLSTR=' . $query);

        $resultset = DB::select(DB::raw($query));
        return $resultset;  
    }
    
     public function equityChartDatasource($policy, $selected_month, $selected_year) {
        $current_year = date("Y");
        $current_month = date("n");
        Log::info(get_class($this) .'::'. __FUNCTION__);
        
        $tbl = 'TBL_P2_EQUITY_NAV';
        
        $where = '';

        $query = " SELECT TOP 1 REF_DATE, DR, ST, OT, RowRank " .
                 " FROM ( " .
                 "     SELECT lp.DEPOSIT_RATIO as DR, " . 
                 "        lp.STOCK_RATIO as ST, " . 
                 "        lp.OTHER_RATIO AS OT, " . 
                 "        lp.REFERENCE_DATE AS REF_DATE, " .
                 "        ROW_NUMBER() OVER (PARTITION BY YEAR(REFERENCE_DATE) ".
                 "               ORDER BY lp.REFERENCE_DATE DESC) 'RowRank' " .
                 "     FROM " . $tbl . "  lp " .
                 " WHERE " ;

                // "     WHERE YEAR(REFERENCE_DATE) = 2016 AND Month(REFERENCE_DATE) = 6 ".
                // " ) sub " . 
                //" WHERE RowRank = 1 " ;

                  
        if($selected_month && $selected_year && (strlen($selected_month) > 0) && (strlen($selected_year) >= 2))  {   
            $where = "    year(REFERENCE_DATE) = " . $selected_year . " AND " .
                     "    month(REFERENCE_DATE) = " . $selected_month . " "  . 
                     " ) sub " . 
                     " WHERE RowRank = 1 " ; 
            $s_year = $selected_year;
            $s_month = $selected_month;
            
        } else if((strlen($selected_month) > 0) && (strlen($selected_year) < 2))  {   
            // CASE: 2 : current year and specified month     
            $where = "    year(REFERENCE_DATE) = " . $current_year . " AND " .
                     "    month(REFERENCE_DATE) = " . $selected_month . " "  .
                     " ) sub " . 
                     " WHERE RowRank = 1 " ; 
            $s_year = $current_year;
            $s_month = $selected_month;

        } else if((strlen($selected_month) < 1) && (strlen($selected_year) >= 2))  {  
            $where = "    year(REFERENCE_DATE) = " . $selected_year . "  " .
                     " ) sub " . 
                     " WHERE RowRank = 1 " ;           
            $s_year = $selected_year;
            $s_month = 0;

     
        } else { 
            // ELSE:  not specified year/month, we will use current year      
            $where = "    year(REFERENCE_DATE) = " . $current_year . "  " .
                     " ) sub " . 
                     " WHERE RowRank = 1 " ;  
            $s_year = $selected_year;
            //$s_month = 0;
        }
        
        $query .= $where; 
        Log::info(get_class($this) .'::'. __FUNCTION__  . ' SQLSTR=' . $query);

        $resultset = DB::select(DB::raw($query));
        return $resultset;  
    }
    
    /**
     * Ajax search and generate report
     *
     * @return json with 
     * @comment This method filled data into 'ajax_p2_equity_nav_report_main.php'
     */
    public function ajax_report_search_main() {

        Log::info(get_class($this) .'::'. __FUNCTION__);

        $view_name = 'backend.pages.ajax.ajax_p2_equity_nav_report_main';
        $plan = Input::get('plan');
        $select_month = Input::get('month');
        $select_year = Input::get('year');
    
        // HTML Render Method
        
        /*
        $UNSPECIFIED                      = 0; // 0 - display TABLE current year and PIE last month 
        $SPECIFIED_YEAR_AND_LAST_MONTH    = 1; // 1 - display TABLE selected year and PIE last month
        $CURRENT_YEAR_AND_SPECIFIED_MONTH = 2; // 2 - display TABLE /PIE with selected month 
        $SPECIFIED_YEAR_AND_MONTH         = 3; // 3 - display PIE chart upon selected year and month
        */

        $method      = self::UNSPECIFIED;
        $table1_show = "display:none;";
        $table2_show = "display:none;";

        if (($select_month == '') && ($select_year=='')) {
           $method = self::UNSPECIFIED;
        
        } else if(($select_month != '') && ($select_year != '')) {
           $method = self::SPECIFIED_YEAR_AND_MONTH;
        
        } else if(($select_month != '') && ($select_year == '')) {
           $method = self::CURRENT_YEAR_AND_SPECIFIED_MONTH;

        } else if(($select_month == '') && ($select_year != '')) {
           $method = self::SPECIFIED_YEAR_AND_LAST_MONTH;
        }

        $table1_show = 'display:none;';
        $table2_show = 'display:none;';
        $tbl1 = 'TBL_P2_BOND_NAV';
        $tbl2 = 'TBL_P2_EQUITY_NAV';

        if($plan =='ตราสารหนี้') {
            $table1_show = 'display:block;';
            $table2_show = 'display:none;';
             
        } else if ($plan =='ตราสารทุน') {
            $table2_show = 'display:block;';
            $table1_show = 'display:none;';
             
        } else {
            $table1_show = 'display:block;';
            $table2_show = 'display:block;';
            // ไม่ระบุ table ดึงทั้ง TBL_P2_BOND_NAV,TBL_P2_EQUITY_NAV
        }

        /////////////////////////
        /// TODO: 
        ///  1. select data from TBL_P2_EQUITY_NAV
        ///  2. select data from TBL_P2_BON_NAV
        ///
        $bondRS   = $this->mainReportDatasource('TBL_P2_BOND_NAV', $plan, $select_month, $select_year);
        $equityRS = $this->mainReportDatasource('TBL_P2_EQUITY_NAV', $plan, $select_month, $select_year);
        
        $bondChart   = $this->bondChartDatasource($plan, $select_month, $select_year);
        $equityChart = $this->equityChartDatasource($plan, $select_month, $select_year);

        Log::info(get_class($this) . '::' . __FUNCTION__ . ':: plan = ' . $plan . ', method = ' . $method . ', select_month = ' . $select_month . ', select_year = '. $select_year);
        $returnHTML = view($view_name)->with([
            'htmlPaginate' => '',
            'data' => array('bond_nav' => $bondRS ,'equity_nav' =>$equityRS),
            'totals' => 1,
            'PageSize' => 1,
            'PageNumber' => 1,
            'plan' => $plan,
            'renderMethod' => $method,
            'table1show' => $table1_show,
            'table2show' => $table2_show,
            'select_year' => $select_year,
            'selected_month' => ($select_month ? $select_month: 0)
        ])->render();

    /*
    "    BOND: sum(GOV_BOND_RATIO)  AS GR,  " . 
                 "    sum(DEPOSIT_RATIO)   AS DR, " . 
                 "    sum(DEBENTURE_RATIO) AS DE,  " . 
                 "    sum(OTHER_RATIO)     AS OT " . 
    

        " EQUITY:    sum(DEPOSIT_RATIO)  AS DR,  " . 
                 "    sum(STOCK_RATIO)    AS ST, " . 
                 "    sum(OTHER_RATIO)    AS OT " . 
                 */
        $pie1 = array(); 
        $pie1_data1 = array();
        $reference_date1 = date('Y-m-d');
        $reference_date2 = date('Y-m-d');

        if ($bondChart && (count($bondChart) > 0 )) {
            $reference_date1 =  getThaiMonth(intval(date('m', strtotime($bondChart[0]->REF_DATE)))) .' ปี '  . (date('Y',strtotime($bondChart[0]->REF_DATE)) +  543);
            $pie1_data1["value"] =  $bondChart[0]->GR;

            $pie1_data2 = array();
            $pie1_data2["value"] =  $bondChart[0]->DR;
            
            $pie1_data3 = array();
            $pie1_data3["value"] =  $bondChart[0]->DE;

            $pie1_data4 = array();
            $pie1_data4["value"] =  $bondChart[0]->OT;
        } else {
            if (($select_month) && ($select_year)) {
                $reference_date1 = getThaiMonth($select_month) .' ปี '  . ($select_year + 543) ;
            }
            else {
                 $reference_date1 = getThaiMonth(intval(date('m'))) . ' ปี ' . (date('Y') + 543);
            }
            $pie1_data1["value"] =  0;

            $pie1_data2 = array();
            $pie1_data2["value"] =  0;
            
            $pie1_data3 = array();
            $pie1_data3["value"] =  0;

            $pie1_data4 = array();
            $pie1_data4["value"] =  0;
        }


        array_push($pie1, $pie1_data1, $pie1_data2, $pie1_data3, $pie1_data4);

        $pie2 = array(); 
        $pie2_data1 = array();
        
        if ($equityChart && (count($equityChart) > 0 )) {
            //$reference_date2 = date('Y-m-d', strtotime($equityChart[0]->REF_DATE));
            $reference_date2 =  getThaiMonth(intval(date('m', strtotime($equityChart[0]->REF_DATE)))) .' ปี '  . (date('Y',strtotime($equityChart[0]->REF_DATE)) +  543);
           
            $pie2_data1["value"] = $equityChart[0]->DR;

            $pie2_data2 = array();
            $pie2_data2["value"] = $equityChart[0]->ST;
            
            $pie2_data3 = array();
            $pie2_data3["value"] = $equityChart[0]->OT;
        } else {
             if (($select_month) && ($select_year)) {
                $reference_date2 = getThaiMonth($select_month) .' ปี '  . ($select_year + 543) ;
            }
            else {
                 $reference_date2 = getThaiMonth(intval(date('m')))  . ' ปี ' . (date('Y') + 543) ;
            }

           $pie2_data1["value"] = 0;

            $pie2_data2 = array();
            $pie2_data2["value"] = 0;
            
            $pie2_data3 = array();
            $pie2_data3["value"] = 0;
        }
        array_push($pie2, $pie2_data1, $pie2_data2, $pie2_data3);

        return response()->json(array('success' => true,
                                      'html' => $returnHTML, 
                                      'pie1' => $pie1,
                                      'pie2' => $pie2,
                                      'plan' => $plan,
                                      'table1show' => $table1_show,
                                      'table2show' => $table2_show,
                                      'reference_date1' => $reference_date1, // ref date for pie1
                                      'reference_date2' => $reference_date2, // ref date for pie2
                                      'renderMethod' => $method));
    }


    public function ajax_report_search_export_main() {

       //  Log::info(get_class($this) .'::'. __FUNCTION__);
        $ArrParam = array();

        $ArrParam["select_year"] =Input::get("select_year");
        $ArrParam["select_month"] =Input::get("select_month");
        $ArrParam["plan"] = Input::get("plan");

       /* $ArrParam = array();
        $ArrParam["pagesize"] ="";
        $ArrParam["PageNumber"] ="";
        $ArrParam["emp_id"] =Input::get("EmpID");
        $ArrParam["depart"] =Input::get("depart");
        $ArrParam["plan"] =Input::get("plan");
        $ArrParam["date_start"] =Input::get("date_start");
        $ArrParam["date_end"] =Input::get("date_end");

        $ArrParam["check_name"] =Input::get("check_name");
        $ArrParam["check_depart"] =Input::get("check_depart");
        $ArrParam["check_plan"] =Input::get("check_plan");
        $ArrParam["check_date"] =Input::get("check_date");
       
        $data = $this->DataSource($ArrParam,true,false);
*/
        $plan = Input::get('plan');
        $select_month = Input::get('select_month');
        $select_year = Input::get('select_year');
        
       
       
        $tbl1 = 'TBL_P2_BOND_NAV';
        $tbl2 = 'TBL_P2_EQUITY_NAV';

        Log::info('ajax_report_search_export_main() :: plan=' . $plan . ' , select_month=' . $select_month . ' , select_year='. $select_year);
        $results = array();
        $results2 = array();
        $bondRS   = $this->mainReportDatasource('TBL_P2_BOND_NAV', $plan, $select_month, $select_year);
        $equityRS = $this->mainReportDatasource('TBL_P2_EQUITY_NAV', $plan, $select_month, $select_year);

        $bondChart   = $this->bondChartDatasource($plan, $select_month, $select_year);
        $equityChart = $this->equityChartDatasource($plan, $select_month, $select_year);

      
        $results = array();
        $ref_date1 = '';
        $ref_date2 = '';
       /* foreach ($bondRS as $item) {
            $results[] = (array)$item;

            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }
        $results2 = array();

        foreach ($equityRS as $item) {
            $results2[] = (array)$item;

            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }
*/
        foreach ($bondRS as $rowset =>$rs) {
            $ref_date1 =  $rs->yyyy;
            break;
        }
        foreach ($equityRS as $rowset =>$rs) {
            $ref_date2 =  $rs->yyyy; //toThaiShortMonth($rs->item) . ' ' .
            break;
        }

        /* PIE Chart */
        $pie1 = array(); 
        $pie1_data1 = array();
        $reference_date1 = date('Y-m-d');
        $reference_date2 = date('Y-m-d');

        if ($bondChart && (count($bondChart) > 0 )) {
            $reference_date1 = getThaiMonth(intval(date('m', strtotime($bondChart[0]->REF_DATE)))) .' ปี '  . (date('Y',strtotime($bondChart[0]->REF_DATE)) +  543);
            $pie1_data1["value"] =  $bondChart[0]->GR;

            $pie1_data2 = array();
            $pie1_data2["value"] =  $bondChart[0]->DR;
            
            $pie1_data3 = array();
            $pie1_data3["value"] =  $bondChart[0]->DE;

            $pie1_data4 = array();
            $pie1_data4["value"] =  $bondChart[0]->OT;

        } else {
            if (($select_month) && ($select_year)) {
                $reference_date1 = getThaiMonth($select_month) .' ปี '  . ($select_year + 543) ;
            }
            else {
                 $reference_date1 = getThaiMonth(intval(date('m'))) . ' ปี ' . (date('Y') + 543);
            }
            $pie1_data1["value"] =  0;

            $pie1_data2 = array();
            $pie1_data2["value"] =  0;
            
            $pie1_data3 = array();
            $pie1_data3["value"] =  0;

            $pie1_data4 = array();
            $pie1_data4["value"] =  0;
        }


        array_push($pie1, $pie1_data1, $pie1_data2, $pie1_data3, $pie1_data4);

        $pie2 = array(); 
        $pie2_data1 = array();
        
        if ($equityChart && (count($equityChart) > 0 )) {
            //$reference_date2 = date('Y-m-d', strtotime($equityChart[0]->REF_DATE));
            $reference_date2 =  getThaiMonth(intval(date('m', strtotime($equityChart[0]->REF_DATE)))) .' ปี '  . (date('Y',strtotime($equityChart[0]->REF_DATE)) +  543);
           
            $pie2_data1["value"] = $equityChart[0]->DR;

            $pie2_data2 = array();
            $pie2_data2["value"] = $equityChart[0]->ST;
            
            $pie2_data3 = array();
            $pie2_data3["value"] = $equityChart[0]->OT;
        } else {
             if (($select_month) && ($select_year)) {
                $reference_date2 = getThaiMonth($select_month) .' ปี '  . ($select_year + 543) ;
            }
            else {
                 $reference_date2 = getThaiMonth(intval(date('m')))  . ' ปี ' . (date('Y') + 543) ;
            }

            $pie2_data1["value"] = 0;

            $pie2_data2 = array();
            $pie2_data2["value"] = 0;
            
            $pie2_data3 = array();
            $pie2_data3["value"] = 0;
        }
        array_push($pie2, $pie2_data1, $pie2_data2, $pie2_data3);

        
        /* create Excel file for download */ 
        return  Excel::create('ExcelExport', function ($excel) use ($bondRS, $equityRS, $ref_date1, $ref_date2, $pie1, $pie2, $ArrParam) {

            /**
             * Sheet ตารางตราสารหนี้
             */
            $excel->sheet('ตารางตราสารหนี้', function ($sheet) use ($bondRS, $ref_date1, $ref_date2, $pie1, $ArrParam) {

                $sheet->setStyle([
                    'borders' => [
                        'allborders' => [
                            'color' => [
                                'rgb' => 'b3b6b7'
                            ]
                        ]
                    ]
                ]);

                $sheet->setFontFamily('Cordia New'); 
                $sheet->setFontSize(18);
                // first row styling and writing content
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:E2');
                $sheet->mergeCells('A3:E3');
                 
                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Cordia New');
                    $row->setFontSize(20);
                    $row->setFontWeight('bold');
                    $row->setAlignment('center');
                    $row->setBackground('#b2babb');
                    // Set all borders (top, right, bottom, left)
                    $row->setBorder('thin', 'thin', 'none', 'thin');

                });

                $sheet->row(2, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'none', 'thin');
                });

                $sheet->row(3, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'thin', 'thin');

                });

                $sheet->row(1, array('รายงาน นโยบายการลงทุนตราสารหนี้'));
                $sheet->row(2, array('รายงานข้อมูล ณ. ' . $ref_date1));
                $sheet->row(3, array('มูลค่าทรัพย์สินสุทธิและอัตราตอบแทน'));
                
                $sheet->mergeCells('A4:A5');
                $sheet->mergeCells('B4:C4');
                $sheet->mergeCells('D4:E4');

                $sheet->cell('A4', function($cell) {

                    // manipulate the cell
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                    // Set all borders (top, right, bottom, left)
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                
                });

                $sheet->row(4, function ($row) {
                    $row->setAlignment('center');
                    
                });

                //$sheet->setBorder('solid', 'none', 'none', 'solid');

                $sheet->cell('B4', function($cell) {
                    $cell->setBackground('#f2f3f4');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
                $sheet->cell('C4', function($cell) {
                    $cell->setBackground('#f2f3f4');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
                $sheet->cell('D4', function($cell) {
                    $cell->setBackground('#f2f3f4');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
                $sheet->cell('E4', function($cell) {
                    $cell->setBackground('#f2f3f4');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $sheet->cell('A5', function($cell) {
                    
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $header0[] = null;
                $header0[0] = 'เดือน';
                $header0[1] = 'มูลค่าทรัพย์สินสุทธิ';
                $header0[2] = '';
                $header0[3] = 'อัตราผลตอบแทน (%)';
                $header0[4] = '';
                $sheet->row(4, $header0);// array('เดือน', 'มูลค่าทรัพย์สินสุทธิ', 'อัตราตอบแทน'));
               
                $sheet->row(5, function ($row) {$row->setAlignment('center');});
                $header[] = null;
                $header[0] = 'เดือน';
                $header[1] = 'ล้านบาท';
                $header[2] = "ต่อหน่วย";
                $header[3] = "รายเดือน";
                $header[4] = "สะสม";
              
                $sheet->row(5, $header);

                // Set multiple column formats
                $sheet->setColumnFormat(array(
                   // 'A' => 'yyyy-mm-dd',
                    'B' => '#,##0.00',
                    'C' => '#,##0.0000',
                    'D' => '#,##0.00',
                    'E' => '0.00',
                ));

               // {{ toThaiShortMonth($rs->item)}} {{$rs->yyyy}}
                foreach ($bondRS as $rows => $rs) {

                    $fields  = [];
                    $fields[0] = toThaiShortMonth($rs->item) . ' ' . $rs->yyyy;
                    $fields[1] = round($rs->MB, 2);
                    $fields[2] = round($rs->NU, 4);
                    $fields[3] = round($rs->YM, 2);
                    $fields[4] = round($rs->YC, 2);
                     
                    $sheet->appendRow($fields);
                }
                 for($i = 5 ; $i< 18; $i++) { 
                     
                    $sheet->setSize('A' . $i, 10, 18);
                    $sheet->setSize('B' . $i, 10, 18);
                    $sheet->setSize('C' . $i, 10, 18);
                    $sheet->setSize('D' . $i, 10, 18);
                    $sheet->setSize('E' . $i, 10, 18);
                    
                    $sheet->cell('A' . $i, function($cell) {
                       $cell->setAlignment('center');
                       
                    });

                    $sheet->row($i, function ($row) {
                        $row->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                }
                // ==================================================================//
                // PIE Chart ตราสารหนี้
                $ls = array('พันธบัตรรัฐบาล ธปท.และรัฐวิสาหกิจ',
                                'เงินฝากและตราสารหนี้ธนาคารพาณิชย์',
                                'หุ้นกู้',
                                'อื่นๆ ลูกหนี้และเจ้าหนี้จากการซื้อ/ ขายหลักทรัพย์');

                $sheet->mergeCells('G4:H4');
                $sheet->setSize('G6', 40, 18);
                $sheet->cell('G4', function($cell) {
                    $cell->setValue('การลงทุนในตราสารทุน');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#b2babb');
                    $cell->setFontWeight('bold');
                    $cell->setFontSize(20);
                });
                $sheet->cell('H4', function($cell) {
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                $sheet->cell('G5', function($cell) {
                    $cell->setValue('ประเภท');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                $sheet->setSize('H5', 15, 18);
                $sheet->cell('H5', function($cell) {
                    $cell->setValue('อัตราร้อยละ (%)');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                for($i = 0; $i < count($ls); $i++) {
                    $sheet->cell('G'. ($i + 6), function($cell) use($ls, $i) {
                        $cell->setValue($ls[$i]);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell('H'. ($i + 6), function($cell) use($ls, $i, $pie1) {
                        $cell->setValue( $pie1[$i]['value']);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                }



            });

            /**
             * Sheet ตารางตราสารทุน
             */
            $excel->sheet('ตารางตราสารทุน', function ($sheet) use ($equityRS, $ref_date1, $ref_date2, $pie2, $ArrParam) {
                $sheet->setStyle([
                    'borders' => [
                        'allborders' => [
                            'color' => [
                                'rgb' => 'b3b6b7'
                            ]
                        ]
                    ]
                ]);

                $sheet->setFontFamily('Cordia New'); 
                $sheet->setFontSize(18);
                // first row styling and writing content
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:E2');
                $sheet->mergeCells('A3:E3');
                 
                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Cordia New');
                    $row->setFontSize(20);
                    $row->setFontWeight('bold');
                    $row->setAlignment('center');
                    $row->setBackground('#b2babb');
                    // Set all borders (top, right, bottom, left)
                    $row->setBorder('thin', 'thin', 'none', 'thin');
                });

                $sheet->row(2, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'none', 'thin');
                });

                $sheet->row(3, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'thin', 'thin');

                });

                $sheet->row(1, array('รายงาน นโยบายการลงทุนตราสารทุน'));
                $sheet->row(2, array('รายงานข้อมูล ณ. ' . $ref_date1));
                $sheet->row(3, array('มูลค่าทรัพย์สินสุทธิและอัตราตอบแทน'));
                
                $sheet->mergeCells('A4:A5');
                $sheet->mergeCells('B4:C4');
                $sheet->mergeCells('D4:E4');

                $sheet->cell('A4', function($cell) {

                    // manipulate the cell
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                    // Set all borders (top, right, bottom, left)
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                
                });

                $sheet->row(4, function ($row) {
                    $row->setAlignment('center');
                    
                });

                //$sheet->setBorder('solid', 'none', 'none', 'solid');

                $sheet->cell('B4', function($cell) {
                    $cell->setBackground('#f2f3f4');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
                $sheet->cell('C4', function($cell) {
                    $cell->setBackground('#f2f3f4');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
                $sheet->cell('D4', function($cell) {
                    $cell->setBackground('#f2f3f4');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });
                $sheet->cell('E4', function($cell) {
                    $cell->setBackground('#f2f3f4');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $sheet->cell('A5', function($cell) {
                    
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                });

                $header0[] = null;
                $header0[0] = 'เดือน';
                $header0[1] = 'มูลค่าทรัพย์สินสุทธิ';
                $header0[2] = '';
                $header0[3] = 'อัตราผลตอบแทน (%)';
                $header0[4] = '';
                $sheet->row(4, $header0);
               
                $sheet->row(5, function ($row) {$row->setAlignment('center');});
                $header[] = null;
                $header[0] = 'เดือน';
                $header[1] = 'ล้านบาท';
                $header[2] = "ต่อหน่วย";
                $header[3] = "รายเดือน";
                $header[4] = "สะสม";
              
                $sheet->row(5, $header);

                // Set multiple column formats
                $sheet->setColumnFormat(array(
                   // 'A' => 'yyyy-mm-dd',
                    'B' => '#,##0.00',
                    'C' => '#,##0.0000',
                    'D' => '#,##0.00',
                    'E' => '0.00',
                    'H' => '0.00'
                ));

               // {{ toThaiShortMonth($rs->item)}} {{$rs->yyyy}}
                foreach ($equityRS as $rows => $rs) {

                    $fields  = [];
                    $fields[0] = toThaiShortMonth($rs->item) . ' ' . $rs->yyyy;
                    $fields[1] = round($rs->MB, 2);
                    $fields[2] = round($rs->NU, 4);
                    $fields[3] = round($rs->YM, 2);
                    $fields[4] = round($rs->YC, 2);
                     
                    $sheet->appendRow($fields);
                }
                 for($i = 5 ; $i< 18; $i++) { 
                     
                    $sheet->setSize('A' . $i, 10, 18);
                    $sheet->setSize('B' . $i, 10, 18);
                    $sheet->setSize('C' . $i, 10, 18);
                    $sheet->setSize('D' . $i, 10, 18);
                    $sheet->setSize('E' . $i, 10, 18);
                    
                    $sheet->cell('A' . $i, function($cell) {
                       $cell->setAlignment('center');
                       
                    });

                    $sheet->row($i, function ($row) {
                        $row->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                }
/*

        var label1 = ['พันธบัตรรัฐบาล ธปท.และรัฐวิสาหกิจ',
                      'เงินฝากและตราสารหนี้ธนาตารพาณิชย์',
                      'หุ้นกู้',
                      'อื่นๆ ลูกหนี้และเจ้าหนี้จากการซื้อ/ ขายหลักทรัพย์'];

        var label2 = [// 'พันธบัตรรัฐบาล ธปท.และรัฐวิสาหกิจ',
                      'เงินฝากธนาคารพาณิชย์', //'เงินฝากและตราสารหนี้ธนาตารพาณิชย์',
                      'ตราสารทุน(หุ้น)',
                      'อื่นๆ ลูกหนี้และเจ้าหนี้จากการซื้อ/ ขายหลักทรัพย์'];
*/
                // ==================================================================//
                // PIE chart ตาสารทุน
                $ls = array( 'เงินฝากธนาคารพาณิชย์',//'พันธบัตรรัฐบาล ธปท.และรัฐวิสาหกิจ',
                             'ตราสารทุน(หุ้น)',
                             'อื่นๆ ลูกหนี้และเจ้าหนี้จากการซื้อ/ ขายหลักทรัพย์');
                $sheet->mergeCells('G4:H4');
                $sheet->setSize('G6', 40, 18);
                $sheet->cell('G4', function($cell) {
                    $cell->setValue('การลงทุนในตราสารทุน');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#b2babb');
                    $cell->setFontWeight('bold');
                    $cell->setFontSize(20);
                });
                $sheet->cell('H4', function($cell) {
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                $sheet->cell('G5', function($cell) {
                    $cell->setValue('ประเภท');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                $sheet->setSize('H5', 15, 18);
                $sheet->cell('H5', function($cell) {
                    $cell->setValue('อัตราร้อยละ (%)');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                for($i = 0; $i < count($ls); $i++) {
                    $sheet->cell('G'. ($i + 6), function($cell) use($ls, $i) {
                        $cell->setValue($ls[$i]);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell('H'. ($i + 6), function($cell) use($ls, $i, $pie2) {
                        $cell->setValue( $pie2[$i]['value']);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                }


            });

        })->download('xls');
    }

     /**
     * Details Report  & Export
     */
    public function detailsReportDatasource($tbl, $policy, $selected_month, $selected_year) {
        $current_year = date("Y");
        $current_month = date("n");
        Log::info(get_class($this) .'::'. __FUNCTION__);
        
          
        $where = '';

        $query = "";
        if($tbl == "TBL_P2_BOND_NAV") {
            $query = " SELECT " . 
                     " year(REFERENCE_DATE)+543 as YYYY , " .   
                     "    REFERENCE_DATE, " .
                     "    day(REFERENCE_DATE) as DD, ".
                     "    month(REFERENCE_DATE) as MM, " .
                     "    month(REFERENCE_DATE) as item,  " .
                     "    datename(month, REFERENCE_DATE) [month], " .
                     "        GOV_BOND_RATIO , " .
                     "        GOV_BOND_MB, " .
                     "        GOV_BOND_B," .
                     "        DEPOSIT_RATIO," .
                     "        DEPOSIT_MB," .
                     "        DEPOSIT_B," .
                     "        DEBENTURE_RATIO," .
                     "        DEBENTURE_MB," .
                     "        DEBENTURE_B," .
                     "        OTHER_RATIO," .
                     "        OTHER_MB," .
                     "        OTHER_B," .
                     "        (GOV_BOND_RATIO + DEPOSIT_RATIO + DEBENTURE_RATIO + OTHER_RATIO) as SUM_RATIO, " .
                     "        (GOV_BOND_MB + DEPOSIT_MB + DEBENTURE_MB + OTHER_MB) as SUM_MB, " . 
                     "        (GOV_BOND_B + DEPOSIT_B + DEBENTURE_B + OTHER_B) AS SUM_B, " .
                     "        NAV_MB," .
                     "        NAV_B , ".
                     "        NAV_UNIT, ".
                     "        YIELD_MONTH as YM,".
                     "        YIELD_CUMULATIVE as YC".
                     " FROM " . 
                     "      " . "TBL_P2_BOND_NAV" . " " .
                     " WHERE  " .
                     "      month(REFERENCE_DATE) = " . $selected_month . " " .
                     "      AND year(REFERENCE_DATE) = " .  $selected_year;
        } else {
            $query = " SELECT " .
                    " year(REFERENCE_DATE)+543 as YYYY , " .
                    "    REFERENCE_DATE, " .
                    "    day(REFERENCE_DATE) as DD, ".
                    "    month(REFERENCE_DATE) as MM, " .
                    "    DEPOSIT_RATIO, " .
                    "    DEPOSIT_MB, " .
                    "    DEPOSIT_B, " .
                    "    STOCK_RATIO, " .
                    "    STOCK_MB, " .
                    "    STOCK_B, " .
                    "    OTHER_RATIO, " .
                    "    OTHER_MB, " .
                    "    OTHER_B, " .
                    "     ".
                    "    (DEPOSIT_RATIO + STOCK_RATIO + OTHER_RATIO) as SUM_RATIO, ".
                    "    (DEPOSIT_MB + STOCK_MB + OTHER_MB) as SUM_MB, " .
                    "    (DEPOSIT_B + STOCK_B + OTHER_B) as SUM_B, " .
                    "    NAV_MB, " .
                    "    NAV_B, " .
                    "    NAV_UNIT, " .
                    "    YIELD_MONTH AS YM," .
                    "    YIELD_CUMULATIVE as YC".
                    " FROM " . 
                    "      " . "TBL_P2_EQUITY_NAV" . " " .
                    " WHERE  " . 
                    "      month(REFERENCE_DATE) = " . $selected_month . " " .
                    "      AND year(REFERENCE_DATE) = " .  $selected_year;
        }
        Log::info(get_class($this) .'::'. __FUNCTION__  . ' SQLSTR=' . $query);

        $resultset = DB::select(DB::raw($query));
        return $resultset;  
    }

   
    /**
     * Ajax search and generate details report
     *
     * @return json with 
     * @comment This method filled data into 'ajax_p2_equity_nav_report_details.php'
     */
    public function ajax_report_details_search() {

        $view_name    = 'backend.pages.ajax.ajax_p2_equity_nav_report_details';
        /*$plan         = Input::get('plan');
        $select_month = Input::get('select_month');
        $select_year  = Input::get('select_yearß');
        */
        $plan         = Input::get('plan');
        $select_month = Input::get('month');
        $select_year  = Input::get('year');
        

        Log::info(get_class($this) .'::'. __FUNCTION__ . ' \n' . ' plan= ' . $plan . '\n month=' .  $select_month . '\n year=' . $select_year);
        // HTML Render Method
        
        /*
        $UNSPECIFIED                      = 0; // 0 - display TABLE current year and PIE last month 
        $SPECIFIED_YEAR_AND_LAST_MONTH    = 1; // 1 - display TABLE selected year and PIE last month
        $CURRENT_YEAR_AND_SPECIFIED_MONTH = 2; // 2 - display TABLE /PIE with selected month 
        $SPECIFIED_YEAR_AND_MONTH         = 3; // 3 - display PIE chart upon selected year and month
        */

        $method      = self::UNSPECIFIED;
        $table1_show = "display:none;";
        $table2_show = "display:none;";

        if (($select_month == '') && ($select_year=='')) {
           $method = self::UNSPECIFIED;
        
        } else if(($select_month != '') && ($select_year != '')) {
           $method = self::SPECIFIED_YEAR_AND_MONTH;
        
        } else if(($select_month != '') && ($select_year == '')) {
           $method = self::CURRENT_YEAR_AND_SPECIFIED_MONTH;

        } else if(($select_month == '') && ($select_year != '')) {
           $method = self::SPECIFIED_YEAR_AND_LAST_MONTH;
        }

        $table1_show = 'display:none;';
        $table2_show = 'display:none;';
        $tbl1 = 'TBL_P2_BOND_NAV';
        $tbl2 = 'TBL_P2_EQUITY_NAV';

        if($plan =='ตราสารหนี้') {
            $table3_show = 'display:block;';
            $table4_show = 'display:none;';
             
        } else if ($plan =='ตราสารทุน') {
            $table4_show = 'display:block;';
            $table3_show = 'display:none;';
             
        } else {
            $table3_show = 'display:block;';
            $table4_show = 'display:block;';
        }

        
        $bondRS   = $this->detailsReportDatasource('TBL_P2_BOND_NAV', $plan, $select_month, $select_year);
        $equityRS = $this->detailsReportDatasource('TBL_P2_EQUITY_NAV', $plan, $select_month, $select_year);
        
        $bondChart   = $this->bondChartDatasource($plan, $select_month, $select_year);
        $equityChart = $this->equityChartDatasource($plan, $select_month, $select_year);

        Log::info(get_class($this) . '::' . __FUNCTION__ . ':: plan = ' . $plan . ', method = ' . $method . ', select_month = ' . $select_month . ', select_year = '. $select_year);
        $returnHTML = view($view_name)->with([
            'htmlPaginate' => '',
            'data' => array('bond_nav' => $bondRS ,'equity_nav' =>$equityRS),
            'totals' => 1,
            'PageSize' => 1,
            'PageNumber' => 1,
            'plan' => $plan,
            'renderMethod' => $method,
            'table3show' => $table3_show,
            'table4show' => $table4_show,
            'selected_year' => $select_year,
            'selected_month' => ($select_month ? $select_month: 0)
        ])->render();

    
        $pie1 = array(); 
        $pie1_data1 = array();
        $reference_date1 = date('Y-m-d');
        $reference_date2 = date('Y-m-d');

        if ($bondChart && (count($bondChart) > 0 )) {
            $reference_date1 =  getThaiMonth(intval(date('m', strtotime($bondChart[0]->REF_DATE)))) .' ปี '  . (date('Y',strtotime($bondChart[0]->REF_DATE)) +  543);
            $pie1_data1["value"] =  $bondChart[0]->GR;

            $pie1_data2 = array();
            $pie1_data2["value"] =  $bondChart[0]->DR;
            
            $pie1_data3 = array();
            $pie1_data3["value"] =  $bondChart[0]->DE;

            $pie1_data4 = array();
            $pie1_data4["value"] =  $bondChart[0]->OT;
        } else {
            if (($select_month) && ($select_year)) {
                $reference_date1 = getThaiMonth($select_month) .' ปี '  . ($select_year + 543) ;
            }
            else {
                 $reference_date1 = getThaiMonth(intval(date('m'))) . ' ปี ' . (date('Y') + 543);
            }
            $pie1_data1["value"] =  0;

            $pie1_data2 = array();
            $pie1_data2["value"] =  0;
            
            $pie1_data3 = array();
            $pie1_data3["value"] =  0;

            $pie1_data4 = array();
            $pie1_data4["value"] =  0;
        }


        array_push($pie1, $pie1_data1, $pie1_data2, $pie1_data3, $pie1_data4);

        $pie2 = array(); 
        $pie2_data1 = array();
        
        if ($equityChart && (count($equityChart) > 0 )) {
            //$reference_date2 = date('Y-m-d', strtotime($equityChart[0]->REF_DATE));
            $reference_date2 =  getThaiMonth(intval(date('m', strtotime($equityChart[0]->REF_DATE)))) .' ปี '  . (date('Y',strtotime($equityChart[0]->REF_DATE)) +  543);
           
            $pie2_data1["value"] = $equityChart[0]->DR;

            $pie2_data2 = array();
            $pie2_data2["value"] = $equityChart[0]->ST;
            
            $pie2_data3 = array();
            $pie2_data3["value"] = $equityChart[0]->OT;
        } else {
             if (($select_month) && ($select_year)) {
                $reference_date2 = getThaiMonth($select_month) .' ปี '  . ($select_year + 543) ;
            }
            else {
                 $reference_date2 = getThaiMonth(intval(date('m')))  . ' ปี ' . (date('Y') + 543) ;
            }

            $pie2_data1["value"] = 0;

            $pie2_data2 = array();
            $pie2_data2["value"] = 0;
            
            $pie2_data3 = array();
            $pie2_data3["value"] = 0;
        }
        array_push($pie2, $pie2_data1, $pie2_data2, $pie2_data3);

        return response()->json(array('success' => true,
                                      'html' => $returnHTML, 
                                      'pie1' => $pie1,
                                      'pie2' => $pie2,
                                      'plan' => $plan,
                                      'table3show' => $table1_show,
                                      'table4show' => $table2_show,
                                      'reference_date1' => $reference_date1, // ref date for pie1
                                      'reference_date2' => $reference_date2, // ref date for pie2
                                      'selected_year' => $select_year,
                                      'selected_month' => ($select_month ? $select_month: 0),
                                      'renderMethod' => $method));


    } // 

    // Export details to file
    /**
     * Ajax search and export details report
     *
     * @return json with 
     * @comment This method filled data into 'ajax_p2_equity_nav_report_details.php'
     */
    public function ajax_report_search_export_details() {

        $view_name    = 'backend.pages.ajax.ajax_p2_equity_nav_report_details';
        $plan         = Input::get('plan');
        $select_month = Input::get('select_month');
        $select_year  = Input::get('select_year');
        
        Log::info(get_class($this) .'::'. __FUNCTION__ . ' \n' . ' plan= ' . $plan . '\n month=' .  $select_month . '\n year=' . $select_year);
        
        /*
        $UNSPECIFIED                      = 0; // 0 - display TABLE current year and PIE last month 
        $SPECIFIED_YEAR_AND_LAST_MONTH    = 1; // 1 - display TABLE selected year and PIE last month
        $CURRENT_YEAR_AND_SPECIFIED_MONTH = 2; // 2 - display TABLE /PIE with selected month 
        $SPECIFIED_YEAR_AND_MONTH         = 3; // 3 - display PIE chart upon selected year and month
        */

        $method      = self::UNSPECIFIED;
        $table1_show = "display:none;";
        $table2_show = "display:none;";

        if (($select_month == '') && ($select_year=='')) {
           $method = self::UNSPECIFIED;
        
        } else if(($select_month != '') && ($select_year != '')) {
           $method = self::SPECIFIED_YEAR_AND_MONTH;
        
        } else if(($select_month != '') && ($select_year == '')) {
           $method = self::CURRENT_YEAR_AND_SPECIFIED_MONTH;

        } else if(($select_month == '') && ($select_year != '')) {
           $method = self::SPECIFIED_YEAR_AND_LAST_MONTH;
        }

        $table1_show = 'display:none;';
        $table2_show = 'display:none;';
        $tbl1 = 'TBL_P2_BOND_NAV';
        $tbl2 = 'TBL_P2_EQUITY_NAV';

        if($plan =='ตราสารหนี้') {
            $table3_show = 'display:block;';
            $table4_show = 'display:none;';
             
        } else if ($plan =='ตราสารทุน') {
            $table4_show = 'display:block;';
            $table3_show = 'display:none;';
             
        } else {
            $table3_show = 'display:block;';
            $table4_show = 'display:block;';
        }

        Log::info(get_class($this) . '::' . __FUNCTION__ . ':: plan = ' . $plan . ', method = ' . $method . ', select_month = ' . $select_month . ', select_year = '. $select_year);
      
        $bondRS   = $this->detailsReportDatasource('TBL_P2_BOND_NAV', 'ตราสารหนี้', $select_month, $select_year);
        $equityRS = $this->detailsReportDatasource('TBL_P2_EQUITY_NAV', 'ตราสารทุน', $select_month, $select_year);
        

        $bondChart   = $this->bondChartDatasource('ตราสารหนี้', $select_month, $select_year);
        $equityChart = $this->equityChartDatasource('ตราสารทุน', $select_month, $select_year);

         $returnHTML = view($view_name)->with([
            'htmlPaginate' => '',
            'data' => array('bond_nav' => $bondRS ,'equity_nav' =>$equityRS),
            'totals' => 1,
            'PageSize' => 1,
            'PageNumber' => 1,
            'plan' => $plan,
            'renderMethod' => $method,
            'table3show' => $table3_show,
            'table4show' => $table4_show,
            'selected_year' => $select_year,
            'selected_month' => ($select_month ? $select_month: 0)
        ])->render();

    
        $pie1 = array(); 
        $pie1_data1 = array();
        $reference_date1 = date('Y-m-d');
        $reference_date2 = date('Y-m-d');

        if ($bondChart && (count($bondChart) > 0 )) {
            $reference_date1 =  getThaiMonth(intval(date('m', strtotime($bondChart[0]->REF_DATE)))) .' ปี '  . (date('Y',strtotime($bondChart[0]->REF_DATE)) +  543);
            $pie1_data1["value"] =  $bondChart[0]->GR;

            $pie1_data2 = array();
            $pie1_data2["value"] =  $bondChart[0]->DR;
            
            $pie1_data3 = array();
            $pie1_data3["value"] =  $bondChart[0]->DE;

            $pie1_data4 = array();
            $pie1_data4["value"] =  $bondChart[0]->OT;
        } else {
            if (($select_month) && ($select_year)) {
                $reference_date1 = getThaiMonth($select_month) .' ปี '  . ($select_year + 543) ;
            }
            else {
                 $reference_date1 = getThaiMonth(intval(date('m'))) . ' ปี ' . (date('Y') + 543);
            }
            $pie1_data1["value"] =  0;

            $pie1_data2 = array();
            $pie1_data2["value"] =  0;
            
            $pie1_data3 = array();
            $pie1_data3["value"] =  0;

            $pie1_data4 = array();
            $pie1_data4["value"] =  0;
        }


        array_push($pie1, $pie1_data1, $pie1_data2, $pie1_data3, $pie1_data4);

        $pie2 = array(); 
        $pie2_data1 = array();
        
        if ($equityChart && (count($equityChart) > 0 )) {
            //$reference_date2 = date('Y-m-d', strtotime($equityChart[0]->REF_DATE));
            $reference_date2 =  getThaiMonth(intval(date('m', strtotime($equityChart[0]->REF_DATE)))) .' ปี '  . (date('Y',strtotime($equityChart[0]->REF_DATE)) +  543);
           
            $pie2_data1["value"] = $equityChart[0]->DR;

            $pie2_data2 = array();
            $pie2_data2["value"] = $equityChart[0]->ST;
            
            $pie2_data3 = array();
            $pie2_data3["value"] = $equityChart[0]->OT;
        } else {
             if (($select_month) && ($select_year)) {
                $reference_date2 = getThaiMonth($select_month) .' ปี '  . ($select_year + 543) ;
            }
            else {
                 $reference_date2 = getThaiMonth(intval(date('m')))  . ' ปี ' . (date('Y') + 543) ;
            }

            $pie2_data1["value"] = 0;

            $pie2_data2 = array();
            $pie2_data2["value"] = 0;
            
            $pie2_data3 = array();
            $pie2_data3["value"] = 0;
        }
        array_push($pie2, $pie2_data1, $pie2_data2, $pie2_data3);


        $results = array();
        $ref_date1 = '';
        $ref_date2 = '';
       /* foreach ($bondRS as $item) {
            $results[] = (array)$item;

            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }
        $results2 = array();

        foreach ($equityRS as $item) {
            $results2[] = (array)$item;

            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }
*/
        //foreach ($bondRS as $rowset =>$rs) {
            $ref_date1 =  $reference_date1; //$select_year;
            $ref_date2 =  $reference_date2; //$select_year;
        //    break;
        //}
        //foreach ($equityRS as $rowset =>$rs) {
             //toThaiShortMonth($rs->item) . ' ' .
         //   break;
       // }

      /*  return response()->json(array('success' => true,
                                      'html' => $returnHTML, 
                                      'pie1' => $pie1,
                                      'pie2' => $pie2,
                                      'plan' => $plan,
                                      'table3show' => $table1_show,
                                      'table4show' => $table2_show,
                                      'reference_date1' => $reference_date1, // ref date for pie1
                                      'reference_date2' => $reference_date2, // ref date for pie2
                                      'renderMethod' => $method)); */
        
        /* create Excel file for download */ 
        return  Excel::create('นโยบายการลงทุน' . $plan, function ($excel) use ($plan, $bondRS, $equityRS, $ref_date1, $ref_date2, $pie1, $pie2) {
        
            if($plan == 'ตราสารหนี้') {
               $excel->sheet('นโยบายการลงทุน' . $plan, function ($sheet) use ($bondRS, $ref_date1, $ref_date2, $pie1) {

                    $sheet->setStyle([
                        'borders' => [
                            'allborders' => [
                                'color' => [
                                    'rgb' => 'b3b6b7'
                                ]
                            ]
                        ]
                    ]);

                $sheet->setFontFamily('Cordia New'); 
                $sheet->setFontSize(18);
                // first row styling and writing content
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
               
                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Cordia New');
                    $row->setFontSize(20);
                    $row->setFontWeight('bold');
                    $row->setAlignment('center');
                    $row->setBackground('#b2babb');
                    $row->setBorder('thin', 'thin', 'none', 'thin');
                });

                $sheet->row(2, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'none', 'thin');
                });

                foreach ($bondRS as $rowset => $rs) {
                    $datadate = intval($rs->DD) . " " . toThaiShortMonth($rs->MM) . " " . $rs->YYYY;
                    $sheet->row(1, array('การลงทุนตราสารหนี้'));
                    $sheet->row(2, array('การลงทุน ณ. ' . $datadate));
                    
                    $sheet->mergeCells('A3:A4');
                    $sheet->mergeCells('B3:D3');

                    $header0[ ] = null;
                    $header0[0] = "ประเภทตราสาร";
                    $header0[1] = "นโยบายการลงทุนตราสารหนี้";
                    $header0[2] = "";
                    $header0[3] = "";                     
                    $sheet->row(3, $header0);

                    $sheet->row(4, function ($row) { $row->setAlignment('center'); });
                    $header[] = null;
                    $header[0] = "";
                    $header[1] = "สัดส่วน (%)";
                    $header[2] = "ล้าน";
                    $header[3] = "  บาท  ";
                    $sheet->row(4, $header);

                    // Set multiple column formats
                    $sheet->setColumnFormat(array(
                        'A' => '@',
                        'B' => '0.00',
                        'C' => '#,##0.00',
                        'D' => '#,##0.00'
                    ));

                    // Set width for multiple cells
                    $sheet->setWidth(array(
                        'A'  => 40,
                        'B'  => 15,
                        'C'  => 15,
                        'D'  => 15,
                        'E'  => 5,
                        'F'  => 5
                    ));

                    $fields  = [];
                    $fields[0] = "พันธบัตรรัฐบาล ธปท.และรัฐวิสาหกิจ";
                    $fields[1] = $rs->GOV_BOND_RATIO;
                    $fields[2] = $rs->GOV_BOND_MB;
                    $fields[3] = $rs->GOV_BOND_B;
                    $sheet->appendRow($fields);
                    $sheet->row(5, function ($row) {
                        $row->setAlignment('right');
                    });
                    
                    $fields[0] = "เงินฝากตราสารหุ้นธนาคารพาณิชย์";
                    $fields[1] = $rs->DEPOSIT_RATIO;
                    $fields[2] = $rs->DEPOSIT_MB;
                    $fields[3] = $rs->DEPOSIT_B;
                    $sheet->appendRow($fields);
                    $sheet->row(6, function ($row) {
                        $row->setAlignment('right');
                    });
                    
                    $fields[0] = "หุ้นกู้";
                    $fields[1] = $rs->DEBENTURE_RATIO;
                    $fields[2] = $rs->DEBENTURE_MB;
                    $fields[3] = $rs->DEBENTURE_B;
                    $sheet->appendRow($fields);
                    $sheet->row(7, function ($row) {
                        $row->setAlignment('right');
                    });

                    $fields[0] = "อื่นๆ (ลูกหนี้และเจ้าหนี้จากการซื้อ-ขายหลักทรัพย์)";
                    $fields[1] = $rs->OTHER_RATIO;
                    $fields[2] = $rs->OTHER_MB;
                    $fields[3] = $rs->OTHER_B;
                    $sheet->appendRow($fields);
                    $sheet->row(8, function ($row) {
                        $row->setAlignment('right');
                    });

                    
                    $fields[0] = "ได้แก่ลูกหนี้จากการซื้อ / ขายหลักทรัพย์ / เงินปันผลลูกหนี้ / เจ้าหนี้อื่นๆ และเงินสมทบรอการจัดสรร";
                    $fields[1] = "";
                    $fields[2] = "";
                    $fields[3] = "";
                    

                    $sheet->appendRow($fields);
                     $sheet->setColumnFormat(array(
                        'A' => '@',
                        'B' => '@',
                        'C' => '@',
                        'D' => '@'
                    )); 
                    $sheet->row(9, function ($row) {
                        $row->setFontFamily('Cordia New');
                        $row->setFontSize(12);
                        $row->setFontWeight('italic');
                        $row->setAlignment('left');
                        $row->setFontColor('#a5a5a5');
                        //$row->setBorder('thin', 'thin', 'none', 'thin');
                    });

                        
                    $sheet->setColumnFormat(array(
                        'A' => '@', 
                        'B' => '0.00',
                        'C' => '#,##0.00',
                        'D' => '#,##0.00'
                    ));

                    $fields[0] = "รวม";
                    $fields[1] = $rs->SUM_RATIO;
                    $fields[2] = $rs->SUM_MB;
                    $fields[3] = $rs->SUM_B;
                    $sheet->appendRow($fields);
                    $sheet->row(10, function ($row) {
                        $row->setAlignment('right');
                    });  
                    
                    $fields[0] = "มูลค่าสินทรัพย์สุทธิ";
                    $fields[1] = "";
                    $fields[2] = $rs->NAV_MB;
                    $fields[3] = $rs->NAV_B;
                    $sheet->appendRow($fields);
                    $sheet->row(11, function ($row) {
                        $row->setAlignment('right');
                    });  


                    $fields[0] = "มูลค่าสินทรัพย์สุทธิต่อหน่วย";
                    $fields[1] = "";
                    $fields[2] = "";
                    $fields[3] = $rs->NAV_UNIT; 
                    $sheet->appendRow($fields);
                    $sheet->row(12, function ($row) {
                        $row->setAlignment('right');
                    });  
                    


                    $fields[0] = "อัตราผลตอบแทนรายเดือน %";
                    $fields[1] = "";
                    $fields[2] = "";
                    $fields[3] = $rs->YM;
                    $sheet->appendRow($fields);
                    $sheet->row(13, function ($row) {
                        $row->setAlignment('right');
                    });  


                    $fields[0] = "อัตราผลตอบแทนสะสม %";
                    $fields[1] = "";
                    $fields[2] = "";
                    $fields[3] = $rs->YC;
                    $sheet->appendRow($fields);
                    $sheet->row(14, function ($row) {
                        $row->setAlignment('right');
                    });  


                



                    // needed only one record
                    break;
                }


                $sheet->cell('A3', function($cell) {
                         
                        $cell->setAlignment('center');
                    
                });
                $sheet->cell('B3', function($cell) {
                         
                        $cell->setAlignment('center');
                    
                });
                for($i = 5; $i < 10; $i++) { 
                    $sheet->cell('A' . $i, function($cell) {
                         
                        $cell->setAlignment('left');

                    });
                }

                for($i = 10; $i < 15; $i++) { 
                    $sheet->cell('A' . $i, function($cell) {
                         
                        $cell->setAlignment('center');

                    });
                }
                /////////////////////////
                // ==================================================================//
                // PIE Chart ตราสารหนี้
                $ls = array('พันธบัตรรัฐบาล ธปท.และรัฐวิสาหกิจ',
                                'เงินฝากและตราสารหนี้ธนาคารพาณิชย์',
                                'หุ้นกู้',
                                'อื่นๆ ลูกหนี้และเจ้าหนี้จากการซื้อ/ ขายหลักทรัพย์');

                $sheet->mergeCells('G4:H4');
                $sheet->setSize('G6', 40, 18);
                $sheet->cell('G4', function($cell) {
                    $cell->setValue('การลงทุนในตราสารหนี้');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#b2babb');
                    $cell->setFontWeight('bold');
                    $cell->setFontSize(20);
                });
                $sheet->cell('H4', function($cell) {
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                $sheet->cell('G5', function($cell) {
                    $cell->setValue('ประเภท');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                $sheet->setSize('H5', 15, 18);
                $sheet->cell('H5', function($cell) {
                    $cell->setValue('อัตราร้อยละ (%)');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                for($i = 0; $i < count($ls); $i++) {
                    $sheet->cell('G'. ($i + 6), function($cell) use($ls, $i) {
                        $cell->setValue($ls[$i]);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell('H'. ($i + 6), function($cell) use($ls, $i, $pie1) {
                        $cell->setValue( $pie1[$i]['value']);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                }

               });
            } // end if 'ตราสารหนี้'


            if($plan == 'ตราสารทุน') {  
                $excel->sheet('นโยบายการลงทุน'. $plan, function ($sheet) use ($equityRS, $ref_date1, $ref_date2, $pie2) {
                    $sheet->setStyle([
                        'borders' => [
                            'allborders' => [
                                'color' => [
                                    'rgb' => 'b3b6b7'
                                ]
                            ]
                        ]
                    ]);

                $sheet->setFontFamily('Cordia New'); 
                $sheet->setFontSize(18);

                
                $sheet->mergeCells('A1:D1');
                $sheet->mergeCells('A2:D2');
               
                $sheet->row(1, function ($row) {
                    $row->setFontFamily('Cordia New');
                    $row->setFontSize(20);
                    $row->setFontWeight('bold');
                    $row->setAlignment('center');
                    $row->setBackground('#b2babb');
                    $row->setBorder('thin', 'thin', 'none', 'thin');
                });

                $sheet->row(2, function ($row) { 
                    $row->setAlignment('center');
                    $row->setBackground('#e5e8e8');
                    $row->setBorder('none', 'thin', 'none', 'thin');
                });

                foreach ($equityRS as $rowset => $rs) {
                    $datadate = intval($rs->DD) . " " . toThaiShortMonth($rs->MM) . " " . $rs->YYYY;
                    $sheet->row(1, array('การลงทุนตราสารทุน'));
                    $sheet->row(2, array('การลงทุน ณ. ' . $datadate));
                    
                    $sheet->mergeCells('A3:A4');
                    $sheet->mergeCells('B3:D3');

                    $header0[ ] = null;
                    $header0[0] = "ประเภทตราสาร";
                    $header0[1] = "นโยบายการลงทุนตราสารทุน";
                    $header0[2] = "";
                    $header0[3] = "";                     
                    $sheet->row(3, $header0);

                    $sheet->row(4, function ($row) { $row->setAlignment('center'); });
                    $header[] = null;
                    $header[0] = "";
                    $header[1] = "สัดส่วน (%)";
                    $header[2] = "ล้าน";
                    $header[3] = "  บาท  ";
                    $sheet->row(4, $header);

                     
                    $sheet->setColumnFormat(array(
                        'A' => '@',
                        'B' => '0.00',
                        'C' => '#,##0.00',
                        'D' => '#,##0.00'
                    ));

                     
                    $sheet->setWidth(array(
                        'A'  => 40,
                        'B'  => 15,
                        'C'  => 15,
                        'D'  => 15,
                        'E'  => 5,
                        'F'  => 5
                    ));

                    
                    
                    $fields[0] = "เงินฝากธนาคารพาณิชย์ ";
                    $fields[1] = $rs->DEPOSIT_RATIO;
                    $fields[2] = $rs->DEPOSIT_MB;
                    $fields[3] = $rs->DEPOSIT_B;
                    $sheet->appendRow($fields);
                    $sheet->row(5, function ($row) {
                        $row->setAlignment('right');
                    });
                    
                    $fields[0] = "ตราสารทุน(หุ้น)";
                    $fields[1] = $rs->STOCK_RATIO;
                    $fields[2] = $rs->STOCK_MB;
                    $fields[3] = $rs->STOCK_B;
                    $sheet->appendRow($fields);
                    $sheet->row(6, function ($row) {
                        $row->setAlignment('right');
                    });

                    $fields[0] = "อื่นๆ (ลูกหนี้และเจ้าหนี้จากการซื้อ-ขายหลักทรัพย์)";
                    $fields[1] = $rs->OTHER_RATIO;
                    $fields[2] = $rs->OTHER_MB;
                    $fields[3] = $rs->OTHER_B;
                    $sheet->appendRow($fields);
                    $sheet->row(7, function ($row) {
                        $row->setAlignment('right');
                    });

                    
                    $fields[0] = "ได้แก่ลูกหนี้จากการซื้อ / ขายหลักทรัพย์ / เงินปันผลลูกหนี้ / เจ้าหนี้อื่นๆ และเงินสมทบรอการจัดสรร";
                    $fields[1] = "";
                    $fields[2] = "";
                    $fields[3] = "";
                    

                    $sheet->appendRow($fields);
                     $sheet->setColumnFormat(array(
                        'A' => '@',
                        'B' => '@',
                        'C' => '@',
                        'D' => '@'
                    )); 
                    $sheet->row(8, function ($row) {
                        $row->setFontFamily('Cordia New');
                        $row->setFontSize(12);
                        $row->setFontWeight('italic');
                        $row->setAlignment('left');
                        $row->setFontColor('#a5a5a5');
                        
                    });

                        
                    $sheet->setColumnFormat(array(
                        'A' => '@', 
                        'B' => '0.00',
                        'C' => '#,##0.00',
                        'D' => '#,##0.00'
                    ));

                    $fields[0] = "รวม";
                    $fields[1] = $rs->SUM_RATIO;
                    $fields[2] = $rs->SUM_MB;
                    $fields[3] = $rs->SUM_B;
                    $sheet->appendRow($fields);
                    $sheet->row(9, function ($row) {
                        $row->setAlignment('right');
                    });  
                    
                    $fields[0] = "มูลค่าสินทรัพย์สุทธิ";
                    $fields[1] = "";
                    $fields[2] = $rs->NAV_MB;
                    $fields[3] = $rs->NAV_B;
                    $sheet->appendRow($fields);
                    $sheet->row(10, function ($row) {
                        $row->setAlignment('right');
                    });  


                    $fields[0] = "มูลค่าสินทรัพย์สุทธิต่อหน่วย";
                    $fields[1] = "";
                    $fields[2] = "";
                    $fields[3] = $rs->NAV_UNIT; 
                    $sheet->appendRow($fields);
                    $sheet->row(11, function ($row) {
                        $row->setAlignment('right');
                    });  
                    


                    $fields[0] = "อัตราผลตอบแทนรายเดือน %";
                    $fields[1] = "";
                    $fields[2] = "";
                    $fields[3] = $rs->YM;
                    $sheet->appendRow($fields);
                    $sheet->row(12, function ($row) {
                        $row->setAlignment('right');
                    });  


                    $fields[0] = "อัตราผลตอบแทนสะสม %";
                    $fields[1] = "";
                    $fields[2] = "";
                    $fields[3] = $rs->YC;
                    $sheet->appendRow($fields);
                    $sheet->row(13, function ($row) {
                        $row->setAlignment('right');
                    });  

                    break;
                } // foreach


                $sheet->cell('A3', function($cell) {
                         
                        $cell->setAlignment('center');
                    
                });
                $sheet->cell('B3', function($cell) {
                         
                        $cell->setAlignment('center');
                    
                });
                for($i = 5; $i < 9; $i++) { 
                    $sheet->cell('A' . $i, function($cell) {
                         
                        $cell->setAlignment('left');

                    });
                }

                for($i = 9; $i < 14; $i++) { 
                    $sheet->cell('A' . $i, function($cell) {
                         
                        $cell->setAlignment('center');

                    });
                }
                /////////////////////////
                // ==================================================================
                // PIE chart ตาสารทุน
                $ls = array( 'เงินฝากธนาคารพาณิชย์',//'พันธบัตรรัฐบาล ธปท.และรัฐวิสาหกิจ',
                             'ตราสารทุน(หุ้น)', //เงินฝากและตราสารหนี้ธนาคารพาณิชย์',
                             'อื่นๆ ลูกหนี้และเจ้าหนี้จากการซื้อ/ ขายหลักทรัพย์');
                $sheet->mergeCells('G4:H4');
                $sheet->setSize('G6', 40, 18);
                $sheet->cell('G4', function($cell) {
                    $cell->setValue('การลงทุนในตราสารทุน');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#b2babb');
                    $cell->setFontWeight('bold');
                    $cell->setFontSize(20);
                });
                $sheet->cell('H4', function($cell) {
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                $sheet->cell('G5', function($cell) {
                    $cell->setValue('ประเภท');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                $sheet->setSize('H5', 15, 18);
                $sheet->cell('H5', function($cell) {
                    $cell->setValue('อัตราร้อยละ (%)');
                    $cell->setAlignment('center');
                    $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    $cell->setBackground('#f2f3f4');
                });

                for($i = 0; $i < count($ls); $i++) {
                    $sheet->cell('G'. ($i + 6), function($cell) use($ls, $i) {
                        $cell->setValue($ls[$i]);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });

                    $sheet->cell('H'. ($i + 6), function($cell) use($ls, $i, $pie2) {
                        $cell->setValue( $pie2[$i]['value']);
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                }


               });
            } // end if
             
        })->download('xls');
    } // 


} // class AdminP2EquityNavReportController
?>