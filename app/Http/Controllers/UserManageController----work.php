<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package\Curl;
use App\Http\Requests;
use App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Input;
use App\Http\Controllers\Security;

class UserManageController extends Controller
{

    public function getsimple()
    {
        $data = getmemulist();
        $this->pageSetting( [
            'menu_group_id' => 51,
            'menu_id' => 1,
            'title' => getMenuName($data,51,1) ." | MEA"
        ]);

        //  
        // $user_group = DB::table('TBL_PRIVILEGE')->select('USER_PRIVILEGE_ID','USER_PRIVILEGE_DESC')->orderBy('USER_PRIVILEGE_ID', 'asc')->get();
        //
        return view('backend.pages.usersimple');
    }

    public  function  Checkdate(Request $request){

        /// $count = 0; 
        $results = null;
        $file = $request->file('exelimport');
        // ORIGINAL
        // $result  = Excel::load($request->file('exelimport'))->get();


        ini_set('max_execution_time', 8000);
		//ini_set('memory_limit', 1024000);
		ini_set('memory_limit', '-1');
        $menu_name = $request->input('menu') .'-'. uuid();

        $request->file('exelimport')->move(storage_path().'/public/import/' , $menu_name . '.xls');
        
        $inputfile = storage_path().'/public/import/' . $menu_name . '.xls';
	    $count = 0;

	   // $results  = Excel::load($inputfile)->all();
       // $count  = $results->count();
      /*
        Excel::load($inputfile, function($reader) use(&$count) {
            $results = $reader->get();
            $ret = $results->toArray();
            $count = count($ret);
        });
	*/
/*
		Excel::load( $inputfile, function($reader) use(&$count) {
            $reader->ignoreExpty();
			$results = $reader->all();
			$ret = $results->toArray();
			$count = count($ret);
		});
*/

		Excel::load($inputfile, function($reader) use(&$count) {
            $reader->ignoreExpty();
			$results = $reader->all();
			foreach($results as $r) {
				$count++;
			}   
		});



	/*
		$data = Excel::load($inputfile, function($reader)  {
		})->get();

		if(!empty($data) && $data->count()){
			$count = $data->count();
		}
*/
/*
		Excel::filter('chunk')->load($inputfile)->chunk(1000, function($results) use(&$count) {
		   foreach($results as $row)
           {
               // do stuff
			   $count++;
           }
		  // dd('Insert Record successfully.');
		  // return true;
		}, true);

*/
	//	importExcel($inputfile);

    /*    $msg = '';
		Excel::filter('chunk', function($excel) use($inputfile, &$count, &$msg) {
			$excel->load($inputfile, function($reader) use(&$count, &$msg) {
				$rowPointer = 0;
				$reader->chunk(1000, function($records) use(&$rowPointer, &$count, &$msg) {
					foreach ($records as $index =>$user) {
						//$this->writeRecordToSheet($rowPointer, $record, $sheet);
						$rowPointer++;
					}
					$count += $rowPoiner;
					
				});
				
			});	
			$msg = 'Completed Success 100000';
		});*/
/*
         
		//	  $count = 0;
        Excel::filter('chunk')->load($inputfile)->chunk(100, function($results) use(&$count)
        {
            $count = 5000;
            // return true if you want to stop importing.
            //return true;
        });


        /*
        Excel::load($inputfile, function($reader) {

        // Getting all results
        ///$results = $reader->get();

        // ->all() is a wrapper for ->get() and will work the same
            $count = 0;
            $result = $reader->get();
            $count  = $result->count();
            //return $count;
        });
*/

        // $result  = Excel::load($inputfile)->get();
        //$result  = Excel::load($inputfile)->all();
        
        //$count   = 1000; //$result->count();



//////////////////////////////////////////////////////////////////////////////////
//        
//      Excel::load($request->file('exelimport'), function ($reader) use($count) {
//
//            $results = $reader->get();
//
//            $ret = $results->toArray();
//
//
//            $count = count($ret);
//
//
//        });
//
/////////////////////////////////////////////////////////////////////////////////

        return response()->json(array('success' => true, 'html'=>$count));

    }

	





    public  function  dowloadsample(){
        $file = 'contents/sample/employee_info.xls';
        $headers = array(
            'Content-Type: application/pdf',
        );
        return \Response::download($file, 'employee_info.xls', $headers);
    }


    public function importdata(Request $request){

        $results = null;
        $ecPass = '';
        $empidtoStatus = '';
		$count = 0;
        $skipedCount = 0;
        $passedCount = 0;

	    ini_set('max_execution_time', 8000);
		ini_set('memory_limit', '-1');


        //DB::connection()->disableQueryLog();
        //ini_set('max_execution_time', 300);

		$file = $request->file('exelimport');

        $menu_name = $request->input('menu');

        $request->file('exelimport')->move(storage_path().'/public/import/' , $menu_name . '.xlsx');
        
        $inputfile = storage_path().'/public/import/' . $menu_name . '.xlsx';

       // return response()->json(array('success' => true, 'html'=>$inputfile));

        $retdate = Excel::load($inputfile, function ($reader) user(&$count,  &$skipedCount, &$passedCount, &$empidtoStatus) {
       //     $retdate = Excel::load(storage_path('/public/import/import.xlsx'), function ($reader)  {
            // LAM
            $reader->ignoreExpty();

            $results =$reader->setDateColumns(array(
                'startdate',
                'enddate'
            ))->get();
            
            $data = array();
            $dataupdate = array();
/*            $datauser = array();  */
//            $results = $reader->get();


            $ret = $results->toArray();

            $empidtoStatus = "123";
            foreach($ret as $index => $value){
                $count++;
//                var_dump($value["enddate"]);

                if(($value == null) || ($value["startdate"] == null) || ($value["startdate"]=='') || ($value["startdate"]=='24M3M1975')) {
                    $skipedCount++;
                    continue;
                }

                $EMP_ID = $value["empid"];

                $userinfo = DB::table('TBL_EMPLOYEE_INFO')->where('EMP_ID', $EMP_ID)->get();

                $user = DB::table('TBL_USER')->where('EMP_ID', $EMP_ID)->get();

                $TBL_EMP_PENSION =DB::table('TBL_EMP_PENSION')->where('EMP_ID', $EMP_ID)->get();

//                $StatusID = $value["user_status_id"];


                if($TBL_EMP_PENSION == null){
                    if($userinfo == null) {

//                    var_dump($value["enddate"]);

                        $dateS = new Date($value["startdate"]);

                        $dateStart = date("d/m/Y", strtotime($dateS));

                        $dateE = new Date($value["enddate"]);
                        $dateEnd = date("d/m/Y", strtotime($dateE));

                        /* LAM */
						$data = array();

                        array_push($data, array(
                            'EMP_ID' => $value["empid"],
                            'PREFIX' => $value["prefix"],
                            'FULL_NAME' => $value["fullname"],
                            'ENG_NAME' =>$value["engname"],
                            'FIRST_NAME' => $value["firstname"],
                            'LAST_NAME' => $value["lastname"],
                            'PRIORITY' => $value["priority"],
                            'JOB_ID' => $value["jobid"],
                            'JOB_DESC_SHT' => $value["jobdescsht"],
                            'JOB_DESC' => $value["jobdesc"],
                            'PER_ID' => $value["perid"],
                            'START_DATE' => $dateStart,
                            'END_DATE' => $dateEnd,
                            'COST_CENTER' => $value["costcenter"],
                            'C_LEVEL' => $value["clevel"],
                            'POST_ID' => $value["posid"],
                            'POS_DESC' => $value["posdesc"],
                            'ORG_ID' => $value["orgid"],
                            'ENG_FIRST_NAME' => $value["engfirstname"],
                            'ENG_LAST_NAME' => $value["englastname"],
                            'BIRTH_DATE' => $value["birthdate"],
                            'ORG_DESC' => $value["orgdesc"],
                            'PATH_ID' => $value["pathid"],
                            'DEP_ID' => $value["depid"],
                            'DIV_ID' => $value["divid"],
                            'SEC_ID' => $value["secid"],
                            'PART_ID' => $value["partid"],
                            'PARTH_SHT' => $value["pathsht"],
                            'DEP_SHT' => $value["depsht"],
                            'DIV_SHT' => $value["divsht"],
                            'SEC_SHT' => $value["secsht"],
                            'PATH_SHT' => $value["partsht"],
                            'PARTH_LNG' => $value["pathlng"],
                            'DEP_LNG' => $value["deplng"],
                            'DIV_LNG' => $value["divlng"],
                            'SEC_LNG' => $value["seclng"],
                            'PART_LNG' => $value["partlng"],
                        ));

						 /* LAM */    
						DB::table('TBL_EMPLOYEE_INFO')->insert($data);  // LAM

                    } else {

                        $dateS = new Date($value["startdate"]);

                        $dateStart = date("d/m/Y", strtotime($dateS));

                        $dateE = new Date($value["enddate"]);
                        $dateEnd = date("d/m/Y", strtotime($dateE));
                        $dataupdate = array(
                            'EMP_ID' => $value["empid"],
                            'PREFIX' => $value["prefix"],
                            'FULL_NAME' => $value["fullname"],
                            'ENG_NAME' =>$value["engname"],
                            'FIRST_NAME' => $value["firstname"],
                            'LAST_NAME' => $value["lastname"],
                            'PRIORITY' => $value["priority"],
                            'JOB_ID' => $value["jobid"],
                            'JOB_DESC_SHT' => $value["jobdescsht"],
                            'JOB_DESC' => $value["jobdesc"],
                            'PER_ID' => $value["perid"],
                            'START_DATE' => $dateStart,
                            'END_DATE' => $dateEnd,
                            'COST_CENTER' => $value["costcenter"],
                            'C_LEVEL' => $value["clevel"],
                            'POST_ID' => $value["posid"],
                            'POS_DESC' => $value["posdesc"],
                            'ORG_ID' => $value["orgid"],
                            'ENG_FIRST_NAME' => $value["engfirstname"],
                            'ENG_LAST_NAME' => $value["englastname"],
                            'BIRTH_DATE' => $value["birthdate"],
                            'ORG_DESC' => $value["orgdesc"],
                            'PATH_ID' => $value["pathid"],
                            'DEP_ID' => $value["depid"],
                            'DIV_ID' => $value["divid"],
                            'SEC_ID' => $value["secid"],
                            'PART_ID' => $value["partid"],
                            'PARTH_SHT' => $value["pathsht"],
                            'DEP_SHT' => $value["depsht"],
                            'DIV_SHT' => $value["divsht"],
                            'SEC_SHT' => $value["secsht"],
                            'PATH_SHT' => $value["partsht"],
                            'PARTH_LNG' => $value["pathlng"],
                            'DEP_LNG' => $value["deplng"],
                            'DIV_LNG' => $value["divlng"],
                            'SEC_LNG' => $value["seclng"],
                            'PART_LNG' => $value["partlng"],


                        );
                       

                        //////////////////////////////////////////////////
                        DB::table('TBL_EMPLOYEE_INFO')->where('EMP_ID',"=", $value["empid"])->update($dataupdate);
                    }

                    $passedCount++;

                    if($user == null){

                        $date = new Date();

                        $pri = $userinfo = DB::table('TBL_PRIVILEGE')->where('USER_PRIVILEGE_ID', 2)->get();

                        $datedata = $value["birthdate"];




                        $newDate =substr($datedata, -2) . substr($datedata, -4,2) . (((int)substr($datedata, -8, 4) )+543) ;



                        $MEASecEncoe = new \Security();
                        $ecPass =  $MEASecEncoe->encrypt($newDate,"#Gm2014$06$30@97");

                        //$ecPass = exec("cmd /c md5.bat -e ".$newDate." 2>&1");

                        //$ecPass = explode(':',$ecPass)[1];

                        $datedefault = new Date("9999-12-31 00:00:00.000") ;
                        $admin= 'Administrator';
                        $user_id = '2';
                        
                         /* LAM */
						$datauser = array();

                        array_push($datauser, array(
                            'EMP_ID' => $EMP_ID,
                            'USERNAME' => $EMP_ID,
                            'PASSWORD' => $ecPass,
                            'PASSWORD_EXPIRE_DATE' =>$datedefault,
                            'CREATE_DATE' => $date,
                            'CREATE_BY' => $admin,
                            'LAST_MODIFY_DATE' =>$date,
                            'USER_PRIVILEGE_ID' => $user_id,
                            'ACCESS_PERMISSIONS' => $pri[0]->ACCESS_PERMISSIONS,
                            'USER_STATUS_ID' => 13,
                            'FIRST_LOGIN_FLAG' => 0,
                            'EMAIL_NOTIFY_FLAG' => 1

                        ));

						/* LAM */
                        DB::table('TBL_USER')->insert($datauser);  
                    } 

	
                }

                $empidtoStatus .= ",". $EMP_ID ;

            } // foreach
            
            /// #### Problem Here ####
           /*
		    DB::table('TBL_EMPLOYEE_INFO')->insert($data); 
            DB::table('TBL_USER')->insert($datauser);
            */

            $sql = "UPDATE TBL_USER SET user_status_id= 14  WHERE EMP_ID NOT IN (" . $empidtoStatus . ")";

            DB::update(DB::raw($sql));

        });

        return response()->json(array('success' => true, 'html'=>'' . 
                ' Total : ' . $count . ' Record(s), '. 
                ' Passed: ' . $passedCount . ' Record(s), '. 
                ' Failed: ' . ($count - $passedCount) . ' Record(s), '. 
                ' Skiped: ' . $skipedCount . ' Record(s)  '.
                ' | '. $empidtoStatus));//$retdate));




    }


}
