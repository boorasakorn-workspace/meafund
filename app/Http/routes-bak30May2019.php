<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

            // LAM <<

Route::group(['middleware' => ['web']], function () {
    Route::get('login','Auth\AuthController@showLogin');
    Route::post('login','Auth\AuthController@checkLogin');
    Route::get('logout','Auth\AuthController@checkLogout');

    Route::get('forgotpassword','Auth\AuthController@showForgotPassword');
    Route::post('forgotpassword','Auth\AuthController@GetPassword');

    Route::get('firstlogin','Auth\AuthController@ShowSetNewpass');
    Route::post('firstlogin','Auth\AuthController@ResetPassword');




    Route::group(['prefix' => '/'] , function() {

        Route::get('/','HomeController@getIndex');
        Route::post('/viewrecord','HomeController@viewrecord');
        Route::get('{id}','HomeController@getIndexByID')->where('id', '[0-9]+');
//        Route::get('download/{file}','HomeController@DownloadFile');
        Route::get('download/{id}','HomeController@DownloadFile');
        Route::get('view/{id}','HomeController@ViewFile');


    });


    Route::group(['prefix' => '/valuefund'] , function() {

        Route::get('/','ValueFundController@getIndex');
        Route::post('/','ValueFundController@getIndex');


    });




//    Route::get('/netasset','NetassetController@getIndex');


    Route::group(['prefix' => '/netasset'] , function() {
        Route::get('/','NetassetController@getIndex');

        Route::post('/viewrecord','NetassetController@viewrecord');
        Route::get('{id}','NetassetController@getIndexByID')->where('id', '[0-9]+');

        Route::get('download/{id}','NetassetController@DownloadFile');
        Route::get('view/{id}','NetassetController@ViewFile');


    });


//
//
//    Route::get('/economic','EconomicController@getIndex');


    Route::group(['prefix' => '/economic'] , function() {
        Route::get('/','EconomicController@getIndex');

        Route::post('/viewrecord','EconomicController@viewrecord');
        Route::get('{id}','EconomicController@getIndexByID')->where('id', '[0-9]+');

        Route::get('download/{id}','EconomicController@DownloadFile');
        Route::get('view/{id}','EconomicController@ViewFile');


    });



//    Route::get('/announce','AnnounceController@getIndex');

    Route::group(['prefix' => '/announce'] , function() {
        Route::get('/','AnnounceController@getIndex');

        Route::post('/viewrecord','AnnounceController@viewrecord');
        Route::get('{id}','AnnounceController@getIndexByID')->where('id', '[0-9]+');

        Route::get('download/{id}','AnnounceController@DownloadFile');
        Route::get('view/{id}','AnnounceController@ViewFile');


    });

//    Route::get('/actfund','ActfundController@getIndex');

    Route::group(['prefix' => '/actfund'] , function() {
        Route::get('/','ActfundController@getIndex');

        Route::post('/viewrecord','ActfundController@viewrecord');
        Route::get('{id}','ActfundController@getIndexByID')->where('id', '[0-9]+');

        Route::get('download/{id}','ActfundController@DownloadFile');
        Route::get('view/{id}','ActfundController@ViewFile');


    });

//    Route::get('/board','BoardController@getIndex');

    Route::group(['prefix' => '/board'] , function() {
        Route::get('/','BoardController@getIndex');

        Route::post('/viewrecord','BoardController@viewrecord');
        Route::get('{id}','BoardController@getIndexByID')->where('id', '[0-9]+');

        Route::get('download/{id}','BoardController@DownloadFile');
        Route::get('view/{id}','BoardController@ViewFile');


    });
//    Route::get('/fundregulations','FundreController@getIndex');
    Route::group(['prefix' => '/fundregulations'] , function() {
        Route::get('/','FundreController@getIndex');

        Route::post('/viewrecord','FundreController@viewrecord');
        Route::get('{id}','FundreController@getIndexByID')->where('id', '[0-9]+');

        Route::get('download/{id}','FundreController@DownloadFile');
        Route::get('view/{id}','FundreController@ViewFile');


    });


    Route::get('/fundboard','FundboardController@getIndex');
    Route::get('/structuralfunds','StrucController@getIndex');


    //Route::get('/download','DownloadController@getIndex');
    Route::get('/test','TestController@getIndex');

    Route::group(['prefix' => '/test'] , function() {
        Route::get('/','TestController@getIndex');

        Route::post('/viewrecord','TestController@viewrecord');
        Route::get('{id}','TestController@getIndexByID')->where('id', '[0-9]+');

        Route::get('download/{id}','TestController@DownloadFile');
        Route::get('view/{id}','TestController@ViewFile');


    });

//    Route::get('/membershipform','MembershipfornController@getIndex');
    Route::group(['prefix' => '/membershipform'] , function() {
        Route::get('/','MembershipfornController@getIndex');

        Route::post('/viewrecord','MembershipfornController@viewrecord');
        Route::get('{id}','MembershipfornController@getIndexByID')->where('id', '[0-9]+');

        Route::get('download/{id}','MembershipfornController@DownloadFile');
        Route::get('view/{id}','MembershipfornController@ViewFile');


    });

//    Route::get('/form','FormController@getIndex');
    Route::group(['prefix' => '/form'] , function() {
        Route::get('/','FormController@getIndex');

        Route::post('/viewrecord','FormController@viewrecord');
        Route::get('{id}','FormController@getIndexByID')->where('id', '[0-9]+');

        Route::get('download/{id}','FormController@DownloadFile');
        Route::get('view/{id}','FormController@ViewFile');


    });
//    Route::get('/otherforms','OtherFormController@getIndex');
    Route::group(['prefix' => '/otherforms'] , function() {
        Route::get('/','OtherFormController@getIndex');

        Route::post('/viewrecord','OtherFormController@viewrecord');
        Route::get('{id}','OtherFormController@getIndexByID')->where('id', '[0-9]+');

        Route::get('download/{id}','OtherFormController@DownloadFile');
        Route::get('view/{id}','OtherFormController@ViewFile');


    });


    Route::group(['prefix' => 'qa'] , function() {

        Route::get('/', 'QaController@getIndex');
        Route::get('{id}', 'QaController@getIndexByID')->where('id', '[0-9]+');


    });




    Route::group(['prefix' => '/newsfund'] , function() {

        Route::get('/','NeesfundController@getIndex');
        Route::post('/viewrecord','NeesfundController@viewrecord');
        Route::get('{id}','NeesfundController@getIndexByID')->where('id', '[0-9]+');

        Route::get('download/{id}','NeesfundController@DownloadFile');
        Route::get('view/{id}','NeesfundController@ViewFile');

    });


    Route::group(['prefix' => '/news'] , function() {
        Route::get('/','NewsController@getIndex');

        Route::post('/viewrecord','NewsController@viewrecord');
        Route::get('{id}','NewsController@getIndexByID')->where('id', '[0-9]+');

        Route::get('download/{id}','NewsController@DownloadFile');
        Route::get('view/{id}','NewsController@ViewFile');

    });




    Route::group(['prefix' => '/yearbook'] , function() {
        Route::get('/','YearbookController@getIndex');

        Route::post('/viewrecord','YearbookController@viewrecord');
        Route::get('{id}','YearbookController@getIndexByID')->where('id', '[0-9]+');

        Route::get('download/{id}','YearbookController@DownloadFile');
        Route::get('view/{id}','YearbookController@ViewFile');


    });

    Route::group(['prefix' => '/downloads'] , function() {

        Route::get('/','DownloadController@getIndex');

        Route::post('/viewrecord','DownloadController@viewrecord');
        Route::get('{id}','DownloadController@getIndexByID')->where('id', '[0-9]+');

        Route::get('download/{id}','DownloadController@DownloadFile');
        Route::get('view/{id}','DownloadController@ViewFile');

    });



    Route::post('/search','SearchController@getIndex');

    Route::get('/search','SearchController@getIndex');

    Route::get('/search_detail','SearchController@getDetail');





    Route::get('/contact','ContactController@getIndex');
    Route::post('/contact','ContactController@SendMail');


    Route::get('/forservices','ForserviceController@getIndex');



    Route::group(['middleware' => ['auth.frontend']], function(){

        Route::get('/profile','ProfileController@getIndex');




        Route::group(['prefix' => 'editprofile'] , function() {

            Route::get('/','editprofileController@getIndex');
            Route::get('{id}','editprofileController@getIndex')->where('id', '[0-9]+');
            Route::post('/e1','editprofileController@EditProfile');
            Route::post('/e2','editprofileController@ResetPassworduser');
            Route::get('download/{id}','editprofileController@DownloadFile');


        });

        Route::get('/resetpassword','editprofileController@getIndex');

        Route::get('/informationbeneficiary','editprofileController@getIndex');



        Route::group(['prefix' => 'trends'] , function() {

            Route::get('/','TrendsController@getIndex');
            Route::get('{id}','TrendsController@getIndex')->where('id', '[0-9]+');


            Route::post('/','TrendsController@getIndexbysearchColum');
//          Route::get('{id}','TrendsController@getIndexbysearchColum')->where('id', '[0-9]+');

            Route::get('/s2','TrendsController@getIndex');
           // Route::get('/s2/{id}','TrendsController@getIndex')->where('id', '[0-9]+');
            Route::post('/s2','TrendsController@getIndexbysearchgp2');
            //Route::post('/s2/{id}','TrendsController@getIndexbysearchgp2')->where('id', '[0-9]+');


           Route::post('/s3','TrendsController@getIndexbysearchgpLastest');
           Route::get('/s3','TrendsController@getIndex');


        });







        Route::get('/trends_excel1','TrendsController@ExportExcel1');
        Route::get('/trends_excel2','TrendsController@ExportExcel2');

        Route::get('/reportingmemberbenefit','TrendsController@getIndex');
        Route::get('/compares','TrendsController@getIndex');


//        Route::get('/changeplan','changeplanController@getIndex');


        Route::group(['prefix' => 'changeplan'] , function() {

            Route::get('/', 'changeplanController@getIndex');
            Route::get('{id}','changeplanController@getIndex')->where('id', '[0-9]+');
            Route::post('/changeplan','changeplanController@InsertInvestPlan');
            Route::get('/delplan1','changeplanController@deleplan');
            Route::post('/', 'changeplanController@getIndexbysearch');
            Route::post('{id}', 'changeplanController@getIndexbysearch')->where('id', '[0-9]+');
//            Route::post('{id}','changeplanController@getIndexbysearch')->where('id', '[0-9]+');
//            Route::get('{id}', 'QaController@getIndexByID')->where('id', '[0-9]+');


        });



        Route::get('/historyinvestmentplan','changeplanController@getIndex');


        Route::group(['prefix' => 'cumulative'] , function() {
            Route::get('/', 'cumulativeController@getIndex');
            Route::get('{id}','cumulativeController@getIndex')->where('id', '[0-9]+');
            Route::get('/delplan', 'cumulativeController@deleplan');
            Route::post('/cumulative', 'cumulativeController@InsertPlan');
            Route::post('/', 'cumulativeController@getIndexbysearch');
            Route::post('{id}', 'cumulativeController@getIndexbysearch')->where('id', '[0-9]+');
        });



        Route::get('/historycumulative','cumulativeController@getIndex');




        Route::get('/riskassessment','riskassessmentController@getIndex');

        Route::get('/quiz','riskassessmentController@getquiz');

        Route::post('/quiz','riskassessmentController@insertQuiz');


    });


});


Route::group(['middleware' => ['backend'],'prefix' => 'admin'], function () {
    Route::get('login','Auth\AdminAuthController@showLogin');
    Route::get('forgotpassword','Auth\AdminAuthController@showforgot');
    Route::post('forgotpassword','Auth\AdminAuthController@ReqPassword');


    Route::get('firstlogin','Auth\AdminAuthController@showfirstlogin');
    Route::post('firstlogin','Auth\AdminAuthController@ResetPassword');

    Route::post('login','Auth\AdminAuthController@checkLogin');
    Route::get('logout','Auth\AdminAuthController@checkLogout');

    Route::group(['middleware' => ['auth.backend']], function() {
        // login required
        Route::get('/', 'DashboardController@showIndex');
//        Route::get('/', 'UserGroupController@userGroups');


        Route::group(['prefix' => 'userGroup'] , function() {

            Route::get('/', 'UserGroupController@userGroups');
            Route::post('getUserGroups', 'UserGroupController@getUserGroups');
            Route::post('checkUserGroupIdExist','UserGroupController@UserGroupIdExist');
            Route::get('add', 'UserGroupController@getAddUserGroup');
            Route::post('add', 'UserGroupController@postAddUserGroup');
            Route::get('edit/{id}', 'UserGroupController@getEditUserGroup')->where('id', '[0-9]+');
            Route::post('edit', 'UserGroupController@postEditUserGroup');
            Route::post('delete', 'UserGroupController@deleteUserGroup');

            Route::post('ishave', 'UserGroupController@CheckUserGroup');


            Route::post('getall', 'UserGroupController@Ajax_GetUserGroup');


        });

       

        Route::group(['prefix' => 'users'], function() {

 /* LAM progress bar */
		/*Route::get('/progress', function()
		{
   			if( Session::has('percent_progress'))
        		return Session::get('percent_progress');
		});

        Route::get('/total', function()
		{
   			if( Session::has('total'))
        		return Session::get('total');
		});
*/
           // >> LAM 
 			Route::get('progress', 'UserController@checkProgress');

            Route::get('/', 'UserController@users');
            Route::post('getUsers', 'UserController@getUsers');
            Route::get('add','UserController@getAddUser');
            Route::post('add','UserController@postAddUser');

            Route::get('edit/{id}', 'UserController@getEditUser')->where('id', '[0-9]+');
            Route::post('edit', 'UserController@postEditUser');
            Route::post('getall', 'UserController@Ajax_GetUser');
            Route::post('delete', 'UserController@deleteUser');
            Route::post('ReqPass', 'UserController@ReqPassword');

            Route::post('ReqPass_optional', 'UserController@ReqPassword_optional');

            Route::get('getimport','UserController@getimport');
            Route::post('import','UserController@importdata');

            Route::post('check','UserController@Checkdate');
		   // Route::post('check','UserController@Checkdate');
            Route::post('import2','UserController@importdata2');

            Route::post('edit1', 'UserController@postEditUser1');
            Route::post('edit2', 'UserController@postEditUser2');

            Route::post('edit3', 'UserController@postEditUser3');


            Route::get('downloadsample', 'UserController@dowloadsample');
            Route::get('downloadsample2', 'UserController@dowloadsample2');
            Route::get('downloadsample3', 'UserController@dowloadsample3');


            Route::get('downloadsample_add', 'UserController@dowloadsample_add');
            Route::get('downloadsample_edit', 'UserController@dowloadsample_edit');
        });

        Route::group(['prefix' => 'simple'], function() {
            Route::get('/', 'UserManageController@getsimple');

            Route::post('import','UserManageController@importdata');
            Route::post('check','UserManageController@Checkdate');

            Route::get('downloadsample', 'UserManageController@dowloadsample');
        });

        Route::group(['prefix' => 'plan'], function() {
            Route::get('/', 'UserManagePlanController@getsimple');

            Route::post('import','UserManagePlanController@importdata');
            Route::post('check','UserManagePlanController@Checkdate');
            Route::get('downloadsample', 'UserManagePlanController@dowloadsample');

        });

        Route::group(['prefix' => 'fund'], function() {

        	//Route::get('progress', 'UserManageFundController@checkProgress');
        	

            Route::get('/', 'UserManageFundController@getsimple');

            Route::post('import','UserManageFundController@importdata');
            Route::post('check','UserManageFundController@Checkdate');

            Route::get('downloadsample', 'UserManageFundController@dowloadsample');

            Route::get('progress', function() {
        		//$percent = 78;//58;
        		//if(Session::has('progress_count')) 
        		   // $percent = (Session::get('progress_count') * 100) /  Session::get('total_records');

        		$filename = storage_path('/public/import/progressbar.txt');
        		// $handle = fopen($filename, "r");
        		$contents = Session::get('progress_count');
        		/*if($handle) {
					$contents = (int)fread($handle, filesize($filename));
					
                }*/
                //fclose($handle);
    			return \Response::json(array('percent_progress' => strval($contents),
                                         'total'          => Session::get('total_records'))
                                  );
			});
        });
//
//
        Route::group(['prefix' => 'benefit'], function() {
            Route::get('/', 'UserManageBenefitcontroller@getsimple');

            Route::post('import','UserManageBenefitcontroller@importdata');
            Route::post('import_pdf','UserManageBenefitcontroller@importdata_pdf');

            Route::post('check','UserManageBenefitcontroller@Checkdate');

            Route::get('downloadsample', 'UserManageBenefitcontroller@dowloadsample');
        });
//
//
        Route::group(['prefix' => 'profit'], function() {
            Route::get('/', 'UserManageProfitController@getsimple');

            Route::post('import','UserManageProfitController@importdata');
            Route::post('check','UserManageProfitController@Checkdate');
            Route::get('downloadsample', 'UserManageProfitController@dowloadsample');

        });

        Route::group(['prefix' => 'extendrate'], function() {
            Route::get('/', 'UserManageExtendController@getsimple');

            Route::post('import','UserManageExtendController@importdata');
            Route::post('check','UserManageExtendController@Checkdate');
            Route::get('downloadsample', 'UserManageExtendController@dowloadsample');

        });

        Route::group(['prefix' => 'currentrate'], function() {
            Route::get('/', 'UserManageCurrentController@getsimple');

            Route::post('import','UserManageCurrentController@importdata');
            Route::post('check','UserManageCurrentController@Checkdate');

            Route::get('downloadsample', 'UserManageCurrentController@dowloadsample');
        });



        Route::group(['prefix' => 'chooseplan'], function() {

            Route::get('/', 'C52_1Controller@getindex');

            Route::get('add', 'C52_1Controller@getAdd');
            Route::post('add', 'C52_1Controller@postAdd');
            Route::get('edit/{id}', 'C52_1Controller@getEdit')->where('id', '[0-9]+');
            Route::post('edits', 'C52_1Controller@postEdit');
            Route::post('delete', 'C52_1Controller@delete');

            Route::post('getall', 'C52_1Controller@Ajax_Index');


        });

        Route::group(['prefix' => 'newstopic'], function() {

            Route::get('/', 'C53_1Controller@getindex');

            Route::get('add', 'C53_1Controller@getAdd');
            Route::post('add', 'C53_1Controller@postAdd');
            Route::get('edit/{id}', 'C53_1Controller@getEdit')->where('id', '[0-9]+');
            Route::post('edits', 'C53_1Controller@postEdit');
            Route::post('delete', 'C53_1Controller@delete');

            Route::post('getall', 'C53_1Controller@Ajax_Index');

            Route::post('menulist', 'C53_1Controller@Ajax_menulist');
           
            Route::get('progress', 'C53_1Controller@getProgess');


        });


        Route::group(['prefix' => 'news'], function() {

            Route::get('/', 'C53_2Controller@getindex');

            Route::get('add', 'C53_2Controller@getAdd');
            Route::post('add', 'C53_2Controller@postAdd');
            Route::get('edit/{id}', 'C53_2Controller@getEdit')->where('id', '[0-9,]+');
            Route::post('edits', 'C53_2Controller@postEdit');
            Route::post('delete', 'C53_2Controller@delete');

            Route::post('getall', 'C53_2Controller@Ajax_Index');

            Route::post('menulist', 'C53_2Controller@Ajax_menulist');

            Route::post('imageupload', 'C53_2Controller@imageupload');


            Route::get('progress', 'C53_2Controller@getProgess');
            /*Route::get('progress', function() {
                // return Response::json(array(46));
                if(Session::has('upload_progress')) 
                    return Response::json(array('progress' => Session::get('upload_progress')));
                else
                    return Response::json(array('progress' => 78));
            });
*/

        });

        Route::group(['prefix' => 'faqcate'], function() {

            Route::get('/', 'C54_1Controller@getindex');

            Route::get('add', 'C54_1Controller@getAdd');
            Route::post('add', 'C54_1Controller@postAdd');
            Route::get('edit/{id}', 'C54_1Controller@getEdit')->where('id', '[0-9,]+');
            Route::post('edits', 'C54_1Controller@postEdit');
            Route::post('delete', 'C54_1Controller@delete');

            Route::post('getall', 'C54_1Controller@Ajax_Index');

            Route::post('menulist', 'C54_1Controller@Ajax_menulist');

            Route::post('imageupload', 'C54_1Controller@imageupload');



        });
        Route::group(['prefix' => 'faqtopic'], function() {

            Route::get('/', 'C54_2Controller@getindex');

            Route::get('add', 'C54_2Controller@getAdd');
            Route::post('add', 'C54_2Controller@postAdd');
            Route::get('edit/{id}', 'C54_2Controller@getEdit')->where('id', '[0-9,]+');
            Route::post('edits', 'C54_2Controller@postEdit');
            Route::post('delete', 'C54_2Controller@delete');

            Route::post('getall', 'C54_2Controller@Ajax_Index');

            Route::post('menulist', 'C54_2Controller@Ajax_menulist');

            Route::post('imageupload', 'C54_2Controller@imageupload');


        });


        Route::group(['prefix' => 'contact'], function() {

            Route::get('/', 'C55_1Controller@getindex');
            Route::post('add', 'C55_1Controller@postAdd');

        });



        Route::group(['prefix' => 'risk'], function() {

            Route::get('/', 'C56_1Controller@getindex');

            Route::post('getall', 'C56_1Controller@Ajax_Index');
            Route::get('add', 'C56_1Controller@getAdd');
            Route::post('add', 'C56_1Controller@postAdd');


            Route::get('edit/{id}', 'C56_1Controller@getEdit')->where('id', '[0-9,]+');
            Route::post('ishave', 'C56_1Controller@CheckUserGroup');
            Route::post('period', 'C56_1Controller@addperiod');
            Route::post('getperiod', 'C56_1Controller@ajax_getperiod');
            Route::post('editperiod', 'C56_1Controller@ajax_editperiod');
            Route::post('deleteperiod', 'C56_1Controller@ajax_deleteperiod');

            Route::post('question', 'C56_1Controller@addquestion');
            Route::post('getquestion', 'C56_1Controller@ajax_getquestion');
            Route::post('editquestion', 'C56_1Controller@ajax_editquestion');
            Route::post('deletequestion', 'C56_1Controller@ajax_deletequestion');

            Route::post('updateflag', 'C56_1Controller@ajax_updateflag');

            Route::post('delete', 'C56_1Controller@ajax_deleteQuiz');



        });

        Route::group(['prefix' => 'nav'], function() {

            Route::get('/', 'C57_1Controller@getindex');
            Route::post('getlist', 'C57_1Controller@getSearch');
            Route::get('getadd', 'C57_1Controller@getAdd');
            Route::post('addsave', 'C57_1Controller@Navsave');
            Route::post('editsave', 'C57_1Controller@editsave');
            Route::post('deletenav', 'C57_1Controller@deletenav');


        });


        Route::group(['prefix' => 'con1'], function() {

            Route::get('/', 'C59_1Controller@getindex');
            Route::post('getlist', 'C59_1Controller@getSearch');
            Route::post('addsave', 'C59_1Controller@Navsave');
            Route::post('editsave', 'C59_1Controller@editsave');
            Route::post('deletenav', 'C59_1Controller@deletenav');
        });

        Route::group(['prefix' => 'con3'], function() {

            Route::get('/', 'C59_3Controller@getindex');
            Route::post('getlist', 'C59_3Controller@getSearch');
            Route::post('addsave', 'C59_3Controller@Navsave');
            Route::post('editsave', 'C59_3Controller@editsave');
            Route::post('deletenav', 'C59_3Controller@deletenav');
        });

        Route::group(['prefix' => 'con4'], function() {

            Route::get('/', 'C59_4Controller@getindex');
            Route::post('getlist', 'C59_4Controller@getSearch');
            Route::post('addsave', 'C59_4Controller@Navsave');
            Route::post('editsave', 'C59_4Controller@editsave');
            Route::post('deletenav', 'C59_4Controller@deletenav');
        });

        /**
         * to support KTAM URL redirect 
         */ 
        Route::group(['prefix' => 'con5'], function() {

            Route::get('/', 'C59_5Controller@getindex');
            Route::post('getlist', 'C59_5Controller@getSearch');
            //Route::post('addsave', 'C59_5Controller@Navsave');
            Route::post('editsave', 'C59_5Controller@editsave');
            //Route::post('deletenav', 'C59_5Controller@deletenav');
        });




        Route::group(['prefix' => 'cmail'], function() {

            Route::get('/', 'C55_2Controller@getindex');
            Route::post('add', 'C55_2Controller@postAdd');
            Route::post('getall', 'C55_2Controller@Ajax_Index');

            Route::get('forward/{id}', 'C55_2Controller@getforward')->where('id', '[0-9,]+');
            Route::get('reply/{id}', 'C55_2Controller@getreply')->where('id', '[0-9,]+');

            Route::post('reply', 'C55_2Controller@SendReply');
            Route::post('forward', 'C55_2Controller@SendForward');
        });


//        Report Start

        Route::group(['prefix' => 'report1'], function() {
            Route::get('/', 'AdminReportController@getreport1');
            Route::post('/all', 'AdminReportController@ajax_report1');
            Route::post('/search', 'AdminReportController@ajax_report1_search');

            Route::post('/search_ana', 'AdminReportController@ajax_report1_search_ana');

            Route::get('/exportsearch', 'AdminReportController@ajax_report1_search_export');
            Route::get('/exportsearch_ana', 'AdminReportController@ajax_report1_search_ana_export');
            Route::get('/exportall', 'AdminReportController@ajax_report1_all_export');


            Route::get('/exportsearch_txt', 'AdminReportController@ajax_report1_search_export_txt');

            Route::get('/exportall_text2', 'AdminReportController@ajax_report1_all_export_text');



        });
        Route::group(['prefix' => 'report2'], function() {
            Route::get('/', 'AdminReport2Controller@getreport2');
            Route::post('/search', 'AdminReport2Controller@ajax_report2_search');
            Route::get('/exportsearch', 'AdminReport2Controller@ajax_report2_search_export');

        });

        Route::group(['prefix' => 'report3'], function() {
            Route::get('/', 'AdminReport3Controller@getreport');
            Route::post('/search', 'AdminReport3Controller@ajax_report_search');
            Route::get('/exportsearch', 'AdminReport3Controller@ajax_report_search_export');

        });

        Route::group(['prefix' => 'report4'], function() {
            Route::get('/', 'AdminReport4Controller@getreport');
            Route::post('/search', 'AdminReport4Controller@ajax_report_search');
            Route::get('/exportsearch', 'AdminReport4Controller@ajax_report_search_export');

        });

        Route::group(['prefix' => 'report5'], function() {
            Route::get('/', 'AdminReport5Controller@getreport');
            Route::post('/search', 'AdminReport5Controller@ajax_report_search');
            Route::get('/exportsearch', 'AdminReport5Controller@ajax_report_search_export');

        });

        Route::group(['prefix' => 'report6'], function() {
            Route::get('/', 'AdminReport6Controller@getreport');
            Route::post('/search', 'AdminReport6Controller@ajax_report_search');
            Route::get('/exportsearch', 'AdminReport6Controller@ajax_report_search_export');

        });

        Route::group(['prefix' => 'report7'], function() {
            Route::get('/', 'AdminReport7Controller@getreport');
            Route::post('/search', 'AdminReport7Controller@ajax_report_search');
            Route::get('/exportsearch', 'AdminReport7Controller@ajax_report_search_export');

        });


        Route::group(['prefix' => 'report8'], function() {
            Route::get('/', 'AdminReport8Controller@getreport');
            Route::post('/search', 'AdminReport8Controller@ajax_report_search');
            Route::get('/exportsearch', 'AdminReport8Controller@ajax_report_search_export');

        });

        Route::group(['prefix' => 'report9'], function() {
            Route::get('/', 'AdminReport9Controller@getreport');
            Route::post('/search', 'AdminReport9Controller@ajax_report_search');
            Route::post('/search2', 'AdminReport9Controller@ajax_report_search2');
            Route::get('/exportsearch', 'AdminReport9Controller@ajax_report_search_export');
            Route::get('/exportsearch2', 'AdminReport9Controller@ajax_report_search_export2');

        });

        Route::group(['prefix' => 'report10'], function() {
            Route::get('/', 'AdminReport10Controller@getreport');
            Route::post('/search', 'AdminReport10Controller@ajax_report_search');
            Route::get('/exportsearch', 'AdminReport10Controller@ajax_report_search_export');

        });
        Route::group(['prefix' => 'report11'], function() {
            Route::get('/', 'AdminReport11Controller@getreport');
            Route::post('/search', 'AdminReport11Controller@ajax_report_search');
            Route::get('/exportsearch', 'AdminReport11Controller@ajax_report_search_export');

        });
        Route::group(['prefix' => 'report12'], function() {
            Route::get('/', 'AdminReport12Controller@getreport');
            Route::post('/search', 'AdminReport12Controller@ajax_report_search');
            Route::get('/exportsearch', 'AdminReport12Controller@ajax_report_search_export');

        });

        Route::group(['prefix' => 'report13'], function() {
            Route::get('/', 'AdminReport13Controller@getreport');
            Route::post('/search', 'AdminReport13Controller@ajax_report_search');
            Route::get('/exportsearch', 'AdminReport13Controller@ajax_report_search_export');

        });

        /**
         * Phase#2 Equity Company Management (ตราสารทุน)
		 * Created: 2016/11/27 00:06
         * menu-id = 60
		 */
        Route::group(['prefix' => 'EquityCompany'], function() {
            /// TAB#1
            /**
             * route GET views for Equity Company Management
             */
            Route::get('/', 'EquityCompanyManagementController@getindex');
            Route::get('add', 'EquityCompanyManagementController@getAdd');
            Route::get('edit/{id}', 'EquityCompanyManagementController@getEdit');//->where('id', '^[a-zA-Z][a-zA-Z0-9_\\-]+$');

		
            /**
             * route POST methods Equity Company Management
             */
            Route::post('add', 'EquityCompanyManagementController@postAdd');
            Route::post('edits', 'EquityCompanyManagementController@postEdit');
            Route::post('delete', 'EquityCompanyManagementController@delete');
            Route::post('getall', 'EquityCompanyManagementController@Ajax_Index');

            ////////////////////////////////////////////////////////////////////////////////////
            // TAB#2
            ////////////////////////////////////////////////////////////////////////////////////
            /**
             * route GET views for Equity Category
             */
            Route::get('category',          'EquityCompanyManagementController@getindexCategory');  // -> VIEW: p2_tab2_equity_category_page.blade.php
            Route::get('addCategory',       'EquityCompanyManagementController@getAddCategory');    // -> VIEW: p2_tab2_add_equity_category_page.blade.php 
            Route::get('editCategory/{id}', 'EquityCompanyManagementController@getEditCategory');   // -> VIEW: 
            Route::get('getimportCategory', 'EquityCompanyManagementController@getimportCategory');
            Route::get('dowloadsampleEquityCategory',    'EquityCompanyManagementController@dowloadsampleEquityCategory');
            
            /**
             * route POST methods Equity Company Management
             */
            Route::post('addCategory',    'EquityCompanyManagementController@postAddCategory');
            Route::post('editsCategory',  'EquityCompanyManagementController@postEditCategory');
            Route::post('deleteCategory', 'EquityCompanyManagementController@deleteCategory');
            Route::post('getallCategory', 'EquityCompanyManagementController@Ajax_Index_Category');

            Route::post('importCategory','EquityCompanyManagementController@importCategory');




            ///////////////////////////////////////////////////////////////////////////////////
            // TAB#3
            ////////////////////////////////////////////////////////////////////////////////////
            /**
             * route GET views for Equity Category  //Ajax_Index_EquityIndex
             */
            Route::get('equityindex',              'EquityCompanyManagementController@getindexEquityIndex');  // -> VIEW: p2_tab3_equity_securities_page.blade.php
            Route::get('addEquityIndex',           'EquityCompanyManagementController@getAddEquityIndex');    // -> VIEW: p2_tab3_add_equity_securities_page.blade.php 
            Route::get('editEquityIndex/{id}',     'EquityCompanyManagementController@getEditEquityIndex');//->where('id', '^[a-zA-Z][a-zA-Z0-9_\\-]+$');
            Route::get('getimportEquityIndex',     'EquityCompanyManagementController@getimportEquityIndex');
            Route::get('dowloadsampleEquityIndex', 'EquityCompanyManagementController@dowloadsampleEquityIndex');
            Route::get('/exportsearch',             'EquityCompanyManagementController@ajax_report_search_export');

            
             /**
             * route POST methods Equity Company Management
             */

            Route::post('addEquityIndex',    'EquityCompanyManagementController@postAddEquityIndex');
            Route::post('editsEquityIndex',    'EquityCompanyManagementController@postEditEquityIndex');
            Route::post('deleteEquityIndex', 'EquityCompanyManagementController@deleteEquityIndex');
            Route::post('getallEquityIndex',  'EquityCompanyManagementController@Ajax_Index_EquityIndex');
           
            Route::post('importEquityIndex','EquityCompanyManagementController@importEquityIndex');

           
            Route::post('getEquityIndexSearchForm', 'EquityCompanyManagementController@Ajax_EquityIndexSearchForm');
           

            ///////////////////////////////////////////////////////////////////////////////////
            /// TAB#4 
            ///////////////////////////////////////////////////////////////////////////////////
            /**
             * route GET views for Equity Broker
             */
            Route::get('equityBroker',    'EquityCompanyManagementController@getindexBroker');
            Route::get('addBroker',       'EquityCompanyManagementController@getAddBroker');
            Route::get('editBroker/{id}', 'EquityCompanyManagementController@getEditBroker');//->where('id', '^[a-zA-Z][a-zA-Z0-9_\\-]+$');

            /**
             * route POST methods Equity Broker
             */
            Route::post('addBroker',      'EquityCompanyManagementController@postAddBroker'); 
            Route::post('editsBroker',    'EquityCompanyManagementController@postEditBroker');
            Route::post('deleteBroker',   'EquityCompanyManagementController@deleteBroker');
            Route::post('getallBroker',   'EquityCompanyManagementController@Ajax_Index_Broker');

        });

        
         
        /**
         * Phase#2 Equity Data Import (ตราสารทุน)
         * Created: 2016/11/27 00:06
         * menu-id = 60.2
         */
        Route::group(['prefix' => 'EquityDataImport'], function() {
            /// TAB#1
            /**
             * route GET views for Bond Company Management
             */
            Route::get('/',         'EquityDataImportController@T2_getimport');
            Route::get('add',       'EquityDataImportController@getAdd');
            Route::get('edit/{id}', 'EquityDataImportController@getEdit');//->where('id', '^[a-zA-Z][a-zA-Z0-9_\\-]+$');

        
            /**
             * route POST methods Bond Company Management
             */
            Route::post('add',    'EquityDataImportController@postAdd');
            Route::post('edits',  'EquityDataImportController@postEdit');
            Route::post('delete', 'EquityDataImportController@delete');
            Route::post('getall', 'EquityDataImportController@Ajax_Index');

            ////////////////////////////////////////////////////////////////////////////////////
            // START: TAB#2
            ////////////////////////////////////////////////////////////////////////////////////
            /**
             * route GET views for Bond Category
             */
            //Route::get('datalist',        'EquityDataImportController@T2_getindexDatalist');  // -> VIEW: p2_tab2_equity_dataimport_page.blade.php
            //Route::get('editdata/{id}',   'EquityDataImportController@T2_getEdit');   // -> VIEW: 
            Route::get('getimport',       'EquityDataImportController@T2_getimport');

            Route::get('dowloadsampleEquity',   'EquityDataImportController@T2_dowloadsampleEquity');
            Route::get('dowloadsampleBond',   'EquityDataImportController@T2_dowloadsampleBond');
            
             /**
             * route POST methods Bond Company Management
             */
            Route::post('t2check',   'EquityDataImportController@T2_checkfile'); 
            Route::post('t2import',  'EquityDataImportController@T2_ImportData'); 
            Route::post('t2edits',   'EquityDataImportController@T2_postEdit');
            Route::post('t2delete',  'EquityDataImportController@T2_delete');
            Route::post('t2getall',  'EquityDataImportController@T2_Ajax_Index');
            

            /**
             * 1) Check Purchase and Sale import file  (Menu 1. Import ข้อมูลซื้อ - ขายหลักทรัพย์)
             */ 
            Route::post('t2checkPurchase',      'EquityDataImportController@T2_checkPurchaseAndSaleFile'); 
            Route::post('t2importPurchase',     'EquityDataImportController@T2_importPurchaseAndSaleFile'); 
            Route::get('dowloadsamplePurchase', 'EquityDataImportController@T2_downloadSamplePurchase');

            /**
             * 2) Check Brokerage import file  (Menu 2. Import Brokerage การ Import ค่านายหน้าซื้อ-ขายหลักทรัพย์ )
             */ 
            Route::post('t2checkBrokerage',      'EquityDataImportController@T2_checkBrokerageFile'); 
            Route::post('t2importBrokerage',     'EquityDataImportController@T2_importBrokerageFile'); 
            Route::get('dowloadsampleBrokerage', 'EquityDataImportController@T2_downloadSampleBrokerage');



            /**
             * 3) Check Closed import file  (Menu 3. Import ข้อมูลราคาปิดการซื้อ-ขายหลักทรัพย์รายวัน)
             */ 
            Route::post('t2checkClosed',      'EquityDataImportController@T2_checkClosedFile'); 
            Route::post('t2importClosed',     'EquityDataImportController@T2_importClosedFile'); 
            Route::get('dowloadsampleClosed', 'EquityDataImportController@T2_downloadSampleClosed');

            /*
             * 4) เป็นเมนูสำหรับนําเข้าข้อมูล มูลค่า หน่วยลงทุน และอัตราผลตอบแทนเข้าสู่ระบบกองทุน ฯ 
             */ 
            //Route::get('getindexEquityUnitValIndex',    'EquityDataImportController@T2_getindexEquityUnitValIndex');
            Route::get('editUnitVal/{id}',     'EquityDataImportController@getEditUnitVal'); 
            Route::get('getAddUnitVal',     'EquityDataImportController@getAddUnitVal'); 
            
            Route::post('addUnitVal',    'EquityDataImportController@postAddUnitVal');
            Route::post('editsUnitVal',  'EquityDataImportController@postEditsUnitVal');
            Route::post('deleteUnitValRecords', 'EquityDataImportController@deleteUnitValRecords'); 
                  
            
            Route::post('equityUnitValIndex', 'EquityDataImportController@Ajax_Index_EquityUnitValIndex');           
            Route::post('equityUnitValIndexSearchForm', 'EquityDataImportController@Ajax_EquityUnitValIndexSearchForm');
           


            /**
             * 7) Check GainLoss import file  (Menu 7. Import ข้อมูลรายละเอียดผลประโยชน์การลงทุน)
             */ 
            Route::post('t2checkGainLoss',      'EquityDataImportController@T2_checkGainLoss'); 
            Route::post('t2importGainLoss',     'EquityDataImportController@T2_importGainLoss'); 
            Route::get('dowloadsampleGainLoss', 'EquityDataImportController@T2_downloadSampleGainLoss');

 
            /**
             * 8) Check Stock Universe import file  (Menu 8. Import Stock Universe)
             */ 
            Route::post('t2checkStockUniverse',      'EquityDataImportController@T2_checkStockUniverseFile'); 
            Route::post('t2importStockUniverse',     'EquityDataImportController@T2_importStockUniverseFile'); 
            Route::get('dowloadsampleStockUniverse', 'EquityDataImportController@T2_downloadSampleStockUniverse');


            // END: TAB2
        });

        

        /* รายงาน ตราสารทุน (Equity Trading Report) No. 1 */ 
        Route::group(['prefix' => 'EquityTradingReport'], function() {
            Route::get('/',             'AdminP2EquityTradingReportController@getreport');
            Route::post('/search',      'AdminP2EquityTradingReportController@ajax_report_search');
            Route::post('/chartsearch', 'AdminP2EquityTradingReportController@ajax_chart_search');
             Route::post('/BusinessChart', 'AdminP2EquityTradingReportController@ajax_business_chart_search');
         

            Route::get('/exportsearch', 'AdminP2EquityTradingReportController@ajax_report_search_export');
            Route::post('/saveheader',   'AdminP2EquityTradingReportController@ajax_store_customize_field');
            Route::post('/readheader',   'AdminP2EquityTradingReportController@ajax_retrive_customize_field');

        });

        /* รายงานข้อมูลมูลค่าหน่วยลงทุนและอัตราผลตอบแทน (AdminP2EquityInvestmentReport) No.2 - Menu ID: 62.2 */
        /* รายงาน  (EquityInvestmentReport) No. 2 */ 
          Route::group(['prefix' => 'EquityInvestmentReport'], function() {
            /* main */
            Route::get('/',                 'AdminP2EquityInvestmentReportController@getreport_main');
            Route::post('/searchmain',      'AdminP2EquityInvestmentReportController@ajax_report_search_main');
            Route::get('/exportsearchmain', 'AdminP2EquityInvestmentReportController@ajax_report_search_export_main');
           
            /* details */
            Route::get('/bonddetails/{id}',      'AdminP2EquityInvestmentReportController@getBondDetails');
            Route::get('/equitydetails/{id}',    'AdminP2EquityInvestmentReportController@getEquityDetails');
            Route::post('/bonddetails',      'AdminP2EquityInvestmentReportController@ajax_report_details_search');
            Route::post('/equitydetails',    'AdminP2EquityInvestmentReportController@ajax_report_details_search');

            Route::get('/exportsearchdetails',     'AdminP2EquityInvestmentReportController@ajax_report_search_export_details');
        });
           
          
        /* รายงาน  (Equity Nav Report) No. 3 */ 
        Route::group(['prefix' => 'EquityNavReport'], function() {
            /* main */
            Route::get('/',                 'AdminP2EquityNavReportController@getreport_main');
            Route::post('/searchmain',      'AdminP2EquityNavReportController@ajax_report_search_main');
            Route::get('/exportsearchmain', 'AdminP2EquityNavReportController@ajax_report_search_export_main');
            /* details */
            Route::get('/bonddetails/{id}',      'AdminP2EquityNavReportController@getBondDetails');
            Route::get('/equitydetails/{id}',    'AdminP2EquityNavReportController@getEquityDetails');
            Route::post('/bonddetails',      'AdminP2EquityNavReportController@ajax_report_details_search');
            Route::post('/equitydetails',    'AdminP2EquityNavReportController@ajax_report_details_search');

            Route::get('/exportsearchdetails',     'AdminP2EquityNavReportController@ajax_report_search_export_details');
           

            /* NAV report 1 */
         /*   Route::get('/nav1',              'AdminP2EquityNavReportController@getreport1');
            Route::post('/search1',      'AdminP2EquityNavReportController@ajax_report_search1');
            Route::get('/exportsearch1', 'AdminP2EquityNavReportController@ajax_report_search_export_main');
*/
            /* NAV report 2 */  
  /*          Route::get('/nav2',          'AdminP2EquityNavReportController@getreport2');
            Route::post('/search2',      'AdminP2EquityNavReportController@ajax_report_search2');
            Route::get('/exportsearch2', 'AdminP2EquityNavReportController@ajax_report_search_export_main');
*/
        });

        /* รายงาน ความเคลื่อนไหวตลาดตราสารทุน (Equity Closed Report) No. 5 */ 
        Route::group(['prefix' => 'EquityClosedReport'], function() {
            Route::get('/',             'AdminP2EquityClosedReportController@getreport');
            Route::post('/search',      'AdminP2EquityClosedReportController@ajax_report_search');
            Route::get('/exportsearch', 'AdminP2EquityClosedReportController@ajax_report_search_export');

        });

        /* รายงาน ผลประโยชน์ จากการลงทุน(Benefits Report) No. 6 */ 
        Route::group(['prefix' => 'BenefitsReport'], function() {
            Route::get('/',             'AdminP2BenefitsReportController@getreport1');
            Route::post('/profitsearch',      'AdminP2BenefitsReportController@ajax_report1_profit_loss_search');
            Route::post('/dividendsearch',    'AdminP2BenefitsReportController@ajax_report2_dividen_search');
            Route::post('/gainlosssearch',    'AdminP2BenefitsReportController@ajax_report3_gain_loss_search');

           // Route::post('/chartsearch', 'AdminP2BenefitsReportController@ajax_chart1_search');
            Route::post('/chartsearch', 'AdminP2BenefitsReportController@ajax_chart1_search');
           // Route::post('/dividendchart', 'AdminP2BenefitsReportController@ajax_chart_dividend_search');

            Route::get('/exportsearch', 'AdminP2BenefitsReportController@ajax_report1_search_export');

        }); 

        /* รายงาน เป็น เมนูสำหรับตรวจสอบรายงาน ค่านายหน้า ซื้อ - ขายหลักทรัพย์ (Equity Brokerage Report) No. 7 */ 
        Route::group(['prefix' => 'EquityBrokerageReport'], function() {
            Route::get('/',             'AdminP2EquityBrokerageReportController@getreport');
            Route::post('/search',      'AdminP2EquityBrokerageReportController@ajax_report_search');
            Route::post('/chartsearch', 'AdminP2EquityBrokerageReportController@ajax_chart_search');

            Route::get('/exportsearch', 'AdminP2EquityBrokerageReportController@ajax_report_search_export');
            Route::post('/saveheader',   'AdminP2EquityBrokerageReportController@ajax_store_customize_field');
            Route::post('/readheader',   'AdminP2EquityBrokerageReportController@ajax_retrive_customize_field');
        });


        /* รายงาน เปรียบเทียบขอบเขตการลงทุน ในตราสารทุน Stock Universe (Stock Universe Report) No. 7 */ 
        Route::group(['prefix' => 'EquityStockUniverseReport'], function() {
            Route::get('/',             'AdminP2EquityStockUniverseReportController@getreport');
            Route::post('/search',      'AdminP2EquityStockUniverseReportController@ajax_report_search');
            Route::get('/exportsearch', 'AdminP2EquityStockUniverseReportController@ajax_report_search_export');

        }); 

        /* รายงาน ค่าใช้จ่าย (Equity Expense Report) Group:63 No.8 */
        Route::group(['prefix' => 'EquityExpenseReport'], function() {
            Route::get('/',             'AdminP2EquityExpenseReportController@getreport');
            Route::post('/search',      'AdminP2EquityExpenseReportController@ajax_report_search');
            Route::get('/exportsearch', 'AdminP2EquityExpenseReportController@ajax_report_search_export');

            Route::post('/saveheader',  'AdminP2EquityExpenseReportController@ajax_store_customize_field');
            Route::post('/readheader',  'AdminP2EquityExpenseReportController@ajax_retrive_customize_field');
        });
        
        
        /* รายงาน กช  report (KC Report) Group:64 No.1 */
        Route::group(['prefix' => 'AdminKorchorReport'], function() {
            
            Route::get('/',         'AdminKorchorReportController@getindex');
            Route::get('add',       'AdminKorchorReportController@getAdd');
            Route::get('getimport', 'AdminKorchorReportController@getimport');

            Route::post('add',    'AdminKorchorReportController@postAdd');
            Route::post('getall', 'AdminKorchorReportController@Ajax_Index');
            Route::post('delete', 'AdminKorchorReportController@delete');

            Route::post('search', 'AdminKorchorReportController@ajax_index_search');
            

            Route::post('upload', 'AdminKorchorReportController@multiple_upload'); 

            Route::post('check', 'AdminKorchorReportController@T2_checkfile'); 
            
            Route::get('downloadKC', 'AdminKorchorReportController@T2_download_korchor');

       });

      

        /**
         * Phase#2 Equity Category  
         * Created: 2016/11/27 00:06
         * menu-id = 60
         */
      /*  Route::group(['prefix' => 'EquityCategory'], function() {
             
            Route::get('/',         'EquityCategoryController@getindex');  // -> VIEW: p2_tab2_equity_category_page.blade.php
            Route::get('add',       'EquityCategoryController@getAdd');    // -> VIEW: p2_tab2_add_equity_category_page.blade.php 
            Route::get('edit/{id}', 'EquityCategoryController@getEdit');   // -> VIEW: p2_tab2_edit_equity_category_page.blade.php 
            
             
            Route::post('add',      'EquityCategoryController@postAdd');   
            Route::post('edits',    'EquityCategoryController@postEdit');
            Route::post('delete',   'EquityCategoryController@delete');
            Route::post('getall',   'EquityCategoryController@Ajax_Index'); // VIEW: ajax_p2_equity_category.blade.php 

        });

*/
        


        /**
         * Phase#2 Bond Company Management (ตราสารหนั้)
         * Created: 2016/11/27 00:06
         * menu-id = 61
         */
        Route::group(['prefix' => 'BondCompany'], function() {
            /// TAB#1
            /**
             * route GET views for Bond Company Management
             */
            Route::get('/',         'BondCompanyManagementController@getindex');
            Route::get('add',       'BondCompanyManagementController@getAdd');
            Route::get('edit/{id}', 'BondCompanyManagementController@getEdit');//->where('id', '^[a-zA-Z][a-zA-Z0-9_\\-]+$');

        
            /**
             * route POST methods Bond Company Management
             */
            Route::post('add',    'BondCompanyManagementController@postAdd');
            Route::post('edits',  'BondCompanyManagementController@postEdit');
            Route::post('delete', 'BondCompanyManagementController@delete');
            Route::post('getall', 'BondCompanyManagementController@Ajax_Index');

            ////////////////////////////////////////////////////////////////////////////////////
            // TAB#2
            ////////////////////////////////////////////////////////////////////////////////////
            /**
             * route GET views for Bond Category
             */
            Route::get('category',          'BondCompanyManagementController@getindexCategory');  // -> VIEW: p2_tab2_equity_category_page.blade.php
            Route::get('addCategory',       'BondCompanyManagementController@getAddCategory');    // -> VIEW: p2_tab2_add_equity_category_page.blade.php 
            Route::get('editCategory/{id}', 'BondCompanyManagementController@getEditCategory');   // -> VIEW: 
            Route::get('getimportCategory', 'BondCompanyManagementController@getimportCategory');
            Route::get('dowloadsampleBondCategory',    'BondCompanyManagementController@dowloadsampleBondCategory');
            
            /**
             * route POST methods Bond Company Management
             */
            Route::post('addCategory',    'BondCompanyManagementController@postAddCategory');
            Route::post('editsCategory',  'BondCompanyManagementController@postEditCategory');
            Route::post('deleteCategory', 'BondCompanyManagementController@deleteCategory');
            Route::post('getallCategory', 'BondCompanyManagementController@Ajax_Index_Category');

            Route::post('importCategory', 'BondCompanyManagementController@importCategory');




            ///////////////////////////////////////////////////////////////////////////////////
            // TAB#3
            ////////////////////////////////////////////////////////////////////////////////////
            /**
             * route GET views for Bond Category
             */
            Route::get('bondindex',              'BondCompanyManagementController@getindexBondIndex');  // -> VIEW: p2_tab3_equity_securities_page.blade.php
            Route::get('addBondIndex',           'BondCompanyManagementController@getAddBondIndex');    // -> VIEW: p2_tab3_add_equity_securities_page.blade.php 
            Route::get('editBondIndex/{id}',     'BondCompanyManagementController@getEditBondIndex');   // -> VIEW: 
            Route::get('getimportBondIndex',     'BondCompanyManagementController@getimportBondIndex');
            Route::get('dowloadsampleBondIndex', 'BondCompanyManagementController@dowloadsampleBondIndex');

            
             /**
             * route POST methods Bond Company Management
             */

            Route::post('addBondIndex',    'BondCompanyManagementController@postAddBondIndex');
            Route::post('editsBondIndex',  'BondCompanyManagementController@postEditBondIndex');
            Route::post('deleteBondIndex', 'BondCompanyManagementController@deleteBondIndex');
            Route::post('getallBondIndex', 'BondCompanyManagementController@Ajax_Index_BondIndex');
           
            Route::post('importBondIndex', 'BondCompanyManagementController@importBondIndex');
            Route::post('getBondIndexSearchForm', 'BondCompanyManagementController@Ajax_BondIndexSearchForm');
           

            ///////////////////////////////////////////////////////////////////////////////////
            /// TAB#4 
            ///////////////////////////////////////////////////////////////////////////////////
            /**
             * route GET views for Bond Broker
             */
            Route::get('bondBroker',      'BondCompanyManagementController@getindexBroker');
            Route::get('addBroker',       'BondCompanyManagementController@getAddBroker');
            Route::get('editBroker/{id}', 'BondCompanyManagementController@getEditBroker');//->where('id', '^[a-zA-Z][a-zA-Z0-9_\\-]+$');

            /**
             * route POST methods Bond Broker
             */
            Route::post('addBroker',      'BondCompanyManagementController@postAddBroker'); 
            Route::post('editsBroker',    'BondCompanyManagementController@postEditBroker');
            Route::post('deleteBroker',   'BondCompanyManagementController@deleteBroker');
            Route::post('getallBroker',   'BondCompanyManagementController@Ajax_Index_Broker');

        });

/**
         * Phase#2 Bond Data Import (ตราสารหนี้)
         * Created: 2016/11/27 00:06
         * menu-id = 61.2
         */
        Route::group(['prefix' => 'BondDataImport'], function() {
            /// TAB#1
            /**
             * route GET views for Bond 
             */
            Route::get('/',           'BondDataImportController@T2_getimport');
            //Route::get('add',       'BondDataImportController@getAdd');
            //Route::get('edit/{id}', 'BondDataImportController@getEdit');//->where('id', '^[a-zA-Z][a-zA-Z0-9_\\-]+$');

           /**
             * 1) Trading Transaction 
             */  
            Route::post('t2checkTreadingTransactionsFile', 'BondDataImportController@T2_checkTradingTransactionsFile'); 
            Route::post('t2importTradingTransactionsFile', 'BondDataImportController@T2_importTradingTransactionsFile'); 
            Route::get('dowloadsampleTradingTransactions', 'BondDataImportController@T2_downloadSampleTradingTransactions');

           /**
             * 2) Portfolio
             */ 
            Route::post('t2checkPortfolioFile',   'BondDataImportController@T2_checkPortfolioFile'); 
            Route::post('t2importPortfolioFile ',     'BondDataImportController@T2_importPortfolioFile'); 
            Route::get('dowloadsamplePortfolio', 'BondDataImportController@T2_downloadSamplePortfolio');

            
            /**
             * route POST methods Bond  
             */
           // Route::post('add',    'BondDataImportController@postAdd');
           // Route::post('edits',  'BondDataImportController@postEdit');
           // Route::post('delete', 'BondDataImportController@delete');
           // Route::post('getall', 'BondDataImportController@Ajax_Index');

            ////////////////////////////////////////////////////////////////////////////////////
            // START: TAB#2
            ////////////////////////////////////////////////////////////////////////////////////
            /**
             * route GET views for Bond DataImport
             */
            //Route::get('getimport',             'BondDataImportController@T2_getimport');
            //Route::get('dowloadsampleEquity',   'BondDataImportController@T2_dowloadsampleEquity');
            Route::get('dowloadsampleBond',     'BondDataImportController@T2_dowloadsampleBond');
            
             /**
             * route POST methods Bond DataImport 
             */
            Route::post('t2check',   'BondDataImportController@T2_checkfile'); 
            Route::post('t2import',  'BondDataImportController@T2_ImportData'); 
            Route::post('t2edits',   'BondDataImportController@T2_postEdit');
            Route::post('t2delete',  'BondDataImportController@T2_delete');
            Route::post('t2getall',  'BondDataImportController@T2_Ajax_Index');
            

            /**
             * 1) Check Purchase and Sale import file  (Menu 1. Bond Import ข้อมูลซื้อ - ขายหลักทรัพย์)
             */ 
            //Route::post('t2checkPurchase',      'BondDataImportController@T2_checkPurchaseAndSaleFile'); 
            //Route::post('t2importPurchase',     'BondDataImportController@T2_importPurchaseAndSaleFile'); 
           // Route::get('dowloadsamplePurchase', 'BondDataImportController@T2_downloadSamplePurchase');

           

            /*
             * 3) เป็นเมนูสำหรับ นําเข้าข้อมูล มูลค่า หน่วยลงทุน และอัตราผลตอบแทนเข้าสู่ระบบกองทุน ฯ
             */ 
            Route::get('editUnitVal/{id}',    'BondDataImportController@getEditUnitVal'); 
            Route::get('getAddUnitVal',       'BondDataImportController@getAddUnitVal'); 
            
            Route::post('addUnitVal',           'BondDataImportController@postAddUnitVal');
            Route::post('editsUnitVal',         'BondDataImportController@postEditsUnitVal');
            Route::post('deleteUnitValRecords', 'BondDataImportController@deleteUnitValRecords'); 
                  
            Route::post('bondUnitValIndex',   'BondDataImportController@Ajax_Index_BondUnitValIndex');           
            Route::post('bondUnitValIndexSearchForm', 'BondDataImportController@Ajax_BondUnitValIndexSearchForm');
           


            /**
             * 7) Check GainLoss import file  (Menu 7. Import ข้อมูลรายละเอียดผลประโยชน์การลงทุน)
             */ 
            //Route::post('t2checkGainLoss',      'BondDataImportController@T2_checkGainLoss'); 
            //Route::post('t2importGainLoss',     'BondDataImportController@T2_importGainLoss'); 
            //Route::get('dowloadsampleGainLoss', 'BondDataImportController@T2_downloadSampleGainLoss');

 
            /**
             * 8) Check Stock Universe import file  (Menu 8. Import Stock Universe)
             */ 
            //Route::post('t2checkStockUniverse',      'BondDataImportController@T2_checkStockUniverseFile'); 
            //Route::post('t2importStockUniverse',     'BondDataImportController@T2_importStockUniverseFile'); 
            //Route::get('dowloadsampleStockUniverse', 'BondDataImportController@T2_downloadSampleStockUniverse');

            // END: TAB2 (Bond Dataimport)
        });

        /* รายงาน ตราสารหนี้ - รายงานพอร์ตการลงทุน (Bond Port Investment Report) No.1 (63.1) */ 
        Route::group(['prefix' => 'BondPortInvestmentReport'], function() {
            Route::get('/',             'AdminP2BondPortInvestmentReportController@getreport');
            Route::post('/search',      'AdminP2BondPortInvestmentReportController@ajax_report_search');
            Route::post('/chartsearch', 'AdminP2BondPortInvestmentReportController@ajax_chart_search');

            Route::get('/exportsearch', 'AdminP2BondPortInvestmentReportController@ajax_report_search_export');
            Route::post('/saveheader',  'AdminP2BondPortInvestmentReportController@ajax_store_customize_field');
            Route::post('/readheader',  'AdminP2BondPortInvestmentReportController@ajax_retrive_customize_field');
        });


        /* รายงาน ตราสารหนี้ -  รายงานการซื้อ-ขาย (Bond Trading Report) No.2 (63.2) */ 
        Route::group(['prefix' => 'BondTradingReport'], function() {
            Route::get('/',             'AdminP2BondTradingReportController@getreport');
            Route::post('/search',      'AdminP2BondTradingReportController@ajax_report_search');
            Route::post('/chartsearch', 'AdminP2BondTradingReportController@ajax_chart_search');

            Route::get('/exportsearch', 'AdminP2BondTradingReportController@ajax_report_search_export');
            Route::post('/saveheader',  'AdminP2BondTradingReportController@ajax_store_customize_field');
            Route::post('/readheader',  'AdminP2BondTradingReportController@ajax_retrive_customize_field');
        });

        /* รายงาน ตราสารหนี้ - รายงานผลประโยชน์การลงทุน (Bond Benefits Report) No. 3 */ 
        Route::group(['prefix' => 'BondBenefitsReport'], function() {
            Route::get('/',             'AdminP2BondBenefitsReportController@getreport');
            Route::post('/search',      'AdminP2BondBenefitsReportController@ajax_report_search');
            Route::post('/chartsearch', 'AdminP2BondBenefitsReportController@ajax_chart_search');
            
            Route::get('/exportsearch', 'AdminP2BondBenefitsReportController@ajax_report_search_export');
            Route::post('/saveheader',  'AdminP2BondBenefitsReportController@ajax_store_customize_field');
            Route::post('/readheader',  'AdminP2BondBenefitsReportController@ajax_retrive_customize_field');
         
        });
        
        /* รายงาน ตราสารหนี้ (Bond NAV Report) No. 5 */ 
        Route::group(['prefix' => 'BondNavReport'], function() {
            /* main */
            Route::get('/',             'AdminP2BondNavReportController@getreport');
            Route::post('/search',      'AdminP2BondNavReportController@ajax_report_search');
            Route::post('/chartsearch', 'AdminP2BondNavReportController@ajax_chart_search');
            
            Route::get('/exportsearch', 'AdminP2BondNavReportController@ajax_report_search_export');
            Route::post('/saveheader',  'AdminP2BondNavReportController@ajax_store_customize_field');
            Route::post('/readheader',  'AdminP2BondNavReportController@ajax_retrive_customize_field');
         
        });

     /* รายงาน ตราสารหนี้ (Bond Portfolio Report) No.2 (63.3) */ 
       /* Route::group(['prefix' => 'BondPortfolioReport'], function() {
            Route::get('/',             'AdminP2BondPortfolioReportController@getreport');
            Route::post('/search',      'AdminP2BondPortfolioReportController@ajax_report_search');
            Route::post('/chartsearch', 'AdminP2BondPortfolioReportController@ajax_chart_search');

            Route::get('/exportsearch', 'AdminP2BondPortfolioReportController@ajax_report_search_export');
            Route::post('/saveheader',  'AdminP2BondPortfolioReportController@ajax_store_customize_field');
            Route::post('/readheader',  'AdminP2BondPortfolioReportController@ajax_retrive_customize_field');
        });
        */
       

//        Route::group(['prefix' => 'report2'], function() {
//            Route::get('/', 'AdminReportController@getreport2');
//
//        });
//        Route::group(['prefix' => 'report3'], function() {
//            Route::get('/', 'AdminReportController@getreport3');
//
//        });
//        Route::group(['prefix' => 'report4'], function() {
//            Route::get('/', 'AdminReportController@getreport4');
//
//        });
//        Route::group(['prefix' => 'report5'], function() {
//            Route::get('/', 'AdminReportController@getreport5');
//
//        });
//        Route::group(['prefix' => 'report6'], function() {
//            Route::get('/', 'AdminReportController@getreport6');
//
//        });
//        Route::group(['prefix' => 'report7'], function() {
//            Route::get('/', 'AdminReportController@getreport7');
//
//        });
//        Route::group(['prefix' => 'report8'], function() {
//            Route::get('/', 'AdminReportController@getreport8');
//
//        });
//        Route::group(['prefix' => 'report9'], function() {
//            Route::get('/', 'AdminReportController@getreport9');
//
//        });
//        Route::group(['prefix' => 'report10'], function() {
//            Route::get('/', 'AdminReportController@getreport10');
//
//        });
//        Route::group(['prefix' => 'report11'], function() {
//            Route::get('/', 'AdminReportController@getreport11');
//
//        });
//        Route::group(['prefix' => 'report12'], function() {
//            Route::get('/', 'AdminReportController@getreport12');
//
//        });
//        Route::group(['prefix' => 'report13'], function() {
//            Route::get('/', 'AdminReportController@getreport13');
//
//        });

//        end report



    });

});