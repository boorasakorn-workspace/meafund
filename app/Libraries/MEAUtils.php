<?php
namespace App\Libraries;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
class MEAUtils {
	 /**
	 * Convert  datetime to Thai datetime
	 * @param string $strDate type of datetime (for example, 2016-12-25 23:30:34)
	 * @param bool flag $withTime, if TRUE will connvert input with date and time, otherwise only Thai date 
	 *        will process.
	 * @return Thai datetime formatted (for example, 12 ส.ค. 2559)
	 */
	function toThaiDateTime($strDate, $withTime)
	{       
	    try { 
	        $strYear = date("Y",strtotime($strDate)) + 543;
	        $strMonth = date("n",strtotime($strDate));
	        $strDay = date("j",strtotime($strDate));
	        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
	        $strMonthThai = $strMonthCut[$strMonth];
	        if ($withTime) {
	            $strHour= date("H",strtotime($strDate));
	            $strMinute= date("i",strtotime($strDate));
	            $strSeconds= date("s",strtotime($strDate));
	            
	            return $strDay . " " . $strMonthThai. " ". $strYear .",  " . $strHour . ":" . $strMinute;
	        } 
	        return $strDay . " " . $strMonthThai. " ". $strYear ;
	    } catch (\Exception $e) {
	        Log::info($e->getMessage());
	    } 
	    return "date Error";
	}

    /**
	 * Convert month number to Thai month short name
	 * @param integer $monthNum type of month (for example, 12)
	 * @return Thai short month from specified month (for example, ส.ค.)
	 */
	function toThaiShortMonth($monthNum) {
		try { 
			$strMonth = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        	return  $strMonth[$monthNum];      
        } catch (\Exception $e) {
            Log::info($e->getMessage());
        }   
        return "";     
	}

	function toEnglishDate($strDate) {
        $v = explode(' ', $strDate);
        if(count($v) < 2) return "date Error";
        $monthNum = '';
        try { 
            if($v[1] == "ม.ค.")  $monthNum ='1';
            if($v[1] == "ก.พ.")  $monthNum ='2';
            if($v[1] == "มี.ค.") $monthNum ='3';
            if($v[1] == "เม.ย.") $monthNum ='4';
            if($v[1] == "พ.ค.")  $monthNum ='5';
            if($v[1] == "มิ.ย.") $monthNum ='6';
            if($v[1] == "ก.ค.")  $monthNum ='7';
            if($v[1] == "ส.ค.")  $monthNum ='8';
            if($v[1] == "ก.ย.")  $monthNum ='9';
            if($v[1] == "ต.ค.")  $monthNum ='10';
            if($v[1] == "พ.ย.")  $monthNum ='11';
            if($v[1] == "ธ.ค.")  $monthNum ='12';
            //$ar = array($v[0], $monthNum, $v[2]);
            //$newstrDate = implode('-', $ar);
            //$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."); 
            $newstrDate = str_replace($v[1], $monthNum, $strDate);
            $newstrDate = str_replace(" ", "-", $newstrDate);
            return $newstrDate;

        } catch (\Exception $e) {
            Log::info($e->getMessage());
        } 
        return $strDate;
    }

    function getThaiMonth($month) {
        $thai_month_arr=array(
            "0"=>"",
            "1"=>"มกราคม",
            "2"=>"กุมภาพันธ์",
            "3"=>"มีนาคม",
            "4"=>"เมษายน",
            "5"=>"พฤษภาคม",
            "6"=>"มิถุนายน", 
            "7"=>"กรกฎาคม",
            "8"=>"สิงหาคม",
            "9"=>"กันยายน",
            "10"=>"ตุลาคม",
            "11"=>"พฤศจิกายน",
            "12"=>"ธันวาคม");
        return $thai_month_arr[$month];
    }

    function toEnglishDateEx($strDate) {
        $v = explode(' ', $strDate);
        if(count($v) < 2) return "date Error";
        $monthNum = '';

        $thai_month_arr=array(
		    "0"=>"",
		    "1"=>"มกราคม",
		    "2"=>"กุมภาพันธ์",
		    "3"=>"มีนาคม",
		    "4"=>"เมษายน",
		    "5"=>"พฤษภาคม",
		    "6"=>"มิถุนายน", 
		    "7"=>"กรกฎาคม",
		    "8"=>"สิงหาคม",
		    "9"=>"กันยายน",
		    "10"=>"ตุลาคม",
		    "11"=>"พฤศจิกายน",
		    "12"=>"ธันวาคม");   

        try { 
           /* if($v[1] == $thai_month_arr[1])  $monthNum ='1';
            if($v[1] == $thai_month_arr[2])  $monthNum ='2';
            if($v[1] == $thai_month_arr[3]) $monthNum ='3';
            if($v[1] == $thai_month_arr[4]) $monthNum ='4';
            if($v[1] == $thai_month_arr[5])  $monthNum ='5';
            if($v[1] == $thai_month_arr[6]) $monthNum ='6';
            if($v[1] == $thai_month_arr[7])  $monthNum ='7';
            if($v[1] == $thai_month_arr[8])  $monthNum ='8';
            if($v[1] == $thai_month_arr[9])  $monthNum ='9';
            if($v[1] == $thai_month_arr[10])  $monthNum ='10';
            if($v[1] == $thai_month_arr[11])  $monthNum ='11';
            if($v[1] == $thai_month_arr[12])  $monthNum ='12';
*/
            $monthNum = '0'; 
            foreach ($thai_month_arr as $key => $value) {
            	if($value == $v[1]) {
            		$monthNum = $key;
            		break;
            	}
            }
            //$ar = array($v[0], $monthNum, $v[2]);
            //$newstrDate = implode('-', $ar);
            //$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."); 
            $year = intval($v[2]) - 543 ;
            $day = intval($v[0]);
            $mm = intval($monthNum);

            $strDD = str_pad(strval($day), 2, '0', STR_PAD_LEFT) ;//sprintf("%02d", $day);
            $strMM = str_pad(strval($mm),  2, '0', STR_PAD_LEFT) ;
            $strYY = str_pad(strval($year),2, '0', STR_PAD_LEFT) ;

            $newstrDate = $strYY . '-' . $strMM . '-' . $strDD; //str_replace($v[1], $monthNum, $strDate);
           // $newstrDate = str_replace(" ", "-", $newstrDate);


            return $newstrDate;

        } catch (\Exception $e) {
            Log::info($e->getMessage());
        } 
        return $strDate;
    }

    function prettyPrintThaiDate($strFromDate, $strToDate, $strcolor) 
    {
        $strOutput = "";
        $date_start = $strFromDate;
        $date_end = $strToDate;
        $prefix1 = ' ข้อมูล ระหว่าง วันที่ ';
        $to = 'ถึง'; 
        $prefix2 = ' ข้อมูล วันที่ ';
        $font_tag_start = '<font color="'. $strcolor .'">';
        $font_tag_end = '</font>';

        if($strcolor =="") {
        	$font_tag_end = '';
        	$font_tag_start = '';
        }
        
        try {
        	
        	 Log::info('prettyPrintThaiDate() ');
		        if((!empty($date_start)) && (!empty($date_end))) {
	                if($date_start != $date_end) {
	                   // $strOutput = 'ข้อมูล ระหว่าง วันที่ <font color="red">'. $date_start .
	                   //     '</font> ถึง <font color="red">' . $date_end .' </font>';
	                    $strOutput = sprintf('%s %s%s%s %s %s%s%s', $prefix1, 
	                    	  $font_tag_start, $date_start, $font_tag_end,
	                    	  $to, 
	                    	  $font_tag_start, $date_end, $font_tag_end);  
	                } else { 
	                    // $strOutput = 'ข้อมูล วันที่ <font color="red">'. $date_start . '</font> เวลา
	                    //    <font color="red">00:00</font> ถึง <font color="red">23:59</font> ';   
	                     $strOutput = sprintf('%s %s%s 00:00%s %s %s%s 23:59%s', $prefix2, 
	                    	  $font_tag_start, $date_start, $font_tag_end,
	                    	  $to, 
	                    	  $font_tag_start, $date_end, $font_tag_end);  
	                }

		        } else if(!empty($date_start)) {
		        	 // $strOutput = 'ข้อมูล วันที่ <font color="red">'. $date_start . '</font> เวลา' .
		        	 //             '<font color="red">00:00</font> ถึง <font color="red">23:59</font> ';   
	                  
	                    $strOutput = sprintf('%s %s%s 00:00%s %s %s23:59%s', $prefix2, 
	                    	  $font_tag_start, $date_start, $font_tag_end,
	                    	  $to, 
	                    	  $font_tag_start,  $font_tag_end);   

		        } else {
		        	// $strOutput = 'ข้อมูล วันที่ <font color="red">' . get_date_notime(date("Y-m-d H:i:s")) . '</font>');
		        	 $strOutput = sprintf('%s %s%s 00:00%s %s %s23:59%s', $prefix2, 
	                    	  $font_tag_start, get_date_notime(date("Y-m-d H:i:s")), $font_tag_end,
	                    	  $to, 
	                    	  $font_tag_start,  $font_tag_end);   

		        }

		    Log::info('prettyPrintThaiDate:' . $strOutput);

        } catch (\Exception $e) {
            Log::info($e->getMessage());

        } 
        return $strOutput;
    }

	function getLoggedInUser() {
    	$user_data = Session::get('user_data');
        //Log::info('SESSION: [user_data] ='. $user_data->emp_id);
        return $user_data->emp_id;
    }

} // class MEAUtils
?>
