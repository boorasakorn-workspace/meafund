<?php
use Jenssegers\Date\Date;
use Illuminate\Support\Facades\DB;
/**
 * set document type
 * @param string $type type of document
 */
function set_content_type($type = 'application/json') {
    header('Content-Type: '.$type);
}

/**
 * Read CSV from URL or File
 * @param  string $filename  Filename
 * @param  string $delimiter Delimiter
 * @return array            [description]
 */
function read_csv($filename, $delimiter = ",") {
    $file_data = array();
    $handle = @fopen($filename, "r") or false;
    if ($handle !== FALSE) {
        while (($data = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
            $file_data[] = $data;
        }
        fclose($handle);
    }
    return $file_data;
}

/**
 * Print Log to the page
 * @param  mixed  $var    Mixed Input
 * @param  boolean $pre    Append <pre> tag
 * @param  boolean $return Return Output
 * @return string/void     Dependent on the $return input
 */
function plog($var, $pre=true, $return=false) {
    $info = print_r($var, true);
    $result = $pre ? "<pre>$info</pre>" : $info;
    if ($return) return $result;
    else echo $result;
}

/**
 * Log to file
 * @param  string $log Log
 * @return void
 */
function elog($log, $fn = "debug.log") {
    $fp = fopen($fn, "a");
    fputs($fp, "[".date("d-m-Y h:i:s")."][Log] $log\r\n");
    fclose($fp);
}



function getSideBar($menulsit){

    $data = $menulsit;
    

    $page_nav = array();
    $page_nav["dashboard"] = array(
        "group_id" => 0,
        "title" => "Dashboard",
        "icon" => "fa-home",
        "url" => "/admin"
    );

    if(menu_access(1,50) || menu_access(2,50)){
        $page_nav["50"] = array(
            "group_id" => 50,
            "title" => getGoupName($data,50),
            "icon" => "fa-user",

            "sub" => array(
                "1" => array(
                    "menu_id" => 1,
                    "title" => getMenuName($data,50,1),
                    "url" => "/admin/userGroup",
                    "sub_mini" => array(
                        "add" => array(
                            "title"=>"สร้างกลุ่มผู้ใช้",
                            "url" => "/admin/userGroup/add"

                        ),
                        "edit"=>array(
                            "title"=> "แก้ไขกลุ่มผู้ใช้",
                            "url" => "/admin/userGroup/edit"
                        )
                    )
                ),
                "2" => array(
                    "menu_id" => 2,
                    "title" => getMenuName($data,50,2),
                    "url" => "/admin/users",
                    "sub_mini" => array(
                        "add" => array(
                            "title"=>"สร้างผู้ใช้",
                            "url" => "/admin/users/add"

                        ),
                        "edit"=>array(
                            "title"=> "แก้ไขผู้ใช้",
                            "url" => "/admin/users/edit"
                        ),
                        "getimport"=>array(
                            "title"=> "นำเข้าผู้ใช้",
                            "url" => "/admin/users/getimport"
                        )

                    )
                )
            )
        );
    }

    if(menu_access(1,51) || menu_access(2,51)|| menu_access(3,51)|| menu_access(4,51)|| menu_access(5,51)|| menu_access(6,51)|| menu_access(7,51)){
        $page_nav["51"] = array(
            "group_id" => 51,
            "title" => getGoupName($data,51),
            "icon" => "fa-group",

            "sub" => array(
                "1" => array(
                    "menu_id" => 1,
                    "title" => getMenuName($data,51,1),
                    "url" => "/admin/simple"

                ),
                "2" => array(
                    "menu_id" => 2,
                    "title" => getMenuName($data,51,2),
                    "url" => "/admin/plan"

                ),
                "3" => array(
                    "menu_id" => 3,
                    "title" => getMenuName($data,51,3),
                    "url" => "/admin/fund"

                ),
                "4" => array(
                    "menu_id" => 4,
                    "title" => getMenuName($data,51,4),
                    "url" => "/admin/benefit"

                ),
                "5" => array(
                    "menu_id" => 5,
                    "title" => getMenuName($data,51,5),
                    "url" => "/admin/profit"

                ),
                "6" => array(
                    "menu_id" => 6,
                    "title" => getMenuName($data,51,6),
                    "url" => "/admin/extendrate"

                ),
                "7" => array(
                    "menu_id" => 7,
                    "title" => getMenuName($data,51,7),
                    "url" => "/admin/currentrate"

                )
            )
        );
    }

    if(menu_access(1,52)){
        $page_nav["52"] = array(
            "group_id" => 52,
            "title" => getGoupName($data,52),
            "icon" => "fa-gavel",

            "sub" => array(
                "1" => array(
                    "menu_id" => 1,
                    "title" => getMenuName($data,52,1),
                    "url" => "/admin/chooseplan",
                    "sub_mini" => array(
                        "add" => array(
                            "title"=>"สร้างแผนการลงทุน",
                            "url" => "/admin/chooseplan/add"

                        ),
                        "edit" => array(
                            "title"=>"แก้ไขแผนการลงทุน",
                            "url" => "/admin/chooseplan/edit"

                        )
                    )

                )

            )
        );
    }
    if(menu_access(1,53) || menu_access(2,53)){
        $page_nav["53"] = array(
            "group_id" => 53,
            "title" => getGoupName($data,53),
            "icon" => "fa-bullhorn",

            "sub" => array(
                "1" => array(
                    "menu_id" => 1,
                    "title" => getMenuName($data,53,1),
                    "url" => "/admin/newstopic",
                    "sub_mini" => array(
                        "add" => array(
                            "title"=>"สร้างหมวดหมู่ข่าว",
                            "url" => "/admin/newstopic/add"

                        ),
                        "edit" => array(
                            "title"=>"แกไขหมวดหมู่ข่าว",
                            "url" => "/admin/newstopic/edit"

                        )
                    )

                ),
                "2" => array(
                    "menu_id" => 2,
                    "title" => getMenuName($data,53,2),
                    "url" => "/admin/news",
                    "sub_mini" => array(
                        "add" => array(
                            "title"=>"สร้างหัวข้อข่าว",
                            "url" => "/admin/news/add"

                        ),
                        "edit" => array(
                            "title"=>"แก้ไขหัวข้อข่าว",
                            "url" => "/admin/news/edit"

                        )
                    )

                )

            )
        );
    }

    if(menu_access(1,54) || menu_access(2,54)){
        $page_nav["54"] = array(
            "group_id" => 54,
            "title" => getGoupName($data,54),
            "icon" => "fa-comments-o",

            "sub" => array(
                "1" => array(
                    "menu_id" => 1,
                    "title" => getMenuName($data,54,1),
                    "url" => "/admin/faqcate",
                    "sub_mini" => array(
                        "add" => array(
                            "title"=>"สร้างหมวดหมู่หัวข้อถาม-ตอบ",
                            "url" => "/admin/faqcate/add"

                        ),
                        "edit" => array(
                            "title"=>"แก้ไข้หมวดหมู่หัวข้อถาม-ตอบ",
                            "url" => "/admin/faqcate/edit"

                        )
                    )

                ),
                "2" => array(
                    "menu_id" => 2,
                    "title" => getMenuName($data,54,2),
                    "url" => "/admin/faqtopic",
                    "sub_mini" => array(
                        "add" => array(
                            "title"=>"สร้างหัวข้อถาม-ตอบ",
                            "url" => "/admin/faqtopic/add"

                        ),
                        "edit" => array(
                            "title"=>"แก้ไขหัวข้อถาม-ตอบ",
                            "url" => "/admin/faqtopic/edit"

                        )
                    )

                )

            )
        );
    }

    if(menu_access(1,55) || menu_access(2,55)){
        $page_nav["55"] = array(
            "group_id" => 55,
            "title" => getGoupName($data,55),
            "icon" => "fa-map-marker",

            "sub" => array(
                "1" => array(
                    "menu_id" => 1,
                    "title" => getMenuName($data,55,1),
                    "url" => "/admin/contact"


                ),
                "2" => array(
                    "menu_id" => 2,
                    "title" => getMenuName($data,55,2),
                    "url" => "/admin/cmail",
                    "sub_mini" => array(
                        "add" => array(
                            "title"=>"ตอบกลับ",
                            "url" => "/admin/cmail/forward"

                        ),
                        "edit" => array(
                            "title"=>"ส่งต่อ",
                            "url" => "/admin/cmail/reply"

                        )
                    )

                )

            )
        );
    }

    if(menu_access(1,56)){
        $page_nav["56"] = array(
            "group_id" => 56,
            "title" => getGoupName($data,56),
            "icon" => "fa-signal",

            "sub" => array(
                "1" => array(
                    "menu_id" => 1,
                    "title" => getMenuName($data,56,1),
                    "url" => "/admin/risk",
                    "sub_mini" => array(
                        "add" => array(
                            "title"=>"สร้างแบบประเมินความเสี่ยง",
                            "url" => "/admin/risk/add"

                        ),
                        "edit" => array(
                            "title"=>"แก้ไขแบบประเมินความเสี่ยง",
                            "url" => "/admin/risk/edit"

                        )
                    )


                )


            )
        );
    }

    if(menu_access(1,57)){
        $page_nav["57"] = array(
            "group_id" => 57,
            "title" => getGoupName($data,57),
            "icon" => "fa-trophy",

            "sub" => array(
                "1" => array(
                    "menu_id" => 1,
                    "title" => getMenuName($data,57,1),
                    "url" => "/admin/nav",
                    "sub_mini" => array(
                        "add" => array(
                            "title"=>"เพิ่มมูลค่าทรัพย์สินสุทธิ",
                            "url" => "/admin/nav/getadd"

                        )
                    )

                )

            )
        );
    }


    if(menu_access(1,58) || menu_access(2,58)|| menu_access(3,58)|| menu_access(4,58)|| menu_access(5,58)|| menu_access(6,58)|| menu_access(7,58)|| menu_access(8,58)|| menu_access(9,58)|| menu_access(10,58)|| menu_access(11,58)|| menu_access(12,58)|| menu_access(13,58)){
        $page_nav["58"] = array(
            "group_id" => 58,
            "title" => getGoupName($data,58),
            "icon" => "fa-table",

            "sub" => array(
                "1" => array(
                    "menu_id" => 1,
                    "title" => getMenuName($data,58,1),
                    "url" => "/admin/report1"


                ),
                "2" => array(
                    "menu_id" => 2,
                    "title" => getMenuName($data,58,2),
                    "url" => "/admin/report2"

                ),
                "3" => array(
                    "menu_id" => 3,
                    "title" => getMenuName($data,58,3),
                    "url" => "/admin/report3"

                ),
                "4" => array(
                    "menu_id" => 4,
                    "title" => getMenuName($data,58,4),
                    "url" => "/admin/report4"

                ),
                "5" => array(
                    "menu_id" => 5,
                    "title" => getMenuName($data,58,5),
                    "url" => "/admin/report5"

                ),
                "6" => array(
                    "menu_id" => 6,
                    "title" => getMenuName($data,58,6),
                    "url" => "/admin/report6"

                ),
                "7" => array(
                    "menu_id" => 7,
                    "title" => getMenuName($data,58,7),
                    "url" => "/admin/report7"

                ),
                "8" => array(
                    "menu_id" => 8,
                    "title" => getMenuName($data,58,8),
                    "url" => "/admin/report8"

                ),
                "9" => array(
                    "menu_id" => 9,
                    "title" => getMenuName($data,58,9),
                    "url" => "/admin/report9"

                ),
                "10" => array(
                    "menu_id" => 10,
                    "title" => getMenuName($data,58,10),
                    "url" => "/admin/report10"

                ),
                "11" => array(
                    "menu_id" => 11,
                    "title" => getMenuName($data,58,11),
                    "url" => "/admin/report11"

                ),
                "12" => array(
                    "menu_id" => 12,
                    "title" => getMenuName($data,58,12),
                    "url" => "/admin/report12"

                ),
                "13" => array(
                    "menu_id" => 13,
                    "title" => getMenuName($data,58,13),
                    "url" => "/admin/report13"

                )
                /*,*/
                
				/* Phase#2  */
                /*"14" => array(
                    "menu_id" => 14,
                    "title" => getMenuName($data,58,14),
                    "url" => "/admin/EnquityCompany",

                    "sub_mini" => array(
                        "add" => array(
                            "title"=>"เพิ่มบริษัทจัดการ", //"สร้างแผนการลงทุน",
                            "url" => "/admin/EnquityCompany/add"

                        ),
                        "edit" => array(
                            "title"=>"แก้ไขบริษัทจัดการ",
                            "url" => "/admin/EnquityCompany/edit"

                        )
                    )

                ),
				"15" => array(
                    "menu_id" => 15,
                    "title" => getMenuName($data,58,15),
                    "url" => "/admin/report13"

                )	*/
            )
        );
    }

    if(menu_access(1,59) || menu_access(5,59) || menu_access(3,59) || menu_access(4,59)){
        $page_nav["59"] = array(
            "group_id" => 59,
            "title" => getGoupName($data,59),
            "icon" => "fa-table",

            "sub" => array(
                "1" => array(
                    "menu_id" => 1,
                    "title" => getMenuName($data,59,1),
                    "url" => "/admin/con1"
                ),

                "3" => array(
                    "menu_id" => 3,
                    "title" => getMenuName($data,59,3),
                    "url" => "/admin/con3"
                ),
                "4" => array(
                    "menu_id" => 4,
                    "title" => getMenuName($data,59,4),
                    "url" => "/admin/con4"
                ),
                "5" => array(
                    "menu_id" => 5,
                    "title" => getMenuName($data,59,5),
                    "url" => "/admin/con5"
                )
            )
        );
    }

    /**
     * Phase#2 Menu 60) ตราสานทุน
     */
    // print_r($data);
    if(menu_access(1, 60) || menu_access(2, 60)) {

        //getMenuName($data, 60, 1). "บริษัทจัดการ"
        $menu_title = "บริษัทจัดการ" ;//getMenuName($data, 60, 1);  
        //if (!(preg_match('/บริษัทจัดการ/', $menu_title))) {
        //    $menu_title = getMenuName($data, 60, 1) . ' / '. " บริษัทจัดการ ";
        //}

        $page_nav["60"] = array(
            "group_id" => 60,
            "title" => getGoupName($data, 60),
            "icon" => "fa-gavel",

            "sub" => array (
                

                "1" => array (
                    "menu_id" => 1,
                    "title" => getMenuName($data, 60, 1),
                    "url" => "/admin/EquityCompany",
                    "sub_mini" => array(
                        
                        /* TAB#1 */
                        "add" => array(
                            "title"=>"เพิ่มข้อมูลบริษัทจัดการ",
                            "url" => "/admin/EquityCompany/add"

                        ),

                        /* TAB#1 */ 
                        "edit" => array(
                            "title"=>"แก้ไขข้อมูลบริษัทจัดการ",
                            "url" => "/admin/EquityCompany/edit"

                        )
						

                        /* TAB#2 */
						,"category" => array (
                            "title"=> "หมวดหมู่ หลักทรัพย์    ",
                            "url" => "/admin/EquityCompany/category"
                            /*,

                            "sub_mini_level_1" => array ( 
                                "addcategory" => array (
                                    "title"=>"เพิ่มข้อมูล หมวดหมู่ หลักทรัพย์    ",
                                    "url" => "/admin/EquityCompany/category/addCategory"
                                ),

                                "editcategory" => array (
                                    "title"=>"แก้ไขข้อมูล หมวดหมู่ หลักทรัพย์  ",
                                    "url" => "/admin/EquityCompany/category/editCategory"
                                )
                            )*/
                        )

                        /* TAB#2 */
						,"addcategory" => array(
                            "title"=>"เพิ่มข้อมูล หมวดหมู่ หลักทรัพย์    ",
                            "url" => "/admin/EquityCompany/addCategory"

                        ),
						
                        /* TAB#2 */
                        "editcategory" => array(
                            "title"=>"แก้ไขข้อมูล หมวดหมู่ หลักทรัพย์  ",
                            "url" => "/admin/EquityCompany/editCategory"

                        )

                        /* TAB#2 */
                        ,"getimportCategory" => array(
                            "title"=>"นำเข้าข้อมูล หมวดหมู่ หลักทรัพย์  ",
                            "url" => "/admin/EquityCompany/getimportCategory"

                        )

                        //////////////
                        // TAB#3 
                        //////////////
                        ,"equityindex" => array (
                            "title"=> "ข้อมูลหลักทรัพย์    ",
                            "url" => "/admin/EquityCompany/equityindex"
                        )

                        /* TAB#3 */
                        ,"addEquityIndex" => array(
                            "title"=>"เพิ่มข้อมูล หลักทรัพย์  ",
                            "url" => "/admin/EquityCompany/addEquityIndex"

                        ),
                        
                        /* TAB#3 */
                        "editEquityIndex" => array(
                            "title"=>"แก้ไขข้อมูล หลักทรัพย์  ",
                            "url" => "/admin/EquityCompany/editEquityIndex"

                        ),
                        /* TAB#3 */
                        "getimportEquityIndex" => array(
                            "title"=>"นำเข้าข้อมูล หลักทรัพย์  ",
                            "url" => "/admin/EquityCompany/getimportEquityIndex"

                        )
                        
                        //////////////
                        // TAB#4 Equity Broker
                        //////////////
                        ,"equityBroker" => array (
                            "title"=> "บริษัท Broker",
                            "url" => "/admin/EquityCompany/equityBroker"
                        )

                        /* TAB#4 */
                        ,"addBroker" => array(
                            "title"=>"เพิ่มข้อมูล บริษัท Broker ",
                            "url" => "/admin/EquityCompany/addBroker"

                        ),
                        
                        /* TAB#4 */
                        "editBroker" => array(
                            "title"=>"แก้ไขข้อมูล บริษัท Broker ",
                            "url" => "/admin/EquityCompany/editBroker"

                        )
                    )
			    ),
                    
                /* START: group[60], submenu[2] นำเข้าตราสารทุน (60.2) */
                "2" => array (
                    "menu_id" => 2,
                    "title" => getMenuName($data, 60, 2),
                    "url" => "/admin/EquityDataImport",

                    "sub_mini" => array(
                        
                        /* TAB#1 */
                        "add" => array(
                            "title"=>"เพิ่มข้อมูล ตราสารทุน",
                            "url" => "/admin/EquityDataImport/add"
                        ),

                        /* TAB#1 */ 
                        "edit" => array(
                            "title"=>"แก้ไขข้อมูล ตราสารทุน",
                            "url" => "/admin/EquityDataImport/edit"
                        ),

                        /* TAB#2 */
                        "datalist" => array (
                            "title"=> "ข้อมุลนำเข้าในระบบ",
                            "url" => "/admin/EquityDataImport/datalist"
                        ),
                            
                        /* TAB#2 */
                        "editdata" => array(
                            "title"=>"แก้ไขข้อมูล ตราสารทุน ประเภทต่างๆในระบบกองทุนฯ ",
                            "url" => "/admin/EquityDataImport/editdata"
                        ),

                        /* TAB#2 */
                        "getimport" => array(
                                "title"=>"นำเข้าข้อมูล ตราสารทุน ประเภทต่างๆในระบบกองทุนฯ  ",
                                "url" => "/admin/EquityDataImport/getimport"

                        ),

                        /* เป็นเมนูสำหรับนําเข้าข้อมูล มูลค่า หน่วยลงทุน และอัตราผลตอบแทนเข้าสู่ระบบกองทุน ฯ */
                        /* TAB#2 */
                        "getAddUnitVal" => array(
                            "title"=>"เพิ่มข้อมูล มูลค่า หน่วยลงทุน และอัตราผลตอบแทนเข้าสู่ระบบกองทุน ฯ ",
                            "url" => "/admin/EquityDataImport/getAddUnitVal"
                        ),

                        /* TAB#2 */ 
                        "editUnitVal" => array(
                            "title"=>"แก้ไขข้อมูล ข้อมูล มูลค่า หน่วยลงทุน และอัตราผลตอบแทนเข้าสู่ระบบกองทุน ฯ",
                            "url" => "/admin/EquityDataImport/editUnitVal"
                        )

                    ) // mini

                ) /* END: group[60], submenu[2] */
               
            )
        );
    }
    

    /**
     * Phase#2 Menu 61) Bond ตราสานหนี้
     */

    if(menu_access(1, 61) || menu_access(2, 61)) {

        $menu_title = getMenuName($data, 61, 1);  
        
        $page_nav["61"] = array (
            "group_id" => 61,
            "title" => getGoupName($data, 61),
            "icon" => "fa-gavel",

            "sub" => array (
                "1" => array (
                    "menu_id" => 1,
                    "title" => getMenuName($data, 61, 1),
                    "url" => "/admin/BondCompany",
                    "sub_mini" => array (
                        
                        /* TAB#1 */
                        "add" => array (
                            "title"=>"เพิ่มข้อมูลบริษัทจัดการ",
                            "url" => "/admin/BondCompany/add"

                        ),

                        /* TAB#1 */ 
                        "edit" => array(
                            "title"=>"แก้ไขข้อมูลบริษัทจัดการ",
                            "url" => "/admin/BondCompany/edit"

                        )
                        
                        //////////////////////////////////////////////////
                        /* TAB#2 */
                        ,"category" => array (
                            "title"=> "หมวดหมู่ หลักทรัพย์    ",
                            "url" => "/admin/BondCompany/category"
                        )

                        /* TAB#2 */
                        ,"addcategory" => array (
                            "title"=>"เพิ่มข้อมูล หมวดหมู่ หลักทรัพย์    ",
                            "url" => "/admin/BondCompany/addCategory"

                        )
                        
                        /* TAB#2 */
                        ,"editcategory" => array (
                            "title"=>"แก้ไขข้อมูล หมวดหมู่ หลักทรัพย์  ",
                            "url" => "/admin/BondCompany/editCategory"

                        )

                        /* TAB#2 */
                        ,"getimportCategory" => array (
                            "title"=>"นำเข้าข้อมูล หมวดหมู่ หลักทรัพย์  ",
                            "url" => "/admin/BondCompany/getimportCategory"

                        )

                        ///////////////////////////////////////////////////
                        /* TAB#3 */
                        ,"bondindex" => array (
                            "title"=> "หมวดหมู่ หลักทรัพย์    ",
                            "url" => "/admin/BondCompany/bondindex"
                        )

                        /* TAB#3 */
                        ,"addBondIndex" => array(
                            "title"=>"เพิ่มข้อมูล หลักทรัพย์  ",
                            "url" => "/admin/BondCompany/addBondIndex"

                        ),
                        
                        /* TAB#3 */
                        "editBondIndex" => array(
                            "title"=>"แก้ไขข้อมูล หลักทรัพย์  ",
                            "url" => "/admin/BondCompany/editBondIndex"

                        ),
                        /* TAB#3 */
                        "getimportEquityIndex" => array(
                            "title"=>"นำเข้าข้อมูล หลักทรัพย์  ",
                            "url" => "/admin/BondCompany/getimportEquityIndex"

                        )
                        
                        //////////////
                        // TAB#4 Equity Broker
                        //////////////
                        ,"bondBroker" => array (
                            "title"=> "บริษัท Broker",
                            "url" => "/admin/BondCompany/bondBroker"
                        )

                        /* TAB#4 */
                        ,"addBroker" => array(
                            "title"=>"เพิ่มข้อมูล บริษัท Broker ",
                            "url" => "/admin/BondCompany/addBroker"

                        ),
                        
                        /* TAB#4 */
                        "editBroker" => array(
                            "title"=>"แก้ไขข้อมูล บริษัท Broker ",
                            "url" => "/admin/BondCompany/editBroker"

                        )
                    )
                ),

                /* START: group[61], submenu[2] นำเข้าตราสารหนี้ (61.2) */
                "2" => array (
                    "menu_id" => 2,
                    "title" => getMenuName($data, 61, 2),
                    "url" => "/admin/BondDataImport",

                    "sub_mini" => array(
                        
                        /* TAB#1 */
                        "add" => array(
                            "title"=>"เพิ่มข้อมูล ตราสารทุน",
                            "url" => "/admin/BondDataImport/add"
                        ),

                        /* TAB#1 */ 
                        "edit" => array(
                            "title"=>"แก้ไขข้อมูล ตราสารทุน",
                            "url" => "/admin/BondDataImport/edit"
                        ),

                        /* TAB#2 */
                        "datalist" => array (
                            "title"=> "ข้อมุลนำเข้าในระบบ",
                            "url" => "/admin/BondDataImport/datalist"
                        ),
                            
                        /* TAB#2 */
                        "editdata" => array(
                            "title"=>"แก้ไขข้อมูล ตราสารทุน ประเภทต่างๆในระบบกองทุนฯ ",
                            "url" => "/admin/BondDataImport/editdata"
                        ),

                        /* TAB#2 */
                        "getimport" => array(
                                "title"=>"นำเข้าข้อมูล ตราสารทุน ประเภทต่างๆในระบบกองทุนฯ  ",
                                "url" => "/admin/BondDataImport/getimport"
                        ),

                        /* เป็นเมนูสำหรับนําเข้าข้อมูล มูลค่า หน่วยลงทุน และอัตราผลตอบแทนเข้าสู่ระบบกองทุน ฯ */
                        /* TAB#2 */
                        "getAddUnitVal" => array(
                            "title"=>"เพิ่มข้อมูล มูลค่า หน่วยลงทุน และอัตราผลตอบแทนเข้าสู่ระบบกองทุน ฯ ",
                            "url" => "/admin/BondDataImport/getAddUnitVal"
                        ),

                        /* TAB#2 */ 
                        "editUnitVal" => array(
                            "title"=>"แก้ไขข้อมูล ข้อมูล มูลค่า หน่วยลงทุน และอัตราผลตอบแทนเข้าสู่ระบบกองทุน ฯ",
                            "url" => "/admin/BondDataImport/editUnitVal"
                        )
                    ) // sub_mini
                ) //[2]
            )
        );
    }

    ////////////////////////////////////////////////////////////////////////////////
     /**
     * Phase#2 Menu 62.) รายงานตราสานทุน (Equity Report)
     * 8 sub-menus
     */

    if (menu_access(1, 62) || menu_access(2, 62) || 
        menu_access(3, 62) || menu_access(4, 62) || 
        menu_access(5, 62) || menu_access(6, 62) || 
        menu_access(7, 62) || menu_access(8, 62))  {

        // $menu_title = getMenuName($data, 62, 1);  
        
        $page_nav["62"] = array (
            "group_id" => 62,
            "title" => getGoupName($data, 62),
            "icon" => "fa-table",

            "sub" => array (
                /* รายงาน ซื้อขายหลักทรัพย์ */ 
                "1" => array (
                    "menu_id" => 1,
                    "title" => getMenuName($data, 62, 1),
                    "url" => "/admin/EquityTradingReport"

                )
                /* รายงานข้อมูลลงทุนและอัตราตอบแทน */  
              /*  ,"2" => array (
                    "menu_id" => 2,
                    "title" => getMenuName($data, 62, 2),
                    "url" => "/admin/EquityNavReport"
                    
                )
*/
                /* รายงานข้อมูลค่าสินทรัพย์สุทธิอัตราตอบแทน */  
                ,"3" => array (
                    "menu_id" => 3,
                    "title" => getMenuName($data, 62, 3),
                    "url" => "/admin/EquityNavReport"
                )

                /* รายงาน ค่านายหน้าซื้อ-ขายหลักทรัพย์ (EquityReport-Brokerage)*/  
                ,"4" => array (
                    "menu_id" => 4,
                    "title" => getMenuName($data, 62, 4),
                    "url" => "/admin/EquityBrokerageReport"
                )

                /* รายงาน ความเลื่อนไหวตลาดตราสารทุน */  
                ,"5" => array (
                    "menu_id" => 5,
                    "title" => getMenuName($data, 62, 5),
                    "url" => "/admin/EquityClosedReport"
                )

                /* รายงาน ผลประโยชน์ และการลงทุน (Benefits Report) */  
                ,"6" => array (
                    "menu_id" => 6,
                    "title" => getMenuName($data, 62, 6),
                    "url" => "/admin/BenefitsReport"
                )

                /* รายงาน เปรียบเทียบขอบเขตการลงทุน ในตราสารทุน  Stock Universe */  
                ,"7" => array (
                    "menu_id" => 7,
                    "title" => getMenuName($data, 62, 7),
                    "url" => "/admin/EquityStockUniverseReport"
                )

                /* รายงาน การคำนวณค่าใช้จ่าย */  
                , "8" => array (
                    "menu_id" => 8,
                    "title" => getMenuName($data, 62, 8),
                    "url" => "/admin/EquityExpenseReport"
                ) //                   
            )
        );
    }

    /**
     * Phase#2 Menu 63) รายงานตราสารหนี้ (!!!! UNDER CONSTRUCTION !!!!)
     */
    
    if (menu_access(1, 63) || menu_access(2, 63) || 
        menu_access(3, 63) || menu_access(4, 63) ||
        menu_access(5, 63)) {
 
        $menu_title = getMenuName($data, 63, 1);  
        
        $page_nav["63"] = array(
            "group_id" => 63,
            "title" => getGoupName($data, 63),
            "icon" => "fa-table",

            "sub" => array (
                /* ตราสารหนี้ - รายงาน พอร์ทการลงทุน   (portfolio) */
                "1" => array (
                    "menu_id" => 1,
                    "title" => getMenuName($data, 63, 1),
                    "url" => "/admin/BondPortInvestmentReport" 
                ),
                /* ตราสารหนี้ - รายงานซื้อขาย */ 
                "2" => array (
                    "menu_id" => 2,
                    "title" => getMenuName($data, 63, 2),
                    "url" => "/admin/BondTradingReport" 
                ),
                /* ตราสารหนี้ - รายงานผลประโยชน์การลงทุน */ 
                "3" => array (
                    "menu_id" => 3,
                    "title" => getMenuName($data, 63, 3),
                    "url" => "/admin/BondBenefitsReport" 
                ),
                /* ตราสารหนี้ - รายงานผลประโยชน์การลงทุน */ 
                /*"4" => array (
                    "menu_id" => 4,
                    "title" => getMenuName($data, 63, 4),
                    "url" => "/admin/BondNavReport" 
                ),*/
                /* ตราสารหนี้ - รายงานรายละเอียดทรัพย์สินสุทธิ */ 
                "5" => array (
                    "menu_id" => 5,
                    "title" => getMenuName($data, 63, 5),
                    "url" => "/admin/BondNavReport" 
                )
            )
        );
    }
    
    /**
     * Phase#2 Menu 64) รายงาน กช 64-1,2
     */
    if(menu_access(1, 64) || menu_access(2, 64)) {

        $menu_title = getMenuName($data, 64, 1);  
        $page_nav["64"] = array(
            "group_id" => 64,
            "title" => getGoupName($data, 64),
            "icon" => "fa-table",

            "sub" => array (
                "1" => array (
                    "menu_id" => 1,
                    "title" => getMenuName($data, 64, 1),
                    "url" => "/admin/AdminKorchorReport",
                    "sub_mini" => array(
                        "getimport" => array(
                            "title"=>"นำข้อมูล รายงาน กช.",
                            "url" => "/admin/AdminKorchorReport/getimport"

                        )
                    )
                )
            )
        );
    }
    
    ////////////////////////////////////////////////////////////////////////////////

    foreach($page_nav as $index => $list){

        if(array_key_exists("url",$list)){
            if(Request::is( substr($list["url"],1,strlen($list["url"])))){
                $page_nav["dashboard"]["active"] = true;
            }

        } else {

            foreach($list["sub"] as $submenu){

                $arrurlsub = explode('/',$submenu["url"]);
                $path = $arrurlsub[1]. "/" . $arrurlsub[2];

                if(Request::is($path)){

                    $page_nav[$list["group_id"]]["sub"][$submenu["menu_id"]]["active"] = true;


                }

                if(array_key_exists("sub_mini",$submenu)){


                    foreach($submenu["sub_mini"] as $mini){
                        $arrurlsub = explode('/', $mini["url"]);
                        $path = $arrurlsub[1]. "/" . $arrurlsub[2] . "/" . $arrurlsub[3];

                        $current = Route::getCurrentRoute()->getPath();
                        $arrCount = explode('/',$current);

                        if(count($arrCount) > 2 ){

                            $Cpath = $arrCount[0] . "/" . $arrCount[1] . "/" . $arrCount[2];

                            if( $Cpath == $path){
                                $page_nav[$list["group_id"]]["sub"][$submenu["menu_id"]]["active"] = true;
                                break;
                            }

                        } else {
                            if(Request::is($path)){
                                $page_nav[$list["group_id"]]["sub"][$submenu["menu_id"]]["active"] = true;
                                break;
                            }
                        }
                       
                        // LAM
                        if(array_key_exists("sub_mini_level_1", $mini)) {
                            foreach($mini["sub_mini_level_1"] as $mini_level_1) {
                                $arrurlsub = explode('/', $mini_level_1["url"]);
                                $path = $arrurlsub[1]. "/" . $arrurlsub[2] . "/" . $arrurlsub[3] . "/" . $arrurlsub[4];

                                $current = Route::getCurrentRoute()->getPath();
                                $arrCount = explode('/',$current);
                                if(count($arrCount) > 3) {

                                    $Cpath = $arrCount[0] . "/" . $arrCount[1] . "/" . $arrCount[2] . "/" . $arrurlsub[3];

                                    //if( $Cpath == $path)
                                    {
                                        $page_nav[$list["group_id"]]["sub"] [$submenu["menu_id"]]  ["active"] = true;
                                        break;
                                    }

                                } else {
                                    //if(Request::is($path))
                                    {
                                        $page_nav[$list["group_id"]]["sub"][$submenu["menu_id"]]  ["active"] = true;
                                        break;
                                    }
                                }
                            }
                        }   
                        /////////////////////////////////////////////////////////////////////////////////////                    

                    }
                }


            }

        }


    }

    return $page_nav;
}


 function getMenutitle($arrSidebar){
 $ret = "";
     foreach($arrSidebar as $index => $list){
         if(!array_key_exists("url",$list)){

             foreach($list["sub"] as $submenu){
                 $arrurlsub = explode('/',$submenu["url"]);
                 $path = $arrurlsub[1]. "/" . $arrurlsub[2];
                 if(Request::is($path)){
                     $ret = $submenu["title"];

                     break;
                 }

                if(array_key_exists("sub_mini",$submenu)){


                    foreach($submenu["sub_mini"] as $mini){
                        $arrurlsub = explode('/',$mini["url"]);
                        $path = $arrurlsub[1]. "/" . $arrurlsub[2] . "/" . $arrurlsub[3];

                        $current = Route::getCurrentRoute()->getPath();
                        $arrCount = explode('/',$current);

                        if(count($arrCount) > 2 ){

                            $Cpath = $arrCount[0] . "/" . $arrCount[1] . "/" . $arrCount[2];

                            if( $Cpath == $path){
                                $ret = $mini["title"];
                                break;
                            }

                        }else{
                            if(Request::is($path)){
                                $ret = $mini["title"];
                                break;
                            }
                        }



                    }
                }

             }

         }
     }

     return $ret;
 }

/**
 * Convert  datetime to Thai datetime
 * @param string $strDate type of datetime (for example, 2016-12-25 23:30:34)
 * @param bool flag $withTime, if TRUE will connvert input with date and time, otherwise only Thai date 
 *        will process.
 * @return Thai datetime formatted (for example, 12 ส.ค. 2559)
 */
function toThaiDateTime($strDate, $withTime)
{       
    try { 
        $strYear = date("Y",strtotime($strDate)) + 543;
        $strMonth = date("n",strtotime($strDate));
        $strDay = date("j",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai = $strMonthCut[$strMonth];
        if ($withTime) {
            $strHour= date("H",strtotime($strDate));
            $strMinute= date("i",strtotime($strDate));
            $strSeconds= date("s",strtotime($strDate));
            
            return $strDay . " " . $strMonthThai. " ". $strYear .",  " . $strHour . ":" . $strMinute;
        } 
        return $strDay . " " . $strMonthThai. " ". $strYear ;
    } catch (\Exception $e) {
        Log::info($e->getMessage());
    } 
    return "date Error";
}

function getThaiMonth($month) {
        $thai_month_arr=array(
            "0"=>"",
            "1"=>"มกราคม",
            "2"=>"กุมภาพันธ์",
            "3"=>"มีนาคม",
            "4"=>"เมษายน",
            "5"=>"พฤษภาคม",
            "6"=>"มิถุนายน", 
            "7"=>"กรกฎาคม",
            "8"=>"สิงหาคม",
            "9"=>"กันยายน",
            "10"=>"ตุลาคม",
            "11"=>"พฤศจิกายน",
            "12"=>"ธันวาคม");
        return $thai_month_arr[$month];
}

function toThaiShortMonth($monthNum) {
        try { 
            $strMonth = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
            return  $strMonth[$monthNum];      
        } catch (\Exception $e) {
            Log::info($e->getMessage());
        }   
        return "";     
}

function toEnglishDate($strDate) {
        $v = explode(' ', $strDate);
        if(count($v) < 2) return "date Error";
        $monthNum = '';
        try { 
            if($v[1] == "ม.ค.")  $monthNum ='1';
            if($v[1] == "ก.พ.")  $monthNum ='2';
            if($v[1] == "มี.ค.") $monthNum ='3';
            if($v[1] == "เม.ย.") $monthNum ='4';
            if($v[1] == "พ.ค.")  $monthNum ='5';
            if($v[1] == "มิ.ย.") $monthNum ='6';
            if($v[1] == "ก.ค.")  $monthNum ='7';
            if($v[1] == "ส.ค.")  $monthNum ='8';
            if($v[1] == "ก.ย.")  $monthNum ='9';
            if($v[1] == "ต.ค.")  $monthNum ='10';
            if($v[1] == "พ.ย.")  $monthNum ='11';
            if($v[1] == "ธ.ค.")  $monthNum ='12';
            //$ar = array($v[0], $monthNum, $v[2]);
            //$newstrDate = implode('-', $ar);
            //$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."); 
            $newstrDate = str_replace($v[1], $monthNum, $strDate);
            $newstrDate = str_replace(" ", "-", $newstrDate);
            return $newstrDate;

        } catch (\Exception $e) {
            Log::info($e->getMessage());
        } 
        return $strDate;
    }
?>